<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="EduBridge">
  <meta name="author" content="EduBridge">
  <title>PIPE Psikotest</title>
  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <!-- Fonts -->
  <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"> -->
  
  <!-- Icons -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/nucleo/css/nucleo.css'); ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css'); ?>" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/css/argon.css?v=1.2.0'); ?>" type="text/css">
  <!-- Datatable -->
  <link rel="stylesheet" href="<?php echo base_url('src/node_modules/mdbootstrap/css/addons/datatables2.min.css'); ?>" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel='stylesheet'>
  <link rel="stylesheet" href="<?php echo base_url('src/assets/js/notifications.css'); ?>" type="text/css">


<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
  <!-- Radio image -->
  <style type="text/css">
    /* montserrat-regular - latin */
body {
  font-family: 'Montserrat';
}
    .input-hidden {
      position: absolute;
      left: -9999px;
    }
    input[type=radio]:checked + label>img {
      /*border: 1px solid #fff;*/
      box-shadow: 0 0 1px 1px #9C020E;
    }

    /* Stuff after this is only to make things more pretty */
    input[type=radio] + label>img {
      border: 0.5px dashed #ECEFF5;
      width: 150px;
      height: 150px;
      transition: 500ms all;
    }

  /*<!-- sempitkan tabel summary -->*/
  .card .sempit td, .card .sempit th {
    padding-top: 0.1rem;
    padding-right: 0rem;
    padding-bottom: 0.1rem;
    padding-left: 0rem;
}
  </style>
  <script>
  function save_all(){
           console.log('save all');
           $('#save_summary0').submit();
           $('#save_summary1').submit();
           $('#save_summary2').submit();
           $('#save_finalsummary').submit();
  }
  </script>

</head>

<body>
  <!-- Sidenav -->
  <div class="sidenav navbar navbar-vertical  fixed-  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    Im floating
  </div>
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <style type="text/css">
        .sidenav-header {
              display: block;
              margin-top: 1rem;
              border-bottom: 1px solid rgba(0, 0, 0, 0.1);
         }
      </style>
      <div class="sidenav-header  align-items-center">
          <div class="row">
            <div class="col-10">
              <a class="navbar-brand" href="<?php echo base_url(''); ?>" style="padding: 12px;">
              <img src="<?php echo base_url('src/assets/img/brand/blue.png'); ?>" class="navbar-brand-img" alt="...">
                <!-- <img src="assets/img/brand/blue.png" class="navbar-brand-img" alt="..."> -->
              </a>
            </div>
            <style type="text/css">
              .sidenav-toggler {
                    position: relative;
                    cursor: pointer;
                    display: inline-block;
                    padding: 0;
                }
            </style>
            <div class="col-2 pr-3 sidenav-toggler sidenav-toggler-dark active" data-action="sidenav-pin" data-target="#sidenav-main" style="padding-top: 14px; text-align: right;">
              <div class="sidenav-toggler-inner">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-x" viewBox="0 0 16 16">
                  <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                </svg>
              </div>
            </div>
          </div>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">

          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link <?php if ($scope=='index') echo 'active'; ?>" href="<?php echo base_url('dashboard'); ?>">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link <?php if ($scope=='online') echo 'active'; ?>" href="<?php echo base_url('test/online'); ?>">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Online tes</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link <?php if ($scope=='test') echo 'active'; ?>" href="<?php echo base_url('test/paket_tes'); ?>">
                <i class="ni ni-collection text-primary"></i>
                <span class="nav-link-text"><?= lang('Global.ready_summarize') ?></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link <?php if ($scope=='correctionstart-summarization') echo 'active'; ?>" href="<?php echo base_url('test/correction/start-summarization'); ?>">
                <i class="ni ni-archive-2 text-primary"></i>
                <span class="nav-link-text">Sedang dianalisa</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link <?php if ($scope=='correctionon-qc') echo 'active'; ?>" href="<?php echo base_url('test/correction/on-qc'); ?>">
                <i class="ni ni-paper-diploma text-primary"></i>
                <span class="nav-link-text">Sedang QC</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link <?php if ($scope=='correctionsummarized') echo 'active'; ?>" href="<?php echo base_url('test/correction/summarized'); ?>">
                <i class="ni ni-check-bold text-primary"></i>
                <span class="nav-link-text">Selesai dianalisa</span>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link <?php if ($scope=='correction') echo 'active'; ?>" href="<?php echo base_url('test/correction'); ?>">
                <i class="ni ni-check-bold text-primary"></i>
                <span class="nav-link-text"><?= lang('Global.my_summarization') ?></span>
              </a>
            </li> -->
          </ul>
          <!-- Divider -->
          <hr class="my-3">
          <!-- Heading -->
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal"><?= lang('Global.documentation') ?></span>
          </h6>
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link <?php if ($scope=='product') echo 'active'; ?>" href="<?php echo base_url('dashboard/product'); ?>">
                <i class="ni ni-single-copy-04"></i>
                <span class="nav-link-text"><?= lang('Global.test_package') ?></span>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link <?php if ($scope=='jenis_tes') echo 'active'; ?>" href="<?php echo base_url('admin/jenis_tes'); ?>">
                <i class="ni ni-collection"></i>
                <span class="nav-link-text">Jenis tes</span>
              </a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link <?php if ($scope=='faq') echo 'active'; ?>" href="<?php echo base_url('psychologist/faq'); ?>">
                <i class="ni ni-spaceship"></i>
                <span class="nav-link-text">FAQ</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Search form -->
          <!-- <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
            <div class="form-group mb-0">
              <div class="input-group input-group-alternative input-group-merge">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-search"></i></span>
                </div>
                <input class="form-control" placeholder="Search" type="text">
              </div>
            </div>
            <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </form> -->
          <!-- Navbar links -->
          <ul class="navbar-nav align-items-center  ml-md-auto ">
            <li class="nav-item d-xl-none">
              <!-- Sidenav toggler -->
              <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </div>
            </li>
            <!-- <li class="nav-item d-sm-none">
              <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                <i class="ni ni-zoom-split-in"></i>
              </a>
            </li> -->
            
          </ul>
          <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
            <li class="nav-item dropdown">
              <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
                  </span>
                  <div class="media-body  ml-2  d-none d-lg-block">
                    <span class="mb-0 text-sm  font-weight-bold"><?= $profile->first_name.' '.$profile->last_name ?></span>
                  </div>
                </div>
              </a>
              <div class="dropdown-menu  dropdown-menu-right ">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0"><?= lang('Global.welcome') ?></h6>
                </div>
                <a href="<?php echo base_url('account/profile'); ?>" class="dropdown-item">
                  <i class="ni ni-single-02"></i>
                  <span><?= lang('Global.my_profile') ?></span>
                </a>
                <a href="settings" class="dropdown-item">
                  <i class="ni ni-settings-gear-65"></i>
                  <span><?= lang('Global.settings') ?></span>
                </a>
                <!-- <a href="#!" class="dropdown-item">
                  <i class="ni ni-calendar-grid-58"></i>
                  <span>Activity</span>
                </a> -->
                <!-- <a href="#!" class="dropdown-item">
                  <i class="ni ni-support-16"></i>
                  <span>Support</span>
                </a> -->
                <div class="dropdown-divider"></div>
                <a href="<?= base_url('auth/logout'); ?>" class="dropdown-item">
                  <i class="ni ni-user-run"></i>
                  <span>Logout</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Main Content -->
    <!-- <div> -->
      <?php 
      if (isset($template)) echo view($template); 
    ?>
    
    </div>
  </div>
  <script src="<?php echo base_url('src/assets/js/notifications.js'); ?>"></script>


  <!-- Vue js -->
  <script src="https://vuejs.org/js/vue.min.js"></script>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="<?php echo base_url('src/assets/vendor/jquery/dist/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/js-cookie/js.cookie.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js'); ?>"></script>
  <!-- Optional JS -->
  <script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.extension.js'); ?>"></script>
  <!-- Argon JS -->
  <script src="<?php echo base_url('src/assets/js/argon.js?v=1.2.0'); ?>"></script>
  <!-- Datatable -->
  <script src="<?php echo base_url('src/node_modules/mdbootstrap/js/addons/datatables2.min.js'); ?>" type="text/javascript"></script>
  
  <script type="text/javascript">
  $(document).ready(function () {
  var table = $('#dtBasicExample').DataTable({
    
  });
  $('ul').on('click', '.all', function() {
      table
        .search('')
        .columns()
        .search('')
        .draw();
    });
    $('ul').on( 'click', '.ringkas', function () {
    table
        .columns(4)
        .search($(this).text())
        .draw();
    });
    $('ul').on( 'click', '.member', function () {
      regex = '\\b' + $(this).text() + '\\b';
    table
        .columns(1)
        .search(regex, true, false)
        .draw();
    });

    $('ul').on( 'click', '.membermenu', function () {
      regex = '\\b' + $(this).text() + '\\b';
    table
        .columns(0)
        .search(regex, true, false)
        .draw();
    });
  $('.dataTables_length').addClass('bs-select');
});
</script>
<script type="text/javascript">
$(document).ready(function() { 
  <?php
  if (!empty($_SESSION['info_data'])){
    ?>
    const myNotification = window.createNotification({
                });
      myNotification({ 
        title: 'Pesan',
        message: '<?= $_SESSION['info_data'] ?>',
        closeOnClick: true,
        displayTutupButton: false,
        positionClass: 'nfc-top-left',
        onclick: false,
        showDuration: 5000,
        theme: 'info'
        });

      <?php unset($_SESSION['info_data']);  ?>
    <?php
  }
  ?>
 });
  
</script>
<script type="text/javascript">
  var textareaValue = $('#summernote0').summernote('code');
  console.log(textareaValue);
  // save score 
  $(":input").bind('keyup mouseup', function(e){
        console.log("hai");
        $jawaban = e.target.id;
        var score = e.target.value;
        var code = $jawaban.split("_");
        $link = window.location.href;
        var id_memberorderproductjenistes = $link.slice(-1);
        // todo
        
        var bagian_tes=code[0];
        var urutan_soal=code[1];
        var urutan_jawaban=code[2];
        // console.log(urutan_soal+"ada"+urutan_jawaban);
        $.ajax({
         url:'<?=base_url()?>/test/save_correction',
         method: 'post',
         data: {
          urutan_soal: urutan_soal, 
          urutan_jawaban: urutan_jawaban,
          score: score,
          bagian_tes: bagian_tes, 
          id_memberorderproductjenistes: id_memberorderproductjenistes},
         dataType: 'json',
         success: function(response){
        console.log(response);
         }
       });
    }); 

</script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
  $('#summernote0').summernote({
    toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['table', ['table']],
    ['insert', ['link', 'picture', 'video']],
  ],
  height: 150,   //set editable area's height
});
  $('#summernote1').summernote({
    toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['table', ['table']],
    ['insert', ['link', 'picture', 'video']],
  ],
  height: 150,   //set editable area's height
});
  $('#summernote2').summernote({
    toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['table', ['table']],
    ['insert', ['link', 'picture', 'video']],
  ],
  height: 150,   //set editable area's height
});
  $('#finalsummernote').summernote({
    toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['table', ['table']],
    ['insert', ['link', 'picture', 'video']],
  ],
  height: 100,   //set editable area's height
});


  // save summary
     $(function(){
         $('#save_summary0').submit(function(event){

          //  var messageData = $('#summernote0').summernote('code');
          var messageData = "";
           var id_memberorderproductjenistes = $('#id_memberorderproductjenistes0').val();
           var jenistes = $('#jenistes0').val();
           var chart = $('#coolImage0').val();
           var iq = $('#iq').val();
           var tingkat_kecerdasan = $('#tingkat_kecerdasan').val();
           console.log(id_memberorderproductjenistes0);

          event.preventDefault();
          $.ajax({
             url:'<?=base_url()?>/test/save_summary',
             method: 'post',
             data: {
              chart: chart,
              summary: messageData,
              iq: iq,
              tingkat_kecerdasan: tingkat_kecerdasan,
              id_memberorderproductjenistes: id_memberorderproductjenistes},
             dataType: 'json',
             success: function(response){
                const myNotification = window.createNotification({
                });
                if(response==1){
                  myNotification({ 
                      title: 'Disimpan',
                    message: 'Chart dan penjelasan '+jenistes+' berhasil disimpan',
                    closeOnClick: true,
                    displayCloseButton: false,
                    positionClass: 'nfc-top-right',
                    onclick: false,
                    showDuration: 3500,
                    theme: 'success'
                    });
                } 
              console.log(response);
              },error: function(errorThrown){
                  // console.log(errorThrown);
                  // alert(errorThrown);
                  alert("Data tidak berhasil disimpan. Cek login Anda atau koneksi internet Anda");
             }    
           });
          });
         $('#save_summary1').submit(function(event){

          //  var messageData = $('#summernote1').summernote('code');
          var messageData = "";
           var id_memberorderproductjenistes = $('#id_memberorderproductjenistes1').val();
           var jenistes = $('#jenistes1').val();
           var chart = $('#coolImage1').val();
           var iq = $('#iq').val();
           var tingkat_kecerdasan = $('#tingkat_kecerdasan').val();
           console.log(id_memberorderproductjenistes);

          event.preventDefault();
          $.ajax({
             url:'<?=base_url()?>/test/save_summary',
             method: 'post',
             data: {
              chart: chart,
              summary: messageData,
              iq: iq,
              tingkat_kecerdasan: tingkat_kecerdasan,
              id_memberorderproductjenistes: id_memberorderproductjenistes},
             dataType: 'json',
             success: function(response){
              const myNotification = window.createNotification({
                });
                if(response==1){
                  myNotification({ 
                      title: 'Disimpan',
                    message: 'Chart dan penjelasan '+jenistes+' berhasil disimpan',
                    closeOnClick: true,
                    displayCloseButton: false,
                    positionClass: 'nfc-top-right',
                    onclick: false,
                    showDuration: 3500,
                    theme: 'success'
                    });
                } else  {
                  myNotification({ 
                    title: 'Data tidak berhasil disimpan',
                    message: 'Data '+jenistes+' tidak berhasil disimpan',
                    closeOnClick: true,
                    displayCloseButton: false,
                    positionClass: 'nfc-top-right',
                    onclick: false,
                    showDuration: 3500,
                    theme: 'error'
                    });
                }
            console.log(response);
             }, error: function(errorThrown){
                  // alert(errorThrown);
                  alert("There is an error, check internet connection!");
             }
           });
          });
         $('#save_summary2').submit(function(event){

          //  var messageData = $('#summernote2').summernote('code');
          var messageData = "";
           var id_memberorderproductjenistes = $('#id_memberorderproductjenistes2').val();
           var jenistes = $('#jenistes2').val();
           var chart = $('#coolImage2').val();
           var iq = $('#iq').val();
           var tingkat_kecerdasan = $('#tingkat_kecerdasan').val();
           console.log(id_memberorderproductjenistes);

          event.preventDefault();
          $.ajax({
             url:'<?=base_url()?>/test/save_summary',
             method: 'post',
             data: {
              chart: chart,
              summary: messageData,
              iq: iq,
              tingkat_kecerdasan: tingkat_kecerdasan,
              id_memberorderproductjenistes: id_memberorderproductjenistes},
             dataType: 'json',
             success: function(response){
              const myNotification = window.createNotification({
                });
                if(response==1){
                  myNotification({ 
                      title: 'Disimpan',
                    message: 'Chart dan penjelasan '+jenistes+' berhasil disimpan',
                    closeOnClick: true,
                    displayCloseButton: false,
                    positionClass: 'nfc-top-right',
                    onclick: false,
                    showDuration: 3500,
                    theme: 'success'
                    });
                } else  {
                  myNotification({ 
                    title: 'Data tidak berhasil disimpan',
                    message: 'Data '+jenistes+' tidak berhasil disimpan',
                    closeOnClick: true,
                    displayCloseButton: false,
                    positionClass: 'nfc-top-right',
                    onclick: false,
                    showDuration: 3500,
                    theme: 'error'
                    });
                }
            console.log(response);
             }, error: function(errorThrown){
                  // alert(errorThrown);
                  alert("There is an error, check internet connection!");
             }      
           });
          });
         $('#save_finalsummary').submit(function(event){

           var messageData = $('#finalsummernote').summernote('code');
           var id_memberorderproduct = $('#id_memberorderproduct').val();
           var jurusan1 = $('#juduljurusan1').val();
           var descjurusan1 = $('#descjurusan1').val();
           var jurusan2 = $('#juduljurusan2').val();
           var descjurusan2 = $('#descjurusan2').val();
           var jurusan3 = $('#juduljurusan3').val();
           var descjurusan3 = $('#descjurusan3').val();
           var jurusan4 = $('#juduljurusan4').val();
           var descjurusan4 = $('#descjurusan4').val();
           var jurusan5 = $('#juduljurusan5').val();
           var descjurusan5 = $('#descjurusan5').val();
           console.log(id_memberorderproduct);

          event.preventDefault();
          $.ajax({
             url:'<?=base_url()?>/test/save_finalsummary',
             method: 'post',
             data: {
              summary: messageData,
              jurusan1: jurusan1,
              jurusan2: jurusan2,
              jurusan3: jurusan3,
              jurusan4: jurusan4,
              jurusan5: jurusan5,
              descjurusan1: descjurusan1,
              descjurusan2: descjurusan2,
              descjurusan3: descjurusan3,
              descjurusan4: descjurusan4,
              descjurusan5: descjurusan5,
              id_memberorderproduct: id_memberorderproduct},
             dataType: 'json',
             success: function(response){
              const myNotification = window.createNotification({
                });
                if(response==1){
                  myNotification({ 
                      title: 'Disimpan',
                    message: 'Jurusan dan ringkasan akhir berhasil disimpan',
                    closeOnClick: true,
                    displayCloseButton: false,
                    positionClass: 'nfc-top-right',
                    onclick: false,
                    showDuration: 3500,
                    theme: 'success'
                    });
                } else  {
                  myNotification({ 
                    title: 'Data tidak berhasil disimpan',
                    message: 'Data jurusan dan ringkasan akhir tidak berhasil disimpan',
                    closeOnClick: true,
                    displayCloseButton: false,
                    positionClass: 'nfc-top-right',
                    onclick: false,
                    showDuration: 3500,
                    theme: 'error'
                    });
                }
            console.log(response);
             }, error: function(errorThrown){
                  // alert(errorThrown);
                  alert("There is an error, check internet connection!");
             }
           });
          });
          $('#request_qc').submit(function(event){
            var id_memberorderproduct = $('#id_memberorderproduct').val();
            event.preventDefault();
            $.ajax({
              url:'<?=base_url()?>/test/request_qc',
              method: 'post',
              data: {
              on_qc: 1,
              id_memberorderproduct: id_memberorderproduct},
              dataType: 'json',
              success: function(response){
              const myNotification = window.createNotification({
                });
                if(response==1){
                  myNotification({ 
                      title: 'Disimpan',
                    message: 'Request QC berhasil',
                    closeOnClick: true,
                    displayCloseButton: false,
                    positionClass: 'nfc-top-right',
                    onclick: false,
                    showDuration: 3500,
                    theme: 'success'
                    });
                } else  {
                  myNotification({ 
                    title: 'Data tidak berhasil disimpan',
                    message: 'Request QC tidak berhasil',
                    closeOnClick: true,
                    displayCloseButton: false,
                    positionClass: 'nfc-top-right',
                    onclick: false,
                    showDuration: 3500,
                    theme: 'error'
                    });
                }
            console.log(response);
              }, error: function(errorThrown){
                  // alert(errorThrown);
                  alert("There is an error, check internet connection!");
              }
            });
            });
          
     });

});
</script>
<!-- chart untuk tes kecerdasan-->
<script type="text/javascript">
  <?php
  if(isset($jenistes)){
    // ganti urutan
    $bagiantes[0] = $jenistes[0]['bagiantes'][0];
    $bagiantes[1] = $jenistes[0]['bagiantes'][1];
    $bagiantes[2] = $jenistes[0]['bagiantes'][2];
    $bagiantes[3] = $jenistes[0]['bagiantes'][3];
    $bagiantes[4] = $jenistes[0]['bagiantes'][8];
    $bagiantes[5] = $jenistes[0]['bagiantes'][4];
    $bagiantes[6] = $jenistes[0]['bagiantes'][5];
    $bagiantes[7] = $jenistes[0]['bagiantes'][6];
    $bagiantes[8] = $jenistes[0]['bagiantes'][7];
    $persentil = array(140 => 100, 139 => 100, 138 => 100, 137 => 100, 136 => 100, 135 => 100, 134 => 100, 133 => 100, 132 => 100, 131 => 100, 130 => 100, 129 => 100, 128 => 100, 127 => 100, 126 => 100, 125 => 99, 124 => 99, 123 => 99, 122 => 99, 121 => 98, 120 => 98, 119 => 97, 118 => 96, 117 => 95, 116 => 95, 115 => 93, 114 => 92, 113 => 90, 112 => 88, 111 => 86, 110 => 84, 109 => 81, 108 => 79, 107 => 76, 106 => 73, 105 => 70, 104 => 66, 103 => 62, 102 => 58, 101 => 54, 100 => 50, 99 => 46,
                            98 => 42, 97 => 38, 96 => 34, 95 => 30, 94 => 27, 93 => 24, 92 => 21, 91 => 18, 90 => 15, 89 => 14, 88 => 12, 87 => 10, 86 => 8, 85 => 7, 84 => 5, 83 => 4, 82 => 3, 81 => 2, 80 => 2, 79 => 1, 78 => 1, 77 => 1, 76 => 1, 75 => 0, 74 => 0, 73 => 0, 72 => 0, 71 => 0, 70 => 0, 69 => 0, 68 => 0, 67 => 0, 66 => 0, 65 => 0, 64 => 0, 63 => 0, 62 => 0, 61 => 0, 60 => 0, 59 => 0, 58 => 0, 
                                                        );
    foreach ($bagiantes as $key => $bagian) {
      if ($bagian['konversi'][0]['code']){
        $code[$key] = $bagian['konversi'][0]['code'];}else {$code[$key] = 0;}
      if($bagian['konversi'][0]['sw']){$sw[$key] = $bagian['konversi'][0]['sw'];} else {$sw[$key] =0;};
      if(!empty($persentil[$bagian['konversi'][0]['sw']])){$persentil_chart[$key] = $persentil[$bagian['konversi'][0]['sw']];} else {$persentil_chart[$key] =0;};
    }

    $new_code = ['Daya Pikir', 'Kemampuan Bahasa','Fleksibilitas Berpikir','Penalaran Logis', 'Daya Ingat', 'Berpikir Praktis', 'Pemikiran Konkret', 'Daya Bayang', 'Pemikiran Spasial'];
    ?>

  var swChart = (function() {
  // Variables
  var $chart = $('#chart-sw0');
  // Methods
  function init($chart) {
    var swChart = new Chart($chart, {
      type: 'line',
      options: {
        tooltips: {enabled: false},
        scales: {
          yAxes: [{
            gridLines: {
              // lineWidth: 1,
              // pointBorderWidth: 2,
              // color: Charts.colors.gray[900],
              // zeroLineColor: Charts.colors.gray[900]
            },
            ticks: {
              callback: function(value) {
                if (!(value % 10)) {
                  return  value ;
                }
              }
            }
          }]
        },
      },
      data: {
        labels: ["<?php echo implode('", "',$code) ?>"],
        datasets: [{
          label: 'SW',
          data: [<?php echo implode(', ',$sw) ?>],
        }]
      }
    });
    // Save to jQuery object
    $chart.data('chart', swChart);
  };
  if ($chart.length) {
    init($chart);
  }

})();
var barChart = (function() {
  // Variables
  var $barChart = $('#chart-pdf0');
  // Methods
  function init($barChart) {
    var barChart = new Chart($barChart, {
      type: 'horizontalBar',
      options: {
        tooltips: {enabled: false},
        animation: {
          onComplete: function() {
          document.getElementById('coolImage0').setAttribute('value', barChart.toBase64Image());
          }
        },
        scales: {
          xAxes: [{ 
              // type: 'linear',
              gridLines: {
                  display: true,
              },
              ticks: {
                callback: function(value, index, values) {
                  return parseFloat(value).toFixed();
                },
                autoSkip: true,
                stepSize: 1,
                fontColor: "#9C020E", // this here
                max:100,
              },
          }],   
          yAxes: [{
            gridLines: {
              // lineWidth: 1,
              // pointBorderWidth: 2,
              // color: Charts.colors.gray[900],
              // zeroLineColor: Charts.colors.gray[900]
            },
            ticks: {
              fontColor: "#172B4D",
              fontSize : 14,
              callback: function(value) {
                if (!(value % 10)) {
                  return  value ;
                }
              }
            }
          }]
        },
      },
      data: {
        labels: ["<?php echo implode('", "',$new_code) ?>"],
        datasets: [{
          label: 'SW',
          data: [<?php echo implode(', ',$persentil_chart) ?>],
        }]
      }
    });
    // Save to jQuery object
    $barChart.data('chart', barChart);
  };
  if ($barChart.length) {
    init($barChart);
  }

})();
    <?php
  }
  ?>
</script>

<!-- chart untuk tes  minat bakat -->
<script type="text/javascript">
  <?php
$riasec_aktivitas[0]['male'] = [7, 15, 22, 30, 38, 47, 56, 65, 75, 85, 90, 99];
$riasec_aktivitas[0]['female'] =  [20 , 54, 64, 75, 82, 85, 90, 94, 96, 99, 99, 99];
$riasec_aktivitas[1]['male'] = [18, 29, 39, 49, 57, 65, 74, 82, 86, 92, 96, 99];
$riasec_aktivitas[1]['female'] =  [21, 33, 45, 55, 63, 74, 78, 86, 91, 93, 96, 99];
$riasec_aktivitas[2]['male'] = [12, 22, 33, 45, 57, 70, 77, 84, 89, 94, 97, 99];
$riasec_aktivitas[2]['female'] =  [8, 15, 23, 31, 45, 56, 68, 77, 85, 91, 96, 99];
$riasec_aktivitas[3]['male'] = [18, 32, 48, 58, 66, 73, 81, 87, 94, 95, 98, 99];
$riasec_aktivitas[3]['female'] =  [3, 6, 12, 21, 30, 41, 52, 62, 77, 88, 96, 99];
$riasec_aktivitas[4]['male'] = [5, 11, 20, 29, 39, 46, 59, 68, 73, 83, 91, 99];
$riasec_aktivitas[4]['female'] =  [10, 15, 23, 30, 38, 47, 60, 71, 83, 90, 96, 99];
$riasec_aktivitas[5]['male'] = [25, 40, 56, 66, 73, 79, 86, 89, 93, 95, 97, 99];
$riasec_aktivitas[5]['female'] =  [19, 36, 47, 58, 66, 72, 79, 86, 91, 94, 97, 99];

$riasec_pekerjaan[0]['male'] = [17, 29, 38, 48, 55, 62, 69, 73, 81, 86, 91, 93, 95, 98, 99];
$riasec_pekerjaan[0]['female'] = [60, 80, 87, 90, 93, 94, 96, 98, 98, 99, 99, 99, 99, 99, 99];
$riasec_pekerjaan[1]['male'] = [33, 45, 57, 66, 72, 78, 83, 87, 91, 94, 96, 97, 98, 98, 99];
$riasec_pekerjaan[1]['female'] = [28, 45, 56, 64, 71, 78, 84, 89, 91, 94, 97, 98, 99, 99, 99];
$riasec_pekerjaan[2]['male'] = [20, 34, 47, 62, 70, 75, 80, 85, 88, 91, 93, 95, 97, 98, 99];
$riasec_pekerjaan[2]['female'] = [20, 31, 42, 52, 61, 70, 77, 81, 86, 90, 93, 95, 96, 98, 99];
$riasec_pekerjaan[3]['male'] = [36, 54, 64, 70, 76, 81, 85, 87, 90, 92, 94, 96, 97, 97, 99];
$riasec_pekerjaan[3]['female'] = [12, 21, 31, 39, 45, 54, 60, 67, 72, 79, 84, 89, 93, 95, 99];
$riasec_pekerjaan[4]['male'] = [15, 27, 38, 48, 59, 65, 72, 76, 81, 83, 89, 92, 95, 97, 99];
$riasec_pekerjaan[4]['female'] = [23, 36, 47, 57, 62, 71, 78, 83, 86, 89, 92, 95, 97, 99, 99];
$riasec_pekerjaan[5]['male'] = [45, 71, 79, 83, 86, 88, 90, 91, 93, 95, 95, 95, 96, 97, 99];
$riasec_pekerjaan[5]['female'] = [48, 66, 74, 80, 83, 87, 90, 92, 93, 94, 96, 97, 98, 98, 99];

$riasec_kemampuan[0]['male'] = [3, 6, 9, 15, 23, 31, 43, 53, 65, 77, 86, 89];
$riasec_kemampuan[0]['female'] = [14, 28, 46, 60, 71, 80, 87, 92, 96, 98, 99, 99];
$riasec_kemampuan[1]['male'] = [5, 11, 17, 23, 32, 42, 55, 69, 79, 89, 95, 99];
$riasec_kemampuan[1]['female'] = [10, 17, 25, 37, 46, 54, 67, 78, 85, 92,97, 99];
$riasec_kemampuan[2]['male'] = [9, 20, 31, 45, 55, 68, 77, 85, 90, 95, 98, 99];
$riasec_kemampuan[2]['female'] = [9, 12, 20, 33, 44, 58, 70, 80, 88, 95, 98, 99];
$riasec_kemampuan[3]['male'] = [6, 14, 20, 26, 33, 42, 54, 63, 72, 82, 92, 99];
$riasec_kemampuan[3]['female'] = [3, 4, 6, 9, 14, 20, 28, 37, 51, 68, 82, 99];
$riasec_kemampuan[4]['male'] = [6, 14, 24, 31, 43, 49, 60, 69, 76, 83, 91, 99];
$riasec_kemampuan[4]['female'] = [4, 8, 15, 27, 34, 46, 54, 66, 78, 88, 94, 99];
$riasec_kemampuan[5]['male'] = [12, 25, 38, 49, 58, 67, 77, 85, 91, 95, 99, 99];
$riasec_kemampuan[5]['female'] = [8, 18, 27, 36, 46, 57, 70, 80, 86, 93, 98, 99];

  if(isset($jenistes[1])){
    $r[0] = 0;
    $r[1] = 0;
    $r[2] = 0;
    $r[3] = 0;
    $r[4] = 0;
    $r[5] = 0;
    $code_riasec = ['R', 'I', 'A', 'S', 'E', 'C'];
    $code_riasec_desc = ['Realistis', 'Investigatif', 'Artistik', 'Sosial', 'Enterprising', 'Conventional'];
    foreach ($jenistes[1]['bagiantes'] as $key => $bagian) {
    foreach ($bagian['riasec'] as $key2 => $riasec) {
      // if ($riasec['score']){
      //   // if($bagian['urutan']==1){
      //     $score[$key][] = $riasec['score'];
      //   // }
      //   $r[$key2] = $r[$key2] + $riasec['score'];
      // } else {
      //   $score[$key][] = 0;
      // }

      if(!empty($riasec['score'])){
        switch ($key) {
          case 0:
            if(!empty($riasec_aktivitas[$key2][$orders[0]['member'][0]['sex']][$riasec['score']])){
                $riasec_konversi = $riasec_aktivitas[$key2][$orders[0]['member'][0]['sex']][$riasec['score']];
            } else {
                $riasec_konversi = 0;
            }
            
            break;
          case 1:
            if(!empty($riasec_kemampuan[$key2][$orders[0]['member'][0]['sex']][$riasec['score']])){
                $riasec_konversi = $riasec_kemampuan[$key2][$orders[0]['member'][0]['sex']][$riasec['score']];
            } else {
                $riasec_konversi = 0;
            }
            break;
          case 2:
            if(!empty($riasec_pekerjaan[$key2][$orders[0]['member'][0]['sex']][$riasec['score']])){
                $riasec_konversi = $riasec_pekerjaan[$key2][$orders[0]['member'][0]['sex']][$riasec['score']];
            } else {
                $riasec_konversi = 0;
            }
            break;
        } 
      } else {
        switch ($key) {
          case 0:
            $riasec_konversi = $riasec_aktivitas[$key2][$orders[0]['member'][0]['sex']][0];
            break;
          case 1:
            $riasec_konversi = $riasec_kemampuan[$key2][$orders[0]['member'][0]['sex']][0];
            break;
          case 2:
            $riasec_konversi = $riasec_pekerjaan[$key2][$orders[0]['member'][0]['sex']][0]; 
            break;
        }
      }

       $score[$key][] = $riasec_konversi;
        // }
        $r[$key2] = $r[$key2] + $riasec_konversi;
      ?>
      <?php
    }
    }

    ?>
  var minkatChart = (function() {
  // Variables
  var $chart = $('#chart-sw1');
  // Methods
  function init($chart) {
    var minkatChart = new Chart($chart, {
      type: 'radar',
      options: {
        tooltips: {enabled: false},
        legend: {
            display: true,
            labels: {
                fontColor: '#525f7f',
            },
            position: 'top',
        },
        scales: {
          yAxes: [{
            ticks: {
              callback: function(value) {
                if (!(value % 10)) {
                  return  value ;
                }
              }
            }
          }],
          xAxes: [{
            categoryPercentage: 1.0,
            barPercentage: 1.0
        }]
        },
      },
      data: {
        labels: ["<?php echo implode('", "',$code_riasec) ?>"],
        datasets: [
        {
          label: 'Aktivitas',
          backgroundColor: "rgba(156,2,14,0.1)",
          borderColor: "#9C020E",
          borderWidth : 2,
          data: [<?php if(!empty($score[0])) echo implode(', ',$score[0]) ?>],
        },
        {
          label: 'Persepsi Kemampuan',
          backgroundColor: "rgba(255,153,0,0.1)",
          borderColor: "rgba(255,153,0,1)",
          borderWidth : 2,
          data: [<?php if(!empty($score[1])) echo implode(', ',$score[1]) ?>],
        },
        {
          label: 'Pekerjaan',
          backgroundColor: "rgba(119,169,232,0.1)",
          borderColor: "rgba(119,169,232,1)",
          borderWidth : 2,
          data: [<?php if(!empty($score[2])) echo implode(', ',$score[2]) ?>],
        },
        {
          label: 'Total',
          backgroundColor: "rgba(45,206,137,0.1)",
          borderColor: "rgba(45,206,137,1)",
          borderWidth : 2,
          data: [<?php echo implode(', ',$r) ?>],
        },
        ]
      }
    });
    // Save to jQuery object
    $chart.data('chart', minkatChart);
  };
  if ($chart.length) {
    init($chart);
  }

})();
var minkatChartpdf = (function() {
  // Variables
  var $chart = $('#chart-pdf1');
  // Methods
  function init($chart) {
    var minkatChartpdf = new Chart($chart, {
      type: 'horizontalBar',
      options: {
        tooltips: {enabled: false},
        legend: {
            display: true,
            labels: {
                fontColor: '#525f7f',
            },
            position: 'top',
        },
        animation: {
          onComplete: function() {
          document.getElementById('coolImage1').setAttribute('value', minkatChartpdf.toBase64Image());
          }
        },
        scales: {
          xAxes: [{ 
              // type: 'linear',
              gridLines: {
                  display: true,
              },
              ticks: {
                callback: function(value, index, values) {
                  return parseFloat(value).toFixed();
                },
                autoSkip: true,
                stepSize: 1,
                fontColor: "#9C020E", // this here
                max:99,
              },
          }], 
          yAxes: [{
            ticks: {
              fontSize : 14,
              fontColor: "#172B4D",
              callback: function(value) {
                if (!(value % 10)) {
                  return  value ;
                }
              }
            }
          }]
        },
      },
      data: {
        labels: ["<?php echo implode('", "',$code_riasec_desc) ?>"],
        datasets: [
        {
          label: 'Aktivitas',
          backgroundColor: "#9C020E",
          borderColor: "#9C020E",
          borderWidth : 2,
          data: [<?php if(!empty($score[0])) echo implode(', ',$score[0]) ?>],
        },
        {
          label: 'Persepsi Kemampuan',
          backgroundColor: "#FFCA2E",
          borderColor: "#FFCA2E",
          borderWidth : 2,
          data: [<?php if(!empty($score[1])) echo implode(', ',$score[1]) ?>],
        },
        {
          label: 'Pekerjaan',
          backgroundColor: "#232941",
          borderColor: "#232941",
          borderWidth : 2,
          data: [<?php if(!empty($score[2])) echo implode(', ',$score[2]) ?>],
        },
        ]
      }
    });
    // Save to jQuery object
    $chart.data('chart', minkatChartpdf);
  };
  if ($chart.length) {
    init($chart);
  }

})();
    <?php
  }
  ?>


</script>

<!-- chart untuk tes kecerdasan-->
<script type="text/javascript">
  <?php
  $desc['N'] = 'Penyelesaian tugas';
  $desc['G'] = 'Bekerja keras';
  $desc['A'] = 'Mencapai prestasi';
  $desc['L'] = 'Peran pemimpin';
  $desc['P'] = 'Mengatur orang lain';
  $desc['I'] = 'Membuat keputusan';
  $desc['T'] = 'Penetapan prioritas';
  $desc['V'] = 'Peran kesibukan';
  $desc['X'] = 'Kebutuhan diperhatikan';
  $desc['S'] = 'Sosialisasi';
  $desc['B'] = 'Bagian dari kelompok';
  $desc['O'] = 'Kelekatan';
  $desc['R'] = 'Teoritis';
  $desc['D'] = 'Pemikiran detail';
  $desc['C'] = 'Keteraturan';
  $desc['Z'] = 'Perubahan dan variasi';
  $desc['E'] = 'Pengontrolan emosi';
  $desc['K'] = 'Agresivitas';
  $desc['F'] = 'Membantu atasan';
  $desc['W'] = 'Mengikuti aturan'; 
  if(isset($jenistes[2])){
    $i = 0;
    foreach ($jenistes[2]['bagiantes'][0]['papikostick'] as $key => $score) {
      if ($score){
        $code[$i] = $key;
        $code_desc[$i] = $desc[$key];
        $papi[$i] = $score;
      }else {
        $code[$i] = $key;
        $code_desc[$i] = $desc[$key];
        $papi[$i] = 0;
      }
      $i++;
    }
    ?>

  var papiChart = (function() {
  // Variables
  var $chart = $('#chart-sw2');
  // Methods
  function init($chart) {
    var papiChart = new Chart($chart, {
      type: 'radar',
      options: {
        tooltips: {enabled: false},
        scales: {
          yAxes: [{
            ticks: {
              callback: function(value) {
                if (!(value % 10)) {
                  return  value ;
                }
              }
            }
          }]
        },
      },
      data: {
        labels: ["<?php echo implode('", "',$code) ?>"],
        datasets: [{
          label: '',
          data: [<?php echo implode(', ',$papi) ?>],
        }]
      }
    });
    // Save to jQuery object
    $chart.data('chart', papiChart);
  };
  if ($chart.length) {
    init($chart);
  }

})();
var papiChartpdf = (function() {
  // Variables
  var $chart = $('#chart-pdf2');
  // Methods
  function init($chart) {
    var papiChartpdf = new Chart($chart, {
      type: 'horizontalBar',
      options: {
        tooltips: {enabled: false},
        animation: {
          onComplete: function() {
          document.getElementById('coolImage2').setAttribute('value', papiChartpdf.toBase64Image());
          }
        },
        scales: {
          xAxes: [{ 
              // type: 'linear',
              gridLines: {
                  display: true,
              },
              ticks: {
                callback: function(value, index, values) {
                  return parseFloat(value).toFixed();
                },
                autoSkip: true,
                stepSize: 1,
                fontColor: "#9C020E", // this here
                max:9,
              },
          }], 
          yAxes: [{
            display: true,
                gridLines: {
                    display: true,
                },
            ticks: {
              fontSize : 14,
              fontColor: "#172B4D",
              callback: function(value) {
                if (!(value % 10)) {
                  return  value ;
                }
              }
            }
          }]
        },
      },
      data: {
        labels: ["<?php echo implode('", "',$code_desc) ?>"],
        datasets: [{
          label: '',
          data: [<?php echo implode(', ',$papi) ?>],
        }]
      }
    });
    // Save to jQuery object
    $chart.data('chart', papiChartpdf);
  };
  if ($chart.length) {
    init($chart);
  }

})();
    <?php
  }
  ?>


</script>

</body>

</html> 