<div class="header bg-primary pb-6">

  <div class="container-fluid">

    <div class="header-body">

      <div class="row align-items-center py-4">

        <div class="col-lg-6 col-7">

          <!-- <h6 class="h2 text-white d-inline-block mb-0">Order</h6> -->

          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">

            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">

              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>

              <li class="breadcrumb-item active" aria-current="page">Ready to summarize</li>

            </ol>

          </nav>

        </div>

        <!-- <div class="col-lg-6 col-5 text-right">

          <a href="#" class="btn btn-sm btn-neutral">New</a>

          <a href="#" class="btn btn-sm btn-neutral">Filters</a>

        </div> -->

      </div>

    </div>

  </div>

</div>

<!-- Page content -->

<div class="container-fluid mt--6">

  <div class="row">

    <div class="col">

      <div class="card">

        <!-- Card header -->

        <div class="card-header border-0">

          <h3 class="mb-0">Tes Peserta</h3>

          <p class="text-sm">Product member yang telah selesai mengerjakan semua jenis tes (status = 'done') dan belum ada psikolog yang mengerjakan summary</p>

        </div>

        <!-- Light table -->

          <div class="table-responsive">

            <table id="dtBasicExample" class="table" width="100%">

              <thead class="thead-light">

                <tr>

                  <!-- <th class="th-sm">ID Order

                  </th> -->

                  <th class="th-sm"><?= lang('Global.test_package') ?>

                  </th>

                  <th class="th-sm"><?= lang('Global.student') ?>

                  </th>

                  <th class="th-sm"><?= lang('Global.jenis_tes') ?>

                  </th>

                  <th class="th-sm"><?= lang('Global.action') ?>

                  </th>

                </tr>

              </thead>

              <tbody class="list">

                <?php 

                if (!empty($orders)) {

                  // dd($orders);

                  foreach ($orders as $key => $order) {

                    foreach ($order['products'] as $key2 => $product) {

                    ?>

                    <tr>

                      <td>

                        <div class="media-body">

                          <a href="<?= base_url('order/id/'.$order['id']) ?>"><?= 'PIPETEST'.sprintf('%04d', $order['id']) ?></a><br>

                          <?php 

                            $total_harga = 0;

                                ?>

                                <span><?= $product['nama'] ?></span>

                                <br>

                                <!-- <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span> -->

                                <?php

                                // dd($product);

                              ?>

                              <!-- <b>Total : Rp. <?= number_format($total_harga, 0, 0, '.'); ?></b> -->

                              <?php

                          ?>

                        </div>

                      </td>

                      <th scope="row">

                        <div class="media align-items-center">

                          <a href="#" class="avatar rounded-circle mr-3">

                            <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">

                          </a>

                          <div class="media-body">

                            <span class="name mb-0 "><?= $order['first_name'].' '.$order['last_name']?></span>

                            <?php 



                              if ($order['not_deleted']==1){

        

                                ?>

        

                                <span class="badge badge-success mr-4">

        

                                  <span class="status">Active</span>

        

                                </span>

        

                                <?php

        

                              }

        

                              elseif ($order['not_deleted']==0){

        

                                  ?>

        

                                  <span class="badge badge-warning mr-4">

        

                                    <span class="status">Deleted</span>

        

                                  </span>

        

                                  <?php

        

                              }

        

                              ?>

                            <br>

                            <a href="https://api.whatsapp.com/send?phone=+628<?= substr($order['phone'], 1) ?>" target="_blank">

                              <span class="name mb-0 "><?= $order['phone'] ?></span>

                            </a>

                            <br>

                            <span class="name mb-0 "><?= $order['email'] ?></span>

                            <br>

                            <span class="name mb-0 "><?= $order['company'] ?></span>

                          </div>

                        </div>

                      </th>

                      <th scope="row">

                        <div class="media align-items-center">

                          <div class="media-body">

                            <?php 

                             foreach ($product['jenistes'] as $key3 => $jenistes) {

                              ?>

                                <span><?= $jenistes['nama'] ?></span>

                                <br>

                                <!-- <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span> -->

                              <?php

                             if($jenistes['status']=='start-test'){

                                ?>

                                  <span class="badge badge-warning mr-4">

                                    <span class="status">On Test</span>

                                  </span>

                                  <br>

                                  <p style="font-size: 12px;"><?= $jenistes['start-test'] ?></p>

                                <?php

                              } else if($jenistes['status']=='done-test' || $jenistes['done-test']!='0000-00-00 00:00:00'){

                                ?>

                                  <span class="badge badge-success mr-4">

                                    <span class="status">Done</span>

                                  </span>

                                  <br>

                                  <p style="font-size: 12px;"><?= $jenistes['done-test'] ?></p>

                                <?php

                              } else {

                                ?>

                                  <span class="badge badge-info mr-4">

                                    <span class="status">Ready</span>

                                  </span>

                                  <br>

                                  <p style="font-size: 12px;"><?= $order['paid-confirm'] ?></p>

                                <?php

                              }

                             }

                            ?>

                          </div>

                          </div>

                        </div>

                      </th>

                      

                      <td class="text-left">

                        <a target="_blank" href="<?= base_url('test/take_summarize/'.$order['id'].'/'.$product['id']) ?>" class="btn btn-sm btn-primary"><?= lang('Global.summarize')  ?></a>
                        <br>
                        <br>
                        <a target="_blank" href="<?= base_url('test/detail_admin/'.$order['id'].'/'.$order['products'][0]['id']) ?>" class="btn btn-sm btn-secondary">Lihat detail jawaban</a>
                        <!-- <a href="<?= base_url('order/id/'.$order['id']) ?>" class="btn btn-sm btn-primary">Lihat jawaban</a> -->

                      </td>

                    </tr>

                    <?php

                  }

                }

                }

                ?>

                <!-- akhir baris -->

              </tbody>

              <thead class="thead-light">

                <tr>

                  <!-- <th class="th-sm">ID Order

                  </th> -->

                  

                  <th class="th-sm"><?= lang('Global.test_package') ?>

                  </th>

                  <th class="th-sm"><?= lang('Global.student') ?>

                  </th>

                  <th class="th-sm"><?= lang('Global.jenis_tes') ?>

                  </th>

                 <!--  <th class="th-sm"><?= lang('Global.correction') ?>

                  </th> -->

                  <!-- <th class="th-sm"><?= lang('Global.summary') ?> -->

                  <!-- </th> -->

                  <th class="th-sm"><?= lang('Global.action') ?>

                  </th>

                </tr>

              </tfoot>

            </table>

          </div>

      </div>

    </div>

  </div>

</div>

