<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">My Correction <br>& Summarization</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="#"><?= lang('Global.test_package') ?></a></li>
              <!-- <li class="breadcrumb-item active" aria-current="page">Daftar tes peserta</li> -->
            </ol>
          </nav>
        </div>
        <!-- <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0"><?= lang('Global.test_package') ?></h3>
          <p class="text-sm">Product member yang telah saya take untuk dilakukan summary</p>
        </div>
        <!-- Custom buttons -->
        <div class="">
          <ul>
            <button class="dt-button buttons-excel buttons-html5 all" tabindex="0" aria-controls="dtBasicExample" type="button"><span>All</span></button>
            <button class="dt-button buttons-excel buttons-html5 ringkas" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Sedang dianalisa</span></button>
            <button class="dt-button buttons-excel buttons-html5 ringkas" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Sudah dianalisa</span></button>
          </ul>
        </div>
        <!-- Light table -->
          <div class="table-responsive">
            <table id="dtBasicExample" class="table" width="100%">
              <thead class="thead-light">
                <tr>
                  <!-- <th class="th-sm">ID Order
                  </th> -->
                  <th class="th-sm"><?= lang('Global.test_package') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.student') ?>
                  </th>
                   <th class="th-sm"><?= lang('Global.test_working') ?>
                  </th>
                  <th>Selesai mengerjakan</th>
                  <!-- <th class="th-sm">Correction
                  </th> -->
                  <th class="th-sm"><?= lang('Global.summary') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.action') ?>
                  </th>
                </tr>
              </thead>
              <tbody class="list">
                <?php 
                if (!empty($orders)) {
                  // dd($orders);
                  foreach ($orders as $key => $order) {
                    foreach ($order['products'] as $key2 => $product) {
                    ?>
                    <tr>
                      <td>
                        <div class="media-body">
                          <a href="<?= base_url('order/id/'.$order['id']) ?>"><?= 'PIPETEST'.sprintf('%04d', $order['id']) ?></a><br>
                          <?php 
                            $total_harga = 0;
                                ?>
                                <span><?= $product['nama'] ?></span>
                                <br>
                                <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span>
                                <?php
                                // dd($product);
                              ?>
                              <!-- <b>Total : Rp. <?= number_format($total_harga, 0, 0, '.'); ?></b> -->
                              <?php
                          ?>
                        </div>
                      </td>
                      <th scope="row">
                        <div class="media align-items-center">
                          <a href="#" class="avatar rounded-circle mr-3">
                            <img src="
                            <?php 
                            if($order['avatar']=="" || @file_get_contents(base_url('public/uploads/avatar/'.$order['avatar']))==FALSE) { 
                                echo base_url('src/assets/img/profile/none.jpg'); 
                                } else { 
                                    echo base_url('public/uploads/avatar/'.$order['avatar']); 
                                } 
                            ?>" class="rounded-circle img-responsive img-circle" />
                          </a>
                          <div class="media-body text-wrap">
                            <span class="name mb-0 "><?= $order['first_name'].' '.$order['last_name']?></span>
                            <?php 

                              if ($order['not_deleted']==1){
        
                                ?>
        
                                <span class="badge badge-success mr-4">
        
                                  <span class="status">Active</span>
        
                                </span>
        
                                <?php
        
                              }
        
                              elseif ($order['not_deleted']==0){
        
                                  ?>
        
                                  <span class="badge badge-warning mr-4">
        
                                    <span class="status">Deleted</span>
        
                                  </span>
        
                                  <?php
        
                              }
        
                              ?>
                            <br>
                            <a href="https://api.whatsapp.com/send?phone=+628<?= substr($order['phone'], 1) ?>" target="_blank">
                            <span class="name mb-0 "><?= $order['phone'] ?></span>
                            </a>
                            <br>
                            <span class="name mb-0 "><?= $order['email'] ?></span>
                            <br>
                            <span class="name mb-0 "><?= $order['company'] ?></span>
                          </div>
                        </div>
                      </th>
                       
                      <!-- correction -->
                      <th scope="row">

                  <div class="media align-items-center">

                    <div class="media-body">

                      <?php 

                      $done_all =1;
                      $none_all =1;
                      $max_done_date = 0;

                       foreach ($product['jenistes'] as $key3 => $jenistes) {
                       if ($max_done_date < $jenistes['done-test']){
                       		$max_done_date = $jenistes['done-test'];
                       }

                        ?>

                          <span><?= $jenistes['nama'] ?></span>

                          <br>

                          <!-- <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span> -->

                        <?php
                        

                       if($jenistes['status']=='start-test'){
                           $none_all =0;
                           $done_all = 0;

                          ?>

                            <span class="badge badge-warning mr-4">

                              <span class="status"><?= lang('Global.on_test') ?></span>

                            </span>

                            <!-- <a href="<?= base_url('order/done/'.$jenistes['id']) ?>" class="btn btn-sm btn-success"><?= lang('Global.assign_as_done') ?></a> -->

                            <br>

                            <p style="font-size: 12px;">
                            <?php
                            if ($jenistes['start-test']!='0000-00-00 00:00:00'){
                              echo date('d-m-Y H:i:s', strtotime($jenistes['start-test'])); 
                            }
                            ?>
                            </p>

                          <?php

                        } else if($jenistes['status']=='done-test' || $jenistes['done-test']!='0000-00-00 00:00:00'){
                          $none_all =0;
                          ?>

                            <span class="badge badge-success mr-4">

                              <span class="status"><?= lang('Global.done') ?></span>

                            </span>

                            <br>

                            <p style="font-size: 12px;">
                            <?php
                            if ($jenistes['done-test']!='0000-00-00 00:00:00'){
                              echo date('d-m-Y H:i:s', strtotime($jenistes['done-test'])); 
                            }
                            ?>
                            </p>

                          <?php

                        } else {
                          

                          $done_all = 0;

                          ?>

                            <span class="badge badge-info mr-4">

                              <span class="status"><?= lang('Global.ready_test') ?></span>

                            </span>

                            <br>

                            <p style="font-size: 12px;">
                            <?php
                            if ($order['paid-confirm']!='0000-00-00 00:00:00'){
                              echo date('d-m-Y H:i:s', strtotime($order['paid-confirm'])); 
                            }
                            ?>
                            </p>

                          <?php

                        }

                       }

                       

                      ?>

                    </div>

                    </div>

                  </div>

                </th>
                <td>
                <?= $max_done_date ?>
                </td>
                      <th scope="row">
                        <div class="media align-items-center">
                          <div class="media-body">
                            <span><?= $product['nama'] ?></span>
                                <br>
                            <?php 
                            $count_jenistes = count($product['jenistes']);
                            $corrected = 0;
                            $ulangi = 0;
                             foreach ($product['jenistes'] as $key3 => $jenistes) {
                                if($jenistes['status']=='corrected') $corrected++;
                                if($jenistes['status']=='ulangi') $ulangi++;
                             }
                             if($product['status']=='' && $count_jenistes==$corrected){
                                ?>
                                  <span class="badge badge-info mr-4">
                                    <span class="status"><?= lang('Global.ready_to_summarize') ?></span>
                                  </span>
                                  <br>
                                  <p style="font-size: 12px;"><?= $product['start-summarization'] ?></p>
                                <?php
                              } else if($product['status']=='start-summarization'){
                                  ?>
                                    <span class="badge badge-warning mr-4 text-wrap">
                                      <span class="status"><?= lang('Global.on_summarization') ?></span>
                                    </span>
                                  <br>
                                  <!-- <a class="text-wrap" href="<?= base_url('psychologist/id/'.$product['psikolog_id']) ?>" class=""><?= lang('Global.psikolog').' '.$product['psikolog_first_name'].' '.$product['psikolog_last_name'] ?></a>
                                  <br> -->
                                  <p style="font-size: 12px;"><?= $product['start-summarization'] ?></p>
                                <?php
                              } else if($product['status']=='summarized'){
                                ?>
                                  <span class="badge badge-success mr-4">
                                    <span class="status"><?= lang('Global.summarized') ?></span>
                                  </span>
                                  <br>
                                 <!--  <a href="<?= base_url('psychologist/id/'.$product['psikolog_id']) ?>" class=""><?= lang('Global.psikolog').' '.$product['psikolog_first_name'].' '.$product['psikolog_last_name'] ?></a>
                                  <br> -->
                                  <p style="font-size: 12px;"><?= $product['summarized'] ?></p>
                                  <a target="_blank" href="<?php echo base_url('pdf/mpdf/'.$order['id'].'/'.$product['id'].'/I');?>" class="btn btn-sm btn-primary">Lihat hasil</a>

                                <?php
                              } else {
                                ?>
                                  <span class="badge badge-secondary mr-4">
                                    <span class="status"></span>
                                  </span>
                                  <br>
                                  <p style="font-size: 12px;">Undefined</p>
                                <?php
                              }
                            ?>
                          </div>
                          </div>
                        </div>
                      </th>
                      <td class="text-left">
                      <?php
                      if($ulangi!=0){
                        ?>
                        <a  class="btn btn-sm btn-light" disabled><?= lang('Global.summary') ?></a>
                        <p class="text-sm">Tes sedang diulang</p>
                        <?php
                      } else {
                          if($product['status']=='summarized'){
                        ?>
                          <a target="_blank" disabled href="<?= base_url('test/cancel_summarization/'.$order['id'].'/'.$product['id']) ?>" class="btn btn-sm btn-primary">Re-analisa</a>
                          <br>
                          <br>
                          <a target="_blank" href="<?= base_url('test/correction/null/'.$order['id'].'/'.$product['id']) ?>" class="btn btn-sm btn-secondary">Lihat <?= lang('Global.summary') ?></a>
                          <?php } else { ?>
                          <a target="_blank" href="<?= base_url('test/correction/null/'.$order['id'].'/'.$product['id']) ?>" class="btn btn-sm btn-primary"><?= lang('Global.summary') ?></a>
                          
                        <?php
                          }
                      }
                      ?>
                      </td>
                    </tr>
                    <?php
                  }
                }
                }
                ?>
                <!-- awal baris -->
                
                <!-- akhir baris -->
              </tbody>
              <thead class="thead-light">
                <tr>
                  <th class="th-sm"><?= lang('Global.test_package') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.student') ?>
                  </th>
                   <th class="th-sm"><?= lang('Global.test_working') ?>
                  </th>
                  <th>Selesai mengerjakan</th>
                  <!-- <th class="th-sm">Correction
                  </th> -->
                  <th class="th-sm"><?= lang('Global.summary') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.action') ?>
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        <!-- Card footer -->
        <!-- <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end mb-0">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <i class="fas fa-angle-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">
                  <i class="fas fa-angle-right"></i>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>
