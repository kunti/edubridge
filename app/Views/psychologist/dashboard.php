

<div class="header bg-primary pb-6">

      <div class="container-fluid">

        <div class="header-body">

          <div class="row align-items-center py-4">

            <div class="col-lg-6 col-7">

              <h6 class="h2 text-white d-inline-block mb-0">Dashboard</h6>

              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">

                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">

                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>

                  <!-- <li class="breadcrumb-item"><a href="#">Dashboards</a></li> -->

                  <li class="breadcrumb-item active" aria-current="page">Dashboard</li>

                </ol>

              </nav>

            </div>

           <!--  <div class="col-lg-6 col-5 text-right">

              <a href="#" class="btn btn-sm btn-neutral">New</a>

              <a href="#" class="btn btn-sm btn-neutral">Filters</a>

            </div> -->

          </div>

          <!-- Card stats -->

          <div class="row" >

            <div class="col-xl-3 col-md-6">

              <div class="card card-stats">

                <!-- Card body -->

                <a href="<?= base_url('test/paket_tes') ?>">

                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="">

                  <div class="row">

                    <div class="col">

                      <h5 class="card-title text-uppercase text-muted mb-0"><?= lang('Global.ready_summarize') ?></h5>

                      <span class="h2 font-weight-bold mb-0"><?= $siap_diringkas ?></span>

                    </div>

                  </div>

                </a>

                  <!-- <p class="mt-3 mb-0 text-sm">

                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> </span>

                    <span class="text-nowrap">Since last month</span> -->

                  <!-- </p> -->

                </div>

              </div>

            </div>

            <div class="col-xl-3 col-md-6">

              <div class="card card-stats">

                <!-- Card body -->

                <a href="<?= base_url('test/correction') ?>">

                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="">

                  <div class="row">

                    <div class="col">

                      <h5 class="card-title text-uppercase text-muted mb-0"><?= lang('Global.my_summarization') ?></h5>

                      <span class="h2 font-weight-bold mb-0"><?= $sedang_dirangkum+$selesai ?></span>

                    </div>

                    <!-- <div class="col-auto">

                      <div class="icon icon-shape bg-gradient-indigo text-white rounded-circle shadow">

                        <i class="ni ni-cart"></i>

                      </div>

                    </div> -->

                  </div>

                  <!-- <p class="mt-3 mb-0 text-sm">

                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>

                    <span class="text-nowrap">Since last month</span>

                  </p> -->

                </div>

              </a>

              </div>

            </div>

            <hr>

            <div class="col-xl-3 col-md-6">

              <div class="card card-stats">

                <!-- Card body -->

                <a href="<?= base_url('test/correction/start-summarization') ?>">

                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="">

                  <div class="row">

                    <div class="col">

                      <h5 class="card-title text-uppercase text-muted mb-0">

                        <span class="badge badge-warning mr-6">

                          <span class="status"><?= lang('Global.on_summarization') ?></span>

                        </span>

                      <span class="h2 font-weight-bold mb-0"><?= $sedang_dirangkum ?></span>

                    </div>

                   <!--  <div class="col-auto">

                      <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">

                        <i class="ni ni-money-coins"></i>

                      </div>

                    </div> -->

                  </div>

                 <!--  <p class="mt-3 mb-0 text-sm">

                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>

                    <span class="text-nowrap">Since last month</span>

                  </p> -->

                </div>

              </a>

              </div>

            </div>

            <div class="col-xl-3 col-md-6">

              <div class="card card-stats">

                <!-- Card body -->

                <a href="<?= base_url('test/correction/summarized') ?>">

                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="">

                  <div class="row">

                    <div class="col">

                      <h5 class="card-title text-uppercase text-muted mb-0">

                        <span class="badge badge-success mr-6">

                          <span class="status"><?= lang('Global.done') ?></span>

                        </span>

                      </h5>

                      <span class="h2 font-weight-bold mb-0"><?= $selesai ?></span>

                    </div>

                   <!--  <div class="col-auto">

                      <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">

                        <i class="ni ni-check-bold"></i>

                      </div>

                    </div> -->

                  </div>

                  <!-- <p class="mt-3 mb-0 text-sm">

                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>

                    <span class="text-nowrap">Since last month</span>

                  </p> -->

                </div>

              </a>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6" hidden="">

      <div class="row">

        <div class="col-xl-8" >

          <div class="card bg-defalut">

            <div class="card-header bg-transparent">

              <div class="row align-items-center">

                <div class="col" data-toggle="tooltip" data-placement="bottom" title="Data checkout peserta yang telah terkonfirmasi pembayarannya">

                  <h6 class="text-uppercase text-muted ls-1 mb-1">Sales Overview</h6>

                  <h5 class="h3 text mb-0">Product Sales Statistic</h5>

                  <br>

                </div>

              </div>

              <div class="row align-items-center">

                <div class="col">

                  <ul class="nav nav-pills justify-content-end">

                    <li class="nav-item mr-2 mr-md-0" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 10, 30, 15, 40, 20, 60, 60]}]}}' data-prefix="Rp " data-suffix="k">

                      <a href="#" class="nav-link py-2 px-3 active" data-toggle="tab">

                        <span class="d-none d-md-block">All</span>

                      </a>

                    </li>

                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="Rp " data-suffix="k">

                      <a href="#" class="nav-link py-2 px-3" data-toggle="tab">

                        <span class="d-none d-md-block">Paket Lengkap</span>

                      </a>

                    </li>

                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="Rp " data-suffix="k">

                      <a href="#" class="nav-link py-2 px-3" data-toggle="tab">

                        <span class="d-none d-md-block">Tes Kecerdasan</span>

                      </a>

                    </li>

                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="Rp " data-suffix="k">

                      <a href="#" class="nav-link py-2 px-3" data-toggle="tab">

                        <span class="d-none d-md-block">Tes Minat Bakat</span>

                      </a>

                    </li>

                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="Rp " data-suffix="k">

                      <a href="#" class="nav-link py-2 px-3" data-toggle="tab">

                        <span class="d-none d-md-block">Tes Kepribadian</span>

                      </a>

                    </li>

                  </ul>

                </div>

              </div>

            </div>

            <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta">

              <!-- Chart -->

              <div class="chart">

                <!-- Chart wrapper -->

                <canvas id="chart-sales-dark" class="chart-canvas"></canvas>

              </div>

            </div>

          </div>

        </div>

        <div class="col-xl-4">

          <div class="card">

            <div class="card-header bg-transparent">

              <div class="row align-items-center">

                <div class="col">

                  <h6 class="text-uppercase text-muted ls-1 mb-1">Sales Overview</h6>

                  <h5 class="h3 mb-0">Product Sales Summary</h5>

                </div>

              </div>

            </div>

            <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta">

              <!-- Chart -->

              <div class="chart">

                <canvas id="chart-bars" class="chart-canvas"></canvas>

              </div>

            </div>

          </div>

        </div>

        <div class="col-xl-6">

          <div class="card">

            <div class="card-header bg-transparent">

              <div class="row align-items-center">

                <div class="col">

                  <h6 class="text-uppercase text-muted ls-1 mb-1">Sales Overview</h6>

                  <h5 class="h3 mb-0">Payment Summary</h5>

                </div>

              </div>

            </div>

            <div class="card-body">

              <!-- Chart -->

              <div class="chart">

              <canvas id="paymentChart" width="100" height="100"></canvas>

              </div>

            </div>

          </div>

        </div>

        <div class="col-xl-12" >

          <div class="card">

            <div class="card-header bg-transparent">

              <div class="row align-items-center">

                <div class="col">

                  <h6 class="text-uppercase text-muted ls-1 mb-1">Sales Overview</h6>

                  <h5 class="h3 mb-0">Payment Confirmation Summary</h5>

                </div>

              </div>

            </div>

            <div class="card-body">

              <!-- Chart -->

              <div class="chart">

              <canvas id="confirmationChart" width="100" height="100"></canvas>

              </div>

            </div>

          </div>

        </div>

      </div>