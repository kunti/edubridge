<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Online tes</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="#">Online tes</a></li>
              <!-- <li class="breadcrumb-item active" aria-current="page">Daftar tes peserta</li> -->
            </ol>
          </nav>
        </div>
        <!-- <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0">Tes yang sedang berjalan</h3>
          <!-- <p class="text-sm">Product member yang telah saya take untuk dilakukan summary</p> -->
        </div>
        <!-- Light table -->
          <div class="table-responsive">
          <table id="dtBasicExample" class="table" width="100%">

<thead class="thead-light">

  <tr>
    <th class="th-sm">Kode order</th>
    <th class="th-sm">Paket tes</th>
    <th class="th-sm" hidden>Harga</th>
    <th class="th-sm" hidden>Ekspirasi</th>
    <th class="th-sm">Photo</th>
    <th class="th-sm">Nama peserta</th>
    <th class="th-sm">Email</th>
    <th class="th-sm">Sekolah/Universitas</th>
    <th class="th-sm">No HP</th>
    <th class="th-sm">Mulai tes</th>
    <th class="th-sm">Jenis tes</th>
    <th class="th-sm">Bagian</th>
    <th class="th-sm">Waktu tersisa</th>
    <th class="th-sm">Updated at</th>
  </tr>

</thead>


<tbody class="list">

  <?php 

  if (!empty($online)) {

    // dd($online);

    foreach ($online as $key => $order) {
        ?>
        <tr>
        <td><a href="<?= base_url('order/id/'.$order['id']) ?>"><?= 'PIPETEST'.sprintf('%04d', $order['id']); ?></a></td>
        <td><span><?= $order['nama_product'] ?></span></td>
        <td hidden>
          <sm style="color: ">

            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16">

            <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/>

            <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/>

            <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>

          </svg>

          <?php 

          $stop_date = new DateTime($order['paid-confirm']);

          $stop_date->modify('+8 day');

          $stop_date->setTime(0,0,0);

          $today = new DateTime("now");

          $birthDate = $stop_date;

          if ($birthDate < $today) { 

            $expired = 1;

            echo lang('Global.hasexpired')."</sm>";

          } else {

            $y = $today->diff($birthDate)->y;

            $m = $today->diff($birthDate)->m;

            $d = $today->diff($birthDate)->d;

            $h = $today->diff($birthDate)->h;

            $i = $today->diff($birthDate)->i;

            $expired = 0;

            

            echo lang('Global.expired')."<br> ".$d." ".lang('Global.day')." ".$h."  ".lang('Global.hour')." ".$i." ".lang('Global.minute')."</sm>"; 

          }

          ?>


        </td>
        <td>
        <a href="#" class="avatar rounded-circle mr-3">

            <img style="width:50px" src="

            <?php 

            if($order['avatar']=="" || @file_get_contents(base_url('public/uploads/avatar/'.$order['avatar']))==FALSE) { 

                echo base_url('src/assets/img/profile/none.jpg'); 

                } else { 

                    echo base_url('public/uploads/avatar/'.$order['avatar']); 

                } 

            ?>" class="rounded-circle img-responsive img-circle" />

            </a>
        </td>
        <td>
        <a href="<?= base_url('account/profile/').'/'.$order['id'] ?>">
        <span class="name mb-0 "><?= $order['first_name'].' '.$order['last_name']?></span>
        </a>
        </td>
        <td><span class="name mb-0 "><?= $order['email'] ?></span></td>
        <td><span class="name mb-0 "><?= $order['company'] ?></span></td>
        <td>
          <a href="https://api.whatsapp.com/send?phone=+62<?= substr($order['phone'], 1) ?>" target="_blank">
        <span class="name mb-0">0<?= $order['phone'] ?></span>
        </a>
        </td>
        
        <td>
          <?= date('Y-m-d H:i:s', strtotime($order['start-test'])); ?>
        </td>
        <td><span><?= $order['nama'] ?></span></td>
        <td><span><?= $order['bagian_checkpoint'] ?></span></td>
        <td><span><?php 
        $s = $order['timer_checkpoint']%60;
        $m = floor(($order['timer_checkpoint']%3600)/60);
        echo $m.' menit '.$s.' detik'  ?></span></td>
        <td><span><?= $order['updated_at'] ?></span></td>
        <th scope="row">

          <div class="media align-items-center">

            <div class="media-body">

             
            </div>

            </div>

          </div>

        </th>
       

      

       


     
      </tr>

      <?php


  }

  }

  ?>

  <!-- awal baris -->

  

  <!-- akhir baris -->

</tbody>

<tfoot class="thead-light">

<tr>
    <th class="th-sm">Kode order</th>
    <th class="th-sm">Paket tes</th>
    <th class="th-sm" hidden>Harga</th>
    <th class="th-sm" hidden>Ekspirasi</th>
    <th class="th-sm">Photo</th>
    <th class="th-sm">Nama peserta</th>
    <th class="th-sm">Email</th>
    <th class="th-sm">Sekolah/Universitas</th>
    <th class="th-sm">No HP</th>
    <th class="th-sm">Mulai tes</th>
    <th class="th-sm">Jenis tes</th>
    <th class="th-sm">Bagian</th>
    <th class="th-sm">Waktu tersisa</th>
    <th class="th-sm">Updated at</th>
  </tr>

</tfoot>

</table>
          </div>
        <!-- Card footer -->
        <!-- <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end mb-0">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <i class="fas fa-angle-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">
                  <i class="fas fa-angle-right"></i>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>
