    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.my_profile') ?></li>
                </ol>
              </nav>
            </div>
            <!-- <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-12 order-xl-2">
        <form role="form" action="<?= base_url('account/save') ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
          <input type="hidden" name="id_user" id="id_user" value="<?= $myprofile['id'] ?>">
          <input type="hidden" name="redirect" id="redirect" value="yes">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-7">
                  <h3 class="mb-0"><?= lang('Global.my_profile') ?></h3>
                </div>
                <div class="col-5 text-right">
                  
                    <input type="submit" name="submit" value="Save changes" class="btn btn-xl btn-success">
                  
                </div>
              </div>
            </div>
            <div class="card-body">
                <h6 class="heading-small text-muted mb-4"><?= lang('Global.identity') ?></h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username"><?= lang('Global.first_name') ?></label>
                        <input type="text" id="first_name" name="first_name" class="form-control" placeholder="Nama depan Anda" value="<?= $myprofile['first_name']; ?>" required>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username"><?= lang('Global.last_name') ?></label>
                        <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Nama belakang Anda" value="<?= $myprofile['last_name']; ?>" required>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">No. SIPP</label>
                    <input class="form-control" placeholder="" type="text" id="sipp" name="sipp" value="<?= $myprofile['sipp'] ?>">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Email</label>
                        <input type="email" id="email" name="email" class="form-control" placeholder="Email Anda" value="<?= $myprofile['email']; ?>" disabled>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="">Password</label>
                        <input type="email" id="" name="" class="form-control" placeholder="********" value="" disabled>
                        <a class="text-sm" href="<?= base_url('auth/change_password') ?>">Reset password</a>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <div class="form-group">
                        <label class="form-control-label" for="input-sex"><?=lang('Global.sex');?></label>p
                          <!-- <label for="inputState" style="font-size: 14px; color: #8898aa"></label> -->
                          <select id="sex" name="sex" class="form-control">
                            <option><?=lang('Global.choose');?>...</option>
                            <option <?php if($myprofile['sex']=='male') echo 'selected'; ?>>Male</option>
                            <option <?php if($myprofile['sex']=='female') echo 'selected'; ?>>Female</option>
                          </select>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-first-name"><?= lang('Global.age') ?></label>
                        <?php 
                        $birthDate = new DateTime($myprofile['birthplace']);
                        $today = new DateTime("today");
                        if ($birthDate > $today) { 
                            // exit("0 tahun 0 bulan 0 hari");
                        }
                        $y = $today->diff($birthDate)->y;
                        $m = $today->diff($birthDate)->m;
                        $d = $today->diff($birthDate)->d;
                        ?>
                        <input type="text" id="birthplace" name="birthplace" class="form-control" placeholder="Usia" value="<?= $y." tahun ".$m." bulan ".$d." hari" ?>" disabled>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="example-datetime-local-input" class="form-control-label"><?= lang('Global.birth_date') ?></label>
                        <input class="form-control" type="date" value="<?= $myprofile['birthdate']; ?>" id="birthdate" name="birthdate">
                      </div>
                    </div>          
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label for="example-datetime-local-input" class="form-control-label"><?= lang('Global.ttd') ?></label>
                        <div class="media align-items-center">
                          <a target="_blank" href="<?= base_url('public/uploads/payment/'.$myprofile['ttd']) ?>" class="mr-3" >
                            <img alt="Image placeholder" src="<?php echo base_url('public/uploads/ttd/'.$myprofile['ttd']); ?>" style="height: 100px">
                          </a>
                        </div>
                        <?php 
                        $validation = \Config\Services::validation(); ?>
                           <?= csrf_field(); ?>
                           <div class="form-group">
                              <input type="file" class="form-control" id="file" name="file" />
                              <!-- Error -->
                              <?php if( $validation->getError('file') ) {?>
                                 <div class='alert alert-danger mt-2'>
                                    <?= $validation->getError('file'); ?>
                                 </div>
                              <?php }?>

                           <input type="submit" class="btn btn-sm btn-primary" name="submit" value="Upload" style="margin-top: 7px">
                           </div>
                        </form>
                      </div>
                    </div>                    
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-muted mb-4"><?= lang('Global.education') ?></h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <div class="form-group">
                          <label for="inputState" style="font-size: 14px; color: #8898aa"><?=lang('Global.education_now');?></label>
                          <select id="education_level" name="education_level" class="form-control">
                            <option><?=lang('Global.choose');?>...</option>
                            <option <?php if($myprofile['education_level']=='SMP') echo 'selected'; ?>>SMP</option>
                            <option <?php if($myprofile['education_level']=='SMA') echo 'selected'; ?>>SMA</option>
                            <option <?php if($myprofile['education_level']=='Universitas') echo 'selected'; ?>>Universitas</option>
                            <option <?php if($myprofile['education_level']=='Lainnya') echo 'selected'; ?>>Lainnya</option>
                          </select>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email"><?=lang('Global.semester');?></label>
                    <input class="form-control" placeholder="<?=lang('Global.semester');?>" type="text" id="semester" name="semester" value="<?= $myprofile['semester'] ?>">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email"><?=lang('Global.company');?></label>
                    <input class="form-control" placeholder="<?=lang('Global.company');?>" type="text" id="company" name="company" value="<?= $myprofile['company']?>">
                      </div>
                    </div>
                  </div>
                  <!-- <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-city"><?= lang('Global.kota') ?></label>
                        <select class="form-control" id="input-city">
                          <option>Yogyakarta</option>
                          <option>Palembang</option>
                          <option>Surabaya</option>
                          <option>Semarang</option>
                          <option>Jakarta</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-kab"><?= lang('Global.kota_kab') ?></label>
                        <input type="text" id="input-kab" class="form-control" placeholder="Kabupaten" value="Sleman">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country"><?= lang('Global.kodepos') ?></label>
                        <input type="number" id="input-postal-code" class="form-control" placeholder="Postal code">
                      </div>
                    </div>
                  </div> -->
                </div>
                <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-muted mb-4"><?= lang('Global.contact_address') ?></h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-nohp"><?= lang('Global.phone') ?></label>
                        <div class="input-group">
                            <input type="text" id="phone" name="phone" class="form-control" placeholder="Nomor Telepon / WA" aria-label="nohp" aria-describedby="basic-addon1" value="<?= $myprofile['phone']; ?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address"><?= lang('Global.address') ?></label>
                        <textarea rows="4" class="form-control" id="address" name="address" placeholder="Alamat lengkap" required><?= $myprofile['address']; ?></textarea>
                      </div>
                    </div>
                  </div>
                  <!-- <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-city"><?= lang('Global.kota') ?></label>
                        <select class="form-control" id="input-city">
                          <option>Yogyakarta</option>
                          <option>Palembang</option>
                          <option>Surabaya</option>
                          <option>Semarang</option>
                          <option>Jakarta</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-kab"><?= lang('Global.kota_kab') ?></label>
                        <input type="text" id="input-kab" class="form-control" placeholder="Kabupaten" value="Sleman">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country"><?= lang('Global.kodepos') ?></label>
                        <input type="number" id="input-postal-code" class="form-control" placeholder="Postal code">
                      </div>
                    </div>
                  </div> -->
                </div>
                <!-- <hr class="my-4" /> -->
                <!-- Description -->
               <!--  <h6 class="heading-small text-muted mb-4"><?= lang('Global.feedback') ?></h6>
                <div class="pl-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Deskripsi</label>
                    <textarea rows="4" id="description" name="description" class="form-control" placeholder="A few words about you ..."><?= $myprofile['description']; ?></textarea>
                  </div>
                </div> -->
              </form>
            </div>
          </div>
        </div>
      </div>