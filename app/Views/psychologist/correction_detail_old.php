<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">My Correction <br>& Summarization</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="#">Product List</a></li>
              <li class="breadcrumb-item active" aria-current="page">Product Detail</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0">Detail Paket Tes</h3>
        </div>
        <div class="card-header border-0">
          <h3><?= $orders[0]['products'][0]['nama'] ?></h3>
          <span class="text-sm"><?= $orders[0]['products'][0]['desc'] ?></span>
          <br>
          <sm><a href="">Order code : <?= 'PIPETEST'.sprintf('%04d', $orders[0]['id']) ?></a></sm>
          <br>
          Status : 
          <?php 
            if($orders[0]['products'][0]['start-summarization']=='0000-00-00 00:00:00'){
              echo "Belum ada summary";
            } else if ($orders[0]['products'][0]['status']=='start-summarization'){
              echo "Sedang summary, menunggu hasil";
            } else if ($orders[0]['products'][0]['status']=='summarized'){
              ?>
              <a href="<?php echo base_url('test/correction/null/'.$orders[0]['products'][0]['id']);?>" class="btn btn-sm btn-success">Download hasil</a>
              <?php
            } else {

            }
          ?>
        </div>
        <div class="container">
        <div class="row">
          <div class="col-sm" style="padding: 0px">
            <div class="card" style="width: 100%;">
              <div class="card-body">
                <h5 class="card-title">Tes Kecerdasan</h5>
                <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary">Card link</a>
                <!-- <a href="#" class="card-link">Correction</a> -->
              </div>
            </div>
          </div>
          <div class="col-sm"  style="padding: 0px">
            <div class="card" style="width: 100%;">
              <div class="card-body">
               <div id="summernote">Hello Summernote</div>
              </div>
            </div>
          </div>
        </div>
        </div>
        <!-- Light table -->
          <div class="table-responsive">
            <table id="" class="table" width="100%" >
              <thead class="thead-light">
                <tr>
                  <th class="th-sm">Tes
                  </th>
                  <th class="th-sm">Progress
                  </th>
                  <th class="th-sm">Correction
                  </th>
                  <th class="th-sm">Action
                  </th>
                </tr>
              </thead>
              <tbody class="list">
                <?php 
                if (!empty($orders[0]['products'])) {
                  foreach ($orders[0]['products'] as $key => $product) {
                    if(!empty($product)){
                      foreach ($product['jenistes'] as $key => $jenistes) {
                    ?>
                    <tr>
                      <td>
                        <h5><?= $jenistes['nama'] ?></h5>
                        <br>
                        <span class="text-wrap"><?= $jenistes['desc'] ?></span>
                      </td>
                      <th scope="row">
                        <div class="media align-items-center">
                          <div class="media-body">
                            <?php 
                              if($jenistes['start-test']=='0000-00-00 00:00:00'){
                                ?>
                                  <span class="badge badge-primary mr-4">
                                    <span class="status">Ready</span>
                                  </span>
                                  <br>
                                  <p style="font-size: 12px;"><?= $orders[0]['paid-confirm'] ?></p>
                                <?php
                              } else if($jenistes['status']=='start-test'){
                                ?>
                                  <span class="badge badge-warning mr-4">
                                    <span class="status">On test</span>
                                  </span>
                                  <br>
                                  <a href="#!" class="">Psikolog Xena</a>
                                  <p style="font-size: 12px;"><?= $jenistes['start-test'] ?></p>
                                <?php
                              } else if($jenistes['done-test']!='0000-00-00 00:00:00' || $jenistes['status']=='done-test'){
                                ?>
                                  <span class="badge badge-success mr-4">
                                    <span class="status">Done</span>
                                  </span>
                                  <br>
                                  <p style="font-size: 12px;"><?= $jenistes['done-test'] ?></p>
                                <?php
                              } 
                            ?>
                          </div>
                          </div>
                        </div>
                      </th>
                      <th scope="row">
                        <div class="media align-items-center">
                          <div class="media-body">
                            <?php 
                             if($jenistes['status']=='start-correction'){
                                ?>
                                  <span class="badge badge-warning mr-4">
                                    <span class="status">On Correction</span>
                                  </span>
                                  <br>
                                  <p style="font-size: 12px;"><?= $jenistes['start-correction'] ?></p>
                                <?php
                              } else if($jenistes['status']=='corrected'){
                                ?>
                                  <span class="badge badge-success mr-4">
                                    <span class="status">Done</span>
                                  </span>
                                  <br>
                                  <a href="#!" class="">Psikolog Xena</a>
                                  <p style="font-size: 12px;"><?= $jenistes['corrected'] ?></p>
                                <?php
                              } else {

                              }
                            ?>
                          </div>
                          </div>
                        </div>
                      </th>
                      <td class="text-left">
                        <?php
                          if($jenistes['status']=='done-test' || $jenistes['status']=='start-correction'){
                            ?>
                            <a href="<?php echo base_url('test/correcting/'.$orders[0]['id'].'/'.$product['id'].'/'.$jenistes['id']);?>" class="btn btn-sm btn-primary">Lihat koreksi</a>
                            <?php
                          } else if($jenistes['status']=='corrected'){
                            ?>
                            <?php
                          } 
                        ?>
                      </td>
                    </tr>
                    <?php
                      }
                    }
                  }
                }
                ?>
              </tbody>
            </table>
          </div>
        <!-- Card footer -->
        <!-- <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end mb-0">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <i class="fas fa-angle-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">
                  <i class="fas fa-angle-right"></i>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>

