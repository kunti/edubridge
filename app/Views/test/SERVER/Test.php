<?php 
namespace App\Controllers;
 
use App\Models\Product_model;
use App\Models\Order_model;
use App\Models\Memberorderproduct_model;
use App\Models\Memberorderproductjenistes_model;
use App\Models\Productjenistes_model;
use App\Models\Jenistes_model;
use App\Models\Bagiantes_model;
use App\Models\Soaltes_model;
use App\Models\Pilihanjawaban_model;
use App\Models\Jawabanmember_model;
use App\Models\User_model;
use App\Models\Iqcalc_model;
use App\Models\Feedback_model;
use App\Models\Setting_model;


use DateTime;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/PHPMailer/PHPMailer/src/Exception.php';
require 'vendor/PHPMailer/PHPMailer/src/PHPMailer.php';
require 'vendor/PHPMailer/PHPMailer/src/SMTP.php';

 
class Test extends BaseController
{
 
    public function __construct() {

        $message = 'public message';

        $this->product = new Product_model;
        $this->order = new Order_model;
        $this->memberorderproduct = new memberorderproduct_model;
        $this->memberorderproductjenistes = new memberorderproductjenistes_model;
        $this->productjenistes = new Productjenistes_model;
        $this->jenistes = new Jenistes_model;
        $this->bagiantes = new Bagiantes_model;
        $this->soaltes = new Soaltes_model;
        $this->pilihanjawaban = new Pilihanjawaban_model;
        $this->jawabanmember = new Jawabanmember_model;
        $this->user = new User_model;
        $this->iqcalc = new Iqcalc_model;
        $this->feedback = new feedback_model;
        $this->settings = new setting_model;

        helper('form');
 
    }
 
    public function index()
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) {
            $this->data['carts'] = $this->cart->asArray()
            ->where('id_member', $_SESSION['user_id'])
            ->where('deleted', 0)
            ->findColumn('id_product');

            $this->data['items']=array();
            $this->data['total'] = 0;
            // jika ada cart 
            if($this->data['carts']!=NULL){
                $this->data['items'] = $this->product->asArray()
                ->whereIn('id', $this->data['carts'])
                ->findAll();
                $this->data['total'] = 0;
                foreach($this->data['items'] as $item){
                    $this->data['total'] += $item['harga'] ;
                }
                $this->product->asArray()
                ->whereIn('id', $this->data['carts'])
                ->findAll();
            } 
        } 
        // jika member tidak login maka cek cart di session
        else {
            $this->data['items'] = $this->wfcart->totals();
            $this->data['total'] = $this->wfcart->count_totals();
        }

        $this->data['scope'] = __FUNCTION__;
        $this->data['template'] = 'main/cart';
        return view('index', $this->data);
    }

    public function paket_tes($deleted=NULL)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();

        $this->data['Pakettes_expiration']= $this->settings->asArray()
                ->select('value')
                ->whereIn('id', ['6'])
                ->findAll()[0]['value'];
        $this->data['Pakettes_expiration_belumdikerjakan']= $this->settings->asArray()
                ->select('value')
                ->whereIn('id', ['7'])
                ->findAll()[0]['value'];
        // dd($this->data['Pakettes_expiration_belumdikerjakan']);

        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('admin')) {
            // data order
            $del=1;
            if($deleted!=NULL){
                $del=0;
            }
            $this->data['orders'] = $this->order->asArray()
                ->select('order.*')
                ->whereIn('order.status', ['paid-confirm'])
                ->where('order.deleted', 0)
                // ->where('order.paid-confirm >=', 'DATE_SUB(NOW(), INTERVAL 1 DAY)')
                ->where('users.not_deleted', $del)
                ->join('users', 'users.id=order.id_member', 'left')
                ->orderBy('order.id', 'DESC')
                ->findAll();

                if($deleted!=NULL){
                    $this->data['scope'] = 'test_deleted';
                }
                
                foreach($this->data['orders'] as $key => $order){
                    $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                    ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
                    ->where('id_order', $order['id'])
                    ->where('deleted', 0)
                    ->join('product', 'product.id=memberorderproduct.id_product')
                    ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                    ->orderBy('id', 'DESC')
                ->findAll();

                $this->data['orders'][$key]['member'] = $this->user->asArray()
                ->where('id', $order['id_member'])
                // ->where('active', 1)
                ->findAll();

                if (empty($this->data['orders'][$key]['member'])){
                    d('Order tanpa member');
                    dd($this->data['orders'][$key]);
                }

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {

                        $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                        ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                        ->where('id_memberorderproduct', $product['id'])
                        ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                        ->findAll();
                        $belum_dikerjakan = 1;
                        foreach ($this->data['orders'][$key]['products'][$key2]['jenistes'] as $key3 => $jenistes) {
                            if ($jenistes['status'] == NULL){

                            } else {
                                $belum_dikerjakan = 0;
                            } 
                        }
                        // dd($belum_dikerjakan==1 && strtotime($this->data['orders'][$key]['paid-confirm']) > strtotime('-24 hours') );
                        // belum dikerjakan dan lebih dari 24 jam
                        if ($belum_dikerjakan==1 && strtotime($this->data['orders'][$key]['paid-confirm']) < strtotime('-'.$this->data['Pakettes_expiration_belumdikerjakan'].' hours') ){
                            // unset($this->data['orders'][$key]['products']);
                        }
                }
            }
            $this->data['scope'] = 'test';
            if($deleted!=NULL){
                $this->data['scope'] = 'test_deleted';
            }
            $this->data['template'] = 'admin/'.__FUNCTION__;
            return view('admin/index', $this->data);
        }
        else if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('psychologists')) {
            // ambil data order yang jenistesnya status done semua ya
            // data order
            $this->data['orders'] = $this->order->asArray()
                ->select('order.*')
                ->whereIn('order.status', ['paid-confirm'])
                ->where('order.deleted', 0)
                ->where('memberorderproductjenistes.status', 'done-test')
                ->join('memberorderproduct', 'order.id=memberorderproduct.id_order', 'left')
                ->join('memberorderproductjenistes', 'memberorderproduct.id=memberorderproductjenistes.id_memberorderproduct')
                ->orderBy('order.id', 'DESC')
                ->groupBy('order.id')
                ->findAll();
                
                
                foreach($this->data['orders'] as $key => $order){
                    $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                    ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
                    ->where('id_order', $order['id'])
                    // ->where('id_psikolog', $this->data['profile']->user_id)
                    // ->whereIn('status', [null, ''])
                    ->where('deleted', 0)
                    ->join('product', 'product.id=memberorderproduct.id_product')
                    ->findAll();
                    
                    
                    $this->data['orders'][$key]['member'] = $this->user->asArray()
                    ->where('id', $order['id_member'])
                    //->where('active', 1)
                    ->findAll();
                    
                    foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                        // database error
                        if($product['start-summarization']!='0000-00-00 00:00:00'){
                            unset($this->data['orders'][$key]);
                            // d('unset karna sudah di summarize'.$order['id']);
                        } else {
                            $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                            ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                            // ->where('id_psikolog', $this->data['profile']->user_id)
                            ->where('id_memberorderproduct', $product['id'])
                            // ->whereIn('status', ['done-test'])
                            ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                            ->findAll();
                            
                            foreach ($this->data['orders'][$key]['products'][$key2]['jenistes'] as $key3 => $jenistes) {
                                if ($jenistes['status']!='done-test'){
                                    unset($this->data['orders'][$key]);
                                    // d('unset tes nya belum selesai'.$order['id']);
                                } 
                            }
                            
                        }
                        
                        
                    }
                    // dd($this->data['orders']);
            }
            // dd($this->data['orders']);
            $this->data['scope'] = 'test';
            $this->data['template'] = 'psychologist/test';
            return view('psychologist/index', $this->data);
        }


        
    }

    public function req_activation()
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();

        $this->data['Pakettes_expiration']= $this->settings->asArray()
                ->select('value')
                ->whereIn('id', ['6'])
                ->findAll()[0]['value'];
        $this->data['Pakettes_expiration_belumdikerjakan']= $this->settings->asArray()
                ->select('value')
                ->whereIn('id', ['7'])
                ->findAll()[0]['value'];
        // dd($this->data['Pakettes_expiration_belumdikerjakan']);

        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('admin')) {
            // data order
            $this->data['orders'] = $this->order->asArray()
                ->select('order.*')
                ->whereIn('order.status', ['paid-confirm'])
                ->where('order.deleted', 0)
                // ->where('order.paid-confirm >=', 'DATE_SUB(NOW(), INTERVAL 1 DAY)')
                ->where('users.not_deleted', 1)
                ->join('users', 'users.id=order.id_member', 'left')
                ->orderBy('order.id', 'DESC')
                ->findAll();
                
                foreach($this->data['orders'] as $key => $order){
                    $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                    ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
                    ->where('id_order', $order['id'])
                    ->where('deleted', 0)
                    ->where('req_unblock', 1)
                    ->join('product', 'product.id=memberorderproduct.id_product')
                    ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                    ->orderBy('id', 'DESC')
                ->findAll();

                $this->data['orders'][$key]['member'] = $this->user->asArray()
                ->where('id', $order['id_member'])
                // ->where('active', 1)
                ->findAll();

                if (empty($this->data['orders'][$key]['member'])){
                    d('Order tanpa member');
                    dd($this->data['orders'][$key]);
                }

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {

                        $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                        ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                        ->where('id_memberorderproduct', $product['id'])
                        ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                        ->findAll();
                        $belum_dikerjakan = 1;
                        foreach ($this->data['orders'][$key]['products'][$key2]['jenistes'] as $key3 => $jenistes) {
                            if ($jenistes['status'] == NULL){

                            } else {
                                $belum_dikerjakan = 0;
                            } 
                        }
                        // dd($belum_dikerjakan==1 && strtotime($this->data['orders'][$key]['paid-confirm']) > strtotime('-24 hours') );
                        // belum dikerjakan dan lebih dari 24 jam
                        if ($belum_dikerjakan==1 && strtotime($this->data['orders'][$key]['paid-confirm']) < strtotime('-'.$this->data['Pakettes_expiration_belumdikerjakan'].' hours') ){
                            // unset($this->data['orders'][$key]['products']);
                        }
                }
            }
            $this->data['scope'] = 'req_activation';
            $this->data['template'] = 'admin/'.__FUNCTION__;
            return view('admin/index', $this->data);
        }
    }

    public function paket_tes_rawdata()
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();

        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('admin')) {
            // data order
            $this->data['orders'] = $this->order->asArray()
                ->whereIn('status', ['paid-confirm'])
                ->where('deleted', 0)
                ->orderBy('id', 'DESC')
                ->findAll();
                
                foreach($this->data['orders'] as $key => $order){
                    $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                    ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
                    ->where('id_order', $order['id'])
                    ->where('deleted', 0)
                    ->join('product', 'product.id=memberorderproduct.id_product')
                    ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                    ->orderBy('id', 'DESC')
                ->findAll();

                $this->data['orders'][$key]['member'] = $this->user->asArray()
                ->where('id', $order['id_member'])
                // ->where('active', 1)
                ->findAll();

                if (empty($this->data['orders'][$key]['member'])){
                    d('Order tanpa member');
                    dd($this->data['orders'][$key]);
                }

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {

                        $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                        ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                        ->where('id_memberorderproduct', $product['id'])
                        ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                        ->findAll();
                        }
            }
            $this->data['scope'] = 'test';
            $this->data['template'] = 'admin/'.__FUNCTION__;
            return view('admin/index', $this->data);
        }
        else if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('psychologists')) {
            // ambil data order yang jenistesnya status done semua ya
            // data order
            $this->data['orders'] = $this->order->asArray()
                ->select('order.*')
                ->whereIn('order.status', ['paid-confirm'])
                ->where('order.deleted', 0)
                ->where('memberorderproductjenistes.status', 'done-test')
                ->join('memberorderproduct', 'order.id=memberorderproduct.id_order', 'left')
                ->join('memberorderproductjenistes', 'memberorderproduct.id=memberorderproductjenistes.id_memberorderproduct')
                ->orderBy('order.id', 'DESC')
                ->groupBy('order.id')
                ->findAll();
                
                
                foreach($this->data['orders'] as $key => $order){
                    $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                    ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
                    ->where('id_order', $order['id'])
                    // ->where('id_psikolog', $this->data['profile']->user_id)
                    // ->whereIn('status', [null, ''])
                    ->where('deleted', 0)
                    ->join('product', 'product.id=memberorderproduct.id_product')
                    ->findAll();
                    
                    
                    $this->data['orders'][$key]['member'] = $this->user->asArray()
                    ->where('id', $order['id_member'])
                    //->where('active', 1)
                    ->findAll();
                    
                    foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                        // database error
                        if($product['start-summarization']!='0000-00-00 00:00:00'){
                            unset($this->data['orders'][$key]);
                            // d('unset karna sudah di summarize'.$order['id']);
                        } else {
                            $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                            ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                            // ->where('id_psikolog', $this->data['profile']->user_id)
                            ->where('id_memberorderproduct', $product['id'])
                            // ->whereIn('status', ['done-test'])
                            ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                            ->findAll();
                            
                            foreach ($this->data['orders'][$key]['products'][$key2]['jenistes'] as $key3 => $jenistes) {
                                if ($jenistes['status']!='done-test'){
                                    unset($this->data['orders'][$key]);
                                    // d('unset tes nya belum selesai'.$order['id']);
                                } 
                            }
                            
                        }
                        
                        
                    }
                    // dd($this->data['orders']);
            }
            // dd($this->data['orders']);

            $this->data['scope'] = 'test';
            $this->data['template'] = 'psychologist/test';
            return view('psychologist/index', $this->data);
        }


        
    }

    public function take_summarize($id_order, $id_memberorderproduct)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $data = [
            'id_psikolog' => $_SESSION['user_id'],
            'start-summarization' => date("Y-m-d H:i:s"),
            'status' => 'start-summarization',
        ];
        $this->memberorderproduct->update($id_memberorderproduct, $data);

        return redirect()->to('/test/correction/null/'.$id_order.'/'.$id_memberorderproduct);
    }

    public function done_summarization($id_order, $id_memberorderproduct)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $data = [
            'summarized' => date("Y-m-d H:i:s"),
            'status' => 'summarized',
        ];  
        if ($this->memberorderproduct->update($id_memberorderproduct, $data)){
            // $email = 'kunti.khoirunnisaa@gmail.com';
            $message = $this->result(NULL, $id_order, $id_memberorderproduct);
            $this->session->setFlashdata('info_data', 'Berhasil simpan data, Kirim email '.$message);
        }

        return redirect()->to('/test/correction/');
    }
    
    public function cancel_summarization($id_order, $id_memberorderproduct)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $data = [
            // 'summarized' => date("Y-m-d H:i:s"),
            'status' => 'start-summarization',
        ];  
        $this->memberorderproduct->update($id_memberorderproduct, $data);
        return redirect()->to('/test/correction/null/'.$id_order.'/'.$id_memberorderproduct);

    }

    
    public function result($email= NULL, $id_order = NULL, $id_memberorderproduct = NULL){
		
        
		$this->data['orders'] = $this->order->asArray()
                ->whereIn('status', ['paid-confirm'])
                ->where('deleted', 0)
                ->where('id', $id_order)
                ->orderBy('id', 'DESC')
                ->findAll();
               

            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
                ->where('id_order', $order['id'])
                ->where('memberorderproduct.id', $id_memberorderproduct)
                //->where('memberorderproduct.deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                // ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();

                $this->data['orders'][$key]['member'] = $this->user->asArray()
                ->where('id', $order['id_member'])
                //->where('active', 1)
                ->findAll();

                if($this->data['orders'][$key]['member']==null){
					return "tidak berhasil karena email tidak ditemukan";
				}

				if($this->data['orders'][$key]['products']==null){
					return "tidak berhasil karena produk tidak ditemukan";
				}

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();
                    }
					
		}
		if($this->data['orders']==null){
			return "tidak berhasil karena order tersebut tidak ditemukan";
		}
        $email = $this->data['orders'][$key]['member'][0]['email'];
        $this->data['user'] = $this->user
	            ->where('email', $email)
	            ->first();
	   
		$mail = new PHPMailer(true);

		//$mail->IsSMTP();
		$mail->SMTPSecure = 'tls'; 
		$mail->Host = "pipe-psikotes.co.id"; //host masing2 provider email
		$mail->SMTPDebug = 3;
		$mail->Port = 25;
		$mail->SMTPAuth = true;
		$mail->Username = "admin@pipe-psikotes.co.id"; //user email
		$mail->Password = "P@ssword123"; //password email 
		$mail->SetFrom("admin@pipe-psikotes.co.id","PIPE Psikotes"); //set email pengirim
		$mail->Subject = "Hasil tes Anda telah keluar"; //subyek email
		$mail->AddAddress($email, $this->data['user']['first_name'].' '.$this->data['user']['last_name']);  //email tujuan email
		$content = view('mail/hasiltes', $this->data);
		$mail->MsgHTML($content);
		if($mail->Send()) return "berhasil ke ".$email;
		else return "tidak berhasil karna gagal dikirim";
	}




    public function myproduct(){
        if (! $this->ionAuth->loggedIn()){
            $this->session->setFlashdata('message', lang('Global.login_first'));
            return redirect()->to('/auth/login');
          }
        $this->data['profile'] = $this->ionAuth->user()->row();
        $this->data['orders'] = $this->order->asArray()
                ->where('id_member', $_SESSION['user_id'])
                ->whereIn('status', ['paid-confirm'])
                ->where('deleted', 0)
                ->orderBy('id', 'DESC')
                ->findAll();

            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
                ->where('id_order', $order['id'])
                ->where('deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                // ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();
                    }
            }

            $this->data['feedbacks'] = $this->feedback->asArray()
            ->select('feedbackmember.*, users.email, users.first_name, users.last_name, users.photo')
            ->where('users.id', $this->data['profile']->id)
            ->join('users', 'users.id=feedbackmember.id_member')->findAll();

            // $this->data['orders'] = $this->order->asArray()
            //     ->where('id_member', $_SESSION['user_id'])
            //     ->where('status', 'paid-confirm')
            //     ->where('deleted', 0)
            //     ->orderBy('id', 'DESC')
            //     ->findAll();

            // foreach($this->data['orders'] as $key => $order){
            //     $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
            //     ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
            //     ->where('id_order', $order['id'])
            //     ->where('memberorderproduct.deleted', 0)
            //     ->join('product', 'product.id=memberorderproduct.id_product')
            //     ->findAll();

            //     foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
            //         $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
            //         ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
            //         ->where('id_memberorderproduct', $product['id'])
            //         ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
            //         ->findAll();
            //         }
            // }

            $this->data['scope'] = 'orderproduct';
            $this->data['template'] = 'member/product';
            return view('member/index', $this->data);
    }
    public function product($id_order, $id_memberorderproduct)
    {
        if (! $this->ionAuth->loggedIn()){
            $this->session->setFlashdata('message', lang('Global.login_first'));
            return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();
        $id_user  = $this->order->asArray()
        ->where('id', $id_order)
        ->where('deleted', 0)
        ->findColumn('id_member')[0];
        
        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members') && $this->data['profile']->id==$id_user) { 
            return redirect()->to('/dashboard');
            // data order
            $this->data['orders'] = $this->order->asArray()
                ->where('id', $id_order)
                ->where('status', 'paid-confirm')
                ->where('deleted', 0)
                ->orderBy('id', 'DESC')
                ->findAll();

            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
                ->where('memberorderproduct.id', $id_memberorderproduct)
                ->where('memberorderproduct.deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();
                    }
            }

            // dd($this->data['orders']);

           
            $this->data['scope'] = 'orderproduct';
            $this->data['template'] = 'member/product';
            return view('member/index', $this->data);
        } else {
            return redirect()->to('/');
        }

    }

    // cek apakah tes boleh dibuka atau tidak, urutannya pake id jenis tes 
    public function continue_test($id_memberorderproduct, $id_memberorderproductjenistes){

        $cek_jenistes = $this->memberorderproductjenistes->asArray()
        ->select('id_jenistes, id_memberorderproduct')
        ->where('memberorderproductjenistes.id', $id_memberorderproductjenistes)
        ->find();

        if($cek_jenistes[0]['id_memberorderproduct']!=$id_memberorderproduct){
            d('id tidak cocok');
            return false;
        }

        if (empty($cek_jenistes)){
            return false;
        } 

        $semua_jenistes = $this->memberorderproductjenistes->asArray()
        ->select('id_jenistes, status')
        ->where('memberorderproductjenistes.id_memberorderproduct', $id_memberorderproduct)
        ->orderBy('id_jenistes', 'ASC')
        ->find();

        if (empty($semua_jenistes)){
            return false;
        }
        $not_done = false;
        foreach($semua_jenistes as $key => $jenistes){
            
            if ($jenistes['id_jenistes']==$cek_jenistes[0]['id_jenistes']){
                // jika ini tes pertama, dan tes belum dikerjakan maka lanjutkan
                if ($key==0){
                    if ($jenistes['status']!='done-test'){
                        return true;
                    } 
                }
                // jika ini bukan tes pertama, cek tes sebelumnya
                else {
                    // jika tes sebelumnya sudah dikerjakan, maka lanjutkan
                    $key_sebelum = $key-1;
                    // dd($not_done);
                    if ($semua_jenistes[$key_sebelum]['status']=='done-test' && !$not_done){
                        return true;
                    } else {
                        d($not_done);
                        d($key);
                        d($key_sebelum);
                        d($semua_jenistes[$key_sebelum]['status']);
                        d('tes sebelumnya belum selesai');
                        return false;
                    }
                }
            } else {
                if ($jenistes['status']!='done-test'){
                    $not_done = true;
                }
            }
            
        }        
    }

    public function tatatertib($id_order, $id_memberorderproduct)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();
        $id_user  = $this->order->asArray()
            ->where('id', $id_order)
            ->where('deleted', 0)
            ->findColumn('id_member')[0];

        $this->data['orders'][0]['products'] = $this->memberorderproduct->asArray()
            ->where('id', $id_memberorderproduct)
            ->where('deleted', 0)
            // ->join('product', 'product.id=memberorderproduct.id_product')
            ->findAll();
        
        if(($this->ionAuth->inGroup('members') && $this->data['profile']->id==$id_user) || $this->ionAuth->inGroup('admin') || $this->ionAuth->inGroup('psychologists')) { 

            $this->data['id_order'] = $id_order;
            $this->data['id_memberorderproduct'] = $id_memberorderproduct;
            $this->data['scope'] = 'index';
            $this->data['template'] = 'member/tatatertib';
            return view('member/index', $this->data);
        } else {
            return redirect()->to('/');
        }
    }

    public function agreement($id_order, $id_memberorderproduct)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();
        $id_user  = $this->order->asArray()
            ->where('id', $id_order)
            ->where('deleted', 0)
            ->findColumn('id_member')[0];
        
        if(($this->ionAuth->inGroup('members') && $this->data['profile']->id==$id_user) || $this->ionAuth->inGroup('admin') || $this->ionAuth->inGroup('psychologists')) { 
            $this->memberorderproduct
            ->set(['isTataTertib' => '1'])        
            ->where('id', $id_memberorderproduct)
            ->update();
            return redirect()->to('/dashboard');

        } else {
            return redirect()->to('/');
        }
    }

    public function jenistes($id_order, $id_memberorderproduct, $id_memberorderproductjenistes)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();
        $id_user  = $this->order->asArray()
            ->where('id', $id_order)
            ->where('deleted', 0)
            ->findColumn('id_member')[0];

        
        // jika member benar yang memiliki tes ini
        if(($this->ionAuth->inGroup('members') && $this->data['profile']->id==$id_user) || $this->ionAuth->inGroup('admin') || $this->ionAuth->inGroup('psychologists')) { 
            
            
            // cek apakah member boleh mngerjakan tes ini, karna member bisa mengerjakan kalo tes sebelumnya sudah selesai, cek pake id_jenistes
            if(!$this->continue_test($id_memberorderproduct, $id_memberorderproductjenistes)){
                // dd('anda dilarang melanjutkan tes');
                return redirect()->to('/dashboard');
            }


            // data order
            $this->data['orders'] = $this->order->asArray()
                ->where('id', $id_order)
                ->where('status', 'paid-confirm')
                ->where('deleted', 0)
                ->orderBy('id', 'DESC')
                ->findAll();

                $pakettes= $this->memberorderproductjenistes->asArray()
                            ->select('memberorderproductjenistes.*, memberorderproduct.is_unblock, memberorderproduct.is_unblock_time')
                            ->where('memberorderproductjenistes.id_memberorderproduct', $id_memberorderproduct)
                            ->join('memberorderproduct', 'memberorderproduct.id=memberorderproductjenistes.id_memberorderproduct', 'right')
                            ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();
                
                $unblock=$pakettes[0]['is_unblock'];
                $done_all =1;
                $none_all =1;
                $is_mengerjakan = 1;

                $Pakettes_expiration= $this->settings->asArray()
                ->select('value')
                ->whereIn('id', ['6'])
                ->findAll()[0]['value'];
                $Pakettes_expiration_belumdikerjakan= $this->settings->asArray()
                ->select('value')
                ->whereIn('id', ['7'])
                ->findAll()[0]['value'];
                
                // cek apakah tes belum dikerjakan sama sekali, sedang mengerjakan atau selesai mengerjakan
                foreach ($pakettes as $key3 => $jenistes) {
                if($jenistes['status']=='start-test'){
                    $none_all =0;
                    $done_all = 0;
                } else if($jenistes['status']=='done-test' || $jenistes['done-test']!='0000-00-00 00:00:00'){
                    $none_all =0;
                } else {
                    $done_all = 0;
                }
                }
                // 3. Jika selesai mengerjakan maka tidak terjadi apa2
                if ($done_all==1) {}
                // 1. Jika belum mengerjakan, cek waktu order apakah lebih dari 24 jam 
                else if ($none_all==1) {
                // 1.b <24 jam maka status mengerjakan = 1
                if(strtotime($this->data['orders'][0]['paid-confirm']) > strtotime('-'.$Pakettes_expiration_belumdikerjakan.' hours')) {
                // 1.a >24 jam maka status is_mengerjakan = 0
                } else {
                    $is_mengerjakan = 0;
                } 
                // jika sedang mengerjakan
                } else {
                    // $stop_date = new DateTime($this->data['orders'][0]['paid-confirm']);
                    // // 3 hari kemudian sampai jam 12 malam
                    // $stop_date->modify('+'.($Pakettes_expiration+1).' day');
                    // $stop_date->setTime(0,0,0);
                    // $today = new DateTime("now");
                    // $last_time = $stop_date;
                    // if ($last_time < $today) { 
                    //     $is_mengerjakan = 0;
                    // } else {
                    //     $is_mengerjakan = 1;
                    // }
                    // 1.b <24 jam maka status mengerjakan = 1
                    if(strtotime($this->data['orders'][0]['paid-confirm']) > strtotime('-'.$Pakettes_expiration_belumdikerjakan.' hours')) {
                    // 1.a >24 jam maka status is_mengerjakan = 0
                    } else {
                        $is_mengerjakan = 0;
                    } 

                }
                if ($unblock==1){
                    if(strtotime($pakettes[0]['is_unblock_time']) > strtotime('-'.$Pakettes_expiration_belumdikerjakan.' hours')) {
                        $is_mengerjakan = 1;
                    } else {
                        $is_mengerjakan = 0;
                    }
                }
                
                if($is_mengerjakan==0){
                    // dd('anda dilarang melanjutkan tes');
                    return redirect()->to('/dashboard');
                }
            
            
                
            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->where('id', $id_memberorderproduct)
                ->where('deleted', 0)
                // ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = 
                    
                    $this->memberorderproductjenistes->asArray()
                        ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                        ->where('memberorderproductjenistes.id', $id_memberorderproductjenistes)
                        ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                ->findAll();
                }
            }
            // dd($this->data['orders'][0]['products'][0]['jenistes'][0]['done-test']);
            // jika ini member yang dimaksud, sudah mulai mengerjakan dan sudah  selesai maka redirect ke depan 
            if($this->ionAuth->inGroup('members') && $this->data['profile']->id==$id_user 
            && $this->data['orders'][0]['products'][0]['jenistes'][0]['start-test']!='0000-00-00 00:00:00'
            && $this->data['orders'][0]['products'][0]['jenistes'][0]['done-test']!='0000-00-00 00:00:00'
            && $this->data['orders'][0]['products'][0]['jenistes'][0]['status']=='done-test'
            ){
                // return redirect()->to('/test/product/'.$id_order.'/'.$id_memberorderproduct);
                return redirect()->to('/dashboard');
            }
            // jika bukan member dan tes belum dimulai
            if(!$this->ionAuth->inGroup('members')  
            && $this->data['orders'][0]['products'][0]['jenistes'][0]['start-test']='0000-00-00 00:00:00'
            ){
                return redirect()->to('/test/detail/'.$id_order.'/'.$id_memberorderproduct);
            }

            // dd($this->data['orders']);

            $id_jenistes = $this->data['orders'][0]['products'][0]['jenistes'][0]['id_jenistes'];

            // data bagian tes
            $this->data['bagiantes'] = $this->bagiantes->asArray()
                ->where('id_jenistes', $id_jenistes)
                ->where('deleted', 0)
                ->orderBy('urutan', 'ASC')
                ->findAll();

            foreach ($this->data['bagiantes'] as $key => $bagiantes) {
               
                $this->data['bagiantes'][$key]['soal'] = $this->soaltes->asArray()
                    ->where('soaltes.id_jenistes', $bagiantes['id_jenistes'])
                    ->where('soaltes.urutan_bagian_tes', $bagiantes['urutan'])
                    ->where('soaltes.deleted', 0)
                    ->orderBy('soaltes.sub_bagian', 'ASC')
                    ->orderBy('soaltes.urutan', 'ASC')
                    ->findAll();
                // dd($bagiantes);
                    foreach ($this->data['bagiantes'][$key]['soal'] as $key2 => $soal) {
                        $this->data['bagiantes'][$key]['soal'][$key2]['pilihanjawaban'] = $this->pilihanjawaban->asArray()
                            ->where('id_jenistes', $bagiantes['id_jenistes'])
                            ->where('urutan_bagian_tes', $bagiantes['urutan'])
                            ->where('sub_bagian', $soal['sub_bagian'])
                            ->where('urutan_soal', $soal['urutan'])
                            ->where('deleted', 0)
                            ->orderBy('urutan', 'ASC')
                            ->findAll();

                            // data jawaban member jika ada
                            $this->data['jawabanmember'] = $this->jawabanmember->asArray()
                                ->select('id, urutan_jawaban, jawaban_isian, jawaban_time')
                                ->where('id_memberorderproductjenistes', $id_memberorderproductjenistes)
                                ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                ->where('sub_bagian', $soal['sub_bagian'])
                                ->where('urutan_soal', $soal['urutan'])
                                ->orderBy('id', 'ASC')
                                ->findAll();
                            // d($this->data['jawabanmember']);

                            if($this->data['jawabanmember']){
                                $this->data['bagiantes'][$key]['soal'][$key2]['jawabanmember'] = $this->data['jawabanmember'];
                            }

                            // dd($this->data['orders'][0]['products'][0]['jenistes'][0]['status']);
                            // jika pertama kali mengerjakan tes, generate jawaban
                            if ($this->data['orders'][0]['status']=='paid-confirm' && $this->data['orders'][0]['products'][0]['jenistes'][0]['status']==''){
                                // $this->jawabanmember->insert(['id_memberorderproductjenistes' => $id_memberorderproductjenistes, 'urutan_bagian_tes' => $bagiantes['urutan'], 'sub_bagian' => $soal['sub_bagian'], 'urutan_soal' => $soal['urutan']]);
                                $this->memberorderproductjenistes
                                ->set(['status' => 'start-test', 'start-test' => date("Y-m-d H:i:s")])        
                                ->where('id', $id_memberorderproductjenistes)
                                ->update();
                            }
                            // jika sudah mngerjakan tes ambil waktu terajhir menjawab


                            }
                        // insert $id_memberorderproductjenistes
                        // urutan bagian tes
                        // $bagiantes['urutan']
                        // urutan soal
                        // $soal['urutan']

            }
            // jika member sudah mulai mengerjaklan dan belum selesai maka lanjutkan dengan waktu yang tersisa
            // time left dalam second, 1 jam = 3600 second
            // dd();
            $this->data['timer_checkpoint'] = 0;
            if($this->data['orders'][0]['products'][0]['jenistes'][0]['timer_checkpoint']!=0){
                $this->data['timer_checkpoint'] = $this->data['orders'][0]['products'][0]['jenistes'][0]['timer_checkpoint'];
            }
            $this->data['bagian_checkpoint'] = 0;
            if($this->data['orders'][0]['products'][0]['jenistes'][0]['bagian_checkpoint']!=0){
                $this->data['bagian_checkpoint'] = $this->data['orders'][0]['products'][0]['jenistes'][0]['bagian_checkpoint'];
            }
            
            $this->data['timerhafalan_checkpoint'] = 0;
            if($this->data['orders'][0]['products'][0]['jenistes'][0]['timerhafalan_checkpoint']!=0){
                $this->data['timerhafalan_checkpoint'] = $this->data['orders'][0]['products'][0]['jenistes'][0]['timerhafalan_checkpoint'];
            }
            
            // jika member melanjutkan tes maka cek jawaban terakhir di menit brp, kalo lebih dari 1 jam, jawaban lama di lock
            $lasttime_jawabanmember = $this->jawabanmember->asArray()
                                ->select('max(jawaban_time)')
                                ->where('id_memberorderproductjenistes', $id_memberorderproductjenistes)
                                // ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                // ->where('sub_bagian', $soal['sub_bagian'])
                                // ->where('urutan_soal', $soal['urutan'])
                                // ->orderBy('id', 'ASC')
                                ->findAll();
            $is_block = 0;
            $stop_date = new DateTime($lasttime_jawabanmember[0]['max(jawaban_time)']);
            // 3 hari kemudian sampai jam 12 malam
            $stop_date->modify('+1 hours');
            // $stop_date->setTime(0,0,0);
            $today = new DateTime("now");
            $last_time = $stop_date;
            if ($last_time < $today) { 
                $is_block = 1;
            } else {
                $is_block = 0;
                // $y = $today->diff($last_time)->y;
                // $m = $today->diff($last_time)->m;
                // $d = $today->diff($last_time)->d;
                // $h = $today->diff($last_time)->h;
                // $i = $today->diff($last_time)->i;
                // $expired = 0;
            //   hangus dalam
                // echo '<small>'.lang('Global.expired')." ".$d." ".lang('Global.day')." ".$h."  ".lang('Global.hour')." ".$i." ".lang('Global.minute')."</small>"; 
            }
            $this->data['is_block'] = $is_block;
            
            // dd($this->data['orders'][0]['products'][0]['jenistes'][0]); 
            // dd($this->data['bagiantes']); 

            $this->data['scope'] = 'orderproduct';
            $this->data['template'] = 'test/memberorderproduct';
            if ($id_jenistes==1){
                return view('test/papertest_teskecerdasan', $this->data);
            } else {
                return view('test/papertest', $this->data);
            }
        } else {
            return redirect()->to('/');
        }
    }

    
 
    // simpan checkpoint setiap detik berkurang
    public function save_checkpoint() 
    {
            $response;
            $response = $this->memberorderproductjenistes
            ->set([
                'bagian_checkpoint' => $_POST['bagian_tes'],
                'timer_checkpoint' => $_POST['timer_checkpoint'],
            ])           
            ->where(['id' => $_POST['id_memberorderproductjenistes']])
            ->update(); 
                
            
            echo $response; 
    }

    // simpan hafalan checkpoint setiap detik berkurang
    public function savehafalan_checkpoint() 
    {
            $response;
            $response = $this->memberorderproductjenistes
            ->set([
                'timerhafalan_checkpoint' => $_POST['timerhafalan_checkpoint']
            ])           
            ->where(['id' => $_POST['id_memberorderproductjenistes']])
            ->update(); 
                
            
            echo $response; 
    }
    
    // simpan jawaban member setiap kali menjawab
    public function save_jawaban() 
    {
    // public function save_jawaban($id_memberorderproductjenistes, $id_bagiantes, $urutan_soal, $urutan_jawaban, $isian) {
            $sub_bagian = NULL;
            if (isset($_POST['sub_bagian'])){
                $sub_bagian=$_POST['sub_bagian'];
            }
           

            $score = $this->cek_jawaban($_POST['bagian_tes'], $sub_bagian, $_POST['urutan_soal'], $_POST['id_memberorderproductjenistes'], $_POST['urutan_jawaban']);
            if($_POST['urutan_jawaban']==0){
                $score=NULL;
            }

            $response;
            $this->cek_baris_availability($_POST['bagian_tes'], $sub_bagian, $_POST['urutan_soal'], $_POST['id_memberorderproductjenistes']);
            // kalo ada sub bagian (untuk jenis tes 2)
            if ($sub_bagian!=NULL){
                $response = $this->jawabanmember
                ->set([
                    'urutan_jawaban' => $_POST['urutan_jawaban'],
                    'jawaban_isian' => $_POST['jawaban_isian'],
                    'jawaban_timer' => $_POST['counts'],
                    'jawaban_time' =>  date("Y-m-d H:i:s"),
                    'score' => $score,
                ])        
                ->where(['id_memberorderproductjenistes' => $_POST['id_memberorderproductjenistes'], 'urutan_bagian_tes' => $_POST['bagian_tes'], 'sub_bagian' => $_POST['sub_bagian'],'urutan_soal' => $_POST['urutan_soal']])
                ->update(); 
            // kalo ngga ada sub bagian
            } else {
                
                // d('ngga ada sub bagian');
                $response = $this->jawabanmember
                ->set([
                    'urutan_jawaban' => $_POST['urutan_jawaban'],
                    'jawaban_isian' => $_POST['jawaban_isian'],
                    'jawaban_timer' => $_POST['counts'],
                    'jawaban_time' =>  date("Y-m-d H:i:s"),
                    'score' => $score,
                ])        
                ->where(['id_memberorderproductjenistes' => $_POST['id_memberorderproductjenistes'], 'urutan_bagian_tes' => $_POST['bagian_tes'], 'urutan_soal' => $_POST['urutan_soal']])
                ->update(); 
                
            }
            echo $response; 
    }

    function cek_baris_availability($urutan_bagian_tes, $sub_bagian=NULL, $urutan_soal, $id_memberorderproductjenistes){
        // insert baris jika belum ada 
        // insert
        if ($sub_bagian==NULL){
            $cek = $this->jawabanmember
            ->select('id')
            ->where(['id_memberorderproductjenistes' => $id_memberorderproductjenistes, 'urutan_bagian_tes' => $urutan_bagian_tes, 'urutan_soal' => $urutan_soal])
            ->findAll();
            if(empty($cek)){
                $this->jawabanmember->insert(['id_memberorderproductjenistes' => $id_memberorderproductjenistes, 'urutan_bagian_tes' => $urutan_bagian_tes, 'urutan_soal' => $urutan_soal]);
            }
        } else {
            $cek = $this->jawabanmember
            ->select('id')
            ->where(['id_memberorderproductjenistes' => $id_memberorderproductjenistes, 'urutan_bagian_tes' => $urutan_bagian_tes, 'sub_bagian' => $sub_bagian, 'urutan_soal' => $urutan_soal])
            ->findAll();
            if(empty($cek)){
            $this->jawabanmember->insert(['id_memberorderproductjenistes' => $id_memberorderproductjenistes, 'urutan_bagian_tes' => $urutan_bagian_tes, 'sub_bagian' => $sub_bagian, 'urutan_soal' => $urutan_soal]);
            }
        }

        
        
         
    }

    public function cek_jawaban($urutan_bagian_tes, $sub_bagian=NULL, $urutan_soal, $id_memberorderproductjenistes, $urutan_jawaban)
    {

            $correct_answer;
            if ($sub_bagian==NULL){
                $correct_answer = $this->pilihanjawaban->asArray()
                ->select('pilihanjawaban.urutan')
                ->where('pilihanjawaban.urutan_bagian_tes', $urutan_bagian_tes)
                ->where('pilihanjawaban.sub_bagian is null')
                ->where('pilihanjawaban.urutan_soal', $urutan_soal)
                ->where('pilihanjawaban.correct_answer', 1)
                ->where('memberorderproductjenistes.id', $id_memberorderproductjenistes)
                ->join('memberorderproductjenistes', 'memberorderproductjenistes.id_jenistes=pilihanjawaban.id_jenistes')
                ->orderBy('pilihanjawaban.urutan', 'ASC')
                ->findColumn('urutan');
            } else {
                $correct_answer = $this->pilihanjawaban->asArray()
                ->select('pilihanjawaban.urutan')
                ->where('pilihanjawaban.urutan_bagian_tes', $urutan_bagian_tes)
                ->where('pilihanjawaban.sub_bagian', $sub_bagian)
                ->where('pilihanjawaban.urutan_soal', $urutan_soal)
                ->where('pilihanjawaban.correct_answer', 1)
                ->where('memberorderproductjenistes.id', $id_memberorderproductjenistes)
                ->join('memberorderproductjenistes', 'memberorderproductjenistes.id_jenistes=pilihanjawaban.id_jenistes')
                ->orderBy('pilihanjawaban.urutan', 'ASC')
                ->findColumn('urutan');
            }


            $urutan_jawaban = explode("_", $urutan_jawaban);
            $urutan_jawaban = array_filter($urutan_jawaban);
            return $urutan_jawaban==$correct_answer;
            
    }

    // function untuk update cart berdasarkan id dan quantity
    public function done_test($id_order, $id_memberorderproduct, $id_memberorderproductjenistes)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();
        $id_user  = $this->order->asArray()
            ->where('id', $id_order)
            ->where('deleted', 0)
            ->findColumn('id_member')[0];

        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members') && $this->data['profile']->id==$id_user) { 
            $this->memberorderproductjenistes
                ->set(['status' => 'done-test', 'done-test' => date("Y-m-d H:i:s")])        
                ->where('id', $id_memberorderproductjenistes)
                ->update();

            return redirect()->to('/test/product/'.$id_order.'/'.$id_memberorderproduct);
        }
    }

    public function detail_admin($id_order, $id_memberorderproduct)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();


        if($this->ionAuth->loggedIn() && !$this->ionAuth->inGroup('members')) {
            // jawaban pencocok papikostick
            $jawaban_pencocok = [['G', 'E'], ['A', 'N'], ['P', 'A'], ['X', 'P'], ['B', 'X'], ['O', 'B'], ['Z', 'O'], ['K', 'Z'], ['F', 'K'], ['F', 'F'], ['G', 'C'], ['L', 'E'], ['P', 'N'], ['X', 'L'], ['B', 'P'], ['O', 'X'], ['Z', 'B'], ['K', 'O'], ['F', 'Z'], ['F', 'K'], ['G', 'D'], ['L', 'C'], ['I', 'E'], ['X', 'N'], ['B', 'A'], ['O', 'P'], ['Z', 'X'], ['K', 'B'], ['F', 'O'], ['F', 'Z'], ['G', 'R'], ['L', 'D'], ['I', 'C'], ['T', 'E'], ['B', 'N'], ['O', 'A'], ['Z', 'P'], ['K', 'X'], ['F', 'B'], ['F', 'O'], ['G', 'S'], ['L', 'R'], ['I', 'D'], ['T', 'C'], ['V', 'E'], ['O', 'N'], ['Z', 'A'], ['K', 'P'], ['F', 'X'], ['F', 'B'], ['G', 'V'], ['L', 'S'], ['I', 'R'], ['T', 'D'], ['V', 'C'], ['S', 'E'], ['Z', 'N'], ['K', 'A'], ['F', 'P'], ['F', 'X'], ['G', 'T'], ['L', 'V'], ['I', 'S'], ['T', 'R'], ['V', 'D'], ['S', 'C'], ['R', 'E'], ['K', 'N'], ['F', 'A'], ['N', 'P'], ['G', 'I'], ['L', 'T'], ['I', 'V'], ['T', 'S'], ['V', 'R'], ['S', 'D'], ['R', 'C'], ['D', 'E'], ['C', 'N'], ['E', 'A'], ['G', 'L'], ['L', 'I'], ['I', 'T'], ['T', 'V'], ['V', 'S'], ['S', 'R'], ['R', 'D'], ['D', 'C'], ['C', 'E'], ['E', 'N'], ];

            $this->data['orders'] = $this->order->asArray()
                ->where('id', $id_order)
                ->where('status', 'paid-confirm')
                ->where('deleted', 0)
                ->orderBy('id', 'DESC')
                ->findAll();
            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['member'] = $this->user->asArray()
                ->where('id', $order['id_member'])
                //->where('active', 1)
                ->findAll();

                
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
                ->where('memberorderproduct.id', $id_memberorderproduct)
                ->where('memberorderproduct.deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {

                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();
                    // dd($this->data['orders'][$key]['products'][$key2]['jenistes']);

                    foreach ($this->data['orders'][$key]['products'][$key2]['jenistes'] as $key3 => $jenistes) { 
                         
;                        $this->data['jenistes_detail'][$key3]['bagiantes'] = $this->bagiantes->asArray()
                            ->where('id_jenistes', $jenistes['id_jenistes'])
                            ->where('deleted', 0)
                            ->orderBy('urutan', 'ASC')
                            ->findAll();

                            foreach ($this->data['jenistes_detail'][$key3]['bagiantes'] as $key4 => $bagiantes) {

                                // dd($bagiantes);
                                $this->data['jenistes_detail'][$key3]['bagiantes'][$key4]['soal_asli'] = $this->soaltes->asArray()
                                    ->where('id_jenistes', $bagiantes['id_jenistes'])
                                    ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                    ->where('deleted', 0)
                                    // ->orderBy('urutan', 'ASC')
                                    ->findAll();
                                if ($bagiantes['id_jenistes']!=2){
                                    $this->data['jenistes_detail'][$key3]['bagiantes'][$key4]['soal'] = $this->soaltes->asArray()
                                    ->select('soaltes.urutan, ANY_VALUE(soaltes.soal) as soal, ANY_VALUE(soaltes.gambar) as gambar, max(soaltes.sub_bagian) as sub_bagian, max(jawabanmember.jawaban_time) as jawaban_time,  max(jawabanmember.score) as score, min(jawabanmember.jawaban_timer) as jawaban_timer, max(jawabanmember.urutan_jawaban), any_value(jawabanmember.jawaban_isian) as jawaban_isian')
                                    ->where('soaltes.id_jenistes', $bagiantes['id_jenistes'])
                                    ->where('soaltes.urutan_bagian_tes', $bagiantes['urutan'])
                                    ->where('soaltes.deleted', 0)
                                    ->where('jawabanmember.id_memberorderproductjenistes', $jenistes['id'])
                                    ->join('jawabanmember', '
                                    soaltes.urutan_bagian_tes=jawabanmember.urutan_bagian_tes AND
                                    soaltes.urutan=jawabanmember.urutan_soal 
                                    ', 'left')
                                    ->join('memberorderproductjenistes', 'memberorderproductjenistes.id=jawabanmember.id_memberorderproductjenistes AND memberorderproductjenistes.id_jenistes=soaltes.id_jenistes', 'right')
                                    ->groupBy('soaltes.urutan')
                                    ->findAll();
                                } else {
                                    $this->data['jenistes_detail'][$key3]['bagiantes'][$key4]['soal'] = $this->soaltes->asArray()
                                    ->select('soaltes.urutan, ANY_VALUE(soaltes.soal) as soal, ANY_VALUE(soaltes.gambar) as gambar, max(soaltes.sub_bagian) as sub_bagian, max(jawabanmember.jawaban_time) as jawaban_time,  max(jawabanmember.score) as score, min(jawabanmember.jawaban_timer) as jawaban_timer, max(jawabanmember.urutan_jawaban), any_value(jawabanmember.jawaban_isian) as jawaban_isian')
                                    // ->select('soaltes.*, jawabanmember.jawaban_time, jawabanmember.jawaban_isian, jawabanmember.score, jawabanmember.jawaban_timer, jawabanmember.urutan_jawaban')
                                    ->where('soaltes.id_jenistes', $bagiantes['id_jenistes'])
                                    ->where('soaltes.urutan_bagian_tes', $bagiantes['urutan'])
                                    ->where('soaltes.deleted', 0)
                                    ->where('jawabanmember.id_memberorderproductjenistes', $jenistes['id'])
                                    ->join('jawabanmember', '
                                    soaltes.urutan_bagian_tes=jawabanmember.urutan_bagian_tes AND
                                    soaltes.sub_bagian=jawabanmember.sub_bagian AND
                                    soaltes.urutan=jawabanmember.urutan_soal 
                                    ', 'left')
                                    ->join('memberorderproductjenistes', 'memberorderproductjenistes.id=jawabanmember.id_memberorderproductjenistes AND memberorderproductjenistes.id_jenistes=soaltes.id_jenistes', 'right')
                                    ->groupBy('soaltes.id')
                                        ->findAll();
                                }

                           
    
                            }
                    }

                }
            }
            if($this->ionAuth->inGroup('psychologists')) {
                $this->data['scope'] = __FUNCTION__;
                $this->data['template'] = 'test/test_detail';
                return view('psychologist/index', $this->data);
                
            } else if($this->ionAuth->inGroup('admin')) {
                // dd($this->data['jenistes_detail']);
                $this->data['scope'] = 'all';
                $this->data['template'] = 'admin/test_detail';
                return view('admin/index', $this->data);
            }
        }
    }

    public function detail($id_order, $id_memberorderproduct)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();


        if($this->ionAuth->loggedIn() && !$this->ionAuth->inGroup('members')) {
            // jawaban pencocok papikostick
            $jawaban_pencocok = [['G', 'E'], ['A', 'N'], ['P', 'A'], ['X', 'P'], ['B', 'X'], ['O', 'B'], ['Z', 'O'], ['K', 'Z'], ['F', 'K'], ['F', 'F'], ['G', 'C'], ['L', 'E'], ['P', 'N'], ['X', 'L'], ['B', 'P'], ['O', 'X'], ['Z', 'B'], ['K', 'O'], ['F', 'Z'], ['F', 'K'], ['G', 'D'], ['L', 'C'], ['I', 'E'], ['X', 'N'], ['B', 'A'], ['O', 'P'], ['Z', 'X'], ['K', 'B'], ['F', 'O'], ['F', 'Z'], ['G', 'R'], ['L', 'D'], ['I', 'C'], ['T', 'E'], ['B', 'N'], ['O', 'A'], ['Z', 'P'], ['K', 'X'], ['F', 'B'], ['F', 'O'], ['G', 'S'], ['L', 'R'], ['I', 'D'], ['T', 'C'], ['V', 'E'], ['O', 'N'], ['Z', 'A'], ['K', 'P'], ['F', 'X'], ['F', 'B'], ['G', 'V'], ['L', 'S'], ['I', 'R'], ['T', 'D'], ['V', 'C'], ['S', 'E'], ['Z', 'N'], ['K', 'A'], ['F', 'P'], ['F', 'X'], ['G', 'T'], ['L', 'V'], ['I', 'S'], ['T', 'R'], ['V', 'D'], ['S', 'C'], ['R', 'E'], ['K', 'N'], ['F', 'A'], ['N', 'P'], ['G', 'I'], ['L', 'T'], ['I', 'V'], ['T', 'S'], ['V', 'R'], ['S', 'D'], ['R', 'C'], ['D', 'E'], ['C', 'N'], ['E', 'A'], ['G', 'L'], ['L', 'I'], ['I', 'T'], ['T', 'V'], ['V', 'S'], ['S', 'R'], ['R', 'D'], ['D', 'C'], ['C', 'E'], ['E', 'N'], ];

            $this->data['orders'] = $this->order->asArray()
                ->where('id', $id_order)
                ->where('status', 'paid-confirm')
                ->where('deleted', 0)
                ->orderBy('id', 'DESC')
                ->findAll();
            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['member'] = $this->user->asArray()
                ->where('id', $order['id_member'])
                //->where('active', 1)
                ->findAll();

                
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
                ->where('memberorderproduct.id', $id_memberorderproduct)
                ->where('memberorderproduct.deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {

                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();

                    foreach ($this->data['orders'][$key]['products'][$key2]['jenistes'] as $key3 => $jenistes) { 
                         
;                        $this->data['jenistes_detail'][$key3]['bagiantes'] = $this->bagiantes->asArray()
                            ->where('id_jenistes', $jenistes['id_jenistes'])
                            ->where('deleted', 0)
                            ->orderBy('urutan', 'ASC')
                            ->findAll();

                            // d($this->data['jenistes_detail'][$key3]['bagiantes']);

                            foreach ($this->data['jenistes_detail'][$key3]['bagiantes'] as $key4 => $bagiantes) {

                                // dd($bagiantes);
                                $this->data['jenistes_detail'][$key3]['bagiantes'][$key4]['soal'] = $this->soaltes->asArray()
                                    ->where('id_jenistes', $bagiantes['id_jenistes'])
                                    ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                    ->where('deleted', 0)
                                    // ->orderBy('urutan', 'ASC')
                                    ->findAll();
                                

;                                $this->data['jenistes_detail'][$key3]['bagiantes'][$key4]['tidak_menjawab'] = $this->jawabanmember->asArray()
                                    ->select('COUNT(id) as tidak_menjawab')
                                    ->where('id_memberorderproductjenistes', $jenistes['id'])
                                    ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                    ->where('jawaban_time is null')
                                    ->where('deleted', 0)
                                    ->groupBy('jawabanmember.urutan_bagian_tes')
                                    ->findAll();

                                    foreach ($this->data['jenistes_detail'][$key3]['bagiantes'][$key4]['soal'] as $key5 => $soal) {
                                            // data jawaban member jika ada
                                            $this->data['jawabanmember'] = $this->jawabanmember->asArray()
                                                ->select('jawaban_time, jawaban_isian, score, jawaban_timer')
                                                ->where('id_memberorderproductjenistes', $jenistes['id'])
                                                ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                                ->where('sub_bagian', $soal['sub_bagian'])
                                                ->where('urutan_soal', $soal['urutan'])
                                                ->orderBy('id', 'ASC')
                                                ->findAll();
                                            // dd($this->data['jawabanmember']);
                                            if($this->data['jawabanmember']){
                                                $this->data['jenistes_detail'][$key3]['bagiantes'][$key4]['soal'][$key5]['jawabanmember'] = $this->data['jawabanmember'];
                                            } else {
                                                $this->data['jenistes_detail'][$key3]['bagiantes'][$key4]['soal'][$key5]['jawabanmember'] = NULL;
                                            }
                                           
                                            }
                                       
                            }
                    }

                }
            }
            if($this->ionAuth->inGroup('psychologists')) {
                $this->data['scope'] = __FUNCTION__;
                $this->data['template'] = 'test/test_detail';
                return view('psychologist/index', $this->data);
            
            } else if($this->ionAuth->inGroup('admin')) {
                $this->data['scope'] = 'all';
                $this->data['template'] = 'test/test_detail';
                return view('admin/index', $this->data);
            }
        }
    }

    public function export($ringkasan = NULL, $id_order = NULL, $id_memberorderproduct = NULL)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();


        if($this->ionAuth->loggedIn() && !$this->ionAuth->inGroup('members')) {
        if($id_order==NULL && $id_memberorderproduct==NULL) {
            // ambil data order yang jenistesnya status done semua ya
            // data order
            $this->data['tes'] = $this->memberorderproductjenistes->asArray()
                ->select('order.id as id_order, memberorderproduct.id as id_memberorderproduct, 
                    memberorderproductjenistes.id as id_memberorderproductjenistes, 
                    memberorderproductjenistes.id_jenistes as id_jenistes, 
                    memberorderproductjenistes.start-test as start-test, 
                    product.nama as nama_product, 
                    jenistes.nama as nama_tes,
                    users.first_name, users.last_name, users.sex, users.last_time
                    ')
                // ->where('id_psikolog', $this->data['profile']->user_id)
                ->where('memberorderproductjenistes.id_jenistes', 2)
                // ->whereIn('status', ['done-test'])
                ->join('memberorderproduct', 'memberorderproduct.id=memberorderproductjenistes.id_memberorderproduct')
                ->join('order', 'order.id=memberorderproduct.id_order')
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->join('users', 'users.id=order.id_member')

                ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                ->findAll();
            
                foreach ($this->data['tes'] as $key1 => $tes){
                    // soal tes join ke jawaban member
                    $this->data['tes'][$key1]['soaltes'] = $this->soaltes->asArray()
                            ->select('soaltes.*, jawabanmember.urutan_jawaban')
                            ->where('soaltes.id_jenistes', $tes['id_jenistes'])
                            ->where('soaltes.deleted', 0)
                            ->where('id_memberorderproductjenistes', $tes['id_memberorderproductjenistes'])
                            ->where('soaltes.deleted', 0)
                            // ->where('jawabanmember.deleted', 0)
                            ->join('jawabanmember', 'soaltes.urutan_bagian_tes=jawabanmember.urutan_bagian_tes AND soaltes.sub_bagian=jawabanmember.sub_bagian AND soaltes.urutan=jawabanmember.urutan_soal', 'left')
                            ->join('memberorderproductjenistes', 'jawabanmember.id_memberorderproductjenistes=memberorderproductjenistes.id')
                            // ->groupBy('soaltes.id')
                            // ->where('urutan_bagian_tes', 3)
                            // ->where('sub_bagian', 2)
                            ->findAll();
                    $this->data['tes'][$key1]['jawaban'] = $this->jawabanmember->asArray()
                            ->select('jawabanmember.*')
                            ->where('id_memberorderproductjenistes', $tes['id_memberorderproductjenistes'])
                            ->where('soaltes.deleted', 0)
                            ->where('jawabanmember.deleted', 0)
                            // ->where('jawabanmember.urutan_bagian_tes', 3)
                            // ->where('jawabanmember.sub_bagian', 2)
                            ->join('memberorderproductjenistes', 'jawabanmember.id_memberorderproductjenistes=memberorderproductjenistes.id')
                            ->join('soaltes', 'soaltes.urutan_bagian_tes=jawabanmember.urutan_bagian_tes AND soaltes.sub_bagian=jawabanmember.sub_bagian AND soaltes.urutan=jawabanmember.urutan_soal')
                            ->groupBy('jawabanmember.id')
                            ->findAll();
                }
            dd($this->data['tes']);
            
            $this->data['orders'] = $this->order->asArray()
                ->select('order.id, users.first_name, users.last_name, users.sex, users.last_time, product.id as id_product, product.nama')
                ->whereIn('order.status', ['paid-confirm'])
                ->where('order.deleted', 0)
                ->where('memberorderproduct.deleted', 0)
                // ->where('memberorderproduct.status', '')
                ->join('memberorderproduct', 'order.id=memberorderproduct.id_order')
                ->join('memberorderproductjenistes', 'memberorderproduct.id=memberorderproductjenistes.id_memberorderproduct')
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->join('users', 'users.id=order.id_member')

                ->orderBy('order.id', 'DESC')
                // ->groupBy('product.id')
                ->findAll();
            dd($this->data['orders']);

            foreach($this->data['orders'] as $key => $order){

                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
                ->where('id_order', $order['id'])
                ->where('id_psikolog', $this->data['profile']->user_id)
                ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                // ->where('status', '')
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->where('deleted', 0)
                ->findAll();

                
                if(!empty($this->data['orders'][$key]['products'])){
                    
                    foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                        
                        $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                        ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                        // ->where('id_psikolog', $this->data['profile']->user_id)
                        ->where('id_memberorderproduct', $product['id'])
                        // ->whereIn('status', ['done-test'])
                        ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                        ->findAll();

                        foreach($this->data['orders'][$key]['products'][$key2]['jenistes'] as $key3 => $jenistes) {
                            $last_time = new DateTime($this->data['orders'][$key]['last_time']);
                            $today = new DateTime($this->data['orders'][$key]['products'][$key2]['jenistes'][$key3]['start-test']);
                            if ($last_time > $today) { 
                            $y = 0;
                            $m = 0;
                            $d = 0;
                            } else {
                            $y = $today->diff($last_time)->y;
                            $m = $today->diff($last_time)->m;
                            $d = $today->diff($last_time)->d;
                            }
                            $this->data['orders'][$key]['products'][$key2]['jenistes'][$key3]['usia_saat_tes'] = $y;

                            $this->data['orders'][$key]['products'][$key2]['jenistes'][$key3]['jawaban'] = $this->jawabanmember->asArray()
                            ->select('jawabanmember.*')
                            ->where('id_memberorderproductjenistes', $jenistes['id'])
                            ->findAll();

                        }
                    }
                        
                    if (empty($this->data['orders'][$key]['products'][$key2]['jenistes'])){
                        unset($this->data['orders'][$key]);
                        // dd('hapus aja');
                    }
                    }
            }

            dd($this->data['orders'][2]);
            
            $this->data['scope'] = __FUNCTION__;
            $this->data['template'] = 'psychologist/'.__FUNCTION__;
            return view('psychologist/index', $this->data);
        }
        
        }
    }
    
    public function correction($ringkasan = NULL, $id_order = NULL, $id_memberorderproduct = NULL)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();


        if($this->ionAuth->loggedIn() && !$this->ionAuth->inGroup('members')) {
            if($id_order==NULL && $id_memberorderproduct==NULL) {
                // ambil data order yang jenistesnya status done semua ya
                // data order
    
    
                $this->data['orders'] = $this->order->asArray()
                    ->select('order.*')
                    ->whereIn('order.status', ['paid-confirm'])
                    ->where('order.deleted', 0)
                    ->where('users.not_deleted', 1)
                    // ->where('memberorderproduct.status', '')
                    ->join('users', 'users.id=order.id_member')
                    ->join('memberorderproduct', 'order.id=memberorderproduct.id_order')
                    ->join('memberorderproductjenistes', 'memberorderproduct.id=memberorderproductjenistes.id_memberorderproduct')
                    ->orderBy('id', 'DESC')
                    ->groupBy('order.id')
                    ->findAll();
                // dd($this->data['orders']);
    
                foreach($this->data['orders'] as $key => $order){
    
                if($ringkasan=='summarized'){
                    $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                    ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
                    ->where('id_order', $order['id'])
                    ->where('id_psikolog', $this->data['profile']->user_id)
                    ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                    ->where('status', 'summarized')
                    ->where('deleted', 0)
                    ->join('product', 'product.id=memberorderproduct.id_product')
                    ->findAll();
                } else if($ringkasan=='start-summarization'){
                    $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                    ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
                    ->where('id_order', $order['id'])
                    ->where('id_psikolog', $this->data['profile']->user_id)
                    ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                    ->where('status', 'start-summarization')
                    ->where('deleted', 0)
                    ->join('product', 'product.id=memberorderproduct.id_product')
                    ->findAll();
                } else {
                    $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                    ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
                    ->where('id_order', $order['id'])
                    ->where('id_psikolog', $this->data['profile']->user_id)
                    ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                    // ->where('status', '')
                    ->where('deleted', 0)
                    ->join('product', 'product.id=memberorderproduct.id_product')
                    ->findAll();
    
                } 
    
                    $this->data['orders'][$key]['member'] = $this->user->asArray()
                    ->where('id', $order['id_member'])
                    //->where('active', 1)
                    ->findAll();
    
                        if(!empty($this->data['orders'][$key]['products'])){
                            
                        foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
    
                            $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                            ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                            // ->where('id_psikolog', $this->data['profile']->user_id)
                            ->where('id_memberorderproduct', $product['id'])
                            // ->whereIn('status', ['done-test'])
                            ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                            ->findAll();
                        }
                            
                        if (empty($this->data['orders'][$key]['products'][$key2]['jenistes'])){
                            unset($this->data['orders'][$key]);
                            // dd('hapus aja');
                        }
                        }
                }
                
                $this->data['scope'] = __FUNCTION__;
                $this->data['template'] = 'psychologist/'.__FUNCTION__;
                return view('psychologist/index', $this->data);
        }
        // detail
         else {
            // jawaban pencocok papikostick
            $jawaban_pencocok = [['G', 'E'], ['A', 'N'], ['P', 'A'], ['X', 'P'], ['B', 'X'], ['O', 'B'], ['Z', 'O'], ['K', 'Z'], ['F', 'K'], ['W', 'F'], ['G', 'C'], ['L', 'E'], ['P', 'N'], ['X', 'A'], ['B', 'P'], ['O', 'X'], ['Z', 'B'], ['K', 'O'], ['F', 'Z'], ['W', 'K'], ['G', 'D'], ['L', 'C'], ['I', 'E'], ['X', 'N'], ['B', 'A'], ['O', 'P'], ['Z', 'X'], ['K', 'B'], ['F', 'O'], ['W', 'Z'], ['G', 'R'], ['L', 'D'], ['I', 'C'], ['T', 'E'], ['B', 'N'], ['O', 'A'], ['Z', 'P'], ['K', 'X'], ['F', 'B'], ['W', 'O'], ['G', 'S'], ['L', 'R'], ['I', 'D'], ['T', 'C'], ['V', 'E'], ['O', 'N'], ['Z', 'A'], ['K', 'P'], ['F', 'X'], ['W', 'B'], ['G', 'V'], ['L', 'S'], ['I', 'R'], ['T', 'D'], ['V', 'C'], ['S', 'E'], ['Z', 'N'], ['K', 'A'], ['F', 'P'], ['W', 'X'], ['G', 'T'], ['L', 'V'], ['I', 'S'], ['T', 'R'], ['V', 'D'], ['S', 'C'], ['R', 'E'], ['K', 'N'], ['F', 'A'], ['W', 'P'], ['G', 'I'], ['L', 'T'], ['I', 'V'], ['T', 'S'], ['V', 'R'], ['S', 'D'], ['R', 'C'], ['D', 'E'], ['F', 'N'], ['W', 'A'], ['G', 'L'], ['L', 'I'], ['I', 'T'], ['T', 'V'], ['V', 'S'], ['S', 'R'], ['R', 'D'], ['D', 'C'], ['C', 'E'], ['W', 'N'], ];

            $this->data['orders'] = $this->order->asArray()
                ->where('order.id', $id_order)
                ->where('order.status', 'paid-confirm')
                ->where('order.deleted', 0)
                ->orderBy('order.id', 'DESC')
                ->findAll();

            foreach($this->data['orders'] as $key => $order){
                
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
                ->where('memberorderproduct.id', $id_memberorderproduct)
                ->where('memberorderproduct.deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();

                $this->data['orders'][$key]['member'] = $this->user->asArray()
                ->select('users.*')
                ->where('id', $this->data['orders'][0]['id_member'])
                // ->where('active', 0)
                // ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {

                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.id_jenistes, memberorderproductjenistes.id, memberorderproductjenistes.start-test, memberorderproductjenistes.done-test, memberorderproductjenistes.summary, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();

                    foreach ($this->data['orders'][$key]['products'][$key2]['jenistes'] as $key3 => $jenistes) { 
                         
;                        $this->data['jenistes'][$key3]['bagiantes'] = $this->bagiantes->asArray()
                            ->where('id_jenistes', $jenistes['id_jenistes'])
                            ->where('deleted', 0)
                            ->orderBy('urutan', 'ASC')
                            ->findAll();

                            // d($this->data['jenistes'][$key3]['bagiantes']);

                            foreach ($this->data['jenistes'][$key3]['bagiantes'] as $key4 => $bagiantes) {

                                // dd($bagiantes);
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['soal'] = $this->soaltes->asArray()
                                    ->select('id, urutan, soal')
                                    ->where('id_jenistes', $bagiantes['id_jenistes'])
                                    ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                    ->where('deleted', 0)
                                    ->orderBy('urutan', 'ASC')
                                    ->findAll();

                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['skor'] = $this->jawabanmember->asArray()
                                    ->select('max(jawabanmember.score) as score')
                                    ->where('id_memberorderproductjenistes', $jenistes['id'])
                                    ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                    ->where('deleted', 0)
                                    ->groupBy('jawabanmember.urutan_soal')
                                    ->findAll();

                                $skor=0;
                                foreach($this->data['jenistes'][$key3]['bagiantes'][$key4]['skor'] as $score){
                                    $skor = $skor + $score['score']; 
                                }
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['skor'][0]['score'] = $skor;

                                // jika ini jenis tes 2 maka cari total untuk RIASEC
                                if($bagiantes['id_jenistes']==2){
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['riasec'] = $this->jawabanmember->asArray()
                                    ->select('jawabanmember.sub_bagian as sub_bagian,  SUM(jawabanmember.score) as score')
                                    ->where('jawabanmember.id_memberorderproductjenistes', $jenistes['id'])
                                    ->where('jawabanmember.urutan_bagian_tes', $bagiantes['urutan'])
                                    ->where('jawabanmember.deleted', 0)
                                    ->whereIn('jawabanmember.sub_bagian', ['1', '2', '3', '4', '5', '6'])
                                    ->groupBy('jawabanmember.sub_bagian')
                                    // ->join('soaltes', 'jawabanmember.sub_bagian=soaltes.sub_bagian', 'left')
                                    ->findAll();
                                // d($this->data['jenistes'][$key3]['bagiantes'][$key4]['riasec']);
                                }
                                if($bagiantes['id_jenistes']==3){
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['N'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['G'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['A'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['L'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['P'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['I'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['T'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['V'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['X'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['S'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['B'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['O'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['R'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['D'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['C'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['Z'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['E'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['K'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['F'] = 0;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['W'] = 0;
                                }
                                // d( $this->data['jenistes'][$key3]['bagiantes'][$key4]['judul']);
                                // d( $this->data['jenistes'][$key3]['bagiantes'][$key4]['skor'][0]['score']);
                                // dd($this->data['jenistes'][$key3]['bagiantes'][$key4]['skor']);
                                $temp_score = 0;
                                if ($this->data['jenistes'][$key3]['bagiantes'][$key4]['skor']){
                                    $temp_score = $this->data['jenistes'][$key3]['bagiantes'][$key4]['skor'][0]['score'];
                                    if($temp_score==null) $temp_score = 0;
                                }

                                $birthdate = DateTime::createFromFormat('Y-m-d', $this->data['orders'][$key]['member'][0]['birthdate']);
                                $today = DateTime::createFromFormat('Y-m-d H:i:s', $this->data['orders'][$key]['products'][$key2]['jenistes'][$key3]['start-test']);
                                // dd($birthdate);
                                if ($birthdate > $today || $birthdate == false) { 
                                  $y = 0;
                                } else {
                                  // $diff = date_diff($today, $birthdate);
                                  $y = $today->diff($birthdate)->y;
                                }
                                $umur = $y;
                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['konversi'][0] = $this->konversi_tabel($umur, $bagiantes['id_jenistes'], $bagiantes['urutan'], $temp_score);
                                // d($temp_score);
                                // d($this->data['jenistes'][$key3]['bagiantes'][$key4]['konversi'][0]);
                                // dd($this->data['jenistes'][$key3]['bagiantes'][$key4]);
                                    
                                    foreach ($this->data['jenistes'][$key3]['bagiantes'][$key4]['soal'] as $key5 => $soal) {
                                        if(($bagiantes['id_jenistes']==1 && $bagiantes['urutan']==4) || $bagiantes['id_jenistes']==3){
                                        $this->data['jenistes'][$key3]['bagiantes'][$key4]['soal'][$key5]['pilihanjawaban'] = $this->pilihanjawaban->asArray()
                                            ->where('id_jenistes', $bagiantes['id_jenistes'])
                                            ->where('urutan_bagian_tes', 4)
                                            ->where('urutan_soal', $soal['urutan'])
                                            ->where('deleted', 0)
                                            ->orderBy('urutan', 'ASC')
                                            ->findAll();

                                            // data jawaban member jika ada
                                            $this->data['jawabanmember'] = $this->jawabanmember->asArray()
                                                ->where('id_memberorderproductjenistes', $jenistes['id'])
                                                ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                                ->where('urutan_soal', $soal['urutan'])
                                                ->orderBy('id', 'ASC')
                                                ->findAll();
                                            // dd($this->data['jawabanmember']);
                                            if($this->data['jawabanmember']){
                                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['soal'][$key5]['jawabanmember'] = $this->data['jawabanmember'];
                                            } else {
                                                $this->data['jenistes'][$key3]['bagiantes'][$key4]['soal'][$key5]['jawabanmember'] = NULL;
                                            }
                                            // dd($this->data['jenistes'][$key3]['bagiantes'][$key4]['soal']);
                                            // score papikostick
                                            if($bagiantes['id_jenistes']==3){
                                                if ($this->data['jenistes'][$key3]['bagiantes'][$key4]['soal'][$key5]['jawabanmember']){
                                                    $nomorsoal = $this->data['jenistes'][$key3]['bagiantes'][$key4]['soal'][$key5]['jawabanmember'][0]['urutan_soal']-1;
                                                    if (!empty($this->data['jenistes'][$key3]['bagiantes'][$key4]['soal'][$key5]['jawabanmember'][0]['urutan_jawaban'])){
                                                        $urutanjawaban = $this->data['jenistes'][$key3]['bagiantes'][$key4]['soal'][$key5]['jawabanmember'][0]['urutan_jawaban']-1;
                                                        $pencocok = $jawaban_pencocok[$nomorsoal][$urutanjawaban];
                                                        $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick'][$pencocok] = $this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick'][$pencocok] + 1; 
                                                    } 
                                                } 
                                            }
                                        }
                                    }
                                        // insert $id_memberorderproductjenistes
                                        // urutan bagian tes
                                        // $bagiantes['urutan']
                                        // urutan soal
                                        // $soal['urutan']

                            }
                    }

                }
            }

            // dd($this->data['jenistes']);
            // d($this->data['jenistes'][$key3]['bagiantes'][$key4]['soal']);
            
            // dd($this->data['jenistes'][$key3]['bagiantes'][$key4]['papikostick']['G']);
            if($this->ionAuth->inGroup('psychologists')) {
                $this->data['scope'] = __FUNCTION__;
                $this->data['template'] = 'psychologist/'.__FUNCTION__.'_detail';
                return view('psychologist/index', $this->data);
            
            } else if($this->ionAuth->inGroup('admin')) {
                $this->data['scope'] = 'all';
                // $this->data['template'] = 'psychologist/'.__FUNCTION__.'_detail';
                $this->data['template'] = 'admin/product_detail';
                return view('admin/index', $this->data);
            }
        }
        }
    }

    public function konversi_tabel($umur, $jenistes, $bagiantes, $skor) {
        $code = [
            'SE',
            'WA',
            'AN',
            'GE',
            'RA',
            'ZR',
            'FA',
            'WU',
            'ME',
        ];
        // $umur=18;
        if($umur >=12 && $umur <=30){
            if($skor>20) $skor = 20;
            $sw = $this->iqcalc->asArray()
                ->select('sw')
                ->where('umur', $umur)
                ->where('code', $bagiantes)
                ->where('skor', $skor)
                ->findAll();
            if ($jenistes==1){
                $konversi['code'] = $code[$bagiantes-1];
                
                if (!empty($sw)){
                    $konversi['sw'] = $sw[0]['sw'];
                    $konversi['klasifikasi'] = $this->konversi($jenistes, $bagiantes, $skor)['klasifikasi'];
                } else {
                    $konversi['sw'] = '';
                    $konversi['klasifikasi'] = '';
                }
                return $konversi;
            } else {
                return 0;
            }
        } else {
            return $this->konversi($jenistes, $bagiantes, $skor);
        }

    }
    
    public function konversi($jenistes, $bagiantes, $skor) {
        $code = [
            'SE',
            'WA',
            'AN',
            'GE',
            'RA',
            'ZR',
            'FA',
            'WU',
            'ME',
        ];
        $sw['1'] = [
            '60',
            '65',
            '70',
            '75',
            '80',
            '85',
            '90',
            '95',
            '100',
            '105',
            '110',
            '115',
            '120',
            '125',
            '130',
            '135',
            '140',
            '145',
            '150',
            '155',
            '160',
        ];
        $klasifikasi['1'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'III -',
            'III',
            'III',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
        ];
        $sw['2'] = [
            '35',
            '40',
            '45',
            '50',
            '55',
            '60',
            '65',
            '70',
            '75',
            '80',
            '85',
            '90',
            '95',
            '100',
            '105',
            '110',
            '115',
            '120',
            '125',
            '130',
            '135',
        ];
        $klasifikasi['2'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'IV',
            'IV',
            'III -',
            'III',
            'III',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
            'I',
        ];
        $sw['3'] = [
            '40',
            '46',
            '52',
            '58',
            '64',
            '70',
            '76',
            '82',
            '88',
            '94',
            '100',
            '106',
            '112',
            '118',
            '124',
            '130',
            '136',
            '142',
            '148',
            '154',
            '160',
        ];
        $klasifikasi['3'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'IV',
            'III -',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
        ];
        $sw['4'] = [
            '57',
            '60',
            '63',
            '64',
            '67',
            '70',
            '73',
            '76',
            '79',
            '81',
            '84',
            '87',
            '90',
            '93',
            '96',
            '97',
            '100',
            '103',
            '106',
            '109',
            '111',
            '114',
            '117',
            '120',
            '123',
            '126',
            '129',
            '131',
            '134',
            '137',
            '140',
            '143',
            '146',
        ];
        $klasifikasi['4'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'IV',
            'IV',
            'IV',
            'IV',
            'IV',
            'III -',
            'III -',
            'III -',
            'III -',
            'III',
            'III',
            'III',
            'III',
            'III',
            'III',
            'III',
            'III +',
            'III +',
            'III +',
            'II',
            'II',
            'II',
            'II',
            'II',
            'II',
            'I',
            'I',
        ];
        $sw['9'] = [
            '52',
            '56',
            '60',
            '64',
            '68',
            '72',
            '76',
            '80',
            '84',
            '88',
            '92',
            '96',
            '100',
            '104',
            '108',
            '112',
            '116',
            '120',
            '124',
            '128',
            '132',
        ];
        $klasifikasi['9'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'III -',
            'III',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
        ];
        $sw['5'] = [
            '60',
            '65',
            '70',
            '75',
            '80',
            '85',
            '90',
            '95',
            '100',
            '105',
            '110',
            '115',
            '120',
            '125',
            '130',
            '135',
            '140',
            '145',
            '150',
            '155',
            '160',
        ];
        $klasifikasi['5'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'III -',
            'III',
            'III +',
            'II',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
        ];
        $sw['6'] = [
            '60',
            '64',
            '68',
            '72',
            '76',
            '80',
            '84',
            '88',
            '92',
            '96',
            '100',
            '104',
            '108',
            '112',
            '116',
            '120',
            '124',
            '128',
            '132',
            '136',
            '140',
        ];
        $klasifikasi['6'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'IV',
            'III -',
            'III',
            'III',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
        ];
        $sw['7'] = [
            '45',
            '50',
            '55',
            '60',
            '65',
            '70',
            '75',
            '80',
            '85',
            '90',
            '95',
            '100',
            '105',
            '110',
            '115',
            '120',
            '125',
            '130',
            '135',
            '140',
            '145',
        ];
        $klasifikasi['7'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'IV',
            'III -',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
        ];
        $sw['8'] = [
            '55',
            '60',
            '65',
            '70',
            '75',
            '80',
            '85',
            '90',
            '95',
            '100',
            '105',
            '110',
            '115',
            '120',
            '125',
            '130',
            '135',
            '140',
            '145',
            '150',
            '155',
        ];
        $klasifikasi['8'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'IV',
            'III -',
            'III',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
        ];

        if ($jenistes==1){
            $konversi['code'] = $code[$bagiantes-1];
            if (!empty($sw[$bagiantes][$skor]) && !empty($klasifikasi[$bagiantes][$skor])){
                $konversi['sw'] = $sw[$bagiantes][$skor];
                $konversi['klasifikasi'] = $klasifikasi[$bagiantes][$skor];
            } else {
                $konversi['sw'] = '';
                $konversi['klasifikasi'] = '';
            }
            return $konversi;
        }

    }


    public function correcting($id_order, $id_memberorderproduct, $id_memberorderproductjenistes)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();

        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('psychologists')) { 
            // data order
            $this->data['orders'] = $this->order->asArray()
                ->where('id', $id_order)
                ->where('status', 'paid-confirm')
                ->where('deleted', 0)
                ->orderBy('id', 'DESC')
                ->findAll();

            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
                ->where('memberorderproduct.id', $id_memberorderproduct)
                ->where('memberorderproduct.deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('memberorderproductjenistes.id', $id_memberorderproductjenistes)
                ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes')
                ->findAll();
                }


            }

            // dd($this->data['orders']);

            $id_jenistes = $this->data['orders'][0]['products'][0]['jenistes'][0]['id_jenistes'];


            // data bagian tes
            $this->data['bagiantes'] = $this->bagiantes->asArray()
                ->where('id_jenistes', $id_jenistes)
                ->where('deleted', 0)
                ->orderBy('urutan', 'ASC')
                ->findAll();

                foreach ($this->data['bagiantes'] as $key => $bagiantes) {
                    // dd($bagiantes);
                    $this->data['bagiantes'][$key]['soal'] = $this->soaltes->asArray()
                        ->where('id_jenistes', $bagiantes['id_jenistes'])
                        ->where('urutan_bagian_tes', $bagiantes['urutan'])
                        ->where('deleted', 0)
                        ->orderBy('urutan', 'ASC')
                        ->findAll();

                    $this->data['bagiantes'][$key]['skor'] = $this->jawabanmember->asArray()
                        ->select('SUM(jawabanmember.score) as score')
                        ->where('id_memberorderproductjenistes', $id_memberorderproductjenistes)
                        ->where('urutan_bagian_tes', $bagiantes['urutan'])
                        ->where('deleted', 0)
                        ->groupBy('jawabanmember.urutan_bagian_tes')
                        ->findAll();


                        foreach ($this->data['bagiantes'][$key]['soal'] as $key2 => $soal) {
                            $this->data['bagiantes'][$key]['soal'][$key2]['pilihanjawaban'] = $this->pilihanjawaban->asArray()
                                ->where('id_jenistes', $bagiantes['id_jenistes'])
                                ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                ->where('urutan_soal', $soal['urutan'])
                                ->where('deleted', 0)
                                ->orderBy('urutan', 'ASC')
                                ->findAll();



                                // data jawaban member jika ada
                                $this->data['jawabanmember'] = $this->jawabanmember->asArray()
                                    ->where('id_memberorderproductjenistes', $id_memberorderproductjenistes)
                                    ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                    ->where('urutan_soal', $soal['urutan'])
                                    ->orderBy('id', 'ASC')
                                    ->findAll();

                                if($this->data['jawabanmember']){
                                    $this->data['bagiantes'][$key]['soal'][$key2]['jawabanmember'] = $this->data['jawabanmember'];
                                }
                                }
                            // insert $id_memberorderproductjenistes
                            // urutan bagian tes
                            // $bagiantes['urutan']
                            // urutan soal
                            // $soal['urutan']

                }

            // data jawaban user 
            

            // dd($this->data['bagiantes']); 

            $this->data['scope'] = 'correction';
            $this->data['template'] = 'psychologist/'.__FUNCTION__;
            return view('psychologist/index', $this->data);
        } else {
            return redirect()->to('/');
        }
    }

    public function save_summary() 
    {
            $id_psychologist = $_SESSION['user_id'];
            $chart = '';
            if (isset($_POST['chart'])){
                $chart = $_POST['chart'];
            }
            $response = $this->memberorderproductjenistes
            ->set([
                'chart' => $chart,
                'summary' => $_POST['summary'],
                'iq' => $_POST['iq'],
                'tingkat_kecerdasan' => $_POST['tingkat_kecerdasan'],
                // 'summary_time' =>  date("Y-m-d H:i:s"),
                'id_psikolog' =>  $id_psychologist,
            ])        
            ->where(['id' => $_POST['id_memberorderproductjenistes']
                // , 'urutan_bagian_tes' => $_POST['bagian_tes'], 'urutan_soal' => $_POST['urutan_soal']
            ])
            ->update(); 

            echo $response; 
    }

    public function save_finalsummary() 
    {
        
            $id_psychologist = $_SESSION['user_id'];
            $response = $this->memberorderproduct
            ->set([
                'summary' => $_POST['summary'],
                'jurusan1' => $_POST['jurusan1'],
              'jurusan2' => $_POST['jurusan2'],
              'jurusan3' => $_POST['jurusan3'],
              'jurusan4' => $_POST['jurusan4'],
              'jurusan5' => $_POST['jurusan5'],
              'descjurusan1' => $_POST['descjurusan1'],
              'descjurusan2' => $_POST['descjurusan2'],
              'descjurusan3' => $_POST['descjurusan3'],
              'descjurusan4' => $_POST['descjurusan4'],
              'descjurusan5' => $_POST['descjurusan5'],
                // 'summary_time' =>  date("Y-m-d H:i:s"),
            ])        
            ->where(['id' => $_POST['id_memberorderproduct']
                // , 'urutan_bagian_tes' => $_POST['bagian_tes'], 'urutan_soal' => $_POST['urutan_soal']
            ])
            ->update(); 

            echo $response; 
    }

    public function save_chart() {
            $response = $this->memberorderproductjenistes
            ->set([
                'chart' => $_POST['chart'],
            ])        
            ->where(['id' => $_POST['id_memberorderproductjenistes']
            ])
            ->update(); 
            echo $response; 
    }

    public function save_finalchart() {
            $response = $this->memberorderproduct
            ->set([
                'chart' => $_POST['chart'],
            ])        
            ->where(['id' => $_POST['id_memberorderproduct']
            ])
            ->update(); 
            echo $response; 
    }

    // simpan skor setiap kali menjawab
    public function save_correction() {

            $id_psychologist = $_SESSION['user_id'];

            $response = $this->jawabanmember
            ->set([
                'score' => $_POST['score'],
                'score_time' =>  date("Y-m-d H:i:s"),
                'scored_by' =>  $id_psychologist,
            ])        
            ->where(['id_memberorderproductjenistes' => $_POST['id_memberorderproductjenistes'], 'urutan_bagian_tes' => $_POST['bagian_tes'], 'urutan_soal' => $_POST['urutan_soal']])
            ->update(); 

            echo $response; 
    }

    public function save_score_all() {
        $id_psychologist = $_SESSION['user_id'];


        $id_order =  $_POST['id_order'];
        $id_memberorderproduct =  $_POST['id_memberorderproduct'];
        $id_memberorderproductjenistes =  $_POST['id_memberorderproductjenistes'];
        unset($_POST['id_order']);
        unset($_POST['id_memberorderproduct']);
        unset($_POST['id_memberorderproductjenistes']);
        unset($_POST['submit']);
        foreach($_POST as $key => $score){
            $this->jawabanmember     
            ->set([
                'score' => $score,
                'score_time' =>  date("Y-m-d H:i:s"),
                'scored_by' =>  $id_psychologist,
            ])        
            ->where(['id_memberorderproductjenistes' => $id_memberorderproductjenistes, 'urutan_bagian_tes' => 4, 'urutan_soal' => $key])
            ->update(); 
        }
        return redirect()->to('/test/correction/null/'.$id_order.'/'.$id_memberorderproduct); 
    }
 
}