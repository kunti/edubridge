<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="EduBridge">
  <meta name="author" content="EduBridge">
  <title>PIPE Psikotest</title>
  <!-- Favicon -->
  <link rel="icon" href="<?php echo base_url('src/assets/img/brand/favicon.png" type="image/png'); ?>">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/nucleo/css/nucleo.css'); ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css'); ?>" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/css/argon.css?v=1.2.0'); ?>" type="text/css">
  <!-- Datatable -->
  <link rel="stylesheet" href="<?php echo base_url('src/node_modules/mdbootstrap/css/addons/datatables2.min.css'); ?>" type="text/css">

  <style>
@media (max-width: 768px) { /* use the max to specify at each container level */
    .table-descriprion {    
        width:360px;  /* adjust to desired wrapping */
        display:table;
        white-space: pre-wrap; /* css-3 */
        white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
        white-space: -pre-wrap; /* Opera 4-6 */
        white-space: -o-pre-wrap; /* Opera 7 */
        word-wrap: break-word; /* Internet Explorer 5.5+ */
    }
}
</style>



</head>

<body>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Search form -->
          <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
            <div class="form-group mb-0">
              <div class="input-group input-group-alternative input-group-merge">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-search"></i></span>
                </div>
                <input class="form-control" placeholder="Search" type="text">
              </div>
            </div>
            <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </form>
          <!-- Navbar links -->
          <ul class="navbar-nav align-items-center  ml-md-auto ">
            <li class="nav-item d-xl-none">
              <!-- Sidenav toggler -->
              <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </div>
            </li>
            <li class="nav-item d-sm-none">
              <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                <i class="ni ni-zoom-split-in"></i>
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-bell-55"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
                <!-- Dropdown header -->
                <div class="px-3 py-3">
                  <h6 class="text-sm text-muted m-0">You have <strong class="text-primary">13</strong> notifications.</h6>
                </div>
                <!-- List group -->
                <div class="list-group list-group-flush">
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <!-- Avatar -->
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/theme/team-1.jpg'); ?>" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">John Snow</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small>2 hrs ago</small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">Let's meet at Starbucks at 11:30. Wdyt?</p>
                      </div>
                    </div>
                  </a>
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <!-- Avatar -->
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/theme/team-2.jpg'); ?>" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">John Snow</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small>3 hrs ago</small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">A new issue has been reported for Argon.</p>
                      </div>
                    </div>
                  </a>
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <!-- Avatar -->
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/theme/team-3.jpg'); ?>" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">John Snow</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small>5 hrs ago</small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">Your posts have been liked a lot.</p>
                      </div>
                    </div>
                  </a>
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <!-- Avatar -->
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">John Snow</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small>2 hrs ago</small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">Let's meet at Starbucks at 11:30. Wdyt?</p>
                      </div>
                    </div>
                  </a>
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <!-- Avatar -->
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/theme/team-5.jpg'); ?>" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">John Snow</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small>3 hrs ago</small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">A new issue has been reported for Argon.</p>
                      </div>
                    </div>
                  </a>
                </div>
                <!-- View all -->
                <a href="#!" class="dropdown-item text-center text-primary font-weight-bold py-3">View all</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-ungroup"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-dark bg-default  dropdown-menu-right ">
                <div class="row shortcuts px-4">
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-red">
                      <i class="ni ni-calendar-grid-58"></i>
                    </span>
                    <small>Calendar</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-orange">
                      <i class="ni ni-email-83"></i>
                    </span>
                    <small>Email</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-info">
                      <i class="ni ni-credit-card"></i>
                    </span>
                    <small>Payments</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-green">
                      <i class="ni ni-books"></i>
                    </span>
                    <small>Reports</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-purple">
                      <i class="ni ni-pin-3"></i>
                    </span>
                    <small>Maps</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-yellow">
                      <i class="ni ni-basket"></i>
                    </span>
                    <small>Shop</small>
                  </a>
                </div>
              </div>
            </li>
          </ul>
          <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
            <li class="nav-item dropdown">
              <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
                  </span>
                  <div class="media-body  ml-2  d-none d-lg-block">
                    <span class="mb-0 text-sm  font-weight-bold">John Snow</span>
                  </div>
                </div>
              </a>
              <div class="dropdown-menu  dropdown-menu-right ">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0">Welcome!</h6>
                </div>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-single-02"></i>
                  <span>My profile</span>
                </a>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-settings-gear-65"></i>
                  <span>Settings</span>
                </a>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-calendar-grid-58"></i>
                  <span>Activity</span>
                </a>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-support-16"></i>
                  <span>Support</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-user-run"></i>
                  <span>Logout</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Main Content -->
    <div>
        <div class="header bg-primary pb-6">
          <div class="container-fluid">
            <div class="header-body">
              <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                  <h6 class="h2 text-white d-inline-block mb-0">Tes Kecerdasan</h6>
                  <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                      <li>Order ID : <b>EB85890</b></li>
                    </ol>
                  </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                  <a href="#" class="btn btn-lg btn-neutral">Keluar dari Test</a>
                  <!-- <a href="#" class="btn btn-lg btn-neutral">Filters</a> -->
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Page content -->
        <div class="container-fluid mt--6">
          <div class="row">
            <div class="col">
              <div class="card">
                <!-- Card header -->
                <div class="card-header border-0 text-center">
                  <h3 class="mb-0">Bagian 1</h3>
                </div>
                <div class="container">
                  <div class="row">
                    <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-danger">Soal 01-20</a></div>
                    <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-danger">Soal 01-20</a></div>
                    <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-danger">Soal 01-20</a></div>
                    <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">Soal 01-20</a></div>
                    <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">Soal 01-20</a></div>
                    <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">Soal 01-20</a></div>
                    <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">Soal 01-20</a></div>
                    <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">Soal 01-20</a></div>
                    <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">Soal 01-20</a></div>
                    <!-- <div class="col" style="margin-bottom: 5px"><span><button type="button" class="btn btn-secondary">Soal 01-20</button></span></div> -->
                  </div>

                  <form>
                    <div class="pl-lg-4"> 
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="text-center col-md-12">
                            <div class="card-header border-0 text-center">
                              <h1 class="mb-0">9. Seorang yang bersikap menyangsikan setiap kemajuan ialah seorang yang</h1>
                            </div>
                            <div class="text-center">
                              <div class="col-sm-12">
                                <img src="<?php echo base_url('src/assets/img/question/117.PNG'); ?>" class="rounded img-fluid" alt="..." style="height: 200px;">
                              </div>
                              <!-- radio button -->
                              <div>
                                <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                  <input name="custom-radio-1" class="custom-control-input" id="customRadio1" type="radio">
                                  <label class="custom-control-label" for="customRadio1">Demokratis</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                  <input name="custom-radio-1" class="custom-control-input" id="customRadio2" checked="" type="radio">
                                  <label class="custom-control-label" for="customRadio2">Radikal</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                  <input name="custom-radio-1" class="custom-control-input" id="customRadio3" checked="" type="radio">
                                  <label class="custom-control-label" for="customRadio3">Liberal</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                  <input name="custom-radio-1" class="custom-control-input" id="customRadio4" checked="" type="radio">
                                  <label class="custom-control-label" for="customRadio4">Konservatif</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                  <input name="custom-radio-1" class="custom-control-input" id="customRadio5" checked="" type="radio">
                                  <label class="custom-control-label" for="customRadio5">Anarkis</label>
                                </div>
                              </div>
                              <div>
                                <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                  <input class="custom-control-input" id="customCheck1" type="checkbox">
                                  <label class="custom-control-label" for="customCheck1">1</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                  <input class="custom-control-input" id="customCheck2" type="checkbox" checked>
                                  <label class="custom-control-label" for="customCheck2">2</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                  <input class="custom-control-input" id="customCheck3" type="checkbox" checked>
                                  <label class="custom-control-label" for="customCheck3">3</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                  <input class="custom-control-input" id="customCheck4" type="checkbox" checked>
                                  <label class="custom-control-label" for="customCheck4">4</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                  <input class="custom-control-input" id="customCheck5" type="checkbox">
                                  <label class="custom-control-label" for="customCheck5">5</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                  <input class="custom-control-input" id="customCheck6" type="checkbox" checked>
                                  <label class="custom-control-label" for="customCheck6">6</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                  <input class="custom-control-input" id="customCheck7" type="checkbox" checked>
                                  <label class="custom-control-label" for="customCheck7">7</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                  <input class="custom-control-input" id="customCheck8" type="checkbox" checked>
                                  <label class="custom-control-label" for="customCheck8">8</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                  <input class="custom-control-input" id="customCheck9" type="checkbox" checked>
                                  <label class="custom-control-label" for="customCheck9">9</label>
                                </div>
                                <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                  <input class="custom-control-input" id="customCheck10" type="checkbox" checked>
                                  <label class="custom-control-label" for="customCheck10">0</label>
                                </div>
                              </div>  
                              <div class="form-group col-sm-12">
                                <input type="text" id="isian" class="form-control" placeholder="Jawaban saya" value="">
                              </div>
                            </div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-sm btn-primary">Petunjuk dan Contoh soal</a></div>
                            <div class="row">
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-success">1</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-success">2</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-success">3</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">4</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">5</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">6</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">7</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">8</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">9</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">10</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">11</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">12</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">13</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">14</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">15</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">16</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">17</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">18</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">19</a></div>
                              <div class="col" style="margin-bottom: 5px; padding:2px;"><span><a href="#!" class="btn btn-lg btn-secondary">20</a></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>

                </div>
                <!-- Card footer -->
                <div class="card-footer py-4">
                  <p class="description opacity-8">Perhatian, Anda harus menjawab dengan cepat dan teliti</p>
                </div>
              </div>
            </div>
          </div>
        </div>

    </div>
    
      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
              &copy; <?php echo date("Y"); ?> <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">EduBridge</a>
            </div>
          </div>
         <!--  <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">EduBridge</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
              </li>
            </ul>
          </div> -->
        </div>
      </footer>
    </div>
  </div>
  <!-- Vue js -->
  <script src="https://vuejs.org/js/vue.min.js"></script>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="<?php echo base_url('src/assets/vendor/jquery/dist/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/js-cookie/js.cookie.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js'); ?>"></script>
  <!-- Optional JS -->
  <script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.extension.js'); ?>"></script>
  <!-- Argon JS -->
  <script src="<?php echo base_url('src/assets/js/argon.js?v=1.2.0'); ?>"></script>
  <!-- Datatable -->
  <script src="<?php echo base_url('src/node_modules/mdbootstrap/js/addons/datatables2.min.js'); ?>" type="text/javascript"></script>
  <script type="text/javascript">
  $(document).ready(function () {
  $('#dtBasicExample').DataTable();
  $('.dataTables_length').addClass('bs-select');
});
</script>




</body>

</html> 