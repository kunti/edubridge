<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <!-- <h6 class="h2 text-white d-inline-block mb-0">Order</h6> -->
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="<?= base_url('member/orderproduct'); ?>"><?= lang('Global.my_test') ?></a></li>
              <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.test_detail') ?></li>
            </ol>
          </nav>
        </div>
        <!-- <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0"><?= lang('Global.test_detail') ?></h3>
        </div>
        <div class="card-header border-0">
          <sm><a href="">Order code : <?= 'PIPETEST'.sprintf('%04d', $orders[0]['id']) ?></a></sm>
          <h3><?= $orders[0]['products'][0]['nama'] ?></h3>
          <span class="text-sm"><?= $orders[0]['products'][0]['desc'] ?></span>
          <br>
          Status : 
          <?php 
            if($orders[0]['products'][0]['start-summarization']=='0000-00-00 00:00:00'){
              echo "Belum ada summary";
            } else if ($orders[0]['products'][0]['status']=='start-summarization'){
              echo "Sedang summary, menunggu hasil";
            } else if ($orders[0]['products'][0]['status']=='summarized'){
              ?>
              <a href="<?php echo base_url('pdf/mpdf/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id'].'/D');?>" class="btn btn-sm btn-success">Download <?= lang('Global.result') ?></a>
              <?php
            } else {

            }
          ?>
        </div>
        <!-- Light table -->
          <div class="table-responsive">
            <table id="dtBasicExample" class="table" width="100%" >
              <thead class="thead-light">
                <tr>
                  <th class="th-sm"><?= lang('Global.test_package') ?>
                  </th>
                  <!-- <th class="th-sm"><?= lang('Global.test_working') ?>
                  </th> -->
                  <!-- <th class="th-sm"><?= lang('Global.correction') ?>
                  </th> -->
                  <!-- <th class="th-sm"><?= lang('Global.action') ?>
                  </th> -->
                </tr>
              </thead>
              <tbody class="list">
                <?php 
                if (!empty($orders[0]['products'])) {
                  foreach ($orders[0]['products'] as $key => $product) {
                    if(!empty($product)){
                      foreach ($product['jenistes'] as $key => $jenistes) {
                    ?>
                    <tr>
                      <td>
                        <h5><?= $jenistes['nama'] ?>
                         <?php 
                              if($jenistes['start-test']=='0000-00-00 00:00:00'){
                                ?>
                                  <span class="badge badge-primary mr-4">
                                    <span class="status"><?= lang('Global.ready_test') ?></span>
                                  </span>
                                  <br>
                                  <p style="font-size: 12px;">Tes bisa dimulai sejak : 
                                  <?php
                                  setlocale(LC_ALL, 'id-ID', 'id_ID');
                                  echo strftime("%A, %d %B %Y", strtotime($orders[0]['paid-confirm'])).', '.
                                  date('h:i:s A', strtotime($orders[0]['paid-confirm'])) 
                                  ?>
                                  </p>
                                <?php
                              } else if($jenistes['status']=='start-test'){
                                ?>
                                  <span class="badge badge-warning mr-4">
                                    <span class="status"><?= lang('Global.on_test') ?></span>
                                  </span>
                                  <p style="font-size: 12px;">
                                  Tes bisa dimulai sejak : 
                                  <?php
                                  setlocale(LC_ALL, 'id-ID', 'id_ID');
                                  echo strftime("%A, %d %B %Y", strtotime($orders[0]['paid-confirm'])).', '.
                                  date('h:i:s A', strtotime($orders[0]['paid-confirm'])) 
                                  ?>
                                  <br>
                                  Anda telah memulai tes pada : 
                                  <?php
                                  setlocale(LC_ALL, 'id-ID', 'id_ID');
                                  echo strftime("%A, %d %B %Y", strtotime($jenistes['start-test'])).', '.
                                  date('h:i:s A', strtotime($jenistes['start-test'])) 
                                  ?>
                                  </p>
                                <?php
                              } else if($jenistes['done-test']!='0000-00-00 00:00:00' || $jenistes['status']=='done-test'){
                                ?>
                                  <span class="badge badge-success mr-4">
                                    <span class="status"><?= lang('Global.done') ?></span>
                                  </span>
                                  <br>
                                  <?php
                                    $date1 = new DateTime($jenistes['start-test']);
                                    $date2 = new DateTime($jenistes['done-test']); 
                                    echo 'Anda menyelesaikan tes dalam : '; 
                                    if($date2->diff($date1)->d!=0){
                                      echo $date2->diff($date1)->d.' hari ';
                                    }
                                    echo $date2->diff($date1)->h.' jam '; 
                                    echo $date2->diff($date1)->i.' menit '; 
                                    echo $date2->diff($date1)->s.' detik '; 
                                    ?>
                                  <br>
                                  <p style="font-size: 12px;">
                                  Tes bisa dimulai sejak : 
                                  <?php
                                  setlocale(LC_ALL, 'id-ID', 'id_ID');
                                  echo strftime("%A, %d %B %Y", strtotime($orders[0]['paid-confirm'])).', '.
                                  date('h:i:s A', strtotime($orders[0]['paid-confirm'])) 
                                  ?>
                                  <br>
                                  Anda telah memulai tes pada : 
                                  <?php
                                  setlocale(LC_ALL, 'id-ID', 'id_ID');
                                  echo strftime("%A, %d %B %Y", strtotime($jenistes['start-test'])).', '.
                                  date('h:i:s A', strtotime($jenistes['start-test'])) 
                                  ?>
                                  <br>
                                  Anda telah menyelesaikan tes pada : 
                                  <?php
                                  setlocale(LC_ALL, 'id-ID', 'id_ID');
                                  echo strftime("%A, %d %B %Y", strtotime($jenistes['done-test'])).', '.
                                  date('h:i:s A', strtotime($jenistes['done-test'])) 
                                  ?>
                                <?php
                              } 
                            ?>
                            </h5>
                            <span class="text-wrap"><?= $jenistes['desc'] ?></span>
                                <br>
                            <div class="text-right" style="padding-top: 24px">
                                <?php
                              if($jenistes['start-test']=='0000-00-00 00:00:00' && $jenistes['status']==""){
                                ?>
                                <a href="<?php echo base_url('test/jenistes/'.$orders[0]['id'].'/'.$product['id'].'/'.$jenistes['id']);?>" class="btn btn-sm btn-primary"><?= lang('Global.take_test') ?></a>
                                <?php
                              } else if($jenistes['status']=='ulangi'){
                                ?>
                                <a href="<?php echo base_url('test/jenistes/'.$orders[0]['id'].'/'.$product['id'].'/'.$jenistes['id']);?>" class="btn btn-sm btn-primary"><?= lang('Global.lanjutkan') ?></a>
                                <?php
                              }
                              else if($jenistes['status']=='done-test' || $jenistes['status']=='start-correction'){
                                ?>
                                <span><?= lang('Global.psikolog_menganalisa') ?></span><br>
                                <a href="#" class="btn btn-sm btn-light" disabled><?= lang('Global.take_test') ?></a>
                                <!-- <a href="<?php echo base_url('test/jenistes/'.$orders[0]['id'].'/'.$product['id'].'/'.$jenistes['id']);?>" class="btn btn-sm btn-primary"><?= lang('Global.lihat_jawaban') ?></a> -->
                                <?php
                              } else if($jenistes['status']=='corrected'){
                                ?>
                                <a href="#" class="btn btn-sm btn-light" disabled><?= lang('Global.take_test') ?></a>
                                <?php
                              } 
                            ?>
                            </div>
                      </td>
                      
                    </tr>
                    <?php
                      }
                    }
                  }
                }
                ?>
              </tbody>
              <thead class="thead-light">
                <tr>
                  <th class="th-sm"><?= lang('Global.test_package') ?>
                  </th>
                  <!-- <th class="th-sm"><?= lang('Global.test_working') ?>
                  </th> -->
                  <!-- <th class="th-sm"><?= lang('Global.correction') ?>
                  </th> -->
                 <!--  <th class="th-sm"><?= lang('Global.action') ?>
                  </th> -->
                </tr>
              </tfoot>
            </table>
          </div>
        <!-- Card footer -->
        <!-- <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end mb-0">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <i class="fas fa-angle-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">
                  <i class="fas fa-angle-right"></i>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>
