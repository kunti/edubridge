<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <!-- <h6 class="h2 text-white d-inline-block mb-0">Riwayat Pembelian</h6> -->
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
              <!-- <li class="breadcrumb-item"><a href="#">Peserta</a></li> -->
              <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.purchase') ?></li>
            </ol>
          </nav>
        </div>
        <!-- <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0"><?= lang('Global.purchase') ?></h3>
        </div>
        <!-- Light table -->
          <div class="table-responsive">
            <table id="dtBasicExample" class="table" >
              <thead class="thead-light">
                <tr>
                  <th class="th-sm"><?= lang('Global.order_code') ?>
                  </th>
                  <!-- <th class="th-sm"><?= lang('Global.test_package') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.total_harga') ?>
                  </th> -->
                  <th class="th-sm"><?= lang('Global.payment') ?>
                  </th>
                  <!-- <th class="th-sm"><?= lang('Global.action') ?>
                  </th> -->
                </tr>
              </thead>
              <tbody class="list">
                <?php 
                if (!empty($orders)) {
                  // dd($orders);
                  foreach ($orders as $key => $order) {
                    ?>
                    <tr>
                      <td>
                        <span class="code" style="font-size: 14px;"><?= 'PIPETEST'.sprintf('%04d', $order['id']) ?></span><br>
                        <!-- product -->
                        <?php 
                        if($profile->volunteer!=1){
                        
                          $total_harga = 0;
                            if(!empty($order['products'])){
                              foreach ($order['products'] as $key => $product) {
                                $total_harga = $total_harga + $product['harga'];
                                ?>
                                <span><b><?= $product['nama'] ?></b></span>
                                <br>
                                <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span>
                                <br>
                                <?php
                                // dd($product);
                              }
                            }
                          ?>
                          <!-- total  -->

                        <span><b>Total : Rp. <?= number_format($total_harga, 0, 0, '.'); ?></b></span><br>
                          <?php
                        }
                        ?>
                            
                        <span>Checkout : <?= $order['checkout'] ?></span>
                      </td>
                      <th scope="row">
                        <div class="media align-items-center">
                          <a target="_blank" href="<?= base_url('public/uploads/payment/'.$order['bukti_pembayaran']) ?>" class="avatar  mr-3">
                            <img alt="Image placeholder" src="<?php echo base_url('public/uploads/payment/'.$order['bukti_pembayaran']); ?>">
                          </a>
                          <div class="media-body" >
                            <span class="badge badge-dot mr-4" style="text-align:left">
                              <?php 
                                if($order['status'] == 'checkout') {
                                  echo "
                                    <span class='badge badge-primary mr-4'>
                                      <span class='status'>".lang('Global.pending')."</span>
                                    </span>
                                      <a class='btn btn-sm btn-primary' style='color:#232941'>".lang('Global.payment_method')."</a>
                                  ";
                                  // upload
                                  if($order['status']=='checkout'){
                                  $validation = \Config\Services::validation(); ?>
                                  <div style="padding-top: 12px">
                                  <form method="post" action="<?=site_url('member/fileUpload/'.$order['id'])?>" enctype="multipart/form-data">
                                     <?= csrf_field(); ?>
                                     <div class="form-group">
                                        <label for="file"><span class="text-sm"><?= lang('Global.upload_payment') ?></span></label>

                                        <input type="file" class="form-control" id="file" name="file" />
                                        <!-- Error -->
                                        <?php if( $validation->getError('file') ) {?>
                                           <div class='alert alert-danger mt-2'>
                                              <?= $validation->getError('file'); ?>
                                           </div>
                                        <?php }?>

                                     <input type="submit" class="btn btn-sm btn-primary" name="submit" value="Upload" style="margin-top: 7px">
                                     </div>
                                  </form>
                                  </div>
                                  <?php
                                  }
                                 // end of upload
                                } else if ($order['status'] == 'paid') {
                                  echo "
                                    <span class='badge badge-warning mr-4'>
                                      <span class='status'>".lang('Global.waiting_confirmation')."</span>
                                    </span>
                                      <a class='btn btn-sm btn-primary' style='color:#232941'>".lang('Global.payment_method')."</a>
                                    <br>
                                    <span style='font-size:13px'>".$order['paid']."</span>
                                  ";
                                  $validation = \Config\Services::validation(); ?>
                                  <div style="padding-top: 12px">
                                  <form method="post" action="<?=site_url('member/fileUpload/'.$order['id'])?>" enctype="multipart/form-data">
                                     <?= csrf_field(); ?>
                                     <div class="form-group">
                                        <label for="file"><span class="text-sm"><?= lang('Global.change_upload_payment') ?></span></label>

                                        <input type="file" class="form-control" id="file" name="file" />
                                        <!-- Error -->
                                        <?php if( $validation->getError('file') ) {?>
                                           <div class='alert alert-danger mt-2'>
                                              <?= $validation->getError('file'); ?>
                                           </div>
                                        <?php }?>

                                     <input type="submit" class="btn btn-sm btn-primary" name="submit" value="Upload" style="margin-top: 7px">
                                     </div>
                                  </form>
                                  </div>
                                  <?php


                                } else if ($order['status'] == 'paid-confirm') {
                                  echo "
                                    <span class='badge badge-success mr-4'>
                                      <span class='status'>".lang('Global.confirmed')."</span>
                                    </span>
                                    <br>
                                    <span style='font-size:13px'>".$order['paid-confirm']."</span>
                                  ";
                                } else {
                                  echo "
                                    <span class='badge badge-warning mr-4'>
                                      <span class='status'>".lang('Global.failed')."</span>
                                    </span>
                                  ";
                                }

                              ?>
                          </div>
                        </div>
                      </th>
                     <!--  <td class="text-left">
                        
                      </td> -->
                    </tr>
                    <?php
                  }
                }
                ?>
                
              </tbody>
              <thead class="thead-light">
                <tr>
                 <th class="th-sm"><?= lang('Global.order_code') ?>
                  </th>
                  <!-- <th class="th-sm"><?= lang('Global.test_package') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.total_harga') ?>
                  </th> -->
                  <th class="th-sm"><?= lang('Global.payment') ?>
                  </th>
                  <!-- <th class="th-sm"><?= lang('Global.action') ?>
                  </th> -->
                </tr>
              </tfoot>
            </table>
          </div>
        <!-- Card footer -->
        <!-- <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end mb-0">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <i class="fas fa-angle-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">
                  <i class="fas fa-angle-right"></i>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>
