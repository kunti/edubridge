<style type="text/css"></style>
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <!-- <h6 class="h2 text-white d-inline-block mb-0">Order</h6> -->
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
              <!-- <li class="breadcrumb-item"><a href="#">Peserta</a></li> -->
              <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.my_test') ?></li>
            </ol>
          </nav>
        </div>
        <!-- <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-primary">New</a>
          <a href="#" class="btn btn-primary">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0"><?= lang('Global.my_test') ?></h3>
          <p class="text-sm">Tes dapat Anda kerjakan satu minggu setelah Admin mengkonfirmasi pembayaran Anda</p>
        </div>
          <div>
            <?php 
                if (!empty($orders)) {
                  // dd($orders);
                  foreach ($orders as $key => $order) {
                    if(!empty($order['products'])){
                              foreach ($order['products'] as $key => $product) {
                                
                    ?>
                    <table class="table">
                      <thead class="thead" style="background-color: #f8f9fe;">
                        <tr>
                          <th class="th-sm">
                            <sm>
                              <?= lang('Global.order_code') ?> :
                              <a href="<?php echo base_url('test/product/'.$order['id'].'/'.$product['id']);?>"><?= 'PIPETEST'.sprintf('%04d', $order['id']) ?></a>
                            </sm>
                          </th>
                        </tr>
                      </thead>
                      </table>
                      <div class="card-body">
                        <h5 style="font-size: 15px"><?= $product['nama'] ?></h5>
                        <span class="text-wrap" style="font-size: 13px;"><?= $product['short_desc'] ?>.</span>
                        <br>
                        <sm style="font-size: 13px;">
                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16">
                          <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"></path>
                          <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"></path>
                          <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"></path>
                        </svg>
                         <?php 
                            $stop_date = new DateTime($order['paid-confirm']);
                            $stop_date->modify('+8 day');
                            $stop_date->setTime(0,0,0);
                            $today = new DateTime("now");
                            $birthDate = $stop_date;
                            if ($birthDate < $today) { 
                              $expired = 1;
                              echo lang('Global.hasexpired_detail')."</sm>";
                            } else {
                              $y = $today->diff($birthDate)->y;
                              $m = $today->diff($birthDate)->m;
                              $d = $today->diff($birthDate)->d;
                              $h = $today->diff($birthDate)->h;
                              $i = $today->diff($birthDate)->i;
                              $expired = 0;
                              
                              echo lang('Global.expired')." ".$d." ".lang('Global.day')." ".$h."  ".lang('Global.hour')." ".$i." ".lang('Global.minute')."</sm>"; 
                            }
                            ?>
                         <br>
                          <?php 
                             if($product['status']=='start-summarization'){
                               ?>
                               <sm style="font-size: 13px;">Status :</sm> 
                                  <span class="badge badge-warning mr-4">
                                    <span class="status"><?= lang('Global.on_summarization') ?></span>
                                  </span>
                                  <br>
                                  <p style="font-size: 12px;"><?= $product['start-summarization'] ?></p>
                                <?php
                              } else if($product['status']=='summarized'){
                                ?>
                                <sm style="font-size: 13px;">Status :</sm> 
                                  <span class="badge badge-success mr-4">
                                    <span class="status"><?= lang('Global.done') ?></span>
                                  </span>
                                  <br>
                                  <a href="<?= base_url('psychologist/id/'.$product['psikolog_id']) ?>" class=""><?= lang('Global.psikolog').' '.$product['psikolog_first_name'].' '.$product['psikolog_last_name'] ?> </a>
                                  <p style="font-size: 12px;"><?= $product['summarized'] ?></p>
                                  <a href="<?php echo base_url('pdf/mpdf/'.$order['id'].'/'.$product['id'].'/D');?>" class="btn btn-success">Download <?= lang('Global.result') ?></a>

                                <?php
                              } else {
                                ?>
                                <?php
                              }
                            ?>
                         <div class="text-right">
                          <?php
                            if($order['status']=='paid-confirm' && $product['status']=="" && $expired==0){
                              ?>
                              <a href="<?php echo base_url('test/product/'.$order['id'].'/'.$product['id']);?>" class="btn btn-primary"><?= lang('Global.take_test') ?></a>
                              <?php
                            } else if($product['status']=='start-test'){
                              ?>
                              <a href="<?php echo base_url('test/product/'.$order['id'].'/'.$product['id']);?>" class="btn btn-primary">Lanjutkan</a>
                              <?php
                            }
                            else if($product['status']=='done-test' || $product['status']=='start-correction'){
                              ?>
                              <!-- <span>Psikolog sedang <br>menganalisa tes Anda</span> -->
                              <a href="<?php echo base_url('test/product/'.$order['id'].'/'.$product['id']);?>" class="btn btn-primary"><?= lang('Global.test_detail') ?></a>
                              <?php
                            } else if($product['status']=='corrected' || $product['status']=='start-summarization'){
                              ?>
                              <!-- <span>Psikolog sedang <br>menganalisa tes Anda</span> -->
                              <a href="<?php echo base_url('test/product/'.$order['id'].'/'.$product['id']);?>" class="btn btn-primary"><?= lang('Global.test_detail') ?></a>

                              <?php
                            } else if($product['status']=='summarized'){
                              ?>
                              <a href="<?php echo base_url('test/product/'.$order['id'].'/'.$product['id']);?>" class="btn btn-primary"><?= lang('Global.test_detail') ?></a>
                              <?php
                            } else {
                              ?>
                              <a href="<?php echo base_url('test/product/'.$order['id'].'/'.$product['id']);?>" class="btn btn-primary"><?= lang('Global.test_detail') ?></a>
                              <?php
                            } 
                          ?>
                         </div>
                      </div>
                    <?php
                      }
                    }
                  }
                }
                ?>
          </div>
        <!-- Card footer -->
        <!-- <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end mb-0">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <i class="fas fa-angle-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">
                  <i class="fas fa-angle-right"></i>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>
