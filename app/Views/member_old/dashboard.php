
<div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <!-- <h6 class="h2 text-white d-inline-block mb-0">Member Area</h6> -->
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                </ol>
              </nav>
            </div>
            <!-- <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div> -->
          </div>
          <!-- Card stats -->
          <div class="row" hidden="">
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah menambahkan produk dalam keranjang lalu checkout menunggu pembayaran">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0"><?= lang('Global.order') ?></h5>
                      <span class="h2 font-weight-bold mb-0">2</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-indigo text-white rounded-circle shadow">
                        <i class="ni ni-cart"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-wrap">Description</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Pembayaran yang telah dikonfirmasi admin">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0"><?= lang('Global.waiting_payment') ?></h5>
                      <span class="h2 font-weight-bold mb-0">1</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                        <i class="ni ni-money-coins"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-wrap">Lakukan pembayaran segera</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Pembayaran yang telah dikonfirmasi admin">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0"><?= lang('Global.confirmed_payment') ?></h5>
                      <span class="h2 font-weight-bold mb-0">1</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                        <i class="ni ni-check-bold"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-wrap">Admin telah mengecek pembayaran tersebut</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Banyaknya produk yang diorder oleh peserta">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0"><?= lang('Global.my_test') ?></h5>
                      <span class="h2 font-weight-bold mb-0">1</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-warning text-white rounded-circle shadow">
                        <i class="ni ni-box-2"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-wrap">Paket yang telah terbayar</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Banyaknya jenis tes yang diorder peserta">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Jenis tes</h5>
                      <span class="h2 font-weight-bold mb-0">3</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-danger text-white rounded-circle shadow">
                        <i class="ni ni-collection"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-wrap">Jenis tes yang dapat dikerjakan </span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah menyelesaikan semua tes pada produk yang telah dibeli">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Mengikuti tes</h5>
                      <span class="h2 font-weight-bold mb-0">1</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-yellow text-white rounded-circle shadow">
                        <i class="ni ni-bullet-list-67"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-wrap">Tes yang telah dikerjakan</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Tes peserta yang telah selesai dirangkum oleh Psikolog">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Summary</h5>
                      <span class="h2 font-weight-bold mb-0">1</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-cyan text-white rounded-circle shadow">
                        <i class="ni ni-chart-pie-35"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-wrap">Tes yang telah dinilai oleh Psikolog, hasil dapat didownload</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-12" hidden="">
          <div class="card bg-default">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col" data-toggle="tooltip" data-placement="bottom" title="Data checkout peserta yang telah terkonfirmasi pembayarannya">
                  <h6 class="text-light text-uppercase ls-1 mb-1">Overview</h6>
                  <h5 class="h3 text-white mb-0">Sales value</h5>
                </div>
                <div class="col">
                  <ul class="nav nav-pills justify-content-end">
                    <li class="nav-item mr-2 mr-md-0" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 10, 30, 15, 40, 20, 60, 60]}]}}' data-prefix="$" data-suffix="k">
                      <a href="#" class="nav-link py-2 px-3 active" data-toggle="tab">
                        <span class="d-none d-md-block">Month</span>
                        <span class="d-md-none">M</span>
                      </a>
                    </li>
                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="$" data-suffix="k">
                      <a href="#" class="nav-link py-2 px-3" data-toggle="tab">
                        <span class="d-none d-md-block">Week</span>
                        <span class="d-md-none">W</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta">
              <!-- Chart -->
              <div class="chart">
                <!-- Chart wrapper -->
                <canvas id="chart-sales-dark" class="chart-canvas"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-12">
          <div class="card">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Dashboard</h6>
                  <h5 class="h3 mb-0">My Activity</h5>
                </div>
              </div>
            </div>
            <div class="card-body" data-toggle="tooltip" data-placement="bottom">
              <!-- Chart -->
              <?php 
              if($profile->volunteer==1 && !empty($orders[0])){
                ?>
                <ul class="timeline">
                <li>
                  <a>Checkout <?= lang('Global.cart') ?></a>
                  <br><span class="text-sm ">Automated by system : </span>
                  <span class="text-sm "><?= $orders[0]['checkout'] ?></span>
                </li>
                <!-- <li>
                  <span class="badge badge-success mr-4">
                    <span class="status"><?= lang('Global.paid') ?></span>
                  </span>
                  <br><span class="text-sm ">Automated by system : </span>
                  <span class="text-sm "><?= $orders[0]['paid'] ?></span>
                </li>
                <li>
                  <span class="badge badge-success mr-4">
                    <span class="status"><?= lang('Global.confirmed') ?></span>
                  </span>
                  <br><span class="text-sm ">Automated by system : </span>
                  <span class="text-sm "><?= $orders[0]['paid-confirm'] ?></span>
                </li> -->
                <li>
                  <a><?= lang('Global.my_profile') ?></a>
                  <br>
                  <span class="text-sm">Pastikan jenis kelamin, tanggal lahir dan jenjang pendidikan Anda sudah terisi</span>
                  <a  href="<?= base_url('account/profile') ?>">     
                  <br><span class="text-sm ">Check <?= lang('Global.my_profile') ?></span>
                  </a>
                </li>
                <li>
                  <?php 
                  foreach ($orders[0]['products'][0]['jenistes'] as $key => $jenistes) {
                    $no = $key+1;
                    # code...
                      if($jenistes['start-test']=='0000-00-00 00:00:00'){
                        ?>
                        <a href="<?= base_url('test/product/'.$orders[0]['id'].'/'.$jenistes['id_memberorderproduct']) ?>"> <?= $no.'. '.$jenistes['nama'] ?>
                          <span class="badge badge-primary mr-4">
                            <span class="status"><?= lang('Global.ready_test') ?></span>
                          </span>
                          </a>
                          <a href="<?= base_url('test/product/'.$orders[0]['id'].'/'.$jenistes['id_memberorderproduct']) ?>" class="btn btn-primary"><?= lang('Global.take_test') ?></a>
                          <br>
                          <p style="font-size: 12px;">Tes bisa dimulai sejak : 
                          <?php
                          setlocale(LC_ALL, 'id-ID', 'id_ID');
                          echo strftime("%A, %d %B %Y", strtotime($orders[0]['paid-confirm'])).', '.
                          date('h:i:s A', strtotime($orders[0]['paid-confirm'])) 
                          ?>
                          </p>
                        <?php
                      } else if($jenistes['status']=='start-test'){
                        ?>
                        <a href="<?= base_url('test/product/'.$orders[0]['id'].'/'.$jenistes['id_memberorderproduct']) ?>"> <?= $no.'. '.$jenistes['nama'] ?>
                          <span class="badge badge-warning mr-4">
                            <span class="status"><?= lang('Global.on_test') ?></span>
                          </span>
                          </a>
                          <p style="font-size: 12px;">
                          Tes bisa dimulai sejak : 
                          <?php
                          setlocale(LC_ALL, 'id-ID', 'id_ID');
                          echo strftime("%A, %d %B %Y", strtotime($orders[0]['paid-confirm'])).', '.
                          date('h:i:s A', strtotime($orders[0]['paid-confirm'])) 
                          ?>
                          <br>
                          Anda telah memulai tes pada : 
                          <?php
                          setlocale(LC_ALL, 'id-ID', 'id_ID');
                          echo strftime("%A, %d %B %Y", strtotime($jenistes['start-test'])).', '.
                          date('h:i:s A', strtotime($jenistes['start-test'])) 
                          ?>
                          </p>
                        <?php
                      } else if($jenistes['done-test']!='0000-00-00 00:00:00' || $jenistes['status']=='done-test'){
                        ?>
                        <a href="<?= base_url('test/product/'.$orders[0]['id'].'/'.$jenistes['id_memberorderproduct']) ?>"> <?= $no.'. '.$jenistes['nama'] ?>
                          <span class="badge badge-success mr-4">
                            <span class="status"><?= lang('Global.done') ?></span>
                          </span>
                          </a>
                          <br>
                            <?php
                            $date1 = new DateTime($jenistes['start-test']);
                            $date2 = new DateTime($jenistes['done-test']); 
                            echo 'Anda menyelesaikan tes dalam : '; 
                            if($date2->diff($date1)->d!=0){
                              echo $date2->diff($date1)->d.' hari ';
                            }
                            echo $date2->diff($date1)->h.' jam '; 
                            echo $date2->diff($date1)->i.' menit '; 
                            echo $date2->diff($date1)->s.' detik '; 
                            ?>
                          <br>
                          <p style="font-size: 12px;">
                          Tes bisa dimulai sejak : 
                          <?php
                          setlocale(LC_ALL, 'id-ID', 'id_ID');
                          echo strftime("%A, %d %B %Y", strtotime($orders[0]['paid-confirm'])).', '.
                          date('h:i:s A', strtotime($orders[0]['paid-confirm'])) 
                          ?>
                          <br>
                          Anda telah memulai tes pada : 
                          <?php
                          setlocale(LC_ALL, 'id-ID', 'id_ID');
                          echo strftime("%A, %d %B %Y", strtotime($jenistes['start-test'])).', '.
                          date('h:i:s A', strtotime($jenistes['start-test'])) 
                          ?>
                          <br>
                          Anda telah menyelesaikan tes pada : 
                          <?php
                          setlocale(LC_ALL, 'id-ID', 'id_ID');
                          echo strftime("%A, %d %B %Y", strtotime($jenistes['done-test'])).', '.
                          date('h:i:s A', strtotime($jenistes['done-test'])) 
                          ?>
                          </p>
                        <?php
                      } 
                  }
                  ?>
                </li>
                <li>
                  <?php 
                  
                     if($orders[0]['products'][0]['status']=='start-summarization'){
                        ?>
                        
                          <span class="badge badge-warning mr-4">
                            <span class="status"><?= lang('Global.on_summarization') ?></span>
                          </span>
                          <br>
                          <span class="text-sm">Psikolog sedang meringkas paket tes Anda</span>
                          <p style="font-size: 12px;"><?= $orders[0]['products'][0]['start-summarization'] ?></p>
                        <?php
                      } else if($orders[0]['products'][0]['status']=='summarized'){
                        ?>
                          <span class="badge badge-success mr-4">
                            <span class="status"><?= lang('Global.done') ?></span>
                          </span>
                          <br>
                          <a href="<?= base_url('psychologist/id/'.$product['psikolog_id']) ?>" class=""><?= lang('Global.psikolog').' '.$orders[0]['products'][0]['psikolog_first_name'].' '.$orders[0]['products'][0]['psikolog_last_name'] ?> </a>
                          <p style="font-size: 12px;"><?= $orders[0]['products'][0]['summarized'] ?></p>
                          <a href="<?php echo base_url('pdf/mpdf/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id'].'/D');?>" class="btn btn-success">Download <?= lang('Global.result') ?></a>
                        <?php
                      } else {
                        ?>
                        <span class="badge badge-info mr-4">
                            <span class="status"><?= lang('Global.result') ?></span>
                          </span>
                          <br>
                          <span class="text-sm">Hasil akan Anda dapatkan setelah menyelesaikan tes dan selesai diringkas oleh Psikolog kami</span>
                          <!-- <p style="font-size: 12px;"><?= $orders[0]['products'][0]['start-summarization'] ?></p> -->
                        <?php
                      }
                    ?>
                </li>
                <li before>
                  <a href="<?= base_url('feedback') ?>">Feedback</a>
                  <?php
                  if(empty($feedbacks[0]['feedback']) || $feedbacks[0]['feedback']==NULL){
                    ?>
                  <br><span class="text-sm">Anda belum mengisi feedback, <a href="<?= base_url('feedback') ?>">klik disini</a></span>
                  <?php
                  } else if(!empty($feedbacks[0]['feedback'])) {
                    ?>
                      <br><span class="text-sm "><?= $feedbacks[0]['feedback'] ?>
                      <br>
                      <a href="<?= base_url('feedback') ?>">Edit feeback</a>
                      </span>
                      <?php
                  }
                  ?>
                </li>
              </ul>
                <?php
              }
              ?>
              <div class="chart" hidden="">
                <canvas id="chart-bars" class="chart-canvas"></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="row">
        <div class="col-xl-8">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Page visits</h3>
                </div>
                <div class="col text-right">
                  <a href="#!" class="btn btn-sm btn-primary">See all</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Page name</th>
                    <th scope="col">Visitors</th>
                    <th scope="col">Unique users</th>
                    <th scope="col">Bounce rate</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">
                      /argon/
                    </th>
                    <td>
                      4,569
                    </td>
                    <td>
                      340
                    </td>
                    <td>
                      <i class="fas fa-arrow-up text-success mr-3"></i> 46,53%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      /argon/index.html
                    </th>
                    <td>
                      3,985
                    </td>
                    <td>
                      319
                    </td>
                    <td>
                      <i class="fas fa-arrow-down text-warning mr-3"></i> 46,53%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      /argon/charts.html
                    </th>
                    <td>
                      3,513
                    </td>
                    <td>
                      294
                    </td>
                    <td>
                      <i class="fas fa-arrow-down text-warning mr-3"></i> 36,49%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      /argon/tables.html
                    </th>
                    <td>
                      2,050
                    </td>
                    <td>
                      147
                    </td>
                    <td>
                      <i class="fas fa-arrow-up text-success mr-3"></i> 50,87%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      /argon/profile.html
                    </th>
                    <td>
                      1,795
                    </td>
                    <td>
                      190
                    </td>
                    <td>
                      <i class="fas fa-arrow-down text-danger mr-3"></i> 46,53%
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-xl-4">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Social traffic</h3>
                </div>
                <div class="col text-right">
                  <a href="#!" class="btn btn-sm btn-primary">See all</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Referral</th>
                    <th scope="col">Visitors</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">
                      Facebook
                    </th>
                    <td>
                      1,480
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">60%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      Facebook
                    </th>
                    <td>
                      5,480
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">70%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;"></div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      Google
                    </th>
                    <td>
                      4,807
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">80%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-primary" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"></div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      Instagram
                    </th>
                    <td>
                      3,678
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">75%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;"></div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      twitter
                    </th>
                    <td>
                      2,645
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">30%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-warning" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> -->