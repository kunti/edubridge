<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <!-- <h6 class="h2 text-white d-inline-block mb-0">Psikolog</h6> -->
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.test_package') ?></li>
            </ol>
          </nav>
        </div>
       <!--  <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0"><?= lang('Global.test_package') ?></h3>
        </div>
        
        <!-- Card footer -->
        <div class="card-footer py-4">
          <div class="container">
            <div class="header-body text-center mb-7">
              <div class="row justify-content-center">
                <div class="col-lg-12 col-md-12">
                  <div class="row">
                    <?php foreach ($products as $key => $product){ ?>
                <div class="col-md-3" style="padding-top: 50px">
                  <div class="info">
                    <div class="icon icon-shape bg-gradient-primary rounded-circle text-white">
                    <!-- <div class="icon icon-lg icon-shape icon-shape-primary shadow rounded-circle"> -->
                      <i class="ni ni-app"></i>
                    </div>
                    <a href="<?php echo base_url('product/id/'.$product['id']) ?>" class="text-primary">
                      <h6 class="info-title text-uppercase text-primary" style="font-size: 16px"><?= $product['nama'] ?></h6>
                    </a>
                    <p class="description opacity-8"><?= $product['short_desc'] ?></p>
                    <a href="<?php echo base_url('product/id/'.$product['id']) ?>" class="text-primary"><?=lang('Global.detail');?>
                      <i class="ni ni-bold-right text-primary"></i>
                    </a>
                  </div>
                </div>
                <?php
                  } 
                ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>