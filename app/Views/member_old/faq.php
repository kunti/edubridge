<div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item active" aria-current="page">FAQ</li>
                </ol>
              </nav>
            </div>
            <!-- <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row mt--5">
        <div class="col-md-10 ml-auto mr-auto">
          <div class="card card-upgrade">
            <div class="card-header text-center border-bottom-0">
              <h4 class="card-title">FAQ</h4>
              <!-- <p class="card-category">Frequently asked questions</p> -->
            </div>
            <div class="card-body">
              <span><b><?= lang('Global.question1') ?></b></span>
              <p><?= lang('Global.answer1') ?></p>
              <span><b><?= lang('Global.question2') ?></b></span>
              <p><?= lang('Global.answer2') ?></p>
              <span><b><?= lang('Global.question3') ?></b></span>
              <p><?= lang('Global.answer3') ?></p>
              <span><b><?= lang('Global.question4') ?></b></span>
              <p><?= lang('Global.answer4') ?></p>
            </div>
          </div>
        </div>
      </div>