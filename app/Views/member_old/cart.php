<div class="main-content">
    <!-- Header -->
    <div class="header pb-6 bg-primary d-flex align-items-center">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <!-- <h6 class="h2 text-white d-inline-block mb-0">Order</h6> -->
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
                  <!-- <li class="breadcrumb-item"><a href="#">Peserta</a></li> -->
                  <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.cart') ?></li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <!-- <a href="#" class="btn btn-sm btn-neutral">New</a> -->
              <!-- <a href="#" class="btn btn-lg btn-neutral">Checkout</a> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-8 order-xl-1">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-12">
                  <h3 class="mb-0"><?= lang('Global.cart_detail') ?></h3>
                  <br>
                  <?php if(session()->getFlashdata('success') != null){ ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                      <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                      <span class="alert-inner--text"><strong>Success!</strong> <?php echo session()->getFlashdata('success'); ?></span>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <?php } ?>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table">
                <?php if(!empty($items)){ // cek apakan keranjang belanja lebih dari 0, jika iya maka tampilkan list dalam bentuk table di bawah ini: ?>
                <?php echo form_open('cart/update'); ?>
                <table id="" class="table" width="100%" >
                  <thead class="thead-light">
                    <tr>
                      <th class="th-sm">No
                      </th>
                      <th class="th-sm">Paket Tes
                      </th>
                      <th class="th-sm">Action
                      </th>
                    </tr>
                  </thead>
                  <tbody class="list">
                    <!-- awal baris -->
                    <?php 
                    foreach($items as $key => $item) { ?>
                    <tr>
                      <td>
                        <?php echo $key + 1; ?>
                      </td>
                      <td>
                        <div class="media-body">
                            <span><?php echo $item[0]['nama']; ?></span>
                            <br>
                            <?php 
                            // dd($item);
                            if($carts[0]['volunteer']==1 && $item[0]['id']==1) {
                            ?>
                                <span><strike>Rp. <?php echo number_format($item[0]['harga'], 0, 0, '.'); ?></strike></span>
                                <span>Rp. <?php echo number_format(0, 0, 0, '.'); ?></span>
                            <?php 
                            } else {
                            ?>
                            <span>Rp. <?php echo number_format($item[0]['harga'], 0, 0, '.'); ?></span>
                            <?php 
                            }
                            ?>
                          </div>
                        </div>
                      </td>
                     <!--  <td>
                        
                      </td> -->
                      <td>
                        <a href="<?php echo base_url('cart/remove/'.$item[0]['id']); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus product ini dari keranjang belanja?')"><i class="fa fa-trash"></i> Hapus</a>
                      </td>
                    </tr>
                    <?php } ?>
                    <!-- akhir baris -->
                  </tbody>
                </table>
                <?php echo form_close(); ?>
                <?php } // selesai menampilkan list cart dalam bentuk table ?>
                <?php if(empty($items)){ // jika cart kosong, maka tampilkan: ?>
                    <?= lang('Global.cart_kosong') ?><a href="<?php echo base_url('/product'); ?>" class="btn btn-success"><?= lang('Global.lihat_semua_paket') ?></a>
                    <?php } else { // jika cart tidak kosong, tampilkan: ?>
                        <a href="<?php echo base_url('product'); ?>" class="btn btn-success"><?= lang('Global.lanjut_belanja') ?></a>
                    <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-4 order-xl-2">
          <div class="card card-profile">
            <div class="row">
              <div class="card-body pt-20">
                <div class="text-center">
                  <h3 class="h1">
                    <p class="opacity-8"><?= lang('Global.total_harga') ?> : </p>
                    Rp. <?php echo number_format($total, 0, 0, '.'); ?> <!-- <del class="h5 opacity-5">Rp. 250.000</del> -->
                  </h3>
                  <div class="h1 mt-4">
                    <a href="<?= base_url('cart/checkout'); ?>" class="btn btn-primary"><!-- <i class="fa fa-shopping-cart"></i> --> Checkout</a>
                  </div>
                  <p class="opacity-8" style="font-size: 12px;"><?= lang('Global.harga_desc') ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="section features-1" style="background-color: #232941; color: #FDC834">
      <div class="container">
        <div class="text-center" style="padding-top: 12px; padding-bottom: 12px; ">
            <h4 class="display-4" style="color: #FDC834"><?=lang('Global.lihat_semua_paket')?></h4>
        </div>
        <div class="row">
          <?php if(!empty($products)) {
            foreach($products as $key => $item) { ?>
                <div class="col-md-3 text-center d-flex flex-column" style="padding-top: 12px; padding-bottom: 24px;">
                  <div class="info">
                    <span class="avatar avatar-lg rounded-circle">
                    <img alt="Image placeholder" src="<?= base_url('src/assets/img/product/'.$item['id'].'.png') ?>">
                  </span>
                    <a href="<?php echo base_url('product/id/'.$item['id']) ?>"><h6 class="info-title text-uppercase" style="color: #FDC834"><?= $item['nama']; ?></h6></a>
                    <p class="description opacity-8"><?= $item['short_desc']; ?></p>
                  </div>
                  <br>
                  <a href="<?php echo base_url('product/id/'.$item['id']) ?>" class="mt-auto btn btn-primary btn-icon mt-3 mb-sm-0">
                      <span class="btn-inner--icon"><?=lang('Global.detail');?></span>
                      <!-- <span class="btn-inner--text">Learn more</span> -->
                    </a>
                </div>    
          <?php }
          } ?>
        </div>
        <div class="container">
          
        </div>
      </div>
    </div>
        
</div>
    