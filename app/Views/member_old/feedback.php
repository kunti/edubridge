<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <!-- <h6 class="h2 text-white d-inline-block mb-0">Order</h6> -->
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
              <!-- <li class="breadcrumb-item"><a href="#">Peserta</a></li> -->
              <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.feedback') ?></li>
            </ol>
          </nav>
        </div>
        <!-- <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0"><?= lang('Global.myfeedback') ?></h3>
        </div>
        <!-- Light table -->
          <div class="container">
            <form action="<?= base_url('feedback/save') ?>" method="post">
            <textarea class="form-control" id="exampleFormControlTextarea1" name="feedback" rows="10" placeholder="Write a large text here ..."><?php if(!empty($feedbacks[0]['feedback'])) {echo $feedbacks[0]['feedback'];} ?></textarea>
            <br>
            <div class="text-right">
            <input type="submit" class="btn btn-primary" value="<?= lang('Global.save') ?>">
            </div>
            <br>
          </form>
          </div>
      </div>
    </div>
  </div>
</div>
