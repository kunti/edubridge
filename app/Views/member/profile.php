<!-- Page content -->

    <div class="container-fluid mt--6">

      <div class="row">

    

        <div class="col-xl-12 order-xl-1">

        <form role="form" action="<?= base_url('account/save') ?>" method="post" accept-charset="utf-8" class="needs-validation" novalidate>

          <input type="hidden" name="id_user" id="id_user" value="<?= $myprofile['id'] ?>">

          <div class="card">

            <div class="card-header">

              <div class="row align-items-center">

                <div class="col-7">

                  <h3 class="mb-0"><?= lang('Global.my_profile') ?></h3>

                </div>

                <div class="col-5 text-right">

                  <small><?php 

                  // dd($_SESSION['message']);

                  if(!empty($_SESSION['message']['birthdate'])) {

                    echo $_SESSION['message']['birthdate'] ;

                  } 

                  ?></small>

                    <!-- <input type="submit" name="submit" value="Save changes" class="btn btn-xl btn-success"> -->

                </div>

              </div>

            </div>

            <div class="card-body">

                <h6 class="heading-small text-muted mb-4"><?= lang('Global.identity') ?></h6>

                <div class="">

                  <div class="row">

                    <div class="col-lg-6">

                      <div class="form-group">

                        <label class="form-control-label" for="input-username"><?= lang('Global.first_name') ?>*</label>

                        <input type="text" id="first_name" name="first_name" class="form-control" placeholder="Nama depan Anda" value="<?= $myprofile['first_name']; ?>" required disabled>

                      </div>

                    </div>

                    <div class="col-lg-6">

                      <div class="form-group">

                        <label class="form-control-label" for="input-username"><?= lang('Global.last_name') ?>*</label>

                        <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Nama belakang Anda" value="<?= $myprofile['last_name']; ?>" required disabled>

                      </div>

                    </div>

                    <div class="col-lg-12">

                      <div class="form-group">

                        <label class="form-control-label" for="input-email">Email*</label>

                        <input type="email" id="email" name="email" class="form-control" placeholder="Email Anda" value="<?= $myprofile['email']; ?>" disabled 

                        >

                        <!-- <?php

                        if ($myprofile['active']==0){

                          ?>

                          <small>Akun Anda belum diaktivasi, jika Anda belum menerima email aktivasi akun klik <a href="<?= base_url('auth/sendMyActivation') ?>" style="color:#0d6efd">Kirim ulang aktivasi email</a> untuk konfirmasi email Anda dan mengaktifkan akun</small>

                          <br>

                          <span class='badge badge-info'>

                            <span class='status'>

                              <?php

                              if(!empty($_SESSION['emailactivation'])) {

                                echo $_SESSION['emailactivation'] ;

                              } 

                              ?>

                            </span>

                          </span>

                          <?php

                        } else {

                          ?>

                          <small>Akun sudah aktif</small>

                          <?php

                        }

                        ?> -->

                      </div>

                    </div>



                    <?php 

                      if($_SESSION['user_id']==$myprofile['id']){

                        ?>

                      <div class="col-lg-12">

                        <div class="form-group">

                          <label class="form-control-label" for="">Password*</label>

                          <input type="email" id="" name="" class="form-control" placeholder="********" value="" disabled>

                          <!-- <a class="text-sm" href="<?= base_url('auth/change_password') ?>">Reset password</a> -->

                        </div>

                      </div>

                        <?php

                      }

                    ?>

                    <div class="col-lg-12">

                      <div class="form-group">

                        <div class="form-group">

                        <label class="form-control-label" for="input-sex"><?=lang('Global.sex');?>*</label>

                          <!-- <label for="inputState" style="font-size: 14px; color: #8898aa"><?=lang('Global.sex');?>*</label> -->

                          <select id="sex" name="sex" class="form-control" disabled>

                            <option><?=lang('Global.choose');?>...</option>

                            <option <?php if($myprofile['sex']=='male') echo 'selected'; ?> value="male">Laki-laki</option>

                            <option <?php if($myprofile['sex']=='female') echo 'selected'; ?> value="female">Perempuan</option>

                          </select>

                        </div>

                        </div>

                    </div>

                    <div class="col-lg-12">

                      <div class="form-group">

                        <div class="form-group">

                        <label class="form-control-label" for="input-sex">Kota lahir*</label>

                        <?php

                        ?>

                        <select id="inputState" class="form-control js-example-basic-single " name="kotalahir" required tabindex="-1" aria-hidden="true" required disabled>

                    <option value="" selected="">Silahkan pilih...</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten simeulue") echo "selected" ?> value="Kabupaten simeulue"> KABUPATEN SIMEULUE</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten aceh singkil") echo "selected" ?> value="Kabupaten aceh singkil"> KABUPATEN ACEH SINGKIL</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten aceh selatan") echo "selected" ?> value="Kabupaten aceh selatan"> KABUPATEN ACEH SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten aceh tenggara") echo "selected" ?> value="Kabupaten aceh tenggara"> KABUPATEN ACEH TENGGARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten aceh timur") echo "selected" ?> value="Kabupaten aceh timur"> KABUPATEN ACEH TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten aceh tengah") echo "selected" ?> value="Kabupaten aceh tengah"> KABUPATEN ACEH TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten aceh barat") echo "selected" ?> value="Kabupaten aceh barat"> KABUPATEN ACEH BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten aceh besar") echo "selected" ?> value="Kabupaten aceh besar"> KABUPATEN ACEH BESAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pidie") echo "selected" ?> value="Kabupaten pidie"> KABUPATEN PIDIE</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bireuen") echo "selected" ?> value="Kabupaten bireuen"> KABUPATEN BIREUEN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten aceh utara") echo "selected" ?> value="Kabupaten aceh utara"> KABUPATEN ACEH UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten aceh barat daya") echo "selected" ?> value="Kabupaten aceh barat daya"> KABUPATEN ACEH BARAT DAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten gayo lues") echo "selected" ?> value="Kabupaten gayo lues"> KABUPATEN GAYO LUES</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten aceh tamiang") echo "selected" ?> value="Kabupaten aceh tamiang"> KABUPATEN ACEH TAMIANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten nagan raya") echo "selected" ?> value="Kabupaten nagan raya"> KABUPATEN NAGAN RAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten aceh jaya") echo "selected" ?> value="Kabupaten aceh jaya"> KABUPATEN ACEH JAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bener meriah") echo "selected" ?> value="Kabupaten bener meriah"> KABUPATEN BENER MERIAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pidie jaya") echo "selected" ?> value="Kabupaten pidie jaya"> KABUPATEN PIDIE JAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota banda aceh") echo "selected" ?> value="Kota banda aceh"> KOTA BANDA ACEH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota sabang") echo "selected" ?> value="Kota sabang"> KOTA SABANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota langsa") echo "selected" ?> value="Kota langsa"> KOTA LANGSA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota lhokseumawe") echo "selected" ?> value="Kota lhokseumawe"> KOTA LHOKSEUMAWE</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota subulussalam") echo "selected" ?> value="Kota subulussalam"> KOTA SUBULUSSALAM</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten nias") echo "selected" ?> value="Kabupaten nias"> KABUPATEN NIAS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mandailing natal") echo "selected" ?> value="Kabupaten mandailing natal"> KABUPATEN MANDAILING NATAL</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tapanuli selatan") echo "selected" ?> value="Kabupaten tapanuli selatan"> KABUPATEN TAPANULI SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tapanuli tengah") echo "selected" ?> value="Kabupaten tapanuli tengah"> KABUPATEN TAPANULI TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tapanuli utara") echo "selected" ?> value="Kabupaten tapanuli utara"> KABUPATEN TAPANULI UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten toba samosir") echo "selected" ?> value="Kabupaten toba samosir"> KABUPATEN TOBA SAMOSIR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten labuhan batu") echo "selected" ?> value="Kabupaten labuhan batu"> KABUPATEN LABUHAN BATU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten asahan") echo "selected" ?> value="Kabupaten asahan"> KABUPATEN ASAHAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten simalungun") echo "selected" ?> value="Kabupaten simalungun"> KABUPATEN SIMALUNGUN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten dairi") echo "selected" ?> value="Kabupaten dairi"> KABUPATEN DAIRI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten karo") echo "selected" ?> value="Kabupaten karo"> KABUPATEN KARO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten deli serdang") echo "selected" ?> value="Kabupaten deli serdang"> KABUPATEN DELI SERDANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten langkat") echo "selected" ?> value="Kabupaten langkat"> KABUPATEN LANGKAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten nias selatan") echo "selected" ?> value="Kabupaten nias selatan"> KABUPATEN NIAS SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten humbang hasundutan") echo "selected" ?> value="Kabupaten humbang hasundutan"> KABUPATEN HUMBANG HASUNDUTAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pakpak bharat") echo "selected" ?> value="Kabupaten pakpak bharat"> KABUPATEN PAKPAK BHARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten samosir") echo "selected" ?> value="Kabupaten samosir"> KABUPATEN SAMOSIR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten serdang bedagai") echo "selected" ?> value="Kabupaten serdang bedagai"> KABUPATEN SERDANG BEDAGAI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten batu bara") echo "selected" ?> value="Kabupaten batu bara"> KABUPATEN BATU BARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten padang lawas utara") echo "selected" ?> value="Kabupaten padang lawas utara"> KABUPATEN PADANG LAWAS UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten padang lawas") echo "selected" ?> value="Kabupaten padang lawas"> KABUPATEN PADANG LAWAS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten labuhan batu selatan") echo "selected" ?> value="Kabupaten labuhan batu selatan"> KABUPATEN LABUHAN BATU SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten labuhan batu utara") echo "selected" ?> value="Kabupaten labuhan batu utara"> KABUPATEN LABUHAN BATU UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten nias utara") echo "selected" ?> value="Kabupaten nias utara"> KABUPATEN NIAS UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten nias barat") echo "selected" ?> value="Kabupaten nias barat"> KABUPATEN NIAS BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota sibolga") echo "selected" ?> value="Kota sibolga"> KOTA SIBOLGA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota tanjung balai") echo "selected" ?> value="Kota tanjung balai"> KOTA TANJUNG BALAI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota pematang siantar") echo "selected" ?> value="Kota pematang siantar"> KOTA PEMATANG SIANTAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota tebing tinggi") echo "selected" ?> value="Kota tebing tinggi"> KOTA TEBING TINGGI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota medan") echo "selected" ?> value="Kota medan"> KOTA MEDAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota binjai") echo "selected" ?> value="Kota binjai"> KOTA BINJAI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota padangsidimpuan") echo "selected" ?> value="Kota padangsidimpuan"> KOTA PADANGSIDIMPUAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota gunungsitoli") echo "selected" ?> value="Kota gunungsitoli"> KOTA GUNUNGSITOLI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kepulauan mentawai") echo "selected" ?> value="Kabupaten kepulauan mentawai"> KABUPATEN KEPULAUAN MENTAWAI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pesisir selatan") echo "selected" ?> value="Kabupaten pesisir selatan"> KABUPATEN PESISIR SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten solok") echo "selected" ?> value="Kabupaten solok"> KABUPATEN SOLOK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sijunjung") echo "selected" ?> value="Kabupaten sijunjung"> KABUPATEN SIJUNJUNG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tanah datar") echo "selected" ?> value="Kabupaten tanah datar"> KABUPATEN TANAH DATAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten padang pariaman") echo "selected" ?> value="Kabupaten padang pariaman"> KABUPATEN PADANG PARIAMAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten agam") echo "selected" ?> value="Kabupaten agam"> KABUPATEN AGAM</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lima puluh kota") echo "selected" ?> value="Kabupaten lima puluh kota"> KABUPATEN LIMA PULUH KOTA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pasaman") echo "selected" ?> value="Kabupaten pasaman"> KABUPATEN PASAMAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten solok selatan") echo "selected" ?> value="Kabupaten solok selatan"> KABUPATEN SOLOK SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten dharmasraya") echo "selected" ?> value="Kabupaten dharmasraya"> KABUPATEN DHARMASRAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pasaman barat") echo "selected" ?> value="Kabupaten pasaman barat"> KABUPATEN PASAMAN BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota padang") echo "selected" ?> value="Kota padang"> KOTA PADANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota solok") echo "selected" ?> value="Kota solok"> KOTA SOLOK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota sawah lunto") echo "selected" ?> value="Kota sawah lunto"> KOTA SAWAH LUNTO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota padang panjang") echo "selected" ?> value="Kota padang panjang"> KOTA PADANG PANJANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota bukittinggi") echo "selected" ?> value="Kota bukittinggi"> KOTA BUKITTINGGI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota payakumbuh") echo "selected" ?> value="Kota payakumbuh"> KOTA PAYAKUMBUH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota pariaman") echo "selected" ?> value="Kota pariaman"> KOTA PARIAMAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kuantan singingi") echo "selected" ?> value="Kabupaten kuantan singingi"> KABUPATEN KUANTAN SINGINGI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten indragiri hulu") echo "selected" ?> value="Kabupaten indragiri hulu"> KABUPATEN INDRAGIRI HULU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten indragiri hilir") echo "selected" ?> value="Kabupaten indragiri hilir"> KABUPATEN INDRAGIRI HILIR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pelalawan") echo "selected" ?> value="Kabupaten pelalawan"> KABUPATEN PELALAWAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten siak") echo "selected" ?> value="Kabupaten siak"> KABUPATEN SIAK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kampar") echo "selected" ?> value="Kabupaten kampar"> KABUPATEN KAMPAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten rokan hulu") echo "selected" ?> value="Kabupaten rokan hulu"> KABUPATEN ROKAN HULU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bengkalis") echo "selected" ?> value="Kabupaten bengkalis"> KABUPATEN BENGKALIS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten rokan hilir") echo "selected" ?> value="Kabupaten rokan hilir"> KABUPATEN ROKAN HILIR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kepulauan meranti") echo "selected" ?> value="Kabupaten kepulauan meranti"> KABUPATEN KEPULAUAN MERANTI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota pekanbaru") echo "selected" ?> value="Kota pekanbaru"> KOTA PEKANBARU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota dumai") echo "selected" ?> value="Kota dumai i"> KOTA DUMAI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kerinci") echo "selected" ?> value="Kabupaten kerinci"> KABUPATEN KERINCI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten merangin") echo "selected" ?> value="Kabupaten merangin"> KABUPATEN MERANGIN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sarolangun") echo "selected" ?> value="Kabupaten sarolangun"> KABUPATEN SAROLANGUN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten batang hari") echo "selected" ?> value="Kabupaten batang hari"> KABUPATEN BATANG HARI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten muaro jambi") echo "selected" ?> value="Kabupaten muaro jambi"> KABUPATEN MUARO JAMBI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tanjung jabung timur") echo "selected" ?> value="Kabupaten tanjung jabung timur"> KABUPATEN TANJUNG JABUNG TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tanjung jabung barat") echo "selected" ?> value="Kabupaten tanjung jabung barat"> KABUPATEN TANJUNG JABUNG BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tebo") echo "selected" ?> value="Kabupaten tebo"> KABUPATEN TEBO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bungo") echo "selected" ?> value="Kabupaten bungo"> KABUPATEN BUNGO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota jambi") echo "selected" ?> value="Kota jambi"> KOTA JAMBI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota sungai penuh") echo "selected" ?> value="Kota sungai penuh"> KOTA SUNGAI PENUH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten ogan komering ulu") echo "selected" ?> value="Kabupaten ogan komering ulu"> KABUPATEN OGAN KOMERING ULU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten ogan komering ilir") echo "selected" ?> value="Kabupaten ogan komering ilir"> KABUPATEN OGAN KOMERING ILIR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten muara enim") echo "selected" ?> value="Kabupaten muara enim"> KABUPATEN MUARA ENIM</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lahat") echo "selected" ?> value="Kabupaten lahat"> KABUPATEN LAHAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten musi rawas") echo "selected" ?> value="Kabupaten musi rawas"> KABUPATEN MUSI RAWAS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten musi banyuasin") echo "selected" ?> value="Kabupaten musi banyuasin"> KABUPATEN MUSI BANYUASIN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten banyu asin") echo "selected" ?> value="Kabupaten banyu asin"> KABUPATEN BANYU ASIN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten ogan komering ulu selatan") echo "selected" ?> value="Kabupaten ogan komering ulu selatan"> KABUPATEN OGAN KOMERING ULU SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten ogan komering ulu timur") echo "selected" ?> value="Kabupaten ogan komering ulu timur"> KABUPATEN OGAN KOMERING ULU TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten ogan ilir") echo "selected" ?> value="Kabupaten ogan ilir"> KABUPATEN OGAN ILIR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten empat lawang") echo "selected" ?> value="Kabupaten empat lawang"> KABUPATEN EMPAT LAWANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten penukal abab lematang ilir") echo "selected" ?> value="Kabupaten penukal abab lematang ilir"> KABUPATEN PENUKAL ABAB LEMATANG ILIR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten musi rawas utara") echo "selected" ?> value="Kabupaten musi rawas utara"> KABUPATEN MUSI RAWAS UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota palembang") echo "selected" ?> value="Kota palembang"> KOTA PALEMBANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota prabumulih") echo "selected" ?> value="Kota prabumulih"> KOTA PRABUMULIH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota pagar alam") echo "selected" ?> value="Kota pagar alam"> KOTA PAGAR ALAM</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota lubuklinggau") echo "selected" ?> value="Kota lubuklinggau"> KOTA LUBUKLINGGAU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bengkulu selatan") echo "selected" ?> value="Kabupaten bengkulu selatan"> KABUPATEN BENGKULU SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten rejang lebong") echo "selected" ?> value="Kabupaten rejang lebong"> KABUPATEN REJANG LEBONG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bengkulu utara") echo "selected" ?> value="Kabupaten bengkulu utara"> KABUPATEN BENGKULU UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kaur") echo "selected" ?> value="Kabupaten kaur"> KABUPATEN KAUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten seluma") echo "selected" ?> value="Kabupaten seluma"> KABUPATEN SELUMA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mukomuko") echo "selected" ?> value="Kabupaten mukomuko"> KABUPATEN MUKOMUKO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lebong") echo "selected" ?> value="Kabupaten lebong"> KABUPATEN LEBONG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kepahiang") echo "selected" ?> value="Kabupaten kepahiang"> KABUPATEN KEPAHIANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bengkulu tengah") echo "selected" ?> value="Kabupaten bengkulu tengah"> KABUPATEN BENGKULU TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota bengkulu") echo "selected" ?> value="Kota bengkulu"> KOTA BENGKULU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lampung barat") echo "selected" ?> value="Kabupaten lampung barat"> KABUPATEN LAMPUNG BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tanggamus") echo "selected" ?> value="Kabupaten tanggamus"> KABUPATEN TANGGAMUS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lampung selatan") echo "selected" ?> value="Kabupaten lampung selatan"> KABUPATEN LAMPUNG SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lampung timur") echo "selected" ?> value="Kabupaten lampung timur"> KABUPATEN LAMPUNG TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lampung tengah") echo "selected" ?> value="Kabupaten lampung tengah"> KABUPATEN LAMPUNG TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lampung utara") echo "selected" ?> value="Kabupaten lampung utara"> KABUPATEN LAMPUNG UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten way kanan") echo "selected" ?> value="Kabupaten way kanan"> KABUPATEN WAY KANAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tulangbawang") echo "selected" ?> value="Kabupaten tulangbawang"> KABUPATEN TULANGBAWANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pesawaran") echo "selected" ?> value="Kabupaten pesawaran"> KABUPATEN PESAWARAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pringsewu") echo "selected" ?> value="Kabupaten pringsewu"> KABUPATEN PRINGSEWU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mesuji") echo "selected" ?> value="Kabupaten mesuji"> KABUPATEN MESUJI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tulang bawang barat") echo "selected" ?> value="Kabupaten tulang bawang barat"> KABUPATEN TULANG BAWANG BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pesisir barat") echo "selected" ?> value="Kabupaten pesisir barat"> KABUPATEN PESISIR BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota bandar lampung") echo "selected" ?> value="Kota bandar lampung"> KOTA BANDAR LAMPUNG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota metro") echo "selected" ?> value="Kota metro"> KOTA METRO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bangka") echo "selected" ?> value="Kabupaten bangka"> KABUPATEN BANGKA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten belitung") echo "selected" ?> value="Kabupaten belitung"> KABUPATEN BELITUNG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bangka barat") echo "selected" ?> value="Kabupaten bangka barat"> KABUPATEN BANGKA BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bangka tengah") echo "selected" ?> value="Kabupaten bangka tengah"> KABUPATEN BANGKA TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bangka selatan") echo "selected" ?> value="Kabupaten bangka selatan"> KABUPATEN BANGKA SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten belitung timur") echo "selected" ?> value="Kabupaten belitung timur"> KABUPATEN BELITUNG TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota pangkal pinang") echo "selected" ?> value="Kota pangkal pinang"> KOTA PANGKAL PINANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten karimun") echo "selected" ?> value="Kabupaten karimun"> KABUPATEN KARIMUN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bintan") echo "selected" ?> value="Kabupaten bintan"> KABUPATEN BINTAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten natuna") echo "selected" ?> value="Kabupaten natuna"> KABUPATEN NATUNA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lingga") echo "selected" ?> value="Kabupaten lingga"> KABUPATEN LINGGA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kepulauan anambas") echo "selected" ?> value="Kabupaten kepulauan anambas"> KABUPATEN KEPULAUAN ANAMBAS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota batam") echo "selected" ?> value="Kota batam"> KOTA BATAM</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota tanjung pinang") echo "selected" ?> value="Kota tanjung pinang"> KOTA TANJUNG PINANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kepulauan seribu") echo "selected" ?> value="Kabupaten kepulauan seribu"> KABUPATEN KEPULAUAN SERIBU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota jakarta selatan") echo "selected" ?> value="Kota jakarta selatan"> KOTA JAKARTA SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota jakarta timur") echo "selected" ?> value="Kota jakarta timur"> KOTA JAKARTA TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota jakarta pusat") echo "selected" ?> value="Kota jakarta pusat"> KOTA JAKARTA PUSAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota jakarta barat") echo "selected" ?> value="Kota jakarta barat"> KOTA JAKARTA BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota jakarta utara") echo "selected" ?> value="Kota jakarta utara"> KOTA JAKARTA UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bogor") echo "selected" ?> value="Kabupaten bogor"> KABUPATEN BOGOR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sukabumi") echo "selected" ?> value="Kabupaten sukabumi"> KABUPATEN SUKABUMI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten cianjur") echo "selected" ?> value="Kabupaten cianjur"> KABUPATEN CIANJUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bandung") echo "selected" ?> value="Kabupaten bandung"> KABUPATEN BANDUNG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten garut") echo "selected" ?> value="Kabupaten garut"> KABUPATEN GARUT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tasikmalaya") echo "selected" ?> value="Kabupaten tasikmalaya"> KABUPATEN TASIKMALAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten ciamis") echo "selected" ?> value="Kabupaten ciamis"> KABUPATEN CIAMIS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kuningan") echo "selected" ?> value="Kabupaten kuningan"> KABUPATEN KUNINGAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten cirebon") echo "selected" ?> value="Kabupaten cirebon"> KABUPATEN CIREBON</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten majalengka") echo "selected" ?> value="Kabupaten majalengka"> KABUPATEN MAJALENGKA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sumedang") echo "selected" ?> value="Kabupaten sumedang"> KABUPATEN SUMEDANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten indramayu") echo "selected" ?> value="Kabupaten indramayu"> KABUPATEN INDRAMAYU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten subang") echo "selected" ?> value="Kabupaten subang"> KABUPATEN SUBANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten purwakarta") echo "selected" ?> value="Kabupaten purwakarta"> KABUPATEN PURWAKARTA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten karawang") echo "selected" ?> value="Kabupaten karawang"> KABUPATEN KARAWANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bekasi") echo "selected" ?> value="Kabupaten bekasi"> KABUPATEN BEKASI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bandung barat") echo "selected" ?> value="Kabupaten bandung barat"> KABUPATEN BANDUNG BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pangandaran") echo "selected" ?> value="Kabupaten pangandaran"> KABUPATEN PANGANDARAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota bogor") echo "selected" ?> value="Kota bogor"> KOTA BOGOR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota sukabumi") echo "selected" ?> value="Kota sukabumi"> KOTA SUKABUMI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota bandung") echo "selected" ?> value="Kota bandung"> KOTA BANDUNG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota cirebon") echo "selected" ?> value="Kota cirebon"> KOTA CIREBON</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota bekasi") echo "selected" ?> value="Kota bekasi"> KOTA BEKASI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota depok") echo "selected" ?> value="Kota depok"> KOTA DEPOK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota cimahi") echo "selected" ?> value="Kota cimahi"> KOTA CIMAHI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota tasikmalaya") echo "selected" ?> value="Kota tasikmalaya"> KOTA TASIKMALAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota banjar") echo "selected" ?> value="Kota banjar"> KOTA BANJAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten cilacap") echo "selected" ?> value="Kabupaten cilacap"> KABUPATEN CILACAP</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten banyumas") echo "selected" ?> value="Kabupaten banyumas"> KABUPATEN BANYUMAS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten purbalingga") echo "selected" ?> value="Kabupaten purbalingga"> KABUPATEN PURBALINGGA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten banjarnegara") echo "selected" ?> value="Kabupaten banjarnegara"> KABUPATEN BANJARNEGARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kebumen") echo "selected" ?> value="Kabupaten kebumen"> KABUPATEN KEBUMEN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten purworejo") echo "selected" ?> value="Kabupaten purworejo"> KABUPATEN PURWOREJO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten wonosobo") echo "selected" ?> value="Kabupaten wonosobo"> KABUPATEN WONOSOBO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten magelang") echo "selected" ?> value="Kabupaten magelang"> KABUPATEN MAGELANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten boyolali") echo "selected" ?> value="Kabupaten boyolali"> KABUPATEN BOYOLALI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten klaten") echo "selected" ?> value="Kabupaten klaten"> KABUPATEN KLATEN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sukoharjo") echo "selected" ?> value="Kabupaten sukoharjo"> KABUPATEN SUKOHARJO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten wonogiri") echo "selected" ?> value="Kabupaten wonogiri"> KABUPATEN WONOGIRI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten karanganyar") echo "selected" ?> value="Kabupaten karanganyar"> KABUPATEN KARANGANYAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sragen") echo "selected" ?> value="Kabupaten sragen"> KABUPATEN SRAGEN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten grobogan") echo "selected" ?> value="Kabupaten grobogan"> KABUPATEN GROBOGAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten blora") echo "selected" ?> value="Kabupaten blora"> KABUPATEN BLORA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten rembang") echo "selected" ?> value="Kabupaten rembang"> KABUPATEN REMBANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pati") echo "selected" ?> value="Kabupaten pati"> KABUPATEN PATI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kudus") echo "selected" ?> value="Kabupaten kudus"> KABUPATEN KUDUS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten jepara") echo "selected" ?> value="Kabupaten jepara"> KABUPATEN JEPARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten demak") echo "selected" ?> value="Kabupaten demak"> KABUPATEN DEMAK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten temanggung") echo "selected" ?> value="Kabupaten temanggung"> KABUPATEN TEMANGGUNG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kendal") echo "selected" ?> value="Kabupaten kendal"> KABUPATEN KENDAL</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten batang") echo "selected" ?> value="Kabupaten batang"> KABUPATEN BATANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pekalongan") echo "selected" ?> value="Kabupaten pekalongan"> KABUPATEN PEKALONGAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pemalang") echo "selected" ?> value="Kabupaten pemalang"> KABUPATEN PEMALANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tegal") echo "selected" ?> value="Kabupaten tegal"> KABUPATEN TEGAL</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten brebes") echo "selected" ?> value="Kabupaten brebes"> KABUPATEN BREBES</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota magelang") echo "selected" ?> value="Kota magelang"> KOTA MAGELANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota surakarta") echo "selected" ?> value="Kota surakarta"> KOTA SURAKARTA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota salatiga") echo "selected" ?> value="Kota salatiga"> KOTA SALATIGA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota semarang") echo "selected" ?> value="Kota semarang"> KOTA SEMARANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota pekalongan") echo "selected" ?> value="Kota pekalongan"> KOTA PEKALONGAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota tegal") echo "selected" ?> value="Kota tegal"> KOTA TEGAL</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kulon progo") echo "selected" ?> value="Kabupaten kulon progo"> KABUPATEN KULON PROGO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bantul") echo "selected" ?> value="Kabupaten bantul"> KABUPATEN BANTUL</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten gunung kidul") echo "selected" ?> value="Kabupaten gunung kidul"> KABUPATEN GUNUNG KIDUL</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sleman") echo "selected" ?> value="Kabupaten sleman"> KABUPATEN SLEMAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota yogyakarta") echo "selected" ?> value="Kota yogyakarta"> KOTA YOGYAKARTA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pacitan") echo "selected" ?> value="Kabupaten pacitan"> KABUPATEN PACITAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten ponorogo") echo "selected" ?> value="Kabupaten ponorogo"> KABUPATEN PONOROGO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten trenggalek") echo "selected" ?> value="Kabupaten trenggalek"> KABUPATEN TRENGGALEK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tulungagung") echo "selected" ?> value="Kabupaten tulungagung"> KABUPATEN TULUNGAGUNG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten blitar") echo "selected" ?> value="Kabupaten blitar"> KABUPATEN BLITAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kediri") echo "selected" ?> value="Kabupaten kediri"> KABUPATEN KEDIRI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten malang") echo "selected" ?> value="Kabupaten malang"> KABUPATEN MALANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lumajang") echo "selected" ?> value="Kabupaten lumajang"> KABUPATEN LUMAJANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten jember") echo "selected" ?> value="Kabupaten jember"> KABUPATEN JEMBER</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten banyuwangi") echo "selected" ?> value="Kabupaten banyuwangi"> KABUPATEN BANYUWANGI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bondowoso") echo "selected" ?> value="Kabupaten bondowoso"> KABUPATEN BONDOWOSO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten situbondo") echo "selected" ?> value="Kabupaten situbondo"> KABUPATEN SITUBONDO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten probolinggo") echo "selected" ?> value="Kabupaten probolinggo"> KABUPATEN PROBOLINGGO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pasuruan") echo "selected" ?> value="Kabupaten pasuruan"> KABUPATEN PASURUAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sidoarjo") echo "selected" ?> value="Kabupaten sidoarjo"> KABUPATEN SIDOARJO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mojokerto") echo "selected" ?> value="Kabupaten mojokerto"> KABUPATEN MOJOKERTO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten jombang") echo "selected" ?> value="Kabupaten jombang"> KABUPATEN JOMBANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten nganjuk") echo "selected" ?> value="Kabupaten nganjuk"> KABUPATEN NGANJUK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten madiun") echo "selected" ?> value="Kabupaten madiun"> KABUPATEN MADIUN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten magetan") echo "selected" ?> value="Kabupaten magetan"> KABUPATEN MAGETAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten ngawi") echo "selected" ?> value="Kabupaten ngawi"> KABUPATEN NGAWI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bojonegoro") echo "selected" ?> value="Kabupaten bojonegoro"> KABUPATEN BOJONEGORO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tuban") echo "selected" ?> value="Kabupaten tuban"> KABUPATEN TUBAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lamongan") echo "selected" ?> value="Kabupaten lamongan"> KABUPATEN LAMONGAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten gresik") echo "selected" ?> value="Kabupaten gresik"> KABUPATEN GRESIK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bangkalan") echo "selected" ?> value="Kabupaten bangkalan"> KABUPATEN BANGKALAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sampang") echo "selected" ?> value="Kabupaten sampang"> KABUPATEN SAMPANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pamekasan") echo "selected" ?> value="Kabupaten pamekasan"> KABUPATEN PAMEKASAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sumenep") echo "selected" ?> value="Kabupaten sumenep"> KABUPATEN SUMENEP</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota kediri") echo "selected" ?> value="Kota kediri"> KOTA KEDIRI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota blitar") echo "selected" ?> value="Kota blitar"> KOTA BLITAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota malang") echo "selected" ?> value="Kota malang"> KOTA MALANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota probolinggo") echo "selected" ?> value="Kota probolinggo"> KOTA PROBOLINGGO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota pasuruan") echo "selected" ?> value="Kota pasuruan"> KOTA PASURUAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota mojokerto") echo "selected" ?> value="Kota mojokerto"> KOTA MOJOKERTO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota madiun") echo "selected" ?> value="Kota madiun"> KOTA MADIUN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota surabaya") echo "selected" ?> value="Kota surabaya"> KOTA SURABAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota batu") echo "selected" ?> value="Kota batu"> KOTA BATU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pandeglang") echo "selected" ?> value="Kabupaten pandeglang"> KABUPATEN PANDEGLANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lebak") echo "selected" ?> value="Kabupaten lebak"> KABUPATEN LEBAK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tangerang") echo "selected" ?> value="Kabupaten tangerang"> KABUPATEN TANGERANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten serang") echo "selected" ?> value="Kabupaten serang"> KABUPATEN SERANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota tangerang") echo "selected" ?> value="Kota tangerang"> KOTA TANGERANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota cilegon") echo "selected" ?> value="Kota cilegon"> KOTA CILEGON</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota serang") echo "selected" ?> value="Kota serang"> KOTA SERANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota tangerang selatan") echo "selected" ?> value="Kota tangerang selatan"> KOTA TANGERANG SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten jembrana") echo "selected" ?> value="Kabupaten jembrana"> KABUPATEN JEMBRANA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tabanan") echo "selected" ?> value="Kabupaten tabanan"> KABUPATEN TABANAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten badung") echo "selected" ?> value="Kabupaten badung"> KABUPATEN BADUNG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten gianyar") echo "selected" ?> value="Kabupaten gianyar"> KABUPATEN GIANYAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten klungkung") echo "selected" ?> value="Kabupaten klungkung"> KABUPATEN KLUNGKUNG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bangli") echo "selected" ?> value="Kabupaten bangli"> KABUPATEN BANGLI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten karang asem") echo "selected" ?> value="Kabupaten karang asem"> KABUPATEN KARANG ASEM</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten buleleng") echo "selected" ?> value="Kabupaten buleleng"> KABUPATEN BULELENG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota denpasar") echo "selected" ?> value="Kota denpasar"> KOTA DENPASAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lombok barat") echo "selected" ?> value="Kabupaten lombok barat"> KABUPATEN LOMBOK BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lombok tengah") echo "selected" ?> value="Kabupaten lombok tengah"> KABUPATEN LOMBOK TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lombok timur") echo "selected" ?> value="Kabupaten lombok timur"> KABUPATEN LOMBOK TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sumbawa") echo "selected" ?> value="Kabupaten sumbawa"> KABUPATEN SUMBAWA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten dompu") echo "selected" ?> value="Kabupaten dompu"> KABUPATEN DOMPU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bima") echo "selected" ?> value="Kabupaten bima"> KABUPATEN BIMA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sumbawa barat") echo "selected" ?> value="Kabupaten sumbawa barat"> KABUPATEN SUMBAWA BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lombok utara") echo "selected" ?> value="Kabupaten lombok utara"> KABUPATEN LOMBOK UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota mataram") echo "selected" ?> value="Kota mataram"> KOTA MATARAM</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota bima") echo "selected" ?> value="Kota bima"> KOTA BIMA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sumba barat") echo "selected" ?> value="Kabupaten sumba barat"> KABUPATEN SUMBA BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sumba timur") echo "selected" ?> value="Kabupaten sumba timur"> KABUPATEN SUMBA TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kupang") echo "selected" ?> value="Kabupaten kupang"> KABUPATEN KUPANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten timor tengah selatan") echo "selected" ?> value="Kabupaten timor tengah selatan"> KABUPATEN TIMOR TENGAH SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten timor tengah utara") echo "selected" ?> value="Kabupaten timor tengah utara"> KABUPATEN TIMOR TENGAH UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten belu") echo "selected" ?> value="Kabupaten belu"> KABUPATEN BELU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten alor") echo "selected" ?> value="Kabupaten alor"> KABUPATEN ALOR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lembata") echo "selected" ?> value="Kabupaten lembata"> KABUPATEN LEMBATA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten flores timur") echo "selected" ?> value="Kabupaten flores timur"> KABUPATEN FLORES TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sikka") echo "selected" ?> value="Kabupaten sikka"> KABUPATEN SIKKA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten ende") echo "selected" ?> value="Kabupaten ende"> KABUPATEN ENDE</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten ngada") echo "selected" ?> value="Kabupaten ngada"> KABUPATEN NGADA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten manggarai") echo "selected" ?> value="Kabupaten manggarai"> KABUPATEN MANGGARAI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten rote ndao") echo "selected" ?> value="Kabupaten rote ndao"> KABUPATEN ROTE NDAO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten manggarai barat") echo "selected" ?> value="Kabupaten manggarai barat"> KABUPATEN MANGGARAI BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sumba tengah") echo "selected" ?> value="Kabupaten sumba tengah"> KABUPATEN SUMBA TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sumba barat daya") echo "selected" ?> value="Kabupaten sumba barat daya"> KABUPATEN SUMBA BARAT DAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten nagekeo") echo "selected" ?> value="Kabupaten nagekeo"> KABUPATEN NAGEKEO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten manggarai timur") echo "selected" ?> value="Kabupaten manggarai timur"> KABUPATEN MANGGARAI TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sabu raijua") echo "selected" ?> value="Kabupaten sabu raijua"> KABUPATEN SABU RAIJUA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten malaka") echo "selected" ?> value="Kabupaten malaka"> KABUPATEN MALAKA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota kupang") echo "selected" ?> value="Kota kupang"> KOTA KUPANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sambas") echo "selected" ?> value="Kabupaten sambas"> KABUPATEN SAMBAS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bengkayang") echo "selected" ?> value="Kabupaten bengkayang"> KABUPATEN BENGKAYANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten landak") echo "selected" ?> value="Kabupaten landak"> KABUPATEN LANDAK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mempawah") echo "selected" ?> value="Kabupaten mempawah"> KABUPATEN MEMPAWAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sanggau") echo "selected" ?> value="Kabupaten sanggau"> KABUPATEN SANGGAU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten ketapang") echo "selected" ?> value="Kabupaten ketapang"> KABUPATEN KETAPANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sintang") echo "selected" ?> value="Kabupaten sintang"> KABUPATEN SINTANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kapuas hulu") echo "selected" ?> value="Kabupaten kapuas hulu"> KABUPATEN KAPUAS HULU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sekadau") echo "selected" ?> value="Kabupaten sekadau"> KABUPATEN SEKADAU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten melawi") echo "selected" ?> value="Kabupaten melawi"> KABUPATEN MELAWI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kayong utara") echo "selected" ?> value="Kabupaten kayong utara"> KABUPATEN KAYONG UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kubu raya") echo "selected" ?> value="Kabupaten kubu raya"> KABUPATEN KUBU RAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota pontianak") echo "selected" ?> value="Kota pontianak"> KOTA PONTIANAK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota singkawang") echo "selected" ?> value="Kota singkawang"> KOTA SINGKAWANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kotawaringin barat") echo "selected" ?> value="Kabupaten kotawaringin barat"> KABUPATEN KOTAWARINGIN BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kotawaringin timur") echo "selected" ?> value="Kabupaten kotawaringin timur"> KABUPATEN KOTAWARINGIN TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kapuas") echo "selected" ?> value="Kabupaten kapuas"> KABUPATEN KAPUAS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten barito selatan") echo "selected" ?> value="Kabupaten barito selatan"> KABUPATEN BARITO SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten barito utara") echo "selected" ?> value="Kabupaten barito utara"> KABUPATEN BARITO UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sukamara") echo "selected" ?> value="Kabupaten sukamara"> KABUPATEN SUKAMARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lamandau") echo "selected" ?> value="Kabupaten lamandau"> KABUPATEN LAMANDAU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten seruyan") echo "selected" ?> value="Kabupaten seruyan"> KABUPATEN SERUYAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten katingan") echo "selected" ?> value="Kabupaten katingan"> KABUPATEN KATINGAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pulang pisau") echo "selected" ?> value="Kabupaten pulang pisau"> KABUPATEN PULANG PISAU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten gunung mas") echo "selected" ?> value="Kabupaten gunung mas"> KABUPATEN GUNUNG MAS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten barito timur") echo "selected" ?> value="Kabupaten barito timur"> KABUPATEN BARITO TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten murung raya") echo "selected" ?> value="Kabupaten murung raya"> KABUPATEN MURUNG RAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota palangka raya") echo "selected" ?> value="Kota palangka raya"> KOTA PALANGKA RAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tanah laut") echo "selected" ?> value="Kabupaten tanah laut"> KABUPATEN TANAH LAUT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kota baru") echo "selected" ?> value="Kabupaten kota baru"> KABUPATEN KOTA BARU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten banjar") echo "selected" ?> value="Kabupaten banjar"> KABUPATEN BANJAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten barito kuala") echo "selected" ?> value="Kabupaten barito kuala"> KABUPATEN BARITO KUALA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tapin") echo "selected" ?> value="Kabupaten tapin"> KABUPATEN TAPIN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten hulu sungai selatan") echo "selected" ?> value="Kabupaten hulu sungai selatan"> KABUPATEN HULU SUNGAI SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten hulu sungai tengah") echo "selected" ?> value="Kabupaten hulu sungai tengah"> KABUPATEN HULU SUNGAI TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten hulu sungai utara") echo "selected" ?> value="Kabupaten hulu sungai utara"> KABUPATEN HULU SUNGAI UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tabalong") echo "selected" ?> value="Kabupaten tabalong"> KABUPATEN TABALONG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tanah bumbu") echo "selected" ?> value="Kabupaten tanah bumbu"> KABUPATEN TANAH BUMBU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten balangan") echo "selected" ?> value="Kabupaten balangan"> KABUPATEN BALANGAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota banjarmasin") echo "selected" ?> value="Kota banjarmasin"> KOTA BANJARMASIN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota banjar baru") echo "selected" ?> value="Kota banjar baru"> KOTA BANJAR BARU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten paser") echo "selected" ?> value="Kabupaten paser"> KABUPATEN PASER</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kutai barat") echo "selected" ?> value="Kabupaten kutai barat"> KABUPATEN KUTAI BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kutai kartanegara") echo "selected" ?> value="Kabupaten kutai kartanegara"> KABUPATEN KUTAI KARTANEGARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kutai timur") echo "selected" ?> value="Kabupaten kutai timur"> KABUPATEN KUTAI TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten berau") echo "selected" ?> value="Kabupaten berau"> KABUPATEN BERAU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten penajam paser utara") echo "selected" ?> value="Kabupaten penajam paser utara"> KABUPATEN PENAJAM PASER UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mahakam hulu") echo "selected" ?> value="Kabupaten mahakam hulu"> KABUPATEN MAHAKAM HULU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota balikpapan") echo "selected" ?> value="Kota balikpapan"> KOTA BALIKPAPAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota samarinda") echo "selected" ?> value="Kota samarinda"> KOTA SAMARINDA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota bontang") echo "selected" ?> value="Kota bontang"> KOTA BONTANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten malinau") echo "selected" ?> value="Kabupaten malinau"> KABUPATEN MALINAU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bulungan") echo "selected" ?> value="Kabupaten bulungan"> KABUPATEN BULUNGAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tana tidung") echo "selected" ?> value="Kabupaten tana tidung"> KABUPATEN TANA TIDUNG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten nunukan") echo "selected" ?> value="Kabupaten nunukan"> KABUPATEN NUNUKAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota tarakan") echo "selected" ?> value="Kota tarakan"> KOTA TARAKAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bolaang mongondow") echo "selected" ?> value="Kabupaten bolaang mongondow"> KABUPATEN BOLAANG MONGONDOW</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten minahasa") echo "selected" ?> value="Kabupaten minahasa"> KABUPATEN MINAHASA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kepulauan sangihe") echo "selected" ?> value="Kabupaten kepulauan sangihe"> KABUPATEN KEPULAUAN SANGIHE</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kepulauan talaud") echo "selected" ?> value="Kabupaten kepulauan talaud"> KABUPATEN KEPULAUAN TALAUD</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten minahasa selatan") echo "selected" ?> value="Kabupaten minahasa selatan"> KABUPATEN MINAHASA SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten minahasa utara") echo "selected" ?> value="Kabupaten minahasa utara"> KABUPATEN MINAHASA UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bolaang mongondow utara") echo "selected" ?> value="Kabupaten bolaang mongondow utara"> KABUPATEN BOLAANG MONGONDOW UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten siau tagulandang biaro") echo "selected" ?> value="Kabupaten siau tagulandang biaro"> KABUPATEN SIAU TAGULANDANG BIARO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten minahasa tenggara") echo "selected" ?> value="Kabupaten minahasa tenggara"> KABUPATEN MINAHASA TENGGARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bolaang mongondow selatan") echo "selected" ?> value="Kabupaten bolaang mongondow selatan"> KABUPATEN BOLAANG MONGONDOW SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bolaang mongondow timur") echo "selected" ?> value="Kabupaten bolaang mongondow timur"> KABUPATEN BOLAANG MONGONDOW TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota manado") echo "selected" ?> value="Kota manado"> KOTA MANADO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota bitung") echo "selected" ?> value="Kota bitung"> KOTA BITUNG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota tomohon") echo "selected" ?> value="Kota tomohon"> KOTA TOMOHON</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota kotamobagu") echo "selected" ?> value="Kota kotamobagu"> KOTA KOTAMOBAGU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten banggai kepulauan") echo "selected" ?> value="Kabupaten banggai kepulauan"> KABUPATEN BANGGAI KEPULAUAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten banggai") echo "selected" ?> value="Kabupaten banggai"> KABUPATEN BANGGAI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten morowali") echo "selected" ?> value="Kabupaten morowali"> KABUPATEN MOROWALI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten poso") echo "selected" ?> value="Kabupaten poso"> KABUPATEN POSO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten donggala") echo "selected" ?> value="Kabupaten donggala"> KABUPATEN DONGGALA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten toli-toli") echo "selected" ?> value="Kabupaten toli-toli"> KABUPATEN TOLI-TOLI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten buol") echo "selected" ?> value="Kabupaten buol"> KABUPATEN BUOL</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten parigi moutong") echo "selected" ?> value="Kabupaten parigi moutong"> KABUPATEN PARIGI MOUTONG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tojo una-una") echo "selected" ?> value="Kabupaten tojo una-una"> KABUPATEN TOJO UNA-UNA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sigi") echo "selected" ?> value="Kabupaten sigi"> KABUPATEN SIGI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten banggai laut") echo "selected" ?> value="Kabupaten banggai laut"> KABUPATEN BANGGAI LAUT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten morowali utara") echo "selected" ?> value="Kabupaten morowali utara"> KABUPATEN MOROWALI UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota palu") echo "selected" ?> value="Kota palu"> KOTA PALU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kepulauan selayar") echo "selected" ?> value="Kabupaten kepulauan selayar"> KABUPATEN KEPULAUAN SELAYAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bulukumba") echo "selected" ?> value="Kabupaten bulukumba"> KABUPATEN BULUKUMBA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bantaeng") echo "selected" ?> value="Kabupaten bantaeng"> KABUPATEN BANTAENG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten jeneponto") echo "selected" ?> value="Kabupaten jeneponto"> KABUPATEN JENEPONTO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten takalar") echo "selected" ?> value="Kabupaten takalar"> KABUPATEN TAKALAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten gowa") echo "selected" ?> value="Kabupaten gowa"> KABUPATEN GOWA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sinjai") echo "selected" ?> value="Kabupaten sinjai"> KABUPATEN SINJAI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten maros") echo "selected" ?> value="Kabupaten maros"> KABUPATEN MAROS</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pangkajene dan kepulauan") echo "selected" ?> value="Kabupaten pangkajene dan kepulauan"> KABUPATEN PANGKAJENE DAN KEPULAUAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten barru") echo "selected" ?> value="Kabupaten barru"> KABUPATEN BARRU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bone") echo "selected" ?> value="Kabupaten bone"> KABUPATEN BONE</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten soppeng") echo "selected" ?> value="Kabupaten soppeng"> KABUPATEN SOPPENG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten wajo") echo "selected" ?> value="Kabupaten wajo"> KABUPATEN WAJO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sidenreng rappang") echo "selected" ?> value="Kabupaten sidenreng rappang"> KABUPATEN SIDENRENG RAPPANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pinrang") echo "selected" ?> value="Kabupaten pinrang"> KABUPATEN PINRANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten enrekang") echo "selected" ?> value="Kabupaten enrekang"> KABUPATEN ENREKANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten luwu") echo "selected" ?> value="Kabupaten luwu"> KABUPATEN LUWU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tana toraja") echo "selected" ?> value="Kabupaten tana toraja"> KABUPATEN TANA TORAJA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten luwu utara") echo "selected" ?> value="Kabupaten luwu utara"> KABUPATEN LUWU UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten luwu timur") echo "selected" ?> value="Kabupaten luwu timur"> KABUPATEN LUWU TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten toraja utara") echo "selected" ?> value="Kabupaten toraja utara"> KABUPATEN TORAJA UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota makassar") echo "selected" ?> value="Kota makassar"> KOTA MAKASSAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota parepare") echo "selected" ?> value="Kota parepare"> KOTA PAREPARE</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota palopo") echo "selected" ?> value="Kota palopo"> KOTA PALOPO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten buton") echo "selected" ?> value="Kabupaten buton"> KABUPATEN BUTON</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten muna") echo "selected" ?> value="Kabupaten muna"> KABUPATEN MUNA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten konawe") echo "selected" ?> value="Kabupaten konawe"> KABUPATEN KONAWE</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kolaka") echo "selected" ?> value="Kabupaten kolaka"> KABUPATEN KOLAKA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten konawe selatan") echo "selected" ?> value="Kabupaten konawe selatan"> KABUPATEN KONAWE SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bombana") echo "selected" ?> value="Kabupaten bombana"> KABUPATEN BOMBANA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten wakatobi") echo "selected" ?> value="Kabupaten wakatobi"> KABUPATEN WAKATOBI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kolaka utara") echo "selected" ?> value="Kabupaten kolaka utara"> KABUPATEN KOLAKA UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten buton utara") echo "selected" ?> value="Kabupaten buton utara"> KABUPATEN BUTON UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten konawe utara") echo "selected" ?> value="Kabupaten konawe utara"> KABUPATEN KONAWE UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kolaka timur") echo "selected" ?> value="Kabupaten kolaka timur"> KABUPATEN KOLAKA TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten konawe kepulauan") echo "selected" ?> value="Kabupaten konawe kepulauan"> KABUPATEN KONAWE KEPULAUAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten muna barat") echo "selected" ?> value="Kabupaten muna barat"> KABUPATEN MUNA BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten buton tengah") echo "selected" ?> value="Kabupaten buton tengah"> KABUPATEN BUTON TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten buton selatan") echo "selected" ?> value="Kabupaten buton selatan"> KABUPATEN BUTON SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota kendari") echo "selected" ?> value="Kota kendari"> KOTA KENDARI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota baubau") echo "selected" ?> value="Kota baubau"> KOTA BAUBAU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten boalemo") echo "selected" ?> value="Kabupaten boalemo"> KABUPATEN BOALEMO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten gorontalo") echo "selected" ?> value="Kabupaten gorontalo"> KABUPATEN GORONTALO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pohuwato") echo "selected" ?> value="Kabupaten pohuwato"> KABUPATEN POHUWATO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten bone bolango") echo "selected" ?> value="Kabupaten bone bolango"> KABUPATEN BONE BOLANGO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten gorontalo utara") echo "selected" ?> value="Kabupaten gorontalo utara"> KABUPATEN GORONTALO UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota gorontalo") echo "selected" ?> value="Kota gorontalo"> KOTA GORONTALO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten majene") echo "selected" ?> value="Kabupaten majene"> KABUPATEN MAJENE</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten polewali mandar") echo "selected" ?> value="Kabupaten polewali mandar"> KABUPATEN POLEWALI MANDAR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mamasa") echo "selected" ?> value="Kabupaten mamasa"> KABUPATEN MAMASA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mamuju") echo "selected" ?> value="Kabupaten mamuju"> KABUPATEN MAMUJU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mamuju utara") echo "selected" ?> value="Kabupaten mamuju utara"> KABUPATEN MAMUJU UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mamuju tengah") echo "selected" ?> value="Kabupaten mamuju tengah"> KABUPATEN MAMUJU TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten maluku tenggara barat") echo "selected" ?> value="Kabupaten maluku tenggara barat"> KABUPATEN MALUKU TENGGARA BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten maluku tenggara") echo "selected" ?> value="Kabupaten maluku tenggara"> KABUPATEN MALUKU TENGGARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten maluku tengah") echo "selected" ?> value="Kabupaten maluku tengah"> KABUPATEN MALUKU TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten buru") echo "selected" ?> value="Kabupaten buru"> KABUPATEN BURU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kepulauan aru") echo "selected" ?> value="Kabupaten kepulauan aru"> KABUPATEN KEPULAUAN ARU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten seram bagian barat") echo "selected" ?> value="Kabupaten seram bagian barat"> KABUPATEN SERAM BAGIAN BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten seram bagian timur") echo "selected" ?> value="Kabupaten seram bagian timur"> KABUPATEN SERAM BAGIAN TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten maluku barat daya") echo "selected" ?> value="Kabupaten maluku barat daya"> KABUPATEN MALUKU BARAT DAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten buru selatan") echo "selected" ?> value="Kabupaten buru selatan"> KABUPATEN BURU SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota ambon") echo "selected" ?> value="Kota ambon"> KOTA AMBON</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota tual") echo "selected" ?> value="Kota tual"> KOTA TUAL</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten halmahera barat") echo "selected" ?> value="Kabupaten halmahera barat"> KABUPATEN HALMAHERA BARAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten halmahera tengah") echo "selected" ?> value="Kabupaten halmahera tengah"> KABUPATEN HALMAHERA TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kepulauan sula") echo "selected" ?> value="Kabupaten kepulauan sula"> KABUPATEN KEPULAUAN SULA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten halmahera selatan") echo "selected" ?> value="Kabupaten halmahera selatan"> KABUPATEN HALMAHERA SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten halmahera utara") echo "selected" ?> value="Kabupaten halmahera utara"> KABUPATEN HALMAHERA UTARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten halmahera timur") echo "selected" ?> value="Kabupaten halmahera timur"> KABUPATEN HALMAHERA TIMUR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pulau morotai") echo "selected" ?> value="Kabupaten pulau morotai"> KABUPATEN PULAU MOROTAI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pulau taliabu") echo "selected" ?> value="Kabupaten pulau taliabu"> KABUPATEN PULAU TALIABU</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota ternate") echo "selected" ?> value="Kota ternate"> KOTA TERNATE</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota tidore kepulauan") echo "selected" ?> value="Kota tidore kepulauan"> KOTA TIDORE KEPULAUAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten fakfak") echo "selected" ?> value="Kabupaten fakfak"> KABUPATEN FAKFAK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kaimana") echo "selected" ?> value="Kabupaten kaimana"> KABUPATEN KAIMANA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten teluk wondama") echo "selected" ?> value="Kabupaten teluk wondama"> KABUPATEN TELUK WONDAMA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten teluk bintuni") echo "selected" ?> value="Kabupaten teluk bintuni"> KABUPATEN TELUK BINTUNI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten manokwari") echo "selected" ?> value="Kabupaten manokwari"> KABUPATEN MANOKWARI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sorong selatan") echo "selected" ?> value="Kabupaten sorong selatan"> KABUPATEN SORONG SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sorong") echo "selected" ?> value="Kabupaten sorong"> KABUPATEN SORONG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten raja ampat") echo "selected" ?> value="Kabupaten raja ampat"> KABUPATEN RAJA AMPAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tambrauw") echo "selected" ?> value="Kabupaten tambrauw"> KABUPATEN TAMBRAUW</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten maybrat") echo "selected" ?> value="Kabupaten maybrat"> KABUPATEN MAYBRAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten manokwari selatan") echo "selected" ?> value="Kabupaten manokwari selatan"> KABUPATEN MANOKWARI SELATAN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pegunungan arfak") echo "selected" ?> value="Kabupaten pegunungan arfak"> KABUPATEN PEGUNUNGAN ARFAK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota sorong") echo "selected" ?> value="Kota sorong"> KOTA SORONG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten merauke") echo "selected" ?> value="Kabupaten merauke"> KABUPATEN MERAUKE</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten jayawijaya") echo "selected" ?> value="Kabupaten jayawijaya"> KABUPATEN JAYAWIJAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten jayapura") echo "selected" ?> value="Kabupaten jayapura"> KABUPATEN JAYAPURA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten nabire") echo "selected" ?> value="Kabupaten nabire"> KABUPATEN NABIRE</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten kepulauan yapen") echo "selected" ?> value="Kabupaten kepulauan yapen"> KABUPATEN KEPULAUAN YAPEN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten biak numfor") echo "selected" ?> value="Kabupaten biak numfor"> KABUPATEN BIAK NUMFOR</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten paniai") echo "selected" ?> value="Kabupaten paniai"> KABUPATEN PANIAI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten puncak jaya") echo "selected" ?> value="Kabupaten puncak jaya"> KABUPATEN PUNCAK JAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mimika") echo "selected" ?> value="Kabupaten mimika"> KABUPATEN MIMIKA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten boven digoel") echo "selected" ?> value="Kabupaten boven digoel"> KABUPATEN BOVEN DIGOEL</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mappi") echo "selected" ?> value="Kabupaten mappi"> KABUPATEN MAPPI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten asmat") echo "selected" ?> value="Kabupaten asmat"> KABUPATEN ASMAT</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten yahukimo") echo "selected" ?> value="Kabupaten yahukimo"> KABUPATEN YAHUKIMO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten pegunungan bintang") echo "selected" ?> value="Kabupaten pegunungan bintang"> KABUPATEN PEGUNUNGAN BINTANG</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten tolikara") echo "selected" ?> value="Kabupaten tolikara"> KABUPATEN TOLIKARA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten sarmi") echo "selected" ?> value="Kabupaten sarmi"> KABUPATEN SARMI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten keerom") echo "selected" ?> value="Kabupaten keerom"> KABUPATEN KEEROM</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten waropen") echo "selected" ?> value="Kabupaten waropen"> KABUPATEN WAROPEN</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten supiori") echo "selected" ?> value="Kabupaten supiori"> KABUPATEN SUPIORI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mamberamo raya") echo "selected" ?> value="Kabupaten mamberamo raya"> KABUPATEN MAMBERAMO RAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten nduga") echo "selected" ?> value="Kabupaten nduga"> KABUPATEN NDUGA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten lanny jaya") echo "selected" ?> value="Kabupaten lanny jaya"> KABUPATEN LANNY JAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten mamberamo tengah") echo "selected" ?> value="Kabupaten mamberamo tengah"> KABUPATEN MAMBERAMO TENGAH</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten yalimo") echo "selected" ?> value="Kabupaten yalimo"> KABUPATEN YALIMO</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten puncak") echo "selected" ?> value="Kabupaten puncak"> KABUPATEN PUNCAK</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten dogiyai") echo "selected" ?> value="Kabupaten dogiyai"> KABUPATEN DOGIYAI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten intan jaya") echo "selected" ?> value="Kabupaten intan jaya"> KABUPATEN INTAN JAYA</option>

                    <option <?php if ($myprofile['kotalahir']=="Kabupaten deiyai") echo "selected" ?> value="Kabupaten deiyai"> KABUPATEN DEIYAI</option>

                    <option <?php if ($myprofile['kotalahir']=="Kota jayapura") echo "selected" ?> value="Kota jayapura"> KOTA JAYAPURA</option>

                    <option <?php if ($myprofile['kotalahir']=="Surabaya timur") echo "selected" ?> value="Surabaya timur"> SURABAYA TIMUR</option>            

                  </select>

                        </div>

                        </div>

                    </div>

                    <div class="col-lg-6">

                      <div class="form-group">

                        <label class="form-control-label" for="input-first-name"><?= lang('Global.age') ?>*</label>

                        <?php 

                        $birthDate = new DateTime($myprofile['birthdate']);

                        $today = new DateTime("today");

                        if ($birthDate > $today) { 

                            // exit("0 tahun 0 bulan 0 hari");

                        }

                        $y = $today->diff($birthDate)->y;

                        $m = $today->diff($birthDate)->m;

                        $d = $today->diff($birthDate)->d;

                        ?>

                        <input type="text" id="birthplace" name="birthplace" class="form-control" placeholder="Usia" value="<?= $y." tahun ".$m." bulan ".$d." hari" ?>" disabled>

                      </div>

                    </div>

                    <div class="col-lg-6">

                      <div class="form-group">

                        <label for="example-datetime-local-input" class="form-control-label"><?= lang('Global.birth_date') ?> *</label>

                        <input class="form-control" type="date" value="<?= $myprofile['birthdate']; ?>" id="birthdate" name="birthdate" disabled>

                      </div>

                      <small style="color:#fb6340" id="age_alert"></small>

                    </div>                    

                  </div>

                </div>

                <hr class="mb-4" />

                <!-- Address -->

                <h6 class="heading-small text-muted mb-4 pt-1"><?= lang('Global.education') ?></h6>



                <div class="">

                  <div class="row">

                    <!-- <div class="col-lg-12">

                      <div class="form-group">

                        <div class="form-group">

                          <label for="inputState" style="font-size: 14px; color: #8898aa">Pendidikan terakhir *</label>

                          <select id="pend" class="form-control js-example-basic-single " name="pendterakhir" required disabled="" tabindex="-1" aria-hidden="true" disabled>

                            <option value="" selected="">Pilih disini...</option>

                            <option <?php if($myprofile['pendterakhir']=="2") echo "selected"; ?>  value="2"> SMA 1/YEAR 10</option>

                            <option <?php if($myprofile['pendterakhir']=="3") echo "selected"; ?> value="3"> SMA 2/YEAR 11</option>

                            <option <?php if($myprofile['pendterakhir']=="4") echo "selected"; ?> value="4"> SMA 3/YEAR 12</option>

                            <option <?php if($myprofile['pendterakhir']=="5") echo "selected"; ?> value="5"> FOUNDATION</option>

                            <option <?php if($myprofile['pendterakhir']=="6") echo "selected"; ?> value="6"> DIPLOMA</option>

                            <option <?php if($myprofile['pendterakhir']=="7") echo "selected"; ?> value="7"> STRATA 1</option>

                            <option <?php if($myprofile['pendterakhir']=="8") echo "selected"; ?> value="8"> STRATA 2</option>

                          </select>

                        </div>

                        </div>

                    </div> -->

                    <div class="col-lg-12">

                      <div class="form-group">

                        <div class="form-group">

                          <!-- <label for="inputState" style="font-size: 14px; color: #8898aa"><?=lang('Global.education_now');?> *</label> -->

                          <label class="form-control-label" for="input-email">Pendidikan Saat Ini*</label>

                          <select id="education_level" name="education_level" class="form-control" disabled>

                            <option><?=lang('Global.choose');?>...</option>

                            <!-- <option <?php if($myprofile['education_level']=='SMP') echo 'selected'; ?>>SMP</option> -->

                            <option <?php if($myprofile['education_level']=='SMA') echo 'selected'; ?>>SMA</option>

                            <option <?php if($myprofile['education_level']=='Universitas') echo 'selected'; ?>>Universitas</option>

                          </select>

                        </div>

                        </div>

                    </div>

                    <div class="col-lg-12">

                      <div class="form-group">

                        <div class="form-group">

                        <label class="form-control-label" for="input-email"><?=lang('Global.semester');?>*</label>

                        <select class="form-control" name="semester" id="semester" required disabled>

                          <option selected disabled value="" class=""><?=lang('Global.choose');?>...</option>

                            <option <?php if ($myprofile['semester']=="7") echo "selected" ?> value="7" class="SMP">7</option>

                            <option <?php if ($myprofile['semester']=="8") echo "selected" ?> value="8" class="SMP">8</option>

                            <option <?php if ($myprofile['semester']=="9") echo "selected" ?> value="9" class="SMP">9</option>

                            <option <?php if ($myprofile['semester']=="10") echo "selected" ?> value="10" class="SMA">10</option>

                            <option <?php if ($myprofile['semester']=="11") echo "selected" ?> value="11" class="SMA">11</option>

                            <option <?php if ($myprofile['semester']=="12") echo "selected" ?> value="12" class="SMA">12</option>

                            <option <?php if ($myprofile['semester']=="1") echo "selected" ?> value="1" class="Universitas">1</option>

                            <option <?php if ($myprofile['semester']=="2") echo "selected" ?> value="2" class="Universitas">2</option>

                            <option <?php if ($myprofile['semester']=="3") echo "selected" ?> value="3" class="Universitas">3</option>

                            <option <?php if ($myprofile['semester']=="4") echo "selected" ?> value="4" class="Universitas">4</option>

                            <option <?php if ($myprofile['semester']=="5") echo "selected" ?> value="5" class="Universitas">5</option>

                            <option <?php if ($myprofile['semester']=="6") echo "selected" ?> value="6" class="Universitas">6</option>

                            <option <?php if ($myprofile['semester']=="7") echo "selected" ?> value="7" class="Universitas">7</option>

                            <option <?php if ($myprofile['semester']=="8") echo "selected" ?> value="8" class="Universitas">8</option>

                            <option <?php if ($myprofile['semester']==">8") echo "selected" ?> value=">8" class="Universitas">>8</option>

                          </select>

                        </div>

                        </div>

                    </div>

                    <div class="col-lg-12">

                      <div class="form-group">

                        <label class="form-control-label" for="input-email"><?=lang('Global.company');?>*</label>

                    <input class="form-control" placeholder="<?=lang('Global.company');?>" type="text" id="company" name="company" value="<?= $myprofile['company']?>">

                      </div>

                    </div>

                  </div>

                  <!-- <div class="row">

                    <div class="col-lg-4">

                      <div class="form-group">

                        <label class="form-control-label" for="input-city"><?= lang('Global.kota') ?> *</label>

                        <select class="form-control" id="input-city">

                          <option>Yogyakarta</option>

                          <option>Palembang</option>

                          <option>Surabaya</option>

                          <option>Semarang</option>

                          <option>Jakarta</option>

                        </select>

                      </div>

                    </div>

                    <div class="col-lg-4">

                      <div class="form-group">

                        <label class="form-control-label" for="input-kab"><?= lang('Global.kota_kab') ?> *</label>

                        <input type="text" id="input-kab" class="form-control" placeholder="Kabupaten" value="Sleman">

                      </div>

                    </div>

                    <div class="col-lg-4">

                      <div class="form-group">

                        <label class="form-control-label" for="input-country"><?= lang('Global.kodepos') ?> *</label>

                        <input type="number" id="input-postal-code" class="form-control" placeholder="Postal code">

                      </div>

                    </div>

                  </div> -->

                </div>

                <hr class="mb-4" />

                <!-- Address -->

                <h6 class="heading-small text-muted mb-4 pt-1"><?= lang('Global.contact_address') ?></h6>

                <div class="">

                  <div class="row">

                    <div class="col-lg-12">

                      <div class="form-group">

                        <label class="form-control-label" for="input-nohp"><?= lang('Global.phone') ?>*</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                          <span class="input-group-text">+62</span>

                        </div>

                            <input type="text" id="phone" name="phone" class="phone_number form-control" placeholder="Nomor Telepon / WA" aria-label="nohp" aria-describedby="basic-addon1" value="<?= $myprofile['phone']; ?>">

                          </div>

                        </div>

                      </div>

                    <div class="col-lg-12">

                      <div class="form-group">

                        <div class="form-group">

                          <label class="form-control-label" for="input-email">Domisili*</label>

                          <select class="form-control js-example-basic-single " name="domisili" id="domisili" required disabled="" tabindex="-1" aria-hidden="true" required disabled>

                    <option value="" selected="">Silahkan pilih...</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten simeulue") echo "selected" ?> value="Kabupaten simeulue"> KABUPATEN SIMEULUE</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten aceh singkil") echo "selected" ?> value="Kabupaten aceh singkil"> KABUPATEN ACEH SINGKIL</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten aceh selatan") echo "selected" ?> value="Kabupaten aceh selatan"> KABUPATEN ACEH SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten aceh tenggara") echo "selected" ?> value="Kabupaten aceh tenggara"> KABUPATEN ACEH TENGGARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten aceh timur") echo "selected" ?> value="Kabupaten aceh timur"> KABUPATEN ACEH TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten aceh tengah") echo "selected" ?> value="Kabupaten aceh tengah"> KABUPATEN ACEH TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten aceh barat") echo "selected" ?> value="Kabupaten aceh barat"> KABUPATEN ACEH BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten aceh besar") echo "selected" ?> value="Kabupaten aceh besar"> KABUPATEN ACEH BESAR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pidie") echo "selected" ?> value="Kabupaten pidie"> KABUPATEN PIDIE</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bireuen") echo "selected" ?> value="Kabupaten bireuen"> KABUPATEN BIREUEN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten aceh utara") echo "selected" ?> value="Kabupaten aceh utara"> KABUPATEN ACEH UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten aceh barat daya") echo "selected" ?> value="Kabupaten aceh barat daya"> KABUPATEN ACEH BARAT DAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten gayo lues") echo "selected" ?> value="Kabupaten gayo lues"> KABUPATEN GAYO LUES</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten aceh tamiang") echo "selected" ?> value="Kabupaten aceh tamiang"> KABUPATEN ACEH TAMIANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten nagan raya") echo "selected" ?> value="Kabupaten nagan raya"> KABUPATEN NAGAN RAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten aceh jaya") echo "selected" ?> value="Kabupaten aceh jaya"> KABUPATEN ACEH JAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bener meriah") echo "selected" ?> value="Kabupaten bener meriah"> KABUPATEN BENER MERIAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pidie jaya") echo "selected" ?> value="Kabupaten pidie jaya"> KABUPATEN PIDIE JAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kota banda aceh") echo "selected" ?> value="Kota banda aceh"> KOTA BANDA ACEH</option>

                    <option <?php if ($myprofile['domisili']=="Kota sabang") echo "selected" ?> value="Kota sabang"> KOTA SABANG</option>

                    <option <?php if ($myprofile['domisili']=="Kota langsa") echo "selected" ?> value="Kota langsa"> KOTA LANGSA</option>

                    <option <?php if ($myprofile['domisili']=="Kota lhokseumawe") echo "selected" ?> value="Kota lhokseumawe"> KOTA LHOKSEUMAWE</option>

                    <option <?php if ($myprofile['domisili']=="Kota subulussalam") echo "selected" ?> value="Kota subulussalam"> KOTA SUBULUSSALAM</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten nias") echo "selected" ?> value="Kabupaten nias"> KABUPATEN NIAS</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mandailing natal") echo "selected" ?> value="Kabupaten mandailing natal"> KABUPATEN MANDAILING NATAL</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tapanuli selatan") echo "selected" ?> value="Kabupaten tapanuli selatan"> KABUPATEN TAPANULI SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tapanuli tengah") echo "selected" ?> value="Kabupaten tapanuli tengah"> KABUPATEN TAPANULI TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tapanuli utara") echo "selected" ?> value="Kabupaten tapanuli utara"> KABUPATEN TAPANULI UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten toba samosir") echo "selected" ?> value="Kabupaten toba samosir"> KABUPATEN TOBA SAMOSIR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten labuhan batu") echo "selected" ?> value="Kabupaten labuhan batu"> KABUPATEN LABUHAN BATU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten asahan") echo "selected" ?> value="Kabupaten asahan"> KABUPATEN ASAHAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten simalungun") echo "selected" ?> value="Kabupaten simalungun"> KABUPATEN SIMALUNGUN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten dairi") echo "selected" ?> value="Kabupaten dairi"> KABUPATEN DAIRI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten karo") echo "selected" ?> value="Kabupaten karo"> KABUPATEN KARO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten deli serdang") echo "selected" ?> value="Kabupaten deli serdang"> KABUPATEN DELI SERDANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten langkat") echo "selected" ?> value="Kabupaten langkat"> KABUPATEN LANGKAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten nias selatan") echo "selected" ?> value="Kabupaten nias selatan"> KABUPATEN NIAS SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten humbang hasundutan") echo "selected" ?> value="Kabupaten humbang hasundutan"> KABUPATEN HUMBANG HASUNDUTAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pakpak bharat") echo "selected" ?> value="Kabupaten pakpak bharat"> KABUPATEN PAKPAK BHARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten samosir") echo "selected" ?> value="Kabupaten samosir"> KABUPATEN SAMOSIR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten serdang bedagai") echo "selected" ?> value="Kabupaten serdang bedagai"> KABUPATEN SERDANG BEDAGAI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten batu bara") echo "selected" ?> value="Kabupaten batu bara"> KABUPATEN BATU BARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten padang lawas utara") echo "selected" ?> value="Kabupaten padang lawas utara"> KABUPATEN PADANG LAWAS UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten padang lawas") echo "selected" ?> value="Kabupaten padang lawas"> KABUPATEN PADANG LAWAS</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten labuhan batu selatan") echo "selected" ?> value="Kabupaten labuhan batu selatan"> KABUPATEN LABUHAN BATU SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten labuhan batu utara") echo "selected" ?> value="Kabupaten labuhan batu utara"> KABUPATEN LABUHAN BATU UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten nias utara") echo "selected" ?> value="Kabupaten nias utara"> KABUPATEN NIAS UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten nias barat") echo "selected" ?> value="Kabupaten nias barat"> KABUPATEN NIAS BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kota sibolga") echo "selected" ?> value="Kota sibolga"> KOTA SIBOLGA</option>

                    <option <?php if ($myprofile['domisili']=="Kota tanjung balai") echo "selected" ?> value="Kota tanjung balai"> KOTA TANJUNG BALAI</option>

                    <option <?php if ($myprofile['domisili']=="Kota pematang siantar") echo "selected" ?> value="Kota pematang siantar"> KOTA PEMATANG SIANTAR</option>

                    <option <?php if ($myprofile['domisili']=="Kota tebing tinggi") echo "selected" ?> value="Kota tebing tinggi"> KOTA TEBING TINGGI</option>

                    <option <?php if ($myprofile['domisili']=="Kota medan") echo "selected" ?> value="Kota medan"> KOTA MEDAN</option>

                    <option <?php if ($myprofile['domisili']=="Kota binjai") echo "selected" ?> value="Kota binjai"> KOTA BINJAI</option>

                    <option <?php if ($myprofile['domisili']=="Kota padangsidimpuan") echo "selected" ?> value="Kota padangsidimpuan"> KOTA PADANGSIDIMPUAN</option>

                    <option <?php if ($myprofile['domisili']=="Kota gunungsitoli") echo "selected" ?> value="Kota gunungsitoli"> KOTA GUNUNGSITOLI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kepulauan mentawai") echo "selected" ?> value="Kabupaten kepulauan mentawai"> KABUPATEN KEPULAUAN MENTAWAI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pesisir selatan") echo "selected" ?> value="Kabupaten pesisir selatan"> KABUPATEN PESISIR SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten solok") echo "selected" ?> value="Kabupaten solok"> KABUPATEN SOLOK</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sijunjung") echo "selected" ?> value="Kabupaten sijunjung"> KABUPATEN SIJUNJUNG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tanah datar") echo "selected" ?> value="Kabupaten tanah datar"> KABUPATEN TANAH DATAR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten padang pariaman") echo "selected" ?> value="Kabupaten padang pariaman"> KABUPATEN PADANG PARIAMAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten agam") echo "selected" ?> value="Kabupaten agam"> KABUPATEN AGAM</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lima puluh kota") echo "selected" ?> value="Kabupaten lima puluh kota"> KABUPATEN LIMA PULUH KOTA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pasaman") echo "selected" ?> value="Kabupaten pasaman"> KABUPATEN PASAMAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten solok selatan") echo "selected" ?> value="Kabupaten solok selatan"> KABUPATEN SOLOK SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten dharmasraya") echo "selected" ?> value="Kabupaten dharmasraya"> KABUPATEN DHARMASRAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pasaman barat") echo "selected" ?> value="Kabupaten pasaman barat"> KABUPATEN PASAMAN BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kota padang") echo "selected" ?> value="Kota padang"> KOTA PADANG</option>

                    <option <?php if ($myprofile['domisili']=="Kota solok") echo "selected" ?> value="Kota solok"> KOTA SOLOK</option>

                    <option <?php if ($myprofile['domisili']=="Kota sawah lunto") echo "selected" ?> value="Kota sawah lunto"> KOTA SAWAH LUNTO</option>

                    <option <?php if ($myprofile['domisili']=="Kota padang panjang") echo "selected" ?> value="Kota padang panjang"> KOTA PADANG PANJANG</option>

                    <option <?php if ($myprofile['domisili']=="Kota bukittinggi") echo "selected" ?> value="Kota bukittinggi"> KOTA BUKITTINGGI</option>

                    <option <?php if ($myprofile['domisili']=="Kota payakumbuh") echo "selected" ?> value="Kota payakumbuh"> KOTA PAYAKUMBUH</option>

                    <option <?php if ($myprofile['domisili']=="Kota pariaman") echo "selected" ?> value="Kota pariaman"> KOTA PARIAMAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kuantan singingi") echo "selected" ?> value="Kabupaten kuantan singingi"> KABUPATEN KUANTAN SINGINGI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten indragiri hulu") echo "selected" ?> value="Kabupaten indragiri hulu"> KABUPATEN INDRAGIRI HULU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten indragiri hilir") echo "selected" ?> value="Kabupaten indragiri hilir"> KABUPATEN INDRAGIRI HILIR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pelalawan") echo "selected" ?> value="Kabupaten pelalawan"> KABUPATEN PELALAWAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten siak") echo "selected" ?> value="Kabupaten siak"> KABUPATEN SIAK</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kampar") echo "selected" ?> value="Kabupaten kampar"> KABUPATEN KAMPAR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten rokan hulu") echo "selected" ?> value="Kabupaten rokan hulu"> KABUPATEN ROKAN HULU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bengkalis") echo "selected" ?> value="Kabupaten bengkalis"> KABUPATEN BENGKALIS</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten rokan hilir") echo "selected" ?> value="Kabupaten rokan hilir"> KABUPATEN ROKAN HILIR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kepulauan meranti") echo "selected" ?> value="Kabupaten kepulauan meranti"> KABUPATEN KEPULAUAN MERANTI</option>

                    <option <?php if ($myprofile['domisili']=="Kota pekanbaru") echo "selected" ?> value="Kota pekanbaru"> KOTA PEKANBARU</option>

                    <option <?php if ($myprofile['domisili']=="Kota dumai") echo "selected" ?> value="Kota dumai"> KOTA DUMAI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kerinci") echo "selected" ?> value="Kabupaten kerinci"> KABUPATEN KERINCI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten merangin") echo "selected" ?> value="Kabupaten merangin"> KABUPATEN MERANGIN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sarolangun") echo "selected" ?> value="Kabupaten sarolangun"> KABUPATEN SAROLANGUN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten batang hari") echo "selected" ?> value="Kabupaten batang hari"> KABUPATEN BATANG HARI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten muaro jambi") echo "selected" ?> value="Kabupaten muaro jambi"> KABUPATEN MUARO JAMBI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tanjung jabung timur") echo "selected" ?> value="Kabupaten tanjung jabung timur"> KABUPATEN TANJUNG JABUNG TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tanjung jabung barat") echo "selected" ?> value="Kabupaten tanjung jabung barat"> KABUPATEN TANJUNG JABUNG BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tebo") echo "selected" ?> value="Kabupaten tebo"> KABUPATEN TEBO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bungo") echo "selected" ?> value="Kabupaten bungo"> KABUPATEN BUNGO</option>

                    <option <?php if ($myprofile['domisili']=="Kota jambi") echo "selected" ?> value="Kota jambi"> KOTA JAMBI</option>

                    <option <?php if ($myprofile['domisili']=="Kota sungai penuh") echo "selected" ?> value="Kota sungai penuh"> KOTA SUNGAI PENUH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten ogan komering ulu") echo "selected" ?> value="Kabupaten ogan komering ulu"> KABUPATEN OGAN KOMERING ULU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten ogan komering ilir") echo "selected" ?> value="Kabupaten ogan komering ilir"> KABUPATEN OGAN KOMERING ILIR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten muara enim") echo "selected" ?> value="Kabupaten muara enim"> KABUPATEN MUARA ENIM</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lahat") echo "selected" ?> value="Kabupaten lahat"> KABUPATEN LAHAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten musi rawas") echo "selected" ?> value="Kabupaten musi rawas"> KABUPATEN MUSI RAWAS</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten musi banyuasin") echo "selected" ?> value="Kabupaten musi banyuasin"> KABUPATEN MUSI BANYUASIN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten banyu asin") echo "selected" ?> value="Kabupaten banyu asin"> KABUPATEN BANYU ASIN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten ogan komering ulu selatan") echo "selected" ?> value="Kabupaten ogan komering ulu selatan"> KABUPATEN OGAN KOMERING ULU SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten ogan komering ulu timur") echo "selected" ?> value="Kabupaten ogan komering ulu timur"> KABUPATEN OGAN KOMERING ULU TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten ogan ilir") echo "selected" ?> value="Kabupaten ogan ilir"> KABUPATEN OGAN ILIR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten empat lawang") echo "selected" ?> value="Kabupaten empat lawang"> KABUPATEN EMPAT LAWANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten penukal abab lematang ilir") echo "selected" ?> value="Kabupaten penukal abab lematang ilir"> KABUPATEN PENUKAL ABAB LEMATANG ILIR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten musi rawas utara") echo "selected" ?> value="Kabupaten musi rawas utara"> KABUPATEN MUSI RAWAS UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kota palembang") echo "selected" ?> value="Kota palembang"> KOTA PALEMBANG</option>

                    <option <?php if ($myprofile['domisili']=="Kota prabumulih") echo "selected" ?> value="Kota prabumulih"> KOTA PRABUMULIH</option>

                    <option <?php if ($myprofile['domisili']=="Kota pagar alam") echo "selected" ?> value="Kota pagar alam"> KOTA PAGAR ALAM</option>

                    <option <?php if ($myprofile['domisili']=="Kota lubuklinggau") echo "selected" ?> value="Kota lubuklinggau"> KOTA LUBUKLINGGAU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bengkulu selatan") echo "selected" ?> value="Kabupaten bengkulu selatan"> KABUPATEN BENGKULU SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten rejang lebong") echo "selected" ?> value="Kabupaten rejang lebong"> KABUPATEN REJANG LEBONG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bengkulu utara") echo "selected" ?> value="Kabupaten bengkulu utara"> KABUPATEN BENGKULU UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kaur") echo "selected" ?> value="Kabupaten kaur"> KABUPATEN KAUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten seluma") echo "selected" ?> value="Kabupaten seluma"> KABUPATEN SELUMA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mukomuko") echo "selected" ?> value="Kabupaten mukomuko"> KABUPATEN MUKOMUKO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lebong") echo "selected" ?> value="Kabupaten lebong"> KABUPATEN LEBONG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kepahiang") echo "selected" ?> value="Kabupaten kepahiang"> KABUPATEN KEPAHIANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bengkulu tengah") echo "selected" ?> value="Kabupaten bengkulu tengah"> KABUPATEN BENGKULU TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kota bengkulu") echo "selected" ?> value="Kota bengkulu"> KOTA BENGKULU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lampung barat") echo "selected" ?> value="Kabupaten lampung barat"> KABUPATEN LAMPUNG BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tanggamus") echo "selected" ?> value="Kabupaten tanggamus"> KABUPATEN TANGGAMUS</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lampung selatan") echo "selected" ?> value="Kabupaten lampung selatan"> KABUPATEN LAMPUNG SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lampung timur") echo "selected" ?> value="Kabupaten lampung timur"> KABUPATEN LAMPUNG TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lampung tengah") echo "selected" ?> value="Kabupaten lampung tengah"> KABUPATEN LAMPUNG TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lampung utara") echo "selected" ?> value="Kabupaten lampung utara"> KABUPATEN LAMPUNG UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten way kanan") echo "selected" ?> value="Kabupaten way kanan"> KABUPATEN WAY KANAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tulangbawang") echo "selected" ?> value="Kabupaten tulangbawang"> KABUPATEN TULANGBAWANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pesawaran") echo "selected" ?> value="Kabupaten pesawaran"> KABUPATEN PESAWARAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pringsewu") echo "selected" ?> value="Kabupaten pringsewu"> KABUPATEN PRINGSEWU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mesuji") echo "selected" ?> value="Kabupaten mesuji"> KABUPATEN MESUJI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tulang bawang barat") echo "selected" ?> value="Kabupaten tulang bawang barat"> KABUPATEN TULANG BAWANG BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pesisir barat") echo "selected" ?> value="Kabupaten pesisir barat"> KABUPATEN PESISIR BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kota bandar lampung") echo "selected" ?> value="Kota bandar lampung"> KOTA BANDAR LAMPUNG</option>

                    <option <?php if ($myprofile['domisili']=="Kota metro") echo "selected" ?> value="Kota metro"> KOTA METRO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bangka") echo "selected" ?> value="Kabupaten bangka"> KABUPATEN BANGKA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten belitung") echo "selected" ?> value="Kabupaten belitung"> KABUPATEN BELITUNG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bangka barat") echo "selected" ?> value="Kabupaten bangka barat"> KABUPATEN BANGKA BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bangka tengah") echo "selected" ?> value="Kabupaten bangka tengah"> KABUPATEN BANGKA TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bangka selatan") echo "selected" ?> value="Kabupaten bangka selatan"> KABUPATEN BANGKA SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten belitung timur") echo "selected" ?> value="Kabupaten belitung timur"> KABUPATEN BELITUNG TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kota pangkal pinang") echo "selected" ?> value="Kota pangkal pinang"> KOTA PANGKAL PINANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten karimun") echo "selected" ?> value="Kabupaten karimun"> KABUPATEN KARIMUN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bintan") echo "selected" ?> value="Kabupaten bintan"> KABUPATEN BINTAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten natuna") echo "selected" ?> value="Kabupaten natuna"> KABUPATEN NATUNA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lingga") echo "selected" ?> value="Kabupaten lingga"> KABUPATEN LINGGA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kepulauan anambas") echo "selected" ?> value="Kabupaten kepulauan anambas"> KABUPATEN KEPULAUAN ANAMBAS</option>

                    <option <?php if ($myprofile['domisili']=="Kota batam") echo "selected" ?> value="Kota batam"> KOTA BATAM</option>

                    <option <?php if ($myprofile['domisili']=="Kota tanjung pinang") echo "selected" ?> value="Kota tanjung pinang"> KOTA TANJUNG PINANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kepulauan seribu") echo "selected" ?> value="Kabupaten kepulauan seribu"> KABUPATEN KEPULAUAN SERIBU</option>

                    <option <?php if ($myprofile['domisili']=="Kota jakarta selatan") echo "selected" ?> value="Kota jakarta selatan"> KOTA JAKARTA SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kota jakarta timur") echo "selected" ?> value="Kota jakarta timur"> KOTA JAKARTA TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kota jakarta pusat") echo "selected" ?> value="Kota jakarta pusat"> KOTA JAKARTA PUSAT</option>

                    <option <?php if ($myprofile['domisili']=="Kota jakarta barat") echo "selected" ?> value="Kota jakarta barat"> KOTA JAKARTA BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kota jakarta utara") echo "selected" ?> value="Kota jakarta utara"> KOTA JAKARTA UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bogor") echo "selected" ?> value="Kabupaten bogor"> KABUPATEN BOGOR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sukabumi") echo "selected" ?> value="Kabupaten sukabumi"> KABUPATEN SUKABUMI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten cianjur") echo "selected" ?> value="Kabupaten cianjur"> KABUPATEN CIANJUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bandung") echo "selected" ?> value="Kabupaten bandung"> KABUPATEN BANDUNG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten garut") echo "selected" ?> value="Kabupaten garut"> KABUPATEN GARUT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tasikmalaya") echo "selected" ?> value="Kabupaten tasikmalaya"> KABUPATEN TASIKMALAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten ciamis") echo "selected" ?> value="Kabupaten ciamis"> KABUPATEN CIAMIS</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kuningan") echo "selected" ?> value="Kabupaten kuningan"> KABUPATEN KUNINGAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten cirebon") echo "selected" ?> value="Kabupaten cirebon"> KABUPATEN CIREBON</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten majalengka") echo "selected" ?> value="Kabupaten majalengka"> KABUPATEN MAJALENGKA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sumedang") echo "selected" ?> value="Kabupaten sumedang"> KABUPATEN SUMEDANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten indramayu") echo "selected" ?> value="Kabupaten indramayu"> KABUPATEN INDRAMAYU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten subang") echo "selected" ?> value="Kabupaten subang"> KABUPATEN SUBANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten purwakarta") echo "selected" ?> value="Kabupaten purwakarta"> KABUPATEN PURWAKARTA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten karawang") echo "selected" ?> value="Kabupaten karawang"> KABUPATEN KARAWANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bekasi") echo "selected" ?> value="Kabupaten bekasi"> KABUPATEN BEKASI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bandung barat") echo "selected" ?> value="Kabupaten bandung barat"> KABUPATEN BANDUNG BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pangandaran") echo "selected" ?> value="Kabupaten pangandaran"> KABUPATEN PANGANDARAN</option>

                    <option <?php if ($myprofile['domisili']=="Kota bogor") echo "selected" ?> value="Kota bogor"> KOTA BOGOR</option>

                    <option <?php if ($myprofile['domisili']=="Kota sukabumi") echo "selected" ?> value="Kota sukabumi"> KOTA SUKABUMI</option>

                    <option <?php if ($myprofile['domisili']=="Kota bandung") echo "selected" ?> value="Kota bandung"> KOTA BANDUNG</option>

                    <option <?php if ($myprofile['domisili']=="Kota cirebon") echo "selected" ?> value="Kota cirebon"> KOTA CIREBON</option>

                    <option <?php if ($myprofile['domisili']=="Kota bekasi") echo "selected" ?> value="Kota bekasi"> KOTA BEKASI</option>

                    <option <?php if ($myprofile['domisili']=="Kota depok") echo "selected" ?> value="Kota depok"> KOTA DEPOK</option>

                    <option <?php if ($myprofile['domisili']=="Kota cimahi") echo "selected" ?> value="Kota cimahi"> KOTA CIMAHI</option>

                    <option <?php if ($myprofile['domisili']=="Kota tasikmalaya") echo "selected" ?> value="Kota tasikmalaya"> KOTA TASIKMALAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kota banjar") echo "selected" ?> value="Kota banjar"> KOTA BANJAR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten cilacap") echo "selected" ?> value="Kabupaten cilacap"> KABUPATEN CILACAP</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten banyumas") echo "selected" ?> value="Kabupaten banyumas"> KABUPATEN BANYUMAS</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten purbalingga") echo "selected" ?> value="Kabupaten purbalingga"> KABUPATEN PURBALINGGA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten banjarnegara") echo "selected" ?> value="Kabupaten banjarnegara"> KABUPATEN BANJARNEGARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kebumen") echo "selected" ?> value="Kabupaten kebumen"> KABUPATEN KEBUMEN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten purworejo") echo "selected" ?> value="Kabupaten purworejo"> KABUPATEN PURWOREJO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten wonosobo") echo "selected" ?> value="Kabupaten wonosobo"> KABUPATEN WONOSOBO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten magelang") echo "selected" ?> value="Kabupaten magelang"> KABUPATEN MAGELANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten boyolali") echo "selected" ?> value="Kabupaten boyolali"> KABUPATEN BOYOLALI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten klaten") echo "selected" ?> value="Kabupaten klaten"> KABUPATEN KLATEN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sukoharjo") echo "selected" ?> value="Kabupaten sukoharjo"> KABUPATEN SUKOHARJO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten wonogiri") echo "selected" ?> value="Kabupaten wonogiri"> KABUPATEN WONOGIRI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten karanganyar") echo "selected" ?> value="Kabupaten karanganyar"> KABUPATEN KARANGANYAR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sragen") echo "selected" ?> value="Kabupaten sragen"> KABUPATEN SRAGEN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten grobogan") echo "selected" ?> value="Kabupaten grobogan"> KABUPATEN GROBOGAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten blora") echo "selected" ?> value="Kabupaten blora"> KABUPATEN BLORA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten rembang") echo "selected" ?> value="Kabupaten rembang"> KABUPATEN REMBANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pati") echo "selected" ?> value="Kabupaten pati"> KABUPATEN PATI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kudus") echo "selected" ?> value="Kabupaten kudus"> KABUPATEN KUDUS</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten jepara") echo "selected" ?> value="Kabupaten jepara"> KABUPATEN JEPARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten demak") echo "selected" ?> value="Kabupaten demak"> KABUPATEN DEMAK</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten temanggung") echo "selected" ?> value="Kabupaten temanggung"> KABUPATEN TEMANGGUNG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kendal") echo "selected" ?> value="Kabupaten kendal"> KABUPATEN KENDAL</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten batang") echo "selected" ?> value="Kabupaten batang"> KABUPATEN BATANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pekalongan") echo "selected" ?> value="Kabupaten pekalongan"> KABUPATEN PEKALONGAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pemalang") echo "selected" ?> value="Kabupaten pemalang"> KABUPATEN PEMALANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tegal") echo "selected" ?> value="Kabupaten tegal"> KABUPATEN TEGAL</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten brebes") echo "selected" ?> value="Kabupaten brebes"> KABUPATEN BREBES</option>

                    <option <?php if ($myprofile['domisili']=="Kota magelang") echo "selected" ?> value="Kota magelang"> KOTA MAGELANG</option>

                    <option <?php if ($myprofile['domisili']=="Kota surakarta") echo "selected" ?> value="Kota surakarta"> KOTA SURAKARTA</option>

                    <option <?php if ($myprofile['domisili']=="Kota salatiga") echo "selected" ?> value="Kota salatiga"> KOTA SALATIGA</option>

                    <option <?php if ($myprofile['domisili']=="Kota semarang") echo "selected" ?> value="Kota semarang"> KOTA SEMARANG</option>

                    <option <?php if ($myprofile['domisili']=="Kota pekalongan") echo "selected" ?> value="Kota pekalongan"> KOTA PEKALONGAN</option>

                    <option <?php if ($myprofile['domisili']=="Kota tegal") echo "selected" ?> value="Kota tegal"> KOTA TEGAL</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kulon progo") echo "selected" ?> value="Kabupaten kulon progo"> KABUPATEN KULON PROGO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bantul") echo "selected" ?> value="Kabupaten bantul"> KABUPATEN BANTUL</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten gunung kidul") echo "selected" ?> value="Kabupaten gunung kidul"> KABUPATEN GUNUNG KIDUL</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sleman") echo "selected" ?> value="Kabupaten sleman"> KABUPATEN SLEMAN</option>

                    <option <?php if ($myprofile['domisili']=="Kota yogyakarta") echo "selected" ?> value="Kota yogyakarta"> KOTA YOGYAKARTA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pacitan") echo "selected" ?> value="Kabupaten pacitan"> KABUPATEN PACITAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten ponorogo") echo "selected" ?> value="Kabupaten ponorogo"> KABUPATEN PONOROGO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten trenggalek") echo "selected" ?> value="Kabupaten trenggalek"> KABUPATEN TRENGGALEK</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tulungagung") echo "selected" ?> value="Kabupaten tulungagung"> KABUPATEN TULUNGAGUNG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten blitar") echo "selected" ?> value="Kabupaten blitar"> KABUPATEN BLITAR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kediri") echo "selected" ?> value="Kabupaten kediri"> KABUPATEN KEDIRI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten malang") echo "selected" ?> value="Kabupaten malang"> KABUPATEN MALANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lumajang") echo "selected" ?> value="Kabupaten lumajang"> KABUPATEN LUMAJANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten jember") echo "selected" ?> value="Kabupaten jember"> KABUPATEN JEMBER</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten banyuwangi") echo "selected" ?> value="Kabupaten banyuwangi"> KABUPATEN BANYUWANGI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bondowoso") echo "selected" ?> value="Kabupaten bondowoso"> KABUPATEN BONDOWOSO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten situbondo") echo "selected" ?> value="Kabupaten situbondo"> KABUPATEN SITUBONDO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten probolinggo") echo "selected" ?> value="Kabupaten probolinggo"> KABUPATEN PROBOLINGGO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pasuruan") echo "selected" ?> value="Kabupaten pasuruan"> KABUPATEN PASURUAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sidoarjo") echo "selected" ?> value="Kabupaten sidoarjo"> KABUPATEN SIDOARJO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mojokerto") echo "selected" ?> value="Kabupaten mojokerto"> KABUPATEN MOJOKERTO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten jombang") echo "selected" ?> value="Kabupaten jombang"> KABUPATEN JOMBANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten nganjuk") echo "selected" ?> value="Kabupaten nganjuk"> KABUPATEN NGANJUK</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten madiun") echo "selected" ?> value="Kabupaten madiun"> KABUPATEN MADIUN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten magetan") echo "selected" ?> value="Kabupaten magetan"> KABUPATEN MAGETAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten ngawi") echo "selected" ?> value="Kabupaten ngawi"> KABUPATEN NGAWI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bojonegoro") echo "selected" ?> value="Kabupaten bojonegoro"> KABUPATEN BOJONEGORO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tuban") echo "selected" ?> value="Kabupaten tuban"> KABUPATEN TUBAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lamongan") echo "selected" ?> value="Kabupaten lamongan"> KABUPATEN LAMONGAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten gresik") echo "selected" ?> value="Kabupaten gresik"> KABUPATEN GRESIK</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bangkalan") echo "selected" ?> value="Kabupaten bangkalan"> KABUPATEN BANGKALAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sampang") echo "selected" ?> value="Kabupaten sampang"> KABUPATEN SAMPANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pamekasan") echo "selected" ?> value="Kabupaten pamekasan"> KABUPATEN PAMEKASAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sumenep") echo "selected" ?> value="Kabupaten sumenep"> KABUPATEN SUMENEP</option>

                    <option <?php if ($myprofile['domisili']=="Kota kediri") echo "selected" ?> value="Kota kediri"> KOTA KEDIRI</option>

                    <option <?php if ($myprofile['domisili']=="Kota blitar") echo "selected" ?> value="Kota blitar"> KOTA BLITAR</option>

                    <option <?php if ($myprofile['domisili']=="Kota malang") echo "selected" ?> value="Kota malang"> KOTA MALANG</option>

                    <option <?php if ($myprofile['domisili']=="Kota probolinggo") echo "selected" ?> value="Kota probolinggo"> KOTA PROBOLINGGO</option>

                    <option <?php if ($myprofile['domisili']=="Kota pasuruan") echo "selected" ?> value="Kota pasuruan"> KOTA PASURUAN</option>

                    <option <?php if ($myprofile['domisili']=="Kota mojokerto") echo "selected" ?> value="Kota mojokerto"> KOTA MOJOKERTO</option>

                    <option <?php if ($myprofile['domisili']=="Kota madiun") echo "selected" ?> value="Kota madiun"> KOTA MADIUN</option>

                    <option <?php if ($myprofile['domisili']=="Kota surabaya") echo "selected" ?> value="Kota surabaya"> KOTA SURABAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kota batu") echo "selected" ?> value="Kota batu"> KOTA BATU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pandeglang") echo "selected" ?> value="Kabupaten pandeglang"> KABUPATEN PANDEGLANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lebak") echo "selected" ?> value="Kabupaten lebak"> KABUPATEN LEBAK</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tangerang") echo "selected" ?> value="Kabupaten tangerang"> KABUPATEN TANGERANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten serang") echo "selected" ?> value="Kabupaten serang"> KABUPATEN SERANG</option>

                    <option <?php if ($myprofile['domisili']=="Kota tangerang") echo "selected" ?> value="Kota tangerang"> KOTA TANGERANG</option>

                    <option <?php if ($myprofile['domisili']=="Kota cilegon") echo "selected" ?> value="Kota cilegon"> KOTA CILEGON</option>

                    <option <?php if ($myprofile['domisili']=="Kota serang") echo "selected" ?> value="Kota serang"> KOTA SERANG</option>

                    <option <?php if ($myprofile['domisili']=="Kota tangerang selatan") echo "selected" ?> value="Kota tangerang selatan"> KOTA TANGERANG SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten jembrana") echo "selected" ?> value="Kabupaten jembrana"> KABUPATEN JEMBRANA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tabanan") echo "selected" ?> value="Kabupaten tabanan"> KABUPATEN TABANAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten badung") echo "selected" ?> value="Kabupaten badung"> KABUPATEN BADUNG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten gianyar") echo "selected" ?> value="Kabupaten gianyar"> KABUPATEN GIANYAR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten klungkung") echo "selected" ?> value="Kabupaten klungkung"> KABUPATEN KLUNGKUNG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bangli") echo "selected" ?> value="Kabupaten bangli"> KABUPATEN BANGLI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten karang asem") echo "selected" ?> value="Kabupaten karang asem"> KABUPATEN KARANG ASEM</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten buleleng") echo "selected" ?> value="Kabupaten buleleng"> KABUPATEN BULELENG</option>

                    <option <?php if ($myprofile['domisili']=="Kota denpasar") echo "selected" ?> value="Kota denpasar"> KOTA DENPASAR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lombok barat") echo "selected" ?> value="Kabupaten lombok barat"> KABUPATEN LOMBOK BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lombok tengah") echo "selected" ?> value="Kabupaten lombok tengah"> KABUPATEN LOMBOK TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lombok timur") echo "selected" ?> value="Kabupaten lombok timur"> KABUPATEN LOMBOK TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sumbawa") echo "selected" ?> value="Kabupaten sumbawa"> KABUPATEN SUMBAWA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten dompu") echo "selected" ?> value="Kabupaten dompu"> KABUPATEN DOMPU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bima") echo "selected" ?> value="Kabupaten bima"> KABUPATEN BIMA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sumbawa barat") echo "selected" ?> value="Kabupaten sumbawa barat"> KABUPATEN SUMBAWA BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lombok utara") echo "selected" ?> value="Kabupaten lombok utara"> KABUPATEN LOMBOK UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kota mataram") echo "selected" ?> value="Kota mataram"> KOTA MATARAM</option>

                    <option <?php if ($myprofile['domisili']=="Kota bima") echo "selected" ?> value="Kota bima"> KOTA BIMA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sumba barat") echo "selected" ?> value="Kabupaten sumba barat"> KABUPATEN SUMBA BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sumba timur") echo "selected" ?> value="Kabupaten sumba timur"> KABUPATEN SUMBA TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kupang") echo "selected" ?> value="Kabupaten kupang"> KABUPATEN KUPANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten timor tengah selatan") echo "selected" ?> value="Kabupaten timor tengah selatan"> KABUPATEN TIMOR TENGAH SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten timor tengah utara") echo "selected" ?> value="Kabupaten timor tengah utara"> KABUPATEN TIMOR TENGAH UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten belu") echo "selected" ?> value="Kabupaten belu"> KABUPATEN BELU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten alor") echo "selected" ?> value="Kabupaten alor"> KABUPATEN ALOR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lembata") echo "selected" ?> value="Kabupaten lembata"> KABUPATEN LEMBATA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten flores timur") echo "selected" ?> value="Kabupaten flores timur"> KABUPATEN FLORES TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sikka") echo "selected" ?> value="Kabupaten sikka"> KABUPATEN SIKKA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten ende") echo "selected" ?> value="Kabupaten ende"> KABUPATEN ENDE</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten ngada") echo "selected" ?> value="Kabupaten ngada"> KABUPATEN NGADA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten manggarai") echo "selected" ?> value="Kabupaten manggarai"> KABUPATEN MANGGARAI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten rote ndao") echo "selected" ?> value="Kabupaten rote ndao"> KABUPATEN ROTE NDAO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten manggarai barat") echo "selected" ?> value="Kabupaten manggarai barat"> KABUPATEN MANGGARAI BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sumba tengah") echo "selected" ?> value="Kabupaten sumba tengah"> KABUPATEN SUMBA TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sumba barat daya") echo "selected" ?> value="Kabupaten sumba barat daya"> KABUPATEN SUMBA BARAT DAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten nagekeo") echo "selected" ?> value="Kabupaten nagekeo"> KABUPATEN NAGEKEO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten manggarai timur") echo "selected" ?> value="Kabupaten manggarai timur"> KABUPATEN MANGGARAI TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sabu raijua") echo "selected" ?> value="Kabupaten sabu raijua"> KABUPATEN SABU RAIJUA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten malaka") echo "selected" ?> value="Kabupaten malaka"> KABUPATEN MALAKA</option>

                    <option <?php if ($myprofile['domisili']=="Kota kupang") echo "selected" ?> value="Kota kupang"> KOTA KUPANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sambas") echo "selected" ?> value="Kabupaten sambas"> KABUPATEN SAMBAS</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bengkayang") echo "selected" ?> value="Kabupaten bengkayang"> KABUPATEN BENGKAYANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten landak") echo "selected" ?> value="Kabupaten landak"> KABUPATEN LANDAK</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mempawah") echo "selected" ?> value="Kabupaten mempawah"> KABUPATEN MEMPAWAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sanggau") echo "selected" ?> value="Kabupaten sanggau"> KABUPATEN SANGGAU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten ketapang") echo "selected" ?> value="Kabupaten ketapang"> KABUPATEN KETAPANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sintang") echo "selected" ?> value="Kabupaten sintang"> KABUPATEN SINTANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kapuas hulu") echo "selected" ?> value="Kabupaten kapuas hulu"> KABUPATEN KAPUAS HULU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sekadau") echo "selected" ?> value="Kabupaten sekadau"> KABUPATEN SEKADAU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten melawi") echo "selected" ?> value="Kabupaten melawi"> KABUPATEN MELAWI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kayong utara") echo "selected" ?> value="Kabupaten kayong utara"> KABUPATEN KAYONG UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kubu raya") echo "selected" ?> value="Kabupaten kubu raya"> KABUPATEN KUBU RAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kota pontianak") echo "selected" ?> value="Kota pontianak"> KOTA PONTIANAK</option>

                    <option <?php if ($myprofile['domisili']=="Kota singkawang") echo "selected" ?> value="Kota singkawang"> KOTA SINGKAWANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kotawaringin barat") echo "selected" ?> value="Kabupaten kotawaringin barat"> KABUPATEN KOTAWARINGIN BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kotawaringin timur") echo "selected" ?> value="Kabupaten kotawaringin timur"> KABUPATEN KOTAWARINGIN TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kapuas") echo "selected" ?> value="Kabupaten kapuas"> KABUPATEN KAPUAS</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten barito selatan") echo "selected" ?> value="Kabupaten barito selatan"> KABUPATEN BARITO SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten barito utara") echo "selected" ?> value="Kabupaten barito utara"> KABUPATEN BARITO UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sukamara") echo "selected" ?> value="Kabupaten sukamara"> KABUPATEN SUKAMARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lamandau") echo "selected" ?> value="Kabupaten lamandau"> KABUPATEN LAMANDAU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten seruyan") echo "selected" ?> value="Kabupaten seruyan"> KABUPATEN SERUYAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten katingan") echo "selected" ?> value="Kabupaten katingan"> KABUPATEN KATINGAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pulang pisau") echo "selected" ?> value="Kabupaten pulang pisau"> KABUPATEN PULANG PISAU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten gunung mas") echo "selected" ?> value="Kabupaten gunung mas"> KABUPATEN GUNUNG MAS</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten barito timur") echo "selected" ?> value="Kabupaten barito timur"> KABUPATEN BARITO TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten murung raya") echo "selected" ?> value="Kabupaten murung raya"> KABUPATEN MURUNG RAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kota palangka raya") echo "selected" ?> value="Kota palangka raya"> KOTA PALANGKA RAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tanah laut") echo "selected" ?> value="Kabupaten tanah laut"> KABUPATEN TANAH LAUT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kota baru") echo "selected" ?> value="Kabupaten kota baru"> KABUPATEN KOTA BARU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten banjar") echo "selected" ?> value="Kabupaten banjar"> KABUPATEN BANJAR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten barito kuala") echo "selected" ?> value="Kabupaten barito kuala"> KABUPATEN BARITO KUALA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tapin") echo "selected" ?> value="Kabupaten tapin"> KABUPATEN TAPIN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten hulu sungai selatan") echo "selected" ?> value="Kabupaten hulu sungai selatan"> KABUPATEN HULU SUNGAI SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten hulu sungai tengah") echo "selected" ?> value="Kabupaten hulu sungai tengah"> KABUPATEN HULU SUNGAI TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten hulu sungai utara") echo "selected" ?> value="Kabupaten hulu sungai utara"> KABUPATEN HULU SUNGAI UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tabalong") echo "selected" ?> value="Kabupaten tabalong"> KABUPATEN TABALONG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tanah bumbu") echo "selected" ?> value="Kabupaten tanah bumbu"> KABUPATEN TANAH BUMBU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten balangan") echo "selected" ?> value="Kabupaten balangan"> KABUPATEN BALANGAN</option>

                    <option <?php if ($myprofile['domisili']=="Kota banjarmasin") echo "selected" ?> value="Kota banjarmasin"> KOTA BANJARMASIN</option>

                    <option <?php if ($myprofile['domisili']=="Kota banjar baru") echo "selected" ?> value="Kota banjar baru"> KOTA BANJAR BARU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten paser") echo "selected" ?> value="Kabupaten paser"> KABUPATEN PASER</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kutai barat") echo "selected" ?> value="Kabupaten kutai barat"> KABUPATEN KUTAI BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kutai kartanegara") echo "selected" ?> value="Kabupaten kutai kartanegara"> KABUPATEN KUTAI KARTANEGARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kutai timur") echo "selected" ?> value="Kabupaten kutai timur"> KABUPATEN KUTAI TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten berau") echo "selected" ?> value="Kabupaten berau"> KABUPATEN BERAU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten penajam paser utara") echo "selected" ?> value="Kabupaten penajam paser utara"> KABUPATEN PENAJAM PASER UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mahakam hulu") echo "selected" ?> value="Kabupaten mahakam hulu"> KABUPATEN MAHAKAM HULU</option>

                    <option <?php if ($myprofile['domisili']=="Kota balikpapan") echo "selected" ?> value="Kota balikpapan"> KOTA BALIKPAPAN</option>

                    <option <?php if ($myprofile['domisili']=="Kota samarinda") echo "selected" ?> value="Kota samarinda"> KOTA SAMARINDA</option>

                    <option <?php if ($myprofile['domisili']=="Kota bontang") echo "selected" ?> value="Kota bontang"> KOTA BONTANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten malinau") echo "selected" ?> value="Kabupaten malinau"> KABUPATEN MALINAU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bulungan") echo "selected" ?> value="Kabupaten bulungan"> KABUPATEN BULUNGAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tana tidung") echo "selected" ?> value="Kabupaten tana tidung"> KABUPATEN TANA TIDUNG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten nunukan") echo "selected" ?> value="Kabupaten nunukan"> KABUPATEN NUNUKAN</option>

                    <option <?php if ($myprofile['domisili']=="Kota tarakan") echo "selected" ?> value="Kota tarakan"> KOTA TARAKAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bolaang mongondow") echo "selected" ?> value="Kabupaten bolaang mongondow"> KABUPATEN BOLAANG MONGONDOW</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten minahasa") echo "selected" ?> value="Kabupaten minahasa"> KABUPATEN MINAHASA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kepulauan sangihe") echo "selected" ?> value="Kabupaten kepulauan sangihe"> KABUPATEN KEPULAUAN SANGIHE</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kepulauan talaud") echo "selected" ?> value="Kabupaten kepulauan talaud"> KABUPATEN KEPULAUAN TALAUD</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten minahasa selatan") echo "selected" ?> value="Kabupaten minahasa selatan"> KABUPATEN MINAHASA SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten minahasa utara") echo "selected" ?> value="Kabupaten minahasa utara"> KABUPATEN MINAHASA UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bolaang mongondow utara") echo "selected" ?> value="Kabupaten bolaang mongondow utara"> KABUPATEN BOLAANG MONGONDOW UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten siau tagulandang biaro") echo "selected" ?> value="Kabupaten siau tagulandang biaro"> KABUPATEN SIAU TAGULANDANG BIARO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten minahasa tenggara") echo "selected" ?> value="Kabupaten minahasa tenggara"> KABUPATEN MINAHASA TENGGARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bolaang mongondow selatan") echo "selected" ?> value="Kabupaten bolaang mongondow selatan"> KABUPATEN BOLAANG MONGONDOW SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bolaang mongondow timur") echo "selected" ?> value="Kabupaten bolaang mongondow timur"> KABUPATEN BOLAANG MONGONDOW TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kota manado") echo "selected" ?> value="Kota manado"> KOTA MANADO</option>

                    <option <?php if ($myprofile['domisili']=="Kota bitung") echo "selected" ?> value="Kota bitung"> KOTA BITUNG</option>

                    <option <?php if ($myprofile['domisili']=="Kota tomohon") echo "selected" ?> value="Kota tomohon"> KOTA TOMOHON</option>

                    <option <?php if ($myprofile['domisili']=="Kota kotamobagu") echo "selected" ?> value="Kota kotamobagu"> KOTA KOTAMOBAGU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten banggai kepulauan") echo "selected" ?> value="Kabupaten banggai kepulauan"> KABUPATEN BANGGAI KEPULAUAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten banggai") echo "selected" ?> value="Kabupaten banggai"> KABUPATEN BANGGAI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten morowali") echo "selected" ?> value="Kabupaten morowali"> KABUPATEN MOROWALI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten poso") echo "selected" ?> value="Kabupaten poso"> KABUPATEN POSO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten donggala") echo "selected" ?> value="Kabupaten donggala"> KABUPATEN DONGGALA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten toli-toli") echo "selected" ?> value="Kabupaten toli-toli"> KABUPATEN TOLI-TOLI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten buol") echo "selected" ?> value="Kabupaten buol"> KABUPATEN BUOL</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten parigi moutong") echo "selected" ?> value="Kabupaten parigi moutong"> KABUPATEN PARIGI MOUTONG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tojo una-una") echo "selected" ?> value="Kabupaten tojo una-una"> KABUPATEN TOJO UNA-UNA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sigi") echo "selected" ?> value="Kabupaten sigi"> KABUPATEN SIGI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten banggai laut") echo "selected" ?> value="Kabupaten banggai laut"> KABUPATEN BANGGAI LAUT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten morowali utara") echo "selected" ?> value="Kabupaten morowali utara"> KABUPATEN MOROWALI UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kota palu") echo "selected" ?> value="Kota palu"> KOTA PALU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kepulauan selayar") echo "selected" ?> value="Kabupaten kepulauan selayar"> KABUPATEN KEPULAUAN SELAYAR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bulukumba") echo "selected" ?> value="Kabupaten bulukumba"> KABUPATEN BULUKUMBA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bantaeng") echo "selected" ?> value="Kabupaten bantaeng"> KABUPATEN BANTAENG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten jeneponto") echo "selected" ?> value="Kabupaten jeneponto"> KABUPATEN JENEPONTO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten takalar") echo "selected" ?> value="Kabupaten takalar"> KABUPATEN TAKALAR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten gowa") echo "selected" ?> value="Kabupaten gowa"> KABUPATEN GOWA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sinjai") echo "selected" ?> value="Kabupaten sinjai"> KABUPATEN SINJAI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten maros") echo "selected" ?> value="Kabupaten maros"> KABUPATEN MAROS</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pangkajene dan kepulauan") echo "selected" ?> value="Kabupaten pangkajene dan kepulauan"> KABUPATEN PANGKAJENE DAN KEPULAUAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten barru") echo "selected" ?> value="Kabupaten barru"> KABUPATEN BARRU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bone") echo "selected" ?> value="Kabupaten bone"> KABUPATEN BONE</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten soppeng") echo "selected" ?> value="Kabupaten soppeng"> KABUPATEN SOPPENG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten wajo") echo "selected" ?> value="Kabupaten wajo"> KABUPATEN WAJO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sidenreng rappang") echo "selected" ?> value="Kabupaten sidenreng rappang"> KABUPATEN SIDENRENG RAPPANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pinrang") echo "selected" ?> value="Kabupaten pinrang"> KABUPATEN PINRANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten enrekang") echo "selected" ?> value="Kabupaten enrekang"> KABUPATEN ENREKANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten luwu") echo "selected" ?> value="Kabupaten luwu"> KABUPATEN LUWU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tana toraja") echo "selected" ?> value="Kabupaten tana toraja"> KABUPATEN TANA TORAJA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten luwu utara") echo "selected" ?> value="Kabupaten luwu utara"> KABUPATEN LUWU UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten luwu timur") echo "selected" ?> value="Kabupaten luwu timur"> KABUPATEN LUWU TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten toraja utara") echo "selected" ?> value="Kabupaten toraja utara"> KABUPATEN TORAJA UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kota makassar") echo "selected" ?> value="Kota makassar"> KOTA MAKASSAR</option>

                    <option <?php if ($myprofile['domisili']=="Kota parepare") echo "selected" ?> value="Kota parepare"> KOTA PAREPARE</option>

                    <option <?php if ($myprofile['domisili']=="Kota palopo") echo "selected" ?> value="Kota palopo"> KOTA PALOPO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten buton") echo "selected" ?> value="Kabupaten buton"> KABUPATEN BUTON</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten muna") echo "selected" ?> value="Kabupaten muna"> KABUPATEN MUNA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten konawe") echo "selected" ?> value="Kabupaten konawe"> KABUPATEN KONAWE</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kolaka") echo "selected" ?> value="Kabupaten kolaka"> KABUPATEN KOLAKA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten konawe selatan") echo "selected" ?> value="Kabupaten konawe selatan"> KABUPATEN KONAWE SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bombana") echo "selected" ?> value="Kabupaten bombana"> KABUPATEN BOMBANA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten wakatobi") echo "selected" ?> value="Kabupaten wakatobi"> KABUPATEN WAKATOBI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kolaka utara") echo "selected" ?> value="Kabupaten kolaka utara"> KABUPATEN KOLAKA UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten buton utara") echo "selected" ?> value="Kabupaten buton utara"> KABUPATEN BUTON UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten konawe utara") echo "selected" ?> value="Kabupaten konawe utara"> KABUPATEN KONAWE UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kolaka timur") echo "selected" ?> value="Kabupaten kolaka timur"> KABUPATEN KOLAKA TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten konawe kepulauan") echo "selected" ?> value="Kabupaten konawe kepulauan"> KABUPATEN KONAWE KEPULAUAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten muna barat") echo "selected" ?> value="Kabupaten muna barat"> KABUPATEN MUNA BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten buton tengah") echo "selected" ?> value="Kabupaten buton tengah"> KABUPATEN BUTON TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten buton selatan") echo "selected" ?> value="Kabupaten buton selatan"> KABUPATEN BUTON SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kota kendari") echo "selected" ?> value="Kota kendari"> KOTA KENDARI</option>

                    <option <?php if ($myprofile['domisili']=="Kota baubau") echo "selected" ?> value="Kota baubau"> KOTA BAUBAU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten boalemo") echo "selected" ?> value="Kabupaten boalemo"> KABUPATEN BOALEMO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten gorontalo") echo "selected" ?> value="Kabupaten gorontalo"> KABUPATEN GORONTALO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pohuwato") echo "selected" ?> value="Kabupaten pohuwato"> KABUPATEN POHUWATO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten bone bolango") echo "selected" ?> value="Kabupaten bone bolango"> KABUPATEN BONE BOLANGO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten gorontalo utara") echo "selected" ?> value="Kabupaten gorontalo utara"> KABUPATEN GORONTALO UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kota gorontalo") echo "selected" ?> value="Kota gorontalo"> KOTA GORONTALO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten majene") echo "selected" ?> value="Kabupaten majene"> KABUPATEN MAJENE</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten polewali mandar") echo "selected" ?> value="Kabupaten polewali mandar"> KABUPATEN POLEWALI MANDAR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mamasa") echo "selected" ?> value="Kabupaten mamasa"> KABUPATEN MAMASA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mamuju") echo "selected" ?> value="Kabupaten mamuju"> KABUPATEN MAMUJU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mamuju utara") echo "selected" ?> value="Kabupaten mamuju utara"> KABUPATEN MAMUJU UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mamuju tengah") echo "selected" ?> value="Kabupaten mamuju tengah"> KABUPATEN MAMUJU TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten maluku tenggara barat") echo "selected" ?> value="Kabupaten maluku tenggara barat"> KABUPATEN MALUKU TENGGARA BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten maluku tenggara") echo "selected" ?> value="Kabupaten maluku tenggara"> KABUPATEN MALUKU TENGGARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten maluku tengah") echo "selected" ?> value="Kabupaten maluku tengah"> KABUPATEN MALUKU TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten buru") echo "selected" ?> value="Kabupaten buru"> KABUPATEN BURU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kepulauan aru") echo "selected" ?> value="Kabupaten kepulauan aru"> KABUPATEN KEPULAUAN ARU</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten seram bagian barat") echo "selected" ?> value="Kabupaten seram bagian barat"> KABUPATEN SERAM BAGIAN BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten seram bagian timur") echo "selected" ?> value="Kabupaten seram bagian timur"> KABUPATEN SERAM BAGIAN TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten maluku barat daya") echo "selected" ?> value="Kabupaten maluku barat daya"> KABUPATEN MALUKU BARAT DAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten buru selatan") echo "selected" ?> value="Kabupaten buru selatan"> KABUPATEN BURU SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kota ambon") echo "selected" ?> value="Kota ambon"> KOTA AMBON</option>

                    <option <?php if ($myprofile['domisili']=="Kota tual") echo "selected" ?> value="Kota tual"> KOTA TUAL</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten halmahera barat") echo "selected" ?> value="Kabupaten halmahera barat"> KABUPATEN HALMAHERA BARAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten halmahera tengah") echo "selected" ?> value="Kabupaten halmahera tengah"> KABUPATEN HALMAHERA TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kepulauan sula") echo "selected" ?> value="Kabupaten kepulauan sula"> KABUPATEN KEPULAUAN SULA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten halmahera selatan") echo "selected" ?> value="Kabupaten halmahera selatan"> KABUPATEN HALMAHERA SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten halmahera utara") echo "selected" ?> value="Kabupaten halmahera utara"> KABUPATEN HALMAHERA UTARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten halmahera timur") echo "selected" ?> value="Kabupaten halmahera timur"> KABUPATEN HALMAHERA TIMUR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pulau morotai") echo "selected" ?> value="Kabupaten pulau morotai"> KABUPATEN PULAU MOROTAI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pulau taliabu") echo "selected" ?> value="Kabupaten pulau taliabu"> KABUPATEN PULAU TALIABU</option>

                    <option <?php if ($myprofile['domisili']=="Kota ternate") echo "selected" ?> value="Kota ternate"> KOTA TERNATE</option>

                    <option <?php if ($myprofile['domisili']=="Kota tidore kepulauan") echo "selected" ?> value="Kota tidore kepulauan"> KOTA TIDORE KEPULAUAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten fakfak") echo "selected" ?> value="Kabupaten fakfak"> KABUPATEN FAKFAK</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kaimana") echo "selected" ?> value="Kabupaten kaimana"> KABUPATEN KAIMANA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten teluk wondama") echo "selected" ?> value="Kabupaten teluk wondama"> KABUPATEN TELUK WONDAMA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten teluk bintuni") echo "selected" ?> value="Kabupaten teluk bintuni"> KABUPATEN TELUK BINTUNI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten manokwari") echo "selected" ?> value="Kabupaten manokwari"> KABUPATEN MANOKWARI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sorong selatan") echo "selected" ?> value="Kabupaten sorong selatan"> KABUPATEN SORONG SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sorong") echo "selected" ?> value="Kabupaten sorong"> KABUPATEN SORONG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten raja ampat") echo "selected" ?> value="Kabupaten raja ampat"> KABUPATEN RAJA AMPAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tambrauw") echo "selected" ?> value="Kabupaten tambrauw"> KABUPATEN TAMBRAUW</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten maybrat") echo "selected" ?> value="Kabupaten maybrat"> KABUPATEN MAYBRAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten manokwari selatan") echo "selected" ?> value="Kabupaten manokwari selatan"> KABUPATEN MANOKWARI SELATAN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pegunungan arfak") echo "selected" ?> value="Kabupaten pegunungan arfak"> KABUPATEN PEGUNUNGAN ARFAK</option>

                    <option <?php if ($myprofile['domisili']=="Kota sorong") echo "selected" ?> value="Kota sorong"> KOTA SORONG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten merauke") echo "selected" ?> value="Kabupaten merauke"> KABUPATEN MERAUKE</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten jayawijaya") echo "selected" ?> value="Kabupaten jayawijaya"> KABUPATEN JAYAWIJAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten jayapura") echo "selected" ?> value="Kabupaten jayapura"> KABUPATEN JAYAPURA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten nabire") echo "selected" ?> value="Kabupaten nabire"> KABUPATEN NABIRE</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten kepulauan yapen") echo "selected" ?> value="Kabupaten kepulauan yapen"> KABUPATEN KEPULAUAN YAPEN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten biak numfor") echo "selected" ?> value="Kabupaten biak numfor"> KABUPATEN BIAK NUMFOR</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten paniai") echo "selected" ?> value="Kabupaten paniai"> KABUPATEN PANIAI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten puncak jaya") echo "selected" ?> value="Kabupaten puncak jaya"> KABUPATEN PUNCAK JAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mimika") echo "selected" ?> value="Kabupaten mimika"> KABUPATEN MIMIKA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten boven digoel") echo "selected" ?> value="Kabupaten boven digoel"> KABUPATEN BOVEN DIGOEL</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mappi") echo "selected" ?> value="Kabupaten mappi"> KABUPATEN MAPPI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten asmat") echo "selected" ?> value="Kabupaten asmat"> KABUPATEN ASMAT</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten yahukimo") echo "selected" ?> value="Kabupaten yahukimo"> KABUPATEN YAHUKIMO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten pegunungan bintang") echo "selected" ?> value="Kabupaten pegunungan bintang"> KABUPATEN PEGUNUNGAN BINTANG</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten tolikara") echo "selected" ?> value="Kabupaten tolikara"> KABUPATEN TOLIKARA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten sarmi") echo "selected" ?> value="Kabupaten sarmi"> KABUPATEN SARMI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten keerom") echo "selected" ?> value="Kabupaten keerom"> KABUPATEN KEEROM</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten waropen") echo "selected" ?> value="Kabupaten waropen"> KABUPATEN WAROPEN</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten supiori") echo "selected" ?> value="Kabupaten supiori"> KABUPATEN SUPIORI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mamberamo raya") echo "selected" ?> value="Kabupaten mamberamo raya"> KABUPATEN MAMBERAMO RAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten nduga") echo "selected" ?> value="Kabupaten nduga"> KABUPATEN NDUGA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten lanny jaya") echo "selected" ?> value="Kabupaten lanny jaya"> KABUPATEN LANNY JAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten mamberamo tengah") echo "selected" ?> value="Kabupaten mamberamo tengah"> KABUPATEN MAMBERAMO TENGAH</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten yalimo") echo "selected" ?> value="Kabupaten yalimo"> KABUPATEN YALIMO</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten puncak") echo "selected" ?> value="Kabupaten puncak"> KABUPATEN PUNCAK</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten dogiyai") echo "selected" ?> value="Kabupaten dogiyai"> KABUPATEN DOGIYAI</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten intan jaya") echo "selected" ?> value="Kabupaten intan jaya"> KABUPATEN INTAN JAYA</option>

                    <option <?php if ($myprofile['domisili']=="Kabupaten deiyai") echo "selected" ?> value="Kabupaten deiyai"> KABUPATEN DEIYAI</option>

                    <option <?php if ($myprofile['domisili']=="Kota jayapura") echo "selected" ?> value="Kota jayapura"> KOTA JAYAPURA</option>

                    <option <?php if ($myprofile['domisili']=="Surabaya timur") echo "selected" ?> value="Surabaya timur"> SURABAYA TIMUR</option>            

                  </select>

                        </div>

                        </div>

                    </div>

                    <div class="col-md-12">

                      <div class="form-group">

                        <label class="form-control-label" for="input-address"><?= lang('Global.address') ?>*</label>

                        <textarea rows="4" class="form-control" id="address" name="address" placeholder="Alamat lengkap" required disabled><?= $myprofile['address']; ?></textarea>

                      </div>

                    </div>

                  </div>

                  <!-- <div class="row">

                    <div class="col-lg-4">

                      <div class="form-group">

                        <label class="form-control-label" for="input-city"><?= lang('Global.kota') ?> *</label>

                        <select class="form-control" id="input-city">

                          <option>Yogyakarta</option>

                          <option>Palembang</option>

                          <option>Surabaya</option>

                          <option>Semarang</option>

                          <option>Jakarta</option>

                        </select>

                      </div>

                    </div>

                    <div class="col-lg-4">

                      <div class="form-group">

                        <label class="form-control-label" for="input-kab"><?= lang('Global.kota_kab') ?> *</label>

                        <input type="text" id="input-kab" class="form-control" placeholder="Kabupaten" value="Sleman">

                      </div>

                    </div>

                    <div class="col-lg-4">

                      <div class="form-group">

                        <label class="form-control-label" for="input-country"><?= lang('Global.kodepos') ?> *</label>

                        <input type="number" id="input-postal-code" class="form-control" placeholder="Postal code">

                      </div>

                    </div>

                  </div> -->

                </div>

                <?php

                if ($_SESSION['user_id']!=$myprofile['id']){

                  ?>



                  <hr class="my-4" />

                  <!-- Other -->

                  <h6 class="heading-small text-muted mb-4">Akun</h6>

                  <div class="pl-lg-4">

                    <div class="row">
                    <div class="col-lg-12">

                        <div class="form-group">

                          <div class="form-group">

                            <label for="inputState" style="font-size: 14px; color: #8898aa">Status </label>

                            <select id="not_deleted" name="not_deleted" class="form-control">

                              <option><?=lang('Global.choose');?>...</option>

                              <option <?php if($myprofile['not_deleted']=='1') echo 'selected'; ?> value="1">Account active</option>

                              <option <?php if($myprofile['not_deleted']=='0') echo 'selected'; ?> value="0">Account deleted</option>

                            </select>

                          </div>

                          </div>

                      </div>
                      <div class="col-lg-12">

                        <div class="form-group">

                          <div class="form-group">

                            <label for="inputState" style="font-size: 14px; color: #8898aa">Email activation </label>

                            <select class="form-control" >

                              <option><?=lang('Global.choose');?>...</option>

                              <option <?php if($myprofile['active']=='1') echo 'selected'; ?> value="1">Activated</option>

                              <option <?php if($myprofile['active']=='0') echo 'selected'; ?> value="0">Not activated</option>

                            </select>

                          </div>

                          </div>

                      </div>

                    </div>

                  </div>

                  <?php

                }

                ?>

                <div class="text-center">

                  <input type="submit" name="submit" value="Simpan" class="btn btn-xl btn-simpan mt-4 pl-5 pr-5 pt-2 pb-2 mb-3" style="background-color: #232941; color: #FDC834; font-weight: 600">

                </div>

                <!-- <hr class="my-4" /> -->

                <!-- Description -->

               <!--  <h6 class="heading-small text-muted mb-4"><?= lang('Global.feedback') ?></h6>

                <div class="pl-lg-4">

                  <div class="form-group">

                    <label class="form-control-label">Deskripsi *</label>

                    <textarea rows="4" id="description" name="description" class="form-control" placeholder="A few words about you ..."><?= $myprofile['description']; ?></textarea>

                  </div>

                </div> -->

              </form>

            </div>

          </div>

        </div>

      </div>



      