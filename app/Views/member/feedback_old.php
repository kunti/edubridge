<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <!-- <h6 class="h2 text-white d-inline-block mb-0">Order</h6> -->
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <!-- <li class="breadcrumb-item"><a href="#">Peserta</a></li> -->
              <li class="breadcrumb-item active" aria-current="page">Tes Saya</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0">Tes Saya</h3>
        </div>
        <!-- Light table -->
          <div class="table">
            <table id="" class="table" width="100%" >
              <thead class="thead-light">
                <tr>
                  <th class="th-sm"><?= lang('Global.order_code') ?>
                  </th>
                  <th class="th-sm">Feedback
                  </th>
                  </tr>
              </thead>
              <tbody class="list">
                <!-- awal baris -->
                <tr>
                  <td>
                    <a href="">EB10987</a>
                  </td>
                  <td>
                    <div class="media-body">
                        <span>Paket Tes Kecerdasan</span>
                        <br>
                        <span>Rp. 120.000</span>
                      </div>
                    </div>
                  </td>
                  <td style="width: 400px; ">
                      <span class="text-wrap">Edu Bridge benar-benar luar biasa membantu saya dengan segala informasi detail yang saya butuhkan untuk ke China. Menyediakan berbagai informasi yang perlu saya tahu dan semua prosedur sebelum saya mulai kuliah telah diberikan dengan efisien. EduBridge benar-benar membuat pendaftaran universitas menjadi sangat mudah dan team nya asik juga ramah, bahkan kelewat ramah. Mereka bahkan masih membantu saya setelah saya sudah mulai masuk kuliah. Pengalaman saya dengan EduBridge benar-benar luar biasa dan jelas-jelas memuaskan. </span>
                  </td>
                </tr>
                <!-- akhir baris -->
                <!-- awal baris -->
                <tr>
                  <td>
                    <a href="">EB34343</a>
                  </td>
                  <td>
                    <div class="media-body">
                        <span>Paket Lengkap</span>
                        <br>
                        <span>Rp. 250.000</span>
                      </div>
                    </div>
                  </td>
                  <td style="width: 400px; ">
                      <span class="text-wrap">Edu Bridge benar-benar luar biasa membantu saya dengan segala informasi detail yang saya butuhkan untuk ke China. Menyediakan berbagai informasi yang perlu saya tahu dan semua prosedur sebelum saya mulai kuliah telah diberikan dengan efisien. EduBridge benar-benar membuat pendaftaran universitas menjadi sangat mudah dan team nya asik juga ramah, bahkan kelewat ramah. Mereka bahkan masih membantu saya setelah saya sudah mulai masuk kuliah. Pengalaman saya dengan EduBridge benar-benar luar biasa dan jelas-jelas memuaskan. </span>
                  </td>
                </tr>
                <!-- akhir baris -->  
                <!-- awal baris -->
                <tr>
                  <td>
                    <a href="">EB10987</a>
                  </td>
                  <td>
                    <div class="media-body">
                        <span>Paket Tes Kecerdasan</span>
                        <br>
                        <span>Rp. 120.000</span>
                      </div>
                    </div>
                  </td>
                  <td style="width: 400px; ">
                      <span class="text-wrap">Edu Bridge benar-benar luar biasa membantu saya dengan segala informasi detail yang saya butuhkan untuk ke China. Menyediakan berbagai informasi yang perlu saya tahu dan semua prosedur sebelum saya mulai kuliah telah diberikan dengan efisien. EduBridge benar-benar membuat pendaftaran universitas menjadi sangat mudah dan team nya asik juga ramah, bahkan kelewat ramah. Mereka bahkan masih membantu saya setelah saya sudah mulai masuk kuliah. Pengalaman saya dengan EduBridge benar-benar luar biasa dan jelas-jelas memuaskan. </span>
                  </td>
                </tr>
                <!-- akhir baris -->
              </tbody>
            </table>
          </div>
        <!-- Card footer -->
        <!-- <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end mb-0">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <i class="fas fa-angle-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">
                  <i class="fas fa-angle-right"></i>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>
