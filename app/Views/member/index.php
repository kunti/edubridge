<!DOCTYPE html>



<html>



  <head>



    <!-- Required meta tags -->



    <meta charset="utf-8">



    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



    <meta name="author" content="EduBridge">



    <!-- Favicon -->



    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">



    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">



    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">



    <link rel="manifest" href="/site.webmanifest">



    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">



    <meta name="msapplication-TileColor" content="#da532c">



    <meta name="theme-color" content="#ffffff">



    <!-- Bootstrap CSS -->



    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">



    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">



    <link rel="stylesheet" href="<?php echo base_url('src/assets/agun/style-index.css'); ?>" type="text/css">







    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    

    <!-- select2 -->

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <!-- cropperjs -->

    <link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />

    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>

    <script src="https://unpkg.com/dropzone"></script>

    <script src="https://unpkg.com/cropperjs"></script>







    <title>Dashboard Volunteer</title>



  </head>



 <body oncontextmenu="return false;">

     <style>

         /* avatar edit pencil */

    .fa-edit {

        position: absolute;

        bottom: 20px;

        right: 59px;

        /* border: 1px solid; */

        /* border-radius: 50%; */

        padding-top: 10px;
        padding-left: 3px;

         height: 40px; 

        width: 40px; 

        font-size: 20px;

        /* display: flex !important; */

        align-items: center;

        justify-content: center;

        border-radius: 50%;

        background-color: #fdc834;

        color: #242843;

        /* box-shadow: 0 0 8px 3px #B8B8B8; */

        }

     </style>

<!-- style for cropper js -->

 <style>

    .image_area {

        position: relative;

        text-align: center !important;

    }



    img {

        display: block;

        max-width: 100%;

    }



    .preview {

        overflow: hidden;

        width: 160px; 

        height: 160px;

        margin: 10px;

        border: 1px solid red;

    }



    .modal-lg{

        max-width: 1000px !important;

    }



    .overlay {

        text-align: center !important;

        border-radius: 50% !important;

        position: absolute;

        top: 0px;

        bottom: 0px;

        background-color: rgba(255, 255, 255, 0.5);

        overflow: hidden;

        height: 0;

        /* transition: .5s ease; */

        width: 150px;

    }



    .image_area:hover .overlay {

        height: 150px;

        cursor: pointer;

    }



    .text {

        color: #333;

        font-size: 12px;

        position: absolute;

        top: 50%;

        left: 50%;

        -webkit-transform: translate(-50%, -50%);

        -ms-transform: translate(-50%, -50%);

        transform: translate(-50%, -50%);

        text-align: center;

    }



</style>

 <!-- style for validation seletc2 library and bootstrap form -->

<style>

.was-validated select.form-control:invalid + .select2-container> span.selection> span.select2-selection {

  border-color: #fb6340;

        border-left-color: rgb(251, 99, 64);

    padding-right: calc(1.5em + 1.25rem);

    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23fb6340' viewBox='-2 -2 7 7'%3e%3cpath stroke='%23fb6340' d='M0 0l3 3m0-3L0 3'/%3e%3ccircle r='.5'/%3e%3ccircle cx='3' r='.5'/%3e%3ccircle cy='3' r='.5'/%3e%3ccircle cx='3' cy='3' r='.5'/%3e%3c/svg%3E");

    background-repeat: no-repeat;

    background-position: center right calc(0.375em + 0.3125rem);

    background-size: calc(0.75em + 0.625rem) calc(0.75em + 0.625rem); 

}



.was-validated select.form-control:invalid + .select2-container--focus> span.selection> span.select2-selection {

  border-color: #fb6340;

        border-left-color: rgb(251, 99, 64);

    padding-right: calc(1.5em + 1.25rem);

    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23fb6340' viewBox='-2 -2 7 7'%3e%3cpath stroke='%23fb6340' d='M0 0l3 3m0-3L0 3'/%3e%3ccircle r='.5'/%3e%3ccircle cx='3' r='.5'/%3e%3ccircle cy='3' r='.5'/%3e%3ccircle cx='3' cy='3' r='.5'/%3e%3c/svg%3E");

    background-repeat: no-repeat;

    background-position: center right calc(0.375em + 0.3125rem);

    background-size: calc(0.75em + 0.625rem) calc(0.75em + 0.625rem);

}



.was-validated select.form-control:valid + .select2-container> span.selection> span.select2-selection {

  border-color: #2dce89;

padding-right: calc(1.5em + 1.25rem);

background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3cpath fill='%232dce89' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/%3e%3c/svg%3e");

background-repeat: no-repeat;

background-position: center right calc(0.375em + 0.3125rem);

background-size: calc(0.75em + 0.625rem) calc(0.75em + 0.625rem);

}



.was-validated select.form-control:valid + .select2-container--focus> span.selection> span.select2-selection {

  border-color: #2dce89;

padding-right: calc(1.5em + 1.25rem);

background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3cpath fill='%232dce89' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/%3e%3c/svg%3e");

background-repeat: no-repeat;

background-position: center right calc(0.375em + 0.3125rem);

background-size: calc(0.75em + 0.625rem) calc(0.75em + 0.625rem);

}

  .select2-container .select2-selection--single {

    display: block;

    width: 100%;

    height: calc(1.5em + 1.25rem + 2px);

    padding: 0.625rem 0.75rem;

    font-size: 0.875rem;

    font-weight: 400;

    line-height: 1.5;

    color: #495057;

    background-color: #fff;

    background-clip: padding-box;

    border: 1px solid #cad1d7;

    border-radius: 0.25rem;

    box-shadow: none;

    font-family: Montserrat;

}



.select2-container--classic .select2-selection--single .select2-selection__rendered {

    color: #495057;

    line-height: 24px;

}

.select2-container--classic .select2-selection--single .select2-selection__rendered {

    color: #495057;

    line-height: 24px;

}

</style>

 <style type="text/css">



    ul.timeline {



        list-style-type: none;



        position: relative;



    }



    ul.timeline:before {



        content: ' ';



        background: #d4d9df;



        display: inline-block;



        position: absolute;



        left: 29px;



        width: 2px;



        height: 100%;



        z-index: 400;



    }



    ul.timeline > li {



        margin: 0 0 20px 0;



        padding-left: 20px;



    }



    ul.timeline > li:before {



        content: ' ';



        background: white;



        display: inline-block;



        position: absolute;



        border-radius: 50%;



        border: 3px solid #22c0e8;



        left: 20px;



        width: 20px;



        height: 20px;



        z-index: 400;



    }



  </style>

  <?php $is_mengerjakan=1; ?>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">

        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title"></h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">×</span>

                    </button>

                </div>

                <div class="modal-body">

                    <div class="img-container">

                        <div class="row">

                            <div class="col-md-11">

                                <img src="" id="sample_image" />

                            </div>

                            <!-- <div class="col-md-4">

                                <div class="preview"></div>

                            </div> -->

                        </div>

                    </div>

                </div>

                <div class="modal-footer">

                    <button type="button" id="crop" class="btn btn-primary">Crop</button>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

                </div>

            </div>

        </div>

    </div>		





  <div class="wrapper">



        <!-- Sidebar  -->



        <nav id="sidebar">



            <div class="sidebar-header">



                <img src="<?php echo base_url('src/assets/agun/img/pipe-yellow.png'); ?>" class="img-fluid mt-3 pt-1">



            </div>







            <div class="avatar text-center">



                <div width="100">

                    <div class="image_area">

                        <form method="post">

                            <label for="upload_image">

                                <img width="140px"; src="

                                <?php 

                                

                                if($profile->avatar=="" || @file_get_contents(base_url('public/uploads/avatar/'.$profile->avatar))==FALSE) { 

                                    echo base_url('src/assets/img/profile/none.jpg'); 

                                    } else { 

                                        echo base_url('public/uploads/avatar/'.$profile->avatar); 

                                    } 

                                ?>" id="uploaded_image" class="rounded-circle img-responsive img-circle" />

                                <div class="overlay text-center d-none" width="150px";>

                                    <div class="text">Upload foto</div>

                                </div>

                                <input type="file" name="image" class="image" id="upload_image" style="display:none" />

                                <!-- avatar edit pencil -->

                            <i class="fa fa-edit" style="cursor: pointer;"></i>

                            </label>

                        </form>

                    </div>

                </div>



                <!-- <img src="img/avatar.jpg" > -->



                <h5 class="mt-2 mb-1 text-uppercase"><?= $profile->first_name.' '.$profile->last_name ?></h5>



                <?php 



                $birthDate = new DateTime($profile->birthdate);



                $today = new DateTime("today");



                if ($birthDate > $today) {



                    $y = 0; 



                }



                $y = $today->diff($birthDate)->y;



                $m = $today->diff($birthDate)->m;



                $d = $today->diff($birthDate)->d;



                ?>                      



                <small><?= $y ?> tahun, <?= $profile->education_level ?></small>



                <?php if($profile->volunteer == '1') {



                    /* echo " <br>



                    <span class='badge badge-success'>



                    <span class='status'>Volunteer</span>



                    </span>



                    ";*/



                }



                ?>



            </div>







            <ul class="list-unstyled components text-center mt-2">



                <li class="<?php if ($scope=='index') echo 'active'; ?> mb-2">



                    <a href="<?php echo base_url('dashboard'); ?>">Ikut Tes</a>



                </li>



                <li class="<?php if ($scope=='orderproduct') echo 'active'; ?> mb-2">



                    <a href="<?= base_url('test/myproduct/')?>">Hasil Tes</a>



                </li>



                <li class="<?php if ($scope=='profile') echo 'active'; ?> mb-2">



                    <a href="<?php echo base_url('account/profile'); ?>">Profil Saya</a>



                </li>



                <li class="<?php if ($scope=='feedback') echo 'active'; ?> mb-2">



                    <a href="<?php echo base_url('feedback'); ?>">Kritik & Saran</a>



                </li>



                <!-- <li>



                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pages</a>



                    <ul class="collapse list-unstyled" id="pageSubmenu">



                        <li>



                            <a href="#">Page 1</a>



                        </li>



                        <li>



                            <a href="#">Page 2</a>



                        </li>



                        <li>



                            <a href="#">Page 3</a>



                        </li>



                    </ul>



                </li> -->



            </ul>







             <ul class="list-unstyled CTAs">



                <!-- <li>



                    <a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>



                </li> -->



                <li>



                    <a href="<?php echo base_url(''); ?>" class="article"><i class="fas fa-arrow-left mr-1"></i> Kembali</a>



                </li>



            </ul>



        </nav>



        <!-- Page Content  -->



        <div id="page-content">







            <nav class="navbar navbar-expand navbar-light bg-light">



                <div class="container-fluid">







                    <button type="button" id="sidebarCollapse" class="btn btn-toggle-dashboard">



                        <i class="fas fa-arrows-alt-h"></i>



                        <!-- <span>Toggle Sidebar</span> -->



                    </button>







                    <div class="" id="navbarSupportedContent">



                        <ul class="nav navbar-nav nav-content ml-auto">



                            <!-- <li class="nav-item mr-1">



                               <a href="<?php echo base_url('member/faq'); ?>" class="nav-link"> <i class="far fa-question-circle mr-1"></i>FAQ</a>



                            </li> -->



                            <li class="nav-item">



                                <a class="nav-link" href="<?php echo base_url('auth/logout'); ?>"><i class="fas fa-sign-out-alt"></i> Logout</a>



                            </li>



                        </ul>



                    </div>



                </div>



            </nav>



        <?php 



        if (isset($template)) echo view($template); 



        ?>



            



        </div>







        



    </div>





<!-- Optional JavaScript -->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script  src="<?php echo base_url('src/assets/agun/script-index.js'); ?>"></script>

<!-- upload image additional jquery -->

<script src="<?php echo base_url('src/assets/vendor/jquery/dist/jquery.min.js'); ?>"></script>

<script src="<?php echo base_url('src/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js'); ?>"></script>

<script src="<?php echo base_url('src/assets/vendor/js-cookie/js.cookie.js'); ?>"></script>

<script src="<?php echo base_url('src/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js'); ?>"></script>

<script src="<?php echo base_url('src/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js'); ?>"></script>

<!-- Optional JS -->

<script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.min.js'); ?>"></script>

<script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.extension.js'); ?>"></script>

<!-- Argon JS -->

<!-- js select picker -->

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="<?php echo base_url('src/assets/js/argon.js?v=1.2.0'); ?>"></script>

    <!-- phone number masking -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){
        $('.phone_number').inputmask('999-9999-99999');
        var is_mengerjakan = document.getElementById('is_mengerjakan').value;
        //  is_mengerjakan = document.getElementById('is_mengerjakan').value();
        // alert(is_mengerjakan);
        if(is_mengerjakan==0){
        $('#briefingModal').modal('toggle');
        }

    });

    

</script>



<!-- script for cropperjs -->

<script>



$(document).ready(function(){



	var $modal = $('#modal');



	var image = document.getElementById('sample_image');



	var cropper;



	$('#upload_image').change(function(event){

		var files = event.target.files;



		var done = function(url){

			image.src = url;

			$modal.modal('show');

		};



		if(files && files.length > 0)

		{

			reader = new FileReader();

			reader.onload = function(event)

			{

				done(reader.result);

			};

			reader.readAsDataURL(files[0]);

		}

	});



	$modal.on('shown.bs.modal', function() {

		cropper = new Cropper(image, {

			aspectRatio: 1,

			viewMode: 3,

			preview:'.preview'

		});

	}).on('hidden.bs.modal', function(){

		cropper.destroy();

   		cropper = null;

	});



	$('#crop').click(function(){

		canvas = cropper.getCroppedCanvas({

			width:400,

			height:400

		});



		canvas.toBlob(function(blob){

			url = URL.createObjectURL(blob);

			var reader = new FileReader();

			reader.readAsDataURL(blob);

			reader.onloadend = function(){

				var base64data = reader.result;

				$.ajax({

					url:'<?=base_url()?>/account/uploadAvatar',

					method:'POST',

					data:{image:base64data, id_user:<?=  $profile->id ?>},

					success:function(data)

					{

                        console.log('success');

						$modal.modal('hide');

                        location.reload();

						$('#uploaded_image').attr('src', data);

					}

				});

			};

		});

	});

	

});

</script>





<!-- all form script -->

<script type="text/javascript">

   

    



    // select picker

    $(".js-example-basic-single").select2({

    theme: "classic",

    debug: "false"

    });



  (function () {

  'use strict'



  // Fetch all the forms we want to apply custom Bootstrap validation styles to

  var forms = document.querySelectorAll('.needs-validation')



  // Loop over them and prevent submission

  Array.prototype.slice.call(forms)

    .forEach(function (form) {

      form.addEventListener('submit', function (event) {

        if (!form.checkValidity()) {

          event.preventDefault()

          event.stopPropagation()

        }



        form.classList.add('was-validated')

      }, false)

    })

    })()



    //Reference: https://jsfiddle.net/fwv18zo1/

    var $select1 = $( '#education_level' ),

            $select2 = $( '#semester' ),

        $options = $select2.find( 'option' );

        

    $select1.on( 'change', function() {

        $select2.html( $options.filter( '[class="' + this.value + '"]' ) );

    } ).trigger( 'change' );



      // cek umur

    var $birthdate = $( '#birthdate' );

    $birthdate.on( 'change', function() {

    var today = new Date();

        var birthDate = new Date($(this).val());

        var age = today.getFullYear() - birthDate.getFullYear();

        var m = today.getMonth() - birthDate.getMonth();

        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 

        {

            age--;

        }

        if (age>=10 && age <=100){

        document.getElementById('age_alert').innerHTML

                    = '';

        }  else if (age>100){

        document.getElementById('age_alert').innerHTML = 'Perhatian, harap isi dengan benar';

        }

        else {

        document.getElementById('age_alert').innerHTML = 'Perhatian, usia minimal 10 tahun';

        }

    })

  </script>

  </body>

</html>