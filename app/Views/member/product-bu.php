<div class="content text-center">

            <h2>HASIL TES</h2>

<?php 
    if(!empty($orders[0])){
    ?>
        <?php 
        $status_belum_selesai = 0;
        if(!empty($orders[0])){
        
            foreach ($orders[0]['products'][0]['jenistes'] as $key => $jenistes) {
            $no = $key+1;
            $status[$key] = 0;
                        if($jenistes['status']=='ulangi'){ 
                            $status_belum_selesai = 1;
                        } else if($jenistes['start-test']=='0000-00-00 00:00:00'){
                            $status_belum_selesai = 1;
                        } else if($jenistes['status']=='start-test' && $jenistes['start-test']!='0000-00-00 00:00:00'){ 
                            $status_belum_selesai = 1;
                        } else if($jenistes['done-test']!='0000-00-00 00:00:00' || $jenistes['status']=='done-test'){
                        } 
            }
            ?>
        <?php
        if ($status_belum_selesai == 1){
        ?>
            <p class="mt-3">Perhatian, Anda belum menyelesaikan semua tes</p>
        
        <?php 
        }
            
        }
        
        
        ?>
        <?php 
        
            if($orders[0]['products'][0]['status']=='start-summarization'){
            ?>
            
                <span class="badge badge-warning mr-4">
                <span class="status"><?= lang('Global.on_summarization') ?></span>
                </span>
                <br>
                <span class="text-sm">Psikolog sedang meringkas tes Anda</span>
                <p style="font-size: 12px;"><?= $orders[0]['products'][0]['start-summarization'] ?></p>
            <?php
            } else if($orders[0]['products'][0]['status']=='summarized'){
            ?>
                <span class="badge badge-success mr-4">
                <span class="status"><?= lang('Global.done') ?></span>
                </span>
                <br>
                <p style="font-size: 12px;"><?= $orders[0]['products'][0]['summarized'] ?></p>
                <p class="mt-3">Tes Anda telah dinilai langsung oleh Psikolog kami 
                    <br class="d-none d-lg-block"> 
                    <h4><?= $orders[0]['products'][0]['psikolog_first_name'].' '.$orders[0]['products'][0]['psikolog_last_name'] ?></h4>
                    <br class="d-none d-lg-block"> 
                    silahkan download hasil Anda dibawah ini</p>
                <a href="<?php echo base_url('pdf/mpdf/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id'].'/D');?>" class="btn btn-success">Download <?= lang('Global.result') ?></a>
            <?php
            } 
        ?>
    <?php
    }
    ?>
    </div>

    
     
  
        <div class="col-xl-12">
          <div class="card">
            <div class="card-body" data-toggle="tooltip" data-placement="bottom">
              <!-- Chart -->
              <?php 
              if($profile->volunteer==1 && !empty($orders[0])){
                ?>
                <ul class="timeline">
                <li>
                  <a>Pendaftaran</a>
                  <!-- <br><span class="text-sm ">Automated by system : </span> -->
                  <br><small>
                  <?php
                setlocale(LC_ALL, 'id-ID', 'id_ID');
                echo strftime("%A, %d %B %Y", strtotime($orders[0]['checkout'])).', '.
                date('h:i:s A', strtotime($orders[0]['checkout'])) 
                ?>
            </small>
                </li>
                <li>
                  <a><?= lang('Global.my_profile') ?></a>
                  <br>
                  <small>Pastikan jenis kelamin, tanggal lahir dan jenjang pendidikan Anda sudah terisi</small>
                </li>
                <li>
                  <p>Paket tes</p>
                <?php
    foreach ($orders[0]['products'][0]['jenistes'] as $key => $jenistes) {
        $no = $key+1;
        $status[$key] = 0;
        ?>
            <div class="col-xl-6">
                <div class="list-test">
                <div class="card-body pt-0 pb-0 pr-0">
                    <div class="d-flex menu-test">
                    <h5 style="font-weight: 700"><?= $no.'. '.$jenistes['nama'] ?></h5>
                    <?php
                    if($jenistes['status']=='ulangi'){ 
                        $status_belum_selesai = 1;
                        ?>
                    <a style="font-weight: 600" href="<?= base_url('test/jenistes/'.$orders[0]['id'].'/'.$jenistes['id_memberorderproduct'].'/'.$jenistes['id']) ?>" class="ml-auto btn-start pt-0 text-center"><p>Ulangi</p></a>
                    <?php 
                    } else if($jenistes['start-test']=='0000-00-00 00:00:00'){
                        $status_belum_selesai = 1;
                        if ($key!=0){
                            $keysebelum = $key-1;
                            if($status[$keysebelum]==0){
                                ?>
                                <span href="" class="ml-auto btn-start pt-0 text-center"><p><i class="fas fa-lock"></i></p></span>
                                <?php
                            } else {
                                ?>
                                    <a href="<?= base_url('test/jenistes/'.$orders[0]['id'].'/'.$jenistes['id_memberorderproduct'].'/'.$jenistes['id']) ?>" class="ml-auto btn-start pt-0 text-center"><p>Mulai Tes</p></a>
                                    <?php
                            }
                        } 
                        ?>
                    <?php 
                    } else if($jenistes['status']=='start-test' && $jenistes['start-test']!='0000-00-00 00:00:00'){ 
                        $status_belum_selesai = 1;
                        ?>
                            <a href="<?= base_url('test/jenistes/'.$orders[0]['id'].'/'.$jenistes['id_memberorderproduct'].'/'.$jenistes['id']) ?>" class="ml-auto btn-start pt-0 text-center"><p>Lanjutkan</p></a>

                    <?php 
                    } else if($jenistes['done-test']!='0000-00-00 00:00:00' || $jenistes['status']=='done-test'){
                        $status[$key] = 1;
                    ?>
                    <span class="ml-auto btn-start pt-0 text-center"><p>Selesai
                    </p></span>
                    <?php 
                    } 
                    ?>
                    </div>
                </div>
                </div>
                <p style="font-size: 12px; text-align: left">
                <br>Tes bisa dimulai sejak : 
                <?php
                setlocale(LC_ALL, 'id-ID', 'id_ID');
                echo strftime("%A, %d %B %Y", strtotime($orders[0]['paid-confirm'])).', '.
                date('h:i:s A', strtotime($orders[0]['paid-confirm'])) 
                ?>
                <br> 
                <?php
                if ($jenistes['start-test']!='0000-00-00 00:00:00'){
                    echo 'Anda telah memulai tes pada : ';
                    echo strftime("%A, %d %B %Y", strtotime($jenistes['start-test'])).', '.
                    date('h:i:s A', strtotime($jenistes['start-test']));
                }
                ?>
                <br>
                <?php
                if ($jenistes['done-test']!='0000-00-00 00:00:00'){
                    echo 'Anda telah menyelesaikan tes pada : ';
                    echo strftime("%A, %d %B %Y", strtotime($jenistes['done-test'])).', '.
                    date('h:i:s A', strtotime($jenistes['done-test']));
                }
                ?>
                </p>
            </div>
            
        <?php 
        }
                  ?>
                </li>
                <li>
                    <p>Hasil tes</p>
                  <?php 
                     if($orders[0]['products'][0]['status']=='start-summarization'){
                        ?>
                          <span class="badge badge-warning mr-4">
                            <span class="status"><?= lang('Global.on_summarization') ?></span>
                          </span>
                          <br>
                          <small>Psikolog sedang meringkas paket tes Anda</small>
                          <p style="font-size: 12px;"><?= $orders[0]['products'][0]['start-summarization'] ?></p>
                        <?php
                      } else if($orders[0]['products'][0]['status']=='summarized'){
                        ?>
                          <span class="badge badge-success mr-4">
                            <span class="status"><?= lang('Global.done') ?></span>
                          </span>
                          <br>
                          <a href="<?= base_url('psychologist/id/'.$orders[0]['products'][0]['id_psikolog']) ?>" class=""><?= lang('Global.psikolog').' '.$orders[0]['products'][0]['psikolog_first_name'].' '.$orders[0]['products'][0]['psikolog_last_name'] ?> </a>
                          <p style="font-size: 12px;"><?= $orders[0]['products'][0]['summarized'] ?></p>
                          <a href="<?php echo base_url('pdf/mpdf/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id'].'/D');?>" class="btn btn-success">Download <?= lang('Global.result') ?></a>
                        <?php
                      } else {
                        ?>
                          <small>Hasil akan Anda dapatkan setelah menyelesaikan tes dan selesai diringkas oleh Psikolog kami</small>
                          <!-- <p style="font-size: 12px;"><?= $orders[0]['products'][0]['start-summarization'] ?></p> -->
                        <?php
                      }
                    ?>
                </li>
                <li before>
                  <a href="<?= base_url('feedback') ?>">Feedback</a>
                  <?php
                  if(empty($feedbacks[0]['feedback']) || $feedbacks[0]['feedback']==NULL){
                    ?>
                  <br><small>Anda belum mengisi feedback, <a href="<?= base_url('feedback') ?>">klik disini</a></small>
                  <?php
                  } else if(!empty($feedbacks[0]['feedback'])) {
                    ?>
                      <br><small><?= $feedbacks[0]['feedback'] ?>
                      <br>
                      <a href="<?= base_url('feedback') ?>">Edit feeback</a>
                  </small>
                      <?php
                  }
                  ?>
                </li>
              </ul>
                <?php
              }
              ?>
              <div class="chart" hidden="">
                <canvas id="chart-bars" class="chart-canvas"></canvas>
              </div>
            </div>
          </div>
        </div>