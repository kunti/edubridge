<!-- Page content -->



    <div class="container-fluid mt--6">



      <div class="row">



        <div class="col-xl-12 order-xl-1">



            <div class="card">



                <div class="card-header">



                    <div class="row align-items-center text-center">



                        <div class="col-12">



                        <h3 class="mb-0">TATA TERTIB DAN PROSEDUR PENGERJAAN TES</h3>



                        </div>



                    </div>



                </div>



                <div class="card-body">

                    <h6 class="pt-2 pb-2 heading-small">PERSIAPAN SEBELUM TES</h6>

                    <div class="row">

                        <ol>

                            <li class="pb-2"><b style="font-weight: 600">Peserta diwajibkan menggunakan Komputer / Laptop.</b></li>

                            <li class="pb-2">Memiliki <b style="font-weight: 600">akses internet yang stabil</b> dengan minimal kecepatan 4G/10Mbps.</li>

                            <li class="pb-2">Tersedia UPS (bagi perangkat PC) dan bila menggunakan laptop, <b style="font-weight: 600">pastikan baterai dalam kondisi penuh</b> sebagai antisipasi apabila terjadi mati listrik.</li>

                            <li class="pb-2">Peserta diperbolehkan <b style="font-weight: 600">menyiapkan kertas kosong dan alat tulis</b> untuk membantu coret-coretan dalam mengerjakan soal hitungan.</li>

                            <li class="pb-2"><b style="font-weight: 600">Luangkan waktu yang cukup</b>. Pelaksanaan tes dapat memakan <b style="font-weight: 600">waktu hingga 2,5 - 4 jam</b> untuk keseluruhan tes.</li>

                            <li class="pb-2"><b style="font-weight: 600">Jika peserta perlu ke toilet</b>, dipersilahkan sebelum mengerjakan tes.</li>

                            <li class="pb-2">Pastikan <b style="font-weight: 600">peserta dalam keadaan fit</b> sehingga dapat berpikir jernih dan siap mengerjakan tes.</li>
                            <li class="pb-2">Apabila dalam <b style="font-weight: 600">1x24 jam</b> setelah registrasi tidak memulai pengerjaan tes maka <b style="font-weight: 600">akun anda akan dinonaktifkan</b>.</li>

                        </ol>

                    </div>



                    <h6 class="pb-2 heading-small pt-2">PERATURAN PENGERJAAN</h6>

                    <div class="row">

                        <ol>

                            <li class="pb-2">Peserta <b style="font-weight: 600">diwajibkan membaca</b> petunjuk soal di setiap bagian tes secara teliti. <b style="font-weight: 600">Kesalahan dan kelalaian</b> peserta dalam membaca dan memahami peraturan <b style="font-weight: 600">menjadi tanggung jawab peserta.</b></li>

                            <li class="pb-2">Peserta harus <b style="font-weight: 600">mengerjakan tes secara mandiri</b> dan <b style="font-weight: 600">tidak boleh dibantu</b> oleh orang lain. <b style="font-weight: 600">Tes yang dikerjakan secara mandiri akan memberikan hasil yang akurat.</b></li>

                            <li class="pb-2">Peserta <b style="font-weight: 600">diwajibkan</b> mengerjakan setiap jenis dan bagian tes secara berurutan.</li>

                            <li class="pb-2">Setiap bagian <b style="font-weight: 600">tes memiliki batas waktu tertentu</b>, sehingga peserta <b style="font-weight: 600">wajib melakukan tes secara cepat dan tepat.</b></li>

                            <li class="pb-2">Pastikan peserta menjawab semua soal. </li>

                            <li class="pb-2">Sifat <b style="font-weight: 600">soal tes</b> yang tersedia adalah <b style="font-weight: 600">rahasia</b>. Peserta <b style="font-weight: 600">dilarang mendokumentasikan atau menyebarluaskan</b> seluruh panduan dan dokumen pendukung soal tes kepada siapapun.</li>

                            <li class="pb-2">Penyelenggara PIPE Psikotes <b style="font-weight: 600">berhak untuk tidak memberikan hasil tes</b> jika mendapati peserta melakukan tindakan <b style="font-weight: 600">kecurangan atau pelanggaran peraturan.</b></li>

                        </ol>

                    </div>



                    <h6 class="pb-2 heading-small pt-2">PROSEDUR SELAMA PENGERJAAN</h6>

                    <div class="row">

                        <ol>

                            <li class="pb-2"><b style="font-weight: 600">Kendala</b> apapun yang peserta hadapi pada saat pelaksanaan tes <b style="font-weight: 600">dapat dikonsultasikan</b> dengan menghubungi penyelenggara PIPE Psikotest <b>melalui kontak yang tertera di bagian paling bawah setiap halaman tes.</b></li>

                            <li class="pb-2"><b style="font-weight: 600">Peserta dilarang menggunakan alat bantu hitung (kalkulator) atau sejenisnya selama tes berlangsung.</b></li>

                            <li class="pb-2">Setiap bagian tes akan ada <b style="font-weight: 600">instruksi dan contoh cara mengerjakan soal.</b></li>
                            <li class="pb-2">Anda diberi waktu selama <b style="font-weight: 600">1x24jam</b> untuk mengerjakan semua tes. Jika lebih dari 1x24jam tidak diselesaikan maka <b style="font-weight: 600">akun anda akan dinonaktifkan</b>.</li>
                            <li class="pb-2">Jika dalam waktu <b style="font-weight: 600">1x24 jam</b> Anda tidak memulai tes sama sekali maka <b style="font-weight: 600">akun anda akan dinonaktifkan</b>.</li>

                        </ol>

                    </div>

                </div>

                <!-- modal tata tertib -->

                <div class="modal fade" id="modalTataTertib" tabindex="-1" role="dialog" aria-labelledby="modalTataTertib" aria-hidden="true">

                    <div class="modal-dialog" role="document">

                        <div class="modal-content">

                        <div class="modal-header">

                            <h5 class="modal-title" id="modalTataTertib">PERHATIAN</h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                                <span aria-hidden="true">&times;</span>

                            </button>

                        </div>

                        <div class="modal-body">

                            <p class="mb-0">Silahkan klik tombol <b>Mulai tes</b> pada menu tes kecerdasan untuk memulai tes.</p>

                        </div>

                        <div class="modal-footer text-center">

                            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->

                            <a type="button" href="<?= base_url('test/agreement/'.$id_order.'/'.$id_memberorderproduct) ?>" class="btn btn-xl btn-simpan mt-4 pl-5 pr-5 pt-2 pb-2 mb-3" style="background-color: #232941; color: #FDC834; font-weight: 600">OK</a>

                        </div>



                        </div>



                    </div>



                </div>

                <div class="text-center">

                <?php

                if($orders[0]['products'][0]['isTataTertib']==0){

                    ?>

                    <a id="modalTataTertib" data-toggle="modal" data-target="#modalTataTertib" class="btn btn-xl btn-simpan mt-4 pl-5 pr-5 pt-2 pb-2 mb-5" style="background-color: #232941; color: #FDC834; font-weight: 600">Saya telah membaca dan setuju</a>

                    <?php

                } else {

                    ?>

                    <a href="<?= base_url('dashboard') ?>" class="btn btn-xl mt-4 pl-5 pr-5 pt-2 pb-2 mb-5" style="background-color: #232941; color: #FDC834; font-weight: 600">Kembali</a>

                    <?php

                }

                ?>

                </div>

            </div>

        </div>

        </div>



    </div>







      