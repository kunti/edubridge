<div class="content text-center">



    <h2>TENTANG PIPE PSIKOTES GRATIS</h2>



    <p class="mt-3">PIPE (Potential, Interest, Personality) Psikotes adalah sebuah layanan tes minat dan kecerdasan berbasis online pertama di Indonesia yang diadakan untuk mengenali potensi akademis, ketertarikan siswa, dan pola kepribadian yang berkaitan erat dalam membantu pemetaan jurusan kuliah yang sesuai sehingga siswa mampu melakukan perencanaan studi yang tepat untuk jenjang karir dan masa depannya.</p>



    <span class="text-sm">Pastikan jenis kelamin, tanggal lahir dan jenjang pendidikan Anda sudah terisi di halaman</span>

    <a class="" style="font-weight: 600" href="<?= base_url('account/profile') ?>">Profil Saya</a>



    <h5 class="mt-5 mb-5" style="font-weight: 600">Klik tombol MULAI TES untuk memulai tes PIPE PSIKOTES GRATIS secara berurutan.</h5>



    <div class="mt-4 text-left">

      

    <?php 

    // dd($orders[0]['products'][0]['jenistes']);

        if(!empty($orders[0])){

        ?>

            <?php 

            foreach ($orders[0]['products'][0]['jenistes'] as $key => $jenistes) {

            $no = $key+1;

            $status[$key] = 0;

            ?>

                <!-- modal konfirmasi tes -->

                <div class="modal fade" id="modalMulaiTes<?=$key?>" tabindex="-1" role="dialog" aria-labelledby="modalMulaiTes<?=$key?>" aria-hidden="true">

                    <div class="modal-dialog" role="document">

                        <div class="modal-content">

                        <div class="modal-header">

                            <h5 class="modal-title" id="modalMulaiTes<?=$key?>">Konfirmasi tes</h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                                <span aria-hidden="true">&times;</span>

                            </button>

                        </div>

                        <div class="modal-body">

                            <p>Jika terjadi kendala atau putus koneksi pada internet, pada peserta maka PIPE Psikotest dapat melanjutkan tes kembali.</p>
                            <b>Apakah Anda yakin ingin mengerjakan tes ?</b>
                            <div class="d-flex menu-test">
                                <h5 style="font-weight: 700"><?= $no.'. '.$jenistes['nama'] ?></h5>
                            </div>

                        </div>

                        <div class="modal-footer text-center">

                            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->

                            <a type="button" href="<?= base_url('test/jenistes/'.$orders[0]['id'].'/'.$jenistes['id_memberorderproduct'].'/'.$jenistes['id']) ?>" class="btn btn-xl btn-simpan mt-4 pl-5 pr-5 pt-2 pb-2 mb-3" style="background-color: #232941; color: #FDC834; font-weight: 600">Mulai tes</a>

                        </div>

                        </div>

                    </div>

                </div>

                <div class="col-xl-6 mx-auto mb-4">

                    <div class="list-test">

                    <div class="card-body pt-0 pb-0 pr-0">

                        <div class="d-flex menu-test">

                        <h5 style="font-weight: 700"><?= $no.'. '.$jenistes['nama'] ?></h5>

                        <?php

                        // jika tes di set ulangi oleh admin

                        if($jenistes['status']=='ulangi'){ 

                        ?>

                        <button id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Ulangi</p></button>

                        <?php 

                        } else if($jenistes['start-test']=='0000-00-00 00:00:00'){

                            // jika ini bukan tes pertama

                            if ($key!=0){

                                $keysebelum = $key-1;

                                // jika tes sebelumnya belum selesai

                                if($status[$keysebelum]==0){

                                    ?>

                                    <span href="" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p><i class="fas fa-lock"></i></p></span>

                                    <?php

                                // jika tes sebelumnya sudah selesai

                                } else {

                                    ?>

                                        <button id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Mulai tes</p></button>

                                        <?php

                                }

                                // jika ini tes pertama

                            } else {

                                ?>

                                    <button id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Mulai tes</p></button>

                                    <?php

                            }

                            ?>

                        <?php 

                        // jika tes terhenti 

                        } else if($jenistes['status']=='start-test' && $jenistes['start-test']!='0000-00-00 00:00:00'){ 

                            ?>

                            <!-- <a id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" href="" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Lanjutkan</p></a> -->

                            <button id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Lanjutkan</p></button>

                        <?php 

                        } else if($jenistes['done-test']!='0000-00-00 00:00:00' || $jenistes['status']=='done-test'){

                            $status[$key] = 1;

                        ?>

                        <span style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Selesai

                        </p></span>

                        <?php 

                        } 

                        ?>

                        </div>

                    </div>

                    </div>

                </div>

            <?php 

            }

            ?>

            

        <?php

        }

        ?>

    </div>



    <?php     

        if(!empty($orders[0])){

            if($orders[0]['products'][0]['status']=='start-summarization'){

            ?>

            <p class="mt-5">Ketiga Test yang sudah Anda kerjakan secara berurutan akan dinilai langsung oleh Psikolog kami <br class="d-none d-lg-block"> dan hasilnya akan keluar minimal 1 bulan setelah pengerjaan test.</p>

            <span class="badge badge-warning mr-4">

                <span class="status"><?= lang('Global.on_summarization') ?></span>

            </span>

            <br>

            <p class="text-sm">Psikolog sedang meringkas paket tes Anda</p>

            <p> Setelah selesai dinilai, Anda akan mendapat notifikasi via email <br class="d-none d-lg-block"> dan mulai dapat mengunduh hasil PIPE PSIKOTES Anda melalui halaman <a href="test/myproduct" style="font-weight: 600">Hasil Tes</a></p>

            <?php

            }  else if($orders[0]['products'][0]['status']=='summarized'){

                ?>

                <p class="mt-5">Tes Anda telah dinilai langsung oleh Psikolog kami <br class="d-none d-lg-block"> silahkan download hasil Anda pada menu <a href="test/myproduct" style="font-weight: 600">Hasil Tes</a></p>

                <?php

            } else {

                ?>

                <p class="mt-5">Ketiga Test yang sudah Anda kerjakan secara berurutan akan dinilai langsung oleh Psikolog kami <br class="d-none d-lg-block"> dan hasilnya akan keluar minimal 1 bulan setelah pengerjaan test.</p>

                <?php

                

            }

        }

    ?>

    <br>

    <small class="d-none">Kode tes ini : <a href="<?php echo base_url('test/product/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id']);?>"><?= 'PIPETEST'.sprintf('%04d', $orders[0]['id']) ?></a></small>



</div>