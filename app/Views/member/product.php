<div class="content text-center">



      <h2>HASIL TES</h2>



<?php 

    if(!empty($orders[0])){

    ?>

        <?php 

        $status_belum_selesai = 0;

        if(!empty($orders[0])){

        

            foreach ($orders[0]['products'][0]['jenistes'] as $key => $jenistes) {

            $no = $key+1;

            $status[$key] = 0;

                        if($jenistes['status']=='ulangi'){ 

                            $status_belum_selesai = 1;

                        } else if($jenistes['start-test']=='0000-00-00 00:00:00'){

                            $status_belum_selesai = 1;

                        } else if($jenistes['status']=='start-test' && $jenistes['start-test']!='0000-00-00 00:00:00'){ 

                            $status_belum_selesai = 1;

                        } else if($jenistes['done-test']!='0000-00-00 00:00:00' || $jenistes['status']=='done-test'){

                        } 

            }

            ?>

        <?php

        if ($status_belum_selesai == 1){

        ?>

            <p class="mt-3">Perhatian, Anda belum menyelesaikan semua tes.</p>

        

        <?php 

        }

            

        }


        ?>

        <?php 

            if($status_belum_selesai == 0 && ($orders[0]['products'][0]['status']=='start-summarization' || $orders[0]['products'][0]['status']=='')){

            ?>

            

                <!-- <span class="badge badge-warning">

                <span class="status"><?= lang('Global.on_summarization') ?></span>

                </span> -->

                <span class="text-sm">Psikolog sedang menganalisa tes Anda.</span>

                <!-- <p style="font-size: 12px;"><?= $orders[0]['products'][0]['start-summarization'] ?></p> -->

            <?php

            } else if($orders[0]['products'][0]['status']=='summarized'){

            ?>

                <span class="badge badge-success d-none">

                <span class="status"><?= lang('Global.done') ?></span>

                </span>

              
                <p class="d-none" style="font-size: 12px;"><?= $orders[0]['products'][0]['summarized'] ?></p>

                <div class="">
                <p class="mt-3">Hasil tes Anda telah tersedia, silahkan unduh dengan menekan tombol di bawah.</p>
                </div>

            <?php

            } 

        ?>

    <?php

    }

    ?>

    </div>

        <div class="col-xl-12 mt-4">

          <div class="card">

            <div class="card-body" data-toggle="tooltip" data-placement="bottom">

              <!-- Chart -->

              <?php 

              if(!empty($orders[0])){

                ?>

                <ul class="timeline">

                <li>

                  <a>Pendaftaran</a>

                  <!-- <br><span class="text-sm ">Automated by system : </span> -->

                  <br><small>

                  <?php

                setlocale(LC_ALL, 'id-ID', 'id_ID');

                echo strftime("%A, %d %B %Y", strtotime($orders[0]['checkout'])).', '.

                date('h:i:s A', strtotime($orders[0]['checkout'])) 

                ?>

            </small>

                </li>

                <li>

                  <a><?= lang('Global.my_profile') ?></a>

                  <br>

                  <small>Pastikan jenis kelamin, tanggal lahir dan jenjang pendidikan Anda sudah terisi di halaman <a href="account/profile" style="font-weight: 600">Profil Saya</a></small>

                </li>

                <li>

                  <p>Paket tes</p>
                <div class="text-left">
                <div class="col-xl-6 pl-0">
                    <div class="list-test">
                    <div class="card-body pt-0 pb-0 pr-0">
                        <div class="d-flex menu-test">
                        <h5 style="font-weight: 700">TATA TERTIB</h5>
                        <?php
                            if($orders[0]['products'][0]['isTataTertib']==0){
                                ?>
                                <a style="cursor:pointer; width: 180px" href="<?= base_url('test/tatatertib/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id']) ?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Baca</p></a>
                                <?php
                            } else {
                                ?>
                                    <a style="cursor:pointer; width: 180px" href="<?= base_url('test/tatatertib/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id']) ?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Lihat</p></a>
                                <?php
                            }
                        ?>
                        </p></span>
                        </div>
                    </div>
                    </div>
                </div>
                <br>
                <?php
     // hapus tes kepribadian dari list
  //    foreach ($orders[0]['products'][0]['jenistes'] as $key => $jenistes) {
  //     if($jenistes['id_jenistes']==3){
  //         unset($orders[0]['products'][0]['jenistes'][$key]);
  //     }
  // }
  
    foreach ($orders[0]['products'][0]['jenistes'] as $key => $jenistes) {

        $no = $key+1;

        $status[$key] = 0;

        ?>

            <!-- modal konfirmasi tes -->

            <div class="modal fade" id="modalMulaiTes<?=$key?>" tabindex="-1" role="dialog" aria-labelledby="modalMulaiTes<?=$key?>" aria-hidden="true">

                <div class="modal-dialog" role="document">

                    <div class="modal-content">

                    <div class="modal-header">

                        <h5 class="modal-title" id="modalMulaiTes<?=$key?>">Konfirmasi tes</h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span>

                        </button>

                    </div>

                    <div class="modal-body">
                        
                        <p style="font-weight: 500">Jika terjadi kendala atau putus koneksi internet, peserta masih dapat melanjutkan tes PIPE Psikotest kembali.</p>

                        <span style="font-weight: 700">Apakah Anda yakin ingin mengerjakan tes ?</span>

                        <div class="d-flex menu-test">

                            <h5 style="font-weight: 700"><?= $no.'. '.$jenistes['nama'] ?></h5>

                        </div>

                    </div>

                    <div class="modal-footer text-center">

                        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->

                        <a type="button" href="<?= base_url('test/jenistes/'.$orders[0]['id'].'/'.$jenistes['id_memberorderproduct'].'/'.$jenistes['id']) ?>" class="btn btn-xl btn-simpan mt-4 pl-5 pr-5 pt-2 pb-2 mb-3" style="background-color: #232941; color: #FDC834; font-weight: 600">Mulai tes</a>

                    </div>

                    </div>

                </div>

            </div>

            <div class="col-xl-6 pl-0">

                <div class="list-test mb-0">

                <div class="card-body pt-0 pb-0 pr-0">

                    <div class="d-flex menu-test">

                    <h5 style="font-weight: 700"><?= $no.'. '.$jenistes['nama'] ?></h5>

                    <?php

                    // jika tes di set ulangi oleh admin

                    if($jenistes['status']=='ulangi'){ 

                    ?>

                    <button id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" style="cursor:pointer; width: 180px" class="border-0 ml-auto btn-start pt-0 text-center"><p>Ulangi</p></button>

                    <?php 

                    } else if($jenistes['start-test']=='0000-00-00 00:00:00'){

                        // jika ini bukan tes pertama

                        if ($key!=0){

                            $keysebelum = $key-1;

                            // jika tes sebelumnya belum selesai

                            if($status[$keysebelum]==0){

                                ?>

                                <span href="" style="cursor:pointer; width: 180px" class="border-0 ml-auto btn-start pt-0 text-center"><p><i class="fas fa-lock"></i></p></span>

                                <?php

                            // jika tes sebelumnya sudah selesai

                            } else {

                                ?>

                                    <button id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" style="cursor:pointer; width: 180px" class="border-0 ml-auto btn-start pt-0 text-center"><p>Mulai tes</p></button>

                                    <?php

                            }

                            // jika ini tes pertama

                        } else {
                          if ($orders[0]['products'][0]['isTataTertib']==0){
                              ?>
                              <span href="" style="cursor:pointer; width: 180px" class="ml-auto btn-start border-0 pt-0 text-center"><p><i class="fas fa-lock"></i></p></span>
                              <?php
                          } else {
                              ?>
                              <button id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Mulai tes</p></button>
                              <?php
                          }
                          ?>
                              <?php

                      }

                        ?>

                    <?php 

                    // jika tes terhenti 

                    } else if($jenistes['status']=='start-test' && $jenistes['start-test']!='0000-00-00 00:00:00'){ 

                        ?>

                        <!-- <a id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" href="" style="cursor:pointer; width: 180px" class="border-0 ml-auto btn-start pt-0 text-center"><p>Lanjutkan</p></a> -->

                        <button id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" style="cursor:pointer; width: 180px" class="border-0 ml-auto btn-start pt-0 text-center"><p>Lanjutkan</p></button>

                    <?php 

                    } else if($jenistes['done-test']!='0000-00-00 00:00:00' || $jenistes['status']=='done-test'){

                        $status[$key] = 1;

                    ?>

                    <span style="cursor:pointer; width: 180px; background-color: #bbbbbb" class="border-0 ml-auto btn-start pt-0 text-center"><p>Selesai

                    </p></span>

                    <?php 

                    } 

                    ?>

                    </div>

                </div>

                </div>

                <p class="pt-2" style="font-size: 12px; text-align: left; line-height: 30px">

                <?php

                if ($jenistes['start-test']=='0000-00-00 00:00:00'){

                  echo 'Anda belum memulai tes';

                }

                if ($jenistes['start-test']!='0000-00-00 00:00:00'){

                    echo 'Mulai pada ';

                    echo strftime("%A, %d %B %Y", strtotime($jenistes['start-test'])).', '.

                    date('h:i:s A', strtotime($jenistes['start-test']));

                }

                ?>

                <br>

                <?php

                if ($jenistes['done-test']!='0000-00-00 00:00:00'){

                    echo 'Selesai pada ';

                    echo strftime("%A, %d %B %Y", strtotime($jenistes['done-test'])).', '.

                    date('h:i:s A', strtotime($jenistes['done-test']));

                            $date1 = new DateTime($jenistes['start-test']);

                            $date2 = new DateTime($jenistes['done-test']); 

                            echo '<br>Waktu pengerjaan '; 

                            if($date2->diff($date1)->d!=0){

                              echo $date2->diff($date1)->d.' hari ';

                            }

                            echo $date2->diff($date1)->h.' jam '; 

                            echo $date2->diff($date1)->i.' menit '; 

                            echo $date2->diff($date1)->s.' detik '; 

                }

                ?>

                </p>

            </div>

            

        <?php 

        }

                  ?>

                </li>

                <li>

                    <p class="mb-0">Hasil tes</p>

                  <?php 

                     if($orders[0]['products'][0]['status']=='start-summarization'){

                        ?>

                          <!-- <span class="badge badge-warning mr-4 mt-2 mb-2">

                            <span class="status"><?= lang('Global.on_summarization') ?></span>

                          </span> -->
                          <small>Psikolog sedang menganalisa tes Anda</small>

                          <!-- <p style="font-size: 12px;"><?= $orders[0]['products'][0]['start-summarization'] ?></p> -->

                        <?php

                      } else if($orders[0]['products'][0]['status']=='summarized'){

                        ?>

                          <span class="badge badge-success d-none mr-4 mt-2 mb-2">

                            <span class="status"><?= lang('Global.done') ?></span>

                          </span>

                          <h6></h6>
                          <p class="mb-2">Hasil tes Anda telah dianalisa oleh psikolog kami.</p>
                          <a style="font-weight: 600"><?= lang('Global.psikolog').' '.$orders[0]['products'][0]['psikolog_first_name'].' '.$orders[0]['products'][0]['psikolog_last_name'] ?> </a>
                          <br>
                          <a href="<?php echo base_url('pdf/mpdf/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id'].'/D');?>" class="btn btn-success mt-3">Download <?= lang('Global.result') ?></a>

                        <?php

                      } else {
                        if ($status_belum_selesai == 1){

                          ?>
                  
                              <small>Perhatian, Anda belum menyelesaikan semua tes.</small>
                  
                          
                  
                          <?php 
                  
                          } else {

                            ?>
                          <small>Psikolog sedang menganalisa tes Anda</small> 
                          <?php
                        }

                      }

                    ?>

                </li>

                <li before>

                  <a href="<?= base_url('feedback') ?>">Feedback</a>

                  <?php
                  // dd($feedbacks);

                  if(empty($feedbacks[0]['feedback']) || $feedbacks[0]['feedback']==NULL){

                    ?>

                  <br><small>Belum ada <br>Untuk mengisi feedback, klik tombol <a href="<?= base_url('feedback') ?>" style="font-weight: 600">Kritik & Saran</a></small>

                  <?php

                  } else if(!empty($feedbacks[0]['feedback'])) {

                    ?>

                      <br><small><?= $feedbacks[0]['feedback'] ?>

                      <br>

                      <a href="<?= base_url('feedback') ?>" style="font-weight: 600">Edit feedback</a>

                  </small>

                      <?php

                  }

                  ?>

                </li>

              </ul>

                <?php

              }

              ?>

              <div class="chart" hidden="">

                <canvas id="chart-bars" class="chart-canvas"></canvas>

              </div>

            </div>

          </div>

        </div>