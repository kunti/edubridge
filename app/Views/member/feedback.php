
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0">Kritik dan saran</h3>
        </div>
        <!-- Light table -->
          <div class="container mt-3">
            <form action="<?= base_url('feedback/save') ?>" method="post">
            <textarea class="form-control pt-4 pl-4" id="exampleFormControlTextarea1" name="feedback" rows="10" placeholder="Ketik kritik dan saran disini..."><?php if(!empty($feedbacks[0]['feedback'])) {echo $feedbacks[0]['feedback'];} ?></textarea>
            <br>
            <div class="text-center">
            <!-- <input style="background-color: #232941; color: #FDC834; font-weight: 600" type="submit" class="btn" value="<?= lang('Global.save') ?>"> -->

            <input style="background-color: #232941; color: #FDC834; font-weight: 600" type="submit" class="btn" value="Kirim">
            </div>
            <br>
          </form>
          </div>
      </div>
    </div>
  </div>
</div>
