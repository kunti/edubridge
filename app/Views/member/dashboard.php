<div class="content text-center">



    <h2>TENTANG PIPE PSIKOTES GRATIS</h2>



    <p class="mt-3">PIPE (Potential, Interest, Personality) Psikotes adalah sebuah layanan tes minat dan kecerdasan berbasis online pertama di Indonesia yang diadakan untuk mengenali potensi akademis, ketertarikan siswa, dan pola kepribadian yang berkaitan erat dalam membantu pemetaan jurusan kuliah yang sesuai sehingga siswa mampu melakukan perencanaan studi yang tepat untuk jenjang karir dan masa depannya.</p>



    <span class="text-sm">Pastikan jenis kelamin, tanggal lahir dan jenjang pendidikan Anda sudah terisi di halaman</span>

    <a class="" style="font-weight: 600" href="<?= base_url('account/profile') ?>">Profil Saya</a>

    <?php
     $is_mengerjakan = 1;
     if(!empty($orders[0])){
     if($orders[0]['products'][0]['status']=='summarized'){
        ?>
        <p class="mt-4">Tes Anda telah dinilai langsung oleh Psikolog kami <br class="d-none d-lg-block"> silahkan download hasil Anda pada menu <a href="test/myproduct" style="font-weight: 600">Hasil Tes</a></p>
        <?php
    } else  {
     $is_mengerjakan = 1;

        ?>
        <h5 class="mt-4 mb-4 pb-2" style="font-weight: 600">Sebelum memulai tes peserta diwajibkan membaca tata tertib terlebih dahulu.</h5>
        <?php
    }

    
             // 3. Jika selesai mengerjakan maka tidak terjadi apa2
             if ($orders[0]['products'][0]['test-status']=='done-test') {
                $is_mengerjakan = 1;
             }
             // 1. Jika belum mengerjakan, cek waktu order apakah lebih dari 24 jam 
             else if ($orders[0]['products'][0]['test-status']==NULL) {
              // 1.b <24 jam maka status mengerjakan = 1
              if(strtotime($orders[0]['paid-confirm']) > strtotime('-'.$Pakettes_expiration_belumdikerjakan.' hours')) {
                $is_mengerjakan = 1;
            //     echo "
            //     <span class='badge badge-warning mr-4'>

            //     <span class='status'>Belum dikerjakan < 24 jam</span>

            //   </span>"; 
              // 1.a >24 jam maka status is_mengerjakan = 0
              } else {
                    $message = "Anda belum memulai tes sama sekali dalam waktu 1x24 jam. <br>
                    Untuk mengulang Tes, Anda perlu request terlebih dahulu ke Administrator dengan kik tombol di bawah.";
                    $is_mengerjakan = 0;
                    // echo "
                    // <span class='badge badge-danger mr-4'>
                    
                    // <span class='status'>Belum dikerjakan > 24 jam</span>
                    
                    // </span>"; 
                    
                    
                }
            
            }
        // 2. Jika sedang mengerjakan, cek waktu apakah lebih dari 3 hari
        // 2.a >3 hari  maka status is_mengerjakan = 0
        // 2.b <3 hari maka status mengerjakan = 1 
        else if ($orders[0]['products'][0]['test-status']=='start-test') {
            //       echo '<span class="badge badge-warning mr-4">
            //     <span class="status">Sedang dikerjakan</span>
            //   </span>';
            // $stop_date = new DateTime($orders[0]['paid-confirm']);
            // // 3 hari kemudian sampai jam 12 malam
            
            // $stop_date->modify('+'.($Pakettes_expiration+1).' day');
            // $stop_date->setTime(0,0,0);
            // $today = new DateTime("now");
            // $paid_confirm = $stop_date;
            // if ($paid_confirm < $today) { 
            //     $is_mengerjakan = 0;
            //     $expired = 1;
            //     $message = "Anda tidak menyelesaikan tes lebih dari 24 jam.";
            //         // echo '<small>Sudah hangus karna lebih dari 3 hari, Anda tidak dapat mengerjakan tes. <br>Jika Anda memiliki tes yang tertunda, silahkan klik Lanjutkan. <br>Hubungi Admin kami melalui WA (0812XXXXXXX) untuk membuka paket tes disertai alasan Anda</small>';
            //         if ($unblock==1){
            //             // echo '<br>Paket tes Anda telah di unblock oleh Admin, Anda dapat mengerjakan tes sampai selesai';
            //         }
            //     } else {
            //       $is_mengerjakan = 1;
            //       $y = $today->diff($paid_confirm)->y;
            //       $m = $today->diff($paid_confirm)->m;
            //       $d = $today->diff($paid_confirm)->d;
            //       $h = $today->diff($paid_confirm)->h;
            //       $i = $today->diff($paid_confirm)->i;
            //       $expired = 0;
            //     //   hangus dalam
            //       echo '<small>'.lang('Global.expired')." ".$d." ".lang('Global.day')." ".$h."  ".lang('Global.hour')." ".$i." ".lang('Global.minute')."</small>"; 
            //     }
            
            if ($orders[0]['products'][0]['test-status']=='done-test'){
                $is_mengerjakan = 1;

            } else {
                if(strtotime($orders[0]['paid-confirm']) > strtotime('-'.$Pakettes_expiration_belumdikerjakan.' hours')) {
                $is_mengerjakan = 1;
                } else {
                    $message = "Anda belum menyelesaikan tes dalam waktu 1x24 jam.<br>
                    Untuk mengulang Tes, Anda perlu request terlebih dahulu ke Administrator dengan klik tombol di bawah.";
                    $is_mengerjakan = 0;
                }

            }
        }
        if ($unblock==1 && ($orders[0]['products'][0]['test-status']!='done-test')){
            if(strtotime($orders[0]['products'][0]['is_unblock_time']) > strtotime('-'.$Pakettes_expiration_belumdikerjakan.' hours')) {
                $is_mengerjakan = 1;
                echo '<br>Paket tes Anda telah di unblock oleh Admin, Anda dapat mengerjakan tes sampai selesai';
            } else {
                $is_mengerjakan = 0;
                $message = "Tes Anda sudah dibuka lebih dari 24 jam namun Anda belum menyelesaikan";

                // dd('lebih dari 24 jam');
            }
        }
        
        if ($orders[0]['products'][0]['req_unblock']==1){
            $message = "Permintaan Anda sedang diproses. 
            Silahkan melakukan login ulang dalam waktu 1x24 jam";
        }

    ?>
    <!-- modal untuk tes yang gexpired -->
   
    <input type="hidden" name="is_mengerjakan" id="is_mengerjakan" value="<?=$is_mengerjakan?>"/>
    <div class="modal show pr-0 mr-0" id="briefingModal" tabindex="-1" role="dialog" aria-labelledby="briefingModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog-centered" role="document" style="width: 100%; height: 100%; margin: 0; padding: 0;">
            <div class="modal-content" style="height: auto; min-height: 100%; border-radius: 0;">
              <div class="modal-header" style="background-color: ">
                <h5 class="modal-title text-center" id="briefingModalLabel" style="color: #fff"></h5>
              </div>
              <div class="modal-body">
                <div style="position: absolute; left: 50%; top: 20%; transform: translate(-50%, -0%);">
                  <h2 class="text-dark pb-3">PERHATIAN</h2>
                  <p>
                      <?php
                      echo $message;
                      if($orders[0]['products'][0]['req_unblock']==0)
                        {
                            ?>
                            <br>
                            Untuk mengerjakan tes kembali Anda perlu request ke admin terlebih dahulu dengan klik "Request" di bawah ini.
                            <div class="text-center pt-3">
                              <a href="<?= base_url('order/request/'.$orders[0]['products'][0]['id']) ?>" type="button" class="btn btn-primary" >Request</a>
                              
                            </div>
                            <?php
                        }                     
                        ?>
                        <a class="nav-link" href="<?php echo base_url('auth/logout'); ?>"><i class="fas fa-sign-out-alt"></i> Logout</a>
                </div>
              </div>
            </div>
          </div>
        </div>
    <!-- modal untuk tes yang gexpired -->

    <div class="mt-4 text-left">
    <div class="col-xl-6 mx-auto mb-4">
        <div class="list-test">
        <div class="card-body pt-0 pb-0 pr-0">
            <div class="d-flex menu-test">
            <h5 style="font-weight: 700">TATA TERTIB</h5>
            <?php
                    if($orders[0]['products'][0]['isTataTertib']==0){
                        ?>
                        <a href="<?= base_url('test/tatatertib/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id']) ?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Baca</p></a>
                        <?php
                    } else {
                        ?>
                            <a href="<?= base_url('test/tatatertib/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id']) ?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Lihat</p></a>
                        <?php
                    }
                    
                    
            ?>
            <!-- <span style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Selesai -->
            </p></span>
            </div>
        </div>
        </div>
    </div>
    <?php 

    // dd($orders[0]['products'][0]['jenistes']);


        ?>
            
            <?php 
            // hapus tes kepribadian dari list
            // foreach ($orders[0]['products'][0]['jenistes'] as $key => $jenistes) {
            //     if($jenistes['id_jenistes']==3){
            //         unset($orders[0]['products'][0]['jenistes'][$key]);
            //     }
            // }
            
            foreach ($orders[0]['products'][0]['jenistes'] as $key => $jenistes) {
            $no = $key+1;

            $status[$key] = 0;

            ?>

                <!-- modal konfirmasi tes -->

                <div class="modal fade" id="modalMulaiTes<?=$key?>" tabindex="-1" role="dialog" aria-labelledby="modalMulaiTes<?=$key?>" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modalMulaiTes<?=$key?>">PERHATIAN</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <p>Jika terjadi kendala atau putus koneksi internet, peserta masih dapat melanjutkan tes PIPE Psikotest kembali.</p>
                            <b>Apakah Anda yakin ingin mengerjakan tes ?</b>
                            <div class="d-flex menu-test">
                                <h5 style="font-weight: 700"><?= $no.'. '.$jenistes['nama'] ?></h5>
                            </div>
                        </div>
                        <div class="modal-footer text-center">
                            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                            <a type="button" href="<?= base_url('test/jenistes/'.$orders[0]['id'].'/'.$jenistes['id_memberorderproduct'].'/'.$jenistes['id']) ?>" class="btn btn-xl btn-simpan mt-4 pl-5 pr-5 pt-2 pb-2 mb-3" style="background-color: #232941; color: #FDC834; font-weight: 600">Mulai tes</a>
                        </div>

                        </div>

                    </div>

                </div>
                <div class="col-xl-6 mx-auto mb-4">

                    <div class="list-test">

                    <div class="card-body pt-0 pb-0 pr-0">

                        <div class="d-flex menu-test">

                        <h5 style="font-weight: 700"><?= $no.'. '.$jenistes['nama'] ?></h5>

                        <?php

                        // jika tes di set ulangi oleh admin

                        if($jenistes['status']=='ulangi'){ 

                        ?>

                        <button id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Ulangi</p></button>

                        <?php 

                        } else if($jenistes['start-test']=='0000-00-00 00:00:00'){

                            // jika ini bukan tes pertama
                            if ($key!=0){
                                $keysebelum = $key-1;
                                // jika tes sebelumnya belum selesai
                                if($status[$keysebelum]==0){
                                    ?>
                                    <span href="" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p><i class="fas fa-lock"></i></p></span>
                                    <?php
                                // jika tes sebelumnya sudah selesai
                                } else {
                                    if ($is_mengerjakan==1){
                                    ?>
                                        <button id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Mulai tes</p></button>
                                        <?php
                                    }
                                    else {
                                        ?>
                                        <span href="" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p><i class="fas fa-lock"></i></p></span>
                                        <?php
                                    }
                                }
                                // jika ini tes pertama
                            } else {
                                if ($orders[0]['products'][0]['isTataTertib']==0){
                                    ?>
                                    <span href="" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p><i class="fas fa-lock"></i></p></span>
                                    <?php
                                } else {
                                    if($is_mengerjakan==1){
                                        ?>
                                            <button id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Mulai tes</p></button>
                                            <?php
                                        }
                                        else {
                                            ?>
                                            <span href="" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p><i class="fas fa-lock"></i></p></span>
                                            <?php
                                        }
                                }
                                ?>
                                    <?php

                            }

                            ?>

                        <?php 

                        // jika tes terhenti 

                        } else if($jenistes['status']=='start-test' && $jenistes['start-test']!='0000-00-00 00:00:00'){ 
                            ?>

                            <!-- <a id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" href="" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Lanjutkan</p></a> -->
                            
                            <button id="ModalMulaiTes<?=$key?>" data-toggle="modal" data-target="#modalMulaiTes<?=$key?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Lanjutkan</p></button>
                                <?php

                        } else if($jenistes['done-test']!='0000-00-00 00:00:00' || $jenistes['status']=='done-test'){

                            $status[$key] = 1;

                        ?>

                        <span style="cursor: pointer; background-color: #bbbbbb" class="ml-auto btn-start border-0 pt-0 text-center"><p>Selesai

                        </p></span>

                        <?php 

                        } 

                        ?>

                        </div>

                    </div>

                    </div>

                </div>

            <?php 

            }

            ?>

            

        <?php

        }

        ?>

    </div>



    <?php     

        if(!empty($orders[0])){

            if($orders[0]['products'][0]['status']=='start-summarization'){

            ?>

            <p class="mt-5">Ketiga Test yang sudah Anda kerjakan secara berurutan akan dinilai langsung oleh Psikolog kami <br class="d-none d-lg-block"> dan hasilnya akan keluar minimal 1 bulan setelah pengerjaan test.</p>

            <span class="badge badge-warning mr-4">

                <span class="status"><?= lang('Global.on_summarization') ?></span>

            </span>

            <br>

            <p class="text-sm">Psikolog sedang menganalisa paket tes Anda</p>

            <p> Setelah selesai dinilai, Anda akan mendapat notifikasi via email <br class="d-none d-lg-block"> dan mulai dapat mengunduh hasil PIPE PSIKOTES Anda melalui halaman <a href="test/myproduct" style="font-weight: 600">Hasil Tes</a></p>

            <?php

            }  else if($orders[0]['products'][0]['status']=='summarized'){
                ?>
                <?php
            } else {

                ?>

                <p class="mt-5">Ketiga Test yang sudah Anda kerjakan secara berurutan akan dinilai langsung oleh Psikolog kami <br class="d-none d-lg-block"> dan hasilnya akan keluar minimal 1 bulan setelah pengerjaan test.</p>

                <?php
                $stop_date = new DateTime($orders[0]['paid-confirm']);
                $stop_date->modify('+3 day');
                $stop_date->setTime(0,0,0);
                $today = new DateTime("now");
                $paid_confirm = $stop_date;
                if ($paid_confirm < $today) { 
                  $expired = 1;
                //   echo '<small>'.lang('Global.hasexpired_detail')."</small>";
                } else {
                  $y = $today->diff($paid_confirm)->y;
                  $m = $today->diff($paid_confirm)->m;
                  $d = $today->diff($paid_confirm)->d;
                  $h = $today->diff($paid_confirm)->h;
                  $i = $today->diff($paid_confirm)->i;
                  $expired = 0;
                //   hangus dalam
                //   echo '<small>'.lang('Global.expired')." ".$d." ".lang('Global.day')." ".$h."  ".lang('Global.hour')." ".$i." ".lang('Global.minute')."</small>"; 
                }
            }
        ?>
        <br>
        <small class="d-none">Kode tes ini : <a href="<?php echo base_url('test/product/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id']);?>"><?= 'PIPETEST'.sprintf('%04d', $orders[0]['id']) ?></a></small>
        <?php
        } else {
            ?>
            <div class="content text-center">
            <h5 class="mt-4 mb-4 pb-2" style="font-weight: 600">Anda belum memiliki tes, silahkan login ulang</h5>
            <div class="mt-4 text-left">
            <div class="col-xl-6 mx-auto mb-4">
                <div class="list-test">
                <div class="card-body pt-0 pb-0 pr-0">
                    <div class="d-flex menu-test">
                    <h5 style="font-weight: 700">PIPE PSIKOTES GRATIS</h5>
                            <a href="<?= base_url('cart/volunteer_generate/'); ?>" style="cursor: pointer" class="ml-auto btn-start border-0 pt-0 text-center"><p>Minta tes</p></a>
                    </p></span>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <?php
        }

    ?>
    



</div>

  