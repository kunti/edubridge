<!-- Page Content  -->
<div id="page-content">

<nav class="navbar navbar-expand navbar-light bg-light mb-4">
    <div class="container-fluid">

        <button type="button" id="sidebarCollapse" class="btn btn-toggle-dashboard">
            <i class="fas fa-arrows-alt-h"></i>
            <!-- <span>Toggle Sidebar</span> -->
        </button>

        <div class="" id="navbarSupportedContent">
            <ul class="nav navbar-nav nav-content ml-auto">
                <li class="nav-item">
                <a class="nav-link" href="<?= base_url('auth/logout/') ?>"><i class="fas fa-sign-out-alt"></i> Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-transparent pl-2">
    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
  </ol>
</nav>

<div class="content">
    <div class="row">
      <div class="col-12 col-sm-4 mb-4">
        <div class="card">
          <div class="card-body">
            <table class="table table-borderless">
            <tbody>
              <tr>
                <th style="border-top-style: none;" scope="row"><h2>AKUN TERDAFTAR</h2></th>
                <th style="border-top-style: none; text-align:right"><h2><?= $order_ready_dibawah+$order_ontest_notexpired+$order_done+$order_ready_diatas+$order_ontest_expired ?></h2></td>
              </tr>
              <tr>
                <th scope="row">Akun Aktif</th>
                <th style="text-align:right"><?= $order_ready_dibawah+$order_ontest_notexpired+$order_done ?></td>
              </tr>
              <tr>
                <th scope="row">Akun Non Aktif</th>
                <th style="text-align:right"><?= $order_ready_diatas+$order_ontest_expired ?></td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>  
      </div>
      <div class="col-12 col-sm-4 mb-4">
        <div class="card">
          <div class="card-body">
            <table class="table table-borderless">
            <tbody>
              <tr>
                <th style="border-top-style: none;" scope="row"><h2>AKUN AKTIF</h2></th>
                <th style="border-top-style: none; text-align:right"><h2><?= $order_ready_dibawah+$order_ontest_notexpired+$order_done ?></h2></td>
              </tr>
              <tr>
                <th scope="row">Belum dikerjakan < 24 jam</th>
                <th style="text-align:right"><?= $order_ready_dibawah ?></td>
              </tr>
              <tr>
                <th scope="row">Sedang dikerjakan < 24 jam</th>
                <th style="text-align:right"><?= $order_ontest_notexpired ?></td>
              </tr>
              <tr>
                <th scope="row">Selesai dikerjakan</th>
                <th style="text-align:right"><?= $order_done ?></td>
              </tr>
            </tbody>
          </table>
          </div>
          <div class="card-body">
            <table class="table table-borderless">
            <tbody>
              <tr>
                <th style="border-top-style: none;" scope="row"><h2>AKUN NON AKTIF</h2></th>
                <th style="border-top-style: none; text-align:right"><h2><?= $order_ready_diatas+$order_ontest_expired ?></h2></td>
              </tr>
              <tr>
                <th scope="row">Belum dikerjakan > 24 jam</th>
                <th style="text-align:right"><?= $order_ready_diatas ?></td>
              </tr>
              <tr>
                <th scope="row">Sedang dikerjakan > 24 jam</th>
                <th style="text-align:right"><?= $order_ontest_expired ?></td>
              </tr>
              
            </tbody>
          </table>
          </div>
        </div>  
      </div>
      <div class="col-12 col-sm-4 mb-4">
        <div class="card">
          <div class="card-body">
            <table class="table table-borderless">
            <tbody>
              <tr>
                <th style="border-top-style: none;" scope="row"><h2>ANALISA</h2></th>
                <th style="border-top-style: none; text-align:right"><h2><?= $order_done ?></h2></td>
              </tr>
              <tr>
                <th scope="row">Belum dianalisa</th>
                <th style="text-align:right"><?= $ready_to_summarized ?></td>
              </tr>
              <tr>
                <th scope="row">Sedang dianalisa</th>
                <th style="text-align:right"><?= $start_summarization ?></td>
              </tr>
              <tr>
                <th scope="row">Selesai dianalisa</th>
                <th style="text-align:right"><?= $summarized ?></td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>  
    <div class="container-xxl">
    <div id="accordion">
        <div class="card">
          <div class="card-header" id="headingTwo">
            <h5 class="mb-0">
              <button class="btn  collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                LAINNYA
              </button>
            </h5>
          </div>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body row">
            <div class="col-12 col-sm-4 mb-4" >
            <div class="card">
              <div class="card-body">
                <h2 class="card-title"><i class="fas fa-users mr-3"></i><?= $member ?></h2>
                <h6 class="card-text mb-2">Peserta terdaftar</h6>
                <!-- Button trigger modal -->
                <a href="" class="" data-toggle="modal" data-target="#exampleModal1">
                  <small><i class="fas fa-long-arrow-alt-right mr-2"></i>Selengkapnya</small>
                </a>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title mx-auto" id="exampleModalLabel2">Peserta Terdaftar</h5>
                      </div>
                      <div class="modal-body">
                        <div class="row text-center mb-3">
                            <div class="col-5">
                                <h6 class="mb-0">Akun Aktif</h6>
                            </div>
                            <div class="col-2">
                                <h6 class="mb-0">:</h6>
                            </div>
                            <div class="col-5">
                                <h6 class="mb-0"><?= $member_active ?></h6>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-5">
                                <h6 class="mb-0">Akun Deleted</h6>
                            </div>
                            <div class="col-2">
                                <h6 class="mb-0">:</h6>
                            </div>
                            <div class="col-5">
                                <h6 class="mb-0"><?= $member_inactive ?></h6>
                            </div>
                        </div>
                      </div>
                      <div class="modal-footer d-flex justify-content-center">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-4 mb-4" >
            <div class="card">
              <div class="card-body">
                <h2 class="card-title"><i class="fas fa-shopping-cart mr-3"></i><?= $orders ?></h2>
                <h6 class="card-text mb-2">Pemesanan</h6>
                <!-- Button trigger modal -->
                <a href="" class="" data-toggle="modal" data-target="#exampleModal2">
                  <small><i class="fas fa-long-arrow-alt-right mr-2"></i>Selengkapnya</small>
                </a>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title mx-auto" id="exampleModalLabel2">Pemesanan</h5>
                      </div>
                      <div class="modal-body">
                        <div class="row text-center mb-3">
                            <div class="col-5">
                                <h6 class="mb-0">Tertunda</h6>
                            </div>
                            <div class="col-2">
                                <h6 class="mb-0">:</h6>
                            </div>
                            <div class="col-5">
                                <h6 class="mb-0"><?= $pending ?></h6>
                            </div>
                        </div>
                        <div class="row text-center mb-3">
                            <div class="col-5">
                                <h6 class="mb-0">Terbayar - Menunggu konfimasi</h6>
                            </div>
                            <div class="col-2">
                                <h6 class="mb-0">:</h6>
                            </div>
                            <div class="col-5">
                                <h6 class="mb-0"><?= $paid ?></h6>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-5">
                                <h6 class="mb-0">Pembayaran terkonfirmasi</h6>
                            </div>
                            <div class="col-2">
                                <h6 class="mb-0">:</h6>
                            </div>
                            <div class="col-5">
                                <h6 class="mb-0"><?= $paid_confirm ?></h6>
                            </div>
                        </div>
                      </div>
                      <div class="modal-footer d-flex justify-content-center">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
          </div>
          <div class="col-12 col-sm-4 mb-4">
            <div class="card">
              <div class="card-body">
                <h2 class="card-title"><i class="fas fa-cubes mr-3"></i><?= $orderproduct ?></h2>
                <table class="table table-borderless">
                <tbody>
                  <tr>
                    <th scope="row">TOTAL PAKET TES</th>
                    <th><?= $orderproduct ?></td>
                  </tr>
                  <tr>
                    <th scope="row">TOTAL PAKET TES AKTIF</th>
                    <th><?= $order_ontest+$order_done+$order_ready_dibawah ?></td>
                  </tr>
                  <tr>
                    <th scope="row">TOTAL PAKET TES NON AKTIF</th>
                    <th><?= $order_ready_diatas ?></td>
                  </tr>
                </tbody>
              </table>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-4 mb-4">
            <div class="card">
              <div class="card-body">
                <h2 class="card-title"><i class="fas fa-cubes mr-3"></i><?= $orderproduct ?></h2>
                <table class="table table-borderless">
                <tbody>
                  <tr>
                    <th scope="row">TOTAL PAKET TES</th>
                    <th><?= $orderproduct ?></td>
                  </tr>
                  <tr>
                    <th scope="row">Belum dikerjakan < 24 jam</th>
                    <th><?= $order_ready_dibawah ?></td>
                  </tr>
                  <tr>
                    <th scope="row">Belum dikerjakan > 24 jam</th>
                    <th><?= $order_ready_diatas ?></td>
                  </tr>
                  <tr>
                    <th scope="row">Sedang dikerjakan < 24 jam</th>
                    <th><?= $order_ontest_notexpired ?></td>
                  </tr>
                  <tr>
                    <th scope="row">Sedang dikerjakan > 24 jam</th>
                    <th><?= $order_ontest_expired ?></td>
                  </tr>
                  <tr>
                    <th scope="row">Selesai dikerjakan</th>
                    <th><?= $order_done ?></td>
                  </tr>
                </tbody>
              </table>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-4 mb-4">
            <div class="card">
              <div class="card-body">
                <h2 class="card-title"><i class="fas fa- mr-3"></i><?= $order_done ?></h2>
                <table class="table table-borderless">
                <tbody>
                  <tr>
                    <th scope="row">TOTAL SELESAI DIKERJAKAN</th>
                    <th><?= $order_done ?></td>
                  </tr>
                  <tr>
                    <th scope="row">Belum dianalisa</th>
                    <th><?= $ready_to_summarized ?></td>
                  </tr>
                  <tr>
                    <th scope="row">Sedang dianalisa</th>
                    <th><?= $start_summarization ?></td>
                  </tr>
                  <tr>
                    <th scope="row">Selesai dianalisa</th>
                    <th><?= $summarized ?></td>
                  </tr>
                </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>