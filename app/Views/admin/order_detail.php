<div id="page-content">

    <nav class="navbar navbar-expand navbar-light bg-light mb-4">
        <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn btn-toggle-dashboard">
                <i class="fas fa-arrows-alt-h"></i>
                <!-- <span>Toggle Sidebar</span> -->
            </button>

            <div class="" id="navbarSupportedContent">
                <ul class="nav navbar-nav nav-content ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fas fa-sign-out-alt"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-transparent pl-2">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">Pemesanan</a></li>
        <li class="breadcrumb-item"><a href="#">Detail pemesanan</a></li>
      </ol>
    </nav>
    <div class="container-fluid mt--6">

        <div class="row">
  
          <div class="col-xl-4 order-xl-2">
  
            <div class="card card-profile">
  
              <div class="row justify-content-center">
  
                <div class="col-lg-3 order-lg-2">
  
                </div>
  
              </div>
  
              <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
  
                <div class="d-flex justify-content-between">
  
                  <h3 class="mb-0">Timeline</h3>
  
                </div>
  
              </div>
  
              <div class="card-body pt-0" style="font-size: 13px;">
  
                <ul class="timeline">
                  
                  <li>
  
                  <span class="badge badge-info mr-4">
  
                  <span class="status">Checkout booking</span>

                </span><br>
                    
                    <span class="float-left"><?= $orders[0]['checkout'] ?></span>
  
                  </li>
                  <br>
  
                  <li>
  
                    <span class="badge badge-success mr-4">
  
                      <span class="status"><?= lang('Global.paid') ?></span>
  
                    </span><br>
  
                    <span class="float-left"><?= $orders[0]['paid'] ?></span>
  
                  </li>
                  
                  <br>
                  <li>
  
                    <span class="badge badge-success mr-4">
  
                      <span class="status"><?= lang('Global.confirmed') ?></span>
  
                    </span>
                    <br>
  
                    <span class="float-left"><?= $orders[0]['paid-confirm'] ?></span>
  
                  </li>
  
                  <br>
                  <br>
                  <li>
    
                    <div class="media-body">
  
                              <?php 
  
                              if(!empty($orders[0]['products'])){
  
                        foreach ($orders[0]['products'] as $key => $product) {
  
                            echo '<b style="font-size:14px; color:#9C020E">'.strtoupper($product['nama']).'</b><br>';
  
                               foreach ($product['jenistes'] as $key3 => $jenistes) {
  
                                ?>
  
                                  <span><b><?= $jenistes['nama'] ?></b></span>
  
                                  <br>
  
                                  <!-- <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span> -->
  
                                <?php
  
                               if($jenistes['status']=='' && $orders[0]['status']=='paid-confirm'){
  
                                  ?>
  
                                    <span class="badge badge-info mr-4">
  
                                      <span class="status"><?= lang('Global.ready_test') ?></span>
  
                                    </span>
  
                                    <br>
  
                                    <p style="font-size: 12px;"><?= $orders[0]['paid-confirm'] ?></p>
  
                                  <?php
  
                                } else if($jenistes['status']=='start-test'){
  
                                  ?>
  
                                    <span class="badge badge-warning mr-4">
  
                                      <span class="status"><?= lang('Global.on_test') ?></span>
  
                                    </span>
  
                                    <br>
  
                                    <p style="font-size: 12px;"><?= $jenistes['start-test'] ?></p>
  
                                  <?php
  
                                } else if($jenistes['done-test']!='0000-00-00 00:00:00') {
  
                                  ?>
  
                                    <span class="badge badge-success mr-4">
  
                                      <span class="status"><?= lang('Global.done') ?></span>
  
                                    </span>
  
                                    <br>
  
                                    <p style="font-size: 12px;"><?= $jenistes['done-test'] ?></p>
  
                                  <?php
  
                                } else {
  
                                  ?>
  
                                      <span class="badge badge-darker mr-4">
  
                                      <span class="status" style="color: #fff"><?= lang('Global.test_not_ready') ?></span>
  
                                    </span>
  
                                  <?php
  
                                }
  
                               }
  
                             }
  
                           }
  
                              ?>
  
                            </div>
  
                  </li>
  
                  <li>
  
                    <?php 
  
                       if(!empty($orders[0]['products'])){
  
                        foreach ($orders[0]['products'] as $key => $product) {
  
                            echo '<b style="font-size:14px; color:#9C020E">'.strtoupper($product['nama']).'</b><br>';
  
                            ?>  
  
                            <span class="badge badge-success mr-4">
  
                              <span class="status"><?= lang('Global.start_summarization') ?></span>
  
                            </span><br>
  
                            <span class="float-left"><?= $product['start-summarization'] ?></span>
  
                            <?php
  
                        }
  
                      }
  
                          ?>
  
                  </li>
                  <br>
                  <br>
  
                  <li>
  
                    <?php 
  
                       if(!empty($orders[0]['products'])){
  
                        foreach ($orders[0]['products'] as $key => $product) {
  
                            echo '<b style="font-size:14px; color:#9C020E">'.strtoupper($product['nama']).'</b><br>';
  
                            ?>  
  
                            <span class="badge badge-success mr-4">
  
                              <span class="status"><?= lang('Global.summarized') ?></span>
  
                            </span><br>
  
                            <span class="float-left"><?= $product['summarized'] ?></span>
  
                            <?php
  
                        }
  
                      }
  
                          ?>
  
                  </li>
  
                </ul>
              <br>
              <div class="text-left">
  
                  <span>
  
                    <?= lang('Global.order_code') ?> : <span class="font-weight-light"><a href="<?php echo base_url('order/id/'.$orders[0]['id']); ?>"><b><?= 'PIPETEST'.sprintf('%04d', $orders[0]['id'])  ?></a></b></span>
  
                  </span>
  
                  <br>
  
                  <span>
  
                    Checkout : <span class="font-weight-light"><?= $orders[0]['checkout'] ?></span>
  
                  </span>
  
                  <br>
  
                  <span>
  
                    Status : <span class="font-weight-light"><?= $orders[0]['status'] ?></span>
  
                  </span>
  
                  <br>
  
                  <span>
  
                    <?= lang('Global.payment_proof') ?> : <br><span class="font-weight-light"><a target="_blank" href="<?= base_url('public/uploads/payment/'.$orders[0]['bukti_pembayaran']) ?>" class="avatar  mr-3">
  
                              <img alt="Image placeholder" src="<?php echo base_url('public/uploads/payment/'.$orders[0]['bukti_pembayaran']); ?>">
  
                            </a></span>
  
                  </span>
  
                </div>
  
              </div>
  
              <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
  
                <div class="d-flex justify-content-between">
  
                  <h3 class="mb-0"><?= lang('Global.test_package') ?></h3>
  
                </div>
  
              </div>
  
              <div class="card-body pt-0" style="font-size: 13px;">          
  
                <div class="text-left">
  
                  <?php 
  
                      $total_harga = 0;
  
                      if(!empty($orders[0]['products'])){
  
                        foreach ($orders[0]['products'] as $key => $product) {
  
                          $total_harga = $total_harga + $product['harga'];
  
                          ?>
  
                          <?= $key+1 ?>. <span><?= $product['nama'] ?></span>
  
                          <br>
  
                          <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span>
  
                          <br>
  
                          <?php
  
                        }
  
                        ?>
  
                        <b>Total : Rp. <?= number_format($total_harga, 0, 0, '.'); ?></b>
  
                        <?php
  
                      }
  
                    ?>
  
                </div>
  
              </div>
  
            </div>
  
          </div>
  
          <div class="col-xl-8 order-xl-1">
  
            <div class="card">
  
              <div class="card-header">
  
                <div class="row align-items-center">
  
                  <div class="col-8">
  
                    <h3 class="mb-0">Detail pemesanan</h3>
  
                  </div>
  
                  <div class="col-4 text-right">
  
                    <!-- <a href="#!" class="btn btn-xl btn-success">Save changes</a> -->
  
                  </div>
  
                </div>
  
              </div>
  
              <style type="text/css">
  
                td {
  
    vertical-align: top;
  
    text-align: left;
  
  }
  
              </style>
  
              <div class="card-header border-0">
  
                <h3><?= lang('Global.student') ?></h3>
  
                <div class="row">
  
                  <div class="col-auto">
  
                    <a href="#">
  
                      <img style="max-width: 72px" alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
  
                    </a>
  
                  </div>
  
                  <div class="col">
  
                    <?php 
  
                              if ($orders[0]['member'][0]['birthdate']==null || $orders[0]['member'][0]['birthdate']=='0000-00-00'){
  
                                  $y=0;
  
                                  $m=0;
  
                                  $d=0;
  
                              } else {
  
                                  $birthDate = new DateTime($orders[0]['member'][0]['birthdate']);
  
                                  $today = new DateTime($orders[0]['products'][0]['jenistes'][0]['start-test']);
  
                                  if ($birthDate > $today) { 
  
                                      $y=0;
  
                                      $m=0;
  
                                      $d=0;
  
                                  }
  
                                  $y = $today->diff($birthDate)->y;
  
                                  $m = $today->diff($birthDate)->m;
  
                                  $d = $today->diff($birthDate)->d;   
  
                              }
  
                              ?>
  
                    <table>
  
                        <tbody class="text-sm">
  
                          <tr>
  
                          <td><?= lang('Global.name') ?></td><td>: <a href="<?= base_url('account/profile/').'/'.$orders[0]['member'][0]['id'] ?>"><b><span><?= $orders[0]['member'][0]['first_name'].' '.$orders[0]['member'][0]['last_name']?></span></b></a></td>
  
                          </tr>
  
                          <tr>
  
                          <td><?= lang('Global.phone') ?></td><td>: <span><?= $orders[0]['member'][0]['phone'] ?></span></td>
  
                          </tr>
  
                          <tr>
  
                          <td>Email</td><td>: <span><?= $orders[0]['member'][0]['email'] ?></span></td>
  
                          </tr>
  
                          <tr>
  
                          <td><?= lang('Global.sex') ?></td><td>: <span><?= $orders[0]['member'][0]['sex'] ?></span></td>
  
                          </tr>
  
                          <tr>
  
                          <td><?= lang('Global.age') ?></td><td>: <span><?= $y." tahun ".$m." bulan ".$d." hari" ?></span></td>
  
                          </tr>
  
                          <tr>
  
                          <td><?= lang('Global.education_now') ?></td><td>: <span><?= $orders[0]['member'][0]['education_level'] ?></span></td>
  
                          </tr>
  
                          <tr>
  
                          <td><?= lang('Global.semester') ?></td><td>: <span><?= $orders[0]['member'][0]['semester'] ?></span></td>
  
                          </tr>
  
                          <tr>
  
                          <td><?= lang('Global.company') ?></td><td>: <span><?= $orders[0]['member'][0]['company'] ?></span></td>
  
                          </tr>
  
                          <tr>
  
                          <td><?= lang('Global.address') ?></td><td>: <span><?= $orders[0]['member'][0]['address'] ?></span></td>
  
                          </tr>
  
                        </tbody>
  
                      </table>
  
                  </div>
  
                  
  
                </div>
  
                <hr>
                <h3>Order</h3>
                <sm><a href="">Order code : <?= 'PIPETEST'.sprintf('%04d', $orders[0]['id']) ?></a></sm>
                <div class="text-right"> 
                <?php
                if ($orders[0]['deleted']==0){
                  ?>
                  <a href="<?= base_url('order/delete/'.$orders[0]['id']) ?>" class="btn btn-danger"><?= lang('Global.delete') ?> order</a>
                  <?php
                } else {
                  ?>
                  <span class="badge badge-danger mr-4">
                    
                    <span class="status">Deleted</span>
                    
                  </span>
                  <?php
                }
                ?>
                </div>
                <hr>
                <!-- <h3>Paket Tes</h3>   -->
                  <?php 
  
                      $total_harga = 0;
  
                      if(!empty($orders[0]['products'])){
  
                        foreach ($orders[0]['products'] as $key => $product) {
  
                          ?>
  
                  <div class="">
  
                    <div class="card-header border-0">
  
                      <h3><?= $product['nama'] ?></h3>
  
                      <span style="font-size: 13px"><?= $product['short_desc'] ?></span>
  
                        <br>
  
                          <th scope="row">
  
                          <div class="media align-items-center">
  
                            <div class="media-body">
  
                              <?php 
  
                               foreach ($product['jenistes'] as $key3 => $jenistes) {
  
                                ?>
  
                                  <span><b><?= $jenistes['nama'] ?></b></span>
  
                                  <br>
  
                                  <!-- <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span> -->
  
                                <?php
  
                               if($jenistes['status']=='' && $orders[0]['status']=='paid-confirm'){
  
                                  ?>
  
                                    <span class="badge badge-info mr-4">
  
                                      <span class="status"><?= lang('Global.ready_test') ?></span>
  
                                    </span>
  
                                    <br>
  
                                    <p style="font-size: 12px;">
  
                                    Tes bisa dimulai sejak : 
  
                                    <?php
  
                                    setlocale(LC_ALL, 'id-ID', 'id_ID');
  
                                    echo strftime("%A, %d %B %Y", strtotime($orders[0]['paid-confirm'])).', '.
  
                                    date('h:i:s A', strtotime($orders[0]['paid-confirm'])) 
  
                                    ?>
  
                                    </p>
  
                                  <?php
  
                                } else if($jenistes['status']=='start-test'){
  
                                  ?>
  
                                    <span class="badge badge-warning mr-4">
  
                                      <span class="status"><?= lang('Global.on_test') ?></span>
  
                                    </span>
  
                                    <br>
  
                                    <p style="font-size: 12px;">
  
                                    Tes bisa dimulai sejak : 
  
                                    <?php
  
                                    setlocale(LC_ALL, 'id-ID', 'id_ID');
  
                                    echo strftime("%A, %d %B %Y", strtotime($orders[0]['paid-confirm'])).', '.
  
                                    date('h:i:s A', strtotime($orders[0]['paid-confirm'])) 
  
                                    ?>
  
                                    <br>
  
                                    Anda telah memulai tes pada : 
  
                                    <?php
  
                                    setlocale(LC_ALL, 'id-ID', 'id_ID');
  
                                    echo strftime("%A, %d %B %Y", strtotime($jenistes['start-test'])).', '.
  
                                    date('h:i:s A', strtotime($jenistes['start-test'])) 
  
                                    ?>
  
                                    </p>
  
                                  <?php
  
                                } else if($jenistes['done-test']!='0000-00-00 00:00:00') {
  
                                  ?>
  
                                    <span class="badge badge-success mr-4">
  
                                      <span class="status"><?= lang('Global.done') ?></span>
  
                                    </span>
  
                                    <br>
  
                                    <?php
  
                                      $date1 = new DateTime($jenistes['start-test']);
  
                                      $date2 = new DateTime($jenistes['done-test']); 
  
                                      echo 'Anda menyelesaikan tes dalam : '; 
                                      if($jenistes['id_jenistes']==1) {
                                        // echo 9000-$jenistes['timer_checkpoint'];
                                      } else {
                                        echo gmdate("H:i:s", 3600-$jenistes['timer_checkpoint']);
                                      }
  
                                      if($date2->diff($date1)->d!=0){
  
                                        // echo $date2->diff($date1)->d.' hari ';
  
                                      }
  
                                      // echo $date2->diff($date1)->h.' jam '; 
  
                                      // echo $date2->diff($date1)->i.' menit '; 
  
                                      // echo $date2->diff($date1)->s.' detik '; 
  
                                      ?>
  
                                    <br>
  
                                    <p style="font-size: 12px;">
  
                                    Tes bisa dimulai sejak : 
  
                                    <?php
  
                                    setlocale(LC_ALL, 'id-ID', 'id_ID');
  
                                    echo strftime("%A, %d %B %Y", strtotime($orders[0]['paid-confirm'])).', '.
  
                                    date('h:i:s A', strtotime($orders[0]['paid-confirm'])) 
  
                                    ?>
  
                                    <br>
  
                                    Anda telah memulai tes pada : 
  
                                    <?php
  
                                    setlocale(LC_ALL, 'id-ID', 'id_ID');
  
                                    echo strftime("%A, %d %B %Y", strtotime($jenistes['start-test'])).', '.
  
                                    date('h:i:s A', strtotime($jenistes['start-test'])) 
  
                                    ?>
  
                                    <br>
  
                                    Anda telah menyelesaikan tes pada : 
  
                                    <?php
  
                                    setlocale(LC_ALL, 'id-ID', 'id_ID');
  
                                    echo strftime("%A, %d %B %Y", strtotime($jenistes['done-test'])).', '.
  
                                    date('h:i:s A', strtotime($jenistes['done-test'])) 
  
                                    ?>
  
                                    </p>
  
                                  <?php
  
                                } else {
  
                                  ?>
  
                                      <span class="badge badge-darker mr-4">
  
                                      <span class="status" style="color: #fff"><?= lang('Global.test_not_ready') ?></span>
  
                                    </span>
  
                                  <?php
  
                                }
  
                               }
  
                              ?>
  
                            </div>
  
                            </div>
                            Analisa psikolog : 
                            <?php 
                              // dd($product);
                              if($product['start-summarization']=='0000-00-00 00:00:00'){
              
                                ?>
              
                                <span class="badge badge-info mr-4">
              
                                  <span class="status"><?= lang('Global.ready_to_summarize') ?></span>
              
                                </span>
              
                                <?php
              
                              } else if ($product['status']=='start-summarization'){
              
                                ?>
              
                                <span class="badge badge-warning mr-4">
              
                                  <span class="status"><?= lang('Global.on_summarization') ?></span>
              
                                </span>
              
                                <?php
              
                              } else if ($product['status']=='summarized'){
              
                                ?>
              
                                <span class="badge badge-success mr-4">
              
                                  <span class="status"><?= lang('Global.done') ?></span>
              
                                </span> 
              
                                <?php
              
                              } else {
              
              
              
                              }
              
                            ?>
                            <h4><?php
                            if(!empty($product['first_name'])){
                              echo $product['first_name']." ".$product['last_name'];
                            }   ?></h4>

                            <div class="text-right"> 
  
                                <a href="<?= base_url('test/detail_admin/'.$orders[0]['id'].'/'.$product['id']) ?>" class="btn btn-primary"><?= lang('Global.detail') ?></a>
  
                                <?php 
  
                                if($jenistes['done-test']!='0000-00-00 00:00:00'){
  
                                  ?>
  
                                <a hidden href="<?= base_url('test/correction/null/'.$orders[0]['id'].'/'.$product['id']) ?>" class="btn  btn-secondary"><?= lang('Global.summary') ?></a>
                                  
                                <?php 
                                 if ($product['status']=='start-summarization'){
                  
                                    ?>
                  
                                    <a target="_blank" href="<?php echo base_url('pdf/mpdf/'.$orders[0]['id'].'/'.$product['id'].'/I');?>" class="btn btn btn-success"><?= lang('Global.preview_result') ?></a>
                  
                                    <?php
                  
                                  } else if ($product['status']=='summarized'){
                  
                                    ?>
                  
                                    <a target="_blank" href="<?php echo base_url('pdf/mpdf/'.$orders[0]['id'].'/'.$product['id'].'/D');?>" class="btn btn btn-success"><?= lang('Global.download_result') ?></a>
                  
                                    <?php
                  
                                  } else {
                  
                  
                  
                                  }
                                  }
  
                                ?>
  
                            </div>
  
                          </div>
  
                        </th>
  
                    </div>
  
                  <hr class="my-4" />
  
                          <?php
  
                        }
  
                        ?>
  
                        <?php
  
                      }
  
                    ?>
  
                  </div>  
  
              </div>
  
            </div>
  
          </div>
  
        </div>
    
</div>