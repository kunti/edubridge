<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Order</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="#">Peserta</a></li>
              <li class="breadcrumb-item active" aria-current="page">Kinerja Admin</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div>
      </div>
      <div class="row">
          <div class="col-xl-4 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah membuat akun">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Pending Payment</h5>
                    <span class="h2 font-weight-bold mb-0">232</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                      <i class="ni ni-watch-time"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                  <span class="text-nowrap">Since last month</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah menambahkan produk dalam keranjang lalu checkout menunggu pembayaran">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Paid</h5>
                    <span class="h2 font-weight-bold mb-0">222</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                      <i class="ni ni-check-bold"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                  <span class="text-nowrap">Since last month</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah membuat akun">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Waiting Confirmation</h5>
                    <span class="h2 font-weight-bold mb-0">232</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                      <i class="ni ni-watch-time"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                  <span class="text-nowrap">Since last month</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah menambahkan produk dalam keranjang lalu checkout menunggu pembayaran">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Confirmed</h5>
                    <span class="h2 font-weight-bold mb-0">222</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                      <i class="ni ni-check-bold"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                  <span class="text-nowrap">Since last month</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah membuat akun">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Not Confirmed</h5>
                    <span class="h2 font-weight-bold mb-0">232</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                      <i class="ni ni-watch-time"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                  <span class="text-nowrap">Since last month</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah menambahkan produk dalam keranjang lalu checkout menunggu pembayaran">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Refund</h5>
                    <span class="h2 font-weight-bold mb-0">222</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                      <i class="ni ni-check-bold"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                  <span class="text-nowrap">Since last month</span>
                </p>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0">Kinerja Admin</h3>
        </div>
        <!-- Light table -->
          <div class="table-responsive">
            <table id="dtBasicExample" class="table" width="100%" >
              <thead class="thead-light">
                <tr>
                  <th class="th-sm">ID Order
                  </th>
                  <th class="th-sm">Peserta
                  </th>
                  <th class="th-sm">Paket Tes
                  </th>
                  <th class="th-sm">Pembayaran
                  </th>
                  <th class="th-sm">Tes
                  </th>
                  <th class="th-sm">Action
                  </th>
                </tr>
              </thead>
              <tbody class="list">
                <!-- awal baris -->
                <tr>
                  <td>
                    <a href="">EB10987</a>
                  </td>
                  <th scope="row">
                    <div class="media align-items-center">
                      <a href="#" class="avatar rounded-circle mr-3">
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
                      </a>
                      <div class="media-body">
                        <span class="name mb-0 text-sm">Anandita Suryaningrum</span>
                        <br>
                        <span class="name mb-0 text-sm">089900998987</span>
                        <br>
                        <span class="name mb-0 text-sm">anandita@email.com</span>
                      </div>
                    </div>
                  </th>
                  <td>
                    <div class="media-body">
                        <span>Paket Tes Kecerdasan</span>
                        <br>
                        <span>Rp. 120.000</span>
                      </div>
                    </div>
                  </td>
                  <th scope="row">
                    <div class="media align-items-center">
                      <a href="#" class="avatar  mr-3">
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/bukti_bayar/anindita.jpg'); ?>">
                      </a>
                      <div class="media-body">
                        <span class="badge badge-dot mr-4">
                          <i class="bg-warning"></i>
                          <span class="status">Not approved</span>
                          <a href="#!" class="btn btn-sm btn-primary">Approve</a>
                        </span>
                        <br>
                        <span>2020-11-25 <br> 13:38:26</span>
                      </div>
                    </div>
                  </th>
                  <th scope="row">
                    <div class="media align-items-center">
                      <div class="media-body">
                        <span class="badge badge-dot mr-4">
                          <i class="bg-danger"></i>
                          <span class="status">Belum mengerjakan</span>
                        </span>
                      </div>
                    </div>
                  </th>
                  <!-- <td>
                    <div class="d-flex align-items-center">
                      <span class="completion mr-2">60%</span>
                      <div>
                        <div class="progress">
                          <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                        </div>
                      </div>
                    </div>
                  </td> -->
                  <td class="text-right">
                    <div class="dropdown">
                      <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v"></i>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <a class="dropdown-item" href="#">Detail</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                    </div>
                  </td>
                </tr>
                <!-- akhir baris -->
                <!-- awal baris -->
                <tr>
                  <td>
                    <a href="">EB34343</a>
                  </td>
                  <th scope="row">
                    <div class="media align-items-center">
                      <a href="#" class="avatar rounded-circle mr-3">
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/budi.jpg'); ?>">
                      </a>
                      <div class="media-body">
                        <span class="name mb-0 text-sm">Budi Setia Nugroho</span>
                        <br>
                        <span class="name mb-0 text-sm">086754654432</span>
                        <br>
                        <span class="name mb-0 text-sm">budi@email.com</span>
                      </div>
                    </div>
                  </th>
                  <td>
                    <div class="media-body">
                        <span>Paket Lengkap</span>
                        <br>
                        <span>Rp. 250.000</span>
                      </div>
                    </div>
                  </td>
                  <th scope="row">
                    <div class="media align-items-center">
                      <a href="#" class="avatar  mr-3">
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/bukti_bayar/sds.jpg'); ?>">
                      </a>
                      <div class="media-body">
                        <span class="badge badge-dot mr-4">
                          <i class="bg-success"></i>
                          <span class="status">Approved</span>
                        </span>
                        <br>
                        <span>2020-11-20 <br> 13:38:26</span>
                      </div>
                    </div>
                  </th>
                  <th scope="row">
                    <div class="media align-items-center">
                      <div class="media-body">
                        <span class="badge badge-dot mr-4">
                          <i class="bg-success"></i>
                          <span class="status">Selesai dikerjakan</span>
                          <a href="#!" class="btn btn-sm btn-primary">Nilai</a>
                        </span>
                      </div>
                    </div>
                  </th>
                  <!-- <td>
                    <div class="d-flex align-items-center">
                      <span class="completion mr-2">60%</span>
                      <div>
                        <div class="progress">
                          <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                        </div>
                      </div>
                    </div>
                  </td> -->
                  <td class="text-right">
                    <div class="dropdown">
                      <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v"></i>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <a class="dropdown-item" href="#">Detail</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                    </div>
                  </td>
                </tr>
                <!-- akhir baris -->
                <!-- awal baris -->
                <tr>
                  <td>
                    <a href="">EB10987</a>
                  </td>
                  <th scope="row">
                    <div class="media align-items-center">
                      <a href="#" class="avatar rounded-circle mr-3">
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
                      </a>
                      <div class="media-body">
                        <span class="name mb-0 text-sm">Anandita Suryaningrum</span>
                        <br>
                        <span class="name mb-0 text-sm">089900998987</span>
                        <br>
                        <span class="name mb-0 text-sm">anandita@email.com</span>
                      </div>
                    </div>
                  </th>
                  <td>
                    <div class="media-body">
                        <span>Paket Tes Kecerdasan</span>
                        <br>
                        <span>Rp. 120.000</span>
                      </div>
                    </div>
                  </td>
                  <th scope="row">
                    <div class="media align-items-center">
                      <a href="#" class="avatar  mr-3">
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/bukti_bayar/anindita.jpg'); ?>">
                      </a>
                      <div class="media-body">
                        <span class="badge badge-dot mr-4">
                          <i class="bg-success"></i>
                          <span class="status">Approved</span>
                        </span>
                        <br>
                        <span>2020-11-25 <br> 13:38:26</span>
                      </div>
                    </div>
                  </th>
                  <th scope="row">
                    <div class="media align-items-center">
                      <div class="media-body">
                        <span class="badge badge-dot mr-4">
                          <span>2020-11-25 13:38:26</span>
                          <br>
                          <i class="bg-default"></i>
                          <span class="status">Selesai dinilai</span>
                          <br>
                          <a href="#!" class="btn btn-sm btn-primary">Summary</a>
                        </span>
                      </div>
                    </div>
                  </th>
                  <!-- <td>
                    <div class="d-flex align-items-center">
                      <span class="completion mr-2">60%</span>
                      <div>
                        <div class="progress">
                          <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                        </div>
                      </div>
                    </div>
                  </td> -->
                  <td class="text-right">
                    <div class="dropdown">
                      <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v"></i>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <a class="dropdown-item" href="#">Detail</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                    </div>
                  </td>
                </tr>
                <!-- akhir baris -->
                <!-- awal baris -->
                <tr>
                  <td>
                    EB10987
                  </td>
                  <th scope="row">
                    <div class="media align-items-center">
                      <a href="#" class="avatar rounded-circle mr-3">
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
                      </a>
                      <div class="media-body">
                        <span class="name mb-0 text-sm">Anandita Suryaningrum</span>
                        <br>
                        <span class="name mb-0 text-sm">089900998987</span>
                        <br>
                        <span class="name mb-0 text-sm">anandita@email.com</span>
                      </div>
                    </div>
                  </th>
                  <td>
                    <div class="media-body">
                        <span>Paket Tes Kecerdasan</span>
                        <br>
                        <span>Rp. 120.000</span>
                      </div>
                    </div>
                  </td>
                  <th scope="row">
                    <div class="media align-items-center">
                      <a href="#" class="avatar  mr-3">
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/bukti_bayar/anindita.jpg'); ?>">
                      </a>
                      <div class="media-body">
                        <span class="badge badge-dot mr-4">
                          <i class="bg-success"></i>
                          <span class="status">Approved</span>
                        </span>
                        <br>
                        <span>2020-11-25 <br> 13:38:26</span>
                      </div>
                    </div>
                  </th>
                  <th scope="row">
                    <div class="media align-items-center">
                      <div class="media-body">
                        <span class="badge badge-dot mr-4">
                          <i class="bg-info"></i>
                          <span class="status">Selesai summary</span>
                          <a href="#!" class="btn btn-sm btn-primary">Export to pdf</a>
                        </span>
                      </div>
                    </div>
                  </th>
                  <!-- <td>
                    <div class="d-flex align-items-center">
                      <span class="completion mr-2">60%</span>
                      <div>
                        <div class="progress">
                          <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                        </div>
                      </div>
                    </div>
                  </td> -->
                  <td class="text-right">
                    <div class="dropdown">
                      <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v"></i>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <a class="dropdown-item" href="#">Detail</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                    </div>
                  </td>
                </tr>
                <!-- akhir baris -->
              </tbody>
              <thead class="thead-light">
                <tr>
                  <th class="th-sm">ID Order
                  </th>
                  <th class="th-sm">Peserta
                  </th>
                  <th class="th-sm">Paket Tes
                  </th>
                  <th class="th-sm">Pembayaran
                  </th>
                  <th class="th-sm">Tes
                  </th>
                  <th class="th-sm">Action
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        <!-- Card footer -->
        <!-- <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end mb-0">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <i class="fas fa-angle-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">
                  <i class="fas fa-angle-right"></i>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>
