<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Documentation</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="#">Jenis Tes</a></li>
              <li class="breadcrumb-item active" aria-current="page">Soal dan Jawaban</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0">Tes Kecerdasan</h3>
        </div>
        <!-- Card footer -->
        <div class="card-footer py-4">
          <div class="container">
            <div class="header-body text-center mb-7">
              <div class="row justify-content-center">
                <div class="col-lg-12 col-md-12">
                  <div class="row">
                    <div class="nav-wrapper">
                      <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                          <li class="nav-item">
                              <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true">Soal 01-20</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false">Soal 21-30</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false">Soal 31-40</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="true">Soal 01-20</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false">Soal 21-30</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false">Soal 31-40</a>
                          </li>
                      </ul>
                  </div>
                  <div class="card shadow">
                      <div class="card-body">
                          <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                <!-- awal soal jawaban -->
                                  <div class="text-center col-md-12">
                                    <div class="card-header border-0 text-left">
                                      <p>1. Pengaruh seseorang terhadap orang lain seharusnya bergantung pada. . . </p>
                                    </div>
                                    <div class="text-center">
                                      <!-- radio button -->
                                      <div>
                                        <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                          <input name="custom-radio-1" class="custom-control-input" id="customRadio1" type="radio">
                                          <label class="custom-control-label" for="customRadio1">Demokratis</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                          <input name="custom-radio-1" class="custom-control-input" id="customRadio2" checked="" type="radio">
                                          <label class="custom-control-label" for="customRadio2">Radikal</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                          <input name="custom-radio-1" class="custom-control-input" id="customRadio3" checked="" type="radio">
                                          <label class="custom-control-label" for="customRadio3">Liberal</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                          <input name="custom-radio-1" class="custom-control-input" id="customRadio4" checked="" type="radio">
                                          <label class="custom-control-label" for="customRadio4">Konservatif</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                          <input name="custom-radio-1" class="custom-control-input" id="customRadio5" checked="" type="radio">
                                          <label class="custom-control-label" for="customRadio5">Anarkis</label>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <hr class="my-3">
                                  <!-- akhir soal jawaban -->
                                  <!-- awal soal jawaban -->
                                  <div class="text-center col-md-12">
                                    <div class="card-header border-0 text-left">
                                      <p>2. Carilah kata yang kelima yang tidak memiliki kesamaan keempat kata berikut</p>
                                    </div>
                                    <div class="text-center">
                                      <!-- radio button -->
                                      <div>
                                        <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                          <input name="custom-radio-1" class="custom-control-input" id="customRadio1" type="radio">
                                          <label class="custom-control-label" for="customRadio1">Demokratis</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                          <input name="custom-radio-1" class="custom-control-input" id="customRadio2" checked="" type="radio">
                                          <label class="custom-control-label" for="customRadio2">Radikal</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                          <input name="custom-radio-1" class="custom-control-input" id="customRadio3" checked="" type="radio">
                                          <label class="custom-control-label" for="customRadio3">Liberal</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                          <input name="custom-radio-1" class="custom-control-input" id="customRadio4" checked="" type="radio">
                                          <label class="custom-control-label" for="customRadio4">Konservatif</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-radio mb-3 col-md-2">
                                          <input name="custom-radio-1" class="custom-control-input" id="customRadio5" checked="" type="radio">
                                          <label class="custom-control-label" for="customRadio5">Anarkis</label>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <hr class="my-3">
                                  <!-- akhir soal jawaban -->
                                  <!-- awal soal jawaban -->
                                  <div class="text-center col-md-12">
                                    <div class="card-header border-0 text-left">
                                      <p>3. mawar – melati</p>
                                    </div>
                                    <div class="text-center col-lg-6">
                                      <div class="form-group">
                                        <input type="text" id="isian" class="form-control" placeholder="Jawaban saya" value="">
                                      </div>
                                    </div>
                                  </div>
                                  <hr class="my-3">
                                  <!-- akhir soal jawaban -->
                                  <!-- awal soal jawaban -->
                                  <div class="text-center col-md-12">
                                    <div class="card-header border-0 text-left">
                                      <p>4. jika seseorang anak memiliki 50 rupiah dan memberikan 15 rupiah, berapa rupiahkah yang masih tinggal padanya?</p>
                                    </div>
                                    <div class="text-center">
                                      <div>
                                        <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                          <input class="custom-control-input" id="customCheck1" type="checkbox">
                                          <label class="custom-control-label" for="customCheck1">1</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                          <input class="custom-control-input" id="customCheck2" type="checkbox" checked>
                                          <label class="custom-control-label" for="customCheck2">2</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                          <input class="custom-control-input" id="customCheck3" type="checkbox" checked>
                                          <label class="custom-control-label" for="customCheck3">3</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                          <input class="custom-control-input" id="customCheck4" type="checkbox" checked>
                                          <label class="custom-control-label" for="customCheck4">4</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                          <input class="custom-control-input" id="customCheck5" type="checkbox">
                                          <label class="custom-control-label" for="customCheck5">5</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                          <input class="custom-control-input" id="customCheck6" type="checkbox" checked>
                                          <label class="custom-control-label" for="customCheck6">6</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                          <input class="custom-control-input" id="customCheck7" type="checkbox" checked>
                                          <label class="custom-control-label" for="customCheck7">7</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                          <input class="custom-control-input" id="customCheck8" type="checkbox" checked>
                                          <label class="custom-control-label" for="customCheck8">8</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                          <input class="custom-control-input" id="customCheck9" type="checkbox" checked>
                                          <label class="custom-control-label" for="customCheck9">9</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-checkbox mb-3 col-md-1">
                                          <input class="custom-control-input" id="customCheck10" type="checkbox" checked>
                                          <label class="custom-control-label" for="customCheck10">0</label>
                                        </div>
                                      </div>  
                                    </div>
                                  </div>
                                  <hr class="my-3">
                                  <!-- akhir soal jawaban -->
                                  <!-- awal soal jawaban -->
                                  <div class="text-center col-md-12">
                                    <div class="text-center">
                                      <div class="col-sm-12">
                                        5. 
                                        <img src="<?php echo base_url('src/assets/img/question/117.PNG'); ?>" class="rounded img-fluid" alt="" style="height: 200px;">
                                      </div>
                                      <!-- radio button -->
                                      <div>
                                        <!-- radio image  -->
                                        <input type="radio" name="image-radio-1" id="imageRadio1" class="input-hidden"  checked="" />
                                        <label for="imageRadio1">
                                          <img 
                                            src="<?php echo base_url('src/assets/img/answer/a.PNG'); ?>" alt="" />
                                        </label>
                                        <!-- end of radio image  -->
                                        <!-- radio image  -->
                                        <input type="radio" name="image-radio-1" id="imageRadio2" class="input-hidden" />
                                        <label for="imageRadio2">
                                          <img 
                                            src="<?php echo base_url('src/assets/img/answer/b.PNG'); ?>" alt="" />
                                        </label>
                                        <!-- end of radio image  -->
                                        <!-- radio image  -->
                                        <input type="radio" name="image-radio-1" id="imageRadio3" class="input-hidden" />
                                        <label for="imageRadio3">
                                          <img 
                                            src="<?php echo base_url('src/assets/img/answer/c.PNG'); ?>" alt="" />
                                        </label>
                                        <!-- end of radio image  -->
                                        <!-- radio image  -->
                                        <input type="radio" name="image-radio-1" id="imageRadio4" class="input-hidden" />
                                        <label for="imageRadio4">
                                          <img 
                                            src="<?php echo base_url('src/assets/img/answer/d.PNG'); ?>" alt="" />
                                        </label>
                                        <!-- end of radio image  -->
                                        <!-- radio image  -->
                                        <input type="radio" name="image-radio-1" id="imageRadio5" class="input-hidden" />
                                        <label for="imageRadio5">
                                          <img 
                                            src="<?php echo base_url('src/assets/img/answer/e.PNG'); ?>" alt="" />
                                        </label>
                                        <!-- end of radio image  -->
                                      </div>
                                    </div>
                                  </div>
                                  <hr class="my-3">
                                  <!-- akhir soal jawaban -->
                              </div>
                                </div>
                              </div>
                              <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                  <p class="description">Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
                              </div>
                              <div class="tab-pane fade" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                                  <p class="description">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth.</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>