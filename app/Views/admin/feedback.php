<div id="page-content">

    <nav class="navbar navbar-expand navbar-light bg-light mb-4">
        <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn btn-toggle-dashboard">
                <i class="fas fa-arrows-alt-h"></i>
                <!-- <span>Toggle Sidebar</span> -->
            </button>

            <div class="" id="navbarSupportedContent">
                <ul class="nav navbar-nav nav-content ml-auto">
                    <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('auth/logout/') ?>"><i class="fas fa-sign-out-alt"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-transparent pl-2">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">Feedback</a></li>
      </ol>
    </nav>

    <nav class="sort-bar mb-4">
        <div class="row">
            <div class="ul col-6">
                <button class="dt-button buttons-excel buttons-html5 all" tabindex="0" aria-controls="dtBasicExample" type="button"><span>All</span></button>
                <button class="dt-button btn bg-transparent mr-2 membermenu" id="btn-member-active" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Active</span></button>
                <button class="dt-button buttons-excel buttons-html5 membermenu" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Deleted</span></button>
            </div>
            <!-- <div class="col-6 text-right">
                <button data-toggle="tooltip" data-placement="bottom" title="Export as PDF" class="btn bg-transparent mr-2"><i class="fas fa-file-pdf"></i></button>
                <button data-toggle="tooltip" data-placement="bottom" title="Export as EXCEL" class="btn bg-transparent mr-2"><i class="fas fa-file-excel"></i></button>
                <button data-toggle="tooltip" data-placement="bottom" title="PRINT" class="btn bg-transparent"><i class="fas fa-print"></i></button>
            </div> -->
        </div>
        <br>
        Tgl daftar : <input type="text" id="min" name="min">-<input type="text" id="max" name="max">
    </nav>


    <!-- Light table -->

    <div class="table-responsive">

        <table id="dtBasicExample" class="table" width="100%" >

          <thead class="thead-light">
            <tr>
              <th class="th-sm">Photo</th>
              <th class="th-sm">Nama peserta</th>
              <th class="th-sm">Email</th>
              <th class="th-sm">No HP</th>
              <th class="th-sm">Jenis kelamin</th>
              
              <th class="th-sm">Tgl lahir</th>
              <th class="th-sm">Sekolah/Universitas</th>
              <th class="th-sm">Pend. terakhir</th>
              <th class="th-sm">Kelas/smtr</th>
              <th class="th-sm">Status

              </th>
              <th class="th-sm" hidden>Status</th>
              <th class="th-sm">Tanggal daftar</th>
              <th class="th-sm">Domisili
              </th>
              <th class="th-sm">Alamat
              </th>
              <th class="th-sm">Feedback

              </th>

              
              <th class="th-sm"><?= lang('Global.action') ?>
              </th>
            </tr>

          </thead>
          
          <tbody class="list">

            <?php 

            if (!empty($feedbacks)) {

              // dd($orders);

              foreach ($feedbacks as $key => $feedback) {

              // foreach ($feedback['orders'] as $key => $order) {

                ?>

                <tr>
                  <td>
                  <a href="#" class="avatar rounded-circle mr-3">

                    <img style="width:50px" src="

                    <?php 

                    if($feedback['avatar']=="" || @file_get_contents(base_url('public/uploads/avatar/'.$feedback['avatar']))==FALSE) { 

                        echo base_url('src/assets/img/profile/none.jpg'); 

                        } else { 

                            echo base_url('public/uploads/avatar/'.$feedback['avatar']); 

                        } 

                    ?>" class="rounded-circle img-responsive img-circle" />

                    </a>
                  </td>
                  <td scope="row">
                    <span class="name mb-0"><a href="<?= base_url('account/profile/').'/'.$feedback['id'] ?>">
                    <span class="name mb-0 "><?= $feedback['first_name'].' '.$feedback['last_name']?></span>
                    </a></span>
                  </td>
                  <td scope="row">
                    <?= $feedback['email'] ?>
                  </td>
                  <td scope="row">
                    <a href="https://api.whatsapp.com/send?phone=+628<?= substr($feedback['phone'], 1) ?>" target="_blank">
                    <span class="name mb-0"><?= $feedback['phone'] ?></span>
                    </a>
                  </td>
                  <td scope="row">
                    <?= $feedback['sex'] ?>
                  </td>
                  <td scope="row">
                    <?= $feedback['birthdate'] ?>
                  </td>
                  <td scope="row">
                    <?= $feedback['company'] ?>
                  </td>
                  <td scope="row">
                    <?= $feedback['education_level'] ?>
                  </td>
                  <td scope="row">
                    <?= $feedback['semester'] ?>
                  </td>
                  <td>
                  <?php 

                      if ($feedback['not_deleted']==1){

                        ?>

                        <span class="badge badge-success mr-4">

                          <span class="status">Active</span>

                        </span>

                        <?php

                      }

                      elseif ($feedback['not_deleted']==0){

                          ?>

                          <span class="badge badge-warning mr-4">

                            <span class="status">Deleted</span>

                          </span>

                          <?php

                      }

                      // if($feedback['volunteer'] == '1') {

                      //       echo "

                      //       <span class='badge badge-info mr-4'>

                      //         <span class='status'>Volunteer</span>

                      //       </span>

                      //       ";

                      //     }

                          ?>
                  </td>
                  <td>
                    <?php
                    if ($feedback['created_on']){
                      $timezone  = +7; //(GMT -5:00) EST (U.S. & Canada)
                      echo gmdate("Y-m-d H:i:s", $feedback['created_on'] + 3600*($timezone+date("I")));
                    }
                    ?>
                  </td>
                  <td class="text-wrap">
                    <?= $feedback['domisili'] ?>
                  </td>
                  <td class="text-wrap">
                    <?= $feedback['address'] ?>
                  </td>
                  

                  <td>
                  <?= $feedback['feedback'] ?>
                  </td>
                  <td hidden>
                  <div class="media align-items-center">

                  <div class="media-body">

                    <span class="badge badge-dot mr-4">

                      <?php 

                      if ($feedback['status']==1){

                        ?>
                        <span class="badge badge-success mr-4">
                          <span class="status">Ditampilkan</span>
                        </span>

                        <?php

                      } else {

                        ?>

                        <span class="badge badge-danger mr-4">
                          <span class="status">Disembunyikan</span>
                        </span>

                        <?php

                      }

                      ?>

                    </span>

                  </div>

                </div>
                  </td>

                  <td class="text-left">

                    <a href="<?= base_url('account/profile/'.$feedback['id']) ?>" class="btn btn-sm btn-primary"><?= lang('Global.detail') ?></a>

                    <!-- <a href="#" class="btn btn-sm btn-secondary"><?= lang('Global.resend_activation') ?></a> -->

                  </td>

                </tr>

                <?php

              }

            // }

            }

            ?>

          </tbody>

          <tfoot class="tfoot thead-light">
            <tr>
            <th class="th-sm">Photo</th>
              <th class="th-sm">Nama peserta</th>
              <th class="th-sm">Email</th>
              <th class="th-sm">No HP</th>
              <th class="th-sm">Jenis kelamin</th>
              
              <th class="th-sm">Tgl lahir</th>
              <th class="th-sm">Sekolah/Universitas</th>
              <th class="th-sm">Pend. terakhir</th>
              <th class="th-sm">Kelas/smtr</th>
              <th class="th-sm">Status

              </th>
              <th class="th-sm" hidden>Status</th>
              <th class="th-sm">Tanggal daftar</th>
              <th class="th-sm">Domisili
              </th>
              <th class="th-sm">Alamat
              </th>
              <th class="th-sm">Feedback

              </th>

              
              <th class="th-sm"><?= lang('Global.action') ?>
              </th>
            </tr>

          </tfoot>

        </table>

      </div>
</div>