
<div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Dashboard</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
                </ol>
              </nav>
            </div>
           <!--  <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div> -->
          </div>
          <!-- Card stats -->
          <div class="row">
            <div class="col-md-6">
              <div class="card card-stats">
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah membuat akun">
                  <div class="row">
                    <div class="col">
                      <h5 class="h2 card-title text-uppercase text-muted mb-0"><?= lang('Global.student') ?></h5>
                      <span class="h1 font-weight-bold mb-0"><?= $member ?></span>
                    </div>
                    <div class="col">
                      <table>
                      <tr>
                        <td>Active</td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $member_active ?></td>
                      </tr>
                      <tr>
                        <td>Inactive</td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $member_inactive ?></td>
                      </tr>
                      </table>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape text-white rounded-circle shadow" style="background-color: #fdc834">
                        <i class="ni ni-single-02"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-plus"></i> </span>
                    <span class="text-nowrap"><?= lang('Global.student') ?> today</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-stats">
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Banyaknya produk yang diorder oleh peserta">
                  <div class="row">
                    <div class="col">
                      <h5 class="h2 card-title text-uppercase text-muted mb-0">Order</h5>
                      <span class="h1 font-weight-bold mb-0"><?= $orders ?></span>
                    </div>
                    <div class="col">
                      <table>
                      <tr>
                        <td>
                          <span class='badge badge-warning'>
                            <span class='status'><?= lang('Global.pending') ?></span>
                          </span>
                      </td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $pending ?></td>
                      </tr>
                      <!-- <tr>
                        <td>
                          <span class='badge badge-success'>
                            <span class='status'><?= lang('Global.paid') ?></span>
                          </span> </td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $paid ?></td>
                      </tr> -->
                      <tr>
                        <td>
                          <span class='badge badge-warning'>
                            <span class='status'><?= lang('Global.waiting_confirmation') ?></span>
                          </span></td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $paid ?></td>
                      </tr>
                      <tr>
                        <td>
                          <span class='badge badge-success'>
                            <span class='status'><?= lang('Global.confirmed') ?></span>
                          </span></td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $paid_confirm ?></td>
                      </tr>
                     <!--  <tr>
                        <td>Canceled</td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= '' ?></td>
                      </tr> -->
                      </table>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape text-white rounded-circle shadow" style="background-color: #fdc834">
                        <i class="ni ni-cart"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-plus"></i> </span>
                    <span class="text-nowrap">Order today</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-stats">
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="">
                  <div class="row">
                    <div class="col">
                      <h5 class="h2 card-title text-uppercase text-muted mb-0"><?= lang('Global.test_package') ?></h5>
                      <span class="h1 font-weight-bold mb-0"><?= $orderproduct ?></span>
                    </div>
                    <div class="col">
                      <table>
                      <tr>
                        <td>
                          <span class="badge badge-info">
                            <span class="status"><?= lang('Global.ready_test') ?></span>
                          </span>
                          </td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $order_ready ?></td>
                      </tr>
                      <tr>
                        <td>
                          <span class="badge badge-warning">
                            <span class="status"><?= lang('Global.on_test') ?></span>
                          </span>
                        </td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $order_ontest ?></td>
                      </tr>
                      <tr>
                        <td>
                          <span class="badge badge-success">
                            <span class="status"><?= lang('Global.done') ?></span>
                          </span>
                        </td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $order_done ?></td>
                      </tr>
                      <tr>
                        <td>
                          <span class="badge badge-warning">
                            <span class="status"><?= lang('Global.on_summarization') ?></span>
                          </span>
                        </td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $start_summarization ?></td>
                      </tr>
                      <tr>
                        <td>
                          <span class="badge badge-success">
                            <span class="status"><?= lang('Global.summarized') ?></span>
                          </span>
                        </td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $summarized ?></td>
                      </tr>
                      </table>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape text-white rounded-circle shadow" style="background-color: #fdc834">
                        <i class="ni ni-collection"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-plus"></i> </span>
                    <span class="text-nowrap">Order today</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-6" hidden="">
              <div class="card card-stats">
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah membuat akun">
                  <div class="row">
                    <div class="col">
                      <h5 class="h2 card-title text-uppercase text-muted mb-0"><?= lang('Global.jenis_tes') ?></h5>
                      <span class="h1 font-weight-bold mb-0"><?= $jenis_tes ?></span>
                    </div>
                    <div class="col">
                      <table>
                      <tr>
                        <td>
                          <span class="badge badge-info">
                            <span class="status"><?= lang('Global.ready_test') ?></span>
                          </span>
                          </td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $member ?></td>
                      </tr>
                      <tr>
                        <td>
                          <span class="badge badge-warning">
                            <span class="status"><?= lang('Global.on_test') ?></span>
                          </span>
                        </td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $member ?></td>
                      </tr>
                      <tr>
                        <td>
                          <span class="badge badge-success">
                            <span class="status"><?= lang('Global.done') ?></span>
                          </span>
                        </td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $done_test ?></td>
                      </tr>
                      </table>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape text-white rounded-circle shadow" style="background-color: #fdc834">
                        <i class="ni ni-"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-plus"></i> 0</span>
                    <span class="text-nowrap"><?= lang('Global.student') ?> today</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-6" hidden="">
              <div class="card card-stats">
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah membuat akun">
                  <div class="row">
                    <div class="col">
                      <h5 class="h2 card-title text-uppercase text-muted mb-0">Feedback</h5>
                      <span class="h1 font-weight-bold mb-0"><?= $member ?></span>
                    </div>
                    <div class="col">
                      <table>
                      <tr>
                        <td>Shown</td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $member ?></td>
                      </tr>
                      <tr>
                        <td>Hidden</td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $member ?></td>
                      </tr>
                      </table>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape text-white rounded-circle shadow" style="background-color: #fdc834">
                        <i class="ni ni-bullet-list-67"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-plus"></i> 0</span>
                    <span class="text-nowrap"><?= lang('Global.student') ?> today</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-6" hidden="">
              <div class="card card-stats">
                <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah membuat akun">
                  <div class="row">
                    <div class="col">
                      <h5 class="h2 card-title text-uppercase text-muted mb-0"><?= lang('Global.psikolog') ?></h5>
                      <span class="h1 font-weight-bold mb-0"><?= $member ?></span>
                    </div>
                    <div class="col">
                      <table>
                      <tr>
                        <td>Active</td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $member ?></td>
                      </tr>
                      <tr>
                        <td>Inactive</td>
                        <td style="padding-left: 10px"><span class="h2 font-weight-bold mb-0"><?= $member ?></td>
                      </tr>
                      </table>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape text-white rounded-circle shadow" style="background-color: #fdc834">
                        <i class="ni ni-satisfied"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-plus"></i> 0</span>
                    <span class="text-nowrap"><?= lang('Global.student') ?> today</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6" hidden="">
      <div class="row">
        <div class="col-xl-8">
          <div class="card bg-defalut">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col" data-toggle="tooltip" data-placement="bottom" title="Data checkout peserta yang telah terkonfirmasi pembayarannya">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Sales Overview</h6>
                  <h5 class="h3 text mb-0">Product Sales Statistic</h5>
                  <br>
                </div>
              </div>
              <div class="row align-items-center">
                <div class="col">
                  <ul class="nav nav-pills justify-content-end">
                    <li class="nav-item mr-2 mr-md-0" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 10, 30, 15, 40, 20, 60, 60]}]}}' data-prefix="Rp " data-suffix="k">
                      <a href="#" class="nav-link py-2 px-3 active" data-toggle="tab">
                        <span class="d-none d-md-block">All</span>
                      </a>
                    </li>
                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="Rp " data-suffix="k">
                      <a href="#" class="nav-link py-2 px-3" data-toggle="tab">
                        <span class="d-none d-md-block">Paket Lengkap</span>
                      </a>
                    </li>
                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="Rp " data-suffix="k">
                      <a href="#" class="nav-link py-2 px-3" data-toggle="tab">
                        <span class="d-none d-md-block">Tes Kecerdasan</span>
                      </a>
                    </li>
                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="Rp " data-suffix="k">
                      <a href="#" class="nav-link py-2 px-3" data-toggle="tab">
                        <span class="d-none d-md-block">Tes Minat Bakat</span>
                      </a>
                    </li>
                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="Rp " data-suffix="k">
                      <a href="#" class="nav-link py-2 px-3" data-toggle="tab">
                        <span class="d-none d-md-block">Tes Kepribadian</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta">
              <!-- Chart -->
              <div class="chart">
                <!-- Chart wrapper -->
                <canvas id="chart-sales-dark" class="chart-canvas"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4">
          <div class="card">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Sales Overview</h6>
                  <h5 class="h3 mb-0">Product Sales Summary</h5>
                </div>
              </div>
            </div>
            <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta">
              <!-- Chart -->
              <div class="chart">
                <canvas id="chart-bars" class="chart-canvas"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-6">
          <div class="card">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Sales Overview</h6>
                  <h5 class="h3 mb-0">Payment Summary</h5>
                </div>
              </div>
            </div>
            <div class="card-body">
              <!-- Chart -->
              <div class="chart">
              <canvas id="paymentChart" width="100" height="100"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-12">
          <div class="card">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Sales Overview</h6>
                  <h5 class="h3 mb-0">Payment Confirmation Summary</h5>
                </div>
              </div>
            </div>
            <div class="card-body">
              <!-- Chart -->
              <div class="chart">
              <canvas id="confirmationChart" width="100" height="100"></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- <div class="row">
        <div class="col-xl-8">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Page visits</h3>
                </div>
                <div class="col text-right">
                  <a href="#!" class="btn btn-sm btn-primary">See all</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Page name</th>
                    <th scope="col">Visitors</th>
                    <th scope="col">Unique users</th>
                    <th scope="col">Bounce rate</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">
                      /argon/
                    </th>
                    <td>
                      4,569
                    </td>
                    <td>
                      340
                    </td>
                    <td>
                      <i class="fas fa-plus text-success mr-3"></i> 46,53%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      /argon/index.html
                    </th>
                    <td>
                      3,985
                    </td>
                    <td>
                      319
                    </td>
                    <td>
                      <i class="fas fa-arrow-down text-warning mr-3"></i> 46,53%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      /argon/charts.html
                    </th>
                    <td>
                      3,513
                    </td>
                    <td>
                      294
                    </td>
                    <td>
                      <i class="fas fa-arrow-down text-warning mr-3"></i> 36,49%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      /argon/tables.html
                    </th>
                    <td>
                      2,050
                    </td>
                    <td>
                      147
                    </td>
                    <td>
                      <i class="fas fa-plus text-success mr-3"></i> 50,87%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      /argon/profile.html
                    </th>
                    <td>
                      1,795
                    </td>
                    <td>
                      190
                    </td>
                    <td>
                      <i class="fas fa-arrow-down text-danger mr-3"></i> 46,53%
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-xl-4">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Social traffic</h3>
                </div>
                <div class="col text-right">
                  <a href="#!" class="btn btn-sm btn-primary">See all</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Referral</th>
                    <th scope="col">Visitors</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">
                      Facebook
                    </th>
                    <td>
                      1,480
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">60%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      Facebook
                    </th>
                    <td>
                      5,480
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">70%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;"></div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      Google
                    </th>
                    <td>
                      4,807
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">80%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-primary" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"></div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      Instagram
                    </th>
                    <td>
                      3,678
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">75%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;"></div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      twitter
                    </th>
                    <td>
                      2,645
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">30%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-warning" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> -->