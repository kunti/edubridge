    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.settings') ?></li>
                </ol>
              </nav>
            </div>
            <!-- <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-12 order-xl-1">
        <form role="form" action="<?= base_url('admin/save_setting') ?>" method="post" accept-charset="utf-8">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-7">
                  <h3 class="mb-0"><?= lang('Global.settings') ?></h3>
                </div>
                <div class="col-5 text-right">
                  
                    <input type="submit" name="submit" value="Save changes" class="btn btn-xl btn-success">
                  
                </div>
              </div>
            </div>
            <div class="card-body">
              <h6 class="heading-small text-muted mb-4">Company</h6>
              <div class="pl-lg-4">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Name</label>
                      <input type="text" id="Company" name="Company" class="form-control" value="<?= $setting['Company']; ?>" required>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Phone</label>
                      <input type="text" id="Phone" name="Phone" class="form-control" value="<?= $setting['Phone']; ?>" required>
                    </div>
                  </div>                  
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Website</label>
                      <input type="text" id="Website" name="Website" class="form-control" value="<?= $setting['Website']; ?>" required>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Address</label>
                      <textarea rows="4" class="form-control" id="Address" name="Address" placeholder="" required><?= $setting['Address']; ?></textarea>
                    </div>
                  </div>  
                </div>
              </div>
              <hr class="my-4" />
              <!-- Address -->
              <h6 class="heading-small text-muted mb-4">PDF</h6>
              <div class="pl-lg-4">
                <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Note</label>
                      <textarea rows="4" class="form-control" id="Note_pdf" name="Note_pdf" placeholder=""><?= $setting['Note_pdf']; ?></textarea>
                    </div>
                  </div>                     
              </div>
          </div>
              <hr class="my-4" />
              <h6 class="heading-small text-muted mb-4">Banner</h6>
              <div class="pl-lg-4">
                <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Services</label>
                      
                    </div>
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Step kuliah</label>
                     
                    </div>
                  </div>                     
              </div>
          </div>
        </div>
        </div>
      </div>
              </form>
