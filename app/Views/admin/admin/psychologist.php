<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0"><?= lang('Global.psikolog') ?></h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <!-- <li class="breadcrumb-item"><a href="#">Psikolog</a></li> -->
              <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.psikolog') ?></li>
            </ol>
          </nav>
        </div>
       <!--  <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
      <div class="row" hidden="">
          <div class="col-xl-4 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah membuat akun">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">
                      <span class="badge badge-info mr-4" style="font-size: 100%">
                      <span class="status">Ready to correct</span>
                    </span>
                    </h5>
                    <span class="h2 font-weight-bold mb-0"><?= $all['ready_to_correct'] ?></span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                      <i class="ni ni-watch-time"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                  <span class="text-nowrap">Since last month</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah menambahkan produk dalam keranjang lalu checkout menunggu pembayaran">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">
                    <span class="badge badge-warning mr-4" style="font-size: 100%">
                      <span class="status">On Correction</span>
                    </span>
                    </h5>
                    <span class="h2 font-weight-bold mb-0"><?= $all['start_correction'] ?></span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-warning text-white rounded-circle shadow">
                      <i class="ni ni-ruler-pencil"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                  <span class="text-nowrap">Since last month</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah menambahkan produk dalam keranjang lalu checkout menunggu pembayaran">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">
                    <span class="badge badge-success mr-4" style="font-size: 100%">
                      <span class="status">Corrected</span>
                    </span>
                    </h5>
                    <span class="h2 font-weight-bold mb-0"><?= $all['corrected'] ?></span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                      <i class="ni ni-check-bold"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                  <span class="text-nowrap">Since last month</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah membuat akun">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">
                      <span class="badge badge-info mr-4" style="font-size: 100%">
                      <span class="status">Ready to summarize</span>
                    </span>
                    </h5>
                    <span class="h2 font-weight-bold mb-0"><?= $all['ready_to_summarized'] ?></span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                      <i class="ni ni-watch-time"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                  <span class="text-nowrap">Since last month</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah menambahkan produk dalam keranjang lalu checkout menunggu pembayaran">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">
                    <span class="badge badge-warning mr-4" style="font-size: 100%">
                      <span class="status">On Summarization</span>
                    </span>
                    </h5>
                    <span class="h2 font-weight-bold mb-0"><?= $all['start_summarization'] ?></span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-warning text-white rounded-circle shadow">
                      <i class="ni ni-ruler-pencil"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                  <span class="text-nowrap">Since last month</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta yang telah menambahkan produk dalam keranjang lalu checkout menunggu pembayaran">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">
                    <span class="badge badge-success mr-4" style="font-size: 100%">
                      <span class="status">Summarized</span>
                    </span>
                    </h5>
                    <span class="h2 font-weight-bold mb-0"><?= $all['summarized'] ?></span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                      <i class="ni ni-check-bold"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                  <span class="text-nowrap">Since last month</span>
                </p>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h3 class="mb-0"><?= lang('Global.psikolog') ?></h3>
            </div>
            <div class="col text-right">
              <a href="#!" class="btn btn btn-primary" data-toggle="modal" data-target="#modal-form">Add Psychologist</a>
            </div>
          </div>
        </div>
        <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Psychologist</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form role="form" action="<?= base_url('auth/create_psychologist'); ?>" method="post" accept-charset="utf-8">
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class=""></i></span>
                    </div>
                    <input class="form-control" placeholder="First Name" type="text" id="first_name" name="first_name" value="">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class=""></i></span>
                    </div>
                    <input class="form-control" placeholder="Last Name" type="text" id="last_name" name="last_name" value="">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class=""></i></span>
                    </div>
                    <input class="form-control" placeholder="Company" type="text" id="company" name="company" value="">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class=""></i></span>
                    </div>
                    <input class="form-control" placeholder="Phone / Whatsapp" type="text" id="phone" name="phone" value="">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" placeholder="Email" type="email" id="email" name="email" value="">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="form-control" placeholder="Password" type="password" id="password" name="password" value="">
                  </div>
                    <small>The Password field must be at least 8 characters in length.</small>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="form-control" placeholder="Confirm Password" type="password" id="password_confirm" name="password_confirm" value="">
                  </div>
                </div>
                <input type="hidden" name="groups" value="3">
               
                <div class="text-center">
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" name="submit" value="Create User" class="btn btn-primary">Create Psychologist</button>
                <!-- <button type="button" class="btn btn-primary">Create Psychologist</button> -->
              </form>
              </div>
            </div>
          </div>
        </div>
        <!-- Light table -->
          <div class="table-responsive">
            <table id="dtBasicExample" class="table" width="100%" >
              <thead class="thead-light">
                <tr>
                  <th class="th">Psychologist
                  </th>
                  <!-- <th class="th">
                    <span class="badge badge-warning mr-4" style="font-size: 88%">
                      <span class="status">On Correction</span>
                    </span>
                  </th>
                  <th class="th">
                    <span class="badge badge-success mr-4" style="font-size: 88%">
                      <span class="status">Corrected</span>
                    </span>
                  </th> -->
                  <th class="th">
                    <span class="badge badge-warning mr-4" style="font-size: 88%">
                      <span class="status">On Summarization</span>
                    </span>
                  </th>
                  <th class="th">
                    <span class="badge badge-success mr-4" style="font-size: 88%">
                      <span class="status">Summarized</span>
                    </span>
                  </th>
                  <th class="th">Status
                  </th>
                  <th class="th">Action
                  </th>
                </tr>
              </thead>
              <tbody class="list">
                <?php 
                if (!empty($psychologists)) {
                  // dd($orders);
                  foreach ($psychologists as $key => $psychologist) {
                  // foreach ($psychologist['orders'] as $key => $order) {
                    ?>
                    <tr>
                      <th scope="row">
                        <div class="media align-items-center">
                          <a href="#" class="avatar rounded-circle mr-3">
                            <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
                          </a>
                          <div class="media-body text-wrap" >
                            <span class="name mb-0"><a href="<?= base_url('account/profile/').'/'.$psychologist['id'] ?>">
                            <span class="name mb-0 "><?= $psychologist['first_name'].' '.$psychologist['last_name']?></span>
                            </a></span>
                            <br>
                            <a href="https://api.whatsapp.com/send?phone=+628<?= substr($psychologist['phone'], 1) ?>" target="_blank">
                            <span class="name mb-0"><?= $psychologist['phone'] ?></span>
                            </a>
                            <br>
                            <span class="name mb-0"><?= $psychologist['email'] ?></span>
                          </div>
                      </th>
                     <!--  <td>
                        <div class="media-body">
                            <span><?= $psychologist['start_correction'] ?></span>
                        </div>
                      </td>
                      <td>
                        <div class="media-body">
                            <span><?= $psychologist['corrected'] ?></span>
                        </div>
                      </td> -->
                      <td>
                        <div class="media-body">
                            <span><?= $psychologist['start_summarization'] ?></span>
                        </div>
                      </td>
                      <td>
                        <div class="media-body">
                            <span><?= $psychologist['summarized'] ?></span>
                        </div>
                      </td>
                      <td>
                        <span class="badge badge-dot mr-4">
                          <i class="bg-<?php if($psychologist['active']=='1') {echo 'success';} else {echo 'danger';}   ?>"></i>
                          <span class="status"><?php if($psychologist['active']=='1') {echo 'Active';} else {echo 'Non active';}  ?></span>
                        </span>
                      </td>
                      <td class="text-left">
                        <a href="<?= base_url('psychologist/id/'.$psychologist['id']) ?>" class="btn btn-sm btn-primary">Detail</a>
                      </td>
                    </tr>
                    <?php
                  }
                // }
                }
                ?>
              </tbody>
              <thead class="thead-light">
                <tr>
                  <th class="th-sm">Psychologist
                  </th>
               <!--    <th class="th">
                    <span class="badge badge-warning mr-4" style="font-size: 88%">
                      <span class="status">On Correction</span>
                    </span>
                  </th>
                  <th class="th">
                    <span class="badge badge-success mr-4" style="font-size: 88%">
                      <span class="status">Corrected</span>
                    </span>
                  </th> -->
                  <th class="th">
                    <span class="badge badge-warning mr-4" style="font-size: 88%">
                      <span class="status">On Summarization</span>
                    </span>
                  </th>
                  <th class="th">
                    <span class="badge badge-success mr-4" style="font-size: 88%">
                      <span class="status">Summarized</span>
                    </span>
                  </th>
                  <th class="th-sm">Status
                  </th>
                  <th class="th-sm">Action
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        <!-- Card footer -->
        <!-- <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end mb-0">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <i class="fas fa-angle-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">
                  <i class="fas fa-angle-right"></i>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>
