<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Feedback</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
              <!-- <li class="breadcrumb-item"><a href="#">Peserta</a></li> -->
              <li class="breadcrumb-item active" aria-current="page">Feedback</li>
            </ol>
          </nav>
        </div>
       <!--  <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0">Feedback</h3>
        </div>
        <!-- Light table -->
          <div class="table-responsive text-wrap">
            <table id="dtBasicExample" class="table" width="100%" >
              <thead class="thead-light">
                <tr>
                  <th class="th-sm"><?= lang('Global.student') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.datetime') ?>
                  </th>
                  <th class="th-sm">Feedback
                  </th>
                  <th class="th-sm">Status
                  </th>
                  <th class="th-sm"><?= lang('Global.action') ?>
                  </th>
                </tr>
              </thead> 
              <tbody class="list">
                <?php 
                if ($feedbacks){
                  foreach ($feedbacks as $key => $feedback) {
                    ?>
                <!-- awal baris -->
                <tr>
                  <th scope="row">
                    <div class="media align-items-center">
                      <a href="#" class="avatar rounded-circle mr-3">
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
                      </a>
                      <div class="media-body text-wrap" >
                        <span class="name mb-0"><a href="<?= base_url('account/profile/').'/'.$feedback['id'] ?>">
                        <span class="name mb-0 "><?= $feedback['first_name'].' '.$feedback['last_name']?></span>
                        </a></span>
                        <br>
                        <a href="https://api.whatsapp.com/send?phone=+628<?= substr($feedback['phone'], 1) ?>" target="_blank">
                        <span class="name mb-0"><?= $feedback['phone'] ?></span>
                        </a>
                        <br>
                        <span class="name mb-0"><?= $feedback['email'] ?></span>
                      </div>

                  </th>
                  <td>
                    <div class="media-body">
                        <span><?= $feedback['created_at'] ?></span>
                      </div>
                    </div>
                  </td>
                  <td style="width: 400px; ">
                      <span class="text-wrap"><?= $feedback['feedback'] ?></span>
                  </td>
                  <th scope="row">
                    <div class="media align-items-center">
                      <div class="media-body">
                        <span class="badge badge-dot mr-4">
                          <?php 
                          if ($feedback['status']==1){
                            ?>
                            <i class="bg-success"></i>
                            <span class="status"><?= lang('Global.showed') ?></span>
                            <?php
                          } else {
                            ?>
                            <i class="bg-warning"></i>
                            <span class="status"><?= lang('Global.hidden') ?></span>
                            <?php
                          }
                          ?>
                        </span>
                      </div>
                    </div>
                  </th>
                  <td class="text-right">
                    <a href="<?= base_url('feedback/detail/'.$feedback['id']) ?>" class="btn btn-sm btn-primary"><?= lang('Global.detail') ?></a>
                  </td>
                </tr>
                <!-- akhir baris -->

                    <?php
                  }
                }
                ?>
                
              </tbody>
              <thead class="thead-light">
                <tr>
                  <th class="th-sm"><?= lang('Global.student') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.datetime') ?>
                  </th>
                  <th class="th-sm">Feedback
                  </th>
                  <th class="th-sm">Status
                  </th>
                  <th class="th-sm"><?= lang('Global.action') ?>
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        <!-- Card footer -->
        <!-- <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end mb-0">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <i class="fas fa-angle-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">
                  <i class="fas fa-angle-right"></i>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>
