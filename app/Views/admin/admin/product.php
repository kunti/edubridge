<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <!-- <h6 class="h2 text-white d-inline-block mb-0">Psikolog</h6> -->
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.test_package') ?></li>
            </ol>
          </nav>
        </div>
       <!--  <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="section features-1" style="background-color: #232941; color: #FDC834">
      <div class="container">
        <div class="row">
          <?php if(!empty($products)) {
            foreach($products as $key => $item) { ?>
                <div class="col-md-3 text-center d-flex flex-column" style="padding-top: 12px; padding-bottom: 24px;">
                  <div class="info">
                    <span class="avatar avatar-lg rounded-circle">
                    <img alt="Image placeholder" src="<?= base_url('src/assets/img/product/'.$item['id'].'.png') ?>">
                  </span>
                    <a href="<?php echo base_url('product/id/'.$item['id']) ?>"><h6 class="info-title text-uppercase" style="color: #FDC834; font-size: 1rem;"><?= $item['nama']; ?></h6></a>
                    <p class="description opacity-8"><?= $item['short_desc']; ?></p>
                  </div>
                  <br>
                  <a href="<?php echo base_url('product/id/'.$item['id']) ?>" class="mt-auto btn btn-primary btn-icon mt-3 mb-sm-0">
                      <span class="btn-inner--icon"><?=lang('Global.detail');?></span>
                      <!-- <span class="btn-inner--text">Learn more</span> -->
                    </a>
                </div>    
          <?php }
          } ?>
        </div>
        <div class="container">
          
        </div>
      </div>
    </div>