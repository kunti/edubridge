<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <!-- <h6 class="h2 text-white d-inline-block mb-0">Order</h6> -->
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="<?= base_url('feedback'); ?>">Feedback</a></li>
              <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.feedback_detail') ?></li>
            </ol>
          </nav>
        </div>
        <!-- <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0"><?= lang('Global.feedback_detail') ?></h3>
        </div>
        <!-- Light table -->
          <div class="container">
            <div class="media align-items-center">
              <a href="#" class="avatar rounded-circle mr-3">
                <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/cahyaning.jpg'); ?>">
              </a>
              <div class="media-body">
                <span class="name mb-0 text-sm"><?= $feedbacks[0]['first_name'].' '.$feedbacks[0]['last_name'] ?></span>
                <br>
                <a href="https://api.whatsapp.com/send?phone=+628<?= substr($feedbacks[0]['phone'], 1) ?>" target="_blank">
                <span class="name mb-0 text-sm"><?= $feedbacks[0]['phone'] ?></span>
                </a>
                <br>
                <span class="name mb-0 text-sm"><?= $feedbacks[0]['email'] ?></span>
              </div>
            </div>
            <br>
            <form action="<?= base_url('feedback/save') ?>" method="post">
            <textarea class="form-control" id="exampleFormControlTextarea1" name="feedback" rows="10" placeholder="Write a large text here ..."><?php if(!empty($feedbacks[0]['feedback'])) {echo $feedbacks[0]['feedback'];} ?></textarea>
            <br>
            <div class="form-check form-switch">
              <input class="form-check-input" type="checkbox" name="status" value="1"
              <?php 
              if($feedbacks[0]['status']==1){
                echo 'checked';
              }
              ?>>
              <label class="form-check-label" for="flexSwitchCheckChecked"><?= lang('Global.showed') ?> ?</label>
            </div>
            <div class="text-right">
            <input type="hidden" name="id_feedback" id="id_feedback"  value="<?= $feedbacks[0]['id'] ?>">
            <input type="submit" class="btn btn-primary" value="<?= lang('Global.save') ?>">
            </div>
            <br>
          </form>
          </div>
      </div>
    </div>
  </div>
</div>
