<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0"><?= lang('Global.payment') ?></h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.payment') ?></li>
            </ol>
          </nav>
        </div>
        <!-- <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0"><?= lang('Global.payment') ?></h3>
        </div>
        <!-- Light table -->
          <div class="table-responsive">
            <table id="dtBasicExample" class="table" width="100%" >
              <thead class="thead-light">
                <tr>
                  <th class="th-sm"><?= lang('Global.order_code') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.student') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.test_package') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.payment') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.confirmation') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.action') ?>
                  </th>
                </tr>
              </thead>
              <tbody class="list">
                <?php 
                if (!empty($orders)) {
                  // dd($orders);
                  foreach ($orders as $key => $order) {
                    // d($order['member'][0]['first_name']);
                    ?>
                    <tr>
                      <td>
                        <a href="<?= base_url('order/id/'.$order['id']) ?>"><?= 'PIPETEST'.sprintf('%04d', $order['id']); ?></a>
                      </td>
                      <th scope="row">
                        <div class="media align-items-center">
                          <a href="#" class="avatar rounded-circle mr-3">
                            <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
                          </a>
                          <div class="media-body text-wrap" >
                            <span class="name mb-0"><a href="<?= base_url('account/profile/').'/'.$order['member'][0]['id'] ?>">
                            <span class="name mb-0 "><?= $order['member'][0]['first_name'].' '.$order['member'][0]['last_name']?></span>
                            </a></span>
                            <br>
                            <a href="https://api.whatsapp.com/send?phone=+628<?= substr($order['member'][0]['phone'], 1) ?>" target="_blank">
                            <span class="name mb-0"><?= $order['member'][0]['phone'] ?></span>
                            </a>
                            <br>
                            <span class="name mb-0"><?= $order['member'][0]['email'] ?></span>
                          </div>
                      </th>
                      <td>
                        <div class="media-body">
                          <?php 
                            $total_harga = 0;
                            if(!empty($order['products'])){
                              foreach ($order['products'] as $key => $product) {
                                $total_harga = $total_harga + $product['harga'];
                                ?>
                                <span><?= $product['nama'] ?></span>
                                <br>
                                <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span>
                                <br>
                                <?php
                                // dd($product);
                              }
                              ?>
                              <b><?= lang('Global.total_harga') ?> : Rp. <?= number_format($total_harga, 0, 0, '.'); ?></b>
                              <?php
                            }
                          ?>
                        </div>
                      </td>
                      <th scope="row">
                        <div class="media align-items-left">
                          <a target="_blank" href="<?= base_url('public/uploads/payment/'.$order['bukti_pembayaran']) ?>" class="avatar  mr-3">
                            <img alt="Image placeholder" src="<?php echo base_url('public/uploads/payment/'.$order['bukti_pembayaran']); ?>">
                          </a>
                          <div class="media-body" >
                              <?php 
                                if($order['status'] == 'checkout') {
                                  echo "
                                  <span class='badge badge-warning mr-4'>
                                    <span class='status'>".lang('Global.pending')."</span>
                                  </span>
                                  <p style='font-size: 12px;''>".$order['checkout']."</p>
                                  ";
                                } else if ($order['status'] == 'paid' || $order['status'] == 'paid-confirm') {
                                  echo "
                                    <span class='badge badge-success mr-4'>
                                      <span class='status'>".lang('Global.paid')."</span>
                                    </span>
                                    <p style='font-size: 12px;''>".$order['paid']."</p>
                                  ";
                                } 
                              ?>
                          </div>
                        </div>
                      </th>
                      
                      <th scope="row">
                        <div class="media align-items-center">
                          <div class="media-body" >
                              <?php 
                                if ($order['status'] == 'paid') {
                                  echo "
                                    <span class='badge badge-danger mr-4'>
                                      <span class='status'>".lang('Global.waiting_confirmation')."</span>
                                    </span>
                                    <p style='font-size: 12px;'>".$order['paid']."</p>
                                  ";
                                  ?>
                                  <a href="<?= base_url('admin/approve_payment/'.$order['id']) ?>" class="btn btn-sm btn-primary">Approve</a>
                                  <?php 
                                } else if ($order['status'] == 'paid-confirm') {
                                  echo "
                                    <span class='badge badge-success mr-4'>
                                      <span class='status'>".lang('Global.confirmed')."</span>
                                    </span>
                                    <p style='font-size: 12px;''>".$order['paid-confirm']."</p>
                                  ";
                                }
                              ?>
                          </div>
                        </div>
                      </th>
                      <td class="text-left">
                        <a href="<?= base_url('order/id/'.$order['id']) ?>" class="btn btn-sm btn-primary"><?= lang('Global.detail') ?></a>
                      </td>
                    </tr>
                    <?php
                  }
                }
                ?>
                <!-- awal baris -->
                <!-- <tr>
                  <td>
                    <a href="">EB10987</a>
                  </td>
                  <th scope="row">
                    <div class="media align-items-center">
                      <a href="#" class="avatar rounded-circle mr-3">
                        <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
                      </a>
                      <div class="media-body">
                        <span class="name mb-0">Anandita Suryaningrum</span>
                        <br>
                        <span class="name mb-0">089900998987</span>
                        <br>
                        <span class="name mb-0">anandita@email.com</span>
                      </div>
                    </div>
                  </th>
                  <td>
                    <div class="media-body">
                        <span>Paket Tes Kecerdasan</span>
                        <br>
                        <span>Rp. 120.000</span>
                      </div>
                    </div>
                  </td>
                  <th scope="row">
                    <div class="media align-items-center">
                      <div class="media-body">
                        <span class="badge badge-warning mr-4">
                          <span class="status">Pending Payment</span>
                        </span>
                        <p style="font-size: 12px;">2020-11-18 13:38:26</p>
                      </div>
                      </div>
                    </div>
                  </th>
                  <td></td>
                  <td class="text-right">
                    <div class="dropdown">
                      <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v"></i>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <a class="dropdown-item" href="#">Detail</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                    </div>
                  </td>
                </tr> -->
                <!-- akhir baris -->
              </tbody>
              <thead class="thead-light">
                <tr>
                  <th class="th-sm"><?= lang('Global.order_code') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.student') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.test_package') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.payment') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.confirmation') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.action') ?>
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        <!-- Card footer -->
        <!-- <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end mb-0">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <i class="fas fa-angle-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">
                  <i class="fas fa-angle-right"></i>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>
