<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0"><?= lang('Global.test_package') ?></h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>"><i class="fas fa-home"></i></a></li>
              <!-- <li class="breadcrumb-item"><a href="#">Peserta</a></li> -->
              <li class="breadcrumb-item active" aria-current="page"><?= lang('Global.test_package') ?></li>
            </ol>
          </nav>
        </div>
        <!-- <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0"><?= lang('Global.test_package') ?></h3>
        </div>
        <!-- Custom buttons -->
        <ul>
          <button class="dt-button buttons-excel buttons-html5 all" tabindex="0" aria-controls="dtBasicExample" type="button"><span>All</span></button>
          <button class="dt-button buttons-excel buttons-html5 ringkas" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Sedang diringkas</span></button>
          <button class="dt-button buttons-excel buttons-html5 ringkas" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Sudah diringkas</span></button>
        </ul>
        <!-- Light table -->
          <div class="table-responsive">
            <table id="dtBasicExample" class="table" width="100%">
              <thead class="thead-light">
                <tr>
                  <!-- <th class="th-sm">ID Order
                  </th> -->
                  
                  <th class="th-sm"><?= lang('Global.test_package') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.student') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.jenis_tes') ?>
                  </th>
                 <!--  <th class="th-sm"><?= lang('Global.correction') ?>
                  </th> -->
                  <th class="th-sm"><?= lang('Global.summary') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.action') ?>
                  </th>
                </tr>
              </thead>
              <tbody class="list">
                <?php 
                if (!empty($orders)) {
                  // dd($orders);
                  foreach ($orders as $key => $order) {
                    foreach ($order['products'] as $key2 => $product) {
                      ?>
                      <tr>
                      <td>
                        <div class="media-body">
                        <a href="<?= base_url('order/id/'.$order['id']) ?>"><?= 'PIPETEST'.sprintf('%04d', $order['id']); ?></a>
                          <br>
                          <?php 
                            $total_harga = 0;
                                ?>
                                <span><?= $product['nama'] ?></span>
                                <br>
                                <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span>
                                <?php
                                // dd($product);
                              ?>
                              <!-- <b>Total : Rp. <?= number_format($total_harga, 0, 0, '.'); ?></b> -->
                              <?php

                          ?>
                          <br>
                        <sm style="color: ">
                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16">
                          <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/>
                          <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/>
                          <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                        </svg>
                        <?php 
                        $stop_date = new DateTime($order['paid-confirm']);
                        $stop_date->modify('+8 day');
                        $stop_date->setTime(0,0,0);
                        $today = new DateTime("now");
                        $birthDate = $stop_date;
                        if ($birthDate < $today) { 
                          $expired = 1;
                          echo lang('Global.hasexpired')."</sm>";
                        } else {
                          $y = $today->diff($birthDate)->y;
                          $m = $today->diff($birthDate)->m;
                          $d = $today->diff($birthDate)->d;
                          $h = $today->diff($birthDate)->h;
                          $i = $today->diff($birthDate)->i;
                          $expired = 0;
                          
                          echo lang('Global.expired')."<br> ".$d." ".lang('Global.day')." ".$h."  ".lang('Global.hour')." ".$i." ".lang('Global.minute')."</sm>"; 
                        }
                        ?>
                        </div>
                      </td>
                      <th scope="row">
                        <div class="media align-items-center">
                          <a href="#" class="avatar rounded-circle mr-3">
                            <img alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
                          </a>
                          <div class="media-body text-wrap">
                              <a href="<?= base_url('account/profile/').'/'.$order['member'][0]['id'] ?>">
                            <span class="name mb-0 "><?= $order['member'][0]['first_name'].' '.$order['member'][0]['last_name']?></span>
                            </a>
                            <br>
                            <a href="https://api.whatsapp.com/send?phone=+62<?= substr($order['member'][0]['phone'], 1) ?>" target="_blank">
                            <span class="name mb-0"><?= $order['member'][0]['phone'] ?></span>
                            </a>
                            <br>
                            <span class="name mb-0 "><?= $order['member'][0]['email'] ?></span>
                          </div>
                        </div>
                      </th>
                      <th scope="row">
                        <div class="media align-items-center">
                          <div class="media-body">
                            <?php 
                             foreach ($product['jenistes'] as $key3 => $jenistes) {
                              ?>
                                <span><?= $jenistes['nama'] ?></span>
                                <br>
                                <!-- <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span> -->
                              <?php
                             if($jenistes['status']=='start-test'){
                                ?>
                                  <span class="badge badge-warning mr-4">
                                    <span class="status"><?= lang('Global.on_test') ?></span>
                                  </span>
                                  <a href="<?= base_url('order/done/'.$jenistes['id']) ?>" class="btn btn-sm btn-success"><?= lang('Global.assign_as_done') ?></a>
                                  <br>
                                  <p style="font-size: 12px;"><?= $jenistes['start-test'] ?></p>
                                <?php
                              } else if($jenistes['status']=='done-test' || $jenistes['done-test']!='0000-00-00 00:00:00'){
                                ?>
                                  <span class="badge badge-success mr-4">
                                    <span class="status"><?= lang('Global.done') ?></span>
                                  </span>
                                  <br>
                                  <p style="font-size: 12px;"><?= $jenistes['done-test'] ?></p>
                                <?php
                              } else {
                                ?>
                                  <span class="badge badge-info mr-4">
                                    <span class="status"><?= lang('Global.ready_test') ?></span>
                                  </span>
                                  <br>
                                  <p style="font-size: 12px;"><?= $order['paid-confirm'] ?></p>
                                <?php
                              }
                             }
                            ?>
                          </div>
                          </div>
                        </div>
                      </th>
                      <!-- <th scope="row">
                        <div class="media align-items-center">
                          <div class="media-body">
                            <?php 
                             foreach ($product['jenistes'] as $key3 => $jenistes) {
                              ?>
                                <span><?= $jenistes['nama'] ?></span>
                                <br>
                                <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span>
                              <?php
                             if($jenistes['status']=='start-correction'){
                                ?>
                                  <span class="badge badge-warning mr-4">
                                    <span class="status">On Correction</span>
                                  </span>
                                  <br>
                                  <p style="font-size: 12px;"><?= $jenistes['start-correction'] ?></p>
                                <?php
                              } else if($jenistes['status']=='corrected'){
                                ?>
                                  <span class="badge badge-success mr-4">
                                    <span class="status"><?= lang('Global.done') ?></span>
                                  </span>
                                  <a href="<?= base_url('psychologist/id/'.$product['psikolog_id']) ?>" class=""><?= lang('Global.psikolog').' '.$product['psikolog_first_name'].' '.$product['psikolog_last_name'] ?></a>
                                  <br>
                                  <p style="font-size: 12px;"><?= $jenistes['corrected'] ?></p>
                                <?php
                              }
                             }
                            ?>
                          </div>
                          </div>
                        </div>
                      </th> -->
                      <th scope="row">
                        <div class="media align-items-center">
                          <div class="media-body text-wrap">
                            <span><?= $product['nama'] ?></span>
                                <br>
                            <?php 
                             if($product['status']=='start-summarization'){
                                ?>
                                  <span class="badge badge-warning mr-4 text-wrap">
                                    <span class="status"><?= lang('Global.on_summarization') ?></span>
                                  </span>
                                  <br>
                                  <a href="<?= base_url('psychologist/id/'.$product['psikolog_id']) ?>" class=""><?= lang('Global.psikolog').' '.$product['psikolog_first_name'].' '.$product['psikolog_last_name'] ?></a>
                                  <br>
                                  <p style="font-size: 12px;"><?= $product['start-summarization'] ?></p>
                                <?php
                              } else if($product['status']=='summarized'){
                                ?>
                                  <span class="badge badge-success mr-4">
                                    <span class="status"><?= lang('Global.summarized') ?></span>
                                  </span>
                                  <br>
                                  <a href="<?= base_url('psychologist/id/'.$product['psikolog_id']) ?>" class=""><?= lang('Global.psikolog').' '.$product['psikolog_first_name'].' '.$product['psikolog_last_name'] ?></a>
                                  <br>
                                  <p style="font-size: 12px;"><?= $product['summarized'] ?></p>
                                  <a target="_blank" href="<?php echo base_url('pdf/mpdf/'.$order['id'].'/'.$product['id'].'/D');?>" class="btn btn-sm btn-primary"><?= lang('Global.download_result') ?></a>

                                <?php
                              }
                            ?>
                          </div>
                          </div>
                        </div>
                      </th>
                      <td class="text-left">
                        <a href="<?= base_url('order/id/'.$order['id']) ?>" class="btn btn-sm btn-primary"><?= lang('Global.detail') ?></a>
                        <?php 
                        if($jenistes['done-test']!='0000-00-00 00:00:00'){
                          ?>
                        <!-- <a href="<?= base_url('test/correction/null/'.$order['id'].'/'.$product['id']) ?>" class="btn btn-sm btn-secondary"><?= lang('Global.summary') ?></a> -->
                        <?php 
                          }
                        ?>
                      </td>
                    </tr>
                    <?php
                  }
                }
                }
                ?>
                <!-- awal baris -->
                
                <!-- akhir baris -->
              </tbody>
              <thead class="thead-light">
                <tr>
                  <!-- <th class="th-sm">ID Order
                  </th> -->
                  
                  <th class="th-sm"><?= lang('Global.test_package') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.student') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.jenis_tes') ?>
                  </th>
                 <!--  <th class="th-sm"><?= lang('Global.correction') ?>
                  </th> -->
                  <th class="th-sm"><?= lang('Global.summary') ?>
                  </th>
                  <th class="th-sm"><?= lang('Global.action') ?>
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        <!-- Card footer -->
        <!-- <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end mb-0">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <i class="fas fa-angle-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">
                  <i class="fas fa-angle-right"></i>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>
