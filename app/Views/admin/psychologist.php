<div id="page-content">

    <nav class="navbar navbar-expand navbar-light bg-light mb-4">
        <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn btn-toggle-dashboard">
                <i class="fas fa-arrows-alt-h"></i>
                <!-- <span>Toggle Sidebar</span> -->
            </button>

            <div class="" id="navbarSupportedContent">
                <ul class="nav navbar-nav nav-content ml-auto">
                    <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('auth/logout/') ?>"><i class="fas fa-sign-out-alt"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-transparent pl-2">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">Psikolog</a></li>
      </ol>
    </nav>

    <nav class="sort-bar mb-4">
        <div class="row">
            <div class="col-12 col-sm-10 mb-3 mb-sm-0">
                <!-- <button class="btn active mr-2">All</button> -->
            </div>
            <!-- <div class="col-12 col-sm-2 text-sm-right">
                <button data-toggle="tooltip" data-placement="bottom" title="Export as PDF" class="btn bg-transparent mr-2"><i class="fas fa-file-pdf"></i></button>
                <button data-toggle="tooltip" data-placement="bottom" title="Export as EXCEL" class="btn bg-transparent mr-2"><i class="fas fa-file-excel"></i></button>
                <button data-toggle="tooltip" data-placement="bottom" title="PRINT" class="btn bg-transparent"><i class="fas fa-print"></i></button>
            </div> -->
        </div>
    </nav>

    <!-- Light table -->

    <div class="table-responsive">

        <table id="dtBasicExample" class="table" width="100%" >

          <thead class="thead-light">

            <tr>

              <th class="th">Psychologist

              </th>

              <!-- <th class="th">

                <span class="badge badge-warning mr-4" style="font-size: 88%">

                  <span class="status">On Correction</span>

                </span>

              </th>

              <th class="th">

                <span class="badge badge-success mr-4" style="font-size: 88%">

                  <span class="status">Corrected</span>

                </span>

              </th> -->

              <th class="th">

                <span class="badge badge-warning mr-4" style="font-size: 88%">

                  <span class="status">On Summarization</span>

                </span>

              </th>

              <th class="th">

                <span class="badge badge-success mr-4" style="font-size: 88%">

                  <span class="status">Summarized</span>

                </span>

              </th>

              <th class="th">Status

              </th>

              <th class="th">Action

              </th>

            </tr>

          </thead>

          <tbody class="list">

            <?php 

            if (!empty($psychologists)) {

              // dd($orders);

              foreach ($psychologists as $key => $psychologist) {

              // foreach ($psychologist['orders'] as $key => $order) {

                ?>

                <tr>

                  <th scope="row">

                    <div class="media align-items-center">

                    <a href="#" class="avatar rounded-circle mr-3">

                      <img style="width:50px" src="

                      <?php 

                      if($psychologist['avatar']=="" || @file_get_contents(base_url('public/uploads/avatar/'.$psychologist['avatar']))==FALSE) { 

                          echo base_url('src/assets/img/profile/none.jpg'); 

                          } else { 

                              echo base_url('public/uploads/avatar/'.$psychologist['avatar']); 

                          } 

                      ?>" class="rounded-circle img-responsive img-circle" />

                      </a>

                      <div class="media-body text-wrap" >

                        <span class="name mb-0"><a href="<?= base_url('account/profile/').'/'.$psychologist['id'] ?>">

                        <span class="name mb-0 "><?= $psychologist['first_name'].' '.$psychologist['last_name']?></span>

                        </a></span>

                        <br>

                        <a href="https://api.whatsapp.com/send?phone=+628<?= substr($psychologist['phone'], 1) ?>" target="_blank">

                        <span class="name mb-0"><?= $psychologist['phone'] ?></span>

                        </a>

                        <br>

                        <span class="name mb-0"><?= $psychologist['email'] ?></span>

                      </div>

                  </th>

                 <!--  <td>

                    <div class="media-body">

                        <span><?= $psychologist['start_correction'] ?></span>

                    </div>

                  </td>

                  <td>

                    <div class="media-body">

                        <span><?= $psychologist['corrected'] ?></span>

                    </div>

                  </td> -->

                  <td>

                    <div class="media-body">

                        <span><?= $psychologist['start_summarization'] ?></span>

                    </div>

                  </td>

                  <td>

                    <div class="media-body">

                        <span><?= $psychologist['summarized'] ?></span>

                    </div>

                  </td>

                  <td>

                    <span class="badge badge-dot mr-4">

                      <i class="bg-<?php if($psychologist['active']=='1') {echo 'success';} else {echo 'danger';}   ?>"></i>

                      <span class="status"><?php if($psychologist['active']=='1') {echo 'Active';} else {echo 'Non active';}  ?></span>

                    </span>

                  </td>

                  <td class="text-left">

                    <a href="<?= base_url('psychologist/id/'.$psychologist['id']) ?>" class="btn btn-sm btn-primary">Detail</a>

                  </td>

                </tr>

                <?php

              }

            // }

            }

            ?>

          </tbody>

          <thead class="thead-light">

            <tr>

              <th class="th-sm">Psychologist

              </th>

           <!--    <th class="th">

                <span class="badge badge-warning mr-4" style="font-size: 88%">

                  <span class="status">On Correction</span>

                </span>

              </th>

              <th class="th">

                <span class="badge badge-success mr-4" style="font-size: 88%">

                  <span class="status">Corrected</span>

                </span>

              </th> -->

              <th class="th">

                <span class="badge badge-warning mr-4" style="font-size: 88%">

                  <span class="status">On Summarization</span>

                </span>

              </th>

              <th class="th">

                <span class="badge badge-success mr-4" style="font-size: 88%">

                  <span class="status">Summarized</span>

                </span>

              </th>

              <th class="th-sm">Status

              </th>

              <th class="th-sm">Action

              </th>

            </tr>

          </tfoot>

        </table>

      </div>


    
</div>