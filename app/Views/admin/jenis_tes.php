<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Documentation</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <!-- <li class="breadcrumb-item"><a href="#">Paket tes</a></li> -->
              <li class="breadcrumb-item active" aria-current="page">Jenis Tes</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0">Jenis tes</h3>
        </div>
        
        <!-- Card footer -->
        <div class="card-footer py-4">
          <div class="container">
            <div class="header-body text-center mb-7">
              <div class="row justify-content-center">
                <div class="col-lg-12 col-md-12">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="info">
                            <div class="icon icon-shape bg-gradient-success rounded-circle text-white">
                        <!-- <div class="icon icon-lg icon-shape icon-shape-success shadow rounded-circle"> -->
                          <i class="ni ni-atom"></i>
                        </div>
                        <h6 class="info-title text-uppercase text-primary">Tes Kecerdasan</h6>
                        <p class="description opacity-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <a href="<?php echo base_url('admin/soaljawaban/1'); ?>" class="text-primary">Lihat Soal dan Jawaban
                          <i class="ni ni-bold-right text-primary"></i>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="info">
                            <div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
                        <!-- <div class="icon icon-lg icon-shape icon-shape-warning shadow rounded-circle"> -->
                          <i class="ni ni-palette"></i>
                        </div>
                        <h6 class="info-title text-uppercase text-primary">Tes Minat Bakat  </h6>
                        <p class="description opacity-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <a href="<?php echo base_url('admin/soaljawaban/1'); ?>" class="text-primary">Lihat Soal dan Jawaban
                          <i class="ni ni-bold-right text-primary"></i>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="info">
                            <div class="icon icon-shape bg-gradient-info rounded-circle text-white">
                        <!-- <div class="icon icon-lg icon-shape icon-shape-warning shadow rounded-circle"> -->
                          <i class="ni ni-single-02"></i>
                        </div>
                        <h6 class="info-title text-uppercase text-primary">Tes Kepribadian</h6>
                        <p class="description opacity-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <a href="<?php echo base_url('admin/soaljawaban/1'); ?>" class="text-primary">Lihat Soal dan Jawaban
                          <i class="ni ni-bold-right text-primary"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>