<!-- Page Content  -->
<div id="page-content">

    <nav class="navbar navbar-expand navbar-light bg-light mb-4">
        <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn btn-toggle-dashboard">
                <i class="fas fa-arrows-alt-h"></i>
                <!-- <span>Toggle Sidebar</span> -->
            </button>

            <div class="" id="navbarSupportedContent">
                <ul class="nav navbar-nav nav-content ml-auto">
                    <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('auth/logout/') ?>"><i class="fas fa-sign-out-alt"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-transparent pl-2">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">Paket Tes</a></li>
      </ol>
    </nav>

    <nav class="sort-bar mb-4">
        <div class="row">
            <div class="ul col-12 col-sm-10 mb-3 mb-sm-0">
                <button class="dt-button btn bg-transparent mr-2 all" tabindex="0" aria-controls="dtBasicExample" type="button"><span>All</span></button>
                <!-- <button class="dt-button btn bg-transparent mr-2 membermenu" id="btn-member-active" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Active</span></button> -->
                <!-- <button class="dt-button btn bg-transparent mr-2 membermenu" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Deleted</span></button> -->
                <button class="dt-button btn bg-transparent mr-2 ringkas" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Belum dikerjakan</span></button>
                <button class="dt-button btn bg-transparent mr-2 ringkas" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Belum dikerjakan < 24 jam</span></button>
                <button class="dt-button btn bg-transparent mr-2 ringkas" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Belum dikerjakan > 24 jam</span></button>
                <button class="dt-button btn bg-transparent mr-2 ringkas" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Sedang dikerjakan</span></button>
                <button class="dt-button btn bg-transparent mr-2 ringkas" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Sedang dikerjakan < 3 hari</span></button>
                <button class="dt-button btn bg-transparent mr-2 ringkas" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Sedang dikerjakan > 3 hari</span></button>
                <button class="dt-button btn bg-transparent mr-2 ringkas" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Selesai dikerjakan</span></button>
                <button class="dt-button btn bg-transparent mr-2 analisa" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Belum dianalisa</span></button>
                <button class="dt-button btn bg-transparent mr-2 analisa" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Sedang dianalisa</span></button>
                <button class="dt-button btn bg-transparent mr-2 analisa" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Sudah dianalisa</span></button>
            <br>    
            <sm>Menampilkan semua paket tes oleh peserta dengan status active (not deleted)</sm>
                <!-- <button class="btn active mr-2">All</button>
                <button class="btn bg-transparent mr-2">Active</button>
                <button class="btn bg-transparent mr-2">Deleted</button>
                <button class="btn bg-transparent mr-2">Belum Komplit</button>
                <button class="btn bg-transparent mr-2">Komplit</button>
                <button class="btn bg-transparent mr-2">Belum Dianalisa</button>
                <button class="btn bg-transparent mr-2">Sedang Dianalisa</button>
                <button class="btn bg-transparent">Sudah Dianalisa</button> -->
            </div>
            <!-- <div class="col-12 col-sm-2 text-sm-right">
                <button data-toggle="tooltip" data-placement="bottom" title="Export as PDF" class="btn bg-transparent mr-2"><i class="fas fa-file-pdf"></i></button>
                <button data-toggle="tooltip" data-placement="bottom" title="Export as EXCEL" class="btn bg-transparent mr-2"><i class="fas fa-file-excel"></i></button>
                <button data-toggle="tooltip" data-placement="bottom" title="PRINT" class="btn bg-transparent"><i class="fas fa-print"></i></button>
            </div> -->
        </div>
        <br>
        Tgl order aktif : <input type="text" id="min" name="min">-<input type="text" id="max" name="max">
    </nav>
    
        
    
      <table id="dtBasicExample" class="table" width="100%">

        <thead class="thead-light">

          <tr>

            <!-- <th class="th-sm">ID Order</th> -->

            

            <th class="th-sm">Kode order</th>
            <th class="th-sm">Paket tes</th>
            <th class="th-sm" hidden>Harga</th>
            <th class="th-sm" hidden>Ekspirasi</th>
            <th class="th-sm">Photo</th>
            <th class="th-sm">Nama peserta</th>
            <th class="th-sm">Email</th>
            <th class="th-sm">Sekolah/Universitas</th>
            <th class="th-sm">No HP</th>
            <th class="th-sm">Status akun</th>
            <th class="th-sm">Order aktif</th>
            <th class="th-sm">Tata tertib</th>
            
            <th class="th-sm"><?= lang('Global.jenis_tes') ?></th>
            
            <!--  <th class="th-sm"><?= lang('Global.correction') ?></th> -->
            
            <th class="th-sm">Tgl request</th>
            <th class="th-sm">Block status</th>
            <th class="th-sm"><?= lang('Global.summary') ?></th>
            <th class="th-sm">Mulai analisa</th>
            <th class="th-sm">Selesai analisa</th>

            <th class="th-sm"><?= lang('Global.action') ?></th>

            <th class="th-sm" hidden>Date</th>

          </tr>

        </thead>
    

        <tbody class="list">

          <?php 

          if (!empty($orders)) {

            // dd($orders);

            foreach ($orders as $key => $order) {
              foreach ($order['products'] as $key2 => $product) {
                ?>
                <tr>
                <td><a href="<?= base_url('order/id/'.$order['id']) ?>"><?= 'PIPETEST'.sprintf('%04d', $order['id']); ?></a></td>
                <td><span><?= $product['nama'] ?></span></td>
                <td hidden><span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span></td>
                <td hidden>
                  <sm style="color: ">

                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16">

                    <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/>

                    <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/>

                    <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>

                  </svg>

                  <?php 

                  $stop_date = new DateTime($order['paid-confirm']);

                  $stop_date->modify('+8 day');

                  $stop_date->setTime(0,0,0);

                  $today = new DateTime("now");

                  $birthDate = $stop_date;

                  if ($birthDate < $today) { 

                    $expired = 1;

                    echo lang('Global.hasexpired')."</sm>";

                  } else {

                    $y = $today->diff($birthDate)->y;

                    $m = $today->diff($birthDate)->m;

                    $d = $today->diff($birthDate)->d;

                    $h = $today->diff($birthDate)->h;

                    $i = $today->diff($birthDate)->i;

                    $expired = 0;

                    

                    echo lang('Global.expired')."<br> ".$d." ".lang('Global.day')." ".$h."  ".lang('Global.hour')." ".$i." ".lang('Global.minute')."</sm>"; 

                  }

                  ?>


                </td>
                <td>
                <a href="#" class="avatar rounded-circle mr-3">

                    <img style="width:50px" src="

                    <?php 

                    if($order['member'][0]['avatar']=="" || @file_get_contents(base_url('public/uploads/avatar/'.$order['member'][0]['avatar']))==FALSE) { 

                        echo base_url('src/assets/img/profile/none.jpg'); 

                        } else { 

                            echo base_url('public/uploads/avatar/'.$order['member'][0]['avatar']); 

                        } 

                    ?>" class="rounded-circle img-responsive img-circle" />

                    </a>
                </td>
                <td>
                <a href="<?= base_url('account/profile/').'/'.$order['member'][0]['id'] ?>">
                <span class="name mb-0 "><?= $order['member'][0]['first_name'].' '.$order['member'][0]['last_name']?></span>
                </a>
                </td>
                <td><span class="name mb-0 "><?= $order['member'][0]['email'] ?></span></td>
                <td><span class="name mb-0 "><?= $order['member'][0]['company'] ?></span></td>
                <td>
                  <a href="https://api.whatsapp.com/send?phone=+628<?= substr($order['member'][0]['phone'], 1) ?>" target="_blank">
                <span class="name mb-0">0<?= $order['member'][0]['phone'] ?></span>
                </a>
                </td>
                <th scope="row">

                 
                      <?php 

                      if ($order['member'][0]['not_deleted']==1){

                        ?>

                        <span class="badge badge-success mr-4">

                          <span class="status">Active</span>
                        </span>
                        <?php
                      }
                      elseif ($order['member'][0]['not_deleted']==0){
                          ?>
                          <span class="badge badge-warning mr-4">
                            <span class="status">Deleted</span>
                          </span>
                          <?php
                      }
                      ?> 
                </th>
                <td>
                  <?= date('Y-m-d H:i:s', strtotime($order['paid-confirm'])); ?>
                </td>
                <td>
                  <span class="name mb-0 ">
                    <?php 
                      if($product['isTataTertib']==0) echo '-'; 
                      else echo  'Setuju';
                    ?>
                  </span></td>
                <th scope="row">

                  <div class="media align-items-center">

                    <div class="media-body">

                      <?php 

                      $done_all =1;
                      $none_all =1;

                       foreach ($product['jenistes'] as $key3 => $jenistes) {

                        ?>

                          <span><?= $jenistes['nama'] ?></span>

                          <br>

                          <!-- <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span> -->

                        <?php

                       if($jenistes['status']=='start-test'){
                           $none_all =0;
                           $done_all = 0;

                          ?>

                            <span class="badge badge-warning mr-4">

                              <span class="status"><?= lang('Global.on_test') ?></span>

                            </span>

                            <!-- <a href="<?= base_url('order/done/'.$jenistes['id']) ?>" class="btn btn-sm btn-success"><?= lang('Global.assign_as_done') ?></a> -->

                            <br>

                            <p style="font-size: 12px;">
                            <?php
                            if ($jenistes['start-test']!='0000-00-00 00:00:00'){
                              echo date('Y-m-d H:i:s', strtotime($jenistes['start-test'])); 
                            }
                            ?>
                            </p>

                          <?php

                        } else if($jenistes['status']=='done-test' || $jenistes['done-test']!='0000-00-00 00:00:00'){
                          $none_all =0;
                          ?>

                            <span class="badge badge-success mr-4">

                              <span class="status"><?= lang('Global.done') ?></span>

                            </span>

                            <br>

                            <p style="font-size: 12px;">
                            <?php
                            if ($jenistes['done-test']!='0000-00-00 00:00:00'){
                              echo date('Y-m-d H:i:s', strtotime($jenistes['done-test'])); 
                            }
                            ?>
                            </p>

                          <?php

                        } else {
                          

                          $done_all = 0;

                          ?>

                            <span class="badge badge-info mr-4">

                              <span class="status"><?= lang('Global.ready_test') ?></span>

                            </span>

                            <br>

                            <p style="font-size: 12px;">
                            <?php
                            if ($order['paid-confirm']!='0000-00-00 00:00:00'){
                              echo date('Y-m-d H:i:s', strtotime($order['paid-confirm'])); 
                            }
                            ?>
                            </p>

                          <?php

                        }

                       }

                       if ($done_all==1) {
                        $is_mengerjakan = 1;
                         echo '<span class="badge badge-success mr-4">

                              <span class="status">Selesai dikerjakan</span>

                            </span>';}
                        else if ($none_all==1) {
                          echo '<span class="badge badge-info mr-4">
                          <span class="status">Belum dikerjakan</span>
                        </span>';
                       
                        if(strtotime($order['paid-confirm']) > strtotime('-'.$Pakettes_expiration_belumdikerjakan.' hours')) {
                          $is_mengerjakan = 1;
                          echo "<span class='badge badge-warning mr-4'>

                          <span class='status'>Belum dikerjakan < 24 jam</span>

                        </span>"; 
                        } else {
                          $is_mengerjakan = 0;
                          echo "<span class='badge badge-danger mr-4'>

                          <span class='status'>Belum dikerjakan > 24 jam</span>

                        </span>"; 
                        }
                        
                      }
                        else  {
                          
                          echo '<span class="badge badge-warning mr-4">

                          <span class="status">Sedang dikerjakan</span>

                        </span>';
                        if(strtotime($order['paid-confirm']) > strtotime('-'.$Pakettes_expiration_belumdikerjakan.' hours')) {
                          $is_mengerjakan = 1;
                          echo "<span class='badge badge-warning mr-4'>

                          <span class='status'>Sedang dikerjakan < 24 jam</span>

                        </span>"; 
                        } else {
                          $is_mengerjakan = 0;
                          echo "<span class='badge badge-danger mr-4'>

                          <span class='status'>Sedang dikerjakan > 24 jam</span>

                        </span>"; 
                        }

                      }

                      ?>

                    </div>

                    </div>

                  </div>

                </th>
                <td>
                  <span class="name mb-0 ">
                      <?= $product['req_unblock_time'] ?>
                  </span>
                </td>
                <td>
                  <span class="name mb-0 ">
                    <?php 
                      if ($product['is_unblock']==1){
                        // echo 'Unblocked';
                        ?>
                        <a  href="<?= base_url('order/block/'.$product['id']) ?>" class="btn btn-sm btn-danger">Block</a> 
                        <?php
                      }
                      else if($product['is_unblock']==0){
                        ?> 
                        <a  href="<?= base_url('order/unblock/'.$product['id'].'/'.$order['member'][0]['email']) ?>" class="btn btn-sm btn-success">Unblock</a> 
                        <?php
                      }
                      else {
                        // echo  'Unblock';
                      } 
                    ?>
                  </span></td>

                <!-- <th scope="row">

                  <div class="media align-items-center">

                    <div class="media-body">

                      <?php 

                       foreach ($product['jenistes'] as $key3 => $jenistes) {

                        ?>

                          <span><?= $jenistes['nama'] ?></span>

                          <br>

                          <span>Rp. <?= number_format($product['harga'], 0, 0, '.'); ?></span>

                        <?php

                       if($jenistes['status']=='start-correction'){

                          ?>

                            <span class="badge badge-warning mr-4">

                              <span class="status">On Correction</span>

                            </span>

                            <br>

                            <p style="font-size: 12px;"><?= $jenistes['start-correction'] ?></p>

                          <?php

                        } else if($jenistes['status']=='corrected'){

                          ?>

                            <span class="badge badge-success mr-4">

                              <span class="status"><?= lang('Global.done') ?></span>

                            </span>

                            <a href="<?= base_url('psychologist/id/'.$product['psikolog_id']) ?>" class=""><?= lang('Global.psikolog').' '.$product['psikolog_first_name'].' '.$product['psikolog_last_name'] ?></a>

                            <br>

                            <p style="font-size: 12px;"><?= $jenistes['corrected'] ?></p>

                          <?php

                        }

                       }

                      ?>

                    </div>

                    </div>

                  </div>

                </th> -->

                <th scope="row">

                  <div class="media align-items-center">

                    <div class="media-body text-wrap">

                      <?php 

                       if($product['status']=='start-summarization'){

                          ?>

                            <span class="badge badge-warning mr-4 text-wrap">

                              <span class="status"><?= lang('Global.on_summarization') ?></span>

                            </span>

                            <br>

                            <a href="<?= base_url('psychologist/id/'.$product['psikolog_id']) ?>" class=""><?= lang('Global.psikolog').' '.$product['psikolog_first_name'].' '.$product['psikolog_last_name'] ?></a>

                            <br>

                            <p style="font-size: 12px;"><?= $product['start-summarization'] ?></p>

                          <?php

                        } else if($product['status']=='summarized'){

                          ?>

                            <span class="badge badge-success mr-4">

                              <span class="status"><?= lang('Global.summarized') ?></span>

                            </span>

                            <br>

                            <a href="<?= base_url('psychologist/id/'.$product['psikolog_id']) ?>" class=""><?= lang('Global.psikolog').' '.$product['psikolog_first_name'].' '.$product['psikolog_last_name'] ?></a>

                            <br>

                            <p style="font-size: 12px;"><?= $product['summarized'] ?></p>

                            <a style="margin-bottom:6px" target="_blank" href="<?php echo base_url('pdf/mpdf/'.$order['id'].'/'.$product['id'].'/D');?>" class="btn btn-sm btn-primary"><?= lang('Global.download_result') ?></a>

                            <br><a style="margin-bottom:6px" target="_blank" href="<?php echo base_url('pdf/mpdf/'.$order['id'].'/'.$product['id'].'/I');?>" class="btn btn-sm btn-secondary"><?= lang('Global.preview_result') ?></a>

                            <br><a style="margin-bottom:6px" target="_blank" href="<?php echo base_url('email/result/'.$order['member'][0]['email'].'/'.$order['id'].'/'.$product['id']);?>" class="btn btn-sm btn-info">Kirim email</a>



                          <?php

                        } else {

                          ?>

                            <span class="badge badge-info mr-4">

                              <span class="status">Belum dianalisa</span>

                            </span>


                          <?php

                        }

                      ?>

                    </div>

                    </div>

                  </div>

                </th>
                

                <td>
                  <?php
                  if ($product['start-summarization']!='0000-00-00 00:00:00'){
                    echo date('Y-m-d H:i:s', strtotime($product['start-summarization'])); 
                  }
                  ?>
                </td>
                <td>
                  <?php
                  if ($product['summarized']!='0000-00-00 00:00:00'){
                    echo date('Y-m-d H:i:s', strtotime($product['summarized'])); 
                  }
                  ?>
                </td>
                


                <td class="text-left">

                  <a href="<?= base_url('order/id/'.$order['id']) ?>" class="btn btn-sm btn-primary"><?= lang('Global.detail') ?></a>

                  <?php 

                  if($jenistes['done-test']!='0000-00-00 00:00:00'){

                    ?>

                  <!-- <a href="<?= base_url('test/correction/null/'.$order['id'].'/'.$product['id']) ?>" class="btn btn-sm btn-secondary"><?= lang('Global.summary') ?></a> -->

                  <?php 

                    }

                  ?>

                </td>

                <td hidden>

                        <?= $order['paid-confirm'] ?>

                    </td>

              </tr>

              <?php

            }

          }

          }

          ?>

          <!-- awal baris -->

          

          <!-- akhir baris -->

        </tbody>

        <tfoot class="thead-light">

          <tr>

          <th class="th-sm">Kode order</th>
            <th class="th-sm">Paket tes</th>
            <th class="th-sm" hidden>Harga</th>
            <th class="th-sm" hidden>Ekspirasi</th>
            <th class="th-sm">Photo</th>
            <th class="th-sm">Nama peserta</th>
            <th class="th-sm">Email</th>
            <th class="th-sm">Sekolah/Universitas</th>
            <th class="th-sm">No HP</th>
            <th class="th-sm">Status akun</th>
            <th class="th-sm">Order aktif</th>
            <th class="th-sm">Tata tertib</th>
            
            <th class="th-sm"><?= lang('Global.jenis_tes') ?></th>

           <!--  <th class="th-sm"><?= lang('Global.correction') ?></th> -->
           <th class="th-sm">Tgl request</th>
            <th class="th-sm">Block status</th>
            <th class="th-sm"><?= lang('Global.summary') ?></th>
            <th class="th-sm">Mulai analisa</th>
            <th class="th-sm">Selesai analisa</th>
            <th class="th-sm"><?= lang('Global.action') ?></th>

            <th class="th-sm" hidden>Date</th>

          </tr>

        </tfoot>

      </table>

    </div>
</div>