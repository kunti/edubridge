<div id="page-content">

    <nav class="navbar navbar-expand navbar-light bg-light mb-4">
        <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn btn-toggle-dashboard">
                <i class="fas fa-arrows-alt-h"></i>
                <!-- <span>Toggle Sidebar</span> -->
            </button>

            <div class="" id="navbarSupportedContent">
                <ul class="nav navbar-nav nav-content ml-auto">
                    <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('auth/logout/') ?>"><i class="fas fa-sign-out-alt"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-transparent pl-2">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">Peserta</a></li>
      </ol>
    </nav>
  

    <nav class="sort-bar mb-4">
        <div class="row">
            <div class="ul col-6">
                <button class="dt-button buttons-excel buttons-html5 all" tabindex="0" aria-controls="dtBasicExample" type="button"><span>All</span></button>
                <!-- <button class="dt-button buttons-excel buttons-html5 membermenu" id="btn-member-active" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Active</span></button> -->
                <!-- <button class="dt-button buttons-excel buttons-html5 membermenu" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Deleted</span></button> -->
                <button class="dt-button buttons-excel buttons-html5 memberpay" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Activated</span></button>
                <button class="dt-button buttons-excel buttons-html5 memberpay" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Unverified</span></button>
                <button class="dt-button buttons-excel buttons-html5 adminconf" tabindex="0" aria-controls="dtBasicExample" type="button"><span>Order kosong</span></button>
              
            </div>
            <!-- <div class="col-6 text-right">
                <button data-toggle="tooltip" data-placement="bottom" title="Export as PDF" class="btn bg-transparent mr-2"><i class="fas fa-file-pdf"></i></button>
                <button data-toggle="tooltip" data-placement="bottom" title="Export as EXCEL" class="btn bg-transparent mr-2"><i class="fas fa-file-excel"></i></button>
                <button data-toggle="tooltip" data-placement="bottom" title="PRINT" class="btn bg-transparent"><i class="fas fa-print"></i></button>
            </div> -->
        </div>
        <br>
        Tgl daftar : <input type="text" id="min" name="min">-<input type="text" id="max" name="max">
    </nav>


    <!-- Light table -->

    <div class="table-responsive">

        <table id="dtBasicExample" class="table" width="100%" >

          <thead class="thead-light">
            <tr>
              <th class="th-sm" hidden>Tanggal daftar</th>
              <th class="th-sm">Photo</th>
              <th class="th-sm">Nama peserta</th>
              <th class="th-sm">Email</th>
              <th class="th-sm">No HP</th>
              <th class="th-sm">Jenis kelamin</th>
              <th class="th-sm">Tgl lahir</th>
              <th class="th-sm">Sekolah/Universitas</th>
              <th class="th-sm">Pend. terakhir</th>
              <th class="th-sm">Kelas/smtr</th>
              <th class="th-sm">Status</th>
              <th class="th-sm">Tanggal daftar</th>
              <th class="th-sm">Email status</th>
              <th class="th-sm">Login terakhir</th>
              <th class="th-sm">Domisili
              </th>
              <th class="th-sm">Alamat
              </th>
              <th class="th-sm">Order
                </th>
              <th class="th-sm">Jml order
              </th>
              <th class="th-sm"><?= lang('Global.action') ?>
              </th>
            </tr>

          </thead>
          
          <tbody class="list">

            <?php 

            if (!empty($students)) {

              // dd($orders);

              foreach ($students as $key => $student) {

              // foreach ($student['orders'] as $key => $order) {

                ?>

                <tr>
                  <td hidden>
                  <?php
                  if ($student['created_on']){
                    $timezone  = +7; //(GMT -5:00) EST (U.S. & Canada)
                    echo gmdate("Y-m-d H:i:s", $student['created_on'] + 3600*($timezone+date("I")));
                  }
                  ?>
                  </td>
                  <td>
                  <a href="#" class="avatar rounded-circle mr-3">

                    <img style="width:50px" src="

                    <?php 

                    if($student['avatar']=="" || @file_get_contents(base_url('public/uploads/avatar/'.$student['avatar']))==FALSE) { 

                        echo base_url('src/assets/img/profile/none.jpg'); 

                        } else { 

                            echo base_url('public/uploads/avatar/'.$student['avatar']); 

                        } 

                    ?>" class="rounded-circle img-responsive img-circle" />

                    </a>
                  </td>
                  <td scope="row">
                    <span class="name mb-0"><a href="<?= base_url('account/profile/').'/'.$student['id'] ?>">
                    <span class="name mb-0 "><?= $student['first_name'].' '.$student['last_name']?></span>
                    </a></span>
                  </td>
                  <td scope="row">
                    <?= $student['email'] ?>
                  </td>
                  <td scope="row">
                    <a href="https://api.whatsapp.com/send?phone=+628<?= substr($student['phone'], 1) ?>" target="_blank">
                    <span class="name mb-0">0<?= $student['phone'] ?></span>
                    </a>
                  </td>
                  <td scope="row">
                    <?= $student['sex'] ?>
                  </td>
                  <td scope="row">
                    <?= $student['birthdate'] ?>
                  </td>
                  <td scope="row">
                    <?= $student['company'] ?>
                  </td>
                  <td scope="row">
                    <?= $student['education_level'] ?>
                  </td>
                  <td scope="row">
                    <?= $student['semester'] ?>
                  </td>
                  <td>
                  <?php 

                      if ($student['not_deleted']==1){

                        ?>

                        <span class="badge badge-success mr-4">

                          <span class="status">Active</span>

                        </span>

                        <?php

                      }

                      elseif ($student['not_deleted']==0){

                          ?>

                          <span class="badge badge-warning mr-4">

                            <span class="status">Deleted</span>

                          </span>

                          <?php

                      }

                      // if($student['volunteer'] == '1') {

                      //       echo "

                      //       <span class='badge badge-info mr-4'>

                      //         <span class='status'>Volunteer</span>

                      //       </span>

                      //       ";

                      //     }

                          ?>
                  </td>
                  
                  <td>
                  <?php
                  if ($student['created_on']){
                    $timezone  = +7; //(GMT -5:00) EST (U.S. & Canada)
                    echo gmdate("Y-m-d H:i:s", $student['created_on'] + 3600*($timezone+date("I")));
                  }
                  ?>
                  </td>
                  <td>
                  <?php 
                      if ($student['active']==1){
                        ?>
                        <span class="badge badge-success mr-4">
                          <span class="status">Activated</span>
                        </span>
                        <?php
                      }
                      elseif ($student['active']==0){

                          ?>

                          <span class="badge badge-warning mr-4">

                            <span class="status">Unverified</span>
                            
                          </span>
                          <?php

                      }
                          ?>
                  </td>
                  <td>
                  <?php
                  if ($student['last_login']){
                    $timezone  = +7; //(GMT -5:00) EST (U.S. & Canada)
                    echo gmdate("Y-m-d H:i:s", $student['last_login'] + 3600*($timezone+date("I")));
                  }
                  ?>
                  </td>
                  <td class="text-wrap">
                    <?= $student['domisili'] ?>
                  </td>
                  <td class="text-wrap">
                    <?= $student['address'] ?>
                  </td>
                  
                  
                  <td>
                      
                      <?php
                      if (count($student['orders'])==0){
                        ?>
                          <span class="badge badge-warning mr-4">

                          <span class="status">Order kosong</span>

                          </span>
                        <?php
                      } 
                      foreach ($student['orders'] as $key2 => $order) {
                      
                        echo "<a href='".base_url('order/id/'.$order['id'])."'>";

                        echo 'PIPETEST'.sprintf('%04d', $order['id']);

                        echo "</a><br>";

                      } 

                      ?>

                  </td>
                  <td>
                    <?= count($student['orders']) ?>
                  </td>

                  <td class="text-left">

                    <a style="margin-bottom: 5px;"  target="_blank" href="<?= base_url('account/profile/'.$student['id']) ?>" class="btn btn-sm btn-primary"><?= lang('Global.detail') ?></a>
                    <br>
                    <a style="margin-bottom: 5px;"  href="<?= base_url('auth/activate/'.$student['id']) ?>" class="btn btn-sm btn-secondary">Aktivasi</a>
                    <br>
                    <a href="<?= base_url('auth/admin_send_email_activation/'.$student['email']) ?>" class="btn btn-sm btn-secondary"><?= lang('Global.resend_activation') ?></a> 
                  </td>

                </tr>

                <?php

              }

            // }

            }

            ?>

          </tbody>

          <tfoot class="tfoot thead-light">
            <tr>
              <th class="th-sm" hidden>Tanggal daftar</th>
              <th class="th-sm">Photo</th>
              <th class="th-sm">Nama peserta</th>
              <th class="th-sm">Email</th>
              <th class="th-sm">No HP</th>
              <th class="th-sm">Jenis kelamin</th>
              
              <th class="th-sm">Tgl lahir</th>
              <th class="th-sm">Sekolah/Universitas</th>
              <th class="th-sm">Pend. terakhir</th>
              <th class="th-sm">Kelas/smtr</th>
              <th class="th-sm">Status</th>
              <th class="th-sm">Tanggal daftar</th>
              <th class="th-sm">Email Status</th>
              <th class="th-sm">Login terakhir</th>
              <th class="th-sm">Domisili
              </th>
              <th class="th-sm">Alamat
              </th>
              <th class="th-sm">Order
                </th>
              <th class="th-sm">Jml order
              </th>
              <th class="th-sm"><?= lang('Global.action') ?>
              </th>
            </tr>

          </tfoot>

        </table>

      </div>
</div>