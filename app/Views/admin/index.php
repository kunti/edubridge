<!doctype html>
<html lang="en" author="Agun Prabowo">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 	
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('src/assets/admin/style-index.css'); ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

   <!-- Datatable -->
  <!-- <link rel="stylesheet" href="<?php echo base_url('src/node_modules/mdbootstrap/css/addons/datatables2.min.css'); ?>" type="text/css"> -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" type="text/css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css" type="text/css">
  <!-- tanggal -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" type="text/css">
  <link rel="stylesheet" href="https://cdn.datatables.net/datetime/1.1.1/css/dataTables.dateTime.min.css" type="text/css">
  
    <title>PIPE PSIKOTES | DASHBOARD ADMIN</title>
  </head>
 <body>
 <style>
tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
*, *::before, *::after {
    box-sizing: border-box;
}

.nav-link[data-toggle].collapsed:after {
    content: " ▾";
}
.nav-link[data-toggle]:not(.collapsed):after {
    content: " ▴";
}
  </style>

  <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <img src="<?php echo base_url('src/assets/admin/img/pipe-yellow.png'); ?>" class="img-fluid mt-3 pt-1">
            </div>

            <div class="avatar text-center">
                <img src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>" class="rounded-circle" width="100">
                <h5 class="mt-4 mb-1 text-uppercase">ADMIN PIPE</h5>
            </div>
            <!-- <div class="collapse show list-unstyled components text-center mt-2" id="sidebar"> -->
                <ul class="list-unstyled components text-center mt-2">
                <li class="<?php if ($scope=='index') echo 'active'; ?> mb-2">
                    <a target="_blank" href="<?php echo base_url('dashboard'); ?>"><i class="fas fa-chart-line mr-2"></i>Dashboard</a>
                </li>
                <li class="<?php if ($scope=='online') echo 'active'; ?> mb-2">
                    <a target="_blank" href="<?php echo base_url('test/online'); ?>"><i class="fas fa-dot-circle mr-2"></i>Online tes</a>
                </li>
                <!-- <li class="<?php if ($scope=='student') echo 'active'; ?> mb-2">
                    <a target="_blank" href="<?php echo base_url('admin/student'); ?>"><i class="fas fa-users mr-2"></i>Peserta</a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link collapsed text-truncate " href="#submenu1" data-toggle="collapse" data-target="#submenu1"><i class="fas fa-cubes mr-2"></i>Peserta</a>
                    <div class=" <?php if (substr($scope, 0, 7)=='student') echo 'show'; else echo 'collapse' ?>" id="submenu1" aria-expanded="false">
                        <ul class="flex-column pl-2 nav">
                            <li class="nav-item <?php if ($scope=='student') echo 'active'; ?>"><a target="_blank" class="nav-link py-0 " href="<?php echo base_url('admin/student'); ?>"><span>All</span></a></li>
                            <li class="nav-item <?php if ($scope=='student_activated') echo 'active'; ?>"><a target="_blank" class="nav-link py-0 " href="<?php echo base_url('admin/student/activated'); ?>"><span>Activated</span></a></li>
                            <li class="nav-item <?php if ($scope=='student_unverified') echo 'active'; ?>"><a target="_blank" class="nav-link py-0 " href="<?php echo base_url('admin/student/unverified'); ?>"><span>Unverified</span></a></li>
                        </ul>
                    </div>
                </li>
                <!-- <li class="<?php if ($scope=='test') echo 'active'; ?> mb-2">
                    <a target="_blank" href="<?php echo base_url('test/paket_tes'); ?>"><i class="fas fa-cubes mr-2"></i>Paket Tes</a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link collapsed text-truncate " href="#submenu2" data-toggle="collapse" data-target="#submenu2"><i class="fas fa-cubes mr-2"></i>Paket Tes</a>
                    <div class=" <?php if (substr($scope, 0, 4)=='test') echo 'show'; else echo 'collapse' ?>" id="submenu2" aria-expanded="false">
                        <ul class="flex-column pl-2 nav">
                            <li class="nav-item <?php if ($scope=='test') echo 'active'; ?>"><a target="_blank" class="nav-link py-0 " href="<?php echo base_url('test/paket_tes'); ?>"><span>All</span></a></li>
                            <li class="nav-item <?php if ($scope=='test_belumd') echo 'active'; ?>"><a target="_blank" class="nav-link py-0 " href="<?php echo base_url('test/paket_tes/belumdikerjakan'); ?>"><span>Belum dikerjakan</span></a></li>
                            <li class="nav-item <?php if ($scope=='test_sedangd') echo 'active'; ?>"><a target="_blank" class="nav-link py-0 " href="<?php echo base_url('test/paket_tes/sedangdikerjakan'); ?>"><span>Sedang dikerjakan</span></a></li>
                            <li class="nav-item <?php if ($scope=='test_selesaid') echo 'active'; ?>"><a target="_blank" class="nav-link py-0 " href="<?php echo base_url('test/paket_tes/selesaidikerjakan'); ?>"><span>Selesai dikerjakan</span></a></li>
                            <li class="nav-item <?php if ($scope=='test_beluma') echo 'active'; ?>"><a target="_blank" class="nav-link py-0 " href="<?php echo base_url('test/paket_tes/belumdianalisa'); ?>"><span>Belum dianalisa</span></a></li>
                            <li class="nav-item <?php if ($scope=='test_sedanga') echo 'active'; ?>"><a target="_blank" class="nav-link py-0 " href="<?php echo base_url('test/paket_tes/sedangdianalisa'); ?>"><span>Sedang dianalisa</span></a></li>
                            <li class="nav-item <?php if ($scope=='test_selesaia') echo 'active'; ?>"><a target="_blank" class="nav-link py-0 " href="<?php echo base_url('test/paket_tes/selesaidianalisa'); ?>"><span>Selesai dianalisa</span></a></li>
                        </ul>
                    </div>
                </li>
                <li class="<?php if ($scope=='req_activation') echo 'active'; ?> mb-2">
                    <a target="_blank" href="<?php echo base_url('test/req_activation'); ?>"><i class="fas fa-key mr-2"></i>Req Aktivasi</a>
                </li>
                <li class="<?php if ($scope=='all') echo 'active'; ?> mb-2">
                    <a target="_blank" href="<?php echo base_url('order/all'); ?>"><i class="fas fa-shopping-cart mr-2"></i>Pemesanan</a>
                </li>
                <li class="<?php if ($scope=='feedback') echo 'active'; ?> mb-2">
                    <a target="_blank" href="<?php echo base_url('feedback'); ?>"><i class="fas fa-comments mr-2"></i>Feedback</a>
                </li>
                <li class="<?php if ($scope=='psychologist') echo 'active'; ?> mb-2">
                    <a target="_blank" href="<?php echo base_url('psychologist/all'); ?>"><i class="far fa-id-badge mr-2"></i>Psikolog</a>
                </li>
                
                </ul>
            <!-- </div> -->
           
            <ul class="list-unstyled components text-center mt-2">
                <li class="<?php if ($scope=='student_deleted') echo 'active'; ?> mb-2">
                    <a target="_blank" href="<?php echo base_url('admin/student/null/deleted'); ?>"><i class="fas fa-users mr-2"></i>Peserta</a>
                </li>
                <li class="<?php if ($scope=='test_deleted') echo 'active'; ?> mb-2">
                    <a target="_blank" href="<?php echo base_url('test/paket_tes/null/deleted'); ?>"><i class="fas fa-cubes mr-2"></i>Paket Tes</a>
                </li>
                <li class="<?php if ($scope=='all_deleted') echo 'active'; ?> mb-2">
                    <a target="_blank" href="<?php echo base_url('order/all/null/deleted'); ?>"><i class="fas fa-shopping-cart mr-2"></i>Pemesanan</a>
                </li>
                <!-- <li>
                    <a target="_blank" href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pages</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                            <a target="_blank" href="#">Page 1</a>
                        </li>
                        <li>
                            <a target="_blank" href="#">Page 2</a>
                        </li>
                        <li>
                            <a target="_blank" href="#">Page 3</a>
                        </li>
                    </ul>
                </li> -->
            </ul>
             <ul class="list-unstyled CTAs">
                <!-- <li>
                    <a target="_blank" href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>
                </li> -->
                <li>
                    <a target="_blank" href="<?= base_url('') ?>" class="article"><i class="fas fa-arrow-left mr-1"></i> Kembali</a>
                </li>
            </ul>
        </nav>
        <!-- page content -->
        <?php 
            if (isset($template)) echo view($template); 
        ?>
    <!-- </div> -->

<!-- Optional JavaScript -->
<!-- Vue js -->
<script src="https://vuejs.org/js/vue.min.js"></script>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="<?php echo base_url('src/assets/vendor/jquery/dist/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/js-cookie/js.cookie.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js'); ?>"></script>
  <!-- Optional JS -->
  <script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.extension.js'); ?>"></script>
  <!-- Argon JS -->
  <script src="<?php echo base_url('src/assets/js/argon.js?v=1.2.0'); ?>"></script>
  <!-- Datatable -->
  <script src="<?php echo base_url('src/node_modules/mdbootstrap/js/addons/datatables2.min.js'); ?>" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/searchbuilder/1.0.1/js/dataTables.searchBuilder.min.js" type="text/javascript"></script>
  <!-- tanggal -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/datetime/1.1.1/js/dataTables.dateTime.min.js" type="text/javascript"></script>
  
  
  <script type="text/javascript">
//   window.onload = function(){
//     document.getElementById('btn-member-active').click();
//     document.getElementById('btn-member-active').focus(); 
//   }
  var minDate, maxDate;
    // Custom filtering function which will search data in column four between two values
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            if (minDate.val()!=null || maxDate.val()!=null){
                var min = new Date(minDate.val());
                min.setHours(0,0,0,0);
                // var max = new Date(maxDate.val());
                if (maxDate.val()==null){
                    var max = new Date(2100, 12, 31, 10, 33, 30, 0);
                    max.setHours(23,59,59,999);    
                } else {
                    var max = new Date(maxDate.val());
                    max.setHours(23,59,59,999);
                }
                console.log(max);
                    var date = new Date( data[0] );
                if (
                    ( min === null && max === null ) ||

                    ( min === null && date <= max ) ||

                    ( min <= date   && max === null ) ||

                    ( min <= date   && date <= max )
                ) {
                    return true;
                }
                return false;
            } else {
                return true;
            }
            

        }
    );

    
  $(document).ready(function () {
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
            $(this).toggleClass('active');
        });
    });


    $('#dtBasicExample tfoot tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#dtBasicExample .thead');

        minDate = new DateTime($('#min'), {

        format: 'YYYY-MM-DD'

        });

        maxDate = new DateTime($('#max'), {

        format: 'YYYY-MM-DD'

        });



    // $('#dtBasicExample tfoot th').each( function () {
    //     var title = $(this).text();
    //     $(this).html( '<input type="text" placeholder="'+title+'" />' );
    // } );


  var table = $('#dtBasicExample').DataTable({
    //   filter tiap kolom
    // initComplete: function () {
    //     var api = this.api();
 
    //         // For each column
    //         api
    //             .columns()
    //             .eq(0)
    //             .each(function (colIdx) {
    //                 // Set the header cell to contain the input element
    //                 var cell = $('.filters th').eq(
    //                     $(api.column(colIdx).header()).index()
    //                 );
    //                 var title = $(cell).text();
    //                 $(cell).html('<input type="text" placeholder="' + title + '" />');
 
    //                 // On every keypress in this input
    //                 $(
    //                     'input',
    //                     $('.filters th').eq($(api.column(colIdx).header()).index())
    //                 )
    //                     .off('keyup change')
    //                     .on('keyup change', function (e) {
    //                         e.stopPropagation();
 
    //                         // Get the search value
    //                         $(this).attr('title', $(this).val());
    //                         var regexr = '({search})'; //$(this).parents('th').find('select').val();
 
    //                         var cursorPosition = this.selectionStart;
    //                         // Search the column for that value
    //                         api
    //                             .column(colIdx)
    //                             .search(
    //                                 this.value != ''
    //                                     ? regexr.replace('{search}', '(((' + this.value + ')))')
    //                                     : '',
    //                                 this.value != '',
    //                                 this.value == ''
    //                             )
    //                             .draw();
 
    //                         $(this)
    //                             .focus()[0]
    //                             .setSelectionRange(cursorPosition, cursorPosition);
    //                     });
    //             });
    //         // Apply the search
            
    //         this.api().columns().every( function () {
    //             var that = this;
 
    //             $( 'input', this.footer() ).on( 'keyup change clear', function () {
    //                 if ( that.search() !== this.value ) {
    //                     that
    //                         .search( this.value )
    //                         .draw();
    //                 }
    //             } );
    //         } );
    //     },
    "order": [],
    select: true,
    dom: 'Blfrtip',
    buttons: [
        { extend: 'excel', text: '<i class="fas fa-file-excel"></i>' },
        { extend: 'pdf', text: '<i class="fas fa-file-pdf"></i>' },
        { extend: 'print', text: '<i class="fas fa-print"></i>' },
      // 'copy', 'csv', 
    ],
    searchBuilder: {
            columns: [0,1,2,3]
        }
    });


    $('#min, #max').on('change', function () {
        table.draw();
    });


    $('.ul').on('click', '.all', function() {
        table
        .search('')
        .columns()
        .search('')
        .draw();
    });
    $('.ul').on( 'click', '.membermenu', function () {
    table
        .columns(10)
        .search($(this).text())
        .draw();
        // $(this).addClass('active');
    });
    $('.ul').on( 'click', '.memberpay', function () {
    table
        .columns(12)
        .search($(this).text(), true, false)
        .draw();
    });
    $('.ul').on( 'click', '.ringkas', function () {
    table
        .columns(13)
        .search($(this).text(), true, false)
        .draw();
    });
    $('.ul').on( 'click', '.analisa', function () {
    table
        .columns(15)
        .search($(this).text())
        .draw();
    });
    $('.ul').on( 'click', '.adminconf', function () {
    table
        .columns(15)
        .search($(this).text())
        .draw();
    });

  $('.dataTables_length').addClass('bs-select');
});
</script>



  </body>
</html>