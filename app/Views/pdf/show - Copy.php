<style type="text/css">
  .table th, .table td {
    padding: 0.25rem;
        padding-top: 0.25rem;
        padding-right: 0.25rem;
        padding-bottom: 0.25rem;
        padding-left: 0.25rem;
}
</style>
<!-- <div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">My Correction <br>& Summarization</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="#">Product List</a></li>
              <li class="breadcrumb-item active" aria-current="page">Product Detail</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-neutral">CONFIDENTIAL</a>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <h3 class="mb-0">Detail Paket Tes</h3>
        </div>
        <div class="card-header border-0">
          <h3><?= $orders[0]['products'][0]['nama'] ?></h3>
          <span class="text-sm"><?= $orders[0]['products'][0]['desc'] ?></span>
          <br>
          <sm><a href="">Order code : <?= $orders[0]['order_code'].' - '.$order['id'] ?></a></sm>
          <br>
          Status : 
          <?php 
            if($orders[0]['products'][0]['start-summarization']=='0000-00-00 00:00:00'){
              echo "Belum ada summary";
            } else if ($orders[0]['products'][0]['status']=='start-summarization'){
              echo "Sedang summary, menunggu hasil";
            } else if ($orders[0]['products'][0]['status']=='summarized'){
              ?>
              <a href="<?php echo base_url('pdf/mpdf/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id'].'/D');?>" class="btn btn-sm btn-success">Download hasil</a>
              <?php
            } else {

            }
          ?>
        </div>
        <div class="container">
            <!-- awal row -->
  <?php 
    foreach ($orders[0]['products'][0]['jenistes'] as $key => $tes) {
      $no = $key+1;
      ?>
            <div class="card" style="width: 100%;">
              <div class="card-body">
                <h3 class="card-title"><?= $no.'. '.$tes['nama'] ?></h3>
                <!-- <h6 class="card-subtitle mb-4 text-muted">Card subtitle</h6> -->
                <p class="small"><?= $tes['desc'] ?></p>
                <p>Test at <b><?= $tes['start-test'] ?></b> and done at <b><?= $tes['done-test'] ?></b><a href="#" class="btn-sm btn-secondary">Lihat detail</a></p>
                <div class="container">
                  <div class="row">
                    <div class="col-xl-6">
                      <!-- <span>Hasil Skor</span> -->
                      <div class="table-responsive">
                        <table id="" class="table sempit" width="100%" >
                          <thead class="thead-light">
                            <tr>
                              <th class="th-sm">Bagian
                              </th>
                              <th class="th-sm">Code
                              </th>
                              <th class="th-sm">Skor
                              </th>
                              <th class="th-sm">SW
                              </th>
                              <th class="th-sm">Klasi<br>fikasi
                              </th>
                            </tr>
                          </thead>
                          <tbody class="list">

                            <?php 
                            $total_skor = 0;
                            $total_skor_sw = 0;
                            $total_skor_klasifikasi = 0;
                            if (!empty($jenistes[$key]['bagiantes'])) {
                              foreach ($jenistes[$key]['bagiantes'] as $bagian) {
                                ?>
                            <tr>
                                <td>
                                  <?php if(!empty($bagian['judul'])) {echo $bagian['judul']; } ?>
                                </td>
                                <td>
                                  <?php if(!empty($bagian['konversi'][0]['code'])) {echo $bagian['konversi'][0]['code']; } else { echo ''; } ?>
                                </td>
                                <td>
                                  <?php if(!empty($bagian['skor'][0]['score'])) {echo $bagian['skor'][0]['score']; $skor = $bagian['skor'][0]['score'];  } else { echo '0'; $skor=0;} ?>
                                </td>
                                <td>
                                  <?php if(!empty($bagian['konversi'][0]['sw'])) {echo $bagian['konversi'][0]['sw']; $skor_sw = $bagian['konversi'][0]['sw'];  } else { echo '0'; $skor_sw=0;} ?>
                                </td>
                                <td>
                                  <?php if(!empty($bagian['konversi'][0]['sw'])) {echo $bagian['konversi'][0]['klasifikasi']; $skor_klasifikasi = $bagian['konversi'][0]['klasifikasi'];  } else { echo '0'; $skor_klasifikasi=0;} ?>
                                </td>
                            </tr>
                                <?php
                                $total_skor = $total_skor + $skor;
                                $total_skor_sw = $total_skor_sw + $skor_sw;
                                // $total_skor_klasifikasi = $total_skor_klasifikasi + $skor_klasifikasi;
                              }
                            }
                            ?>
                          </tbody>
                          <tfoot>
                            <th>Total</th>
                            <th><?= $total_skor ?></th>
                            <th><?= $total_skor_sw ?></th>
                            <th><?= $total_skor_klasifikasi ?></th>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                    <div class="col-xl-6">
                      <!-- <span>Grafik</span> -->
                      <div class="container">
                        <div class="row">
                          <div class="col">
                            <div class="card bg-defalut">
                                <div class="row align-items-center">
                                </div>
                              <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta">
                                <!-- Chart -->
                                <div class="chart">
                                  <!-- Chart wrapper -->
                                  <canvas id="chart-sw" class="chart-canvas"></canvas>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="container">
                  <h3>Penjelasan : </h3>
                  <!-- <span>Summary</span> -->
                    <!-- <textarea id="summernote<?= $key; ?>"> -->
                    <?= $tes['summary']; ?>
                   <!-- </textarea> -->
                   <br>
                    <form id="save_summary<?= $key; ?>">
                      <input type="hidden" name="id_memberorderproductjenistes<?= $key; ?>" id="id_memberorderproductjenistes<?= $key; ?>" value="<?= 
                      $tes['id'];?>">
                      <!-- <input type="submit" class="btn btn-primary" value="Save Changes"> -->
                    </form>
                </div>
              </div>
            </div>
          </div>
    <?php
        }
      ?>
        <!-- akhir row -->
        </div>
        <!-- final summary -->
          <div class="card" style="width: 100%;">
            <div class="card-body">
                <h3 class="card-title">Final Summary</h3>
                 <!-- <textarea id="finalsummernote"> -->
                  <?= $orders[0]['products'][0]['summary']; ?>
                 <!-- </textarea> -->
                 <br>
              <form id="save_finalsummary">
                <input type="hidden" name="id_memberorderproduct" id="id_memberorderproduct" value="<?= 
                $orders[0]['products'][0]['id']; ?>">
                <!-- <input type="submit" class="btn btn-primary" value="Save Changes"> -->
              </form>
              <div class=" text-center">
                  <span>Jakarta, 24 Februari 2021</span>
                  <p>Psychologist</p>
                  <img alt="" style="height: 75px" src="<?php echo base_url('public/uploads/ttd/yana.jpg'); ?>">
                  <br>
                  <span><b><u>Yana., Psi</u></b></span>
                  <p>SIPP. No. 0183.3038.2838</p>
              </div>
              <div class="text-right">
                <p class="text-sm">*psikogram ini berlaku hanya jika ditanda tangani psikolog</p>
              </div>
          </div>
        </div>
        <div class="card" style="width: 100%;">
            <div class="card-body">
                <h3 class="card-title">Appendix</h3>
                 <!-- <textarea id="finalsummernote"> -->
                  <?= $orders[0]['products'][0]['summary']; ?>
                 <!-- </textarea> -->
                 <br>
              <div class=" text-center">
                      <div class="table-responsive">
                        <table id="" class="table sempit" width="100%" >
                          <thead class="thead-light">
                            <tr>
                              <th class="th-sm">Fakultas / Faculty
                              </th>
                              <th class="th-sm">Internasional
                              </th>
                              <th class="th-sm">Indonesia
                              </th>
                            </tr>
                          </thead>
                          <tbody class="list text-left">
                            <?php
                            for ($i=0; $i < 5; $i++) { 
                               ?>
                            <tr>
                                <td>
                                  <b>Ekonomi</b>
                                  Economy
                                </td>
                                <td>
                                  <span class="text-wrap">Accounting, Business Administration, Small Business Operations, Entrepreneurial and Finance, Human Resources Management, Insurance, International Business, Marketing, Taxation, Business Operations Support, Business/Commerce, International Economics, Applied Economics,Business/Managerial Economics</span>
                                </td>
                                <td>
                                  <span class="text-wrap">
                                  Akuntansi, Akuntansi Pajak, Manajemen,Manajemen Keuangan, Manajemen Bisnis, Manajemen Logistik,Manajemen Perbankan,  Manajemen Pemasaran,Bisnis Internasional, Administrasi Fiskal (Perpajakan), Administrasi KeuangandanPerbankan, Bisnis Digital,Branding,IlmuEkonomi, Ekonomi Syariah
                                  </span>
                                </td>
                            </tr>
                            <?php
                             } 
                            ?>
                          </tbody>
                        </table>
                      </div>
              </div>
          </div>
        </div>
        <!-- akhir final summary -->
  </div>
</div>

