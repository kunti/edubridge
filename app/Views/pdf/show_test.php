<html>

<head>
  <title>EduBridge - Test Result</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
    @page {
      margin: 200px 0in 1in 0in;
    }
    .flyleaf {
    page-break-after: always;
    padding: 10px 50px;
  }

    body {
      padding: 0;
      margin: 0;
      font-family: Open Sans, sans-serif;
    }

    header {
      position: fixed;
      top: -200px;
      left: 0;
      right: 0;
      height: 100px;
      padding: 0px 0px;
      background-color: #9C020E;
      z-index: 1000;
    }

    .text-center {
      text-align: center;
    }

    main {
      margin-top: 100px;
      padding: 10px 50px;
    }

    .page-header {
      margin-top: 5px;
      padding: 5px;
      background-color: aqua;
    }

    .page-header h2 {
      font-family: monospace;
      font-size: 20px;
      text-align: center;
    }

    footer {
      position: fixed;
      bottom: 0;
      left: 0;
      right: 0;
      background-color: #9C020E;
      height: 10px;
      /*border-top: 1px solid #1f1f1f;*/
      z-index: 1000;
    }

    footer h3 {
      padding-left: 50px;
    }

    .table
    {
        width: 100%;
        margin-bottom: 1rem;

        color: #212529;
    }
    .table th,
    .table td
    {
        padding: .75rem;

        vertical-align: top;

        border-top: 1px solid #dee2e6;
    }
    .table thead th
    {
        vertical-align: bottom;

        border-bottom: 2px solid #dee2e6;
    }
    .table tbody + tbody
    {
        border-top: 2px solid #dee2e6;
    }

    .table-sm th,
    .table-sm td
    {
        padding: .3rem;
    }

    .table-bordered
    {
        border: 1px solid #dee2e6;
    }
    .table-bordered th,
    .table-bordered td
    {
        border: 1px solid #dee2e6;
    }
    .table-bordered thead th,
    .table-bordered thead td
    {
        border-bottom-width: 2px;
    }
  </style>
</head>

<body>
  <?php
    $eduBridge = base64_encode(file_get_contents(base_url('src/assets/img/brand/white.png'))); 
    $eduBridge_bold = base64_encode(file_get_contents(base_url('src/assets/img/brand/blue.png'))); 

  
  ?>
  <div class="flyleaf">
    Cover page
    <div class="row">
      <div  class="text-center">
      <img style="height: 35px" src="data:image/gif;base64,<?= $eduBridge_bold ?>" />
      </div>
      <div style="padding-top: 5px; text-align: right;" class="col-xl-8">
        <p style="font-size: 12px; color: #fff"><b>Header example</b><br>
      </div>
    </div>
  </div>
<?php
    $header = base64_encode(file_get_contents(base_url('src/assets/img/pdf/header.jpg'))); 
    $logo = base64_encode(file_get_contents(base_url('src/assets/img/pdf/edubridge.png'))); 
?>
<header>
    <div class="row">
      <!-- <div style="padding-top: 5px" class="col-xl-4"> -->
      <img style="width:  30%" src="data:image/gif;base64,<?= $logo ?>" />
      <img style="width:  100%" src="data:image/gif;base64,<?= $header ?>" />
      <!-- </div> -->
      <div style="padding-top: 5px; text-align: right;" class="col-xl-8">
        <p style="font-size: 12px; color: #fff"><b>Header example</b><br>
      </div>
    </div>
  </header>
  <footer>
    <h3>Footer example</h3>
  </footer>

  <main>
    <div class="text-left row">
      <h4 class="mb-0">Member identity</h4>
      <hr>
      <div class="col">
      </div>
     
    </div>
    <div>
     
    </div>
    <div class="" style="page-break-before: always;">
         
        <div class="text-right">
          <p class="text-sm">*psikogram ini berlaku hanya jika ditanda tangani psikolog</p>
          <p class="text-sm">Hasil tes ini selanjutnya dapat Anda konsultasikan dengan psikolog kami. Anda dapat menghubungi psikolog kami</p>
        </div>
    </div>
    <div class=" text-center">
      <h4 class="card-title">Appendix</h4>
      <div class="table-responsive">
        <table id="sempit" class="table sempit" width="100%" >
          <thead class="thead-light">
            <tr>
              <th class="th">Fakultas / Faculty
              </th>
              <th class="th-sm">Internasional
              </th>
              <th class="th-sm">Indonesia
              </th>
            </tr>
          </thead>
          <tbody class="list text-left">
            <?php
            for ($i=0; $i < 5; $i++) { 
               ?>
            <tr>
                <td>
                    <b>Ekonomi</b>
                    <br>
                    Economy
                </td>
                <td>
                  <span class="text-wrap">Accounting, Business Administration, Small Business Operations, Entrepreneurial and Finance, Human Resources Management, Insurance, International Business, Marketing, Taxation, Business Operations Support, Business/Commerce, International Economics, Applied Economics,Business/Managerial Economics</span>
                </td>
                <td>
                  <span class="text-wrap">
                  Akuntansi, Akuntansi Pajak, Manajemen,Manajemen Keuangan, Manajemen Bisnis, Manajemen Logistik,Manajemen Perbankan,  Manajemen Pemasaran,Bisnis Internasional, Administrasi Fiskal (Perpajakan), Administrasi KeuangandanPerbankan, Bisnis Digital,Branding,IlmuEkonomi, Ekonomi Syariah
                  </span>
                </td>
            </tr>
            <?php
             } 
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </main>
</body>

</html>
  