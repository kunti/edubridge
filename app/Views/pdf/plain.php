<style>
  body {
    font-size: 12px;
    line-height: 1.3rem;
  }
  header {
      position: fixed;
      top: -80px;
      left: 0;
      right: 0;
      height: 80px;
      padding: 0px 0px;
      /*background-color: #9C020E;*/
      z-index: 1000;
    }

    .text-center {
      text-align: center;
    }

    main {
      margin-top: 0px;
      padding: 10px 50px;
    }

    content {
      padding: 0px 50px;
    }

    footer {
      position: fixed;
      bottom: 0;
      left: 0;
      right: 0;
      background-color: #232941;
      height: 5px;
      /*border-top: 1px solid #1f1f1f;*/
      z-index: 1000;
    }

    footer h3 {
      background-color: #232941;
      padding-left: 100px;
    }

    .table
    {
        width: 100%;
        margin-bottom: 1rem;

        color: #212529;
    }
    .table th,
    .table td
    {
        padding: .75rem;

        vertical-align: top;

        border-top: 1px solid #dee2e6;
    }
    .table thead th
    {
        vertical-align: bottom;

        border-bottom: 2px solid #dee2e6;
    }
    .table tbody + tbody
    {
        border-top: 2px solid #dee2e6;
    }

    .table-sm th,
    .table-sm td
    {
        padding: .3rem;
    }

    .table-bordered
    {
        border: 1px solid #ffca2e;
    }
    .table-bordered th,
    .table-bordered td
    {
        border: 1px solid #ffca2e;
    }
    .table-bordered thead th,
    .table-bordered thead td
    {
        border-bottom-width: 2px;
    }
    .parent {
      position: relative;
      top: 0;
      left: 0;
    }
    .image1 {
      position: relative;
      top: 0;
      left: 0;
    }
    .image2 {
      position: absolute;
      top: 0px;
      left: 17px;
    }
    .image3 {
      position: absolute;
      top: 15px;
      right: 100px;
    }

    .judul {
      padding-top: 18px;
width: 100%;
height: 30px;
text-align: center;
background: #FFCA2E;
font-size: 14px;
color: #232942;
    }
    .text-judul {
padding-top: 7px;
font-style: normal;
font-weight: bold;
font-size: 12px;
line-height: 16px;
color: #252A40;
    }

    .order {
      width: 164px;
height: 51px;

background: #FEC834;
border-radius: 6px;
    }

    .nama-paket {
      width: 72px;
height: 19px;

font-family: Montserrat;
font-style: normal;
font-weight: bold;
font-size: 8px;
line-height: 8px;

/* or 100% */
text-align: center;

color: #232942;
    }

    .text-sm {
      line-height: 1;
    }

    .text-right  {
      text-align : right;
    }
   
  </style>
  <!-- bootstrap -->
  <style type="text/css">
    .container {
    width: 100%;
    margin-right: auto;
    margin-left: auto;
}
.container, .container-sm, .container-md, .container-lg, .container-xl {
    max-width: 1140px;
}
.main-content .container-fluid, .main-content .container-sm, .main-content .container-md, .main-content .container-lg, .main-content .container-xl {
}
.mt--6, .my--6 {
    margin-top: -4.5rem !important;
}
.container-fluid, .container-sm, .container-md, .container-lg, .container-xl {
    width: 100%;
    margin-right: auto;
    margin-left: auto;
}  
.row {
    display: flex;
    /*margin-right: -15px;*/
    /*margin-left: -15px;*/
    /*flex-wrap: wrap;*/
}
.col-md-12 {
    max-width: 100%;
    flex: 0 0 100%;
}
.card {
    margin-bottom: 30px;
    border: 0;
    box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15);
}
.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    border: 1px solid rgba(0, 0, 0, .05);
    border-radius: .375rem;
    /*background-color: #fff;*/
    background-clip: border-box;
}
.card-header:first-child {
    border-radius: calc(.375rem - 1px) calc(.375rem - 1px) 0 0;
    margin-bottom: 0;
    padding: 1.25rem 1.5rem;
    border-bottom: 1px solid rgba(0, 0, 0, .05);
    /*background-color: #fff;*/
}
.card-body {
    min-height: 1px;
    padding: 1.5rem;
    flex: 1 1 auto;
}
.card-title {
    margin-bottom: 0.25rem;
}
.col-xl-6 {
    max-width: 50%;
    flex: 0 0 50%;
}
.table-responsive {
    display: block;
    overflow-x: auto;
    width: 100%;
    -webkit-overflow-scrolling: touch;
}
.card .table {
    margin-bottom: 0;
}
.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #ffca2e;
    background-color: transparent;
}
table {
    border-collapse: collapse;
}
.card .sempit td, .card .sempit th {
    padding-right: 0rem;
    padding-left: 0rem;
}
.table th, .table td {
    padding-top: 0.25rem;
    padding-bottom: 0.25rem;
}
.table td, .table th {
    font-size: .7rem;
    white-space: nowrap;
}
.table th, .table td {
    vertical-align: top;
    border-top: 1px solid #ffca2e;
}
.col-xl-1
    {
        max-width: 8.33333%; 

        flex: 0 0 8.33333%;
    }
    .col-xl-2
    {
        max-width: 16.66667%; 

        flex: 0 0 16.66667%;
    }
    .col-xl-3
    {
        max-width: 25%; 

        flex: 0 0 25%;
    }
    .col-xl-4
    {
        max-width: 33.33333%; 

        flex: 0 0 33.33333%;
    }
    .col-xl-5
    {
        max-width: 41.66667%; 

        flex: 0 0 41.66667%;
    }
    .col-xl-6
    {
        max-width: 50%; 

        flex: 0 0 50%;
    }
    .col-xl-7
    {
        max-width: 58.33333%; 

        flex: 0 0 58.33333%;
    }
    .col-xl-8
    {
        max-width: 66.66667%; 

        flex: 0 0 66.66667%;
    }
    .col-xl-9
    {
        max-width: 75%; 

        flex: 0 0 75%;
    }
    .col-xl-10
    {
        max-width: 83.33333%; 

        flex: 0 0 83.33333%;
    }
    .col-xl-11
    {
        max-width: 91.66667%; 

        flex: 0 0 91.66667%;
    }
    .col-xl-12
    {
        max-width: 100%; 

        flex: 0 0 100%;
    }
.col {
    max-width: 100%;
    flex-basis: 0;
    flex-grow: 1;
}
.align-items-center {
    align-items: center !important;
}
.chart {
    position: relative;
    height: 350px;
}
.chartjs-size-monitor, .chartjs-size-monitor-expand, .chartjs-size-monitor-shrink {
    position: absolute;
    direction: ltr;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    overflow: hidden;
    pointer-events: none;
    visibility: hidden;
    z-index: -1;
}
element {
    display: block;
    width: 423px;
    height: 350px;
}
.chartjs-render-monitor {
    animation: chartjs-render-animation 1ms;
}

.border-0 {
    border: 0 !important;
}

.container, .container-sm, .container-md, .container-lg, .container-xl {
    max-width: 1140px;
}
.container {
    width: 100%;
    margin-right: auto;
    margin-left: auto;
}
.card {
    word-wrap: break-word;
}
.footer {
    padding: 10px 0;
    padding-top: 10px;
    background-color: #232941;
}
.pt-0, .py-0 {
    padding-top: 0 !important;
}
article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
    display: block;
}
.text-center {
    text-align: center !important;
}

.text-wrap {
    white-space: normal !important;
}
.text-left {
    text-align: left !important;
}
span, ul, li, p, div {
    font-size: 12px;
  }

  .text-wrap {
    font-size: .5rem;
  }

  td[rowspan] {
  vertical-align: top;
  text-align: left;
}

td {
  padding : 0 10 0 10px;
}

table tr {
  height:30px;
}
</style>
<img src="src/assets/img/pdf/cover.png" />
<?php
  // ini_set('memory_limit', '-1');

    $eduBridge = base64_encode(file_get_contents(base_url('src/assets/img/brand/white.png'))); 
    $eduBridge_bold = base64_encode(file_get_contents(base_url('src/assets/img/brand/blue.png'))); 

    $birthDate = new DateTime($orders[0]['member'][0]['birthdate']);
    $today = new DateTime($orders[0]['paid-confirm']);
    if ($birthDate > $today) { 
      $y = 0;
      $m = 0;
      $d = 0;
    } else {
      $y = $today->diff($birthDate)->y;
      $m = $today->diff($birthDate)->m;
      $d = $today->diff($birthDate)->d;
    }
  ?>
<div class="row" style="color:#FFCA2E">
      <div class="col-md-8">
        <div class="" style="background: #232941; width: 100%; color: #FFCA2E; padding-bottom: 16px; position: relative;">
        <div class="text-center">
        <h2 style="margin-bottom: 5px;">PESERTA TES</h2>
        </div>
        <style>
            table, th, td {
            }

            .profile tr {  }
        </style>
        <?php 
            $avatar = @file_get_contents(base_url('public/uploads/avatar/'.$orders[0]['member'][0]['avatar'])); 
            if ($avatar === FALSE){
              $avatar="";
            } else {
              $avatar =  base64_encode($avatar);
            }
            ?>
            <!-- <img style="border-radius: 50% !important; height: 80px; margin:5px; " src="data:image/gif;base64,<?= $avatar ?>" /> -->

        <table style="color:#FFCA2E; width:100%" class="profile">
          <tr>
            <td style="width:20%">
                Nama lengkap<br>
            </td>
            <td style="width:80%">  
              : <b><?= substr($orders[0]['member'][0]['first_name'].' '.$orders[0]['member'][0]['last_name'], 0, 50); ?></b><br>
            </td>
          </tr>
          <tr>
            <td style="" >
                Usia saat tes<br>
            </td>
            <td style="" >
              : <?= $y." tahun ".$m." bulan ".$d." hari" ?><br>
            </td>
          </tr>
          <tr>
            <td style="" >
                Email<br>
            </td>
            <td style="" >
              : <?= $orders[0]['member'][0]['email'] ?><br>
            </td>
          </tr>
          <tr>
            <td style="" >
                Institusi<br>
            </td>
            <td style="" >
              : <?= substr($orders[0]['member'][0]['company'], 0, 50); ?><br>
            </td>
          </tr>
          <tr>
            <td style="" >
                Jenjang pendidikan<br>
            </td>
            <td style="" >
              : <?= $orders[0]['member'][0]['education_level'] ?><br>
            </td>
          </tr>
          <tr>
            <td style="" >
                Kelas/semester<br>
            </td>
            <td style="" >
              : <?= $orders[0]['member'][0]['semester'] ?><br>
            </td>
          </tr>
          <tr>
            <td style="" >
                Alamat<br>
            </td>
            <td style="" >
              : <?= substr($orders[0]['member'][0]['address'], 0, 200); ?><br>
            </td>
          </tr>
          
        </table>
      </div>
      <div class="col-md-4">
              
      </div>
      </div>
    </div>
<pagebreak />
      <div class="judul text-center" style="margin-bottom:16px;">
        <b>HASIL TES</b>
      </div>
      <?php 
            foreach ($orders[0]['products'][0]['jenistes'] as $key => $tes) {
              if($tes['id_jenistes']!=3){
              // dd($tes);
              $no = $key+1;
              ?>
            <!-- <div class="card" style="width: 100%;"> -->
              <!-- <div class="card-body"> -->
                <!-- style="page-break-before: always;" -->
            <div style="background: #232941; width: 100%; height:50px; margin-bottom: 16px;">
              <div class="col-xl-12">
                <table width="100%">
                  <tr>
                    <td width="60%" style="padding: 10px 10px 10px 10px;">
                      <h1 style=" padding-left: 16px; color: #FFCA2E;" class="card-title"><?= $tes['nama'] ?></h1>
                    </td>
                    <td width="40%" class="text-right">
                      <p style="color: #FFCA2E;">Mulai tes <b><?=$tes['start-test'] ?></b> <br>Selesai tes <b><?=$tes['done-test'] ?></b></p>
                    </td>
                  </tr>
                </table>
              </div>
              </div>
                <?php 
                if ($tes['id_jenistes']==1){
                  ?>
              <table class="text-center" style="width: 100%; height: 50px; margin-bottom: 16px;">
                  <tr style="">
                    <td style="background: #232941; width: 50px; height:50px; color: #FFCA2E" class="text-center">
                      <h1>IQ</h1>
                    </td>
                    <td style="background: #FFCA2E; width: 60px; color: black">
                      <h1><?= $tes['iq'] ?></h1>
                    </td>
                    <td style="width: 100px">
                      
                    </td>
                    <td style="background: #FFCA2E;  color: black">
                      <h3>TINGKAT KECERDASAN : <?= strtoupper($tes['tingkat_kecerdasan']) ?></h3>
                    </td>
                  </tr>
                </table>
                  <?php
                }
                ?>
                <!-- <h6 class="card-subtitle mb-4 text-muted">Card subtitle</h6> -->

                <!-- <div class="container"> -->
                  <!-- <div class="row"> -->
                    <!-- <div class="col-xl-6"> -->
                      <!-- <span>Hasil Skor</span> -->
                    <!-- </div> -->
                    <!-- <div class="col-xl-6"> -->
                      <!-- <span>Grafik</span> -->
                      <!-- <div class="container"> -->
                        <div class="row">
                          <div class="col">
                            <!-- <div class="card bg-defalut"> -->
                                <div class="row align-items-center">
                                </div>
                              <!-- <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta"> -->
                                <!-- Chart -->
                                <div class="text-center" style="margin-bottom: 32px;">
                                  <!-- Chart wrapper -->
                                  <!-- <canvas id="chart-sw" class="chart-canvas"></canvas> -->
                                  <img style="width: 98%; border: 1px solid #9C020E; padding: 5px;" src="<?= $tes['chart'] ?>" class="img-fluid" alt="Responsive image">
                                </div>
                              <!-- </div> -->
                            <!-- </div> -->
                          </div>
                      </div>
                  <!-- </div> -->
                <!-- </div> -->
    <?php
        }
        }
      ?>
<pagebreak />

      <div class="judul text-center">
            <?php 
            if($orders[0]['member'][0]['education_level']=='SMP'){
             echo "<b>SARAN PEMINATAN SEKOLAH</b>";
            } else {
             echo "<b>SARAN JURUSAN</b>";
            }
            ?>
          </div>
          <div style="">

          <style type="text/css">
            .imageDiv
              {
                  background: #ffca2e;
                  display: block;
                  width: 345px;
                  height: 200px;
                  padding: 10px;
              }
              .bottomDiv{
                 border-radius: 10px;
                 background-color: #9C020E;
                 position:relative;
                 width: 345px;
                 height: 50px;
                 bottom:250px;
                 left:0px;
              }
              p {
                /*margin-right: -100px;*/
              }
         
            .jurusan {
               background-color: #ffca2e; height: auto; color: #232941; width: 80%; padding-top: 8px;margin-left: 50px; border-radius: 5px; padding: 2px 20px 2px 20px; margin-top:20px !important; margin-bottom:20px !important;
            }
            .descjurusan {
              margin-left: 30px; margin-top: -10px; background-color: #232941; color: #ffca2e; width: auto; padding-top: 8px; border-radius: 5px; padding: 12px; text-align:justify;
            }
            .penjelasan {
               background-color: #232941; color: #ffca2e; padding-top: 8px; padding: 20px
            }
            .small {
              /*margin-right: -100  px;*/
            }
          </style>
          <?php
          if(!empty($orders[0]['products'][0]['jurusan1'])){
            $jurusans = explode(',', $orders[0]['products'][0]['jurusan1']);
            foreach ($jurusans as $jurusan){
              ?>
              <div class="jurusan">
                <p style="font-size: 12px; font-weight:bold;"><?= strtoupper($jurusan); ?></b>
              </div>
              <?php
            }
            ?>
            <?php
          }
          ?>
          
          <?php
          if(!empty($orders[0]['products'][0]['descjurusan1'])){
            ?>
            <div class="descjurusan" >
              <p class="small"><?= $orders[0]['products'][0]['descjurusan1']; ?></p>
            </div>
            <?php
          }
          ?>
          <?php
          if(!empty($orders[0]['products'][0]['jurusan2'])){
            ?>
            <div class="jurusan">
              <p style="font-size: 12px; font-weight:bold;"><?= strtoupper($orders[0]['products'][0]['jurusan2']); ?></b>
            </div>
            <?php
          }
          ?>
          <?php
          if(!empty($orders[0]['products'][0]['descjurusan2'])){
            ?>
            <div class="descjurusan" >
              <p class="small"><?= $orders[0]['products'][0]['descjurusan2']; ?></p>
            </div>
            <?php
          }
          ?>
          <?php
          if(!empty($orders[0]['products'][0]['jurusan3'])){
            ?>
            <div class="jurusan">
              <p style="font-size: 12px; font-weight:bold;"><?= strtoupper($orders[0]['products'][0]['jurusan3']); ?></b>
            </div>
            <?php
          }
          ?>
          <?php
          if(!empty($orders[0]['products'][0]['descjurusan3'])){
            ?>
            <div class="descjurusan" >
              <p class="small"><?= $orders[0]['products'][0]['descjurusan3']; ?></p>
            </div>
            <?php
          }
          ?>
          <?php
          if(!empty($orders[0]['products'][0]['jurusan4'])){
            ?>
            <div class="jurusan">
              <p style="font-size: 12px; font-weight:bold;"><?= strtoupper($orders[0]['products'][0]['jurusan4']); ?></b>
            </div>
            <?php
          }
          ?>
          <?php
          if(!empty($orders[0]['products'][0]['descjurusan4'])){
            ?>
            <div class="descjurusan" >
              <p class="small"><?= $orders[0]['products'][0]['descjurusan4']; ?></p>
            </div>
            <?php
          }
          ?>
          <?php
          if(!empty($orders[0]['products'][0]['jurusan5'])){
            ?>
            <div class="jurusan">
              <p style="font-size: 12px; font-weight:bold;"><?= strtoupper($orders[0]['products'][0]['jurusan5']); ?></b>
            </div>
            <?php
          }
          ?>
          <?php
          if(!empty($orders[0]['products'][0]['descjurusan5'])){
            ?>
            <div class="descjurusan" >
              <p class="small"><?= $orders[0]['products'][0]['descjurusan5']; ?></p>
            </div>
            <?php
          }
          ?>
          <pagebreak />
          <div class="judul text-center" style="margin-top: 32px;">
            <b>KESIMPULAN</b>
          </div>
        <div style="margin-top: -10px;">
              <p style="; font-size: 12px;">
                <?= $orders[0]['products'][0]['summary']; ?>
              </p>
          </div>

          <div class="text-center" style="margin-top: 16px;" >
            <b>Jakarta
              <?php  
              if ($orders[0]['products'][0]['summarized'] != '0000-00-00 00:00:00'){
              echo ", ".date_format(date_create($orders[0]['products'][0]['summarized']),"d F Y"); 
              }
              ?>  
            </b>
            <br>
            <span style="">Psikolog</span>
            <br>
            <?php 
            // $ttd = base64_encode(file_get_contents(base_url('public/uploadss/ttd/'.$orders[0]['products'][0]['ttd']))); 
            $ttd = @file_get_contents(base_url('public/uploads/ttd/'.$orders[0]['products'][0]['ttd'])); 
            if ($ttd === FALSE){
              $ttd="";
            } else {
              $ttd =  base64_encode($ttd);
            }
            ?>
            <img style="height: 80px; margin:5px;" src="data:image/gif;base64,<?= $ttd ?>" />
            <br>
            <b style="font-size:13px;"><u><?= $orders[0]['products'][0]['first_name']." ".$orders[0]['products'][0]['last_name'] ?></u></b>
            <br>
            <span><?= $orders[0]['products'][0]['sipp'] ?></span>
        </div>
        <br>
        <div class="">
          <span class="text-sm"><?= $settings[4]['value'] ?></span>
        </div>
          
            