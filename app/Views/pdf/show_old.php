<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="EduBridge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="author" content="EduBridge">
  <title>PIPE Psikotest</title>

  <style type="text/css">
.main-content {
    position: relative;
}
body {
    font-family: Open Sans, sans-serif;
    font-size: 0.75rem;
    font-weight: 400;
    line-height: 1.5;
    text-align: left;
    /*color: #525f7f;*/
}
.container {
    width: 100%;
    margin-right: auto;
    margin-left: auto;
    padding-right: 15px;
    padding-left: 15px;
}
.container, .container-sm, .container-md, .container-lg, .container-xl {
    max-width: 1140px;
}
.main-content .container-fluid, .main-content .container-sm, .main-content .container-md, .main-content .container-lg, .main-content .container-xl {
    padding-right: 30px !important;
    padding-left: 30px !important;
}
.mt--6, .my--6 {
    margin-top: -4.5rem !important;
}
.container-fluid, .container-sm, .container-md, .container-lg, .container-xl {
    width: 100%;
    margin-right: auto;
    margin-left: auto;
}  
.row {
    display: flex;
    margin-right: -15px;
    margin-left: -15px;
    flex-wrap: wrap;
}
.col-md-12 {
    max-width: 100%;
    flex: 0 0 100%;
}
.card {
    margin-bottom: 30px;
    border: 0;
    box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15);
}
.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    border: 1px solid rgba(0, 0, 0, .05);
    border-radius: .375rem;
    /*background-color: #fff;*/
    background-clip: border-box;
}
.card-header:first-child {
    border-radius: calc(.375rem - 1px) calc(.375rem - 1px) 0 0;
    margin-bottom: 0;
    padding: 1.25rem 1.5rem;
    border-bottom: 1px solid rgba(0, 0, 0, .05);
    /*background-color: #fff;*/
}
.card-body {
    min-height: 1px;
    padding: 1.5rem;
    flex: 1 1 auto;
}
.card-title {
    margin-bottom: 1.25rem;
}
.col-xl-6 {
    max-width: 50%;
    flex: 0 0 50%;
}
.table-responsive {
    display: block;
    overflow-x: auto;
    width: 100%;
    -webkit-overflow-scrolling: touch;
}
.card .table {
    margin-bottom: 0;
}
.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #525f7f;
    background-color: transparent;
}
table {
    border-collapse: collapse;
}
.card .sempit td, .card .sempit th {
    padding-right: 0rem;
    padding-left: 0rem;
}
.table th, .table td {
    padding-top: 0.25rem;
    padding-bottom: 0.25rem;
}
.table td, .table th {
    font-size: .5rem;
    white-space: nowrap;
}
.table th, .table td {
    vertical-align: top;
    border-top: 1px solid #e9ecef;
}
.col-xl-1
    {
        max-width: 8.33333%; 

        flex: 0 0 8.33333%;
    }
    .col-xl-2
    {
        max-width: 16.66667%; 

        flex: 0 0 16.66667%;
    }
    .col-xl-3
    {
        max-width: 25%; 

        flex: 0 0 25%;
    }
    .col-xl-4
    {
        max-width: 33.33333%; 

        flex: 0 0 33.33333%;
    }
    .col-xl-5
    {
        max-width: 41.66667%; 

        flex: 0 0 41.66667%;
    }
    .col-xl-6
    {
        max-width: 50%; 

        flex: 0 0 50%;
    }
    .col-xl-7
    {
        max-width: 58.33333%; 

        flex: 0 0 58.33333%;
    }
    .col-xl-8
    {
        max-width: 66.66667%; 

        flex: 0 0 66.66667%;
    }
    .col-xl-9
    {
        max-width: 75%; 

        flex: 0 0 75%;
    }
    .col-xl-10
    {
        max-width: 83.33333%; 

        flex: 0 0 83.33333%;
    }
    .col-xl-11
    {
        max-width: 91.66667%; 

        flex: 0 0 91.66667%;
    }
    .col-xl-12
    {
        max-width: 100%; 

        flex: 0 0 100%;
    }
.col {
    max-width: 100%;
    flex-basis: 0;
    flex-grow: 1;
}
.align-items-center {
    align-items: center !important;
}
.chart {
    position: relative;
    height: 350px;
}
.chartjs-size-monitor, .chartjs-size-monitor-expand, .chartjs-size-monitor-shrink {
    position: absolute;
    direction: ltr;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    overflow: hidden;
    pointer-events: none;
    visibility: hidden;
    z-index: -1;
}
element {
    display: block;
    width: 423px;
    height: 350px;
}
.chartjs-render-monitor {
    animation: chartjs-render-animation 1ms;
}

.border-0 {
    border: 0 !important;
}

.container, .container-sm, .container-md, .container-lg, .container-xl {
    max-width: 1140px;
}
.container {
    width: 100%;
    margin-right: auto;
    margin-left: auto;
    padding-right: 15px;
    padding-left: 15px;
}
.card {
    word-wrap: break-word;
}
.footer {
    padding: 30px 0;
        padding-top: 30px;
    background: #f8f9fe;
}
.pt-0, .py-0 {
    padding-top: 0 !important;
}
article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
    display: block;
}
.text-center {
    text-align: center !important;
}

.text-wrap {
    white-space: normal !important;
}
.text-left {
    text-align: left !important;
}
@page {
                margin: 75px 75px 25px 75px;
            }
header {
                /*position: fixed;*/
                top: -60px;
                left: 0px;
                right: 0px;
                height: 70px;

                /** Extra personal styles **/
                background-color: #9C020E;
                color: white;
                text-align: left;
                line-height: 35px;
                padding-top: 8px;
                padding-left: 45px;
                padding-right: 45px;
            }

            footer {
                position: fixed; 
                bottom: -60px; 
                left: 0px; 
                right: 0px;
                height: 50px; 

                /** Extra personal styles **/
                background-color: #03a9f4;
                color: white;
                text-align: center;
                line-height: 35px;
            }
  </style>


  <!-- Radio image -->
  <style type="text/css">
    .input-hidden {
      position: absolute;
      left: -9999px;
    }
    input[type=radio]:checked + label>img {
      /*border: 1px solid #fff;*/
      box-shadow: 0 0 1px 1px #9C020E;
    }

    /* Stuff after this is only to make things more pretty */
    input[type=radio] + label>img {
      border: 0.5px dashed #ECEFF5;
      width: 150px;
      height: 150px;
      transition: 500ms all;
    }

  /*<!-- sempitkan tabel summary -->*/
  .card .sempit td, .card .sempit th {
    padding-right: 0rem;
    padding-left: 0rem;
}
  </style>

</head>

<body>
  <header>
        <!-- <img src="<?php echo base_url('src/assets/img/brand/blue.png'); ?>" class="navbar-brand-img" alt="..."> -->
        <?php
        $eduBridge = base64_encode(file_get_contents(base_url('src/assets/img/brand/white.png'))); 
        // echo $img;
        // echo '<img src="data:image/gif;base64,' . $img . '" />';
        ?>
    <div class="row">
    <div style="padding-top: 5px" class="col-xl-4">
    <img style="height: 35px" src="data:image/gif;base64,<?= $eduBridge ?>" />
    </div>
    <div style="padding-top: 5px; text-align: right;" class="col-xl-8">
      <p style="font-size: 12px"><b>Education Consulting</b><br>
      Jl. Mgr Sugiyopranoto No.25, Bulustalan, Semarang Selatan, Jawa Tengah 50245</p>
    </div>
    </div>
  </header>

  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Main Content -->
    <div>
      <style type="text/css">
  .table th, .table td {
    padding: 0.25rem;
        padding-top: 0.25rem;
        padding-right: 0.25rem;
        padding-bottom: 0.25rem;
        padding-left: 0.25rem;
}
hr {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
    border: 0;
        border-top-color: currentcolor;
        border-top-style: none;
        border-top-width: 0px;
    border-top: 1px solid rgb(0, 0, 0, .1);
}
</style>
<!-- <div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">My Correction <br>& Summarization</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="#">Product List</a></li>
              <li class="breadcrumb-item active" aria-current="page">Product Detail</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-neutral">CONFIDENTIAL</a>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- Page content -->
<!-- <div class="container-fluid mt--6"> -->
  <?php 
  $birthDate = new DateTime($orders[0]['member'][0]['birthdate']);
  $today = new DateTime($orders[0]['products'][0]['jenistes'][0]['start-test']);
  if ($birthDate > $today) { 
      exit("0 tahun 0 bulan 0 hari");
  }
  $y = $today->diff($birthDate)->y;
  $m = $today->diff($birthDate)->m;
  $d = $today->diff($birthDate)->d;
  ?>
  <div class="card-body">
    <div class="col-md-12">
      <div class="card-header border-0 text-center" >
          <h3 class="mb-0">Member identity</h3>
          <hr>
        <div class="text-left row">
          <div class="col">
          <span class="text-sm">Nama lengkap : <?= $orders[0]['member'][0]['first_name'].' '.$orders[0]['member'][0]['last_name'] ?> </span><br>
          <span class="text-sm">Usia saat tes : <?= $y." tahun ".$m." bulan ".$d." hari" ?></span><br>
          <span class="text-sm">Email : <?= $orders[0]['member'][0]['email'] ?></span><br>

          <span class="text-sm">Institusi : <?= $orders[0]['member'][0]['company'] ?></span><br>
          <span class="text-sm">Jenjang pendidikan : <?= $orders[0]['member'][0]['education_level'] ?> </span><br>
          <span class="text-sm">Kelas / semester : <?= $orders[0]['member'][0]['semester'] ?> </span><br>
          </div>
          <!-- <div class="col">
          <span class="text-sm">: Member EduBridge</span><br>
          <span class="text-sm">: 10 tahun</span><br>
          <span class="text-sm">: member@member.com</span><br>

          <span class="text-sm">: EduBridge</span><br>
          <span class="text-sm">: SMA</span><br>
          <span class="text-sm">: 2</span><br>
          </div> -->
          <!-- <div class="col-xl-2" style="background-color: #03a9f4; height: 150px"> -->
          <!-- </div> -->
        </div>
      </div>
        <div class="card-header border-0 text-center" >
          <h3 class="mb-0">Detail Tes</h3>
          <hr>
        </div>
        <div class="card-header border-0">
          <h3 class="mb-0"><?= $orders[0]['products'][0]['nama'] ?></h3>
          <span class="text-sm"><?= $orders[0]['products'][0]['desc'] ?></span>
          <br>
          <sm><a href="">Order code : <?= 'PIPETEST'.sprintf('%04d', $orders[0]['id']) ?></a></sm>
          <br>
          <span class="text-sm">Tanggal tes : <?= $orders[0]['products'][0]['jenistes'][0]['start-test'] ?></span><br>
        </div>
        <!-- <div class="container"> -->
        <div class="card-header border-0 text-center" >
          <h3 class="mb-0">Hasil tes</h3>
          <hr>
        </div>
            <!-- awal row -->
  <?php 
    foreach ($orders[0]['products'][0]['jenistes'] as $key => $tes) {
      $no = $key+1;
      ?>
            <!-- <div class="card" style="width: 100%;"> -->
              <!-- <div class="card-body"> -->
                <h3 class="card-title" style="page-break-before: always;"><?= $no.'. '.$tes['nama'] ?></h3>
                <!-- <h6 class="card-subtitle mb-4 text-muted">Card subtitle</h6> -->
                <p class="small"><?= $tes['desc'] ?></p>
                <p>Test at <b><?= $tes['start-test'] ?></b> and done at <b><?= $tes['done-test'] ?></b></p>
                <!-- <div class="container"> -->
                  <!-- <div class="row"> -->
                    <!-- <div class="col-xl-6"> -->
                      <!-- <span>Hasil Skor</span> -->
                      <div class="table-responsive">
                        <table id="" class="table sempit" width="100%" >
                          <thead class="thead-light">
                            <tr>
                              <th class="th-sm">Bagian
                              </th>
                              <th class="th-sm">Code
                              </th>
                              <th class="th-sm">Skor
                              </th>
                              <th class="th-sm">SW
                              </th>
                              <th class="th-sm">Klasi<br>fikasi
                              </th>
                            </tr>
                          </thead>
                          <tbody class="list">

                            <?php 
                            $total_skor = 0;
                            $total_skor_sw = 0;
                            $total_skor_klasifikasi = 0;
                            if (!empty($jenistes[$key]['bagiantes'])) {
                              foreach ($jenistes[$key]['bagiantes'] as $key_bagian => $bagian) {
                                ?>
                            <tr>
                                <td>
                                  <?php 
                                  $new_code = ['Daya Pikir', 'Kemampuan Bahasa','Fleksibilitas Berpikir','Penalaran Logis', 'Berpikir Praktis', 'Pemikiiran Konkret', 'Daya Bayang', 'Pemikiran Spasial', 'Daya Ingat'];
                                  if(!empty($bagian['judul'])) {
                                    if($bagian['id_jenistes']==1){
                                      echo $new_code[$key_bagian];
                                    } else {
                                      echo $bagian['judul']; 
                                    }
                                  } ?>
                                </td>
                                <td>
                                  <?php if(!empty($bagian['konversi'][0]['code'])) {echo $bagian['konversi'][0]['code']; } else { echo ''; } ?>
                                </td>
                                <td>
                                  <?php if(!empty($bagian['skor'][0]['score'])) {echo $bagian['skor'][0]['score']; $skor = $bagian['skor'][0]['score'];  } else { echo '0'; $skor=0;} ?>
                                </td>
                                <td>
                                  <?php if(!empty($bagian['konversi'][0]['sw'])) {echo $bagian['konversi'][0]['sw']; $skor_sw = $bagian['konversi'][0]['sw'];  } else { echo '0'; $skor_sw=0;} ?>
                                </td>
                                <td>
                                  <?php if(!empty($bagian['konversi'][0]['sw'])) {echo $bagian['konversi'][0]['klasifikasi']; $skor_klasifikasi = $bagian['konversi'][0]['klasifikasi'];  } else { echo '0'; $skor_klasifikasi=0;} ?>
                                </td>
                            </tr>
                                <?php
                                $total_skor = $total_skor + $skor;
                                $total_skor_sw = $total_skor_sw + $skor_sw;
                                // $total_skor_klasifikasi = $total_skor_klasifikasi + $skor_klasifikasi;
                              }
                            }
                            ?>
                          </tbody>
                          <tfoot>
                            <th>Total</th>
                            <th><?= $total_skor ?></th>
                            <th><?= $total_skor_sw ?></th>
                            <th><?= $total_skor_klasifikasi ?></th>
                          </tfoot>
                        </table>
                      </div>
                    <!-- </div> -->
                    <!-- <div class="col-xl-6"> -->
                      <!-- <span>Grafik</span> -->
                      <!-- <div class="container"> -->
                        <div class="row">
                          <div class="col">
                            <!-- <div class="card bg-defalut"> -->
                                <div class="row align-items-center">
                                </div>
                              <!-- <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta"> -->
                                <!-- Chart -->
                                <div class="text-center">
                                  <!-- Chart wrapper -->
                                  <!-- <canvas id="chart-sw" class="chart-canvas"></canvas> -->
                                  <img style="width: 50%;" src="<?= $tes['chart'] ?>" class="img-fluid" alt="Responsive image">
                                </div>
                              <!-- </div> -->
                            <!-- </div> -->
                          </div>
                      </div>
                  <!-- </div> -->
                <!-- </div> -->
                <div>
                          <h4>Penjelasan : </h4>
                <?= $tes['summary']; ?>
                  
                   <br>
                </div>
              <!-- </div> -->
            <!-- </div> -->
          <!-- </div> -->
    <?php
        }
      ?>
        <!-- akhir row -->
        <!-- </div> -->
        <!-- final summary -->
          <!-- <div class="card" style="width: 100%;"> -->
            <div class="" style="page-break-before: always;">
                <h3 class="card-title text-center">Saran Jurusan</h3>
                 <hr>
                <p>1. <?= $orders[0]['products'][0]['jurusan1']; ?></p>
                <p class="small"><?= $orders[0]['products'][0]['descjurusan1']; ?></p>

                <p>2. <?= $orders[0]['products'][0]['jurusan2']; ?></p>
                <p class="small"><?= $orders[0]['products'][0]['descjurusan2']; ?></p>

                <p>3. <?= $orders[0]['products'][0]['jurusan3']; ?></p>
                <p class="small"><?= $orders[0]['products'][0]['descjurusan3']; ?></p>

                <p>4. <?= $orders[0]['products'][0]['jurusan4']; ?></p>
                <p class="small"><?= $orders[0]['products'][0]['descjurusan4']; ?></p>

                <p>5. <?= $orders[0]['products'][0]['jurusan5']; ?></p>
                <p class="small"><?= $orders[0]['products'][0]['descjurusan5']; ?></p>

                <h3 class="card-title text-center">Final Summary</h3>
                 <hr>
                  <?= $orders[0]['products'][0]['summary']; ?>
              <div class=" text-center">
                  <span>Jakarta, 24 Februari 2021</span>
                  <p>Psychologist</p>
                  <img src="data:image/jpg;base64, {{ base64_encode(file_get_contents('<?php echo base_url('public/uploads/ttd/yana.jpg'); ?>')) }} ">
                  <!-- <img alt="" style="height: 75px" src="<?php echo base_url('public/uploads/ttd/yana.jpg'); ?>"> -->
                  <br>
                  <span><b><u>Yana., Psi</u></b></span>
                  <p>SIPP. No. 0183.3038.2838</p>
              </div>
              <div class="text-right">
                <p class="text-sm">*psikogram ini berlaku hanya jika ditanda tangani psikolog</p>
                <p class="text-sm">Hasil tes ini selanjutnya dapat Anda konsultasikan dengan psikolog kami. Anda dapat menghubungi psikolog kami</p>
              </div>
          </div>
        <!-- </div> -->
        <div class="card" style="width: 100%; page-break-before: always;">
            <div class="card-body">
                <h3 class="card-title">Appendix</h3>
                 <hr>
              <div class=" text-center">
                      <div class="table-responsive">
                        <table id="sempit" class="table sempit" width="100%" >
                          <thead class="thead-light">
                            <tr>
                              <th class="th">Fakultas / Faculty
                              </th>
                              <th class="th-sm">Internasional
                              </th>
                              <th class="th-sm">Indonesia
                              </th>
                            </tr>
                          </thead>
                          <tbody class="list text-left">
                            <?php
                            for ($i=0; $i < 5; $i++) { 
                               ?>
                            <tr>
                                <td>
                                    <b>Ekonomi</b>
                                    <br>
                                    Economy
                                </td>
                                <td>
                                  <span class="text-wrap">Accounting, Business Administration, Small Business Operations, Entrepreneurial and Finance, Human Resources Management, Insurance, International Business, Marketing, Taxation, Business Operations Support, Business/Commerce, International Economics, Applied Economics,Business/Managerial Economics</span>
                                </td>
                                <td>
                                  <span class="text-wrap">
                                  Akuntansi, Akuntansi Pajak, Manajemen,Manajemen Keuangan, Manajemen Bisnis, Manajemen Logistik,Manajemen Perbankan,  Manajemen Pemasaran,Bisnis Internasional, Administrasi Fiskal (Perpajakan), Administrasi KeuangandanPerbankan, Bisnis Digital,Branding,IlmuEkonomi, Ekonomi Syariah
                                  </span>
                                </td>
                            </tr>
                            <?php
                             } 
                            ?>
                          </tbody>
                        </table>
                      </div>
              </div>
          </div>
        </div>
        <!-- akhir final summary -->
  <!-- </div> -->
<!-- </div> -->


    </div>
    
      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
              &copy; <?php echo date("Y"); ?> <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">EduBridge</a>
            </div>
          </div>
          <!-- <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">EduBridge</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
              </li>
            </ul>
          </div> -->
        </div>
      </footer>
    </div>
  <!-- </div> -->
  <!-- Vue js -->
  <script src="https://vuejs.org/js/vue.min.js"></script>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="<?php echo base_url('src/assets/vendor/jquery/dist/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/js-cookie/js.cookie.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js'); ?>"></script>
  <!-- Optional JS -->
  <script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.extension.js'); ?>"></script>
  <!-- Argon JS -->
  <script src="<?php echo base_url('src/assets/js/argon.js?v=1.2.0'); ?>"></script>
  <!-- Datatable -->
  <script src="<?php echo base_url('src/node_modules/mdbootstrap/js/addons/datatables2.min.js'); ?>" type="text/javascript"></script>
  <script type="text/javascript">
  $(document).ready(function () {
  $('#dtBasicExample').DataTable();
  $('.dataTables_length').addClass('bs-select');
});
</script>
<script type="text/javascript">
  

</script>
<!-- ini lola banget -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>




</body>

</html> 