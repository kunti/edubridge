<html>

<head>
  <title>EduBridge - Test Result</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style type="text/css">
    
      </style>
  <style>
    .page {
  background: white;
  box-shadow: 2px 2px 2px rgba(0,0,0,0.6);
  max-width: 1600px;
  min-width: 1000px;
}
    @page {
      margin: 80px 0in 1in 0in;
    }
    .flyleaf {
    margin-top: -80px;
    margin-bottom: -1in;
    page-break-after: always;
    /*padding: 5px 5px;*/
  }

    body {
      padding: 0;
      margin: 0;
      font-family: 'montserrat';
      letter-spacing: 0.3px;

    }

    #watermark {
                position: fixed;
                top:   225px;
                left:  150;
                /** The width and height may change 
                    according to the dimensions of your letterhead
                **/
                width:    21.8cm;
                height:   28cm;

                /** Your watermark should be behind every content**/
                z-index:  1000;
                 opacity: 0.1;
            }

    header {
      position: fixed;
      top: -80px;
      left: 0;
      right: 0;
      height: 80px;
      padding: 0px 0px;
      /*background-color: #9C020E;*/
      z-index: 1000;
    }

    .text-center {
      text-align: center;
    }

    main {
      margin-top: 0px;
      padding: 10px 50px;
    }

    content {
      padding: 0px 50px;
    }

    footer {
      position: fixed;
      bottom: 0;
      left: 0;
      right: 0;
      background-color: #232941;
      height: 5px;
      /*border-top: 1px solid #1f1f1f;*/
      z-index: 1000;
    }

    footer h3 {
      background-color: #232941;
      padding-left: 100px;
    }

    .table
    {
        width: 100%;
        margin-bottom: 1rem;

        color: #212529;
    }
    .table th,
    .table td
    {
        padding: .75rem;

        vertical-align: top;

        border-top: 1px solid #dee2e6;
    }
    .table thead th
    {
        vertical-align: bottom;

        border-bottom: 2px solid #dee2e6;
    }
    .table tbody + tbody
    {
        border-top: 2px solid #dee2e6;
    }

    .table-sm th,
    .table-sm td
    {
        padding: .3rem;
    }

    .table-bordered
    {
        border: 1px solid #ffca2e;
    }
    .table-bordered th,
    .table-bordered td
    {
        border: 1px solid #ffca2e;
    }
    .table-bordered thead th,
    .table-bordered thead td
    {
        border-bottom-width: 2px;
    }
    .parent {
      position: relative;
      top: 0;
      left: 0;
    }
    .image1 {
      position: relative;
      top: 0;
      left: 0;
    }
    .image2 {
      position: absolute;
      top: 0px;
      left: 17px;
    }
    .image3 {
      position: absolute;
      top: 15px;
      right: 100px;
    }

   
  </style>
  <!-- bootstrap -->
  <style type="text/css">
    .container {
    width: 100%;
    margin-right: auto;
    margin-left: auto;
    padding-right: 15px;
    padding-left: 15px;
}
.container, .container-sm, .container-md, .container-lg, .container-xl {
    max-width: 1140px;
}
.main-content .container-fluid, .main-content .container-sm, .main-content .container-md, .main-content .container-lg, .main-content .container-xl {
    padding-right: 30px !important;
    padding-left: 30px !important;
}
.mt--6, .my--6 {
    margin-top: -4.5rem !important;
}
.container-fluid, .container-sm, .container-md, .container-lg, .container-xl {
    width: 100%;
    margin-right: auto;
    margin-left: auto;
}  
.row {
    display: flex;
    /*margin-right: -15px;*/
    /*margin-left: -15px;*/
    /*flex-wrap: wrap;*/
}
.col-md-12 {
    max-width: 100%;
    flex: 0 0 100%;
}
.card {
    margin-bottom: 30px;
    border: 0;
    box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15);
}
.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    border: 1px solid rgba(0, 0, 0, .05);
    border-radius: .375rem;
    /*background-color: #fff;*/
    background-clip: border-box;
}
.card-header:first-child {
    border-radius: calc(.375rem - 1px) calc(.375rem - 1px) 0 0;
    margin-bottom: 0;
    padding: 1.25rem 1.5rem;
    border-bottom: 1px solid rgba(0, 0, 0, .05);
    /*background-color: #fff;*/
}
.card-body {
    min-height: 1px;
    padding: 1.5rem;
    flex: 1 1 auto;
}
.card-title {
    margin-bottom: 0.25rem;
}
.col-xl-6 {
    max-width: 50%;
    flex: 0 0 50%;
}
.table-responsive {
    display: block;
    overflow-x: auto;
    width: 100%;
    -webkit-overflow-scrolling: touch;
}
.card .table {
    margin-bottom: 0;
}
.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #ffca2e;
    background-color: transparent;
}
table {
    border-collapse: collapse;
}
.card .sempit td, .card .sempit th {
    padding-right: 0rem;
    padding-left: 0rem;
}
.table th, .table td {
    padding-top: 0.25rem;
    padding-bottom: 0.25rem;
}
.table td, .table th {
    font-size: .7rem;
    white-space: nowrap;
}
.table th, .table td {
    vertical-align: top;
    border-top: 1px solid #ffca2e;
}
.col-xl-1
    {
        max-width: 8.33333%; 

        flex: 0 0 8.33333%;
    }
    .col-xl-2
    {
        max-width: 16.66667%; 

        flex: 0 0 16.66667%;
    }
    .col-xl-3
    {
        max-width: 25%; 

        flex: 0 0 25%;
    }
    .col-xl-4
    {
        max-width: 33.33333%; 

        flex: 0 0 33.33333%;
    }
    .col-xl-5
    {
        max-width: 41.66667%; 

        flex: 0 0 41.66667%;
    }
    .col-xl-6
    {
        max-width: 50%; 

        flex: 0 0 50%;
    }
    .col-xl-7
    {
        max-width: 58.33333%; 

        flex: 0 0 58.33333%;
    }
    .col-xl-8
    {
        max-width: 66.66667%; 

        flex: 0 0 66.66667%;
    }
    .col-xl-9
    {
        max-width: 75%; 

        flex: 0 0 75%;
    }
    .col-xl-10
    {
        max-width: 83.33333%; 

        flex: 0 0 83.33333%;
    }
    .col-xl-11
    {
        max-width: 91.66667%; 

        flex: 0 0 91.66667%;
    }
    .col-xl-12
    {
        max-width: 100%; 

        flex: 0 0 100%;
    }
.col {
    max-width: 100%;
    flex-basis: 0;
    flex-grow: 1;
}
.align-items-center {
    align-items: center !important;
}
.chart {
    position: relative;
    height: 350px;
}
.chartjs-size-monitor, .chartjs-size-monitor-expand, .chartjs-size-monitor-shrink {
    position: absolute;
    direction: ltr;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    overflow: hidden;
    pointer-events: none;
    visibility: hidden;
    z-index: -1;
}
element {
    display: block;
    width: 423px;
    height: 350px;
}
.chartjs-render-monitor {
    animation: chartjs-render-animation 1ms;
}

.border-0 {
    border: 0 !important;
}

.container, .container-sm, .container-md, .container-lg, .container-xl {
    max-width: 1140px;
}
.container {
    width: 100%;
    margin-right: auto;
    margin-left: auto;
    padding-right: 15px;
    padding-left: 15px;
}
.card {
    word-wrap: break-word;
}
.footer {
    padding: 10px 0;
    padding-top: 10px;
    background-color: #232941;
}
.pt-0, .py-0 {
    padding-top: 0 !important;
}
article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
    display: block;
}
.text-center {
    text-align: center !important;
}

.text-wrap {
    white-space: normal !important;
}
.text-left {
    text-align: left !important;
}
span, ul, li, p, div {
    font-size: 10px;
  }

  .text-wrap {
    font-size: .5rem;
  }

  td[rowspan] {
  vertical-align: top;
  text-align: left;
}
  </style>

</head>

<body>
  <?php
  // ini_set('memory_limit', '-1');

    $eduBridge = base64_encode(file_get_contents(base_url('src/assets/img/brand/white.png'))); 
    $eduBridge_bold = base64_encode(file_get_contents(base_url('src/assets/img/brand/blue.png'))); 

    $birthDate = new DateTime($orders[0]['member'][0]['birthdate']);
    $today = new DateTime($orders[0]['paid-confirm']);
    if ($birthDate > $today) { 
      $y = 0;
      $m = 0;
      $d = 0;
    } else {
      $y = $today->diff($birthDate)->y;
      $m = $today->diff($birthDate)->m;
      $d = $today->diff($birthDate)->d;
    }
  ?>
  <?php
    $cover = @file_get_contents(base_url('src/assets/img/pdf/cover.png')); 
    if ($cover === FALSE){
      $cover="";
    } else {
      $cover =  base64_encode($cover);
    }
    $header = @file_get_contents(base_url('src/assets/img/pdf/header.jpg')); 
    if ($header === FALSE){
      $header="";
    } else {
      $header =  base64_encode($header);
    }
    $logo = @file_get_contents(base_url('src/assets/img/pdf/eduBridge.png')); 
    if ($logo === FALSE){
      $logo="";
    } else {
      $logo =  base64_encode($logo);
    }
    $pipe_color = @file_get_contents(base_url('src/assets/img/pdf/pipe-color.png')); 
    if ($pipe_color === FALSE){
      $pipe_color="";
    } else {
      $pipe_color =  base64_encode($pipe_color);
    }
    $member = @file_get_contents(base_url('src/assets/img/pdf/member.png')); 
    if ($member === FALSE){
      $member="";
    } else {
      $member =  base64_encode($member);
    }
    $pipe_black = @file_get_contents(base_url('src/assets/img/pdf/pipe-black.png')); 
    if ($pipe_black === FALSE){
      $pipe_black="";
    } else {
      $pipe_black =  base64_encode($pipe_black);
    }
    $potential = @file_get_contents(base_url('src/assets/img/pdf/potential.png')); 
    if ($potential === FALSE){
      $potential="";
    } else {
      $potential =  base64_encode($potential);
    }
  ?>
  
    <!-- Cover page -->
  <div class="flyleaf">
    <div class="row">
      <div>
        <img style="width:  100%" src="data:image/gif;base64,<?= $cover ?>" />
        <div class="" style="background: #232941; width: 100%; color: #FFCA2E; padding-bottom: 16px; position: relative;">
        <div class="text-center">
        <h3 style="padding-left: 50px; padding-right: 50px; margin-bottom: -5px;">PESERTA TES</h3>
        </div>
        <table style="padding-left: 50px; padding-right: 12px">
          <tr>
            <td style="width: 130px">
              <span class="text-sm">Nama lengkap</span><br>
              <span class="text-sm">Usia saat tes</span><br>
              <span class="text-sm">Email</span><br>

              <span class="text-sm">Institusi</span><br>
              <span class="text-sm">Jenjang pendidikan</span><br>
              <span class="text-sm">Kelas/semester</span><br>
              <span class="text-sm">Alamat</span><br>
              <span class="text-sm" style="color:#232941">Alamat2</span>
            </td>
            <td style="width: 350px;">
              <span class="text-sm">: <?= substr($orders[0]['member'][0]['first_name'].' '.$orders[0]['member'][0]['last_name'], 0, 50); ?></span><br>
              <span class="text-sm">: <?= $y." tahun ".$m." bulan ".$d." hari" ?></span><br>
              <span class="text-sm">: <?= $orders[0]['member'][0]['email'] ?></span><br>

              <span class="text-sm">: <?= substr($orders[0]['member'][0]['company'], 0, 50); ?></span><br>
              <span class="text-sm">: <?= $orders[0]['member'][0]['education_level'] ?></span><br>
              <span class="text-sm">: <?= $orders[0]['member'][0]['semester'] ?></span><br>
              <span class="text-sm">: <?= substr($orders[0]['member'][0]['address'], 0, 200); ?></span><br>
            </td>
            <td class="text-right" style="font-size: 10px;">
              <div style="
              margin-left: 0px;
                background: #FEC834;
                border-radius: 6px;
                width: 220px;
                color: #232941;
                text-align: center;
                ">
              <p style=" font-size: 10px">
              Kode order<br><b style="font-size: 14px"><?= 'PIPETEST'.sprintf('%04d', $orders[0]['id']); ?></b><br>
              Nama layanan<br><b style="font-size: 14px"><?= $orders[0]['products'][0]['nama'] ?></b><br>
              <span style="font-size: 10px">Tanggal order<br><?= $orders[0]['paid-confirm'] ?></p>
              </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div style="margin-right: 50px; margin-left: 50px; margin-top: 5px; text-align: left; font-size: 10px;">
        <b style="">PIPE PSIKOTEST</b><br>
        <!-- alamat -->
        <p style="padding: 0px; margin : 0px"><?= $settings[0]['value'] ?></p>
        <!-- no telp web -->
         <p style="padding: 0px; margin : 0px"><?= $settings[1]['value'].' '.$settings[3]['value']; ?></p>
        <!-- copyright -->
         <p style="padding: 0px; margin : 0px"><b>&#169; EduBridge <?php  
                  if ($orders[0]['products'][0]['summarized'] != '0000-00-00 00:00:00'){
                  echo date_format(date_create($orders[0]['products'][0]['summarized']),"Y"); 
                  } ?></b></p>
        </div>
      </div>
    </div>
  </div>
<header>
  <div id="watermark">
      <!-- <img src="data:image/gif;base64,<?= $pipe_black ?>" width="50%" /> -->
  </div>
    <div class="row">
      <div class="parent">
        <!-- <img class="image1" style="width:  100%" src="data:image/gif;base64,<?= $header ?>" /> -->
        <!-- <img class="image2" style="width:  10%" src="data:image/gif;base64,<?= $logo ?>" /> -->
        <img class="image3" style="height:  55px" src="data:image/gif;base64,<?= $pipe_color ?>" />

        <!-- <img class="image1" src="https://placehold.it/50" /> -->
        <!-- <img class="image2" src="https://placehold.it/100" /> -->
      </div>
<!-- <div style="padding-top: 5px" class="col-xl-4"> -->
      <!-- </div> -->
      <div style="padding-top: 5px; text-align: right;" class="col-xl-8">
        <p style="font-size: 12px; color: #fff"><b>Header example</b><br>
      </div>
    </div>
  </header>
  <footer>
    <div style="margin-right: 50px; margin-left: 50px; margin-top: 5px; text-align: right; font-size: 10px;">
        <b style="">PIPE PSIKOTEST</b><br>
        <!-- alamat -->
        <p style="padding: 0px; margin : 0px"><?= $settings[0]['value'] ?></p>
        <!-- no telp web -->
         <p style="padding: 0px; margin : 0px"><?= $settings[1]['value'].' '.$settings[3]['value']; ?></p>
        <!-- copyright -->
         <p style="padding: 0px; margin : 0px"><b>&#169; EduBridge <?php  
                  if ($orders[0]['products'][0]['summarized'] != '0000-00-00 00:00:00'){
                  echo date_format(date_create($orders[0]['products'][0]['summarized']),"Y"); 
                  } ?></b></p>
        </div>
  </footer>
<style type="text/css">
    .judul {
      padding-top: 12px;
width: 100%;
height: 32px;
text-align: center;
background: #FFCA2E;
font-size: 14px;
color: #232942;
    }
    .text-judul {
padding-top: 7px;
font-style: normal;
font-weight: bold;
font-size: 12px;
line-height: 16px;
color: #252A40;
    }

    .order {
      width: 164px;
height: 51px;

background: #FEC834;
border-radius: 6px;
    }

    .nama-paket {
      width: 72px;
height: 19px;

font-family: Montserrat;
font-style: normal;
font-weight: bold;
font-size: 8px;
line-height: 8px;

/* or 100% */
text-align: center;

color: #232942;
    }

    .text-sm {
      line-height: 1;
    }
  </style>
  <main>
      <div class="judul text-center">
        <b>HASIL TES</b>
      </div>
      <?php 
            foreach ($orders[0]['products'][0]['jenistes'] as $key => $tes) {
              $no = $key+1;
              ?>
            <!-- <div class="card" style="width: 100%;"> -->
              <!-- <div class="card-body"> -->
                <!-- style="page-break-before: always;" -->
            <div class="" style="<?php if ($key!=0) echo "page-break-before: always;" ?> background: #232941; width: 100%; color: #FFCA2E; margin-bottom: 16px;">
              <div class="col-xl-12">
                <table width="100%">
                  <tr>
                    <td width="60%">
                      <h3 style="padding-left: 16px; margin-bottom: 16px;" class="card-title"><?= $no.'. '.$tes['nama'] ?></h3>
                    </td>
                    <td width="40%" class="text-right">
                      <p style="">Mulai tes <b><?=$tes['start-test'] ?></b> <br>Selesai tes <b><?=$tes['done-test'] ?></b></p>
                    </td>
                  </tr>
                </table>
              </div>
                <!-- <table>
                  <tr>
                    <td style="width: 100px" class="text-center">
                      <img style="width:  50px" src="data:image/gif;base64,<?= $potential ?>" />
                    </td>
                    <td>
                      <p><?= $tes['desc'] ?></p>
                    </td>
                  </tr>
                </table> -->
              </div>
                <?php 
                if ($key==0){
                  ?>
              <table class="text-center" style="width: 100%; height: 50px; margin-bottom: 16px;">
                  <tr style="height: 50px">
                    <td style="background: #232941; width: 50px; color: #FFCA2E" class="text-center">
                      IQ
                    </td>
                    <td style="background: #FFCA2E; width: 60px; color: black">
                      <?= $tes['iq'] ?>
                    </td>
                    <td style="width: 100px">
                      
                    </td>
                    <td style="background: #FFCA2E;  color: black">
                      TINGKAT KECERDASAN : <?= strtoupper($tes['tingkat_kecerdasan']) ?>
                    </td>
                  </tr>
                </table>
                  <?php
                }
                ?>
                <!-- <h6 class="card-subtitle mb-4 text-muted">Card subtitle</h6> -->

                <!-- <div class="container"> -->
                  <!-- <div class="row"> -->
                    <!-- <div class="col-xl-6"> -->
                      <!-- <span>Hasil Skor</span> -->
                    <!-- </div> -->
                    <!-- <div class="col-xl-6"> -->
                      <!-- <span>Grafik</span> -->
                      <!-- <div class="container"> -->
                        <div class="row">
                          <div class="col">
                            <!-- <div class="card bg-defalut"> -->
                                <div class="row align-items-center">
                                </div>
                              <!-- <div class="card-body" data-toggle="tooltip" data-placement="bottom" title="Peserta"> -->
                                <!-- Chart -->
                                <div class="text-center">
                                  <!-- Chart wrapper -->
                                  <!-- <canvas id="chart-sw" class="chart-canvas"></canvas> -->
                                  <img style="width: 98%; border: 1px solid #9C020E; padding: 5px;" src="<?= $tes['chart'] ?>" class="img-fluid" alt="Responsive image">
                                </div>
                              <!-- </div> -->
                            <!-- </div> -->
                          </div>
                      </div>
                  <!-- </div> -->
                <!-- </div> -->
                <div>
                <div style="margin-top: 16px;">
                <div class="judul text-left" style="padding-left: 16px;">
                  <b>PENJELASAN</b>
                </div>
                <div class="penjelasan">
                  <?= $tes['summary']; ?> 
                </div>
                </div>
                </div>
    <?php
        }
      ?>
    </div>
    <div class="" style="page-break-before: always; margin-top: 16px;">
          <div class="judul text-center">
            <b>SARAN JURUSAN</b>
          </div>
          <div style="padding: 12px">

          <style type="text/css">
            .imageDiv
              {
                  margin-left: 20px;
                  background: #ffca2e;
                  display: block;
                  width: 345px;
                  height: 200px;
                  padding: 10px;
              }
              .bottomDiv{
                 border-radius: 10px;
                 background-color: #9C020E;
                 position:relative;
                 width: 345px;
                 height: 50px;
                 bottom:250px;
                 left:0px;
              }
              p {
                /*margin-right: -100px;*/
              }
         
            .jurusan {
              position: relative; background-color: #ffca2e; height: auto; color: #232941; width: 70%; padding-top: 8px; border-radius: 5px; padding: 10px 20px 10px 20px; margin-top: 16px;
            }
            .descjurusan {
              margin-left: 30px; margin-top: -10px; background-color: #232941; color: #ffca2e; width: auto; padding-top: 8px; border-radius: 5px; padding: 12px
            }
            .penjelasan {
                
               background-color: #232941; color: #ffca2e; padding-top: 8px; padding: 20px
            }
            .small {
              /*margin-right: -100  px;*/
            }
          </style>
          <div class="jurusan">
            <b style="font-size: 14px"><?= strtoupper($orders[0]['products'][0]['jurusan1']); ?></b>
          </div>
          <div class="descjurusan" >
            <p class="small"><?= $orders[0]['products'][0]['descjurusan1']; ?></p>
          </div>
          <div class="jurusan">
            <b style="font-size: 14px"><?= strtoupper($orders[0]['products'][0]['jurusan2']); ?></b>
          </div>
          <div class="descjurusan">
            <p class="small"><?= $orders[0]['products'][0]['descjurusan2']; ?></p>
          </div>
          <div class="jurusan">
            <b style="font-size: 14px"><?= strtoupper($orders[0]['products'][0]['jurusan3']); ?></b>
          </div>
          <div class="descjurusan">
            <p class="small"><?= $orders[0]['products'][0]['descjurusan3']; ?></p>
          </div>
          <div class="jurusan">
            <b style="font-size: 14px"><?= strtoupper($orders[0]['products'][0]['jurusan4']); ?></b>
          </div>
          <div class="descjurusan">
            <p class="small"><?= $orders[0]['products'][0]['descjurusan4']; ?></p>
          </div>
          <div class="jurusan">
            <b style="font-size: 14px"><?= strtoupper($orders[0]['products'][0]['jurusan5']); ?></b>
          </div>
          <div class="descjurusan">
            <p class="small"><?= $orders[0]['products'][0]['descjurusan5']; ?></p>
          </div>

          <div style="page-break-before: always;" class="judul text-center" style="margin-top: 16px;">
            <b>KESIMPULAN</b>
          </div>
            
              <p style="text-align: left; font-size: 10px; margin-right: 10px;">
                <?= $orders[0]['products'][0]['summary']; ?>
              </p>
            
          <div style="right: 100px;">
            
          </div>
        <div class="text-center" style="margin-top: 16px; padding-left: 8px;" >
            <span>Jakarta
              <?php  
              if ($orders[0]['products'][0]['summarized'] != '0000-00-00 00:00:00'){
              echo ", ".date_format(date_create($orders[0]['products'][0]['summarized']),"d F Y"); 
              }
              ?>  
            </span>
            <p></p>
            <span style="padding-bottom: 10px;">Psikolog</span>
            <p></p>
            <br>
            <?php 
            // $ttd = base64_encode(file_get_contents(base_url('public/uploadss/ttd/'.$orders[0]['products'][0]['ttd']))); 
            $ttd = @file_get_contents(base_url('public/uploads/ttd/'.$orders[0]['products'][0]['ttd'])); 
            if ($ttd === FALSE){
              $ttd="";
            } else {
              $ttd =  base64_encode($ttd);
            }
            ?>
            <img style="height: 50px" src="data:image/gif;base64,<?= $ttd ?>" />
            <br>
            <span><b><u><?= $orders[0]['products'][0]['first_name']." ".$orders[0]['products'][0]['last_name'] ?></u></b></span>
            <br>
            <span><?= $orders[0]['products'][0]['sipp'] ?></span>
        </div>
        <div class="text-center">
          <p class="text-sm"><?= $settings[4]['value'] ?></p>
        </div>
    </div>
    <style type="text/css">
      .table2 th,
.table2 td
{
    padding: .75rem;
    font-size: .7rem;


    vertical-align: top;

    border-top: 1px solid #dee2e6;
}
.table2 thead th
{
    vertical-align: bottom;

    border-bottom: 2px solid #dee2e6;
}
.table2 tbody + tbody
{
    border-top: 2px solid #dee2e6;
}

.table2-sm th,
.table2-sm td
{
    padding: .3rem;
}

.table2-bordered
{
    border: 1px solid #dee2e6;
    border: 1px solid #dee2e6;
}
.table2-bordered th,
.table2-bordered td
{
    border: 1px solid #dee2e6;
}
.table2-bordered thead th,
.table2-bordered thead td
{
    border-bottom-width: 2px;
}

    </style>
    <div style="page-break-before: always;">
      <div class="judul text-center">
        <b>APPENDIX</b>
      </div>    
      <div class="text-center" width="100%">
        <table class="table2" >
          <thead class="thead-light">
            <tr>
              <th class="th">IQ
              </th>
              <th class="th-sm">Tingkat kecerdasan
              </th>
            </tr>
          </thead>
          <tbody class="list text-left">
            <tr>
                <td>>= 130</td>
                <td>Sangat Superior</td>
            </tr>
            <tr>
                <td>120 - 129</td>
                <td>Superior</td>
            </tr>
            <tr>
                <td>110 - 119</td>
                <td>Diatas rata-rata</td>
            </tr>
            <tr>
                <td>90 - 109</td>
                <td>Rata-rata</td>
            </tr>
            <tr>
                <td>80 - 89</td>
                <td>Dibawah rata-rata</td>
            </tr>
            <tr>
                <td>70-79</td>
                <td>Borderline</td>
            </tr>
            <tr>
                <td><= 69</td>
                <td>Intellectual deficient</td>
            </tr>
          </tbody>
        </table>
         <!-- 2. Daftar jurusan
        <table id="sempit" class="table2 sempit" width="100%" >
          <thead class="thead-light">
            <tr>
              <th class="th">Fakultas / Faculty
              </th>
              <th class="th-sm">Internasional
              </th>
              <th class="th-sm">Indonesia
              </th>
            </tr>
          </thead>
          <tbody class="list text-left">
            <?php
            for ($i=0; $i < 5; $i++) { 
               ?>
            <tr>
                <td>
                    <b>Ekonomi</b>
                    <br>
                    Economy
                </td>
                <td>
                  <span class="text-wrap">Accounting, Business Administration, Small Business Operations, Entrepreneurial and Finance, Human Resources Management, Insurance, International Business, Marketing, Taxation, Business Operations Support, Business/Commerce, International Economics, Applied Economics,Business/Managerial Economics</span>
                </td>
                <td>
                  <span class="text-wrap">
                  Akuntansi, Akuntansi Pajak, Manajemen,Manajemen Keuangan, Manajemen Bisnis, Manajemen Logistik,Manajemen Perbankan,  Manajemen Pemasaran,Bisnis Internasional, Administrasi Fiskal (Perpajakan), Administrasi KeuangandanPerbankan, Bisnis Digital,Branding,IlmuEkonomi, Ekonomi Syariah
                  </span>
                </td>
            </tr>
            <?php
             } 
            ?>
          </tbody>
        </table> -->
      </div>
    </div>
  </main>
</body>

</html>
