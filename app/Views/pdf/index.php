<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="EduBridge">
  <meta name="author" content="EduBridge">
  <title>PIPE Psikotest</title>
  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/nucleo/css/nucleo.css'); ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css'); ?>" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/css/argon.css?v=1.2.0'); ?>" type="text/css">
  <!-- Datatable -->
  <link rel="stylesheet" href="<?php echo base_url('src/node_modules/mdbootstrap/css/addons/datatables2.min.css'); ?>" type="text/css">

  


<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">


  <!-- Radio image -->
  <style type="text/css">
    .input-hidden {
      position: absolute;
      left: -9999px;
    }
    input[type=radio]:checked + label>img {
      /*border: 1px solid #fff;*/
      box-shadow: 0 0 1px 1px #9C020E;
    }

    /* Stuff after this is only to make things more pretty */
    input[type=radio] + label>img {
      border: 0.5px dashed #ECEFF5;
      width: 150px;
      height: 150px;
      transition: 500ms all;
    }

  /*<!-- sempitkan tabel summary -->*/
  .card .sempit td, .card .sempit th {
    padding-right: 0rem;
    padding-left: 0rem;
}
  </style>

</head>

<body>

  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Main Content -->
    <div>
      <?php 
      if (isset($template)) echo view($template); 
    ?>
    </div>
    
      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
              &copy; <?php echo date("Y"); ?> <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">EduBridge</a>
            </div>
          </div>
         <!--  <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">EduBridge</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
              </li>
            </ul>
          </div> -->
        </div>
      </footer>
    </div>
  </div>
  <!-- Vue js -->
  <script src="https://vuejs.org/js/vue.min.js"></script>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="<?php echo base_url('src/assets/vendor/jquery/dist/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/js-cookie/js.cookie.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js'); ?>"></script>
  <!-- Optional JS -->
  <script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.extension.js'); ?>"></script>
  <!-- Argon JS -->
  <script src="<?php echo base_url('src/assets/js/argon.js?v=1.2.0'); ?>"></script>
  <!-- Datatable -->
  <script src="<?php echo base_url('src/node_modules/mdbootstrap/js/addons/datatables2.min.js'); ?>" type="text/javascript"></script>
  <script type="text/javascript">
  $(document).ready(function () {
  $('#dtBasicExample').DataTable();
  $('.dataTables_length').addClass('bs-select');
});
</script>
<script type="text/javascript">
  var textareaValue = $('#summernote0').summernote('code');
  console.log(textareaValue);
  // save score 
  $(":input").bind('keyup mouseup', function(e){
        $jawaban = e.target.id;
        var score = e.target.value;
        var code = $jawaban.split("_");
        $link = window.location.href;
        var id_memberorderproductjenistes = $link.slice(-1);
        // todo
        
        var bagian_tes=code[0];
        var urutan_soal=code[1];
        var urutan_jawaban=code[2];
        // console.log(urutan_soal+"ada"+urutan_jawaban);
        $.ajax({
         url:'<?=base_url()?>/test/save_correction',
         method: 'post',
         data: {
          urutan_soal: urutan_soal, 
          urutan_jawaban: urutan_jawaban,
          score: score,
          bagian_tes: bagian_tes, 
          id_memberorderproductjenistes: id_memberorderproductjenistes},
         dataType: 'json',
         success: function(response){
        console.log(response);
         }
       });
    }); 

</script>
<!-- ini lola banget -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
  $('#summernote0').summernote('focus');
  $('#summernote1').summernote();
  $('#summernote2').summernote();
  $('#finalsummernote').summernote();

  // save summary
     $(function(){
         $('#save_summary0').submit(function(event){

           var messageData = $('#summernote0').summernote('code');
           var id_memberorderproductjenistes = $('#id_memberorderproductjenistes0').val();
           console.log(id_memberorderproductjenistes);

          event.preventDefault();
          $.ajax({
             url:'<?=base_url()?>/test/save_summary',
             method: 'post',
             data: {
              summary: messageData,
              id_memberorderproductjenistes: id_memberorderproductjenistes},
             dataType: 'json',
             success: function(response){
            console.log(response);
             }
           });
          });
         $('#save_summary1').submit(function(event){

           var messageData = $('#summernote1').summernote('code');
           var id_memberorderproductjenistes = $('#id_memberorderproductjenistes1').val();
           console.log(id_memberorderproductjenistes);

          event.preventDefault();
          $.ajax({
             url:'<?=base_url()?>/test/save_summary',
             method: 'post',
             data: {
              summary: messageData,
              id_memberorderproductjenistes: id_memberorderproductjenistes},
             dataType: 'json',
             success: function(response){
            console.log(response);
             }
           });
          });
         $('#save_summary2').submit(function(event){

           var messageData = $('#summernote2').summernote('code');
           var id_memberorderproductjenistes = $('#id_memberorderproductjenistes2').val();
           console.log(id_memberorderproductjenistes);

          event.preventDefault();
          $.ajax({
             url:'<?=base_url()?>/test/save_summary',
             method: 'post',
             data: {
              summary: messageData,
              id_memberorderproductjenistes: id_memberorderproductjenistes},
             dataType: 'json',
             success: function(response){
            console.log(response);
             }
           });
          });
         $('#save_finalsummary').submit(function(event){

           var messageData = $('#finalsummernote').summernote('code');
           var id_memberorderproduct = $('#id_memberorderproduct').val();
           console.log(messageData);

          event.preventDefault();
          $.ajax({
             url:'<?=base_url()?>/test/save_finalsummary',
             method: 'post',
             data: {
              summary: messageData,
              id_memberorderproduct: id_memberorderproduct},
             dataType: 'json',
             success: function(response){
            console.log(response);
             }
           });
          });
     });

});
</script>

<!-- chart -->
<script type="text/javascript">
  <?php
    foreach ($jenistes[0]['bagiantes'] as $key => $bagian) {
      $code[$key] = $bagian['konversi'][0]['code'];
      $sw[$key] = $bagian['konversi'][0]['sw'];
      # code...
    }
  ?>
  var swChart = (function() {
  // Variables
  var $chart = $('#chart-sw');
  // Methods
  function init($chart) {
    var swChart = new Chart($chart, {
      type: 'line',
      options: {
        scales: {
          yAxes: [{
            gridLines: {
              // lineWidth: 1,
              // pointBorderWidth: 2,
              // color: Charts.colors.gray[900],
              // zeroLineColor: Charts.colors.gray[900]
            },
            ticks: {
              callback: function(value) {
                if (!(value % 10)) {
                  return  value ;
                }
              }
            }
          }]
        },
        tooltips: {
          callbacks: {
            label: function(item, data) {
              var label = data.datasets[item.datasetIndex].label || '';
              var yLabel = item.yLabel;
              var content = '';

              if (data.datasets.length > 1) {
                content += '<span class="popover-body-label mr-auto">' + label + '</span>';
              }

              content += yLabel;
              return content;
            }
          }
        }
      },
      data: {
        labels: ["<?php echo implode('", "',$code) ?>"],
        datasets: [{
          label: 'Performance',
          data: [<?php echo implode(', ',$sw) ?>],
        }]
      }
    });
    // Save to jQuery object
    $chart.data('chart', swChart);
  };
  if ($chart.length) {
    init($chart);
  }

})();


</script>

</body>

</html> 