<div class="main-content">
    <!-- Header -->
  <div class="header pb-6 d-flex align-items-center" style="min-height: 500px; background-image: url(<?php echo base_url('src/assets/img/theme/paket-tes-kepribadian.jpg'); ?>); background-size: cover; background-position: center top; padding-top:50px;">
      <span class="mask bg-gradient-darker opacity-8"></span>

    <!-- <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9"> -->
      <div class="container">
        <div class="header-body text-center">
          <div class="row justify-content-center">
            <div class="col-lg-7 col-md-12" style="padding-bottom: 10px">
              <h1 class="text-white pt-5" style="font-size: 30px; font-weight: 600"><?= $product['nama']; ?>
                <?php 
                /*
                    if(isset($terlaris) && $terlaris=true){
                      ?>
                      <span class="badge badge-success badge-pill mb-1" style="font-size: 7pt"><?= lang('Global.terlaris') ?></span>
                      <?php
                    }*/
                    ?>
              </h1>
              <p class="text-white mt-0 mb-4 pb-4"><?= $product['short_desc']; ?></p><!-- 
              <a href="<?= base_url('cart/beli/'.$product['id']) ?>" class="btn btn-primary"><i class="fa fa-shopping-cart"></i><?= ' '.lang('Global.tambah_cart') ?></a> -->
             
            </div>
          </div>
        </div>
      </div>
      
    <!-- </div> -->
    </div>
    <div class="container-fluid mt--8">
      <div class="row">
        <div class="col-xl-12 order-xl-3">
        <!-- <div class="col-xl-4 order-xl-2"> -->
          <div class="card card-profile">
            <!-- <img src="<?php echo base_url('src/assets/img/theme/paket-tes-kepribadian.jpg'); ?>" alt="Image placeholder" class="card-img-top"> -->
            <!-- <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#">
                    <img src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>" class="rounded-circle">
                  </a>
                </div>
              </div>
            </div> -->
            <div class="card">
              <div class="card-body pb-5 pt-50" style="padding : 30px;">
                <div class="text-center">
                  <p class="description opacity-8"><?= $product['desc']; ?></p>
                  <div class="h1 mt-4 mb-4" style="font-weight: 600">
                    <i class="ni business_briefcase-24 mr-2"></i><!-- <del class="h5 opacity-5">Rp. 250.000</del> --> GRATIS 
                  </div>
                  <div class="row">
                    <div class="col-12 col-md-6 mb-3 mb-md-0">
                    <a href="<?=base_url('dashboard')?>" class="btn btn-primary" style="width: 100%"> <!-- <?= lang('Global.tambah_cart') ?> --> IKUT TES GRATIS</a>
                    </div>
                    <div class="col-12 col-md-6">
                    <a href="whatsapp://send?text=Check this link <?=base_url('product/id/'.$product['id'])?>" onclick="shared()" class="btn btn-secondary" style="width: 100%"><i class="fa fa-share"></i> <?= lang('Global.bagikan') ?></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center">
                    <div>
                      <span class="heading"><?=$product['viewed']?></span>
                      <span class="description"><?= lang('Global.dilihat') ?></span>
                    </div>
                    <div>
                      <span class="heading"><?=$ordered[$product['id']]?></span>
                      <span class="description"><?= lang('Global.peserta') ?></span>
                    </div>
                    <div>
                      <span class="heading"><?= $product['shared'] ?></span>
                      <span class="description"><?= lang('Global.dibagikan') ?></span>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="col-xl-8 order-xl-1" hidden="">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-12">
                  <h3 class="mb-0">Detail <?= $product['nama']; ?></h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form>
                <h6 class="heading-small text-muted mb-4"><?= lang('Global.deskripsi') ?></h6>
                <div class="pl-lg-4">
                  <div class="row">
                      <p class="description opacity-8"><?= $product['desc']; ?></p>
                    <div class="col-md-12" hidden="">
                      <div class="text-left">
                        <p class="description opacity-8">Paket terdiri dari :  
                        <br><b><i class="ni ni-check-bold"></i> Tes Kepribadian</b>
                        <br><b><i class="ni ni-check-bold"></i> Hasil tes dari Psikolog</b>
                        <br><b><i class="ni ni-check-bold"></i> Konsultasi langsung dengan Psikolog</b>
                        </p>
                      </div>
                      <p class="description opacity-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                      <p class="description opacity-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                      
                    </div>                    
                  </div>
                </div>
                <div hidden="">
                <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-muted mb-4">Apa yang didapat</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="description opacity-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                      
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Description -->
                <h6 class="heading-small text-muted mb-4">Persyaratan</h6>
                <div class="pl-lg-4">
                    <p class="description opacity-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
                <hr class="my-4" />
                <h6 class="heading-small text-muted mb-4">Lain lain</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="description opacity-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="section features-1" style="background-color: #232941; color: #FDC834">
      <div class="container">
        <div class="text-center" style="padding-top: 5px; padding-bottom: 12px; ">
            <!-- <h4 class="display-4" style="color: #FDC834; font-weight: 500"><?=lang('Global.lihat_semua_paket')?></h4> -->

            <h3 class="display-3" style="color: #FDC834; font-weight: 500">MENU LAYANAN TES PSIKOTES GRATIS</h3>
        </div>
        <div class="row mb-2">
          <?php if(!empty($products)) {
            foreach($products as $key => $item) { 
                if ($item['id']==1){ ?>
                <div class="col-lg-8 mx-auto pt-4 text-center d-flex flex-column" style="padding-top: 12px; padding-bottom: 24px;">
                  <div class="info">
                    <span class="avatar avatar-lg rounded-circle">
                    <img alt="Image placeholder" src="<?= base_url('src/assets/img/product/'.$item['id'].'.png') ?>">
                  </span>
                    <a href="#menu-test"><h6 class="info-title text-uppercase" style="color: #FDC834"><?= $item['nama']; ?></h6></a>
                    <p class="description opacity-8" style="font-weight: 400"><?= $item['short_desc']; ?></p>
                  </div>
                  <br>
                  <a href="#menu-test">
                    <?php if ($item['id'] > 1): ?>
                      <a href="#menu-test" class="mt-auto btn btn-primary btn-icon mt-3 mb-sm-0">
                    <?php endif ?>
                    <?php if ($item['id'] == 1): ?>
                      <a href="<?php echo base_url('product/id/'.$item['id']) ?>" class="mt-auto btn btn-primary btn-icon mt-3 mb-sm-0">
                    <?php endif ?>
                      <!-- <span class="btn-inner--icon"><?=lang('Global.detail');?> --></span>

                      <span class="btn-inner--icon"> IKUT TES GRATIS
                      <!-- <span class="btn-inner--text">Learn more</span> -->
                    </a>
                </div>    
          <?php } }
          } ?>
        </div>
        <div class="container">
          
        </div>
      </div>
    </div>
        
</div>
    