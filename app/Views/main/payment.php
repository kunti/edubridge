<div class="main-content">
    <!-- Header -->
     <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 col-md-8 px-5">
              <h1 class="text-white"><?= lang('Global.payment_title') ?></h1>
              <p class="text-lead text-white"></p>
            </div>
          </div>
        </div>
      </div>
    <div class="header  d-flex align-items-center" style="min-height: 500px; background-color: #FFFFFF">

    <!-- <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9"> -->
      <div class="container">
        <div class="header-body  mb-7">
          <div class="row justify-content-center">
            <div class="col-lg-8 col-md-">
              <div class="row">
                <div class="col-md-8">
                  <div class="info">
                    <h3 class="info-title text-uppercase text-primary">Paket Lengkap</h3>
                    <p class="description opacity-8"><?= lang('Global.payment_desc') ?></p>

                    <p><?= lang('Global.payment_account') ?> : 7398273293752932</p>
                    <a href="<?php echo base_url('member/payment') ?>" class="text-primary"><?= lang('Global.lihat_riwayat_pembelian') ?>
                      <i class="ni ni-bold-right text-primary"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    <!-- </div> -->
    </div>
        
</div>
