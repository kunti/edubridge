
<div class="main-content">
    <!-- Header -->
     <div class="header bg-white pt-7 pt-lg-9">
      <div class="container">
        <div class="header-body text-center mt-2 mb-4 mb-lg-6">
          <div class="row justify-content-center">
            <div class="col-12">
              <h1 class="" style="font-weight: 600">Semua Paket Tes</h1>
            </div>
          </div>
        </div>
      </div>
    <div class="header bg-primary d-flex align-items-center" style="min-height: 500px; background-color: #FFFFFF;">

    <!-- <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9"> -->
      <div class="container">
        <div class="header-body text-center mb-5 mt-5">
          <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
             <div class="row">
          <?php if(!empty($products)) {
            foreach($products as $key => $item) { ?>
                <div class="col-lg-3 pt-4 text-center d-flex flex-column" style="padding-top: 12px; padding-bottom: 24px;">
                  <div class="info">
                    <span class="avatar avatar-lg rounded-circle">
                    <img alt="Image placeholder" src="<?= base_url('src/assets/img/product/'.$item['id'].'.png') ?>">
                  </span>
                    <a><h6 class="info-title text-uppercase" style="color: #FDC834"><?= $item['nama']; ?></h6></a>
                    <p class="description" style="font-weight: 400; color: #FDC834"><?= $item['short_desc']; ?></p>
                  </div>
                  <br>
                  <a href="#menu-test">
                    <?php if ($item['id'] > 1): ?>
                      <a href="#menu-test" class="mt-auto btn btn-primary btn-icon mt-3 mb-sm-0">
                    <?php endif ?>
                    <?php if ($item['id'] == 1): ?>
                      <a href="<?php echo base_url('product/id/'.$item['id']) ?>" class="mt-auto btn btn-primary btn-icon mt-3 mb-sm-0">
                    <?php endif ?>
                      <span class="btn-inner--icon"><?=lang('Global.detail');?></span>
                      <!-- <span class="btn-inner--text">Learn more</span> -->
                    </a>
                </div>    
          <?php }
          } ?>
        </div>
            </div>
          </div>
        </div>
      </div>
      
    <!-- </div> -->
    </div>
        
</div>
