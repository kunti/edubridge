  <!-- Navbar -->
  <nav  id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent headroom">
    <div class="container">
      <a class="navbar-brand mr-lg-5" href="<?php echo base_url(''); ?>">
        <img src="<?php echo base_url('src/assets/img/brand/pipe-yellow.png'); ?>">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
        <span  class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse collapse" id="navbar_global">
        <div class="navbar-collapse-header">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="<?php echo base_url(''); ?>">
                <img src="<?php echo base_url('src/assets/img/brand/pipe-blue.png'); ?>">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
          <li class="nav-item dropdown d-none">
            <a href="<?= base_url('Tesiqdanpenjurusangratis') ?>" class="nav-link" role="button">
              <i class="ni ni-istanbul d-lg-none"></i>
              <span class="nav-link-inner--text"><?=lang('Global.tesgratis');?></span>
            </a>
          </li> 
          <li class="nav-item dropdown dropdown-desktop d-none">
            <a href="#menu-test" class="nav-link"  role="button">
              <i class="ni ni-app d-lg-none"></i>
              <span  class="nav-link-inner--text"><?=lang('Global.paket_tes');?></span>
            </a>
            <div class="dropdown-menu dropdown-menu-xl">
              <div class="dropdown-menu-inner pt-0 pb-2">
                <?php if(!empty($products)) {
                  foreach($products as $key => $item) { ?>
                    <a href="" class="media d-flex align-items-center mt-0 mb-3">
                      
                      <!-- <?php echo base_url('product/id/'.$item['id']) ?> -->
                      <?php if ($item['id'] > 1): ?>
                        <a href="" class=" media d-flex align-items-center mt-0 pt-0 mb-1">
                      <?php endif ?>
                      <?php if ($item['id'] == 1): ?>
                        <a href="<?php echo base_url('product/id/'.$item['id']) ?>" class=" media d-flex align-items-center mt-0 pt-0 mb-1">
                      <?php endif ?>
                      
                    
                      <span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?= base_url('src/assets/img/product/'.$item['id'].'.png') ?>">
                  </span>
                      <div class="media-body ml-3">
                        <h6 class="heading text-primary mb-md-1" style="font-weight: 700"><?= $item['nama']; ?></h6>
                        <p class="description d-none d-md-inline-block mb-0" style="font-weight: 400"><?= $item['short_desc']; ?></p>
                      </div>
                    </a>
                <?php }
                } ?>
              </div>
            </div> 
          </li>
          <li class="nav-item dropdown dropdown-mobile d-none">
            <a href="" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="ni ni-app d-lg-none"></i>
              <span  class="nav-link-inner--text"><?=lang('Global.paket_tes');?></span>
            </a>
            <div class="dropdown-menu pt-0 mt-0 mb-3" aria-labelledby="navbarDropdown">
              <div class="dropdown-menu-inner mt-0 pt-0">
                <?php if(!empty($products)) {
                  foreach($products as $key => $item) { ?>
                    <a href="" class="media d-flex align-items-center mt-0 mb-3">
                      
                      <!-- <?php echo base_url('product/id/'.$item['id']) ?> -->
                      <?php if ($item['id'] > 1): ?>
                        <a href="" class=" media d-flex align-items-center mt-0 pt-0 mb-1">
                      <?php endif ?>
                      <?php if ($item['id'] == 1): ?>
                        <a href="<?php echo base_url('product/id/'.$item['id']) ?>" class=" media d-flex align-items-center mt-0 pt-0 mb-1">
                      <?php endif ?>
                      
                    
                      <span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?= base_url('src/assets/img/product/'.$item['id'].'.png') ?>">
                  </span>
                      <div class="media-body ml-3">
                        <h6 class="heading text-primary mb-md-1" style="font-weight: 700"><?= $item['nama']; ?></h6>
                        <p class="description d-none d-md-inline-block mb-0" style="font-weight: 400"><?= $item['short_desc']; ?></p>
                      </div>
                    </a>
                <?php }
                } ?>
              </div>
            </div>
          </li>
          <li class="nav-item dropdown d-none">
            <a href="" class="nav-link" role="button">
              <i class="ni ni-collection d-lg-none"></i>
              <span class="nav-link-inner--text"><?=lang('Global.tentang');?></span>
            </a>
            <div class="dropdown-menu">
               <a href="../examples/profile.html" class="dropdown-item" style="font-weight: 500">PIPE PSIKOTES</a>
               <a href="http://www.edubridge.co.id/" class="dropdown-item" style="font-weight: 500">EDUBRIDGE</a>
             </div>
          </li>
          <li class="nav-item dropdown d-none">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="ni ni-collection d-lg-none"></i>
              <span class="nav-link-inner--text"><?=lang('Global.tentang');?></span>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a href="../examples/profile.html" class="dropdown-item" style="font-weight: 500">PIPE PSIKOTES</a>
               <a href="http://www.edubridge.co.id/" class="dropdown-item" style="font-weight: 500">EDUBRIDGE</a>
            </div>
          </li>
          <!-- <li class="nav-item dropdown">
            <a href="http://www.edubridge.co.id/destination.php" class="nav-link" role="button">
              <i class="ni ni-istanbul d-lg-none"></i>
              <span class="nav-link-inner--text"><?=lang('Global.study_overseas');?></span>
            </a>
          </li> -->
        </ul>
        <ul class="navbar-nav align-items-lg-center ml-lg-auto">
          <!-- <li class="nav-item">
            <a class="nav-link nav-link-icon" href="https://www.facebook.com/CreativeTim/" target="_blank" data-toggle="tooltip" title="Like us on Facebook">
              <i class="fa fa-facebook-square"></i>
              <span class="nav-link-inner--text d-lg-none">Facebook</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="https://www.instagram.com/creativetimofficial" target="_blank" data-toggle="tooltip" title="Follow us on Instagram">
              <i class="fa fa-instagram"></i>
              <span class="nav-link-inner--text d-lg-none">Instagram</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="https://twitter.com/creativetim" target="_blank" data-toggle="tooltip" title="Follow us on Twitter">
              <i class="fa fa-twitter-square"></i>
              <span class="nav-link-inner--text d-lg-none">Twitter</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="https://github.com/creativetimofficial/argon-design-system" target="_blank" data-toggle="tooltip" title="Star us on Github">
              <i class="fa fa-github"></i>
              <span class="nav-link-inner--text d-lg-none">Github</span>
            </a>
          </li> -->
          <!-- <li class="nav-item">
            <a class="nav-link" href="<?= base_url('cart') ?>" role="button">
              <i class="ni ni-cart d-lg-none"></i>
              <span class="nav-link-inner--text"><?=lang('Global.my_cart');?></span>
            </a>
          </li> -->
          
            <?php
              if(!empty($_SESSION['identity'])){
                  ?>
                  <li class="nav-item">
                   <!--  <a class="nav-link nav-link-icon" href="<?= base_url('dashboard') ?>" data-toggle="tooltip" title="Dashboard">
                      <i class="ni ni-circle-08" style="font-size: 13px;"> &nbsp<span style="font-family: montserrat; font-size: 13px;"><?php if (isset($_SESSION['email'])) { echo $_SESSION['email']; } ?></span></i>
                      <span class="nav-link-inner--text d-lg-none">Dashboard</span>
                    </a> -->
                    <a href="<?= base_url('dashboard') ?>" class="btn btn-primary btn-icon mb-sm-0">
                    <span class="btn-inner--icon" style="font-size: 12px;"><?=lang('Global.btn-dashboard');?></span>
                    <!-- <span class="btn-inner--text">Learn more</span> -->
                  </a>
                  </li>
                  <?php 
              } else {
                ?>
                <li class="nav-item mb-2 mb-sm-0">
                <a href="<?= base_url('auth/login') ?>" class="nav-link" role="button">
                  <i class="ni ni-collection d-lg-none"></i>
                  <span class="nav-link-inner--text">LOGIN</span>
                </a>
                </li>
                <li class="nav-item">
                <a href="<?= base_url('Tesiqdanpenjurusangratis') ?>" class="btn btn-primary btn-icon mb-sm-0">
                    <span class="btn-inner--icon" style="font-size: 12px"><?=lang('Global.btn-register');?></span>
                    <!-- <span class="btn-inner--text">Learn more</span> -->
                  </a>
                  </li>

                <?php 
              }
            ?>
          
          <li class="nav-item dropdown mt-2 mb-2  mt-lg-0 mb-lg-0">
            
            <div class="dropdown-menu">

               <a href="../examples/profile.html" class="dropdown-item">PIPE PSIKOTES</a>
               <a href="http://www.edubridge.co.id/" class="dropdown-item">EDUBRIDGE</a>
             </div>
          </li>
           <li class="nav-item d-none">
                  <a class="nav-link nav-link-icon" href="
                    <?php if ($_SESSION['web_lang']=='en'){
                      echo base_url('frontlang/id');
                    } else {
                      echo base_url('frontlang/en');
                    } ?>
                  " >
                    <span class="nav-link-inner--text ">
                     <?=lang('Global.switch_lang');?>
                    </span>
                  </a>
          </li> 
        </ul>
      </div>
    </div>
  </nav>
<!-- End Navbar -->