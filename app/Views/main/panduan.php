<!doctype html>
<html lang="en" author="Agun Prabowo">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.iconify.design/1/1.0.5/iconify.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('src/assets/img/panduan-aplikasi/style-panduan.css') ?>">

    <!-- <link rel="icon" href=""> -->
    <title>PIPE Psikotes | Panduan Pengguna</title>
  </head>
  <body oncontextmenu="return false;" class="pl-3 pr-3">

  <section class="full-page col-md-8 col-lg-8 col-xl-6 mx-auto">

  <section class="opening text-center ml-0 pl-0">
    <h3 class="mt-5 mb-2" style="font-weight: 700">PIPE Psikotes</h3>
    <h5 class="mb-4">PANDUAN PENGGUNAAN BAGI PESERTA</h5>

    <div class="">
      <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/pipe-logo.png'); ?>" width="150">
    </div>

    <p class="mt-5 mb-5" style="font-weight: 600">Berikut kami lampirkan petunjuk penggunaan aplikasi tes:</p>
  </section>


  <div class="steps mb-5">
    <p>1.) Buka alamat website PIPE Psikotes yaitu 
        <a href="https://pipe-psikotes.co.id/">https://pipe-psikotes.co.id/</a></p>
    

    <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/homepage.jpg'); ?>" class="img-fluid">


    <p class="mt-4 mb-0">2.) Mengikuti tes</p>
    <p><b>Klik ‘IKUT TES’</b> pada halaman depan website PIPE Psikotes.</p>

    <p class="mt-4">3.) <b>Isikan semua data diri</b> Anda dengan sebenar-benarnya, kemudian klik <b>'DAFTAR TES GRATIS'</b></p>


    <div>
    <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/register1.jpg'); ?>" class="img-fluid"><br>
    <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/register2.jpg'); ?>" class="img-fluid"><br>
    <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/register3.jpg'); ?>" class="img-fluid"><br>
    <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/register4.jpg'); ?>" class="img-fluid"><br>
    </div>

    <p class="mt-4 mb-3">4.) Layanan tes akan otomatis tampil di halaman akun Anda, yang terdiri dari <b> TES KECERDASAN, TES MINAT dan TES KEPRIBADIAN.</b> Sebelum mengerjakan tes, Anda diharuskan untuk mengklik tombol <b>'Baca'</b> pada kotak Tata Tertib.</p>

    <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/dashboard1.jpg'); ?>" class="img-fluid"><br>
    <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/dashboard2.jpg'); ?>" class="img-fluid"><br>


    <p class="mt-4 mb-3">5.) Setelah membaca dan memahami keseluruhan dari Tata Tertib, Anda bisa mengklik tombol <b>'Saya telah membaca dan setuju'.</b></p>

    <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/tatatertib1.jpg'); ?>" class="img-fluid"><br>
    <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/tatatertib2.jpg'); ?>" class="img-fluid"><br>

    <p class="mt-4 mb-0">6.) Selanjutnya Anda bisa memulai tes dengan mengklik tombol <b>'Mulai tes'</b>, pengerjaan tes dimulai dari <b>Tes Kecerdasan</b> dan dilanjutkan dengan <b>Tes Minat</b> lalu <b>Tes Kepribadian</b> secara berurutan.</p>

    <p class="mt-4 mb-0">7.) Pengenalan tampilan tes:</p>

    <ul class="mt-3">
      <li> Untuk <b>TES KECERDASAN</b>, Anda <b>diberi waktu per-bagian</b>. Jika Anda belum selesai mengerjakan semua soal namun waktu sudah habis, maka akan muncul peringatan berikut untuk melanjutkan ke bagian setelahnya dan <b>Anda tidak dapat lanjut mengerjakan bagian yang waktunya sudah habis.</b></li>
      <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/timerout.jpg'); ?>" class="mt-3 img-fluid">
      <li class="mt-4"> Pada bagian kiri terdapat progress bar yang menunjukkan jumlah soal yang telah Anda jawab per semua soal pada tes tersebut. Misal pada TES KECERDASAN tertulis 8/128 artinya Anda telah mengerjakan 8 soal dari 128 soal.
      </li>
      <li class="mt-3"> Terdapat juga tombol untuk menghubungi tim PIPE Psikotes (di bagian pojok kanan bawah) jika terjadi kendala teknis saat mengerjakan tes.</li>
      <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/kontak-pipe.jpg'); ?>" class="mt-3 img-fluid img-thumbnail">
    </ul>

    <p class="mt-4 mb-0">8.) Bagian tes pertama:</p>

    <ul class="mt-3">
      <li> Pada setiap bagian tes akan muncul <b>petunjuk soal</b>, silahkan cermati petunjuk soal tersebut, kemudian klik <b>'Saya telah memahami petunjuk'</b>.</li>
      <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/petunjuk-umum.jpg'); ?>" class="mt-3 img-fluid">

      <li class="mb-3"> Apabila Anda ingin membaca petunjuk soal kembali, silahkan klik <b>'Lihat Petunjuk'</b> pada atas kiri layar.</li>
      <li> Anda dapat mulai mengerjakan soal bagian 1. Pastikan jumlah soal yang terjawab sesuai dengan progress bar yang berada di sebelah kiri layar. Setelah selesai, klik Bagian 2 dan <b>kerjakan hingga semua bagian selesai.</b></li>
      <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/bagiantes.jpg'); ?>" class="mt-3 img-fluid">
    </ul>


    <p class="mt-4 mb-0">9.) Cara menjawab soal:</p>

    <ul class="mt-3">
      <li> Soal pilihan ganda (misal pada tes bagian 1). <br> Untuk menjawab soal pilihan ganda, klik pada pilihan yang dikehendaki. Anda dapat klik tepat pada text atau pada icon bulat maka jawaban Anda otomatis tersimpan dan menambah 1 progress bar.</li>
      <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/contohsoalbag1.jpg'); ?>" class="mt-3 img-fluid">

      <li class="mt-4"> Soal isian (misal pada tes bagian 4). <br> Untuk menjawab soal isian, silahkan klik pada kotak input kemudian ketikkan jawaban Anda. Jika Anda klik pada kotak input lainnya atau Anda klik Tab untuk menyorot kotak input selanjutnya, maka jawaban Anda otomatis tersimpan dan menambah 1 progress bar.</li>
      <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/contohsoalbag4.jpg'); ?>" class="mt-3 img-fluid">

      <li class="mt-4"> Soal gambar. <br> Untuk menjawab soal gambar, klik pada pilihan yang dikehendaki. Anda dapat klik tepat pada gambar atau pada icon bulat maka jawaban Anda otomatis tersimpan dan menambah 1 progress bar.</li>
      <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/contohsoalbag7.jpg'); ?>" class="mt-3 img-fluid">
    </ul>

    <p class="mt-4 mb-0">10.) Setelah semua selesai Anda dapat keluar dari tes dengan klik tombol <b>'Akhiri tes'</b> pada pojok kanan bawah layar Anda (Tombol <b>Akhiri tes</b> akan muncul hanya saat dibagian terakhir dari tes dan dapat Anda klik apabila telah selesai mengerjakan semua soal, meskipun waktu Anda masih tersisa).</p>

    <p class="mt-4 mb-0">11.) Setelah selesai mengerjakan semua bagian tes, <b>Anda tidak dapat mengerjakan tes kembali.</b></p>

    <p class="mt-4 mb-0">12.) Selanjutnya psikolog kami akan mengolah data hasil asesmen agar dapat dipahami hasilnya. Hasil psikotes akan keluar kurang lebih dalam waktu 30 hari dan akan <b>diinfokan melalui notifikasi email</b> (berdasarkan email yang didaftarkan) <b>untuk mengunduh hasil tes.</b></p>

    <img src="<?php echo base_url('src/assets/img/panduan-aplikasi/downloadhasil.jpg'); ?>" class="mt-3 img-fluid">

  </div>
  
  <div class="text-center mb-5">
   <a href="https://pipe-psikotes.co.id/Tesiqdanpenjurusangratis">
    <button class="btn" style="color: #232941; background-color: #FCBD0F; font-weight: 600">Kembali</button>
   </a>
  </div>



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  </body>
</html>