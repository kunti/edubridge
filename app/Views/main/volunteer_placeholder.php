<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="EduBridge">
  <meta name="author" content="EduBridge">
  <title>PIPE Psikotest</title>
  
  <link rel="shortcut icon" type="image/png" href="<?php echo base_url('src/assets/img/brand/favicon.png'); ?>">

  <!-- Favicon -->
  <link rel="icon" href="<?php echo base_url('src/assets/img/brand/favicon.png" type="image/png'); ?>">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/nucleo/css/nucleo.css'); ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css'); ?>" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/css/argon.css?v=1.2.0'); ?>" type="text/css">
    <!-- CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets_landing/css/argon-design-system.css'); ?>" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel='stylesheet'>
    <!-- STYLE TAMBAHAN -->
  <link rel="stylesheet" href="<?php echo base_url('src/style_tambahan/style-tambahan.css'); ?>" type="text/css">
  
  <!-- select picker  -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  


</head>
<!-- style select2 -->
<style>
.was-validated select.form-control:invalid + .select2-container> span.selection> span.select2-selection {
  border-color: #fb6340;
        border-left-color: rgb(251, 99, 64);
    padding-right: calc(1.5em + 1.25rem);
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23fb6340' viewBox='-2 -2 7 7'%3e%3cpath stroke='%23fb6340' d='M0 0l3 3m0-3L0 3'/%3e%3ccircle r='.5'/%3e%3ccircle cx='3' r='.5'/%3e%3ccircle cy='3' r='.5'/%3e%3ccircle cx='3' cy='3' r='.5'/%3e%3c/svg%3E");
    background-repeat: no-repeat;
    background-position: center right calc(0.375em + 0.3125rem);
    background-size: calc(0.75em + 0.625rem) calc(0.75em + 0.625rem); 
}

.was-validated select.form-control:invalid + .select2-container--focus> span.selection> span.select2-selection {
  border-color: #fb6340;
        border-left-color: rgb(251, 99, 64);
    padding-right: calc(1.5em + 1.25rem);
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23fb6340' viewBox='-2 -2 7 7'%3e%3cpath stroke='%23fb6340' d='M0 0l3 3m0-3L0 3'/%3e%3ccircle r='.5'/%3e%3ccircle cx='3' r='.5'/%3e%3ccircle cy='3' r='.5'/%3e%3ccircle cx='3' cy='3' r='.5'/%3e%3c/svg%3E");
    background-repeat: no-repeat;
    background-position: center right calc(0.375em + 0.3125rem);
    background-size: calc(0.75em + 0.625rem) calc(0.75em + 0.625rem);
}

.was-validated select.form-control:valid + .select2-container> span.selection> span.select2-selection {
  border-color: #2dce89;
padding-right: calc(1.5em + 1.25rem);
background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3cpath fill='%232dce89' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/%3e%3c/svg%3e");
background-repeat: no-repeat;
background-position: center right calc(0.375em + 0.3125rem);
background-size: calc(0.75em + 0.625rem) calc(0.75em + 0.625rem);
}

.was-validated select.form-control:valid + .select2-container--focus> span.selection> span.select2-selection {
  border-color: #2dce89;
padding-right: calc(1.5em + 1.25rem);
background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3cpath fill='%232dce89' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/%3e%3c/svg%3e");
background-repeat: no-repeat;
background-position: center right calc(0.375em + 0.3125rem);
background-size: calc(0.75em + 0.625rem) calc(0.75em + 0.625rem);
}
  .select2-container .select2-selection--single {
    display: block;
    width: 100%;
    height: calc(1.5em + 1.25rem + 2px);
    padding: 0.625rem 0.75rem;
    font-size: 0.875rem;
    font-weight: 400;
    line-height: 1.5;
    color: #8898aa;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #cad1d7;
    border-radius: 0.25rem;
    box-shadow: none;
    font-family: Montserrat;
}

.select2-container--classic .select2-selection--single .select2-selection__rendered {
    color: #8898aa;
    line-height: 24px;
}
.select2-container--classic .select2-selection--single .select2-selection__rendered {
    color: #8898aa;
    line-height: 24px;
}



</style>
<style type="text/css">
  body {
  font-family: 'Montserrat';
}
</style>

<body class="bg-default">

<!-- Awal Navbar -->
 <?php include('navbar.php') ?> 
<!-- Akhir Navbar -->


  <!-- Main content -->
    <div class="main-content pt-5">
    <!-- Header -->
    <div class="header py-7 py-lg-8 pt-lg-7" style="min-height: 500px; background-image: url(<?php echo base_url('src/assets/img/brand/bg-header.jpg'); ?>); background-size: cover; background-position: center top; padding-top:50px;">
      <div class="container">
        <div class="header-body text-center mb-5">
          <div class="row justify-content-center">
            <div class="col-12 col-md-8">
              <span class="">
                <img alt="Image placeholder" style="height: 150px; width: 150px;" src="<?= base_url('src/assets/img/pdf/pipe-color.png') ?>">
              </span>
              <h2 class="mt-2" style="font-weight: 600">Tes IQ dan Penjurusan Gratis</h2>
              <h5 class="mt-4 mt-sm-0" style="font-weight: 500">Salam professional, </h5>
              <p class="text-center pl-2 pr-2" style="font-weight: 400">Guna penelitian dan kepentingan pendidikan, kami menawarkan rekan-rekan untuk berpartisiasi dalam penelitian kami. Anda akan mendapatkan informasi mengenai tingkat kecerdasan (IQ) dan minat karir Anda melalui PIPE Psikotest yang sedang kami kembangkan.</p>
            </div>
            <!-- <div class="col-xl-5 col-lg-6 col-md-8 px-5">
              <h1 class="">Syarat</h1>
            <ul class=" text-left">
                <li>Laki-laki maupun perempuan</li>
                <li>Berusia 13-25 tahun</li>
                <li>Dapat mengerjakan paket tes berikut. Anda tidak akan dikenakan biaya jika mendaftar sebagai volunteer</li><a class="btn btn-sm btn-primary" href="<?= base_url('product/id/1') ?>">Lihat paket</a>
                <li>Menulisan feedback</li>
            </ul>
            </div> -->

            <div class="col-md-12">
              <h3 class="mb-4 mt-3" style="font-weight: 600">Benefit</h3>
            </div>
            <style>
            .card-new {
                    margin: 0 auto; /* Added */
                    float: none; /* Added */
                    margin-bottom:10px;
                    height:225px;
                    width: 100% !important;
            }
                </style>
            </style>
            <div class="row col-md-8">
              <div class="col-lg-6 text-center mb-2 mb-lg-0">
                <div class="card card-new text-center" style="width: 10rem; background-color: #232941;">
                  <div class="card-body">
                  <img style="width: 5rem" class="card-img-top mt-3 mt-lg-0 mt-xl-2" src="<?= base_url('src/assets/img/product/1.png') ?>" alt="Card image cap">
                    <p class="pt-3" style="color: #FDC834">Hasil tes tingkat kecerdasan (IQ) Anda</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="card card-new text-center" style="width: 10rem; background-color: #232941;">
                  <div class="card-body">
                  <img style="width: 5rem" class="card-img-top mt-3 mt-lg-0 mt-xl-2" src="<?= base_url('src/assets/img/product/3.png') ?>" alt="Card image cap">
                    <p class="pt-3" style="color: #FDC834">Hasil tes minat karir Anda</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-12 col-md-8" style="margin-top:24px;">
              <h3 class="mt-1" style="font-weight: 600">Cara mendaftar</h3>
              <p class="text-center pt-1 pl-2 pr-2" style="font-weight: 400">Silahkan lengkapi data diri Anda pada form di bawah ini dengan sebenar-benarnya dengan mengikuti langkah-langkah pada panduan berikut: <br>
              <a class="btn btn-sm btn-primary mt-3" target="_blank" href="https://docs.google.com/document/d/15eCM8KVIzKiTmPHUb1wVKgv6wsuQlRq2a0HJKngAyic/edit?usp=sharing">Lihat panduan menggunakan aplikasi</a></p>
            </div>
          </div>
        </div>
      </div>

    </div>


    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <!-- Table -->
      <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
          <div class="card bg-secondary border-0">
            <!-- <div class="card-header bg-transparent pb-5">
              <div class="text-muted text-center mt-2 mb-4"><small>Sign up with</small></div>
              <div class="text-center">
                <a href="#" class="btn btn-neutral btn-icon mr-4">
                  <span class="btn-inner--icon"><img src="../assets/img/icons/common/github.svg"></span>
                  <span class="btn-inner--text">Github</span>
                </a>
                <a href="#" class="btn btn-neutral btn-icon">
                  <span class="btn-inner--icon"><img src="../assets/img/icons/common/google.svg"></span>
                  <span class="btn-inner--text">Google</span>
                </a>
              </div>
            </div> -->
            
            <div class="card-body px-lg-5 py-lg-5">
              <small>Perhatian, harap diisi dengan benar, * wajib diisi</small>
              <br>
              <br>
              <form role="form" action="<?= base_url('auth/create_user'); ?>" method="post" accept-charset="utf-8" class="needs-validation" novalidate>
                <p style="font-size: 14px;"><b><?=lang('Global.identity');?></b></p>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class=""></i></span>
                    </div>
                    <input class="form-control" placeholder="<?=lang('Global.first_name');?> *" type="text" id="first_name" name="first_name" value="" required>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class=""></i></span>
                    </div>
                    <input class="form-control" placeholder="<?=lang('Global.last_name');?> *" type="text" id="last_name" name="last_name" value="" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputState" style="font-size: 12px; color: #8898aa">Kota lahir *</label>                  
                  <select required="" id="kotalahir" class="form-control js-example-basic-single" name="kotalahir" >
                  <!-- <select id="inputState" class="form-control js-example-basic-single" name="kotalahir" required="" tabindex="-1" aria-hidden="true" required> -->
                    <option value="" selected="">Silahkan pilih...</option>
                    <option value="Kabupaten simeulue"> KABUPATEN SIMEULUE</option>
                    <option value="Kabupaten aceh singkil"> KABUPATEN ACEH SINGKIL</option>
                    <option value="Kabupaten aceh selatan"> KABUPATEN ACEH SELATAN</option>
                    <option value="Kabupaten aceh tenggara"> KABUPATEN ACEH TENGGARA</option>
                    <option value="Kabupaten aceh timur"> KABUPATEN ACEH TIMUR</option>
                    <option value="Kabupaten aceh tengah"> KABUPATEN ACEH TENGAH</option>
                    <option value="Kabupaten aceh barat"> KABUPATEN ACEH BARAT</option>
                    <option value="Kabupaten aceh besar"> KABUPATEN ACEH BESAR</option>
                    <option value="Kabupaten pidie"> KABUPATEN PIDIE</option>
                    <option value="Kabupaten bireuen"> KABUPATEN BIREUEN</option>
                    <option value="Kabupaten aceh utara"> KABUPATEN ACEH UTARA</option>
                    <option value="Kabupaten aceh barat daya"> KABUPATEN ACEH BARAT DAYA</option>
                    <option value="Kabupaten gayo lues"> KABUPATEN GAYO LUES</option>
                    <option value="Kabupaten aceh tamiang"> KABUPATEN ACEH TAMIANG</option>
                    <option value="Kabupaten nagan raya"> KABUPATEN NAGAN RAYA</option>
                    <option value="Kabupaten aceh jaya"> KABUPATEN ACEH JAYA</option>
                    <option value="Kabupaten bener meriah"> KABUPATEN BENER MERIAH</option>
                    <option value="Kabupaten pidie jaya"> KABUPATEN PIDIE JAYA</option>
                    <option value="Kota banda aceh"> KOTA BANDA ACEH</option>
                    <option value="Kota sabang"> KOTA SABANG</option>
                    <option value="Kota langsa"> KOTA LANGSA</option>
                    <option value="Kota lhokseumawe"> KOTA LHOKSEUMAWE</option>
                    <option value="Kota subulussalam"> KOTA SUBULUSSALAM</option>
                    <option value="Kabupaten nias"> KABUPATEN NIAS</option>
                    <option value="Kabupaten mandailing natal"> KABUPATEN MANDAILING NATAL</option>
                    <option value="Kabupaten tapanuli selatan"> KABUPATEN TAPANULI SELATAN</option>
                    <option value="Kabupaten tapanuli tengah"> KABUPATEN TAPANULI TENGAH</option>
                    <option value="Kabupaten tapanuli utara"> KABUPATEN TAPANULI UTARA</option>
                    <option value="Kabupaten toba samosir"> KABUPATEN TOBA SAMOSIR</option>
                    <option value="Kabupaten labuhan batu"> KABUPATEN LABUHAN BATU</option>
                    <option value="Kabupaten asahan"> KABUPATEN ASAHAN</option>
                    <option value="Kabupaten simalungun"> KABUPATEN SIMALUNGUN</option>
                    <option value="Kabupaten dairi"> KABUPATEN DAIRI</option>
                    <option value="Kabupaten karo"> KABUPATEN KARO</option>
                    <option value="Kabupaten deli serdang"> KABUPATEN DELI SERDANG</option>
                    <option value="Kabupaten langkat"> KABUPATEN LANGKAT</option>
                    <option value="Kabupaten nias selatan"> KABUPATEN NIAS SELATAN</option>
                    <option value="Kabupaten humbang hasundutan"> KABUPATEN HUMBANG HASUNDUTAN</option>
                    <option value="Kabupaten pakpak bharat"> KABUPATEN PAKPAK BHARAT</option>
                    <option value="Kabupaten samosir"> KABUPATEN SAMOSIR</option>
                    <option value="Kabupaten serdang bedagai"> KABUPATEN SERDANG BEDAGAI</option>
                    <option value="Kabupaten batu bara"> KABUPATEN BATU BARA</option>
                    <option value="Kabupaten padang lawas utara"> KABUPATEN PADANG LAWAS UTARA</option>
                    <option value="Kabupaten padang lawas"> KABUPATEN PADANG LAWAS</option>
                    <option value="Kabupaten labuhan batu selatan"> KABUPATEN LABUHAN BATU SELATAN</option>
                    <option value="Kabupaten labuhan batu utara"> KABUPATEN LABUHAN BATU UTARA</option>
                    <option value="Kabupaten nias utara"> KABUPATEN NIAS UTARA</option>
                    <option value="Kabupaten nias barat"> KABUPATEN NIAS BARAT</option>
                    <option value="Kota sibolga"> KOTA SIBOLGA</option>
                    <option value="Kota tanjung balai"> KOTA TANJUNG BALAI</option>
                    <option value="Kota pematang siantar"> KOTA PEMATANG SIANTAR</option>
                    <option value="Kota tebing tinggi"> KOTA TEBING TINGGI</option>
                    <option value="Kota medan"> KOTA MEDAN</option>
                    <option value="Kota binjai"> KOTA BINJAI</option>
                    <option value="Kota padangsidimpuan"> KOTA PADANGSIDIMPUAN</option>
                    <option value="Kota gunungsitoli"> KOTA GUNUNGSITOLI</option>
                    <option value="Kabupaten kepulauan mentawai"> KABUPATEN KEPULAUAN MENTAWAI</option>
                    <option value="Kabupaten pesisir selatan"> KABUPATEN PESISIR SELATAN</option>
                    <option value="Kabupaten solok"> KABUPATEN SOLOK</option>
                    <option value="Kabupaten sijunjung"> KABUPATEN SIJUNJUNG</option>
                    <option value="Kabupaten tanah datar"> KABUPATEN TANAH DATAR</option>
                    <option value="Kabupaten padang pariaman"> KABUPATEN PADANG PARIAMAN</option>
                    <option value="Kabupaten agam"> KABUPATEN AGAM</option>
                    <option value="Kabupaten lima puluh kota"> KABUPATEN LIMA PULUH KOTA</option>
                    <option value="Kabupaten pasaman"> KABUPATEN PASAMAN</option>
                    <option value="Kabupaten solok selatan"> KABUPATEN SOLOK SELATAN</option>
                    <option value="Kabupaten dharmasraya"> KABUPATEN DHARMASRAYA</option>
                    <option value="Kabupaten pasaman barat"> KABUPATEN PASAMAN BARAT</option>
                    <option value="Kota padang"> KOTA PADANG</option>
                    <option value="Kota solok"> KOTA SOLOK</option>
                    <option value="Kota sawah lunto"> KOTA SAWAH LUNTO</option>
                    <option value="Kota padang panjang"> KOTA PADANG PANJANG</option>
                    <option value="Kota bukittinggi"> KOTA BUKITTINGGI</option>
                    <option value="Kota payakumbuh"> KOTA PAYAKUMBUH</option>
                    <option value="Kota pariaman"> KOTA PARIAMAN</option>
                    <option value="Kabupaten kuantan singingi"> KABUPATEN KUANTAN SINGINGI</option>
                    <option value="Kabupaten indragiri hulu"> KABUPATEN INDRAGIRI HULU</option>
                    <option value="Kabupaten indragiri hilir"> KABUPATEN INDRAGIRI HILIR</option>
                    <option value="Kabupaten pelalawan"> KABUPATEN PELALAWAN</option>
                    <option value="Kabupaten siak"> KABUPATEN SIAK</option>
                    <option value="Kabupaten kampar"> KABUPATEN KAMPAR</option>
                    <option value="Kabupaten rokan hulu"> KABUPATEN ROKAN HULU</option>
                    <option value="Kabupaten bengkalis"> KABUPATEN BENGKALIS</option>
                    <option value="Kabupaten rokan hilir"> KABUPATEN ROKAN HILIR</option>
                    <option value="Kabupaten kepulauan meranti"> KABUPATEN KEPULAUAN MERANTI</option>
                    <option value="Kota pekanbaru"> KOTA PEKANBARU</option>
                    <option value="Kota dumai"> KOTA DUMAI</option>
                    <option value="Kabupaten kerinci"> KABUPATEN KERINCI</option>
                    <option value="Kabupaten merangin"> KABUPATEN MERANGIN</option>
                    <option value="Kabupaten sarolangun"> KABUPATEN SAROLANGUN</option>
                    <option value="Kabupaten batang hari"> KABUPATEN BATANG HARI</option>
                    <option value="Kabupaten muaro jambi"> KABUPATEN MUARO JAMBI</option>
                    <option value="Kabupaten tanjung jabung timur"> KABUPATEN TANJUNG JABUNG TIMUR</option>
                    <option value="Kabupaten tanjung jabung barat"> KABUPATEN TANJUNG JABUNG BARAT</option>
                    <option value="Kabupaten tebo"> KABUPATEN TEBO</option>
                    <option value="Kabupaten bungo"> KABUPATEN BUNGO</option>
                    <option value="Kota jambi"> KOTA JAMBI</option>
                    <option value="Kota sungai penuh"> KOTA SUNGAI PENUH</option>
                    <option value="Kabupaten ogan komering ulu"> KABUPATEN OGAN KOMERING ULU</option>
                    <option value="Kabupaten ogan komering ilir"> KABUPATEN OGAN KOMERING ILIR</option>
                    <option value="Kabupaten muara enim"> KABUPATEN MUARA ENIM</option>
                    <option value="Kabupaten lahat"> KABUPATEN LAHAT</option>
                    <option value="Kabupaten musi rawas"> KABUPATEN MUSI RAWAS</option>
                    <option value="Kabupaten musi banyuasin"> KABUPATEN MUSI BANYUASIN</option>
                    <option value="Kabupaten banyu asin"> KABUPATEN BANYU ASIN</option>
                    <option value="Kabupaten ogan komering ulu selatan"> KABUPATEN OGAN KOMERING ULU SELATAN</option>
                    <option value="Kabupaten ogan komering ulu timur"> KABUPATEN OGAN KOMERING ULU TIMUR</option>
                    <option value="Kabupaten ogan ilir"> KABUPATEN OGAN ILIR</option>
                    <option value="Kabupaten empat lawang"> KABUPATEN EMPAT LAWANG</option>
                    <option value="Kabupaten penukal abab lematang ilir"> KABUPATEN PENUKAL ABAB LEMATANG ILIR</option>
                    <option value="Kabupaten musi rawas utara"> KABUPATEN MUSI RAWAS UTARA</option>
                    <option value="Kota palembang"> KOTA PALEMBANG</option>
                    <option value="Kota prabumulih"> KOTA PRABUMULIH</option>
                    <option value="Kota pagar alam"> KOTA PAGAR ALAM</option>
                    <option value="Kota lubuklinggau"> KOTA LUBUKLINGGAU</option>
                    <option value="Kabupaten bengkulu selatan"> KABUPATEN BENGKULU SELATAN</option>
                    <option value="Kabupaten rejang lebong"> KABUPATEN REJANG LEBONG</option>
                    <option value="Kabupaten bengkulu utara"> KABUPATEN BENGKULU UTARA</option>
                    <option value="Kabupaten kaur"> KABUPATEN KAUR</option>
                    <option value="Kabupaten seluma"> KABUPATEN SELUMA</option>
                    <option value="Kabupaten mukomuko"> KABUPATEN MUKOMUKO</option>
                    <option value="Kabupaten lebong"> KABUPATEN LEBONG</option>
                    <option value="Kabupaten kepahiang"> KABUPATEN KEPAHIANG</option>
                    <option value="Kabupaten bengkulu tengah"> KABUPATEN BENGKULU TENGAH</option>
                    <option value="Kota bengkulu"> KOTA BENGKULU</option>
                    <option value="Kabupaten lampung barat"> KABUPATEN LAMPUNG BARAT</option>
                    <option value="Kabupaten tanggamus"> KABUPATEN TANGGAMUS</option>
                    <option value="Kabupaten lampung selatan"> KABUPATEN LAMPUNG SELATAN</option>
                    <option value="Kabupaten lampung timur"> KABUPATEN LAMPUNG TIMUR</option>
                    <option value="Kabupaten lampung tengah"> KABUPATEN LAMPUNG TENGAH</option>
                    <option value="Kabupaten lampung utara"> KABUPATEN LAMPUNG UTARA</option>
                    <option value="Kabupaten way kanan"> KABUPATEN WAY KANAN</option>
                    <option value="Kabupaten tulangbawang"> KABUPATEN TULANGBAWANG</option>
                    <option value="Kabupaten pesawaran"> KABUPATEN PESAWARAN</option>
                    <option value="Kabupaten pringsewu"> KABUPATEN PRINGSEWU</option>
                    <option value="Kabupaten mesuji"> KABUPATEN MESUJI</option>
                    <option value="Kabupaten tulang bawang barat"> KABUPATEN TULANG BAWANG BARAT</option>
                    <option value="Kabupaten pesisir barat"> KABUPATEN PESISIR BARAT</option>
                    <option value="Kota bandar lampung"> KOTA BANDAR LAMPUNG</option>
                    <option value="Kota metro"> KOTA METRO</option>
                    <option value="Kabupaten bangka"> KABUPATEN BANGKA</option>
                    <option value="Kabupaten belitung"> KABUPATEN BELITUNG</option>
                    <option value="Kabupaten bangka barat"> KABUPATEN BANGKA BARAT</option>
                    <option value="Kabupaten bangka tengah"> KABUPATEN BANGKA TENGAH</option>
                    <option value="Kabupaten bangka selatan"> KABUPATEN BANGKA SELATAN</option>
                    <option value="Kabupaten belitung timur"> KABUPATEN BELITUNG TIMUR</option>
                    <option value="Kota pangkal pinang"> KOTA PANGKAL PINANG</option>
                    <option value="Kabupaten karimun"> KABUPATEN KARIMUN</option>
                    <option value="Kabupaten bintan"> KABUPATEN BINTAN</option>
                    <option value="Kabupaten natuna"> KABUPATEN NATUNA</option>
                    <option value="Kabupaten lingga"> KABUPATEN LINGGA</option>
                    <option value="Kabupaten kepulauan anambas"> KABUPATEN KEPULAUAN ANAMBAS</option>
                    <option value="Kota batam"> KOTA BATAM</option>
                    <option value="Kota tanjung pinang"> KOTA TANJUNG PINANG</option>
                    <option value="Kabupaten kepulauan seribu"> KABUPATEN KEPULAUAN SERIBU</option>
                    <option value="Kota jakarta selatan"> KOTA JAKARTA SELATAN</option>
                    <option value="Kota jakarta timur"> KOTA JAKARTA TIMUR</option>
                    <option value="Kota jakarta pusat"> KOTA JAKARTA PUSAT</option>
                    <option value="Kota jakarta barat"> KOTA JAKARTA BARAT</option>
                    <option value="Kota jakarta utara"> KOTA JAKARTA UTARA</option>
                    <option value="Kabupaten bogor"> KABUPATEN BOGOR</option>
                    <option value="Kabupaten sukabumi"> KABUPATEN SUKABUMI</option>
                    <option value="Kabupaten cianjur"> KABUPATEN CIANJUR</option>
                    <option value="Kabupaten bandung"> KABUPATEN BANDUNG</option>
                    <option value="Kabupaten garut"> KABUPATEN GARUT</option>
                    <option value="Kabupaten tasikmalaya"> KABUPATEN TASIKMALAYA</option>
                    <option value="Kabupaten ciamis"> KABUPATEN CIAMIS</option>
                    <option value="Kabupaten kuningan"> KABUPATEN KUNINGAN</option>
                    <option value="Kabupaten cirebon"> KABUPATEN CIREBON</option>
                    <option value="Kabupaten majalengka"> KABUPATEN MAJALENGKA</option>
                    <option value="Kabupaten sumedang"> KABUPATEN SUMEDANG</option>
                    <option value="Kabupaten indramayu"> KABUPATEN INDRAMAYU</option>
                    <option value="Kabupaten subang"> KABUPATEN SUBANG</option>
                    <option value="Kabupaten purwakarta"> KABUPATEN PURWAKARTA</option>
                    <option value="Kabupaten karawang"> KABUPATEN KARAWANG</option>
                    <option value="Kabupaten bekasi"> KABUPATEN BEKASI</option>
                    <option value="Kabupaten bandung barat"> KABUPATEN BANDUNG BARAT</option>
                    <option value="Kabupaten pangandaran"> KABUPATEN PANGANDARAN</option>
                    <option value="Kota bogor"> KOTA BOGOR</option>
                    <option value="Kota sukabumi"> KOTA SUKABUMI</option>
                    <option value="Kota bandung"> KOTA BANDUNG</option>
                    <option value="Kota cirebon"> KOTA CIREBON</option>
                    <option value="Kota bekasi"> KOTA BEKASI</option>
                    <option value="Kota depok"> KOTA DEPOK</option>
                    <option value="Kota cimahi"> KOTA CIMAHI</option>
                    <option value="Kota tasikmalaya"> KOTA TASIKMALAYA</option>
                    <option value="Kota banjar"> KOTA BANJAR</option>
                    <option value="Kabupaten cilacap"> KABUPATEN CILACAP</option>
                    <option value="Kabupaten banyumas"> KABUPATEN BANYUMAS</option>
                    <option value="Kabupaten purbalingga"> KABUPATEN PURBALINGGA</option>
                    <option value="Kabupaten banjarnegara"> KABUPATEN BANJARNEGARA</option>
                    <option value="Kabupaten kebumen"> KABUPATEN KEBUMEN</option>
                    <option value="Kabupaten purworejo"> KABUPATEN PURWOREJO</option>
                    <option value="Kabupaten wonosobo"> KABUPATEN WONOSOBO</option>
                    <option value="Kabupaten magelang"> KABUPATEN MAGELANG</option>
                    <option value="Kabupaten boyolali"> KABUPATEN BOYOLALI</option>
                    <option value="Kabupaten klaten"> KABUPATEN KLATEN</option>
                    <option value="Kabupaten sukoharjo"> KABUPATEN SUKOHARJO</option>
                    <option value="Kabupaten wonogiri"> KABUPATEN WONOGIRI</option>
                    <option value="Kabupaten karanganyar"> KABUPATEN KARANGANYAR</option>
                    <option value="Kabupaten sragen"> KABUPATEN SRAGEN</option>
                    <option value="Kabupaten grobogan"> KABUPATEN GROBOGAN</option>
                    <option value="Kabupaten blora"> KABUPATEN BLORA</option>
                    <option value="Kabupaten rembang"> KABUPATEN REMBANG</option>
                    <option value="Kabupaten pati"> KABUPATEN PATI</option>
                    <option value="Kabupaten kudus"> KABUPATEN KUDUS</option>
                    <option value="Kabupaten jepara"> KABUPATEN JEPARA</option>
                    <option value="Kabupaten demak"> KABUPATEN DEMAK</option>
                    <option value="Kabupaten temanggung"> KABUPATEN TEMANGGUNG</option>
                    <option value="Kabupaten kendal"> KABUPATEN KENDAL</option>
                    <option value="Kabupaten batang"> KABUPATEN BATANG</option>
                    <option value="Kabupaten pekalongan"> KABUPATEN PEKALONGAN</option>
                    <option value="Kabupaten pemalang"> KABUPATEN PEMALANG</option>
                    <option value="Kabupaten tegal"> KABUPATEN TEGAL</option>
                    <option value="Kabupaten brebes"> KABUPATEN BREBES</option>
                    <option value="Kota magelang"> KOTA MAGELANG</option>
                    <option value="Kota surakarta"> KOTA SURAKARTA</option>
                    <option value="Kota salatiga"> KOTA SALATIGA</option>
                    <option value="Kota semarang"> KOTA SEMARANG</option>
                    <option value="Kota pekalongan"> KOTA PEKALONGAN</option>
                    <option value="Kota tegal"> KOTA TEGAL</option>
                    <option value="Kabupaten kulon progo"> KABUPATEN KULON PROGO</option>
                    <option value="Kabupaten bantul"> KABUPATEN BANTUL</option>
                    <option value="Kabupaten gunung kidul"> KABUPATEN GUNUNG KIDUL</option>
                    <option value="Kabupaten sleman"> KABUPATEN SLEMAN</option>
                    <option value="Kota yogyakarta"> KOTA YOGYAKARTA</option>
                    <option value="Kabupaten pacitan"> KABUPATEN PACITAN</option>
                    <option value="Kabupaten ponorogo"> KABUPATEN PONOROGO</option>
                    <option value="Kabupaten trenggalek"> KABUPATEN TRENGGALEK</option>
                    <option value="Kabupaten tulungagung"> KABUPATEN TULUNGAGUNG</option>
                    <option value="Kabupaten blitar"> KABUPATEN BLITAR</option>
                    <option value="Kabupaten kediri"> KABUPATEN KEDIRI</option>
                    <option value="Kabupaten malang"> KABUPATEN MALANG</option>
                    <option value="Kabupaten lumajang"> KABUPATEN LUMAJANG</option>
                    <option value="Kabupaten jember"> KABUPATEN JEMBER</option>
                    <option value="Kabupaten banyuwangi"> KABUPATEN BANYUWANGI</option>
                    <option value="Kabupaten bondowoso"> KABUPATEN BONDOWOSO</option>
                    <option value="Kabupaten situbondo"> KABUPATEN SITUBONDO</option>
                    <option value="Kabupaten probolinggo"> KABUPATEN PROBOLINGGO</option>
                    <option value="Kabupaten pasuruan"> KABUPATEN PASURUAN</option>
                    <option value="Kabupaten sidoarjo"> KABUPATEN SIDOARJO</option>
                    <option value="Kabupaten mojokerto"> KABUPATEN MOJOKERTO</option>
                    <option value="Kabupaten jombang"> KABUPATEN JOMBANG</option>
                    <option value="Kabupaten nganjuk"> KABUPATEN NGANJUK</option>
                    <option value="Kabupaten madiun"> KABUPATEN MADIUN</option>
                    <option value="Kabupaten magetan"> KABUPATEN MAGETAN</option>
                    <option value="Kabupaten ngawi"> KABUPATEN NGAWI</option>
                    <option value="Kabupaten bojonegoro"> KABUPATEN BOJONEGORO</option>
                    <option value="Kabupaten tuban"> KABUPATEN TUBAN</option>
                    <option value="Kabupaten lamongan"> KABUPATEN LAMONGAN</option>
                    <option value="Kabupaten gresik"> KABUPATEN GRESIK</option>
                    <option value="Kabupaten bangkalan"> KABUPATEN BANGKALAN</option>
                    <option value="Kabupaten sampang"> KABUPATEN SAMPANG</option>
                    <option value="Kabupaten pamekasan"> KABUPATEN PAMEKASAN</option>
                    <option value="Kabupaten sumenep"> KABUPATEN SUMENEP</option>
                    <option value="Kota kediri"> KOTA KEDIRI</option>
                    <option value="Kota blitar"> KOTA BLITAR</option>
                    <option value="Kota malang"> KOTA MALANG</option>
                    <option value="Kota probolinggo"> KOTA PROBOLINGGO</option>
                    <option value="Kota pasuruan"> KOTA PASURUAN</option>
                    <option value="Kota mojokerto"> KOTA MOJOKERTO</option>
                    <option value="Kota madiun"> KOTA MADIUN</option>
                    <option value="Kota surabaya"> KOTA SURABAYA</option>
                    <option value="Kota batu"> KOTA BATU</option>
                    <option value="Kabupaten pandeglang"> KABUPATEN PANDEGLANG</option>
                    <option value="Kabupaten lebak"> KABUPATEN LEBAK</option>
                    <option value="Kabupaten tangerang"> KABUPATEN TANGERANG</option>
                    <option value="Kabupaten serang"> KABUPATEN SERANG</option>
                    <option value="Kota tangerang"> KOTA TANGERANG</option>
                    <option value="Kota cilegon"> KOTA CILEGON</option>
                    <option value="Kota serang"> KOTA SERANG</option>
                    <option value="Kota tangerang selatan"> KOTA TANGERANG SELATAN</option>
                    <option value="Kabupaten jembrana"> KABUPATEN JEMBRANA</option>
                    <option value="Kabupaten tabanan"> KABUPATEN TABANAN</option>
                    <option value="Kabupaten badung"> KABUPATEN BADUNG</option>
                    <option value="Kabupaten gianyar"> KABUPATEN GIANYAR</option>
                    <option value="Kabupaten klungkung"> KABUPATEN KLUNGKUNG</option>
                    <option value="Kabupaten bangli"> KABUPATEN BANGLI</option>
                    <option value="Kabupaten karang asem"> KABUPATEN KARANG ASEM</option>
                    <option value="Kabupaten buleleng"> KABUPATEN BULELENG</option>
                    <option value="Kota denpasar"> KOTA DENPASAR</option>
                    <option value="Kabupaten lombok barat"> KABUPATEN LOMBOK BARAT</option>
                    <option value="Kabupaten lombok tengah"> KABUPATEN LOMBOK TENGAH</option>
                    <option value="Kabupaten lombok timur"> KABUPATEN LOMBOK TIMUR</option>
                    <option value="Kabupaten sumbawa"> KABUPATEN SUMBAWA</option>
                    <option value="Kabupaten dompu"> KABUPATEN DOMPU</option>
                    <option value="Kabupaten bima"> KABUPATEN BIMA</option>
                    <option value="Kabupaten sumbawa barat"> KABUPATEN SUMBAWA BARAT</option>
                    <option value="Kabupaten lombok utara"> KABUPATEN LOMBOK UTARA</option>
                    <option value="Kota mataram"> KOTA MATARAM</option>
                    <option value="Kota bima"> KOTA BIMA</option>
                    <option value="Kabupaten sumba barat"> KABUPATEN SUMBA BARAT</option>
                    <option value="Kabupaten sumba timur"> KABUPATEN SUMBA TIMUR</option>
                    <option value="Kabupaten kupang"> KABUPATEN KUPANG</option>
                    <option value="Kabupaten timor tengah selatan"> KABUPATEN TIMOR TENGAH SELATAN</option>
                    <option value="Kabupaten timor tengah utara"> KABUPATEN TIMOR TENGAH UTARA</option>
                    <option value="Kabupaten belu"> KABUPATEN BELU</option>
                    <option value="Kabupaten alor"> KABUPATEN ALOR</option>
                    <option value="Kabupaten lembata"> KABUPATEN LEMBATA</option>
                    <option value="Kabupaten flores timur"> KABUPATEN FLORES TIMUR</option>
                    <option value="Kabupaten sikka"> KABUPATEN SIKKA</option>
                    <option value="Kabupaten ende"> KABUPATEN ENDE</option>
                    <option value="Kabupaten ngada"> KABUPATEN NGADA</option>
                    <option value="Kabupaten manggarai"> KABUPATEN MANGGARAI</option>
                    <option value="Kabupaten rote ndao"> KABUPATEN ROTE NDAO</option>
                    <option value="Kabupaten manggarai barat"> KABUPATEN MANGGARAI BARAT</option>
                    <option value="Kabupaten sumba tengah"> KABUPATEN SUMBA TENGAH</option>
                    <option value="Kabupaten sumba barat daya"> KABUPATEN SUMBA BARAT DAYA</option>
                    <option value="Kabupaten nagekeo"> KABUPATEN NAGEKEO</option>
                    <option value="Kabupaten manggarai timur"> KABUPATEN MANGGARAI TIMUR</option>
                    <option value="Kabupaten sabu raijua"> KABUPATEN SABU RAIJUA</option>
                    <option value="Kabupaten malaka"> KABUPATEN MALAKA</option>
                    <option value="Kota kupang"> KOTA KUPANG</option>
                    <option value="Kabupaten sambas"> KABUPATEN SAMBAS</option>
                    <option value="Kabupaten bengkayang"> KABUPATEN BENGKAYANG</option>
                    <option value="Kabupaten landak"> KABUPATEN LANDAK</option>
                    <option value="Kabupaten mempawah"> KABUPATEN MEMPAWAH</option>
                    <option value="Kabupaten sanggau"> KABUPATEN SANGGAU</option>
                    <option value="Kabupaten ketapang"> KABUPATEN KETAPANG</option>
                    <option value="Kabupaten sintang"> KABUPATEN SINTANG</option>
                    <option value="Kabupaten kapuas hulu"> KABUPATEN KAPUAS HULU</option>
                    <option value="Kabupaten sekadau"> KABUPATEN SEKADAU</option>
                    <option value="Kabupaten melawi"> KABUPATEN MELAWI</option>
                    <option value="Kabupaten kayong utara"> KABUPATEN KAYONG UTARA</option>
                    <option value="Kabupaten kubu raya"> KABUPATEN KUBU RAYA</option>
                    <option value="Kota pontianak"> KOTA PONTIANAK</option>
                    <option value="Kota singkawang"> KOTA SINGKAWANG</option>
                    <option value="Kabupaten kotawaringin barat"> KABUPATEN KOTAWARINGIN BARAT</option>
                    <option value="Kabupaten kotawaringin timur"> KABUPATEN KOTAWARINGIN TIMUR</option>
                    <option value="Kabupaten kapuas"> KABUPATEN KAPUAS</option>
                    <option value="Kabupaten barito selatan"> KABUPATEN BARITO SELATAN</option>
                    <option value="Kabupaten barito utara"> KABUPATEN BARITO UTARA</option>
                    <option value="Kabupaten sukamara"> KABUPATEN SUKAMARA</option>
                    <option value="Kabupaten lamandau"> KABUPATEN LAMANDAU</option>
                    <option value="Kabupaten seruyan"> KABUPATEN SERUYAN</option>
                    <option value="Kabupaten katingan"> KABUPATEN KATINGAN</option>
                    <option value="Kabupaten pulang pisau"> KABUPATEN PULANG PISAU</option>
                    <option value="Kabupaten gunung mas"> KABUPATEN GUNUNG MAS</option>
                    <option value="Kabupaten barito timur"> KABUPATEN BARITO TIMUR</option>
                    <option value="Kabupaten murung raya"> KABUPATEN MURUNG RAYA</option>
                    <option value="Kota palangka raya"> KOTA PALANGKA RAYA</option>
                    <option value="Kabupaten tanah laut"> KABUPATEN TANAH LAUT</option>
                    <option value="Kabupaten kota baru"> KABUPATEN KOTA BARU</option>
                    <option value="Kabupaten banjar"> KABUPATEN BANJAR</option>
                    <option value="Kabupaten barito kuala"> KABUPATEN BARITO KUALA</option>
                    <option value="Kabupaten tapin"> KABUPATEN TAPIN</option>
                    <option value="Kabupaten hulu sungai selatan"> KABUPATEN HULU SUNGAI SELATAN</option>
                    <option value="Kabupaten hulu sungai tengah"> KABUPATEN HULU SUNGAI TENGAH</option>
                    <option value="Kabupaten hulu sungai utara"> KABUPATEN HULU SUNGAI UTARA</option>
                    <option value="Kabupaten tabalong"> KABUPATEN TABALONG</option>
                    <option value="Kabupaten tanah bumbu"> KABUPATEN TANAH BUMBU</option>
                    <option value="Kabupaten balangan"> KABUPATEN BALANGAN</option>
                    <option value="Kota banjarmasin"> KOTA BANJARMASIN</option>
                    <option value="Kota banjar baru"> KOTA BANJAR BARU</option>
                    <option value="Kabupaten paser"> KABUPATEN PASER</option>
                    <option value="Kabupaten kutai barat"> KABUPATEN KUTAI BARAT</option>
                    <option value="Kabupaten kutai kartanegara"> KABUPATEN KUTAI KARTANEGARA</option>
                    <option value="Kabupaten kutai timur"> KABUPATEN KUTAI TIMUR</option>
                    <option value="Kabupaten berau"> KABUPATEN BERAU</option>
                    <option value="Kabupaten penajam paser utara"> KABUPATEN PENAJAM PASER UTARA</option>
                    <option value="Kabupaten mahakam hulu"> KABUPATEN MAHAKAM HULU</option>
                    <option value="Kota balikpapan"> KOTA BALIKPAPAN</option>
                    <option value="Kota samarinda"> KOTA SAMARINDA</option>
                    <option value="Kota bontang"> KOTA BONTANG</option>
                    <option value="Kabupaten malinau"> KABUPATEN MALINAU</option>
                    <option value="Kabupaten bulungan"> KABUPATEN BULUNGAN</option>
                    <option value="Kabupaten tana tidung"> KABUPATEN TANA TIDUNG</option>
                    <option value="Kabupaten nunukan"> KABUPATEN NUNUKAN</option>
                    <option value="Kota tarakan"> KOTA TARAKAN</option>
                    <option value="Kabupaten bolaang mongondow"> KABUPATEN BOLAANG MONGONDOW</option>
                    <option value="Kabupaten minahasa"> KABUPATEN MINAHASA</option>
                    <option value="Kabupaten kepulauan sangihe"> KABUPATEN KEPULAUAN SANGIHE</option>
                    <option value="Kabupaten kepulauan talaud"> KABUPATEN KEPULAUAN TALAUD</option>
                    <option value="Kabupaten minahasa selatan"> KABUPATEN MINAHASA SELATAN</option>
                    <option value="Kabupaten minahasa utara"> KABUPATEN MINAHASA UTARA</option>
                    <option value="Kabupaten bolaang mongondow utara"> KABUPATEN BOLAANG MONGONDOW UTARA</option>
                    <option value="Kabupaten siau tagulandang biaro"> KABUPATEN SIAU TAGULANDANG BIARO</option>
                    <option value="Kabupaten minahasa tenggara"> KABUPATEN MINAHASA TENGGARA</option>
                    <option value="Kabupaten bolaang mongondow selatan"> KABUPATEN BOLAANG MONGONDOW SELATAN</option>
                    <option value="Kabupaten bolaang mongondow timur"> KABUPATEN BOLAANG MONGONDOW TIMUR</option>
                    <option value="Kota manado"> KOTA MANADO</option>
                    <option value="Kota bitung"> KOTA BITUNG</option>
                    <option value="Kota tomohon"> KOTA TOMOHON</option>
                    <option value="Kota kotamobagu"> KOTA KOTAMOBAGU</option>
                    <option value="Kabupaten banggai kepulauan"> KABUPATEN BANGGAI KEPULAUAN</option>
                    <option value="Kabupaten banggai"> KABUPATEN BANGGAI</option>
                    <option value="Kabupaten morowali"> KABUPATEN MOROWALI</option>
                    <option value="Kabupaten poso"> KABUPATEN POSO</option>
                    <option value="Kabupaten donggala"> KABUPATEN DONGGALA</option>
                    <option value="Kabupaten toli-toli"> KABUPATEN TOLI-TOLI</option>
                    <option value="Kabupaten buol"> KABUPATEN BUOL</option>
                    <option value="Kabupaten parigi moutong"> KABUPATEN PARIGI MOUTONG</option>
                    <option value="Kabupaten tojo una-una"> KABUPATEN TOJO UNA-UNA</option>
                    <option value="Kabupaten sigi"> KABUPATEN SIGI</option>
                    <option value="Kabupaten banggai laut"> KABUPATEN BANGGAI LAUT</option>
                    <option value="Kabupaten morowali utara"> KABUPATEN MOROWALI UTARA</option>
                    <option value="Kota palu"> KOTA PALU</option>
                    <option value="Kabupaten kepulauan selayar"> KABUPATEN KEPULAUAN SELAYAR</option>
                    <option value="Kabupaten bulukumba"> KABUPATEN BULUKUMBA</option>
                    <option value="Kabupaten bantaeng"> KABUPATEN BANTAENG</option>
                    <option value="Kabupaten jeneponto"> KABUPATEN JENEPONTO</option>
                    <option value="Kabupaten takalar"> KABUPATEN TAKALAR</option>
                    <option value="Kabupaten gowa"> KABUPATEN GOWA</option>
                    <option value="Kabupaten sinjai"> KABUPATEN SINJAI</option>
                    <option value="Kabupaten maros"> KABUPATEN MAROS</option>
                    <option value="Kabupaten pangkajene dan kepulauan"> KABUPATEN PANGKAJENE DAN KEPULAUAN</option>
                    <option value="Kabupaten barru"> KABUPATEN BARRU</option>
                    <option value="Kabupaten bone"> KABUPATEN BONE</option>
                    <option value="Kabupaten soppeng"> KABUPATEN SOPPENG</option>
                    <option value="Kabupaten wajo"> KABUPATEN WAJO</option>
                    <option value="Kabupaten sidenreng rappang"> KABUPATEN SIDENRENG RAPPANG</option>
                    <option value="Kabupaten pinrang"> KABUPATEN PINRANG</option>
                    <option value="Kabupaten enrekang"> KABUPATEN ENREKANG</option>
                    <option value="Kabupaten luwu"> KABUPATEN LUWU</option>
                    <option value="Kabupaten tana toraja"> KABUPATEN TANA TORAJA</option>
                    <option value="Kabupaten luwu utara"> KABUPATEN LUWU UTARA</option>
                    <option value="Kabupaten luwu timur"> KABUPATEN LUWU TIMUR</option>
                    <option value="Kabupaten toraja utara"> KABUPATEN TORAJA UTARA</option>
                    <option value="Kota makassar"> KOTA MAKASSAR</option>
                    <option value="Kota parepare"> KOTA PAREPARE</option>
                    <option value="Kota palopo"> KOTA PALOPO</option>
                    <option value="Kabupaten buton"> KABUPATEN BUTON</option>
                    <option value="Kabupaten muna"> KABUPATEN MUNA</option>
                    <option value="Kabupaten konawe"> KABUPATEN KONAWE</option>
                    <option value="Kabupaten kolaka"> KABUPATEN KOLAKA</option>
                    <option value="Kabupaten konawe selatan"> KABUPATEN KONAWE SELATAN</option>
                    <option value="Kabupaten bombana"> KABUPATEN BOMBANA</option>
                    <option value="Kabupaten wakatobi"> KABUPATEN WAKATOBI</option>
                    <option value="Kabupaten kolaka utara"> KABUPATEN KOLAKA UTARA</option>
                    <option value="Kabupaten buton utara"> KABUPATEN BUTON UTARA</option>
                    <option value="Kabupaten konawe utara"> KABUPATEN KONAWE UTARA</option>
                    <option value="Kabupaten kolaka timur"> KABUPATEN KOLAKA TIMUR</option>
                    <option value="Kabupaten konawe kepulauan"> KABUPATEN KONAWE KEPULAUAN</option>
                    <option value="Kabupaten muna barat"> KABUPATEN MUNA BARAT</option>
                    <option value="Kabupaten buton tengah"> KABUPATEN BUTON TENGAH</option>
                    <option value="Kabupaten buton selatan"> KABUPATEN BUTON SELATAN</option>
                    <option value="Kota kendari"> KOTA KENDARI</option>
                    <option value="Kota baubau"> KOTA BAUBAU</option>
                    <option value="Kabupaten boalemo"> KABUPATEN BOALEMO</option>
                    <option value="Kabupaten gorontalo"> KABUPATEN GORONTALO</option>
                    <option value="Kabupaten pohuwato"> KABUPATEN POHUWATO</option>
                    <option value="Kabupaten bone bolango"> KABUPATEN BONE BOLANGO</option>
                    <option value="Kabupaten gorontalo utara"> KABUPATEN GORONTALO UTARA</option>
                    <option value="Kota gorontalo"> KOTA GORONTALO</option>
                    <option value="Kabupaten majene"> KABUPATEN MAJENE</option>
                    <option value="Kabupaten polewali mandar"> KABUPATEN POLEWALI MANDAR</option>
                    <option value="Kabupaten mamasa"> KABUPATEN MAMASA</option>
                    <option value="Kabupaten mamuju"> KABUPATEN MAMUJU</option>
                    <option value="Kabupaten mamuju utara"> KABUPATEN MAMUJU UTARA</option>
                    <option value="Kabupaten mamuju tengah"> KABUPATEN MAMUJU TENGAH</option>
                    <option value="Kabupaten maluku tenggara barat"> KABUPATEN MALUKU TENGGARA BARAT</option>
                    <option value="Kabupaten maluku tenggara"> KABUPATEN MALUKU TENGGARA</option>
                    <option value="Kabupaten maluku tengah"> KABUPATEN MALUKU TENGAH</option>
                    <option value="Kabupaten buru"> KABUPATEN BURU</option>
                    <option value="Kabupaten kepulauan aru"> KABUPATEN KEPULAUAN ARU</option>
                    <option value="Kabupaten seram bagian barat"> KABUPATEN SERAM BAGIAN BARAT</option>
                    <option value="Kabupaten seram bagian timur"> KABUPATEN SERAM BAGIAN TIMUR</option>
                    <option value="Kabupaten maluku barat daya"> KABUPATEN MALUKU BARAT DAYA</option>
                    <option value="Kabupaten buru selatan"> KABUPATEN BURU SELATAN</option>
                    <option value="Kota ambon"> KOTA AMBON</option>
                    <option value="Kota tual"> KOTA TUAL</option>
                    <option value="Kabupaten halmahera barat"> KABUPATEN HALMAHERA BARAT</option>
                    <option value="Kabupaten halmahera tengah"> KABUPATEN HALMAHERA TENGAH</option>
                    <option value="Kabupaten kepulauan sula"> KABUPATEN KEPULAUAN SULA</option>
                    <option value="Kabupaten halmahera selatan"> KABUPATEN HALMAHERA SELATAN</option>
                    <option value="Kabupaten halmahera utara"> KABUPATEN HALMAHERA UTARA</option>
                    <option value="Kabupaten halmahera timur"> KABUPATEN HALMAHERA TIMUR</option>
                    <option value="Kabupaten pulau morotai"> KABUPATEN PULAU MOROTAI</option>
                    <option value="Kabupaten pulau taliabu"> KABUPATEN PULAU TALIABU</option>
                    <option value="Kota ternate"> KOTA TERNATE</option>
                    <option value="Kota tidore kepulauan"> KOTA TIDORE KEPULAUAN</option>
                    <option value="Kabupaten fakfak"> KABUPATEN FAKFAK</option>
                    <option value="Kabupaten kaimana"> KABUPATEN KAIMANA</option>
                    <option value="Kabupaten teluk wondama"> KABUPATEN TELUK WONDAMA</option>
                    <option value="Kabupaten teluk bintuni"> KABUPATEN TELUK BINTUNI</option>
                    <option value="Kabupaten manokwari"> KABUPATEN MANOKWARI</option>
                    <option value="Kabupaten sorong selatan"> KABUPATEN SORONG SELATAN</option>
                    <option value="Kabupaten sorong"> KABUPATEN SORONG</option>
                    <option value="Kabupaten raja ampat"> KABUPATEN RAJA AMPAT</option>
                    <option value="Kabupaten tambrauw"> KABUPATEN TAMBRAUW</option>
                    <option value="Kabupaten maybrat"> KABUPATEN MAYBRAT</option>
                    <option value="Kabupaten manokwari selatan"> KABUPATEN MANOKWARI SELATAN</option>
                    <option value="Kabupaten pegunungan arfak"> KABUPATEN PEGUNUNGAN ARFAK</option>
                    <option value="Kota sorong"> KOTA SORONG</option>
                    <option value="Kabupaten merauke"> KABUPATEN MERAUKE</option>
                    <option value="Kabupaten jayawijaya"> KABUPATEN JAYAWIJAYA</option>
                    <option value="Kabupaten jayapura"> KABUPATEN JAYAPURA</option>
                    <option value="Kabupaten nabire"> KABUPATEN NABIRE</option>
                    <option value="Kabupaten kepulauan yapen"> KABUPATEN KEPULAUAN YAPEN</option>
                    <option value="Kabupaten biak numfor"> KABUPATEN BIAK NUMFOR</option>
                    <option value="Kabupaten paniai"> KABUPATEN PANIAI</option>
                    <option value="Kabupaten puncak jaya"> KABUPATEN PUNCAK JAYA</option>
                    <option value="Kabupaten mimika"> KABUPATEN MIMIKA</option>
                    <option value="Kabupaten boven digoel"> KABUPATEN BOVEN DIGOEL</option>
                    <option value="Kabupaten mappi"> KABUPATEN MAPPI</option>
                    <option value="Kabupaten asmat"> KABUPATEN ASMAT</option>
                    <option value="Kabupaten yahukimo"> KABUPATEN YAHUKIMO</option>
                    <option value="Kabupaten pegunungan bintang"> KABUPATEN PEGUNUNGAN BINTANG</option>
                    <option value="Kabupaten tolikara"> KABUPATEN TOLIKARA</option>
                    <option value="Kabupaten sarmi"> KABUPATEN SARMI</option>
                    <option value="Kabupaten keerom"> KABUPATEN KEEROM</option>
                    <option value="Kabupaten waropen"> KABUPATEN WAROPEN</option>
                    <option value="Kabupaten supiori"> KABUPATEN SUPIORI</option>
                    <option value="Kabupaten mamberamo raya"> KABUPATEN MAMBERAMO RAYA</option>
                    <option value="Kabupaten nduga"> KABUPATEN NDUGA</option>
                    <option value="Kabupaten lanny jaya"> KABUPATEN LANNY JAYA</option>
                    <option value="Kabupaten mamberamo tengah"> KABUPATEN MAMBERAMO TENGAH</option>
                    <option value="Kabupaten yalimo"> KABUPATEN YALIMO</option>
                    <option value="Kabupaten puncak"> KABUPATEN PUNCAK</option>
                    <option value="Kabupaten dogiyai"> KABUPATEN DOGIYAI</option>
                    <option value="Kabupaten intan jaya"> KABUPATEN INTAN JAYA</option>
                    <option value="Kabupaten deiyai"> KABUPATEN DEIYAI</option>
                    <option value="Kota jayapura"> KOTA JAYAPURA</option>
                    <option value="Surabaya timur"> SURABAYA TIMUR</option>            
                  </select>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><span style="font-size: 14px; padding-left: 10px">Tanggal lahir *</span></div>
                    </div>
                    <input class="form-control" placeholder="" type="date" id="birthdate" name="birthdate" value="" required>
                  </div>
                  <small style="color:#fb6340" id="age_alert"></small>
                </div>
                <div class="form-group">
                  <label for="inputState" style="font-size: 13px; color: #8898aa"><?=lang('Global.sex');?> *</label>
                  <select id="sex" name="sex" class="form-control" required="">
                    <option selected disabled value=""><?=lang('Global.choose');?>...</option>
                    <option value="male">Laki-laki</option>
                    <option value="female">Perempuan</option>
                  </select>
                </div>
                <!-- Pendidikan -->
                <p style="font-size: 14px; margin-top:40px;"><b><?=lang('Global.education');?></b></p>
                
                <input hidden class="form-control" placeholder="" type="text" id="pendterakhir" name="pendterakhir" value="null" required>
                <!-- <div class="form-group">
                  <label for="inputState" style="font-size: 12px; color: #8898aa">Pendidikan terakhir *</label>
                  
                  <select id="pendterakhir" class="form-control js-example-basic-single" name="pendterakhir" required="" tabindex="-1" aria-hidden="true">
                    <option value="" selected="">Silahkan pilih...</option>
                    <option value="2"> SMA 1/YEAR 10</option>
                    <option value="3"> SMA 2/YEAR 11</option>
                    <option value="4"> SMA 3/YEAR 12</option>
                    <option value="5"> FOUNDATION</option>
                    <option value="6"> DIPLOMA</option>
                    <option value="7"> STRATA 1</option>
                    <option value="8"> STRATA 2</option>
                  </select>
                </div> -->
                <div class="form-group">
                  <label for="inputState" style="font-size: 12px; color: #8898aa"><?=lang('Global.education_now');?> *</label>
                  <select id="education_level" name="education_level" class="form-control"  required>
                    <option selected disabled value=""><?=lang('Global.choose');?>...</option>
                    <option value="SMP">SMP</option>
                    <option value="SMA">SMA</option>
                    <option value="Universitas">Universitas</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="inputState" style="font-size: 12px; color: #8898aa"><?=lang('Global.semester');?> *</label>
                  <select class="form-control" name="semester" id="semester" required>
                    <option selected disabled value="" class=""><?=lang('Global.choose');?>...</option>
                      <option value="7" class="SMP">7</option>
                      <option value="8" class="SMP">8</option>
                      <option value="9" class="SMP">9</option>
                      <option value="10" class="SMA">10</option>
                      <option value="11" class="SMA">11</option>
                      <option value="12" class="SMA">12</option>
                      <option value="1" class="Universitas">1</option>
                      <option value="2" class="Universitas">2</option>
                      <option value="3" class="Universitas">3</option>
                      <option value="4" class="Universitas">4</option>
                      <option value="5" class="Universitas">5</option>
                      <option value="6" class="Universitas">6</option>
                      <option value="7" class="Universitas">7</option>
                      <option value="8" class="Universitas">8</option>
                      <option value=">8" class="Universitas">>8</option>
                    </select>
                </div>
                <!-- <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class=""></i></span>
                    </div>
                    <input class="form-control" placeholder="<?=lang('Global.semester');?> *" type="text" id="semester" name="semester" value="" required="">
                  </div>
                </div> -->
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class=""></i></span>
                    </div>
                    <input class="form-control" placeholder="<?=lang('Global.company');?> *" type="text" id="company" name="company" value="" required="">
                  </div>
                </div>
                <p style="font-size: 14px; margin-top:40px;"><b><?=lang('Global.contact_address');?></b></p>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">+62</span>
                    </div>
                    <input class="phone_number form-control" placeholder="<?=lang('Global.phone');?> / Whatsapp *" type="text" id="phone" name="phone" value="" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputState" style="font-size: 12px; color: #8898aa">Domisili *</label>                  
                  <!-- <select id="pendterakhir" class="form-control js-example-basic-single" name="pendterakhir" required tabindex="-1" aria-hidden="true"> -->

                  <select class="form-control js-example-basic-single" name="domisili" id="domisili"  tabindex="-1" aria-hidden="true" required="">
                    <option value="" selected="">Silahkan pilih...</option>
                    <option value="Kabupaten simeulue"> KABUPATEN SIMEULUE</option>
                    <option value="Kabupaten aceh singkil"> KABUPATEN ACEH SINGKIL</option>
                    <option value="Kabupaten aceh selatan"> KABUPATEN ACEH SELATAN</option>
                    <option value="Kabupaten aceh tenggara"> KABUPATEN ACEH TENGGARA</option>
                    <option value="Kabupaten aceh timur"> KABUPATEN ACEH TIMUR</option>
                    <option value="Kabupaten aceh tengah"> KABUPATEN ACEH TENGAH</option>
                    <option value="Kabupaten aceh barat"> KABUPATEN ACEH BARAT</option>
                    <option value="Kabupaten aceh besar"> KABUPATEN ACEH BESAR</option>
                    <option value="Kabupaten pidie"> KABUPATEN PIDIE</option>
                    <option value="Kabupaten bireuen"> KABUPATEN BIREUEN</option>
                    <option value="Kabupaten aceh utara"> KABUPATEN ACEH UTARA</option>
                    <option value="Kabupaten aceh barat daya"> KABUPATEN ACEH BARAT DAYA</option>
                    <option value="Kabupaten gayo lues"> KABUPATEN GAYO LUES</option>
                    <option value="Kabupaten aceh tamiang"> KABUPATEN ACEH TAMIANG</option>
                    <option value="Kabupaten nagan raya"> KABUPATEN NAGAN RAYA</option>
                    <option value="Kabupaten aceh jaya"> KABUPATEN ACEH JAYA</option>
                    <option value="Kabupaten bener meriah"> KABUPATEN BENER MERIAH</option>
                    <option value="Kabupaten pidie jaya"> KABUPATEN PIDIE JAYA</option>
                    <option value="Kota banda aceh"> KOTA BANDA ACEH</option>
                    <option value="Kota sabang"> KOTA SABANG</option>
                    <option value="Kota langsa"> KOTA LANGSA</option>
                    <option value="Kota lhokseumawe"> KOTA LHOKSEUMAWE</option>
                    <option value="Kota subulussalam"> KOTA SUBULUSSALAM</option>
                    <option value="Kabupaten nias"> KABUPATEN NIAS</option>
                    <option value="Kabupaten mandailing natal"> KABUPATEN MANDAILING NATAL</option>
                    <option value="Kabupaten tapanuli selatan"> KABUPATEN TAPANULI SELATAN</option>
                    <option value="Kabupaten tapanuli tengah"> KABUPATEN TAPANULI TENGAH</option>
                    <option value="Kabupaten tapanuli utara"> KABUPATEN TAPANULI UTARA</option>
                    <option value="Kabupaten toba samosir"> KABUPATEN TOBA SAMOSIR</option>
                    <option value="Kabupaten labuhan batu"> KABUPATEN LABUHAN BATU</option>
                    <option value="Kabupaten asahan"> KABUPATEN ASAHAN</option>
                    <option value="Kabupaten simalungun"> KABUPATEN SIMALUNGUN</option>
                    <option value="Kabupaten dairi"> KABUPATEN DAIRI</option>
                    <option value="Kabupaten karo"> KABUPATEN KARO</option>
                    <option value="Kabupaten deli serdang"> KABUPATEN DELI SERDANG</option>
                    <option value="Kabupaten langkat"> KABUPATEN LANGKAT</option>
                    <option value="Kabupaten nias selatan"> KABUPATEN NIAS SELATAN</option>
                    <option value="Kabupaten humbang hasundutan"> KABUPATEN HUMBANG HASUNDUTAN</option>
                    <option value="Kabupaten pakpak bharat"> KABUPATEN PAKPAK BHARAT</option>
                    <option value="Kabupaten samosir"> KABUPATEN SAMOSIR</option>
                    <option value="Kabupaten serdang bedagai"> KABUPATEN SERDANG BEDAGAI</option>
                    <option value="Kabupaten batu bara"> KABUPATEN BATU BARA</option>
                    <option value="Kabupaten padang lawas utara"> KABUPATEN PADANG LAWAS UTARA</option>
                    <option value="Kabupaten padang lawas"> KABUPATEN PADANG LAWAS</option>
                    <option value="Kabupaten labuhan batu selatan"> KABUPATEN LABUHAN BATU SELATAN</option>
                    <option value="Kabupaten labuhan batu utara"> KABUPATEN LABUHAN BATU UTARA</option>
                    <option value="Kabupaten nias utara"> KABUPATEN NIAS UTARA</option>
                    <option value="Kabupaten nias barat"> KABUPATEN NIAS BARAT</option>
                    <option value="Kota sibolga"> KOTA SIBOLGA</option>
                    <option value="Kota tanjung balai"> KOTA TANJUNG BALAI</option>
                    <option value="Kota pematang siantar"> KOTA PEMATANG SIANTAR</option>
                    <option value="Kota tebing tinggi"> KOTA TEBING TINGGI</option>
                    <option value="Kota medan"> KOTA MEDAN</option>
                    <option value="Kota binjai"> KOTA BINJAI</option>
                    <option value="Kota padangsidimpuan"> KOTA PADANGSIDIMPUAN</option>
                    <option value="Kota gunungsitoli"> KOTA GUNUNGSITOLI</option>
                    <option value="Kabupaten kepulauan mentawai"> KABUPATEN KEPULAUAN MENTAWAI</option>
                    <option value="Kabupaten pesisir selatan"> KABUPATEN PESISIR SELATAN</option>
                    <option value="Kabupaten solok"> KABUPATEN SOLOK</option>
                    <option value="Kabupaten sijunjung"> KABUPATEN SIJUNJUNG</option>
                    <option value="Kabupaten tanah datar"> KABUPATEN TANAH DATAR</option>
                    <option value="Kabupaten padang pariaman"> KABUPATEN PADANG PARIAMAN</option>
                    <option value="Kabupaten agam"> KABUPATEN AGAM</option>
                    <option value="Kabupaten lima puluh kota"> KABUPATEN LIMA PULUH KOTA</option>
                    <option value="Kabupaten pasaman"> KABUPATEN PASAMAN</option>
                    <option value="Kabupaten solok selatan"> KABUPATEN SOLOK SELATAN</option>
                    <option value="Kabupaten dharmasraya"> KABUPATEN DHARMASRAYA</option>
                    <option value="Kabupaten pasaman barat"> KABUPATEN PASAMAN BARAT</option>
                    <option value="Kota padang"> KOTA PADANG</option>
                    <option value="Kota solok"> KOTA SOLOK</option>
                    <option value="Kota sawah lunto"> KOTA SAWAH LUNTO</option>
                    <option value="Kota padang panjang"> KOTA PADANG PANJANG</option>
                    <option value="Kota bukittinggi"> KOTA BUKITTINGGI</option>
                    <option value="Kota payakumbuh"> KOTA PAYAKUMBUH</option>
                    <option value="Kota pariaman"> KOTA PARIAMAN</option>
                    <option value="Kabupaten kuantan singingi"> KABUPATEN KUANTAN SINGINGI</option>
                    <option value="Kabupaten indragiri hulu"> KABUPATEN INDRAGIRI HULU</option>
                    <option value="Kabupaten indragiri hilir"> KABUPATEN INDRAGIRI HILIR</option>
                    <option value="Kabupaten pelalawan"> KABUPATEN PELALAWAN</option>
                    <option value="Kabupaten siak"> KABUPATEN SIAK</option>
                    <option value="Kabupaten kampar"> KABUPATEN KAMPAR</option>
                    <option value="Kabupaten rokan hulu"> KABUPATEN ROKAN HULU</option>
                    <option value="Kabupaten bengkalis"> KABUPATEN BENGKALIS</option>
                    <option value="Kabupaten rokan hilir"> KABUPATEN ROKAN HILIR</option>
                    <option value="Kabupaten kepulauan meranti"> KABUPATEN KEPULAUAN MERANTI</option>
                    <option value="Kota pekanbaru"> KOTA PEKANBARU</option>
                    <option value="Kota dumai"> KOTA DUMAI</option>
                    <option value="Kabupaten kerinci"> KABUPATEN KERINCI</option>
                    <option value="Kabupaten merangin"> KABUPATEN MERANGIN</option>
                    <option value="Kabupaten sarolangun"> KABUPATEN SAROLANGUN</option>
                    <option value="Kabupaten batang hari"> KABUPATEN BATANG HARI</option>
                    <option value="Kabupaten muaro jambi"> KABUPATEN MUARO JAMBI</option>
                    <option value="Kabupaten tanjung jabung timur"> KABUPATEN TANJUNG JABUNG TIMUR</option>
                    <option value="Kabupaten tanjung jabung barat"> KABUPATEN TANJUNG JABUNG BARAT</option>
                    <option value="Kabupaten tebo"> KABUPATEN TEBO</option>
                    <option value="Kabupaten bungo"> KABUPATEN BUNGO</option>
                    <option value="Kota jambi"> KOTA JAMBI</option>
                    <option value="Kota sungai penuh"> KOTA SUNGAI PENUH</option>
                    <option value="Kabupaten ogan komering ulu"> KABUPATEN OGAN KOMERING ULU</option>
                    <option value="Kabupaten ogan komering ilir"> KABUPATEN OGAN KOMERING ILIR</option>
                    <option value="Kabupaten muara enim"> KABUPATEN MUARA ENIM</option>
                    <option value="Kabupaten lahat"> KABUPATEN LAHAT</option>
                    <option value="Kabupaten musi rawas"> KABUPATEN MUSI RAWAS</option>
                    <option value="Kabupaten musi banyuasin"> KABUPATEN MUSI BANYUASIN</option>
                    <option value="Kabupaten banyu asin"> KABUPATEN BANYU ASIN</option>
                    <option value="Kabupaten ogan komering ulu selatan"> KABUPATEN OGAN KOMERING ULU SELATAN</option>
                    <option value="Kabupaten ogan komering ulu timur"> KABUPATEN OGAN KOMERING ULU TIMUR</option>
                    <option value="Kabupaten ogan ilir"> KABUPATEN OGAN ILIR</option>
                    <option value="Kabupaten empat lawang"> KABUPATEN EMPAT LAWANG</option>
                    <option value="Kabupaten penukal abab lematang ilir"> KABUPATEN PENUKAL ABAB LEMATANG ILIR</option>
                    <option value="Kabupaten musi rawas utara"> KABUPATEN MUSI RAWAS UTARA</option>
                    <option value="Kota palembang"> KOTA PALEMBANG</option>
                    <option value="Kota prabumulih"> KOTA PRABUMULIH</option>
                    <option value="Kota pagar alam"> KOTA PAGAR ALAM</option>
                    <option value="Kota lubuklinggau"> KOTA LUBUKLINGGAU</option>
                    <option value="Kabupaten bengkulu selatan"> KABUPATEN BENGKULU SELATAN</option>
                    <option value="Kabupaten rejang lebong"> KABUPATEN REJANG LEBONG</option>
                    <option value="Kabupaten bengkulu utara"> KABUPATEN BENGKULU UTARA</option>
                    <option value="Kabupaten kaur"> KABUPATEN KAUR</option>
                    <option value="Kabupaten seluma"> KABUPATEN SELUMA</option>
                    <option value="Kabupaten mukomuko"> KABUPATEN MUKOMUKO</option>
                    <option value="Kabupaten lebong"> KABUPATEN LEBONG</option>
                    <option value="Kabupaten kepahiang"> KABUPATEN KEPAHIANG</option>
                    <option value="Kabupaten bengkulu tengah"> KABUPATEN BENGKULU TENGAH</option>
                    <option value="Kota bengkulu"> KOTA BENGKULU</option>
                    <option value="Kabupaten lampung barat"> KABUPATEN LAMPUNG BARAT</option>
                    <option value="Kabupaten tanggamus"> KABUPATEN TANGGAMUS</option>
                    <option value="Kabupaten lampung selatan"> KABUPATEN LAMPUNG SELATAN</option>
                    <option value="Kabupaten lampung timur"> KABUPATEN LAMPUNG TIMUR</option>
                    <option value="Kabupaten lampung tengah"> KABUPATEN LAMPUNG TENGAH</option>
                    <option value="Kabupaten lampung utara"> KABUPATEN LAMPUNG UTARA</option>
                    <option value="Kabupaten way kanan"> KABUPATEN WAY KANAN</option>
                    <option value="Kabupaten tulangbawang"> KABUPATEN TULANGBAWANG</option>
                    <option value="Kabupaten pesawaran"> KABUPATEN PESAWARAN</option>
                    <option value="Kabupaten pringsewu"> KABUPATEN PRINGSEWU</option>
                    <option value="Kabupaten mesuji"> KABUPATEN MESUJI</option>
                    <option value="Kabupaten tulang bawang barat"> KABUPATEN TULANG BAWANG BARAT</option>
                    <option value="Kabupaten pesisir barat"> KABUPATEN PESISIR BARAT</option>
                    <option value="Kota bandar lampung"> KOTA BANDAR LAMPUNG</option>
                    <option value="Kota metro"> KOTA METRO</option>
                    <option value="Kabupaten bangka"> KABUPATEN BANGKA</option>
                    <option value="Kabupaten belitung"> KABUPATEN BELITUNG</option>
                    <option value="Kabupaten bangka barat"> KABUPATEN BANGKA BARAT</option>
                    <option value="Kabupaten bangka tengah"> KABUPATEN BANGKA TENGAH</option>
                    <option value="Kabupaten bangka selatan"> KABUPATEN BANGKA SELATAN</option>
                    <option value="Kabupaten belitung timur"> KABUPATEN BELITUNG TIMUR</option>
                    <option value="Kota pangkal pinang"> KOTA PANGKAL PINANG</option>
                    <option value="Kabupaten karimun"> KABUPATEN KARIMUN</option>
                    <option value="Kabupaten bintan"> KABUPATEN BINTAN</option>
                    <option value="Kabupaten natuna"> KABUPATEN NATUNA</option>
                    <option value="Kabupaten lingga"> KABUPATEN LINGGA</option>
                    <option value="Kabupaten kepulauan anambas"> KABUPATEN KEPULAUAN ANAMBAS</option>
                    <option value="Kota batam"> KOTA BATAM</option>
                    <option value="Kota tanjung pinang"> KOTA TANJUNG PINANG</option>
                    <option value="Kabupaten kepulauan seribu"> KABUPATEN KEPULAUAN SERIBU</option>
                    <option value="Kota jakarta selatan"> KOTA JAKARTA SELATAN</option>
                    <option value="Kota jakarta timur"> KOTA JAKARTA TIMUR</option>
                    <option value="Kota jakarta pusat"> KOTA JAKARTA PUSAT</option>
                    <option value="Kota jakarta barat"> KOTA JAKARTA BARAT</option>
                    <option value="Kota jakarta utara"> KOTA JAKARTA UTARA</option>
                    <option value="Kabupaten bogor"> KABUPATEN BOGOR</option>
                    <option value="Kabupaten sukabumi"> KABUPATEN SUKABUMI</option>
                    <option value="Kabupaten cianjur"> KABUPATEN CIANJUR</option>
                    <option value="Kabupaten bandung"> KABUPATEN BANDUNG</option>
                    <option value="Kabupaten garut"> KABUPATEN GARUT</option>
                    <option value="Kabupaten tasikmalaya"> KABUPATEN TASIKMALAYA</option>
                    <option value="Kabupaten ciamis"> KABUPATEN CIAMIS</option>
                    <option value="Kabupaten kuningan"> KABUPATEN KUNINGAN</option>
                    <option value="Kabupaten cirebon"> KABUPATEN CIREBON</option>
                    <option value="Kabupaten majalengka"> KABUPATEN MAJALENGKA</option>
                    <option value="Kabupaten sumedang"> KABUPATEN SUMEDANG</option>
                    <option value="Kabupaten indramayu"> KABUPATEN INDRAMAYU</option>
                    <option value="Kabupaten subang"> KABUPATEN SUBANG</option>
                    <option value="Kabupaten purwakarta"> KABUPATEN PURWAKARTA</option>
                    <option value="Kabupaten karawang"> KABUPATEN KARAWANG</option>
                    <option value="Kabupaten bekasi"> KABUPATEN BEKASI</option>
                    <option value="Kabupaten bandung barat"> KABUPATEN BANDUNG BARAT</option>
                    <option value="Kabupaten pangandaran"> KABUPATEN PANGANDARAN</option>
                    <option value="Kota bogor"> KOTA BOGOR</option>
                    <option value="Kota sukabumi"> KOTA SUKABUMI</option>
                    <option value="Kota bandung"> KOTA BANDUNG</option>
                    <option value="Kota cirebon"> KOTA CIREBON</option>
                    <option value="Kota bekasi"> KOTA BEKASI</option>
                    <option value="Kota depok"> KOTA DEPOK</option>
                    <option value="Kota cimahi"> KOTA CIMAHI</option>
                    <option value="Kota tasikmalaya"> KOTA TASIKMALAYA</option>
                    <option value="Kota banjar"> KOTA BANJAR</option>
                    <option value="Kabupaten cilacap"> KABUPATEN CILACAP</option>
                    <option value="Kabupaten banyumas"> KABUPATEN BANYUMAS</option>
                    <option value="Kabupaten purbalingga"> KABUPATEN PURBALINGGA</option>
                    <option value="Kabupaten banjarnegara"> KABUPATEN BANJARNEGARA</option>
                    <option value="Kabupaten kebumen"> KABUPATEN KEBUMEN</option>
                    <option value="Kabupaten purworejo"> KABUPATEN PURWOREJO</option>
                    <option value="Kabupaten wonosobo"> KABUPATEN WONOSOBO</option>
                    <option value="Kabupaten magelang"> KABUPATEN MAGELANG</option>
                    <option value="Kabupaten boyolali"> KABUPATEN BOYOLALI</option>
                    <option value="Kabupaten klaten"> KABUPATEN KLATEN</option>
                    <option value="Kabupaten sukoharjo"> KABUPATEN SUKOHARJO</option>
                    <option value="Kabupaten wonogiri"> KABUPATEN WONOGIRI</option>
                    <option value="Kabupaten karanganyar"> KABUPATEN KARANGANYAR</option>
                    <option value="Kabupaten sragen"> KABUPATEN SRAGEN</option>
                    <option value="Kabupaten grobogan"> KABUPATEN GROBOGAN</option>
                    <option value="Kabupaten blora"> KABUPATEN BLORA</option>
                    <option value="Kabupaten rembang"> KABUPATEN REMBANG</option>
                    <option value="Kabupaten pati"> KABUPATEN PATI</option>
                    <option value="Kabupaten kudus"> KABUPATEN KUDUS</option>
                    <option value="Kabupaten jepara"> KABUPATEN JEPARA</option>
                    <option value="Kabupaten demak"> KABUPATEN DEMAK</option>
                    <option value="Kabupaten temanggung"> KABUPATEN TEMANGGUNG</option>
                    <option value="Kabupaten kendal"> KABUPATEN KENDAL</option>
                    <option value="Kabupaten batang"> KABUPATEN BATANG</option>
                    <option value="Kabupaten pekalongan"> KABUPATEN PEKALONGAN</option>
                    <option value="Kabupaten pemalang"> KABUPATEN PEMALANG</option>
                    <option value="Kabupaten tegal"> KABUPATEN TEGAL</option>
                    <option value="Kabupaten brebes"> KABUPATEN BREBES</option>
                    <option value="Kota magelang"> KOTA MAGELANG</option>
                    <option value="Kota surakarta"> KOTA SURAKARTA</option>
                    <option value="Kota salatiga"> KOTA SALATIGA</option>
                    <option value="Kota semarang"> KOTA SEMARANG</option>
                    <option value="Kota pekalongan"> KOTA PEKALONGAN</option>
                    <option value="Kota tegal"> KOTA TEGAL</option>
                    <option value="Kabupaten kulon progo"> KABUPATEN KULON PROGO</option>
                    <option value="Kabupaten bantul"> KABUPATEN BANTUL</option>
                    <option value="Kabupaten gunung kidul"> KABUPATEN GUNUNG KIDUL</option>
                    <option value="Kabupaten sleman"> KABUPATEN SLEMAN</option>
                    <option value="Kota yogyakarta"> KOTA YOGYAKARTA</option>
                    <option value="Kabupaten pacitan"> KABUPATEN PACITAN</option>
                    <option value="Kabupaten ponorogo"> KABUPATEN PONOROGO</option>
                    <option value="Kabupaten trenggalek"> KABUPATEN TRENGGALEK</option>
                    <option value="Kabupaten tulungagung"> KABUPATEN TULUNGAGUNG</option>
                    <option value="Kabupaten blitar"> KABUPATEN BLITAR</option>
                    <option value="Kabupaten kediri"> KABUPATEN KEDIRI</option>
                    <option value="Kabupaten malang"> KABUPATEN MALANG</option>
                    <option value="Kabupaten lumajang"> KABUPATEN LUMAJANG</option>
                    <option value="Kabupaten jember"> KABUPATEN JEMBER</option>
                    <option value="Kabupaten banyuwangi"> KABUPATEN BANYUWANGI</option>
                    <option value="Kabupaten bondowoso"> KABUPATEN BONDOWOSO</option>
                    <option value="Kabupaten situbondo"> KABUPATEN SITUBONDO</option>
                    <option value="Kabupaten probolinggo"> KABUPATEN PROBOLINGGO</option>
                    <option value="Kabupaten pasuruan"> KABUPATEN PASURUAN</option>
                    <option value="Kabupaten sidoarjo"> KABUPATEN SIDOARJO</option>
                    <option value="Kabupaten mojokerto"> KABUPATEN MOJOKERTO</option>
                    <option value="Kabupaten jombang"> KABUPATEN JOMBANG</option>
                    <option value="Kabupaten nganjuk"> KABUPATEN NGANJUK</option>
                    <option value="Kabupaten madiun"> KABUPATEN MADIUN</option>
                    <option value="Kabupaten magetan"> KABUPATEN MAGETAN</option>
                    <option value="Kabupaten ngawi"> KABUPATEN NGAWI</option>
                    <option value="Kabupaten bojonegoro"> KABUPATEN BOJONEGORO</option>
                    <option value="Kabupaten tuban"> KABUPATEN TUBAN</option>
                    <option value="Kabupaten lamongan"> KABUPATEN LAMONGAN</option>
                    <option value="Kabupaten gresik"> KABUPATEN GRESIK</option>
                    <option value="Kabupaten bangkalan"> KABUPATEN BANGKALAN</option>
                    <option value="Kabupaten sampang"> KABUPATEN SAMPANG</option>
                    <option value="Kabupaten pamekasan"> KABUPATEN PAMEKASAN</option>
                    <option value="Kabupaten sumenep"> KABUPATEN SUMENEP</option>
                    <option value="Kota kediri"> KOTA KEDIRI</option>
                    <option value="Kota blitar"> KOTA BLITAR</option>
                    <option value="Kota malang"> KOTA MALANG</option>
                    <option value="Kota probolinggo"> KOTA PROBOLINGGO</option>
                    <option value="Kota pasuruan"> KOTA PASURUAN</option>
                    <option value="Kota mojokerto"> KOTA MOJOKERTO</option>
                    <option value="Kota madiun"> KOTA MADIUN</option>
                    <option value="Kota surabaya"> KOTA SURABAYA</option>
                    <option value="Kota batu"> KOTA BATU</option>
                    <option value="Kabupaten pandeglang"> KABUPATEN PANDEGLANG</option>
                    <option value="Kabupaten lebak"> KABUPATEN LEBAK</option>
                    <option value="Kabupaten tangerang"> KABUPATEN TANGERANG</option>
                    <option value="Kabupaten serang"> KABUPATEN SERANG</option>
                    <option value="Kota tangerang"> KOTA TANGERANG</option>
                    <option value="Kota cilegon"> KOTA CILEGON</option>
                    <option value="Kota serang"> KOTA SERANG</option>
                    <option value="Kota tangerang selatan"> KOTA TANGERANG SELATAN</option>
                    <option value="Kabupaten jembrana"> KABUPATEN JEMBRANA</option>
                    <option value="Kabupaten tabanan"> KABUPATEN TABANAN</option>
                    <option value="Kabupaten badung"> KABUPATEN BADUNG</option>
                    <option value="Kabupaten gianyar"> KABUPATEN GIANYAR</option>
                    <option value="Kabupaten klungkung"> KABUPATEN KLUNGKUNG</option>
                    <option value="Kabupaten bangli"> KABUPATEN BANGLI</option>
                    <option value="Kabupaten karang asem"> KABUPATEN KARANG ASEM</option>
                    <option value="Kabupaten buleleng"> KABUPATEN BULELENG</option>
                    <option value="Kota denpasar"> KOTA DENPASAR</option>
                    <option value="Kabupaten lombok barat"> KABUPATEN LOMBOK BARAT</option>
                    <option value="Kabupaten lombok tengah"> KABUPATEN LOMBOK TENGAH</option>
                    <option value="Kabupaten lombok timur"> KABUPATEN LOMBOK TIMUR</option>
                    <option value="Kabupaten sumbawa"> KABUPATEN SUMBAWA</option>
                    <option value="Kabupaten dompu"> KABUPATEN DOMPU</option>
                    <option value="Kabupaten bima"> KABUPATEN BIMA</option>
                    <option value="Kabupaten sumbawa barat"> KABUPATEN SUMBAWA BARAT</option>
                    <option value="Kabupaten lombok utara"> KABUPATEN LOMBOK UTARA</option>
                    <option value="Kota mataram"> KOTA MATARAM</option>
                    <option value="Kota bima"> KOTA BIMA</option>
                    <option value="Kabupaten sumba barat"> KABUPATEN SUMBA BARAT</option>
                    <option value="Kabupaten sumba timur"> KABUPATEN SUMBA TIMUR</option>
                    <option value="Kabupaten kupang"> KABUPATEN KUPANG</option>
                    <option value="Kabupaten timor tengah selatan"> KABUPATEN TIMOR TENGAH SELATAN</option>
                    <option value="Kabupaten timor tengah utara"> KABUPATEN TIMOR TENGAH UTARA</option>
                    <option value="Kabupaten belu"> KABUPATEN BELU</option>
                    <option value="Kabupaten alor"> KABUPATEN ALOR</option>
                    <option value="Kabupaten lembata"> KABUPATEN LEMBATA</option>
                    <option value="Kabupaten flores timur"> KABUPATEN FLORES TIMUR</option>
                    <option value="Kabupaten sikka"> KABUPATEN SIKKA</option>
                    <option value="Kabupaten ende"> KABUPATEN ENDE</option>
                    <option value="Kabupaten ngada"> KABUPATEN NGADA</option>
                    <option value="Kabupaten manggarai"> KABUPATEN MANGGARAI</option>
                    <option value="Kabupaten rote ndao"> KABUPATEN ROTE NDAO</option>
                    <option value="Kabupaten manggarai barat"> KABUPATEN MANGGARAI BARAT</option>
                    <option value="Kabupaten sumba tengah"> KABUPATEN SUMBA TENGAH</option>
                    <option value="Kabupaten sumba barat daya"> KABUPATEN SUMBA BARAT DAYA</option>
                    <option value="Kabupaten nagekeo"> KABUPATEN NAGEKEO</option>
                    <option value="Kabupaten manggarai timur"> KABUPATEN MANGGARAI TIMUR</option>
                    <option value="Kabupaten sabu raijua"> KABUPATEN SABU RAIJUA</option>
                    <option value="Kabupaten malaka"> KABUPATEN MALAKA</option>
                    <option value="Kota kupang"> KOTA KUPANG</option>
                    <option value="Kabupaten sambas"> KABUPATEN SAMBAS</option>
                    <option value="Kabupaten bengkayang"> KABUPATEN BENGKAYANG</option>
                    <option value="Kabupaten landak"> KABUPATEN LANDAK</option>
                    <option value="Kabupaten mempawah"> KABUPATEN MEMPAWAH</option>
                    <option value="Kabupaten sanggau"> KABUPATEN SANGGAU</option>
                    <option value="Kabupaten ketapang"> KABUPATEN KETAPANG</option>
                    <option value="Kabupaten sintang"> KABUPATEN SINTANG</option>
                    <option value="Kabupaten kapuas hulu"> KABUPATEN KAPUAS HULU</option>
                    <option value="Kabupaten sekadau"> KABUPATEN SEKADAU</option>
                    <option value="Kabupaten melawi"> KABUPATEN MELAWI</option>
                    <option value="Kabupaten kayong utara"> KABUPATEN KAYONG UTARA</option>
                    <option value="Kabupaten kubu raya"> KABUPATEN KUBU RAYA</option>
                    <option value="Kota pontianak"> KOTA PONTIANAK</option>
                    <option value="Kota singkawang"> KOTA SINGKAWANG</option>
                    <option value="Kabupaten kotawaringin barat"> KABUPATEN KOTAWARINGIN BARAT</option>
                    <option value="Kabupaten kotawaringin timur"> KABUPATEN KOTAWARINGIN TIMUR</option>
                    <option value="Kabupaten kapuas"> KABUPATEN KAPUAS</option>
                    <option value="Kabupaten barito selatan"> KABUPATEN BARITO SELATAN</option>
                    <option value="Kabupaten barito utara"> KABUPATEN BARITO UTARA</option>
                    <option value="Kabupaten sukamara"> KABUPATEN SUKAMARA</option>
                    <option value="Kabupaten lamandau"> KABUPATEN LAMANDAU</option>
                    <option value="Kabupaten seruyan"> KABUPATEN SERUYAN</option>
                    <option value="Kabupaten katingan"> KABUPATEN KATINGAN</option>
                    <option value="Kabupaten pulang pisau"> KABUPATEN PULANG PISAU</option>
                    <option value="Kabupaten gunung mas"> KABUPATEN GUNUNG MAS</option>
                    <option value="Kabupaten barito timur"> KABUPATEN BARITO TIMUR</option>
                    <option value="Kabupaten murung raya"> KABUPATEN MURUNG RAYA</option>
                    <option value="Kota palangka raya"> KOTA PALANGKA RAYA</option>
                    <option value="Kabupaten tanah laut"> KABUPATEN TANAH LAUT</option>
                    <option value="Kabupaten kota baru"> KABUPATEN KOTA BARU</option>
                    <option value="Kabupaten banjar"> KABUPATEN BANJAR</option>
                    <option value="Kabupaten barito kuala"> KABUPATEN BARITO KUALA</option>
                    <option value="Kabupaten tapin"> KABUPATEN TAPIN</option>
                    <option value="Kabupaten hulu sungai selatan"> KABUPATEN HULU SUNGAI SELATAN</option>
                    <option value="Kabupaten hulu sungai tengah"> KABUPATEN HULU SUNGAI TENGAH</option>
                    <option value="Kabupaten hulu sungai utara"> KABUPATEN HULU SUNGAI UTARA</option>
                    <option value="Kabupaten tabalong"> KABUPATEN TABALONG</option>
                    <option value="Kabupaten tanah bumbu"> KABUPATEN TANAH BUMBU</option>
                    <option value="Kabupaten balangan"> KABUPATEN BALANGAN</option>
                    <option value="Kota banjarmasin"> KOTA BANJARMASIN</option>
                    <option value="Kota banjar baru"> KOTA BANJAR BARU</option>
                    <option value="Kabupaten paser"> KABUPATEN PASER</option>
                    <option value="Kabupaten kutai barat"> KABUPATEN KUTAI BARAT</option>
                    <option value="Kabupaten kutai kartanegara"> KABUPATEN KUTAI KARTANEGARA</option>
                    <option value="Kabupaten kutai timur"> KABUPATEN KUTAI TIMUR</option>
                    <option value="Kabupaten berau"> KABUPATEN BERAU</option>
                    <option value="Kabupaten penajam paser utara"> KABUPATEN PENAJAM PASER UTARA</option>
                    <option value="Kabupaten mahakam hulu"> KABUPATEN MAHAKAM HULU</option>
                    <option value="Kota balikpapan"> KOTA BALIKPAPAN</option>
                    <option value="Kota samarinda"> KOTA SAMARINDA</option>
                    <option value="Kota bontang"> KOTA BONTANG</option>
                    <option value="Kabupaten malinau"> KABUPATEN MALINAU</option>
                    <option value="Kabupaten bulungan"> KABUPATEN BULUNGAN</option>
                    <option value="Kabupaten tana tidung"> KABUPATEN TANA TIDUNG</option>
                    <option value="Kabupaten nunukan"> KABUPATEN NUNUKAN</option>
                    <option value="Kota tarakan"> KOTA TARAKAN</option>
                    <option value="Kabupaten bolaang mongondow"> KABUPATEN BOLAANG MONGONDOW</option>
                    <option value="Kabupaten minahasa"> KABUPATEN MINAHASA</option>
                    <option value="Kabupaten kepulauan sangihe"> KABUPATEN KEPULAUAN SANGIHE</option>
                    <option value="Kabupaten kepulauan talaud"> KABUPATEN KEPULAUAN TALAUD</option>
                    <option value="Kabupaten minahasa selatan"> KABUPATEN MINAHASA SELATAN</option>
                    <option value="Kabupaten minahasa utara"> KABUPATEN MINAHASA UTARA</option>
                    <option value="Kabupaten bolaang mongondow utara"> KABUPATEN BOLAANG MONGONDOW UTARA</option>
                    <option value="Kabupaten siau tagulandang biaro"> KABUPATEN SIAU TAGULANDANG BIARO</option>
                    <option value="Kabupaten minahasa tenggara"> KABUPATEN MINAHASA TENGGARA</option>
                    <option value="Kabupaten bolaang mongondow selatan"> KABUPATEN BOLAANG MONGONDOW SELATAN</option>
                    <option value="Kabupaten bolaang mongondow timur"> KABUPATEN BOLAANG MONGONDOW TIMUR</option>
                    <option value="Kota manado"> KOTA MANADO</option>
                    <option value="Kota bitung"> KOTA BITUNG</option>
                    <option value="Kota tomohon"> KOTA TOMOHON</option>
                    <option value="Kota kotamobagu"> KOTA KOTAMOBAGU</option>
                    <option value="Kabupaten banggai kepulauan"> KABUPATEN BANGGAI KEPULAUAN</option>
                    <option value="Kabupaten banggai"> KABUPATEN BANGGAI</option>
                    <option value="Kabupaten morowali"> KABUPATEN MOROWALI</option>
                    <option value="Kabupaten poso"> KABUPATEN POSO</option>
                    <option value="Kabupaten donggala"> KABUPATEN DONGGALA</option>
                    <option value="Kabupaten toli-toli"> KABUPATEN TOLI-TOLI</option>
                    <option value="Kabupaten buol"> KABUPATEN BUOL</option>
                    <option value="Kabupaten parigi moutong"> KABUPATEN PARIGI MOUTONG</option>
                    <option value="Kabupaten tojo una-una"> KABUPATEN TOJO UNA-UNA</option>
                    <option value="Kabupaten sigi"> KABUPATEN SIGI</option>
                    <option value="Kabupaten banggai laut"> KABUPATEN BANGGAI LAUT</option>
                    <option value="Kabupaten morowali utara"> KABUPATEN MOROWALI UTARA</option>
                    <option value="Kota palu"> KOTA PALU</option>
                    <option value="Kabupaten kepulauan selayar"> KABUPATEN KEPULAUAN SELAYAR</option>
                    <option value="Kabupaten bulukumba"> KABUPATEN BULUKUMBA</option>
                    <option value="Kabupaten bantaeng"> KABUPATEN BANTAENG</option>
                    <option value="Kabupaten jeneponto"> KABUPATEN JENEPONTO</option>
                    <option value="Kabupaten takalar"> KABUPATEN TAKALAR</option>
                    <option value="Kabupaten gowa"> KABUPATEN GOWA</option>
                    <option value="Kabupaten sinjai"> KABUPATEN SINJAI</option>
                    <option value="Kabupaten maros"> KABUPATEN MAROS</option>
                    <option value="Kabupaten pangkajene dan kepulauan"> KABUPATEN PANGKAJENE DAN KEPULAUAN</option>
                    <option value="Kabupaten barru"> KABUPATEN BARRU</option>
                    <option value="Kabupaten bone"> KABUPATEN BONE</option>
                    <option value="Kabupaten soppeng"> KABUPATEN SOPPENG</option>
                    <option value="Kabupaten wajo"> KABUPATEN WAJO</option>
                    <option value="Kabupaten sidenreng rappang"> KABUPATEN SIDENRENG RAPPANG</option>
                    <option value="Kabupaten pinrang"> KABUPATEN PINRANG</option>
                    <option value="Kabupaten enrekang"> KABUPATEN ENREKANG</option>
                    <option value="Kabupaten luwu"> KABUPATEN LUWU</option>
                    <option value="Kabupaten tana toraja"> KABUPATEN TANA TORAJA</option>
                    <option value="Kabupaten luwu utara"> KABUPATEN LUWU UTARA</option>
                    <option value="Kabupaten luwu timur"> KABUPATEN LUWU TIMUR</option>
                    <option value="Kabupaten toraja utara"> KABUPATEN TORAJA UTARA</option>
                    <option value="Kota makassar"> KOTA MAKASSAR</option>
                    <option value="Kota parepare"> KOTA PAREPARE</option>
                    <option value="Kota palopo"> KOTA PALOPO</option>
                    <option value="Kabupaten buton"> KABUPATEN BUTON</option>
                    <option value="Kabupaten muna"> KABUPATEN MUNA</option>
                    <option value="Kabupaten konawe"> KABUPATEN KONAWE</option>
                    <option value="Kabupaten kolaka"> KABUPATEN KOLAKA</option>
                    <option value="Kabupaten konawe selatan"> KABUPATEN KONAWE SELATAN</option>
                    <option value="Kabupaten bombana"> KABUPATEN BOMBANA</option>
                    <option value="Kabupaten wakatobi"> KABUPATEN WAKATOBI</option>
                    <option value="Kabupaten kolaka utara"> KABUPATEN KOLAKA UTARA</option>
                    <option value="Kabupaten buton utara"> KABUPATEN BUTON UTARA</option>
                    <option value="Kabupaten konawe utara"> KABUPATEN KONAWE UTARA</option>
                    <option value="Kabupaten kolaka timur"> KABUPATEN KOLAKA TIMUR</option>
                    <option value="Kabupaten konawe kepulauan"> KABUPATEN KONAWE KEPULAUAN</option>
                    <option value="Kabupaten muna barat"> KABUPATEN MUNA BARAT</option>
                    <option value="Kabupaten buton tengah"> KABUPATEN BUTON TENGAH</option>
                    <option value="Kabupaten buton selatan"> KABUPATEN BUTON SELATAN</option>
                    <option value="Kota kendari"> KOTA KENDARI</option>
                    <option value="Kota baubau"> KOTA BAUBAU</option>
                    <option value="Kabupaten boalemo"> KABUPATEN BOALEMO</option>
                    <option value="Kabupaten gorontalo"> KABUPATEN GORONTALO</option>
                    <option value="Kabupaten pohuwato"> KABUPATEN POHUWATO</option>
                    <option value="Kabupaten bone bolango"> KABUPATEN BONE BOLANGO</option>
                    <option value="Kabupaten gorontalo utara"> KABUPATEN GORONTALO UTARA</option>
                    <option value="Kota gorontalo"> KOTA GORONTALO</option>
                    <option value="Kabupaten majene"> KABUPATEN MAJENE</option>
                    <option value="Kabupaten polewali mandar"> KABUPATEN POLEWALI MANDAR</option>
                    <option value="Kabupaten mamasa"> KABUPATEN MAMASA</option>
                    <option value="Kabupaten mamuju"> KABUPATEN MAMUJU</option>
                    <option value="Kabupaten mamuju utara"> KABUPATEN MAMUJU UTARA</option>
                    <option value="Kabupaten mamuju tengah"> KABUPATEN MAMUJU TENGAH</option>
                    <option value="Kabupaten maluku tenggara barat"> KABUPATEN MALUKU TENGGARA BARAT</option>
                    <option value="Kabupaten maluku tenggara"> KABUPATEN MALUKU TENGGARA</option>
                    <option value="Kabupaten maluku tengah"> KABUPATEN MALUKU TENGAH</option>
                    <option value="Kabupaten buru"> KABUPATEN BURU</option>
                    <option value="Kabupaten kepulauan aru"> KABUPATEN KEPULAUAN ARU</option>
                    <option value="Kabupaten seram bagian barat"> KABUPATEN SERAM BAGIAN BARAT</option>
                    <option value="Kabupaten seram bagian timur"> KABUPATEN SERAM BAGIAN TIMUR</option>
                    <option value="Kabupaten maluku barat daya"> KABUPATEN MALUKU BARAT DAYA</option>
                    <option value="Kabupaten buru selatan"> KABUPATEN BURU SELATAN</option>
                    <option value="Kota ambon"> KOTA AMBON</option>
                    <option value="Kota tual"> KOTA TUAL</option>
                    <option value="Kabupaten halmahera barat"> KABUPATEN HALMAHERA BARAT</option>
                    <option value="Kabupaten halmahera tengah"> KABUPATEN HALMAHERA TENGAH</option>
                    <option value="Kabupaten kepulauan sula"> KABUPATEN KEPULAUAN SULA</option>
                    <option value="Kabupaten halmahera selatan"> KABUPATEN HALMAHERA SELATAN</option>
                    <option value="Kabupaten halmahera utara"> KABUPATEN HALMAHERA UTARA</option>
                    <option value="Kabupaten halmahera timur"> KABUPATEN HALMAHERA TIMUR</option>
                    <option value="Kabupaten pulau morotai"> KABUPATEN PULAU MOROTAI</option>
                    <option value="Kabupaten pulau taliabu"> KABUPATEN PULAU TALIABU</option>
                    <option value="Kota ternate"> KOTA TERNATE</option>
                    <option value="Kota tidore kepulauan"> KOTA TIDORE KEPULAUAN</option>
                    <option value="Kabupaten fakfak"> KABUPATEN FAKFAK</option>
                    <option value="Kabupaten kaimana"> KABUPATEN KAIMANA</option>
                    <option value="Kabupaten teluk wondama"> KABUPATEN TELUK WONDAMA</option>
                    <option value="Kabupaten teluk bintuni"> KABUPATEN TELUK BINTUNI</option>
                    <option value="Kabupaten manokwari"> KABUPATEN MANOKWARI</option>
                    <option value="Kabupaten sorong selatan"> KABUPATEN SORONG SELATAN</option>
                    <option value="Kabupaten sorong"> KABUPATEN SORONG</option>
                    <option value="Kabupaten raja ampat"> KABUPATEN RAJA AMPAT</option>
                    <option value="Kabupaten tambrauw"> KABUPATEN TAMBRAUW</option>
                    <option value="Kabupaten maybrat"> KABUPATEN MAYBRAT</option>
                    <option value="Kabupaten manokwari selatan"> KABUPATEN MANOKWARI SELATAN</option>
                    <option value="Kabupaten pegunungan arfak"> KABUPATEN PEGUNUNGAN ARFAK</option>
                    <option value="Kota sorong"> KOTA SORONG</option>
                    <option value="Kabupaten merauke"> KABUPATEN MERAUKE</option>
                    <option value="Kabupaten jayawijaya"> KABUPATEN JAYAWIJAYA</option>
                    <option value="Kabupaten jayapura"> KABUPATEN JAYAPURA</option>
                    <option value="Kabupaten nabire"> KABUPATEN NABIRE</option>
                    <option value="Kabupaten kepulauan yapen"> KABUPATEN KEPULAUAN YAPEN</option>
                    <option value="Kabupaten biak numfor"> KABUPATEN BIAK NUMFOR</option>
                    <option value="Kabupaten paniai"> KABUPATEN PANIAI</option>
                    <option value="Kabupaten puncak jaya"> KABUPATEN PUNCAK JAYA</option>
                    <option value="Kabupaten mimika"> KABUPATEN MIMIKA</option>
                    <option value="Kabupaten boven digoel"> KABUPATEN BOVEN DIGOEL</option>
                    <option value="Kabupaten mappi"> KABUPATEN MAPPI</option>
                    <option value="Kabupaten asmat"> KABUPATEN ASMAT</option>
                    <option value="Kabupaten yahukimo"> KABUPATEN YAHUKIMO</option>
                    <option value="Kabupaten pegunungan bintang"> KABUPATEN PEGUNUNGAN BINTANG</option>
                    <option value="Kabupaten tolikara"> KABUPATEN TOLIKARA</option>
                    <option value="Kabupaten sarmi"> KABUPATEN SARMI</option>
                    <option value="Kabupaten keerom"> KABUPATEN KEEROM</option>
                    <option value="Kabupaten waropen"> KABUPATEN WAROPEN</option>
                    <option value="Kabupaten supiori"> KABUPATEN SUPIORI</option>
                    <option value="Kabupaten mamberamo raya"> KABUPATEN MAMBERAMO RAYA</option>
                    <option value="Kabupaten nduga"> KABUPATEN NDUGA</option>
                    <option value="Kabupaten lanny jaya"> KABUPATEN LANNY JAYA</option>
                    <option value="Kabupaten mamberamo tengah"> KABUPATEN MAMBERAMO TENGAH</option>
                    <option value="Kabupaten yalimo"> KABUPATEN YALIMO</option>
                    <option value="Kabupaten puncak"> KABUPATEN PUNCAK</option>
                    <option value="Kabupaten dogiyai"> KABUPATEN DOGIYAI</option>
                    <option value="Kabupaten intan jaya"> KABUPATEN INTAN JAYA</option>
                    <option value="Kabupaten deiyai"> KABUPATEN DEIYAI</option>
                    <option value="Kota jayapura"> KOTA JAYAPURA</option>
                    <option value="Surabaya timur"> SURABAYA TIMUR</option>            
                  </select>
                </div>
            
                <div class="form-group">
                  <label for="inputState" style="font-size: 12px; color: #8898aa"><?=lang('Global.address');?> *</label>
                  <textarea class="form-control" id="address" name="address" rows="3" required></textarea>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" placeholder="Email *" type="email" id="email" name="email" value="" required="">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="form-control" placeholder="Password *" type="password" id="password" name="password" value="" required="">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="form-control" placeholder="<?=lang('Global.confirm_password');?> *" type="password" id="password_confirm" name="password_confirm" value="" required="">
                  </div>
                </div>
                <input type="hidden" name="volunteer" id="volunteer" value="1">
                <!-- <div class="text-muted font-italic"><small>password strength: <span class="text-success font-weight-700">strong</span></small></div> -->
               <!--  <div class="row my-4">
                  <div class="col-12">
                    <div class="custom-control custom-control-alternative custom-checkbox">
                      <input class="custom-control-input" id="customCheckRegister" type="checkbox">
                      <label class="custom-control-label" for="customCheckRegister">
                        <span class="text-muted">I agree with the <a href="#!">Privacy Policy</a></span>
                      </label>
                    </div>
                  </div>
                </div> -->
                <div class="text-center">
                  <button type="submit" name="submit" value="Create User" class="btn btn-primary mt-4"><?=lang('Global.being_volunteer');?></button>
                </div>
              </form>
            </div>
          </div>
          <div class="row mt-3">
            <div class="col-12 text-center text-sm-right">
              <a href="<?= base_url('auth/login'); ?>" class="text-light"><small>Login</small></a>
            </div>
            <!-- <div class="col-6 text-right">
              <a href="<?= base_url('auth/create_user'); ?>" class="text-light"><small>Create new account</small></a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
 
  
  <!-- Footer -->
  <br /><br />
  

<!-- Awal Footer -->
 <?php include('footer.php') ?> 
<!-- Akhir Footer -->

  <!-- Argon Scripts -->
  <!-- Core -->
  
  <script src="<?php echo base_url('src/assets/vendor/jquery/dist/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/js-cookie/js.cookie.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js'); ?>"></script>
  <!-- Argon JS -->
  <script src="<?php echo base_url('src/assets/js/argon.js?v=1.2.0'); ?>"></script>
  <!-- js select picker -->
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

  <!-- phone number masking -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
<script>
$(document).ready(function(){
        $('.phone_number').inputmask('999-9999-99999');
    });  
</script>

  <script type="text/javascript">
    $(".js-example-basic-single").select2({

theme: "classic",

debug: "false"

});



  (function () {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll('.needs-validation')


  // Loop over them and prevent submission
  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }
        form.classList.add('was-validated')
      }, false)
    })
})()

//Reference: https://jsfiddle.net/fwv18zo1/
var $select1 = $( '#education_level' ),
		$select2 = $( '#semester' ),
    $options = $select2.find( 'option' );
    
$select1.on( 'change', function() {
	$select2.html( $options.filter( '[class="' + this.value + '"]' ) );
} ).trigger( 'change' );

// cek umur
  var $birthdate = $( '#birthdate' );
  $birthdate.on( 'change', function() {
  var today = new Date();
    var birthDate = new Date($(this).val());
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
    {
        age--;
    }
    if (age>=15 && age <=100){
      document.getElementById('age_alert').innerHTML
                = '';
    }  else if (age>100){
      document.getElementById('age_alert').innerHTML = 'Perhatian, harap isi dengan benar';
    }
    else {
      document.getElementById('age_alert').innerHTML = 'Perhatian, usia minimal 15 tahun';
    }
  })




  </script>

</body>

</html>