<!-- Start Footer -->
  <footer class="footer" style="background-color: #FDC834; color: #232941">

    <div class="container">
       
       <div class="row pt-md-0">
         
          <div class="col-12 col-md-6 col-lg-4 text-center text-md-left pl-5 pr-5 pl-lg-0 pr-lg-0">
            <img src="<?php echo base_url('src/assets/img/brand/pipe-blue.png'); ?>" class="img-fluid">
          </div>

          <div class="col-2 d-none d-lg-block"></div>

          <div class="col-12 col-md-6 mt-3 mt-md-0 text-center text-md-right">
            <p style="font-weight: 500">Jl. Boulevard Timur Raya Blok NE1 No. 58,<br class="d-none d-lg-block"> Kelapa Gading, Jakarta Utara - 14240</p>
            <i class="fab fa-facebook-square mr-2"></i>
            <i class="fab fa-instagram mr-2"></i>
            <i class="far fa-envelope"></i>
          </div>

       </div> 

       <hr style="background-color: #25305A" class="mt-4 mb-4">

       <p class="copyright text-center pt-1 mb-1">©Copyright 2021 <b>Pipe Psikotes</b> | All Rights Reserved.</p>

       <div class="nav-footer text-center">
         <a href="">Disclaimer |</a>
         <a href="">Sitemap |</a>
         <a href="">Kebijakan Privasi |</a>
         <a href="">Kebijakan Pelayanan</a>
       </div>

    </div>

  </footer>
<!-- End Footer -->