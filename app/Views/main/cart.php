<div class="main-content">
    <!-- Header -->
     <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 col-md-8 px-5">
              <h1 class="text-white"><?=lang('Global.cart');?></h1>
              <p class="text-lead text-white"></p>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-8 order-xl-1">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-12">
                <h3 class="mb-0"><?=lang('Global.cart_detail');?></h3>
                  <br>
                  <?php if(session()->getFlashdata('success') != null){ ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" id="success-alert">
                      <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                      <span class="alert-inner--text"><strong>Success!</strong><?php echo session()->getFlashdata('success'); ?></span>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <?php } ?>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table">
                <?php if(count($items) != 0){ // cek apakan keranjang belanja lebih dari 0, jika iya maka tampilkan list dalam bentuk table di bawah ini: ?>
                <?php echo form_open('cart/update'); ?>
                <table id="" class="table" width="100%" >
                  <thead class="thead-light">
                    <tr>
                      <th class="th-sm">No
                      </th>
                      <th class="th-sm"><?=lang('Global.paket_tes')?>
                      </th>
                      <th class="th-sm"><?=lang('Global.harga')?>
                      </th>
                      <th class="th-sm">Action
                      </th>
                    </tr>
                  </thead>
                  <tbody class="list">
                    <!-- awal baris -->
                    <?php foreach($items as $key => $item) { ?>
                    <tr>
                      <td>
                        <?php echo $key + 1; ?>
                      </td>
                      <td>
                        <div class="media-body">
                            <span><?php echo $item['nama']; ?></span>
                          </div>
                        </div>
                      </td>
                      <td>
                        <span>Rp. <?php echo number_format($item['harga'], 0, 0, '.'); ?></span>
                      </td>
                      <td>
                        <a href="<?php echo base_url('cart/remove/'.$item['id']); ?>" class="btn btn-danger btn-sm" onclick="return confirm('<?=lang('Global.delete_confirmation')?>')"><i class="fa fa-trash"></i> <?=lang('Global.delete')?></a>
                      </td>
                    </tr>
                    <?php } ?>
                    <!-- akhir baris -->
                  </tbody>
                </table>
                <?php echo form_close(); ?>
                <?php } // selesai menampilkan list cart dalam bentuk table ?>
                <?php if(count($items) == 0){ // jika cart kosong, maka tampilkan: ?>
                    <?=lang('Global.cart_kosong')?><a href="<?php echo base_url('/product'); ?>" class="btn btn-success"><?=lang('Global.lihat_semua_paket')?></a>
                    <?php } else { // jika cart tidak kosong, tampilkan: ?>
                        <a href="<?php echo base_url('product'); ?>" class="btn btn-success"><?=lang('Global.lanjut_belanja')?></a>
                    <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-4 order-xl-2">
          <div class="card card-profile">
            <div class="row">
              <div class="card-body pt-20">
                <div class="text-center">
                  <h3 class="h1">
                    <p class="opacity-8"><?=lang('Global.total_harga')?> : </p>
                    Rp. <?php echo number_format($total, 0, 0, '.'); ?> <!-- <del class="h5 opacity-5">Rp. 250.000</del> -->
                  </h3>
                  <div class="h1 mt-4">
                    <a href="<?= base_url('cart/checkout'); ?>" class="btn btn-primary"><!-- <i class="fa fa-shopping-cart"></i> --> Checkout</a>
                  </div>
                  <p class="opacity-8" style="font-size: 12px;"><?=lang('Global.harga_desc')?></p>
                </div>
              </div>
            </div>
            <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center">
                    <div>
                      <span class="heading">12540</span>
                      <span class="description">Dilihat</span>
                    </div>
                    <div>
                      <span class="heading">754</span>
                      <span class="description">Peserta</span>
                    </div>
                    <div>
                      <span class="heading">680</span>
                      <span class="description">Dibagikan</span>
                    </div>
                  </div>
                </div>
              </div> -->
          </div>
        </div>
    </div>
    <div class="section features-1" style="background-color: #232941; color: #FDC834">
      <div class="container">
        <div class="text-center" style="padding-top: 12px; padding-bottom: 12px; ">
            <h4 class="display-4" style="color: #FDC834"><?=lang('Global.lihat_semua_paket')?></h4>
        </div>
        <div class="row">
          <?php if(!empty($products)) {
            foreach($products as $key => $item) { ?>
                <div class="col-md-3 text-center">
                  <div class="info">
                    <span class="avatar avatar-lg rounded-circle">
                    <img alt="Image placeholder" src="<?= base_url('src/assets/img/product/'.$item['id'].'.png') ?>">
                  </span>
                    <a href="<?php echo base_url('product/id/'.$item['id']) ?>"><h6 class="info-title text-uppercase" style="color: #FDC834; font-size: 1rem;"><?= $item['nama']; ?></h6></a>
                    <p class="description opacity-8"><?= $item['short_desc']; ?></p>
                   
                  </div>
                  <br>
                </div>    
          <?php }
          } ?>
        </div>
        <div class="row">
          <?php if(!empty($products)) {
            foreach($products as $key => $item) { ?>
                <div class="col-md-3 text-center">
                  <div class="info">
                    <a href="<?php echo base_url('product/id/'.$item['id']) ?>" class="btn btn-primary btn-icon mt-3 mb-sm-0">
                      <span class="btn-inner--icon"><?=lang('Global.detail');?></span>
                      <!-- <span class="btn-inner--text">Learn more</span> -->
                    </a>
                  </div>
                  <br>
                </div>    
          <?php }
          } ?>
        </div>
        <div class="container">
          
        </div>
      </div>
    </div>
        
</div>
