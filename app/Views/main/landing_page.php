
<div class="section section-hero section-shaped" style="background-image: url('src/assets/img/brand/header-bg.svg');background-repeat: no-repeat;
  background-size: cover;
  height: 100%;">
      <div class="shape shape-style-3 shape-default">
      </div>
      <div class="page-header">
        <div class="container shape-container d-flex align-items-center py-md mt-2">
          <div class="col px-0">
            <div class="row align-items-center justify-content-center">
              <div class="col-lg-12 text-center" style="font-color: #232941" >
                <div class="mt-md-5 mt-4 pt-4 pt-md-4">
                    <img alt="Image placeholder" style="height: 150px; width: 150px;" src="<?= base_url('src/assets/img/pdf/pipe-color.png') ?>">
                  </div>
                <h2 style="font-size: 50px; font-weight: 700"><?=lang('Global.welcome');?></h2>
                <p class="pt-2 pl-1 pr-1" style="font-weight: 400"><?=lang('Global.welcome_desc');?></p>
                <p class="pt-2" style="font-weight: 600"><?=lang('Global.welcome_desc_klickbtn');?></p>
                <div class="btn-wrapper" >
                  <a href="<?php if($isMember) {echo base_url('dashboard');} else {echo base_url('Tesiqdanpenjurusangratis');} ?>" class="btn btn-primary btn-icon mt-3 mb-sm-0">
                    <span class="btn-inner--icon pl-4 pr-4">IKUT TES GRATIS</span>
                    <!-- <span class="btn-inner--text">Learn more</span> -->
                  </a>
                 <!--  <a href="#" class="btn btn-secondary btn-icon mt-3 mb-sm-0">
                    <span class="btn-inner--icon"><i class="ni ni-atom"></i> <?=lang('Global.what_pipe');?></span>
                  </a>
                  <a href="#" class="btn btn-secondary btn-icon mt-3 mb-sm-0">
                    <span class="btn-inner--icon"><i class="ni ni-satisfied"></i> <?=lang('Global.our_expert');?></span>
                  </a> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>

    <div class="section features-1" style="background-color: #232941; color: #FDC834" id="menu-test">
      <div class="container">
        <h2 class="text-center text-menu-test">MENU LAYANAN TES PSIKOTES GRATIS</h2>
        <div class="row">
          <?php if(!empty($products)) {
            foreach($products as $key => $item) { 
                if ($item['id']==1){ ?>
                <div class="col-lg-6 mx-auto pt-4 text-center d-flex flex-column" style="padding-top: 12px; padding-bottom: 24px;">
                  <div class="info">
                    <span class="avatar avatar-lg rounded-circle">
                    <img alt="Image placeholder" src="<?= base_url('src/assets/img/product/'.$item['id'].'.png') ?>">
                  </span>
                    <a href="#menu-test"><h6 class="info-title text-uppercase" style="color: #FDC834"><?= $item['nama']; ?></h6></a>
                    <p class="description opacity-8" style="font-weight: 400"><?= $item['short_desc']; ?></p>
                  </div>
                  <br>
                  <a href="#menu-test">
                    <?php if ($item['id'] > 1): ?>
                      <a href="#menu-test" class="mt-auto btn btn-primary btn-icon mb-sm-0">
                    <?php endif ?>
                    <?php if ($item['id'] == 1): ?>
                    <a href="<?php if($isMember) {echo base_url('dashboard');} else {echo base_url('Tesiqdanpenjurusangratis');} ?>" class="btn btn-primary btn-icon mb-sm-0">
                    <?php endif ?>
                    <!-- <span class="btn-inner--icon"><?=lang('Global.detail');?></span> -->
                    <span class="btn-inner--icon">IKUT TES GRATIS</span>
                    <!-- <span class="btn-inner--text">Learn more</span> -->
                  </a>
                     
                </div>    
          <?php } }
          } ?>
        </div>
        <div class="container">
          
        </div>
      </div>
    </div>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" hidden="">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="<?php echo base_url('/public/uploads/banner_services/main.png'); ?>" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="<?php echo base_url('/public/uploads/banner_services/1.png'); ?>" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="<?php echo base_url('/public/uploads/banner_services/2.png'); ?>" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
     <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel" hidden="">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <a target="_blank" href="http://www.edubridge.co.id/destination.php">
          <img class="d-block w-100" src="<?php echo base_url('/public/uploads/banner_step_kuliah/main.png'); ?>" alt="First slide">
          </a>
        </div>
        <div class="carousel-item">
          <a target="_blank" href="http://www.edubridge.co.id/destination.php">
          <img class="d-block w-100" src="<?php echo base_url('/public/uploads/banner_step_kuliah/1.png'); ?>" alt="Second slide">
          </a>
        </div>
        <div class="carousel-item">
          <a target="_blank" href="http://www.edubridge.co.id/destination.php">
          <img class="d-block w-100" src="<?php echo base_url('/public/uploads/banner_step_kuliah/2.png'); ?>" alt="Third slide">
          </a>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    </div>


    
    <!-- testimonial / feedback-->
   <?php
   if(false){
     if(!empty($feedbacks)) {
       foreach($feedbacks as $key => $feedback) { ?>
                  <div class="col-md-3">
                    <div class="info">
                      <div class="icon icon-shape bg-primary rounded-circle text-white">
                        <i class="ni ni-app"></i>
                      </div>
                      <a href="<?php echo base_url('product/id/'.$feedback['id']) ?>"><h6 class="info-title text-uppercase text-primary"><?= $feedback['first_name'].' '.$feedback['last_name']; ?></h6></a>
                      <p class="description opacity-8"><?= $feedback['feedback']; ?></p>
                      <a href="<?php echo base_url('product/id/'.$feedback['id']) ?>" class="text-primary"><?=lang('Global.detail');?>
                      <i class="ni ni-bold-right text-primary"></i>
                    </a>
                  </div>
                  <br>
                </div>    
                <?php }
            } 
    } 
  ?>  
    <div class="section features-1" hidden="">
      <div class="container">
        <div class="row">
          <div class="col-md-8 mx-auto text-center">
            <span class="badge badge-primary badge-pill mb-3">Insight</span>
            <h4 class="display-4"><?=lang('Global.insight');?></h4>
            <p class=""><?=lang('Global.insight_desc');?></p>
          </div>
        </div>
      </div>
    </div>
    
