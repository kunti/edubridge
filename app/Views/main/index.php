<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="PIPE (Potential, Interest, Personality) Psikotes adalah sebuah layanan tes minat dan kecerdasan berbasis online pertama di Indonesia untuk mengenali potensi akademis dan ketertarikan yang ditujukan membantu pemetaan jurusan kuliah yang sesuai sehingga siswa mampu melakukan perencanaan dan pilihan yang tepat untuk masa depannya.">
  <meta name="keywords" content="tes iq, tes iq online, psikotes online, tes minat bakat, pipe psikotes, psikotes gratis, tes iq eq, tes bakat online, tes kecerdasan">
  <meta name="author" content="PIPE Psikotes">
  <meta name="google-site-verification" content="6i_Tp4lLdjmAfKDwNMm_famEJkDz2O2dlXU05u72cd0" />
  <title>PIPE Psikotes | Layanan tes minat dan kecerdasan berbasis online</title>
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/png" href="<?php echo base_url('src/assets/img/brand/favicon.png'); ?>">
  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <!-- Nucleo Icons -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets_landing/css/nucleo-icons.css'); ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('src/assets_landing/css/nucleo-svg.css'); ?>" type="text/css">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets_landing/css/font-awesome.css'); ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('src/assets_landing/css/nucleo-svg.css'); ?>" type="text/css">
  <!-- CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets_landing/css/argon-design-system.css'); ?>" type="text/css">

  <!-- STYLE TAMBAHAN -->
  <link rel="stylesheet" href="<?php echo base_url('src/style_tambahan/style-tambahan.css'); ?>" type="text/css">

  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel='stylesheet'>

  <!-- whatsapp style  -->
  <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-196349671-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-196349671-2');
</script>

  <style type="text/css">
    body {
  font-family: 'Montserrat';
}

    .float{
    position:fixed;
    width:60px;
    height:60px;
    bottom:40px;
    right:40px;
    background-color:#25d366;
    color:#FFF;
    border-radius:50px;
    text-align:center;
    font-size:30px;
    box-shadow: 2px 2px 3px #999;
    z-index:100;
  }

  .my-float{
    margin-top:16px;
  }

  .carousel-text {
    position: absolute;
  right: 15%;
  bottom: 20px;
  left: 15%;
  z-index: 10;
  padding-top: 20px;
  padding-bottom: 20px;
  color: #fff;
  text-align: center;
  }

  .navbar-nav .nav-link {
    font-family: 'montserrat';
  }

.navbar-transparent .navbar-nav .nav-link {
    color: #232941;
}

.navbar-transparent .navbar-nav .nav-link:hover, .navbar-transparent .navbar-nav .nav-link:focus {
    /*color: rgba(0, 0, 0, 0.5);*/
            color: #FDC834;
    /*color: rgba(255, 255, 255, 0.65);*/
}
  </style>
  
</head>
<body class="landing-page">



<!-- Awal Navbar -->
 <?php include('navbar.php') ?> 
<!-- Akhir Navbar -->

<!-- Template Content -->
 <?php if (isset($template)) echo view($template); ?>
<!-- Template Content -->

<!-- Awal Footer -->
 <?php include('footer.php') ?> 
<!-- Akhir Footer -->






<!--   Core JS Files   -->
<script src="<?php echo base_url('src/assets_landing/js/core/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('src/assets_landing/js/core/popper.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('src/assets_landing/js/core/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('src/assets_landing/js/plugins/perfect-scrollbar.jquery.min.js'); ?>"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="<?php echo base_url('src/assets_landing/js/plugins/bootstrap-switch.js'); ?>"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="<?php echo base_url('src/assets_landing/js/plugins/nouislider.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('src/assets_landing/js/plugins/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('src/assets_landing/js/plugins/datetimepicker.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('src/assets_landing/js/plugins/bootstrap-datepicker.min.js'); ?>"></script>
    <!-- Control Center for Argon UI Kit: parallax effects, scripts for the example pages etc -->
    <!--  Google Maps Plugin    -->
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <script src="<?php echo base_url('src/assets_landing/js/argon-design-system.min.js'); ?>" type="text/javascript"></script>
    <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
  <script>
    



    function scrollToDownload() {

      if ($('.section-download').length != 0) {
        $("html, body").animate({
          scrollTop: $('.section-download').offset().top
        }, 1000);
      }
    }
  </script>
  <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
  <!-- <script>
    window.TrackJS &&
      TrackJS.install({
        token: "ee6fab19c5a04ac1a32a645abde4613a",
        application: "argon-design-system-pro"
      });
  </script> -->

</body>

</html>