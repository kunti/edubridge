<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="EduBridge">
  <meta name="author" content="EduBridge">
  <title>PIPE Psikotest</title>
   <!-- Favicon -->
  <link rel="shortcut icon" type="image/png" href="<?php echo base_url('src/assets/img/brand/favicon.png'); ?>">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/nucleo/css/nucleo.css'); ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css'); ?>" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/css/argon.css?v=1.2.0'); ?>" type="text/css">
  <!-- STYLE TAMBAHAN -->
      <!-- CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets_landing/css/argon-design-system.css'); ?>" type="text/css">

  <!-- STYLE TAMBAHAN -->
  <link rel="stylesheet" href="<?php echo base_url('src/style_tambahan/style-tambahan.css'); ?>" type="text/css">
  <!-- whatsapp style  -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel='stylesheet'>

  <style type="text/css">
  body {
  font-family: 'Montserrat';
}
    .float{
    position:fixed;
    width:60px;
    height:60px;
    bottom:40px;
    right:40px;
    background-color:#25d366;
    color:#FFF;
    border-radius:50px;
    text-align:center;
    font-size:30px;
    box-shadow: 2px 2px 3px #999;
    z-index:100;
  }

  .my-float{
    margin-top:16px;
  }
  </style>
</head>

<body class="bg-default">
<!--<a href="https://api.whatsapp.com/send?phone=6289505147095&text=Hai%21%20EduBridge" class="float" target="_blank">-->
<!--<i class="fa fa-whatsapp my-float"></i>-->
<!--</a>-->
  


<!-- Navbar -->
  <?php include('app/Views/main/navbar.php') ?> 
<!-- Akhir Navbar -->


<!-- Main content -->
  <?php if (isset($template)) echo view($template); ?>
<!-- Main Content -->


<!-- Footer -->
  <?php include('app/Views/main/footer.php') ?> 
<!-- Akhir Footer -->




  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="<?php echo base_url('src/assets/vendor/jquery/dist/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/js-cookie/js.cookie.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js'); ?>"></script>
  <!-- Argon JS -->
  <script src="<?php echo base_url('src/assets/js/argon.js?v=1.2.0'); ?>"></script>
  <script type="text/javascript">

  $(document).ready(function() {
    $("#success-alert").fadeTo(5000, 1000).slideUp(1000, function() {
      $("#success-alert").slideUp(1000);
    });
  });
  </script>

</body>

</html>