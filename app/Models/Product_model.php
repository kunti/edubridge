<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class Product_model extends Model {
 
    protected $table = "product";
    protected $primaryKey = "id";

    protected $allowedFields = ['nama', '', 'harga', ' short_desc', 'desc', '', 'gambar', 'used', 'viewed', 'shared'];


    public function getProducts()
    {
        return $this->db->table($this->table)->get()->getRowArray();
    }

    public function getProduct($id)
    {
        return $this->db->table($this->table)->where('id', $id)->get()->getRowArray();
    }
 
}

