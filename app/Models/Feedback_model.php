<?php 

namespace App\Models;

use CodeIgniter\Model;

class Feedback_model extends Model
{
    protected $table      = 'feedbackmember';
    protected $primaryKey = 'id';

    protected $allowedFields = ['id_member', 'feedback', 'status'];
}