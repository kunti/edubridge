<?php 

namespace App\Models;

use CodeIgniter\Model;

class User_model extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'id';

    protected $allowedFields = ['username', 'password', 'email', 'activation_selector', 'activation_code', 'forgotten_password_selector', 'forgotten_password_code', 'forgotten_password_time', 'remember_selector', 'remember_code', 'created_on', 'last_login', 'active', 'not_deleted', 'avatar', 'first_name', 'last_name', 'company', 'phone', 'sex', 'sipp', 'ttd','birthplace', 'birthdate', 'address', 'description', 'kotalahir', 'pendterakhir', 'domisili'];

    protected $returnType     = 'array';
}

