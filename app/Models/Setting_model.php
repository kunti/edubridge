<?php 

namespace App\Models;

use CodeIgniter\Model;

class Setting_model extends Model
{
    protected $table      = 'settings';
    protected $primaryKey = 'id';

    protected $allowedFields = ['element', 'value'];

    protected $returnType     = 'array';
}

