<?php 

namespace App\Models;

use CodeIgniter\Model;

class Productjenistes_model extends Model
{
    protected $table      = 'productjenistes';
    protected $primaryKey = 'id';

    protected $allowedFields = ['id_product', 'id_jenistes', 'deleted'];
}