<?php 

namespace App\Models;

use CodeIgniter\Model;

class Jawabanmember_model extends Model
{
    protected $table      = 'jawabanmember';
    protected $primaryKey = 'id';

    protected $allowedFields = ['id_memberorderproductjenistes', 'urutan_bagian_tes', 'sub_bagian', 'urutan_soal', 'urutan_jawaban', 'jawaban', 'jawaban_isian', 'jawaban_time', 'jawaban_timer', 'score', 'scored_by', 'deleted'];



}