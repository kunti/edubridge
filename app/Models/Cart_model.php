<?php 

namespace App\Models;

use CodeIgniter\Model;

class Cart_model extends Model
{
    protected $table      = 'cart';
    protected $primaryKey = 'id';

    protected $allowedFields = ['id_member', 'id_product', 'deleted'];
}