<?php 

namespace App\Models;

use CodeIgniter\Model;

class Memberorderproductjenistes_model extends Model
{
    protected $table      = 'memberorderproductjenistes';
    protected $primaryKey = 'id';

    protected $allowedFields = ['id_memberorderproduct', 'id_jenistes', 'start-test', 'done-test', 'start-correction', 'corrected', 'summary', 'iq', 'tingkat_kecerdasan', 'chart', 'id_psikolog', 'bagian_checkpoint' ,'timer_checkpoint','timerhafalan_checkpoint', 'status', 'deleted'];
}