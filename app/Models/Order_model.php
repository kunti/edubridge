<?php 

namespace App\Models;

use CodeIgniter\Model;

class Order_model extends Model
{
    protected $table      = 'order';
    protected $primaryKey = 'id';

    protected $allowedFields = ['id_member', 'id_peserta', 'order_code', 'cart', 'checkout', 'paid', 'paid-confirm', 'start-test', 'done-test', 'corrected', 'summarized', 'bukti_pembayaran', 'approved', 'status', 'deleted'];
}