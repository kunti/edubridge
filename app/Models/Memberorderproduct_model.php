<?php 

namespace App\Models;

use CodeIgniter\Model;

class Memberorderproduct_model extends Model
{
    protected $table      = 'memberorderproduct';
    protected $primaryKey = 'id';

    protected $allowedFields = ['id_order', 'id_product', 'nama', 'harga', 'req_unblock', 'req_unblock_time', 'is_unblock', 'is_unblock_time', 'isTataTertib', 'test-status', 'start-summarization', 'summarized', 'summary', 'chart', 'jurusan1', 'jurusan2', 'jurusan3', 'jurusan4', 'jurusan5', 'descjurusan1', 'descjurusan2', 'descjurusan3', 'descjurusan4', 'descjurusan5', 'id_psikolog', 'status','on_qc', 'downloaded', 'deleted'];
}