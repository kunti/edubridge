<?php namespace App\Controllers;

use App\Models\User_model;
use App\Models\Order_model;
use App\Models\Memberorderproduct_model;
use App\Models\Memberorderproductjenistes_model;

class Psychologist extends BaseController
{
	public function __construct()
	{
        $this->user = new User_model;
        $this->order = new Order_model;
        $this->memberorderproduct = new memberorderproduct_model;
        $this->memberorderproductjenistes = new memberorderproductjenistes_model;
	}
	
	public function all($id = NULL)
	{
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();
        
		if ($id) {
			$this->data['psychologists']  = $this->user->asArray()
	            ->select('users.*')
	            ->where('users.id', $id)
	            ->where('users_groups.group_id', 3)
	            ->where('users.not_deleted', 1)
	            ->join('users_groups', 'users.id=users_groups.user_id')
	            ->orderBy('users.id', 'DESC')
	            ->findAll();
		} else {
			$this->data['psychologists']  = $this->user->asArray()
		            ->select('users.*')
		            ->where('users_groups.group_id', 3)
		            ->where('users.not_deleted', 1)
		            ->join('users_groups', 'users.id=users_groups.user_id')
		            ->orderBy('users.id', 'DESC')
		            ->findAll();
		}

		 foreach($this->data['psychologists'] as $key => $psychologist){
            $this->data['psychologists'][$key]['start_correction'] = $this->memberorderproductjenistes->asArray()
            ->select('COUNT(id) as start_correction')
            ->where('id_psikolog', $psychologist['id'])
            ->where('status', 'start-correction')
            ->where('deleted', 0)
            ->findAll()[0]['start_correction'];
            $this->data['psychologists'][$key]['corrected'] = $this->memberorderproductjenistes->asArray()
            ->select('COUNT(id) as corrected')
            ->where('id_psikolog', $psychologist['id'])
            ->where('status', 'corrected')
            ->where('deleted', 0)
            ->findAll()[0]['corrected'];
            $this->data['psychologists'][$key]['start_summarization'] = $this->memberorderproduct->asArray()
            ->select('COUNT(id) as start_summarization')
            ->where('id_psikolog', $psychologist['id'])
            ->where('status', 'start-summarization')
            ->where('deleted', 0)
            ->findAll()[0]['start_summarization'];
            $this->data['psychologists'][$key]['summarized'] = $this->memberorderproduct->asArray()
            ->select('COUNT(id) as summarized')
            ->where('id_psikolog', $psychologist['id'])
            ->where('status', 'summarized')
            ->where('deleted', 0)
            ->findAll()[0]['summarized'];
        }

        	$this->data['all']['ready_to_correct'] = $this->memberorderproductjenistes->asArray()
            ->select('COUNT(id) as done_test')
            ->where('status', 'done-test')
            ->where('deleted', 0)
            ->findAll()[0]['done_test'];
            $this->data['all']['start_correction'] = $this->memberorderproductjenistes->asArray()
            ->select('COUNT(id) as start_correction')
            ->where('status', 'start-correction')
            ->where('deleted', 0)
            ->findAll()[0]['start_correction'];
            $this->data['all']['corrected'] = $this->memberorderproductjenistes->asArray()
            ->select('COUNT(id) as corrected')
            ->where('status', 'corrected')
            ->where('deleted', 0)
            ->findAll()[0]['corrected'];

            $this->data['all']['ready_to_summarized'] = $this->memberorderproductjenistes->asArray()
            ->select('COUNT(id) as ready_to_summarized')
            ->where('status', 'corrected')
            ->where('deleted', 0)
            ->findAll()[0]['ready_to_summarized'];
            $this->data['all']['start_summarization'] = $this->memberorderproduct->asArray()
            ->select('COUNT(id) as start_summarization')
            ->where('status', 'start-summarization')
            ->where('deleted', 0)
            ->findAll()[0]['start_summarization'];
            $this->data['all']['summarized'] = $this->memberorderproduct->asArray()
            ->select('COUNT(id) as summarized')
            ->where('status', 'summarized')
            ->where('deleted', 0)
            ->findAll()[0]['summarized'];

        // dd($this->data['psychologists']);

		$this->data['scope'] = 'psychologist';
		$this->data['template'] = 'admin/psychologist';
		return view('admin/index', $this->data);	
	}

	public function id($id=NULL)
	{
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
			$this->data['products'] = $this->product->asArray()->findAll();
		    $this->data['product'] = $this->product->getProduct($id);
			$this->data['scope'] = __FUNCTION__;
			$this->data['template'] = 'main/product';
			return view('index', $this->data);
	}

    public function faq()
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();
        
        $this->data['scope'] = __FUNCTION__;
        $this->data['template'] = 'psychologist/'.__FUNCTION__;
        return view('psychologist/index', $this->data);
    }

	
}
