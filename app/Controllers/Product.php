<?php namespace App\Controllers;

use App\Models\Memberorderproduct_model;
use App\Models\Setting_model;


class Product extends BaseController
{
	public function __construct()
	{

        $this->memberorderproduct = new memberorderproduct_model;
        $this->setting = new setting_model;

	}
	
	public function index()
	{
		
        $this->data['settings'] = $this->setting->asArray()
                ->select('*')
                ->findAll();

        $this->data['products'] = $this->product->asArray()
        ->orderBy('urutan', 'asc')
        ->findAll();
        // dd($this->data['products']);
		// $this->data['product'] = $this->product->getProducts();

		

		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'main/products';
		return view('index', $this->data);	
	}

	public function id($id=NULL)
	{
		// view count save to db
		$this->data['settings'] = $this->setting->asArray()
                ->select('*')
                ->findAll();
		$this->product
        ->set('viewed', 'viewed+1', FALSE)        
        ->where('id', $id)
        ->update();

		$this->data['products'] = $this->product->asArray()->orderBy('urutan', 'asc')->findAll();
		foreach ($this->data['products'] as $key => $product) {
		    $this->data['ordered'][$product['id']] =  $this->memberorderproduct->asArray()
	            ->select('count(id) as ordered')
	            ->where('id_product', $product['id'])
	            ->where('deleted', 0)
	            ->findAll()[0]['ordered'];
		}
		if ($this->data['ordered'][$id] == max($this->data['ordered'])){
			$this->data['terlaris'] = true;
		}

	    $this->data['product'] = $this->product->getProduct($id);

		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'main/product';
		return view('index', $this->data);
	}

	public function whatsapp($id=NULL)
	{
		// view count save to db
		$this->product
        ->set('shared', 'shared+1', FALSE)        
        ->where('id', $id)
        ->update();
	}

	
}
