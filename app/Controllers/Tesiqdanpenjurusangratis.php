<?php 
namespace App\Controllers;

class Tesiqdanpenjurusangratis extends BaseController
{
	public function __construct()
	{
		
	}
	
	public function index()
	{
		$this->data['products'] = $this->product->asArray()->findAll();
		
		$this->language->setLocale('id');
			$_SESSION['web_lang'] = 'id';
		return view('main/volunteer', $this->data);
	}

}
