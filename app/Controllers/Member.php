<?php namespace App\Controllers;

use App\Models\Order_model;
use App\Models\Memberorderproduct_model;

class Member extends BaseController
{
	public function __construct()
	{
        helper('form');
        $this->order = new Order_model;
        $this->memberorderproduct = new Memberorderproduct_model;
	 }

	public function index()
	{
    if (! $this->ionAuth->loggedIn()){
      $this->session->setFlashdata('message', lang('Global.login_first'));
      return redirect()->to('/auth/login');
    }
    $this->data['profile'] = $this->ionAuth->user()->row();
		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'member/dashboard';
		return view('member/index', $this->data);
	}

  public function all()
	{
    if (! $this->ionAuth->loggedIn()){
      $this->session->setFlashdata('message', lang('Global.login_first'));
      return redirect()->to('/auth/login');
    }
    echo "belum";
  }

	public function cart()
	{
    if (! $this->ionAuth->loggedIn()){
      $this->session->setFlashdata('message', lang('Global.login_first'));
      return redirect()->to('/auth/login');
    }
        $this->data['products'] = $this->product->asArray()->orderBy('urutan', 'asc')->findAll();

        $this->data['profile'] = $this->ionAuth->user()->row();
        // data cart
        $this->data['carts'] = $this->cart->asArray()
            ->select('cart.*, users.volunteer as volunteer')
            ->where('id_member', $_SESSION['user_id'])
            ->where('deleted', 0)
            ->join('users', 'cart.id_member=users.id', 'left')
            ->findAll();
            // dd($this->data['carts']);

        $this->data['items']=array();
        $this->data['total'] = 0;
        // jika ada cart 
        if($this->data['carts']!=NULL){
            foreach ($this->data['carts'] as $key => $cart) {
              // dd($cart);
              $this->data['items'][$key] = $this->product->asArray()
              ->where('id', $cart['id_product'])
              ->findAll();
              $this->product->asArray()
              ->where('id', $cart['id_product'])
              ->findAll();
            }
            $this->data['total'] = 0;
              foreach($this->data['items'] as $item){
                // dd($item);
                  if ($this->data['carts'][0]['volunteer']==1 && $item[0]['id']==1){
                    $item[0]['harga'] = 0;
                    // dd($item['harga']);
                  }
                  $this->data['total'] += $item[0]['harga'] ;
              }
        } 

		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'member/'.__FUNCTION__;
		return view('member/index', $this->data);
	}

	public function orderproduct()
	{
    if (! $this->ionAuth->loggedIn()){
      $this->session->setFlashdata('message', lang('Global.login_first'));
      return redirect()->to('/auth/login');
    }
        $this->data['profile'] = $this->ionAuth->user()->row();

        // data order
        $this->data['orders'] = $this->order->asArray()
            ->where('id_member', $_SESSION['user_id'])
            ->whereIn('status', ['paid-confirm'])
            ->where('deleted', 0)
            ->orderBy('id', 'DESC')
            ->findAll();
        // $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
        //       ->where('id_order', $order['id'])
        //       ->where('deleted', 0)
        //       ->join('product', 'product.id=memberorderproduct.id_product')
        //       ->findAll();

        foreach($this->data['orders'] as $key => $order){
        	$this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
            ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
            ->where('id_order', $order['id'])
            ->where('deleted', 0)
            ->join('product', 'product.id=memberorderproduct.id_product')
            ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
            // ->join('product', 'product.id=memberorderproduct.id_product')
            ->findAll();
        }

        // dd($this->data['orders']);

		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'member/'.__FUNCTION__;
		return view('member/index', $this->data);
	}

	public function payment()
	{
    if (! $this->ionAuth->loggedIn()){
      $this->session->setFlashdata('message', lang('Global.login_first'));
      return redirect()->to('/auth/login');
    }
        $this->data['profile'] = $this->ionAuth->user()->row();

        // data order
        $this->data['orders'] = $this->order->asArray()
            ->where('id_member', $_SESSION['user_id'])
            ->whereIn('status', ['checkout', 'paid', 'paid-confirm'])
            ->where('deleted', 0)
            ->orderBy('id', 'DESC')
            ->findAll();

        foreach($this->data['orders'] as $key => $order){
        	$this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
            ->where('id_order', $order['id'])
            ->where('deleted', 0)
            ->join('product', 'product.id=memberorderproduct.id_product')
            ->findAll();
        }

        // dd($data['orders']);


		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'member/'.__FUNCTION__;
		return view('member/index', $this->data);
	}

	public function feedback()
	{
    if (! $this->ionAuth->loggedIn()){
      $this->session->setFlashdata('message', lang('Global.login_first'));
      return redirect()->to('/auth/login');
    }
        $this->data['profile'] = $this->ionAuth->user()->row();

		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'member/'.__FUNCTION__;
		return view('member/index', $this->data);
	}

	public function product()
	{
    if (! $this->ionAuth->loggedIn()){
      $this->session->setFlashdata('message', lang('Global.login_first'));
      return redirect()->to('/auth/login');
    }
    $this->data['profile'] = $this->ionAuth->user()->row();
    $this->data['products'] = $this->product->asArray()->orderBy('urutan', 'asc')->findAll();


		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'member/'.__FUNCTION__;
		return view('member/index', $this->data);
	}

	public function faq()
	{
    if (! $this->ionAuth->loggedIn()){
      $this->session->setFlashdata('message', lang('Global.login_first'));
      return redirect()->to('/auth/login');
    }
    $this->data['profile'] = $this->ionAuth->user()->row();
		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'member/'.__FUNCTION__;
		return view('member/index', $this->data);
	}

	public function test($jenistes)
	{
    if (! $this->ionAuth->loggedIn()){
      $this->session->setFlashdata('message', lang('Global.login_first'));
      return redirect()->to('/auth/login');
    }
		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'member/'.__FUNCTION__;
		return view('member/test', $this->data);
	}

	public function petunjuk($jenistes)
	{
    if (! $this->ionAuth->loggedIn()){
      $this->session->setFlashdata('message', lang('Global.login_first'));
      return redirect()->to('/auth/login');
    }
		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'member/'.__FUNCTION__;
		return view('member/petunjuk', $this->data);
	}

	public function fileUpload($id_order){

  if (! $this->ionAuth->loggedIn()){
      $this->session->setFlashdata('message', lang('Global.login_first'));
      return redirect()->to('/auth/login');
    }
     // Validation
     $input = $this->validate([
        'file' => 'uploaded[file]|max_size[file,1024]|ext_in[file,png,jpg,jpeg,pdf],'
     ]);

     if (!$input) { // Not valid
         // $data['validation'] = $this->validator; 
         // return view('users',$data); 
         session()->setFlashdata('message', 'File not valid. Only accept jpg, png, jpeg and pdf');
         session()->setFlashdata('alert-class', 'alert-danger');
     }else{ // Valid

         if($file = $this->request->getFile('file')) {
            if ($file->isValid() && ! $file->hasMoved()) {
               // Get file name and extension
               $name = $file->getName();
               $ext = $file->getClientExtension();

               // Get random file name
               $newName = $file->getRandomName(); 

               // Store file in public/uploads/ folder
               $file->move('public/uploads/payment', $newName);

               // Save name to database
               $this->order->update($id_order, ['bukti_pembayaran' => $newName]);
               $this->order->update($id_order, ['status' => 'paid']);
               $this->order->update($id_order, ['paid' => date("Y-m-d H:i:s")]);

               // File path to display preview
               $filepath = base_url()."/uploads/payment/".$newName;
               
               // Set Session
               session()->setFlashdata('message', 'Uploaded Successfully!');
               session()->setFlashdata('alert-class', 'alert-success');
               session()->setFlashdata('filepath', $filepath);
               session()->setFlashdata('extension', $ext);

            }else{
               // Set Session
               session()->setFlashdata('message', 'File not uploaded.');
               session()->setFlashdata('alert-class', 'alert-danger');

            }
         }

     }
  
     return redirect()->to('/member/payment'); 
   }

		//--------------------------------------------------------------------

}
