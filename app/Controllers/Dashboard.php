<?php namespace App\Controllers;

use App\Models\User_model;
use App\Models\Order_model;
use App\Models\Memberorderproduct_model;
use App\Models\Memberorderproductjenistes_model;
use App\Models\Productjenistes_model;
use App\Models\Feedback_model;
use App\Models\Setting_model;

use DateTime;


class Dashboard extends BaseController
{
    
    public function __construct() {
        $this->users = new User_model;
        $this->order = new Order_model;
        $this->memberorderproduct = new memberorderproduct_model;
        $this->memberorderproductjenistes = new memberorderproductjenistes_model;
        $this->productjenistes = new Productjenistes_model;
        $this->feedback = new feedback_model;
        $this->settings = new setting_model;
    }


    public function index()
    {

        // helper('cookie');
        // set_cookie([
		// 	'name'   => 'remember_code',
		// 	'value'  => '91b7669f7713fa355fbb78552bab95be40f6b848.b1378b484b3bf8357c73bfbc487514962e6a71cc16bef054a5d832ad2b881ebcf3c42a23cdd67c0567b3e3f283c8c414d573eb49c5f96e40893f873ee4f39709',
		// 	'expire' => 630720000,
		// ]);
        
        if (! $this->ionAuth->loggedIn()){
            $this->session->setFlashdata('message', lang('Global.login_first'));
            return redirect()->to('/auth/login');
        }
        if (! $this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        
        $this->data['profile'] = $this->ionAuth->user()->row();
        $this->data['scope'] = __FUNCTION__;
        $this->data['Pakettes_expiration']= $this->settings->asArray()
            ->select('value')
            ->whereIn('id', ['6'])
            ->findAll()[0]['value'];
        $this->data['Pakettes_expiration_belumdikerjakan']= $this->settings->asArray()
            ->select('value')
            ->whereIn('id', ['7'])
            ->findAll()[0]['value'];

        if ($this->ionAuth->inGroup('admin')) {
            // $db = \Config\Database::connect();
            // $this->data['online'] = $db->query('SELECT `order`.id, memberorderproduct.nama as nama_product, users.avatar, users.phone,  `order`.`paid-confirm`, memberorderproduct.isTataTertib, users.first_name, users.last_name, users.company, users.email, jenistes.nama, 
            // memberorderproductjenistes.id, 
            // memberorderproductjenistes.`start-test`, 
            // memberorderproductjenistes.id_memberorderproduct, memberorderproductjenistes.id_jenistes, memberorderproductjenistes.`start-test`, memberorderproductjenistes.bagian_checkpoint, memberorderproductjenistes.timer_checkpoint, memberorderproductjenistes.timerhafalan_checkpoint, memberorderproductjenistes.updated_at FROM `memberorderproductjenistes`  
            // join memberorderproduct on memberorderproductjenistes.id_memberorderproduct=memberorderproduct.id
            // join `order` on memberorderproduct.id_order=`order`.id
            // join users on `order`.`id_member`=users.id
            // join jenistes on memberorderproductjenistes.id_jenistes=jenistes.id
            // WHERE memberorderproductjenistes.status="start-test" and memberorderproductjenistes.updated_at BETWEEN timestamp(DATE_SUB(NOW(), INTERVAL 2 MINUTE)) AND timestamp(NOW())
            // ORDER BY memberorderproductjenistes.updated_at DESC
            // limit 50')->getResultArray();
            // if (count($this->data['online'])>=1){
            //     return redirect()->to('/test/online');
            // }

            
            $this->data['member'] = $this->users->asArray()
            ->select('COUNT(users.id) as member')
            ->where('users_groups.group_id', 2)
            ->join('users_groups', 'users_groups.user_id=users.id')
            ->findAll()[0]['member'];

            $this->data['member_active'] = $this->users->asArray()
            ->select('COUNT(users.id) as member_active')
            ->where('users_groups.group_id', 2)
            ->where('users.not_deleted', 1)
            ->join('users_groups', 'users_groups.user_id=users.id')
            ->findAll()[0]['member_active'];

            $this->data['member_inactive'] = $this->users->asArray()
            ->select('COUNT(users.id) as member_inactive')
            ->where('users_groups.group_id', 2)
            ->where('users.not_deleted', 0)
            ->join('users_groups', 'users_groups.user_id=users.id')
            ->findAll()[0]['member_inactive'];
           
            $this->data['orders'] = $this->order->asArray()
            ->select('COUNT(order.id) as orders')
            ->where('order.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['orders'];
            $this->data['booking'] = $this->order->asArray()
            ->select('COUNT(order.id) as booking')
            ->where('order.checkout !=', '0000-00-00 00:00:00')
            ->where('order.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['booking'];
            $this->data['pending'] = $this->order->asArray()
            ->select('COUNT(order.id) as pending')
            ->where('status', 'checkout')
            ->where('order.checkout !=', '0000-00-00 00:00:00')
            ->where('order.paid =', '0000-00-00 00:00:00')
            ->where('order.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['pending'];
            $this->data['paid'] = $this->order->asArray()
            ->select('COUNT(order.id) as paid')
            ->where('order.status', 'paid')
            ->where('order.checkout !=', '0000-00-00 00:00:00')
            ->where('order.paid !=', '0000-00-00 00:00:00')
            ->where('order.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['paid'];
            $this->data['waiting_confirmation'] = $this->order->asArray()
            ->select('COUNT(order.id) as waiting_confirmation')
            ->where('order.status', 'paid')
            ->where('order.paid !=', '0000-00-00 00:00:00')
            ->where('order.paid-confirm =', '0000-00-00 00:00:00')
            ->where('order.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['waiting_confirmation'];
            $this->data['paid_confirm'] = $this->order->asArray()
            ->select('COUNT(order.id) as paid_confirm')
            ->where('order.status', 'paid-confirm')
            ->where('order.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['paid_confirm'];

            $this->data['orderproduct'] = $this->memberorderproduct->asArray()
            ->select('COUNT(memberorderproduct.id) as orderproduct')
            ->where('order.status', 'paid-confirm')
            ->where('memberorderproduct.deleted', 0)
            ->where('users.not_deleted', 1)
            ->where('order.deleted', 0)
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['orderproduct'];

            $main_orders  = $this->memberorderproduct->asArray()
            ->select('memberorderproduct.id, count(memberorderproductjenistes.id) as jumlah_jenistes, order.paid-confirm')
            ->where('order.status', 'paid-confirm')
            ->where('memberorderproduct.deleted', 0)
            ->where('users.not_deleted', 1)
            ->where('order.deleted', 0)
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->join('memberorderproductjenistes', 'memberorderproductjenistes.id_memberorderproduct=memberorderproduct.id')
            ->groupby('memberorderproduct.id')
            ->findAll();

            $this->data['order_done'] = $this->memberorderproduct->asArray()
            ->select('count(memberorderproduct.id) as jumlah')
            ->where('order.status', 'paid-confirm')
            ->where('memberorderproduct.deleted', 0)
            ->where('memberorderproduct.test-status', 'done-test')
            ->where('users.not_deleted', 1)
            ->where('order.deleted', 0)
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['jumlah'];      

            $this->data['order_ontest'] = $this->memberorderproduct->asArray()
            ->select('count(memberorderproduct.id) as jumlah')
            ->where('order.status', 'paid-confirm')
            ->where('memberorderproduct.deleted', 0)
            ->where('memberorderproduct.test-status', 'start-test')
            ->where('users.not_deleted', 1)
            ->where('order.deleted', 0)
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['jumlah'];      
            
            $this->data['order_ontest_expired'] = $this->memberorderproduct->asArray()
            ->select('count(memberorderproduct.id) as jumlah')
            ->where('order.status', 'paid-confirm')
            ->where('memberorderproduct.deleted', 0)
            ->where('memberorderproduct.test-status', 'start-test')
            ->where('users.not_deleted', 1)
            ->where('order.deleted', 0)
            ->where('order.paid-confirm < now() - interval 1 day')
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['jumlah'];   
            
            $this->data['order_ontest_notexpired'] = $this->memberorderproduct->asArray()
            ->select('count(memberorderproduct.id) as jumlah')
            ->where('order.status', 'paid-confirm')
            ->where('memberorderproduct.deleted', 0)
            ->where('memberorderproduct.test-status', 'start-test')
            ->where('users.not_deleted', 1)
            ->where('order.deleted', 0)
            ->where('order.paid-confirm > now() - interval 1 day')
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['jumlah'];   

            $this->data['order_ready_dibawah'] = $this->memberorderproduct->asArray()
            ->select('count(memberorderproduct.id) as jumlah')
            ->where('order.status', 'paid-confirm')
            ->where('memberorderproduct.deleted', 0)
            ->where('memberorderproduct.test-status', NULL)
            ->where('users.not_deleted', 1)
            ->where('order.deleted', 0)
            ->where('order.paid-confirm > now() - interval 1 day')
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['jumlah'];   

            $this->data['order_ready_diatas'] = $this->memberorderproduct->asArray()
            ->select('count(memberorderproduct.id) as jumlah')
            ->where('order.status', 'paid-confirm')
            ->where('memberorderproduct.deleted', 0)
            ->where('memberorderproduct.test-status', NULL)
            ->where('users.not_deleted', 1)
            ->where('order.deleted', 0)
            ->where('order.paid-confirm < now() - interval 1 day')
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['jumlah'];   
            
            // optimasi query 
            // $orders = $main_orders;
            // $main_orders  = $this->memberorderproduct->asArray()
            // ->select('memberorderproduct.id, count(memberorderproductjenistes.id) as jumlah_jenistes, order.paid-confirm')
            // ->where('order.status', 'paid-confirm')
            // ->where('memberorderproduct.deleted', 0)
            // ->where('users.not_deleted', 1)
            // ->where('order.deleted', 0)
            // ->join('order', 'order.id=memberorderproduct.id_order')
            // ->join('users', 'order.id_member=users.id')
            // ->join('memberorderproductjenistes', 'memberorderproductjenistes.id_memberorderproduct=memberorderproduct.id')
            // ->groupby('memberorderproduct.id')
            // ->findAll();
            // $this->data['order_done']=0;
            // $this->data['order_ontest_expired']=0;
            // $this->data['order_ontest_notexpired']=0;
            // $this->data['order_ready_dibawah'] = 0;
            // $this->data['order_ready_diatas'] = 0;
            // $orders_ready = array();


            // // UBAH STATUS MEMBERORDERPRODUCT
            // foreach ($orders as $key => $value) {
            //     $jenistes_selesai = $this->memberorderproductjenistes->asArray()
            //     ->select('count(id) as jumlah, id_memberorderproduct')
            //     ->where('id_memberorderproduct', $value['id'])
            //     ->where('status', 'done-test')
            //     ->groupby('id_memberorderproduct')
            //     ->findAll();

                
            //     $jenistes_belum_dikerjakan = $this->memberorderproductjenistes->asArray()
            //     ->select('count(id) as jumlah, id_memberorderproduct')
            //     ->where('id_memberorderproduct', $value['id'])
            //     ->where('start-test', '0000-00-00 00:00:00')
            //     ->groupby('id_memberorderproduct')
            //     ->findAll();

            //     if (!empty($jenistes_selesai)){
            //         if ($jenistes_selesai[0]['jumlah']==$value['jumlah_jenistes']){
            //             // set status menjadi done-test
            //             // dd($value['id']);
            //             $data = [
            //                 'test-status' => 'done-test',
            //             ];  
            //             if ($this->memberorderproduct->update($value['id'], $data)){
            //                 $orders[$key]['status'] = "selesai";
            //                 $this->data['order_done']++;
            //                 unset($orders[$key]);
            //             }
            //         } else {
            //         }
            //     }
            //     if (!empty($jenistes_belum_dikerjakan)){
            //         if ($jenistes_belum_dikerjakan[0]['jumlah']==$value['jumlah_jenistes']){
            //             $orders[$key]['status'] = "belum dikerjakan";
            //             if(strtotime($value['paid-confirm']) > strtotime('-'.$this->data['Pakettes_expiration_belumdikerjakan'].' hours')) {
            //                 $this->data['order_ready_dibawah']++;
            //             } else {
            //                 $this->data['order_ready_diatas']++;
            //             }
            //             array_push($orders_ready, $orders[$key]);
            //             unset($orders[$key]);
            //         } else {
            //             $orders[$key]['status'] = "belum selesai";
            //         }
            //     }

            // }
            // $this->data['order_ontest_notexpired']=0;
            // $this->data['order_ontest_expired']=0;
            // foreach ($orders as $key => $value) {
            //     $data = [
            //         'test-status' => 'start-test',
            //     ];  
            //     if ($this->memberorderproduct->update($value['id'], $data)){
            //         if(strtotime($value['paid-confirm']) > strtotime('-'.$this->data['Pakettes_expiration_belumdikerjakan'].' hours')) {
            //             $this->data['order_ontest_notexpired']++;
            //         } else {
            //             $this->data['order_ontest_expired']++;
            //         }
            //     }
               
           
            // }

            // $this->data['order_ontest'] = $this->data['order_ontest_expired']+$this->data['order_ontest_notexpired'];
            
            $this->data['jenis_tes'] = $this->memberorderproductjenistes->asArray()
            ->select('COUNT(memberorderproductjenistes.id) as jenis_tes')
            ->where('memberorderproductjenistes.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('memberorderproduct', 'memberorderproduct.id=memberorderproductjenistes.id_memberorderproduct')
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')

            ->findAll()[0]['jenis_tes'];
            $this->data['done_test'] = $this->memberorderproductjenistes->asArray()
            ->select('COUNT(memberorderproductjenistes.id) as done_test')
            ->where('memberorderproductjenistes.status', 'done-test')
            ->where('memberorderproductjenistes.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('memberorderproduct', 'memberorderproduct.id=memberorderproductjenistes.id_memberorderproduct')
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['done_test'];
            $this->data['start_correction'] = $this->memberorderproductjenistes->asArray()
            ->select('COUNT(memberorderproductjenistes.id) as start_correction')
            ->where('memberorderproductjenistes.status', 'start-correction')
            ->where('memberorderproductjenistes.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('memberorderproduct', 'memberorderproduct.id=memberorderproductjenistes.id_memberorderproduct')
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['start_correction'];
            $this->data['corrected'] = $this->memberorderproductjenistes->asArray()
            ->select('COUNT(memberorderproductjenistes.id) as corrected')
            ->where('memberorderproductjenistes.status', 'corrected')
            ->where('memberorderproductjenistes.deleted', 0)
            ->where('order.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('memberorderproduct', 'memberorderproduct.id=memberorderproductjenistes.id_memberorderproduct')
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['corrected'];

            $this->data['ready_to_summarized'] = $this->memberorderproductjenistes->asArray()
            ->select('COUNT(memberorderproductjenistes.id) as ready_to_summarized')
            ->where('memberorderproduct.start-summarization', '0000-00-00 00:00:00')
            ->where('memberorderproductjenistes.deleted', 0)
            ->where('order.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('memberorderproduct', 'memberorderproduct.id=memberorderproductjenistes.id_memberorderproduct')
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['ready_to_summarized'];
            $this->data['ready_to_summarized'] = $this->memberorderproduct->asArray()
            ->select('COUNT(memberorderproduct.id) as ready_to_summarized')
            ->where('memberorderproduct.status', NULL)
            ->where('memberorderproduct.deleted', 0)
            ->where('memberorderproduct.test-status', 'done-test')
            ->where('order.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['ready_to_summarized'];
            $this->data['start_summarization'] = $this->memberorderproduct->asArray()
            ->select('COUNT(memberorderproduct.id) as start_summarization')
            ->where('memberorderproduct.status', 'start-summarization')
            ->where('memberorderproduct.deleted', 0)
            ->where('order.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['start_summarization'];
            $this->data['summarized'] = $this->memberorderproduct->asArray()
            ->select('COUNT(memberorderproduct.id) as summarized')
            ->where('memberorderproduct.status', 'summarized')
            ->where('memberorderproduct.deleted', 0)
            ->where('order.deleted', 0)
            ->where('users.not_deleted', 1)
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['summarized'];

            $this->data['template'] = 'admin/dashboard';
            return view('admin/index', $this->data);
            
        } else if ($this->ionAuth->inGroup('members')) {

            $this->data['orders'] = $this->order->asArray()
                ->where('id_member', $_SESSION['user_id'])
                ->whereIn('status', ['paid-confirm'])
                ->where('deleted', 0)
                ->orderBy('id', 'DESC')
                ->findAll();

            if (empty($this->data['orders'])){
                $this->volunteer_generate();
            }

            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
                ->where('id_order', $order['id'])
                ->where('deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                // ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();
            }

            $this->data['orders'] = $this->order->asArray()
                ->where('id_member', $_SESSION['user_id'])
                ->where('status', 'paid-confirm')
                ->where('deleted', 0)
                ->orderBy('id', 'DESC')
                ->findAll();

            

            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
                ->where('id_order', $order['id'])
                ->where('memberorderproduct.deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();
                    }
            }

            $this->data['feedbacks'] = $this->feedback->asArray()
                ->select('feedbackmember.*, users.email, users.first_name, users.last_name, users.photo')
                ->where('users.id', $this->data['profile']->id)
                ->join('users', 'users.id=feedbackmember.id_member')->findAll();

            // dd($this->data['orders']);
            $this->data['template'] = 'member/dashboard';
            return view('member/index', $this->data);
        } else if ($this->ionAuth->inGroup('psychologists')) {

            $this->data['siap_diringkas'] = $this->memberorderproduct->asArray()
            ->select('count(memberorderproduct.id) as jumlah')
            ->where('order.status', 'paid-confirm')
            ->where('memberorderproduct.deleted', 0)
            ->where('memberorderproduct.test-status', 'done-test')
            ->where('memberorderproduct.status', NULL)
            ->where('users.not_deleted', 1)
            ->where('order.deleted', 0)
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['jumlah'];      

            $this->data['sedang_dirangkum'] = $this->memberorderproduct->asArray()
            ->select('count(memberorderproduct.id) as jumlah')
            ->where('order.status', 'paid-confirm')
            ->where('memberorderproduct.deleted', 0)
            ->where('memberorderproduct.test-status', 'done-test')
            ->where('memberorderproduct.status', 'start-summarization')
            ->where('id_psikolog', $this->data['profile']->user_id)
            ->where('users.not_deleted', 1)
            ->where('order.deleted', 0)
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['jumlah'];     

            $this->data['selesai'] = $this->memberorderproduct->asArray()
            ->select('count(memberorderproduct.id) as jumlah')
            ->where('order.status', 'paid-confirm')
            ->where('memberorderproduct.deleted', 0)
            ->where('memberorderproduct.test-status', 'done-test')
            ->where('memberorderproduct.status', 'summarized')
            ->where('id_psikolog', $this->data['profile']->user_id)
            ->where('users.not_deleted', 1)
            ->where('order.deleted', 0)
            ->join('order', 'order.id=memberorderproduct.id_order')
            ->join('users', 'order.id_member=users.id')
            ->findAll()[0]['jumlah'];     

            $this->data['template'] = 'psychologist/dashboard';
            return view('psychologist/index', $this->data);
        }
    }
    
    public function volunteer_generate() {
        // adding new product to cart
        $id=1;
        $product = $this->product->getProduct($id);
        // cek data product
        if($product != null){ // jika product tidak kosong
 
            // buat variabel array untuk menampung data product
            $item = [
                'id'        => $product['id'],
                'nama'      => $product['nama'],
                'harga'     => 0,
                'quantity'  => 1
            ];
            // jika member login, maka add cart di database
            if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) { 
                // masukkan jika product belum masuk cart
                if($this->cart->asArray()
                    ->where('id_member', $_SESSION['user_id'])
                    ->where('id_product', $product['id'])
                    ->where('deleted', 0)
                    ->findColumn('id_product')==NULL){
                        $this->cart->insert([
                        'id_member'        => $_SESSION['user_id'],
                        'id_product'        => $product['id'],
                        ]);
                }
            } else {
            // tambahkan product ke dalam cart
            $this->wfcart->add_cart($id, $item);
            }
        }
        // checkout
        $data['carts'] = $this->cart->asArray()
        ->where('id_member', $_SESSION['user_id'])
        ->where('deleted', 0)
        ->findAll();
        $id_order = 0;
        // jika ada cart 
        if($data['carts']!=NULL){
            // create order
            $id_order = $this->order->insert([
                'id_member'    => $_SESSION['user_id'],
                'checkout' => date("Y-m-d H:i:s"),
                'status'    => 'checkout',
            ]);
            // insert order has products
            foreach($data['carts'] as $cart){
        // dd($cart);
            // dd($this->product->where('id', $cart['id_product'])->findColumn('harga')['0']);
                $id_memberorderproduct = $this->memberorderproduct->insert([
                    'id_order'    => $id_order,
                    'id_product'    => $cart['id_product'],
                    'nama'    => $this->product->where('id', $cart['id_product'])->findColumn('nama')['0'],
                    'harga'    => $this->product->where('id', $cart['id_product'])->findColumn('harga')['0'],
                ]);
                // change cart status
                $this->cart->update($cart['id'], ['deleted' => '1']);
                // insert memberorderproductjenistes
                $data['jenistes'] = $this->productjenistes->asArray()
                    ->where('id_product', $cart['id_product'])
                    ->where('deleted', 0)
                    ->findAll();
                foreach ($data['jenistes'] as $key => $jenistes) {
                    $this->memberorderproductjenistes->insert([
                        'id_memberorderproduct'    => $id_memberorderproduct,
                        'id_jenistes'    => $jenistes['id_jenistes'],
                    ]);
                }
            }


        } 
        // change order payment to paid-confirm
        $this->order
            ->set(['status' => 'paid-confirm', 'paid-confirm' => date("Y-m-d H:i:s")])        
            ->where('id', $id_order)
            ->update();

        return redirect()->to('/dashboard');

    }
    public function payment()
    {
        if (! $this->ionAuth->loggedIn()){
            $this->session->setFlashdata('message', lang('Global.login_first'));
            return redirect()->to('/auth/login');
        }
        $this->data['scope'] = __FUNCTION__;
        $this->data['template'] = 'admin/'.__FUNCTION__;
        return view('admin/index', $this->data);
    }

    public function test()
    {
        if (! $this->ionAuth->loggedIn()){
            $this->session->setFlashdata('message', lang('Global.login_first'));
            return redirect()->to('/auth/login');
        }
        $this->data['scope'] = __FUNCTION__;
        $this->data['template'] = 'admin/'.__FUNCTION__;
        return view('admin/index', $this->data);
    }

    public function feedback()
    {
        if (! $this->ionAuth->loggedIn()){
            $this->session->setFlashdata('message', lang('Global.login_first'));
            return redirect()->to('/auth/login');
        }
        $this->data['scope'] = __FUNCTION__;
        $this->data['template'] = 'admin/'.__FUNCTION__;
        return view('admin/index', $this->data);
    }

    public function admin()
    {
        if (! $this->ionAuth->loggedIn()){
            $this->session->setFlashdata('message', lang('Global.login_first'));
            return redirect()->to('/auth/login');
        }
        $this->data['scope'] = __FUNCTION__;
        $this->data['template'] = 'admin/'.__FUNCTION__;
        return view('admin/index', $this->data);
    }

    public function psychologist()
    {
        if (! $this->ionAuth->loggedIn()){
            $this->session->setFlashdata('message', lang('Global.login_first'));
            return redirect()->to('/auth/login');
        }
        $this->data['scope'] = __FUNCTION__;
        $this->data['template'] = 'admin/'.__FUNCTION__;
        return view('admin/index', $this->data);
    }

    public function product()
    {
        if (! $this->ionAuth->loggedIn()){
            $this->session->setFlashdata('message', lang('Global.login_first'));
            return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();
        $this->data['products'] = $this->product->asArray()->findAll();

        $this->data['scope'] = __FUNCTION__;
        $this->data['template'] = 'admin/'.__FUNCTION__;
        if ($this->ionAuth->inGroup('admin')) {
            return view('admin/index', $this->data);
        } else if ($this->ionAuth->inGroup('psychologists')) {
            return view('psychologist/index', $this->data);
        } else if ($this->ionAuth->inGroup('members')) {
            return view('member/index', $this->data);
        }
    }

    public function jenis_tes()
    {
        if (! $this->ionAuth->loggedIn()){
            $this->session->setFlashdata('message', lang('Global.login_first'));
            return redirect()->to('/auth/login');
        }
        $this->data['scope'] = __FUNCTION__;
        $this->data['template'] = 'admin/'.__FUNCTION__;
        return view('admin/index', $this->data);
    }

    public function soaljawaban($id_jenis_tes)
    {
        if (! $this->ionAuth->loggedIn()){
            $this->session->setFlashdata('message', lang('Global.login_first'));
            return redirect()->to('/auth/login');
        }
        $this->data['scope'] = 'jenis_tes';
        $this->data['template'] = 'admin/'.__FUNCTION__;
        return view('admin/index', $this->data);
    }


    // public function product()
    // {
    //  $this->data['scope'] = __FUNCTION__;
    //  $this->data['template'] = 'admin/'.__FUNCTION__;
    //  return view('admin/index', $this->data);
    // }

    public function faq()
    {
        if (! $this->ionAuth->loggedIn()){
            $this->session->setFlashdata('message', lang('Global.login_first'));
            return redirect()->to('/auth/login');
        }
        $this->data['scope'] = __FUNCTION__;
        $this->data['template'] = 'admin/'.__FUNCTION__;
        return view('admin/index', $this->data);
    }

        //--------------------------------------------------------------------

}
