<?php
namespace App\Controllers;


helper('cookie');


/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;
use App\Models\Product_model;						
use App\Models\Order_model;
use App\Models\Cart_model;




class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];


	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		$this->session       = \Config\Services::session();
		
		parent::initController($request, $response, $logger);
        $this->product = new Product_model;
		$this->ionAuth    = new \IonAuth\Libraries\IonAuth();
		$this->cart = new Cart_model;
		
		$this->language = \Config\Services::language();
        if (!isset($_SESSION['web_lang'])){
			$this->language->setLocale('id');
			$_SESSION['web_lang'] = 'id';
		} else {
        	$this->language->setLocale($_SESSION['web_lang']);
		}

		$this->product = new Product_model;
		if (! $this->ionAuth->loggedIn())
		{
			// redirect them to the login page
			if (!isset($_SESSION['web_lang'])){
				$this->language->setLocale('id');
				$_SESSION['web_lang'] = 'id';
			} else {
	        	$this->language->setLocale($_SESSION['web_lang']);
			}
			return redirect()->to('/auth/login');
		}
		// else if (! $this->ionAuth->isAdmin()) // remove this elseif if you want to enable this for non-admins
		// {
			// redirect them to the home page because they must be an administrator to view this
			//show_error('You must be an administrator to view this page.');
			// throw new \Exception('You must be an administrator to view this page.');
		// }
		else
		{
			return redirect()->to('/');
			// $this->data['title'] = lang('Auth.index_heading');

			// // set the flash data error message if there is one
			// $this->data['message'] = $this->validation->getErrors() ? $this->validation->listErrors($this->validationListTemplate) : $this->session->getFlashdata('message');
			// //list the users
			// $this->data['users'] = $this->ionAuth->users()->result();
			// foreach ($this->data['users'] as $k => $user)
			// {
			// 	$this->data['users'][$k]->groups = $this->ionAuth->getUsersGroups($user->id)->getResult();
			// }
			// return $this->renderPage($this->viewsFolder . DIRECTORY_SEPARATOR . 'index', $this->data);
		}

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		// $this->session = \Config\Services::session();
		// helper('auth');
	}

}
