<?php namespace App\Controllers;

use App\Models\Feedback_model;


class Feedback extends BaseController
{
	public function __construct()
	{

        $this->feedback = new feedback_model;

	}
	
	public function index()
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
        $this->data['profile'] = $this->ionAuth->user()->row();

        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) {
       		$this->data['feedbacks'] = $this->feedback->asArray()
	        	->select('feedbackmember.*, users.email, users.first_name, users.last_name, users.photo')
		        ->where('users.id', $this->data['profile']->id)
	            ->join('users', 'users.id=feedbackmember.id_member')->findAll();
	           if(empty($this->data['feedbacks'])){
	           		$this->feedback->insert([
                        'id_member'        => $this->data['profile']->id,
                        'feedback'        => '',
                        ]);
	           		$this->data['feedbacks'] = $this->feedback->asArray()
			        	->select('feedbackmember.*, users.email, users.first_name, users.last_name, users.photo')
				        ->where('users.id', $this->data['profile']->id)
			            ->join('users', 'users.id=feedbackmember.id_member')->findAll();
	           }
			$this->data['scope'] = 'feedback';
			$this->data['template'] = 'member/feedback';
		return view('member/index', $this->data);	
        } else if ($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('admin')){
        	$this->data['feedbacks'] = $this->feedback->asArray()
	        	->select('feedbackmember.*, users.*')
				->where('users_groups.group_id', 2)
				->where('feedbackmember.feedback!=', "")
	            ->join('users', 'users.id=feedbackmember.id_member')
	            ->join('users_groups', 'users.id=users_groups.user_id')
	            ->findAll();
	        // dd($this->data['feedbacks']);

			$this->data['scope'] = 'feedback';
			$this->data['template'] = 'admin/feedback';
			return view('admin/index', $this->data);
        }
		
		// $this->data['feedback'] = $this->feedback->getProducts();

	}

	public function detail($id_feedback)
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
        $this->data['profile'] = $this->ionAuth->user()->row();

        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('admin')) {
       		$this->data['feedbacks'] = $this->feedback->asArray()
	        	->select('feedbackmember.*, users.email, users.first_name, users.last_name, users.photo, users.phone')
		        ->where('feedbackmember.id', $id_feedback)
	            ->join('users', 'users.id=feedbackmember.id_member')->findAll();
	          // dd($this->data['feedbacks']);	
			$this->data['scope'] = 'feedback';
			$this->data['template'] = 'admin/feedback_detail';
		return view('admin/index', $this->data);	
        }
	}

	public function save()
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}

		$rules = [
			'feedback'	=> 'required',
		];
		$data = [
			'feedback' => $this->request->getPost('feedback'),
			'status' => $this->request->getPost('status'),
		];
		if ($this->request->getPost('id_feedback')){
		    $this->feedback->update($this->request->getPost('id_feedback'), $data);
        	return redirect()->to('/feedback/detail/'.$this->request->getPost('id_feedback'));
		} else {
			$this->feedback
		    ->set('feedback', $data['feedback'])        
		    ->where('id_member', $_SESSION['user_id'])
		    ->update();
        	return redirect()->to('/feedback');
		}

	}

	public function id_member($id_member=NULL)
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
	    $this->data['feedback'] = $this->feedback->getProduct($id);

		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'main/feedback';
		return view('index', $this->data);
	}

	public function whatsapp($id=NULL)
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
		// view count save to db
		$this->feedback
        ->set('shared', 'shared+1', FALSE)        
        ->where('id', $id)
        ->update();
	}

	
}
