<?php namespace App\Controllers;

use App\Models\User_model;	
use App\Models\Order_model;
use App\Models\Memberorderproduct_model;
use App\Models\Memberorderproductjenistes_model;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/PHPMailer/PHPMailer/src/Exception.php';
require 'vendor/PHPMailer/PHPMailer/src/PHPMailer.php';
require 'vendor/PHPMailer/PHPMailer/src/SMTP.php';

class Email extends BaseController
{
	
	public function __construct() { 
        $this->user = new User_model;
		$this->order = new Order_model;
        $this->memberorderproduct = new memberorderproduct_model;
        $this->memberorderproductjenistes = new memberorderproductjenistes_model;
 
    }


	public function view_result($email= NULL, $id_order = NULL, $id_memberorderproduct = NULL){
		$this->data['user'] = $this->user
	            ->where('email', $email)
	            ->first();
		
		if($this->data['user']==null){
			return "error peserta tidak ditemukan";
		}

		$this->data['orders'] = $this->order->asArray()
                ->where('id_member', $this->data['user']['id'])
                ->whereIn('status', ['paid-confirm'])
                ->where('deleted', 0)
                ->where('id', $id_order)
                ->orderBy('id', 'DESC')
                ->findAll();

            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
                ->where('id_order', $order['id'])
                ->where('memberorderproduct.id', $id_memberorderproduct)
                ->where('deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                // ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();

				if($this->data['orders'][$key]['products']==null){
					return "error product tidak ditemukan";
				}

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();
                    }
					
		}
		if($this->data['orders']==null){
			return "error peserta tidak ditemukan";
		}
		return view('mail/hasiltes', $this->data);
	}

	public function result($email= NULL, $id_order = NULL, $id_memberorderproduct = NULL){
		$this->data['user'] = $this->user
	            ->where('email', $email)
	            ->first();
		
		if($this->data['user']==null){
			return "error peserta tidak ditemukan";
		}

		$this->data['orders'] = $this->order->asArray()
                ->where('id_member', $this->data['user']['id'])
                ->whereIn('status', ['paid-confirm'])
                ->where('deleted', 0)
                ->where('id', $id_order)
                ->orderBy('id', 'DESC')
                ->findAll();

            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc, users.first_name as psikolog_first_name, users.last_name as psikolog_last_name, users.id as psikolog_id')
                ->where('id_order', $order['id'])
                ->where('memberorderproduct.id', $id_memberorderproduct)
                ->where('deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                // ->join('product', 'product.id=memberorderproduct.id_product')
                ->findAll();

				if($this->data['orders'][$key]['products']==null){
					return "error product tidak ditemukan";
				}

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();
                    }
					
		}
		if($this->data['orders']==null){
			return "error peserta tidak ditemukan";
		}
		$mail = new PHPMailer(true);

		$mail->IsSMTP();
		$mail->SMTPSecure = 'ssl'; 
		$mail->Host = "pipe-psikotes.co.id"; //host masing2 provider email
		// $mail->SMTPDebug = 2;
		$mail->Port = 465;
		$mail->SMTPAuth = true;
		$mail->Username = "admin@pipe-psikotes.co.id"; //user email
		$mail->Password = "P@ssword123"; //password email 
		$mail->SetFrom("admin@pipe-psikotes.co.id","PIPE Psikotes"); //set email pengirim
		$mail->Subject = "Hasil tes Anda telah keluar"; //subyek email
		$mail->AddAddress($email, $this->data['user']['first_name'].' '.$this->data['user']['last_name']);  //email tujuan email
		$content = view('mail/hasiltes', $this->data);
		$mail->MsgHTML($content);
		if($mail->Send()) return "Pesan Berhasil Di Kirim";
		else return "Email Gagal Di Kirim";
	}
	
	public function sendEmail($attachment=NULL, $to=NULL, $title=NULL, $message=NULL){

		$email = \Config\Services::email();

		$email->setFrom('kunti.khoirunnisaa@gmail.com', 'Kunti Khoirunnisaa');
		$email->setTo('azifahmanset@gmail.com');
		$email->attach($attachment);
		// $email->setCC('another@another-example.com');
		// $email->setBCC('them@their-example.com');

		$email->setSubject('Email Test');
		$email->setMessage('Testing the email class.');
		if ($email->send())
		{
			$this->setMessage('IonAuth.forgot_password_successful');
			return true;
		} else {
			return false;
		}

// 		//file attachment
// $attachment = base_url('uploads/Invoice.pdf');//html message untuk body email
// $message = "<h1>Invoice Pembelian</h1>Kepada ".$pembeli->username." Berikut Invoice atas pembelian ".$barang->nama."";//memanggil private function sendEmail
// $this->sendEmail($attachment, 'deavenditama@gmail.com', 'Invoice', $message);


	}

		//--------------------------------------------------------------------

}


	