<?php 
namespace App\Controllers;
 
use App\Models\Product_model;
use App\Models\Cart_model;
use App\Models\Order_model;
use App\Models\Memberorderproduct_model;
use App\Models\Memberorderproductjenistes_model;
use App\Models\Productjenistes_model;
use App\Models\Setting_model;
// memanggil library / package WFcart
use Wildanfuady\WFcart\WFcart;
 
class Cart extends BaseController
{
 
    public function __construct() {

        $this->product = new Product_model;
        $this->cart = new Cart_model;
        $this->order = new Order_model;
        $this->memberorderproduct = new Memberorderproduct_model;
        $this->memberorderproductjenistes = new Memberorderproductjenistes_model;
        $this->productjenistes = new Productjenistes_model;
        $this->setting = new Setting_model;
        $this->wfcart = new WFcart();
        helper('form');
 
    }
 
    public function index()
    {
        $data['settings'] = $this->setting->asArray()
                ->select('*')
                ->findAll();

        // dd($this->data['settings']);
        
        $data['products'] = $this->product->asArray()->orderBy('urutan', 'asc')->findAll();
        
        // jika member login, maka cek cart di database
        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) {
            $data['carts'] = $this->cart->asArray()
            ->where('id_member', $_SESSION['user_id'])
            ->where('deleted', 0)
            ->findColumn('id_product');

            $data['items']=array();
            $data['total'] = 0;
            // jika ada cart 
            if($data['carts']!=NULL){
                $data['items'] = $this->product->asArray()
                ->whereIn('id', $data['carts'])
                ->findAll();
                $data['total'] = 0;
                foreach($data['items'] as $item){
                    $data['total'] += $item['harga'] ;
                }
                $this->product->asArray()
                ->whereIn('id', $data['carts'])
                ->findAll();
            } 
        } 
        // jika member tidak login maka cek cart di session
        else {
            $data['items'] = $this->wfcart->totals();
            $data['total'] = $this->wfcart->count_totals();
        }

        $data['scope'] = __FUNCTION__;
        $data['template'] = 'main/cart';
        return view('index', $data);
    }
 
    public function beli($id = null)
    {
        // cari product berdasarkan id
        $product = $this->product->getProduct($id);
        // cek data product
        if($product != null){ // jika product tidak kosong
 
            // buat variabel array untuk menampung data product
            $item = [
                'id'        => $product['id'],
                'nama'      => $product['nama'],
                'harga'     => $product['harga'],
                'quantity'  => 1
            ];
            // jika member login, maka add cart di database
            if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) { 
                // masukkan jika product belum masuk cart
                if($this->cart->asArray()
                    ->where('id_member', $_SESSION['user_id'])
                    ->where('id_product', $product['id'])
                    ->where('deleted', 0)
                    ->findColumn('id_product')==NULL){
                        $this->cart->insert([
                        'id_member'        => $_SESSION['user_id'],
                        'id_product'        => $product['id'],
                        ]);
                }
            } else {
            // tambahkan product ke dalam cart
            $this->wfcart->add_cart($id, $item);
            }
            // tampilkan nama product yang ditambahkan
            $product = $item['nama'];
            // success flashdata
            session()->setFlashdata('success', ' '.lang('Global.cart_add'));
            // session()->setFlashdata('success', " Berhasil memasukan {$product} ke booking");
        } else {
            // error flashdata
            session()->setFlashdata('error', " Tidak dapat menemukan data product");
        }
        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) { 
            return redirect()->to('/member/cart');    
        } else {
            return redirect()->to('/cart');
        }
    }
 
    // function untuk update cart berdasarkan id dan quantity
    public function update()
    {
        // update cart
        $this->wfcart->update();
        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) { 
            return redirect()->to('/member/cart');    
        } else {
            return redirect()->to('/cart');
        }
    }
 
    // function untuk menghapus cart berdasarkan id
    public function remove($id = null)
    {
         
        // cari product berdasarkan id
        $product = $this->product->getProduct($id);
        // cek data product
        if($product != null){ // jika product tidak kosong
            if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) { 
                if($this->cart->asArray()
                    ->where('id_member', $_SESSION['user_id'])
                    ->where('id_product', $product['id'])
                    ->where('deleted', 0)
                    ->findColumn('id_product')!=NULL){
                        $this->cart->where([
                        'id_member'        => $_SESSION['user_id'],
                        'id_product'        => $product['id'],
                        ])->delete();
                }
            } else {
                // hapus keranjang belanja berdasarkan id
                $this->wfcart->remove($id);
            }
            // success flashdata
            session()->setFlashdata('success', ' '.lang('Global.cart_remove'));
            // session()->setFlashdata('success', "Berhasil menghapus product dari keranjang belanja");
        } else { // product tidak ditemukan
            // error flashdata
            session()->setFlashdata('error', "Tidak dapat menemukan data product");
        }
       if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) { 
            return redirect()->to('/member/cart');    
        } else {
            return redirect()->to('/cart');
        }
    }

    public function checkout()
    {
        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) {
            // masukkan data ke order, update cart menjadi deleted
            $data['carts'] = $this->cart->asArray()
            ->where('id_member', $_SESSION['user_id'])
            ->where('deleted', 0)
            ->findAll();
            $id_order = 0;
            // jika ada cart 
            if($data['carts']!=NULL){
                // create order
                $id_order = $this->order->insert([
                    'id_member'    => $_SESSION['user_id'],
                    'checkout' => date("Y-m-d H:i:s"),
                    'status'    => 'checkout',
                ]);
                // insert order has products
                foreach($data['carts'] as $cart){
            // dd($cart);
                // dd($this->product->where('id', $cart['id_product'])->findColumn('harga')['0']);
                    $id_memberorderproduct = $this->memberorderproduct->insert([
                        'id_order'    => $id_order,
                        'id_product'    => $cart['id_product'],
                        'nama'    => $this->product->where('id', $cart['id_product'])->findColumn('nama')['0'],
                        'harga'    => $this->product->where('id', $cart['id_product'])->findColumn('harga')['0'],
                    ]);
                    // change cart status
                    $this->cart->update($cart['id'], ['deleted' => '1']);
                    // insert memberorderproductjenistes
                    $data['jenistes'] = $this->productjenistes->asArray()
                        ->where('id_product', $cart['id_product'])
                        ->where('deleted', 0)
                        ->findAll();
                    foreach ($data['jenistes'] as $key => $jenistes) {
                        $this->memberorderproductjenistes->insert([
                            'id_memberorderproduct'    => $id_memberorderproduct,
                            'id_jenistes'    => $jenistes['id_jenistes'],
                        ]);
                    }
                }


            } 
            return redirect()->to('/cart/payment/'.$id_order);
        } 
        else {
            return redirect()->to('/auth/login');
        }
        // $this->wfcart->update();
        // return redirect()->to('/cart');
    }

    public function payment($order_id)
    {

        $data['settings'] = $this->setting->asArray()
                ->select('*')
                ->findAll();

                $data['scope'] = __FUNCTION__;
        $data['template'] = 'main/payment';
        return view('index', $data);
    }

    public function cart_session_to_db() {
        $data['items'] = $this->wfcart->totals();
        foreach ($data['items'] as $key => $item) {
            if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) { 
                // masukkan jika product belum masuk cart
                if($this->cart->asArray()
                    ->where('id_member', $_SESSION['user_id'])
                    ->where('id_product', $item['id'])
                    ->where('deleted', 0)
                    ->findColumn('id_product')==NULL){
                        $this->cart->insert([
                        'id_member'        => $_SESSION['user_id'],
                        'id_product'        => $item['id'],
                        ]);
                }
            }
        }
        return redirect()->to('/')->withCookies();

    }

    public function volunteer_generate() {
        // adding new product to cart
        $id=1;
        $product = $this->product->getProduct($id);
        // cek data product
        if($product != null){ // jika product tidak kosong
 
            // buat variabel array untuk menampung data product
            $item = [
                'id'        => $product['id'],
                'nama'      => $product['nama'],
                'harga'     => 0,
                'quantity'  => 1
            ];
            // jika member login, maka add cart di database
            if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) { 
                // masukkan jika product belum masuk cart
                if($this->cart->asArray()
                    ->where('id_member', $_SESSION['user_id'])
                    ->where('id_product', $product['id'])
                    ->where('deleted', 0)
                    ->findColumn('id_product')==NULL){
                        $this->cart->insert([
                        'id_member'        => $_SESSION['user_id'],
                        'id_product'        => $product['id'],
                        ]);
                }
            } else {
            // tambahkan product ke dalam cart
            $this->wfcart->add_cart($id, $item);
            }
        }
        // checkout
        $data['carts'] = $this->cart->asArray()
        ->where('id_member', $_SESSION['user_id'])
        ->where('deleted', 0)
        ->findAll();
        $id_order = 0;
        // jika ada cart 
        if($data['carts']!=NULL){
            // create order
            $id_order = $this->order->insert([
                'id_member'    => $_SESSION['user_id'],
                'checkout' => date("Y-m-d H:i:s"),
                'status'    => 'checkout',
            ]);
            // insert order has products
            foreach($data['carts'] as $cart){
        // dd($cart);
            // dd($this->product->where('id', $cart['id_product'])->findColumn('harga')['0']);
                $id_memberorderproduct = $this->memberorderproduct->insert([
                    'id_order'    => $id_order,
                    'id_product'    => $cart['id_product'],
                    'nama'    => $this->product->where('id', $cart['id_product'])->findColumn('nama')['0'],
                    'harga'    => $this->product->where('id', $cart['id_product'])->findColumn('harga')['0'],
                ]);
                // change cart status
                $this->cart->update($cart['id'], ['deleted' => '1']);
                // insert memberorderproductjenistes
                $data['jenistes'] = $this->productjenistes->asArray()
                    ->where('id_product', $cart['id_product'])
                    ->where('deleted', 0)
                    ->findAll();
                foreach ($data['jenistes'] as $key => $jenistes) {
                    $this->memberorderproductjenistes->insert([
                        'id_memberorderproduct'    => $id_memberorderproduct,
                        'id_jenistes'    => $jenistes['id_jenistes'],
                    ]);
                }
            }


        } 
        // change order payment to paid-confirm
        $this->order
            ->set(['status' => 'paid-confirm', 'paid-confirm' => date("Y-m-d H:i:s")])        
            ->where('id', $id_order)
            ->update();

        return redirect()->to('/dashboard');

    }

    public function generate($user_id) {
        // adding new product to cart
        $id=1;
        $product = $this->product->getProduct($id);
        // cek data product
        if($product != null){ // jika product tidak kosong
 
            // buat variabel array untuk menampung data product
            $item = [
                'id'        => $product['id'],
                'nama'      => $product['nama'],
                'harga'     => 0,
                'quantity'  => 1
            ];
            // add cart di database
            // masukkan jika product belum masuk cart
            if($this->cart->asArray()
                ->where('id_member', $user_id)
                ->where('id_product', $product['id'])
                ->where('deleted', 0)
                ->findColumn('id_product')==NULL){
                    $this->cart->insert([
                    'id_member'        => $user_id,
                    'id_product'        => $product['id'],
                    ]);
            }
            
        }
        // checkout
        $data['carts'] = $this->cart->asArray()
        ->where('id_member', $user_id)
        ->where('deleted', 0)
        ->findAll();
        $id_order = 0;
        // jika ada cart 
        if($data['carts']!=NULL){
            // create order
            $id_order = $this->order->insert([
                'id_member'    => $user_id,
                'checkout' => date("Y-m-d H:i:s"),
                'status'    => 'checkout',
            ]);
            // insert order has products
            foreach($data['carts'] as $cart){
        // dd($cart);
            // dd($this->product->where('id', $cart['id_product'])->findColumn('harga')['0']);
                $id_memberorderproduct = $this->memberorderproduct->insert([
                    'id_order'    => $id_order,
                    'id_product'    => $cart['id_product'],
                    'nama'    => $this->product->where('id', $cart['id_product'])->findColumn('nama')['0'],
                    'harga'    => $this->product->where('id', $cart['id_product'])->findColumn('harga')['0'],
                ]);
                // change cart status
                $this->cart->update($cart['id'], ['deleted' => '1']);
                // insert memberorderproductjenistes
                $data['jenistes'] = $this->productjenistes->asArray()
                    ->where('id_product', $cart['id_product'])
                    ->where('deleted', 0)
                    ->findAll();
                foreach ($data['jenistes'] as $key => $jenistes) {
                    $this->memberorderproductjenistes->insert([
                        'id_memberorderproduct'    => $id_memberorderproduct,
                        'id_jenistes'    => $jenistes['id_jenistes'],
                    ]);
                }
            }


        } 
        // change order payment to paid-confirm
        $this->order
            ->set(['status' => 'paid-confirm', 'paid-confirm' => date("Y-m-d H:i:s")])        
            ->where('id', $id_order)
            ->update();

        echo 'Berhasil menambahkan';

    }
 
}