<?php 
namespace App\Controllers;
 
use App\Models\Product_model;
use App\Models\Order_model;
use App\Models\Memberorderproduct_model;
use App\Models\Memberorderproductjenistes_model;
use App\Models\Productjenistes_model;
use App\Models\Jenistes_model;
use App\Models\Bagiantes_model;
use App\Models\Soaltes_model;
use App\Models\Pilihanjawaban_model;
use App\Models\Jawabanmember_model;
use App\Models\User_model;
use App\Models\Setting_model;
use Dompdf\Options;
use Mpdf\Mpdf;

 
class Pdf extends BaseController
{
 
    public function __construct() {
 
        $this->product = new Product_model;
        $this->order = new Order_model;
        $this->memberorderproduct = new memberorderproduct_model;
        $this->memberorderproductjenistes = new memberorderproductjenistes_model;
        $this->productjenistes = new Productjenistes_model;
        $this->jenistes = new Jenistes_model;
        $this->bagiantes = new Bagiantes_model;
        $this->soaltes = new Soaltes_model;
        $this->pilihanjawaban = new Pilihanjawaban_model;
        $this->jawabanmember = new Jawabanmember_model;
        $this->user = new User_model;
        $this->setting = new Setting_model;
        helper('form');
 
    }
 
  

    function htmlToPDF()
	{

        $dompdf = new \Dompdf\Dompdf(); 
        $dompdf->loadHtml(view('pdf/plain'));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();
    }

    public function render($id_order = NULL, $id_memberorderproduct = NULL, $attachment = FALSE)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $this->data['profile'] = $this->ionAuth->user()->row();


        if($this->ionAuth->loggedIn()) {
        
        // detail
             $this->data['settings'] = $this->setting->asArray()
                ->select('*')
                ->findAll();


            $this->data['orders'] = $this->order->asArray()
                ->select('order.id, order.order_code, order.id_member, order.status, order.paid-confirm , users.first_name as member_first_name, users.last_name as member_last_name, users.id as member_id')
                ->where('order.id', $id_order)
                ->where('order.status', 'paid-confirm')
                ->where('order.deleted', 0)
                ->join('users', 'users.id=order.id_member', 'left')
                ->orderBy('order.id', 'DESC')
                ->findAll();

            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.id, memberorderproduct.id_product, memberorderproduct.nama, memberorderproduct.summarized, memberorderproduct.summary, memberorderproduct.chart, 
                    memberorderproduct.jurusan1,
                    memberorderproduct.jurusan2,
                    memberorderproduct.jurusan3,
                    memberorderproduct.jurusan4,
                    memberorderproduct.jurusan5,
                    memberorderproduct.descjurusan1,
                    memberorderproduct.descjurusan2,
                    memberorderproduct.descjurusan3,
                    memberorderproduct.descjurusan4,
                    memberorderproduct.descjurusan5,
                    memberorderproduct.id_psikolog,
                    memberorderproduct.status,
                     product.short_desc as short_desc, product.desc as desc, users.first_name as first_name, users.last_name as last_name, users.photo as photo, users.ttd as ttd, users.sipp as sipp')
                ->where('memberorderproduct.id', $id_memberorderproduct)
                ->where('memberorderproduct.deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                ->findAll();

                 $this->data['orders'][$key]['member'] = $this->user->asArray()
                ->select('email, first_name, last_name, company, phone, birthdate, address, photo, volunteer, education_level, semester')
                ->where('id', $order['id_member'])
                // ->where('active', 1)
                ->findAll();

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {

                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('
                        memberorderproductjenistes.id,
                        memberorderproductjenistes.id_jenistes,
                        memberorderproductjenistes.start-test,
                        memberorderproductjenistes.done-test,
                        memberorderproductjenistes.summary,
                        memberorderproductjenistes.iq,
                        memberorderproductjenistes.tingkat_kecerdasan,
                        memberorderproductjenistes.chart,
                        memberorderproductjenistes.status,
                     jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();

                    foreach ($this->data['orders'][$key]['products'][$key2]['jenistes'] as $key3 => $jenistes) { 
                         
                         // data bagian tes
                        $this->data['jenistes'][$key3]['bagiantes'] = $this->bagiantes->asArray()
                            ->select('id, id_jenistes, urutan')
                            ->where('id_jenistes', $jenistes['id_jenistes'])
                            ->where('deleted', 0)
                            ->orderBy('urutan', 'ASC')
                            ->findAll();
                    }

                
            }
            
            // dd($this->data['orders']);
            
            $this->data['scope'] = __FUNCTION__;
            
            // $dompdf = new \Dompdf\Dompdf(); 
            $options = new Options();
            // $options->set('defaultFont', 'Montserrat');
            // dd($options);
            $dompdf = new \Dompdf\Dompdf($options);
            
            
            $dompdf->loadHtml(view('pdf/show', $this->data));
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $font = $dompdf->getFontMetrics()->get_font("Open Sans, sans-serif");
            $dompdf->getCanvas()->page_text(500, 750, "Halaman {PAGE_NUM} / {PAGE_COUNT}", $font, 8, array(.14,.16,.25));
            $dompdf->stream("PIPE Psikotes - ".$this->data['orders'][0]['member_first_name']." ".$this->data['orders'][0]['member_last_name']." - ".$this->data['orders'][0]['products'][0]['nama'].".pdf", array("Attachment" => $attachment));
            // exit(0);
            return view('pdf/index', $this->data);
        }
        }
    }

    public function mpdf($id_order = NULL, $id_memberorderproduct = NULL, $attachment = FALSE){
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        
        $this->data['profile'] = $this->ionAuth->user()->row();
        $id_user  = $this->order->asArray()
            ->where('id', $id_order)
            ->where('deleted', 0)
            ->findColumn('id_member')[0];

        
        // jika member benar yang memiliki tes ini
        if(($this->ionAuth->inGroup('members') && $this->data['profile']->id==$id_user) || $this->ionAuth->inGroup('admin') || $this->ionAuth->inGroup('psychologists')) {
            
        
        $this->data['settings'] = $this->setting->asArray()
                ->select('*')
                ->findAll();


            $this->data['orders'] = $this->order->asArray()
                ->select('order.id, order.order_code, order.id_member, order.status, order.paid-confirm , users.first_name as member_first_name, users.last_name as member_last_name, users.id as member_id')
                ->where('order.id', $id_order)
                ->where('order.status', 'paid-confirm')
                ->where('order.deleted', 0)
                ->join('users', 'users.id=order.id_member', 'left')
                ->orderBy('order.id', 'DESC')
                ->findAll();

            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.id, memberorderproduct.id_product, memberorderproduct.nama, memberorderproduct.summarized, memberorderproduct.summary, memberorderproduct.chart, 
                    memberorderproduct.jurusan1,
                    memberorderproduct.jurusan2,
                    memberorderproduct.jurusan3,
                    memberorderproduct.jurusan4,
                    memberorderproduct.jurusan5,
                    memberorderproduct.descjurusan1,
                    memberorderproduct.descjurusan2,
                    memberorderproduct.descjurusan3,
                    memberorderproduct.descjurusan4,
                    memberorderproduct.descjurusan5,
                    memberorderproduct.id_psikolog,
                    memberorderproduct.status,
                     product.short_desc as short_desc, product.desc as desc, users.first_name as first_name, users.last_name as last_name, users.photo as photo, users.ttd as ttd, users.sipp as sipp')
                ->where('memberorderproduct.id', $id_memberorderproduct)
                ->where('memberorderproduct.deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                ->findAll();

                 $this->data['orders'][$key]['member'] = $this->user->asArray()
                ->select('email, avatar, first_name, last_name, company, phone, birthdate, address, photo, volunteer, education_level, semester')
                ->where('id', $order['id_member'])
                ->findAll();

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {

                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('
                        memberorderproductjenistes.id,
                        memberorderproductjenistes.id_jenistes,
                        memberorderproductjenistes.start-test,
                        memberorderproductjenistes.done-test,
                        memberorderproductjenistes.summary,
                        memberorderproductjenistes.iq,
                        memberorderproductjenistes.tingkat_kecerdasan,
                        memberorderproductjenistes.chart,
                        memberorderproductjenistes.status,
                     jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();

                    foreach ($this->data['orders'][$key]['products'][$key2]['jenistes'] as $key3 => $jenistes) { 
                         
                         // data bagian tes
                        $this->data['jenistes'][$key3]['bagiantes'] = $this->bagiantes->asArray()
                            ->select('id, id_jenistes, urutan')
                            ->where('id_jenistes', $jenistes['id_jenistes'])
                            ->where('deleted', 0)
                            ->orderBy('urutan', 'ASC')
                            ->findAll();
                    }

                }
            }
        
        $this->data['scope'] = 'render';
        
        // $stylesheet = file_get_contents('src/assets/css/argon.css');

        // dd($stylesheet);
        
        // $mpdf->WriteHTML($stylesheet,1);
        // $mpdf->WriteHTML('pdf/plain');
        

        $mpdf = new \Mpdf\Mpdf([
            'mode' => '',
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_top' => 20,
            'margin_bottom' => 30,
            'margin_header' => 10,
            'margin_footer' => 10,
            'default_font' => 'montserrat'
            ]);
            
        
$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins

$header = '
';

$headerEven = '
<img style="height:30px;" src="src/assets/img/brand/white.png" />';
if ($this->data['orders'][0]['products'][0]['summarized'] != '0000-00-00 00:00:00'){
    $year = date_format(date_create($this->data['orders'][0]['products'][0]['summarized']),"Y"); 
} else {
    $year = '';
}
$footer = '<table width="100%" style="border-top: 1px solid #000000; vertical-align: bottom; font-family: montserrat; font-size: 9pt; color: #232941;"><tr>
<td width="20%"><b>Halaman <span style="font-size:9pt;">{PAGENO}</span></b></td>
<td width="80%" style="text-align: right;">
<b style="">'.$this->data['settings'][2]['value'].'</b><br>
        <!-- alamat -->
        <p >'.$this->data['settings'][0]['value'].'</p>
        <!-- no telp web -->
         <p >'.$this->data['settings'][1]['value'].' '.$this->data['settings'][3]['value'].'</p>
        <!-- copyright -->
         <p ><b>&#169; '.$this->data['settings'][2]['value'].' '.$year.'</b></p>
</td>
</tr></table>';
$mpdf->SetTitle($this->data['orders'][0]['member_first_name']." ".$this->data['orders'][0]['member_last_name']." - ".$this->data['orders'][0]['products'][0]['nama'].".pdf");
$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLHeader($headerEven, 'E');

$mpdf->SetHTMLFooter($footer);
$mpdf->SetHTMLFooter($footer, 'E');


$html = view('pdf/plain', $this->data);

$mpdf->WriteHTML($html);

$mpdf->Output("PIPE Psikotes - ".$this->data['orders'][0]['member_first_name']." ".$this->data['orders'][0]['member_last_name']." - ".$this->data['orders'][0]['products'][0]['nama'].".pdf", $attachment);
exit();
        } else {
            return redirect()->to('/dashboard');
        }
    }
    public function render2()
    {
            $this->data['scope'] = __FUNCTION__;       
            // $dompdf = new \Dompdf\Dompdf(); 
            $options = new Options();
            // $options->set('defaultFont', 'Montserrat');
            // dd($options);
            $dompdf = new \Dompdf\Dompdf($options);

            $dompdf->loadHtml(view('pdf/plain'));
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $font = $dompdf->getFontMetrics()->get_font("Open Sans, sans-serif");
            $dompdf->getCanvas()->page_text(500, 750, "Halaman {PAGE_NUM} / {PAGE_COUNT}", $font, 8, array(.14,.16,.25));
            $dompdf->stream("PIPE Psikotes.pdf", array("Attachment" => false));
            exit(0);
            // return view('pdf/index', $this->data);
        
        
    }

    public function montserrat()
    {
            $this->data['scope'] = __FUNCTION__;       
            // $dompdf = new \Dompdf\Dompdf(); 
            $options = new Options();
            // $options->set('defaultFont', 'Montserrat');
            // dd($options);
            $dompdf = new \Dompdf\Dompdf($options);

            $dompdf->loadHtml(view('pdf/montserrat'));
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $font = $dompdf->getFontMetrics()->get_font("Open Sans, sans-serif");
            $dompdf->getCanvas()->page_text(500, 750, "Halaman {PAGE_NUM} / {PAGE_COUNT}", $font, 8, array(.14,.16,.25));
            $dompdf->stream("PIPE Psikotes.pdf", array("Attachment" => false));
            exit(0);
            // return view('pdf/index', $this->data);
        
        
    }

    public function show2()
    {

            
            return view('pdf/plain');
        
        
    }



    public function show($id_order = NULL, $id_memberorderproduct = NULL)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        
        $this->data['profile'] = $this->ionAuth->user()->row();

        if($this->ionAuth->loggedIn()) {
        // detail
            $this->data['settings'] = $this->setting->asArray()
                ->select('*')
                ->findAll();
                
            $this->data['orders'] = $this->order->asArray()
                ->select('id, order_code, id_member, status, paid-confirm')
                ->where('id', $id_order)
                ->where('status', 'paid-confirm')
                ->where('deleted', 0)
                ->orderBy('id', 'DESC')
                ->findAll();


            foreach($this->data['orders'] as $key => $order){
                $this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.id, memberorderproduct.id_product, memberorderproduct.nama, memberorderproduct.summarized, memberorderproduct.summary, memberorderproduct.chart, 
                    memberorderproduct.jurusan1,
                    memberorderproduct.jurusan2,
                    memberorderproduct.jurusan3,
                    memberorderproduct.jurusan4,
                    memberorderproduct.jurusan5,
                    memberorderproduct.descjurusan1,
                    memberorderproduct.descjurusan2,
                    memberorderproduct.descjurusan3,
                    memberorderproduct.descjurusan4,
                    memberorderproduct.descjurusan5,
                    memberorderproduct.id_psikolog,
                    memberorderproduct.status,
                     product.short_desc as short_desc, product.desc as desc, users.first_name as first_name, users.last_name as last_name, users.photo as photo, users.ttd as ttd, users.sipp as sipp')
                ->where('memberorderproduct.id', $id_memberorderproduct)
                ->where('memberorderproduct.deleted', 0)
                ->join('product', 'product.id=memberorderproduct.id_product')
                ->join('users', 'users.id=memberorderproduct.id_psikolog', 'left')
                ->findAll();

                $this->data['orders'][$key]['member'] = $this->user->asArray()
                ->select('email, first_name, last_name, company, phone, birthdate, address, photo, volunteer, education_level, semester')
                ->where('id', $order['id_member'])
                // ->where('active', 1)
                ->findAll();

                foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {

                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('
                        memberorderproductjenistes.id,
                        memberorderproductjenistes.id_jenistes,
                        memberorderproductjenistes.start-test,
                        memberorderproductjenistes.done-test,
                        memberorderproductjenistes.summary,
                        memberorderproductjenistes.iq,
                        memberorderproductjenistes.tingkat_kecerdasan,
                        memberorderproductjenistes.chart,
                        memberorderproductjenistes.status,
                     jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();

                    foreach ($this->data['orders'][$key]['products'][$key2]['jenistes'] as $key3 => $jenistes) { 
                         
                         // data bagian tes
                        $this->data['jenistes'][$key3]['bagiantes'] = $this->bagiantes->asArray()
                            ->select('id, id_jenistes, urutan')
                            ->where('id_jenistes', $jenistes['id_jenistes'])
                            ->where('deleted', 0)
                            ->orderBy('urutan', 'ASC')
                            ->findAll();

                            // foreach ($this->data['jenistes'][$key3]['bagiantes'] as $key4 => $bagiantes) {
                                // dd($bagiantes);
                                // $this->data['jenistes'][$key3]['bagiantes'][$key4]['soal'] = $this->soaltes->asArray()
                                //     ->select('id, id_jenistes, urutan_bagian_tes, sub_bagian, nama_sub_bagian, urutan')
                                //     ->where('id_jenistes', $bagiantes['id_jenistes'])
                                //     ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                //     ->where('deleted', 0)
                                //     ->orderBy('urutan', 'ASC')
                                //     ->findAll();

                                // $this->data['jenistes'][$key3]['bagiantes'][$key4]['skor'] = $this->jawabanmember->asArray()
                                //     ->select('SUM(jawabanmember.score) as score')
                                //     ->where('id_memberorderproductjenistes', $jenistes['id'])
                                //     ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                //     ->where('deleted', 0)
                                //     ->groupBy('jawabanmember.urutan_bagian_tes')
                                //     ->findAll();

                                // if (empty($this->data['jenistes'][$key3]['bagiantes'][$key4]['skor'])){
                                //     $this->data['jenistes'][$key3]['bagiantes'][$key4]['skor'][0]['score'] = 0;  
                                // }

                                // dd( $this->data['jenistes'][$key3]['bagiantes'][$key4]['skor'][0]['score']);
                                // $this->data['jenistes'][$key3]['bagiantes'][$key4]['konversi'][0] = $this->konversi($bagiantes['id_jenistes'], $bagiantes['urutan'], $this->data['jenistes'][$key3]['bagiantes'][$key4]['skor'][0]['score']);
                                // dd($this->data['jenistes'][$key3]['bagiantes'][$key4]);

                                    // foreach ($this->data['jenistes'][$key3]['bagiantes'][$key4]['soal'] as $key5 => $soal) {
                                    //     $this->data['bagiantes'][$key3]['soal'][$key2]['pilihanjawaban'] = $this->pilihanjawaban->asArray()
                                    //         ->where('id_jenistes', $bagiantes['id_jenistes'])
                                    //         ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                    //         ->where('urutan_soal', $soal['urutan'])
                                    //         ->where('deleted', 0)
                                    //         ->orderBy('urutan', 'ASC')
                                    //         ->findAll();



                                    //         // data jawaban member jika ada
                                    //         $this->data['jawabanmember'] = $this->jawabanmember->asArray()
                                    //             ->where('id_memberorderproductjenistes', $jenistes['id'])
                                    //             ->where('urutan_bagian_tes', $bagiantes['urutan'])
                                    //             ->where('urutan_soal', $soal['urutan'])
                                    //             ->orderBy('id', 'ASC')
                                    //             ->findAll();

                                    //         if($this->data['jawabanmember']){
                                    //             $this->data['jenistes'][$key3]['bagiantes'][$key4]['soal'][$key5]['jawabanmember'] = $this->data['jawabanmember'];
                                    //         }

                                    //         // dd($this->data['orders'][0]['products'][0]['jenistes'][0]['status']);
                                    //         // jika pertama kali mengerjakan tes, generate jawaban
                                    //         if ($this->data['orders'][0]['status']=='paid-confirm' && $this->data['orders'][0]['products'][0]['jenistes'][0]['status']==''){
                                                
                                    //             $this->jawabanmember->insert(['id_memberorderproductjenistes' => $jenistes['id'], 'urutan_bagian_tes' => $bagiantes['urutan'], 'urutan_soal' => $soal['urutan']]);
                                                
                                    //             $this->memberorderproductjenistes
                                    //             ->set(['status' => 'start-test', 'start-test' => date("Y-m-d H:i:s")])        
                                    //             ->where('id', $jenistes['id'])
                                    //             ->update();
                                    //         }
                                    //         }
                                        // insert $id_memberorderproductjenistes
                                        // urutan bagian tes
                                        // $bagiantes['urutan']
                                        // urutan soal
                                        // $soal['urutan']

                            // }
                    }

                }
            }
            // echo $this->data['orders'][0]['products'][0]['first_name'];
            $this->data['scope'] = __FUNCTION__;
            // $this->data['template'] = 'pdf/'.__FUNCTION__;
            return view('pdf/'.__FUNCTION__, $this->data);
        
        }
    }

    public function konversi($jenistes, $bagiantes, $skor) {
        $code = [
            'SE',
            'WA',
            'AN',
            'GE',
            'ME',
            'RA',
            'ZR',
            'FA',
            'WU',
        ];
        $sw['1'] = [
            '65',
            '70',
            '75',
            '80',
            '85',
            '90',
            '95',
            '100',
            '105',
            '110',
            '115',
            '120',
            '125',
            '130',
            '135',
            '140',
            '145',
            '150',
            '155',
            '160',
        ];
        $klasifikasi['1'] = [
            'V',
            'V',
            'V',
            'V',
            'IV',
            'III -',
            'III',
            'III',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
        ];
        $sw['2'] = [
            '40',
            '45',
            '50',
            '55',
            '60',
            '65',
            '70',
            '75',
            '80',
            '85',
            '90',
            '95',
            '100',
            '105',
            '110',
            '115',
            '120',
            '125',
            '130',
            '135',
        ];
        $klasifikasi['2'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'IV',
            'IV',
            'III -',
            'III',
            'III',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
            'I',
        ];
        $sw['3'] = [
            '46',
            '52',
            '58',
            '64',
            '70',
            '76',
            '82',
            '88',
            '94',
            '100',
            '106',
            '112',
            '118',
            '124',
            '130',
            '136',
            '142',
            '148',
            '154',
            '160',
        ];
        $klasifikasi['3'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'IV',
            'III -',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
        ];
        $sw['4'] = [
            '60',
            '63',
            '64',
            '67',
            '70',
            '73',
            '76',
            '79',
            '81',
            '84',
            '87',
            '90',
            '93',
            '96',
            '97',
            '100',
            '103',
            '106',
            '109',
            '111',
            '114',
            '117',
            '120',
            '123',
            '126',
            '129',
            '131',
            '134',
            '137',
            '140',
            '143',
            '146',
        ];
        $klasifikasi['4'] = [
            'V',
            'V',
            'V',
            'V',
            'IV',
            'IV',
            'IV',
            'IV',
            'IV',
            'IV',
            'III -',
            'III -',
            'III -',
            'III -',
            'III',
            'III',
            'III',
            'III',
            'III',
            'III',
            'III',
            'III +',
            'III +',
            'III +',
            'II',
            'II',
            'II',
            'II',
            'II',
            'II',
            'I',
            'I',
        ];
        $sw['5'] = [
            '56',
            '60',
            '64',
            '68',
            '72',
            '76',
            '80',
            '84',
            '88',
            '92',
            '96',
            '100',
            '104',
            '108',
            '112',
            '116',
            '120',
            '124',
            '128',
            '132',
        ];
        $klasifikasi['5'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'III -',
            'III',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
        ];
        $sw['6'] = [
            '65',
            '70',
            '75',
            '80',
            '85',
            '90',
            '95',
            '100',
            '105',
            '110',
            '115',
            '120',
            '125',
            '130',
            '135',
            '140',
            '145',
            '150',
            '155',
            '160',
        ];
        $klasifikasi['6'] = [
            'V',
            'V',
            'V',
            'V',
            'IV',
            'III -',
            'III',
            'III +',
            'II',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
        ];
        $sw['7'] = [
            '64',
            '68',
            '72',
            '76',
            '80',
            '84',
            '88',
            '92',
            '96',
            '100',
            '104',
            '108',
            '112',
            '116',
            '120',
            '124',
            '128',
            '132',
            '136',
            '140',
        ];
        $klasifikasi['7'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'IV',
            'III -',
            'III',
            'III',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
        ];
        $sw['8'] = [
            '50',
            '55',
            '60',
            '65',
            '70',
            '75',
            '80',
            '85',
            '90',
            '95',
            '100',
            '105',
            '110',
            '115',
            '120',
            '125',
            '130',
            '135',
            '140',
            '145',
        ];
        $klasifikasi['8'] = [
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'V',
            'IV',
            'IV',
            'III -',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
        ];
        $sw['9'] = [
            '60',
            '65',
            '70',
            '75',
            '80',
            '85',
            '90',
            '95',
            '100',
            '105',
            '110',
            '115',
            '120',
            '125',
            '130',
            '135',
            '140',
            '145',
            '150',
            '155',
        ];
        $klasifikasi['9'] = [
            'V',
            'V',
            'V',
            'V',
            'IV',
            'IV',
            'III -',
            'III',
            'III +',
            'II',
            'II',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
            'I',
        ];

        if ($jenistes==1){
            $konversi['code'] = $code[$bagiantes-1];
            if (!empty($sw[$bagiantes][$skor-1]) && !empty($klasifikasi[$bagiantes][$skor-1])){
                $konversi['sw'] = $sw[$bagiantes][$skor-1];
                $konversi['klasifikasi'] = $klasifikasi[$bagiantes][$skor-1];
            } else {
                $konversi['sw'] = '';
                $konversi['klasifikasi'] = '';
            }
            return $konversi;
        }

    }
 
}