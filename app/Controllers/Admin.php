<?php namespace App\Controllers;

use App\Models\Order_model;
use App\Models\Memberorderproduct_model;
use App\Models\Memberorderproductjenistes_model;
use App\Models\Productjenistes_model;
use App\Models\Jenistes_model;
use App\Models\Bagiantes_model;
use App\Models\User_model;
use App\Models\Setting_model;

class Admin extends BaseController
{
	public function __construct()
	{
        helper('form');
        $this->order = new Order_model;
        $this->memberorderproduct = new memberorderproduct_model;
        $this->memberorderproductjenistes = new memberorderproductjenistes_model;
        $this->productjenistes = new Productjenistes_model;
        $this->jenistes = new Jenistes_model;
        $this->bagiantes = new Bagiantes_model;
        $this->user = new User_model;
        $this->setting = new Setting_model;
	 }

	 public function index()
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'admin/dashboard';
		return view('admin/index', $this->data);
	}

	public function payment()
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
	}

        $this->data['profile'] = $this->ionAuth->user()->row();

		// data order
        $this->data['orders'] = $this->order->asArray()
            ->whereIn('status', ['checkout', 'paid', 'paid-confirm'])
            ->where('deleted', 0)
            ->orderBy('id', 'DESC')
            ->findAll();

        foreach($this->data['orders'] as $key => $order){
        	$this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
            ->where('id_order', $order['id'])
            ->where('deleted', 0)
            ->join('product', 'product.id=memberorderproduct.id_product')
            ->findAll();

            $this->data['orders'][$key]['member'] = $this->user->asArray()
            ->where('id', $order['id_member'])
            //->where('active', 1)
            ->findAll();


        }

		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'admin/'.__FUNCTION__;
		return view('admin/index', $this->data);
	}

	public function approve_payment($id_order)
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}

        $this->data['profile'] = $this->ionAuth->user()->row();

		// data order
		$this->order
            ->set(['status' => 'paid-confirm', 'paid-confirm' => date("Y-m-d H:i:s")])        
            ->where('id', $id_order)
            ->update();
     	return redirect()->to('/admin/payment'); 
	}

	

	public function student($category=NULL, $deleted=NULL)
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
		$this->data['profile'] = $this->ionAuth->user()->row();
		
        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('admin')){

			
        	// data student
			$del=1;
            if($deleted!=NULL){
                $del=0;
            }

			if($category=='activated'){
				$this->data['scope'] = __FUNCTION__.'_activated';
				
                $this->data['students'] = $this->user->asArray()
	            ->select('users.*')
	            ->where('users_groups.group_id', 2)
                ->where('users.not_deleted', $del)
	            ->where('users.active', 1)
	            ->join('users_groups', 'users.id=users_groups.user_id', 'left')
	            ->orderBy('users.id', 'DESC')
	            ->findAll();
            } else if($category=='unverified'){
				$this->data['scope'] = __FUNCTION__.'_unverified';
                $this->data['students'] = $this->user->asArray()
	            ->select('users.*')
	            ->where('users_groups.group_id', 2)
                ->where('users.not_deleted', $del)
	            ->where('users.active', 0)
	            ->join('users_groups', 'users.id=users_groups.user_id', 'left')
	            ->orderBy('users.id', 'DESC')
	            ->findAll();
            } else {
				$this->data['scope'] = __FUNCTION__;

				$this->data['students'] = $this->user->asArray()
	            ->select('users.*')
	            ->where('users_groups.group_id', 2)
                ->where('users.not_deleted', $del)
	            // ->where('users.active', 1)
	            ->join('users_groups', 'users.id=users_groups.user_id', 'left')
	            ->orderBy('users.id', 'DESC')
	            ->findAll();
			}
	         foreach ($this->data['students'] as $key0 => $student) {
	         	
				// data order
		        $this->data['students'][$key0]['orders'] = $this->order->asArray()
		            ->whereIn('status', ['checkout', 'paid', 'paid-confirm'])
		            ->where('id_member', $student['id'])
		            ->where('deleted', 0)
		            ->orderBy('id', 'DESC')
		            ->findAll();

	         }

	         // dd($this->data['students']);
			if($deleted!=NULL){
                $this->data['scope'] = 'student_deleted';
            }
			$this->data['template'] = 'admin/'.__FUNCTION__;
			return view('admin/index', $this->data);
        }	
	}


	public function admin()
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'admin/'.__FUNCTION__;
		return view('admin/index', $this->data);
	}

	public function psychologist()
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'admin/'.__FUNCTION__;
		return view('admin/index', $this->data);
	}

	public function product()
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
        $this->data['profile'] = $this->ionAuth->user()->row();
		
		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'admin/'.__FUNCTION__;
		return view('admin/index', $this->data);
	}

	public function jenis_tes()
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
        $this->data['profile'] = $this->ionAuth->user()->row();

		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'admin/'.__FUNCTION__;
		return view('admin/index', $this->data);
	}

	public function soaljawaban($id_jenis_tes)
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
		$this->data['scope'] = 'jenis_tes';
		$this->data['template'] = 'admin/'.__FUNCTION__;
		return view('admin/index', $this->data);
	}


	// public function product()
	// {
	// 	$this->data['scope'] = __FUNCTION__;
	// 	$this->data['template'] = 'admin/'.__FUNCTION__;
	// 	return view('admin/index', $this->data);
	// }

	public function faq()
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
		$this->data['profile'] = $this->ionAuth->user()->row();
		
		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'admin/'.__FUNCTION__;
		return view('admin/index', $this->data);
	}

	public function setting()
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
		$this->data['profile'] = $this->ionAuth->user()->row();

		$this->data['settings'] = $this->setting->asArray()
	            ->findAll();
	    foreach ($this->data['settings'] as $key => $setting) {
	    	$this->data['setting'][$setting['element']] = $setting['value']; 
	    }
		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'admin/'.__FUNCTION__;
		return view('admin/index', $this->data);
	}

	public function save_setting()
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}

		foreach ($_POST as $key => $value) {
			$this->setting
            ->set('value', $value)        
            ->where('element', $key)
            ->update();
		}

        return redirect()->to('/admin/setting');
	}

		//--------------------------------------------------------------------

}
