<?php 
namespace App\Controllers;
use App\Models\Feedback_model;
use App\Models\Setting_model;


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


require 'vendor/PHPMailer/PHPMailer/src/Exception.php';
require 'vendor/PHPMailer/PHPMailer/src/PHPMailer.php';
require 'vendor/PHPMailer/PHPMailer/src/SMTP.php';


class Home extends BaseController
{
	public function __construct()
	{
        $this->setting = new Setting_model;
        $this->feedback = new feedback_model;
		
		
	}
	
	public function index()
	{
		
		
		$this->data['isMember'] = false;
		if ($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')){
			$this->data['isMember'] = true;	
		}
		
		$this->data['settings'] = $this->setting->asArray()
                ->select('*')
                ->findAll();
		$this->data['products'] = $this->product->asArray()->findAll();
		$this->data['feedbacks'] = $this->feedback->asArray()
	        	->select('feedbackmember.*, users.email, users.phone, users.first_name, users.last_name, users.photo')
				->where('users_groups.group_id', 2)
				->where('feedbackmember.feedback!=', "")
	            ->join('users', 'users.id=feedbackmember.id_member')
	            ->join('users_groups', 'users.id=users_groups.user_id')
	            ->findAll();
		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'main/landing_page';

		return view('main/index', $this->data);
	}

	public function sendEmail(){
		// $attachment = base_url('uploads/Invoice.pdf');//html message untuk body email
		
		$email = \Config\Services::email();
		// dd($email);

		$config = [
			'protocol' => 'smtp',
			'SMTPHost' => 'pipe-psikotes.co.id',
			'SMTPUser' => 'admin@pipe-psikotes.co.id',
			'SMTPPass' => 'P@ssword123',
			'SMTPCrypto' => 'tls',
			'SMTPPort' => 587,
			'charset' => 'utf-8',
			'mailType' => 'html',
			'wordwrap' => true,
			'newline' => "\r\n",
			]; 
		$email->initialize($config);

		$email->setFrom('admin@pipe-psikotes.co.id', 'Your Name');
		$email->setTo('azifahmanset@gmail.com');

		$email->setSubject('Hai kami dari Edubridge');
		$email->setMessage('Testing the email class.');
		$email->send();
		if ($email->send()) {
            dd(true);
        } else {
            dd($email->printDebugger());                
        }

		// if(! $email->send()){
		// 	$email->printDebugger();
		//     d('email tidak terkirim');
		// 	return false;
		// }else{
		//     d('email terkirim');
		// 	return true;
		// }
	}
	
	public function sendemail2(){
		$email = 'member@member.com';
       
		$mail = new PHPMailer(true);
		$mail->SMTPDebug = SMTP::DEBUG_SERVER;
		$mail->SMTPDebug = 4;
		//$mail->IsSMTP();
		$mail->SMTPSecure = 'tls'; 
		$mail->Host = "pipe-psikotes.co.id"; //host masing2 provider email
		$mail->Port = 25;
		$mail->SMTPAuth = true;
		$mail->Username = "admin@pipe-psikotes.co.id"; //user email
		$mail->Password = "P@ssword123"; //password email 
		$mail->SetFrom("admin@pipe-psikotes.co.id","PIPE Psikotes"); //set email pengirim
		$mail->Subject = "Hasil tes Anda telah keluar"; //subyek email
		$mail->AddAddress($email, 'nama depan');  //email tujuan email
		$content = "kontennya";
		$mail->MsgHTML($content);
		$mail->Debugoutput = function($str, $level) {echo "debug level $level; message: $str";};
		if($mail->Send()) return "berhasil ke ".$email;
		else return "tidak berhasil karna gagal dikirim";
	}
	
	public function volunteer()
	{
		return view('main/volunteer');
	}
	
	public function panduan()
	{
		
phpinfo();
	}

		//--------------------------------------------------------------------

}
