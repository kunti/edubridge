<?php namespace App\Controllers;

use App\Models\Product_model;
use App\Models\Order_model;
use App\Models\Memberorderproduct_model;
use App\Models\Memberorderproductjenistes_model;
use App\Models\Productjenistes_model;
use App\Models\Jenistes_model;
use App\Models\Bagiantes_model;
use App\Models\User_model;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/PHPMailer/PHPMailer/src/Exception.php';
require 'vendor/PHPMailer/PHPMailer/src/PHPMailer.php';
require 'vendor/PHPMailer/PHPMailer/src/SMTP.php';



class Order extends BaseController
{
	public function __construct()
	{
        $this->product = new Product_model;
        $this->order = new Order_model;
        $this->memberorderproduct = new memberorderproduct_model;
        $this->memberorderproductjenistes = new memberorderproductjenistes_model;
        $this->productjenistes = new Productjenistes_model;
        $this->jenistes = new Jenistes_model;
        $this->bagiantes = new Bagiantes_model;
        $this->user = new User_model;

	}
	
	public function index()
	{
		 if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
		$this->data['product'] = $this->product->getProducts();
		$this->data['scope'] = __FUNCTION__;
		$this->data['template'] = 'main/products';
		return view('index', $this->data);	
	}

	public function id($id=NULL)
	{
		 if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
		$this->data['profile'] = $this->ionAuth->user()->row();
		
		$id_user  = $this->order->asArray()
            ->where('id', $id)
            ->where('deleted', 0)
            ->findColumn('id_member')[0];

        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members') && $this->data['profile']->id==$id_user) { 
        	// data order
	        $this->data['orders'] = $this->order->asArray()
	            ->whereIn('status', ['checkout', 'paid', 'paid-confirm'])
	            ->where('id', $id)
	            ->where('deleted', 0)
	            ->findAll();

	        foreach($this->data['orders'] as $key => $order){

	        	$this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
				->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
	            ->where('id_order', $order['id'])
	            ->where('deleted', 0)
	            ->join('product', 'product.id=memberorderproduct.id_product')
	            ->findAll();

	            foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();
				}

	            $this->data['orders'][$key]['member'] = $this->user->asArray()
	            ->where('id', $order['id_member'])
	            ////->where('active', 1)
	            ->findAll();
	        }

			$this->data['scope'] = __FUNCTION__;
			$this->data['template'] = 'order/detail';
			return view('index', $this->data);
        } else if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('admin')){
        	// data order
	        $this->data['orders'] = $this->order->asArray()
	            ->whereIn('status', ['checkout', 'paid', 'paid-confirm'])
	            ->where('id', $id)
	            // ->where('deleted', 0)
	            ->findAll();

	        foreach($this->data['orders'] as $key => $order){

	        	$this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
				->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
	            ->where('id_order', $order['id'])
	            ->where('deleted', 0)
	            ->join('product', 'product.id=memberorderproduct.id_product')
	            ->findAll();
	            
	            foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();
				}

	            $this->data['orders'][$key]['member'] = $this->user->asArray()
	            ->where('id', $order['id_member'])
	            ////->where('active', 1)
	            ->findAll();
	        }

	        // dd($this->data['orders']);

			$this->data['scope'] = 'all';
			$this->data['template'] = 'admin/order_detail';
			return view('admin/index', $this->data);
        } else if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('psychologists')){
        	// data order
	        $this->data['orders'] = $this->order->asArray()
	            ->whereIn('status', ['checkout', 'paid', 'paid-confirm'])
	            ->where('id', $id)
	            ->where('deleted', 0)
	            ->findAll();

	        foreach($this->data['orders'] as $key => $order){

	        	$this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
				->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
	            ->where('id_order', $order['id'])
	            ->where('deleted', 0)
	            ->join('product', 'product.id=memberorderproduct.id_product')
	            ->findAll();
	            
	            foreach ($this->data['orders'][$key]['products'] as $key2 => $product) {
                    $this->data['orders'][$key]['products'][$key2]['jenistes'] = $this->memberorderproductjenistes->asArray()
                    ->select('memberorderproductjenistes.*, jenistes.nama as nama, jenistes.desc as desc')
                    ->where('id_memberorderproduct', $product['id'])
                    ->join('jenistes', 'jenistes.id=memberorderproductjenistes.id_jenistes', 'right')
                    ->findAll();
				}

	            $this->data['orders'][$key]['member'] = $this->user->asArray()
	            ->where('id', $order['id_member'])
	            ////->where('active', 1)
	            ->findAll();
	        }

	        // dd($this->data['orders']);

			$this->data['scope'] = 'test';
			$this->data['template'] = 'admin/order_detail';
			return view('psychologist/index', $this->data);
        }

	}


	public function all($deleted=NULL)
	{
		 if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
		$this->data['profile'] = $this->ionAuth->user()->row();
		
        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('admin')){
			$del=0;
			if($deleted!=NULL){
				$del=1;
			}
			// data order
	        $this->data['orders'] = $this->order->asArray()
	            ->whereIn('status', ['checkout', 'paid', 'paid-confirm'])
	            ->where('deleted', $del)
	            ->orderBy('id', 'DESC')
	            ->findAll();
			
	        foreach($this->data['orders'] as $key => $order){
	        	$this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
                ->select('memberorderproduct.*, product.short_desc as short_desc, product.desc as desc')
	            ->where('id_order', $order['id'])
	            ->where('deleted', 0)
	            ->join('product', 'product.id=memberorderproduct.id_product')
	            ->findAll();

	            $this->data['orders'][$key]['member'] = $this->user->asArray()
	            ->where('id', $order['id_member'])
	            ////->where('active', 1)
	            ->findAll();


	        }

			$this->data['scope'] = __FUNCTION__;
			if($deleted!=NULL){
                $this->data['scope'] = 'all_deleted';
            }
			$this->data['template'] = 'admin/'.__FUNCTION__;
			return view('admin/index', $this->data);
        }	
	}

	public function payment()
	{
		 if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}

		$this->data['profile'] = $this->ionAuth->user()->row();
		
        if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('members')) { 
        	// data order
	        $this->data['orders'] = $this->order->asArray()
	            ->where('id_member', $_SESSION['user_id'])
	            ->whereIn('status', ['checkout', 'paid', 'paid-confirm'])
	            ->where('deleted', 0)
	            ->orderBy('id', 'DESC')
	            ->findAll();

	        foreach($this->data['orders'] as $key => $order){
	        	$this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
	            ->where('id_order', $order['id'])
	            ->where('deleted', 0)
	            ->join('product', 'product.id=memberorderproduct.id_product')
	            ->findAll();
	        }
	        $this->data['scope'] = __FUNCTION__;
			$this->data['template'] = 'member/'.__FUNCTION__;
			return view('member/index', $this->data);
			
        } else if($this->ionAuth->loggedIn() && $this->ionAuth->inGroup('admin')){

			// data order
	        $this->data['orders'] = $this->order->asArray()
	            ->whereIn('status', ['checkout', 'paid', 'paid-confirm'])
	            ->where('deleted', 0)
	            ->orderBy('id', 'DESC')
	            ->findAll();

	        foreach($this->data['orders'] as $key => $order){
	        	$this->data['orders'][$key]['products'] = $this->memberorderproduct->asArray()
	            ->where('id_order', $order['id'])
	            ->where('deleted', 0)
	            ->join('product', 'product.id=memberorderproduct.id_product')
	            ->findAll();

	            $this->data['orders'][$key]['member'] = $this->user->asArray()
	            ->where('id', $order['id_member'])
	            ////->where('active', 1)
	            ->findAll();

	        }

			$this->data['scope'] = __FUNCTION__;
			$this->data['template'] = 'admin/'.__FUNCTION__;
			return view('admin/index', $this->data);
        }	
	}

	public function done($id_memberorderproductjenistes)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $data = [
            'done-test' => date("Y-m-d H:i:s"),
            'status' => 'done-test',
        ];
        $this->memberorderproductjenistes->update($id_memberorderproductjenistes, $data);
		
        return redirect()->to('/test/paket_tes');
    }
	
	public function request($id_memberorderproduct)
    {
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
        }
        $data = [
			'req_unblock' => '1',
			'req_unblock_time' => date("Y-m-d H:i:s"),
			'is_unblock' => '0',

        ];
        $this->memberorderproduct->update($id_memberorderproduct, $data);
        return redirect()->to('/dashboard');
		
    }
	
	public function unblock($id_memberorderproduct, $email)
    {
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
        }
		$this->data['user'] = $this->user
		->where('email', $email)
		->first();

		$mail = new PHPMailer(true);

		//$mail->IsSMTP();
		$mail->SMTPSecure = 'tls'; 
		$mail->Host = "pipe-psikotes.co.id"; //host masing2 provider email
		$mail->SMTPDebug = 3;
		$mail->Port = 25;
		$mail->SMTPAuth = true;
		$mail->Username = "admin@pipe-psikotes.co.id"; //user email
		$mail->Password = "P@ssword123"; //password email 
		$mail->SetFrom("admin@pipe-psikotes.co.id","PIPE Psikotes"); //set email pengirim
		$mail->Subject = "Tes Anda telah dibuka"; //subyek email
		$mail->AddAddress($email, $this->data['user']['first_name'].' '.$this->data['user']['last_name']);  //email tujuan email
		$content = view('mail/unblock', $this->data);
		// echo $content;
		// exit();
		$mail->MsgHTML($content);
		if ($mail->Send()){
			// if there were no errors
			$data = [
				'is_unblock' => '1',
				'is_unblock_time' => date("Y-m-d H:i:s"),
				'req_unblock' => '0',
				
			];
			$this->memberorderproduct->update($id_memberorderproduct, $data);
			$this->session->setFlashdata('message', $this->ionAuth->messages());
			return redirect()->to('/test/req_activation');
		} else {
			d("Email gagal terkirim");
			exit();
		}

    }
	
	public function block($id_memberorderproduct)
    {
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
        }
        $data = [
			'is_unblock' => '0',
			'is_unblock_time' => date("Y-m-d H:i:s"),
        ];
        $this->memberorderproduct->update($id_memberorderproduct, $data);
		return redirect()->to('/test/req_activation');

    }
	
	public function delete($id_order)
    {
		if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $data = [
            'deleted' => '1',
        ];
        $this->order->update($id_order, $data);
		return redirect()->to('/order/id/'.$id_order);

    }

	public function undelete($id_order)
    {
        if (! $this->ionAuth->loggedIn()){
          $this->session->setFlashdata('message', lang('Global.login_first'));
          return redirect()->to('/auth/login');
        }
        $data = [
            'deleted' => '0',
        ];
        $this->order->update($id_order, $data);
		echo  "<script type='text/javascript'>";
		echo "window.close();";
		echo "</script>";
    }

	
}
