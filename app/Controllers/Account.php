<?php namespace App\Controllers;

use App\Models\User_model;


class Account extends BaseController
{
	public function __construct() { 
        $this->user = new User_model;
		$this->validation = \Config\Services::validation();
 
    }

    // public function index()
	// {
	// 	return view('homepage');
	// }

	public function profile($id=NULL)
	{
		 if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
        $this->data['profile'] = $this->ionAuth->user()->row();

		if($id){
			$this->data['myprofile'] = $this->user
	            ->where('id', $id)
	            ->first();
		} else {
			$this->data['myprofile'] = $this->user
	            ->where('id', $_SESSION['user_id'])
	            ->first();
		}

		$this->data['scope'] = __FUNCTION__;

		if ($this->ionAuth->inGroup('admin')) {
			$this->data['scope'] = 'student';
			$this->data['template'] = 'member/profile';
			return view('admin/index', $this->data);
		// kalo lihat profil sendiri
		} else if ($this->ionAuth->inGroup('psychologists') && $id==NULL) {
			$this->data['template'] = 'psychologist/profile';
			return view('psychologist/index', $this->data);
		// kalo liat profil orang lain
		} else if ($this->ionAuth->inGroup('psychologists') && $id!=NULL) {
			$this->data['template'] = 'member/profile';
			return view('psychologist/index', $this->data);
		} else if ($this->ionAuth->inGroup('members')) {
			$this->data['template'] = 'member/profile';
			return view('member/index', $this->data);
		}

          // dd($this->data['profile']);

		// return view('member/index', $this->data);
	}

	public function save()
	{
		if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
		
		// $this->validation->setRule('first_name', lang('Auth.create_user_validation_fname_label'), 'trim|required');
		// $this->validation->setRule('last_name', lang('Auth.create_user_validation_lname_label'), 'trim|required');
		$this->validation->setRule('phone', lang('Auth.create_user_validation_phone_label'), 'required|trim');
		$this->validation->setRule('company', lang('Auth.create_user_validation_company_label'), 'trim');
		// $this->validation->setRule('kotalahir', 'Kota Lahir', 'required');
		// $this->validation->setRule('pendterakhir', 'Pendidikan Terakhir', 'required');
		// $this->validation->setRule('domisili', 'Domisili', 'required');
		// $this->validation->setRule('birthdate', 'Date of Birth', 'trim|required|validate_age');
		
		if ($this->request->getPost() && $this->validation->withRequest($this->request)->run())
		{
			// dd($this->request->getPost('company'));
			$data = [
				// 'email' => $this->request->getPost('email'),
				// 'first_name' => $this->request->getPost('first_name'),
				// 'last_name' => $this->request->getPost('last_name'),
				// 'sipp' => $this->request->getPost('sipp'),
				// 'birthdate' => $this->request->getPost('birthdate'),
				// 'sex' => strtolower($this->request->getPost('sex')),
				// 'education_level' => $this->request->getPost('education_level'),
				// 'semester' => $this->request->getPost('semester'),
				'company' => $this->request->getPost('company'),
				'phone' => $this->request->getPost('phone'),
				// 'address' => $this->request->getPost('address'),
				// 'description' => $this->request->getPost('description'),
				// 'kotalahir' => $this->request->getPost('kotalahir'),
				// 'pendterakhir' => $this->request->getPost('pendterakhir'),
				// 'domisili' => $this->request->getPost('domisili'),
			];
			if ($this->request->getPost('not_deleted')!=null){
				$data['not_deleted'] = $this->request->getPost('not_deleted');
			}
			if ($this->user->update($this->request->getPost('id_user'), $data))
			{
				$this->session->setFlashdata('message', $this->ionAuth->messages());
			}
			else
			{
				$this->session->setFlashdata('message', $this->ionAuth->errors($this->validationListTemplate));
			}
			
			$this->uploadttd($this->request->getPost('id_user'));
			
			if ($this->request->getPost('redirect')==null){
				return redirect()->to('/account/profile/'.$this->request->getPost('id_user'));
			} else if ($this->request->getPost('redirect')=="yes"){
				return redirect()->to('/account/profile');
			}
		}
		$this->session->setFlashdata('message', $this->validation->getErrors());
		return redirect()->to('/account/profile');
	}

	public function uploadAvatar(){
		if(isset($_POST['image']))
			{
				$data = $_POST['image'];
				$image_array_1 = explode(";", $data);
				$image_array_2 = explode(",", $image_array_1[1]);
				$data = base64_decode($image_array_2[1]);

				$image_name = time() .'-'.$_POST['id_user']. '.png';
				$route = 'public/uploads/avatar/' . $image_name;
				$url = base_url('public/uploads/avatar/' . $image_name);
				$this->user->update($_POST['id_user'], ['avatar' => $image_name]);
				file_put_contents($route, $data);

				echo $url;

			} else {
				echo 'post image tidak ada';
			}
	}
	
	public function uploadttd($id_user){
		// dd($id_user);

		if (! $this->ionAuth->loggedIn()){
		$this->session->setFlashdata('message', lang('Global.login_first'));
		return redirect()->to('/auth/login');
		}
		// Validation
		$input = $this->validate([
			'file' => 'uploaded[file]|max_size[file,1024]|ext_in[file,png,jpg,jpeg,pdf],'
		]);

		if (!$input) { // Not valid
			// $data['validation'] = $this->validator; 
			// return view('users',$data); 
			session()->setFlashdata('message', 'File not valid. Only accept jpg, png, jpeg and pdf');
			session()->setFlashdata('alert-class', 'alert-danger');
		}else{ // Valid

			if($file = $this->request->getFile('file')) {
				if ($file->isValid() && ! $file->hasMoved()) {
				// Get file name and extension
				$name = $file->getName();
				$ext = $file->getClientExtension();

				// Get random file name
				$newName = $file->getRandomName(); 

				// Store file in public/uploads/ folder
				$file->move('public/uploads/ttd', $newName);

				// Save name to database
				$this->user->update($id_user, ['ttd' => $newName]);

				// File path to display preview
				$filepath = base_url()."/uploads/ttd/".$newName;
				
				// Set Session
				session()->setFlashdata('message', 'Uploaded Successfully!');
				session()->setFlashdata('alert-class', 'alert-success');
				session()->setFlashdata('filepath', $filepath);
				session()->setFlashdata('extension', $ext);

				}else{
				// Set Session
				session()->setFlashdata('message', 'File not uploaded.');
				session()->setFlashdata('alert-class', 'alert-danger');

				}
			}

		}
	
		return redirect()->to('/account/profile'); 
   }

   public function settings($id=NULL)
	{
		 if (! $this->ionAuth->loggedIn()){
			$this->session->setFlashdata('message', lang('Global.login_first'));
			return redirect()->to('/auth/login');
		}
        $this->data['profile'] = $this->ionAuth->user()->row();

		if($id){
			$this->data['myprofile'] = $this->user
	            ->where('id', $id)
	            ->first();
		} else {
			$this->data['myprofile'] = $this->user
	            ->where('id', $_SESSION['user_id'])
	            ->first();
		}


		if ($this->ionAuth->inGroup('admin')) {
			$this->data['scope'] = 'setting';
			$this->data['template'] = 'admin/setting';
			return view('admin/index', $this->data);
		} 
          // dd($this->data['profile']);

		// return view('member/index', $this->data);
	}

	//--------------------------------------------------------------------

}
