<?php

return [
   // page cart
   'cart'            => 'Booking',
   'my_cart'            => 'Keranjang Saya',
   'cart_detail'            => 'Detail Booking',
   'cart_add'            => 'Tes berhasil ditambahkan',
   'cart_remove'            => 'Tes berhasil dihapus',
   'lanjut_belanja'            => 'Lanjut Belanja',
   'delete'            => 'Hapus',
   'total_harga'            => 'Total Harga',
   'harga'            => 'Harga',
   'harga_desc'            => '*Setelah checkout, order kamu akan masuk ke menu Riwayat Pembelian menunggu pembayaran',
   'lihat_semua_paket'            => 'Lihat semua paket tes yang tersedia :',
   'cart_kosong'            => 'Booking list kamu kosong ',
   'delete_confirmation'            => 'Apakah kamu yakin ingin menghapus product ini dari Booking List ?',


   // navbar
   'tesgratis'             => 'TES GRATIS',
   'paket_tes'            => 'PAKET TES',
   'jenis_tes'            => 'Jenis Tes',
   'payment'            => 'Pembayaran',
   'tentang'            => 'TENTANG KAMI',
   'tentang_kami'            => 'Tentang Kami',
   'study_overseas'            => 'Kuliah di Luar Negeri',
   'detail'            => 'Detail',
   'resend_activation'            => 'Kirim ulang aktivasi',
   'btn-dashboard'            => 'Dasbor Saya',
   'btn-register'            => 'DAFTAR',
   'btn-login'            => 'MASUK',


   // login register page
   'login_first'            => 'Silahkan login terlebih dahulu',
   'login_welcome'            => 'Selamat datang',
   'login_desc'            => 'Silahkan Login menggunakan akun PIPE Psikotes kamu',
   'remember_me'            => 'Ingat akun saya',
   'create_account'            => 'Buat akun',
   'create_desc'            => 'Buat akun kemudian ambil paket tes',
   'forgot_password'            => 'Lupa password',
   'forgot_password_desc'            => 'Jangan khawatir, kamu bisa pulihkan akun PIPE Psikotes kamu',
   'forgot_password_desc2'            => 'Silahkan masukkan email kamu lalu kami akan mengirimkan email untuk reset password akun PIPE Psikotes kamu',
   'reset_password'            => 'Reset password',

   'name'            => 'Nama lengkap',
   'first_name'            => 'Nama depan',
   'last_name'            => 'Nama belakang',
   'company'            => 'Nama sekolah / universitas',
   'phone'            => 'Nomor telepon',
   'sex'            => 'Jenis kelamin',
   'age'            => 'Usia',
   'confirm_password'            => 'Konfirmasi password',

   // product page
   'order_detail'            => 'Detail order',
   'tambah_cart'            => 'Tambahkan ke keranjang',
   'bagikan'            => 'Bagikan',
   'dilihat'            => 'Dilihat',
   'peserta'            => 'Peserta',
   'dibagikan'            => 'Dibagikan',
   'terlaris'            => 'Terlaris',
   'deskripsi'            => 'Deskripsi',

   // payment page
   'payment_title'            => 'Kamu telah Checkout, silahkan lakukan pembayaran',
   'payment_desc'            => 'Order kamu telah berhasil disimpan dan akan diproses setelah kamu melakukan pembayaran dan dikonfirmasi oleh Admin. Pembayaran harus dilakukan dan dikonfirmasi melalui Whatsapp dalam waktu kurang dari 24 jam. Order kamu akan otomatis batal tanpa pemberitahuan apapun jika pembayaran tidak selesai',
   'payment_account'            => 'Silahkan transfer ke :',
   'lihat_riwayat_pembelian'            => 'Lihat riwayat pembelian',
   'confirmation'            => 'Konfirmasi',

   // member menu
   'my_test'            => 'Paket tes saya',
   'test_detail'            => 'Detail paket tes',
   'purchase'            => 'Riwayat pembelian',
   'expired'            => 'Hangus dalam',
   'expired'            => 'Hangus dalam',
   'hasexpired'            => ' Sudah hangus',
   'hasexpired_detail'            => ' Sudah hangus, Anda tidak dapat mengerjakan tes. Hubungi Admin kami untuk informasi lebih lanjut',
   'day'            => 'hari',
   'hour'            => 'jam',
   'minute'            => 'menit',
   'test_package'            => 'Paket tes',
   'documentation'            => 'Dokumentasi',
   'test_working'            => 'Pengerjaan',
   'correction'            => 'Koreksi',
   'result'            => 'Hasil',
   'see_result'            => 'Lihat hasil',
   'download_result'            => 'Download hasil',
   'preview_result'            => 'Preview hasil',
   'take_test'            => 'Kerjakan tes',
   'action'            => 'Aksi',
   'psikolog'            => 'Psikolog',
   'student'            => 'Peserta',
   'order_code'            => 'Kode Pemesanan',
   'Summarize'            => 'Analisa',
   'ready_summarize'            => 'Siap dianalisa',
   'my_summarization'            => 'Analisa saya',

   // test status
   'summary'            => 'Analisa',
   'test_status'            => 'Status Tes',
   'start_summarization'            => 'Mulai menganalisa',
   'ready_to_summarize'            => 'Siap dianalisa',
   'on_summarization'            => 'Sedang dianalisa',
   'summarized'            => 'Sudah dianalisa',
   'on_correction'            => 'Sedang dikoreksi',
   'on_test'            => 'Sedang mengerjakan tes',
   'test_not_ready'            => 'Tes belum siap dikerjakan',
   'ready_test'            => 'Tes siap dikerjakan',
   'done'            => 'Selesai',
   'assign_as_done'            => 'Tandai Selesai',
   'lanjutkan'            => 'Lanjutkan',
   'lihat_jawaban'            => 'Lihat jawaban',
   'psikolog_menganalisa'            => 'Psikolog sedang <br>menganalisa tes Anda',

      // payment status
   'paid'            => 'Terbayar',
   'confirmed'            => 'Terkonfirmasi',
   'pending'            => 'Tertunda',
   'waiting_confirmation'            => 'Terbayar - Menunggu konfimasi',
   'failed'            => 'Gagal',
   'payment_proof'            => 'Bukti pembayaran',
   'payment_method'            => 'Lihat cara pembayaran',
   'upload_payment'            => 'Upload bukti pembayaran',
   'change_upload_payment'            => 'Upload ulang bukti pembayaran',

   // feedback page
   'feedback'            => 'Feedback',
   'feedback_detail'            => 'Detail Feedback',
   'myfeedback'            => 'Feedback saya',
   'showed'            => 'Ditampilkan',
   'hidden'            => 'Sembunyikan',
   'save'            => 'Simpan perubahan',
   'save_all'            => 'Simpan semua',
   'datetime'            => 'Tanggal & Waktu',

   // profile page
   'my_profile'            => 'Profil saya',
   'identity'            => 'Identitas',
   'birth_place'            => 'Tempat lahir',
   'birth_date'            => 'Tanggal lahir',
   'contact_address'            => 'Kontak & alamat',
   'address'            => 'Alamat',
   'kota'            => 'Kota',
   'kota_kab'            => 'Kota / kabupaten',
   'kodepos'            => 'Kode pos',
   'settings'            => 'Pengaturan',
   'logout'            => 'Keluar',

   'switch_lang'            => 'EN',
   'welcome'            => 'Selamat Datang di PIPE PSIKOTES',
   'welcome_desc'           => '<span>PIPE (Potential, Interest, Personality) Psikotes adalah sebuah layanan tes minat dan kecerdasan berbasis online pertama di Indonesia untuk mengenali potensi akademis dan ketertarikan yang ditujukan membantu pemetaan jurusan kuliah yang sesuai sehingga siswa mampu melakukan perencanaan dan pilihan yang tepat untuk masa depannya.',
   'welcome_desc_klickbtn'            => 'Klik tombol dibawah untuk mengikuti PIPE PSIKOTES GRATIS',
   // 'welcome_desc'           => 'Tes berbasis online pertama berdasarkan analisa PIPE (Potential, Interest, Personality). Dapatkan hasil dengan cepat dan akurat dari psikolog ahli kami',
   'what_pipe'            => 'Apa itu PIPE',
   'our_expert'            => 'Lihat psikolog ahli kami',
   'info1'                  => 'Belajar di Luar Negeri adalah impian banyak orang',
   'info1_desc'            => 'Mendapatkan gelar sarjana dari universitas luar negeri dengan banyak pengalaman unik akan memberikan nilai tersendiri saat memasuki dunia profesional, terutama saat kembali ke Indonesia. Untuk mencapai semua hal tersebut di atas, persiapan harus direncanakan dan dilaksanakan dengan sempurna',

   'info2'                  => 'Persiapkan diri kamu',
   'info2_desc'            => 'Waktunya sekarang dari biasa saja untuk menjadi hebat. Orang-orang di dunia ini cenderung menghindari orang hebat. Karena mereka warnanya cerah dan menonjol. Waktunya kamu bersinar sekarang.',

   'info3'                  => 'Kenali dirimu',
   'info3_desc'            => 'Keterampilan dan kemampuan adalah tugas yang secara alami dapat kamu lakukan dengan baik. Bakat dan kekuatan yang kamu pakai sebagai siswa dan / atau karyawan. Lebih mudah jika kamu mengenali apa saka ketrampilan, kemampuan, bakat dan kekuatan yang kamu miliki dan seberapa besar.',

   'insight'                  => 'PIPE Psikotes bisa bantu kamu untuk mengenali kemampuan kamu',
   'insight_desc'             => 'Menggunakan analitik yang tepat tentang diri kamu cukup dengan mengikuti tes. Kami memiliki psikolog khusus untuk kamu',



   // faq page
   'question1'                  => 'Menambahkan test ke dalam keranjang',
   'question2'                  => 'Checkout',
   'question3'                  => 'Upload bukti pembayaran',
   'question4'                  => 'Mengerjakan tes',
   'question5'                  => 'Menulis feedback saya',

   'answer1'                  => 'Buka menu Keranjang pada Dashboard Anda, klik tes yang Anda ingin tambahkan ke dalam keranjang. Anda akan melihat detail tes tersebut kemudian klik Tambahkan ke keranjang',
   'answer2'                  => 'Buka menu Keranjang pada Dashboard Anda, klik tombol Checkout setelah Anda menambahkan tes yang Anda inginkan',
   'answer3'                  => 'Anda telah checkout kemudian Anda ingin mengupload bukti pembayaran. Silahkan klik menu Riwayat pembelian pada Dashboard Anda, cari order Anda kemudian upload bukti pembayaran dengan memilih file jpg, jpeg atau png yang dimaksud. Lalu klik Upload. Tunggu sampai admin mengkonfirmasi bukti pembayaran Anda',
   'answer4'                  => 'Jawaban',

   // volunteer
   'education'                  => 'Pendidikan',
   'education_now'                  => 'Jenjang pendidikan',
   'choose'                  => 'Silahkan pilih',
   'being_volunteer'                  => 'Daftar tes gratis',
   'semester'                  => 'Kelas / Semester',


];