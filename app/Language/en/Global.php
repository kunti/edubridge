<?php

return [
	// page cart
	'cart'            => 'Booking',
   'my_cart'            => 'My Booking',
   'cart_detail'            => 'Booking Detail',
   'cart_add'            => 'Test booked successfully ',
   'cart_remove'            => 'Test removed successfully ',
   'lanjut_belanja'            => 'Continue shopping',
   'delete'            => 'Delete',
   'total_harga'            => 'Total Price',
   'harga'            => 'Price',
   'harga_desc'            => '*After checkout, your order will go into the Purchase History menu waiting for payment',
   'lihat_semua_paket'            => 'See all available test :',
   'cart_kosong'            => 'Your booking list is empty ',
   'payment'            => 'Payment',
	'delete_confirmation'            => 'Are you sure you want to delete this product from your cart?',

	// menu landing page
   'tesgratis'             => 'FREE TEST',
   'paket_tes'            => 'TEST PACK',
   'jenis_tes'            => 'Test type',
   'tentang'            => 'ABOUT US',
   'tentang_kami'            => 'About Us',
   'study_overseas'            => 'Study Overseas',
   'detail'            => 'See detail',
   'resend_activation'            => 'Resend Activation',
   'btn-dashboard'            => 'MY DASHBOARD',
   'btn-register'            => 'REGISTER',
   'btn-login'            => 'LOGIN',

   // login register page
   'login_welcome'            => 'Welcome back',
   'login_first'            => 'Please login first',
   'login_desc'            => 'Login to your PIPE Psikotes account',
   'remember_me'            => 'Remember me',
   'create_account'            => 'Create account',
   'create_desc'            => 'Create account and take the test',
   
   'forgot_password'            => 'Forgot password',
   'forgot_password_desc'            => 'Dont worry get back your PIPE Psikotes account',
   'forgot_password_desc2'            => 'Please enter your Email so we can send you an email to reset your password',
   'reset_password'            => 'Reset password',

   'Name'            => 'Name',
   'first_name'            => 'First name',
   'last_name'            => 'Last name',
   'company'            => 'Institution name',
   'phone'            => 'Phone',
   'age'            => 'Age',
   'sex'            => 'Sex',
   'confirm_password'            => 'Confirm password',

   // product page
   'order_detail'            => 'Order detail',
   'tambah_cart'            => 'Add to cart',
   'bagikan'            => 'Share',
   'dilihat'            => 'Viewed',
   'peserta'            => 'Participant',
   'dibagikan'            => 'Shared',
   'terlaris'            => 'Bestseller',
   'deskripsi'            => 'Description',

   // payment page
   'payment_title'            => 'You have checkout, please do payment' ,
   'payment_desc'            => 'Please complete your payment within 24 hours
   Your order has been successfully received and will be processes once payment has been confirmed
   Payment must be made and notified to us via WA within 24 hours. Orders will be automatically cancelled without prior notice if payment is not received within the stipulated timeframe given.',
   'payment_account'            => 'Please transfer to :',
   'lihat_riwayat_pembelian'            => 'Go to purchase history',
   'confirmation'            => 'Confirmation',

   // member menu
   'my_test'            => 'My test',
   'test_detail'            => 'Test detail',
   'purchase'            => 'Purchase history',
   'expired'            => 'Expired in',
   'hasexpired'            => ' Expired',
   'hasexpired_detail'            => ' Expired, you cant take the test. Chat our Admin for more detail',
   'day'            => 'day',
   'hour'            => 'hour',
   'minute'            => 'minute',
   'test_package'            => 'Test',
   'documentation'            => 'Documentation',
   'test_working'            => 'Test working',
   'correction'            => 'Correction',
   'result'            => 'Result',
   'see_result'            => 'See result',
   'download_result'            => 'Download result',
   'preview_result'            => 'Preview result',
   'take_test'            => 'Take the test',
   'action'            => 'Action',
   'psikolog'            => 'Psychologist',
   'student'            => 'Student',
   'order_code'            => 'Order Code',
   'summarize'            => 'Summarize',
   'ready_summarize'            => 'Ready to summarize',
   'my_summarization'            => 'My summarization',

   // test status
   'summary'            => 'Summary',
   'test_status'            => 'Test Status',
   'start_summarization'            => 'Start Summarization',
   'ready_to_summarize'            => 'Ready to Summarize',
   'ready_to_summarize'            => 'Ready to Summarize',
   'on_summarization'            => 'On summarization',
   'summarized'            => 'Summarized',
   'on_correction'            => 'On correction',
   'on_test'            => 'On test',
   'test_not_ready'            => 'Test not ready to take',
   'ready_test'            => 'Test ready to take',
   'done'            => 'Done',
   'assign_as_done'            => 'Change status as Done',
   'lanjutkan'            => 'Continue',
   'lihat_jawaban'            => 'See answers',
   'psikolog_menganalisa'            => 'Psychologist is <br>analyzing your test',

   // payment status
   'paid'            => 'Paid',
   'confirmed'            => 'Paid - Confirmed',
   'pending'            => 'Pending',
   'waiting_confirmation'            => 'Paid - Waiting Confirmation',
   'failed'            => 'Failed',
   'payment_proof'            => 'Payment proof',
   'payment_method'            => 'Payment instructions',
   'upload_payment'            => 'Upload payment receipt',
   'change_upload_payment'            => 'Change payment receipt',

   // feedback page
   'feedback'            => 'Feedback',
   'feedback_detail'            => 'Feedback Detail',
   'myfeedback'            => 'My feedback',
   'showed'            => 'Showed',
   'hidden'            => 'Hidden',
   'save'            => 'Save Changes',
   'save_all'            => 'Save All Changes',
   'datetime'            => 'Datetime',

   // profile page
   'my_profile'            => 'My profile',
   'identity'            => 'Identity',
   'birth_place'            => 'Birth place',
   'birth_date'            => 'Birth date',
   'contact_address'            => 'Contact & Address',
   'address'            => 'Address',
   'kota'            => 'City',
   'kota_kab'            => 'Region',
   'kodepos'            => 'Postal code',
   'settings'            => 'Settings',
   'logout'            => 'Logout',



   'switch_lang'            => 'ID',
   'welcome'				=> 'Welcome to PIPE Psikotes PIPE Test',
   'welcome_desc'			=> 'The first online test based on PIPE (Potential, Interest, Personality) analysis. Get quick and accurate result from our expert psychologist',
   'what_pipe'            => 'What is PIPE',
   'our_expert'            => 'See our experts',
   
   'info1'                  => 'Study Overseas is a Dream of Many',
   'info1_desc'				=> 'Being granted a bachelor’s degree from university overseas with lots of unique experiences above bestow us with our own value when entering professional world, especially when going back to Indonesia. To achieve all things mentioned above, all preparations should be planned and executed perfectly',

   'info2'                  => 'Preparing Yourself',
   'info2_desc'				=> 'The time is now for it to be okay to be great. People in this world shun people for being great. For being a bright color. For standing out. But the time is now.',

   'info3'                  => 'Get Know YourSelf',
   'info3_desc'				=> 'Skills and abilities are tasks that you naturally do well, talents and strengths that you bring to the table as a student and/or employee. Its easier if you recognize what skills, abilities, talents and strengths you have and how much',

   'insight'                  => 'PIPE Psikotes help you know your Ability',
   'insight_desc'             => 'With right analytic of yourself by taking the test. We have psychologist for you',

   // faq page
   'question1'                  => 'Adding product test package to cart',
   'question2'                  => 'Place order or checkout',
   'question3'                  => 'Upload payment receipt',
   'question4'                  => 'Take the test',
   'question4'                  => 'Dowload test result',
   'question5'                  => 'Write my feedback',

   'answer1'                  => 'Go to cart menu in dashboard, click the test you want to add. You will see the test detail, so click add to cart.',
   'answer2'                  => 'Go to cart menu in dashboard, and then click checkout button if you have adding the test you want',
   'answer3'                  => 'You have checkout and then want to upload payment receipt for the order. Click menu purchase history, find your order and upload the payment receipt by select the file you want. Then click upload. Wait until admin approving your payment receipt.',
   'answer4'                  => 'Go to Feedback menu, fill the text area and finally click Save Changes',

   // volunteer
   'education'                  => 'Education',
   'education_now'                  => 'Current education level',
   'choose'                  => 'Choose',
   'being_volunteer'                  => 'Register to be volunteer',
   'semester'                  => 'Grade / Semester',

];