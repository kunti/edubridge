<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">My Correction <br>& Summarization</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="#">Product List</a></li>
              <li class="breadcrumb-item"><a href="#">Product Detail</a></li>
              <li class="breadcrumb-item active" aria-current="page">Correction</li>
            </ol>
          </nav>
        </div>
       <!--  <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral">New</a>
          <a href="#" class="btn btn-sm btn-neutral">Filters</a>
        </div> -->
      </div>
    </div>
  </div>
</div>
<!-- Page content -->

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card-header border-0">
          <h3><?= lang('Global.student') ?></h3>
          <div class="row">
            <div class="col-auto">
              <a href="#">
                <img style="max-width: 140px" alt="Image placeholder" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
              </a>
            </div>
            <div class="col">
              <?php 
                        if ($orders[0]['member'][0]['birthdate']==null || $orders[0]['member'][0]['birthdate']=='0000-00-00'){
                            $y=0;
                            $m=0;
                            $d=0;
                        } else {
                            $birthDate = new DateTime($orders[0]['member'][0]['birthdate']);
                            $today = new DateTime($orders[0]['products'][0]['jenistes'][0]['start-test']);
                            if ($birthDate > $today) { 
                                $y=0;
                                $m=0;
                                $d=0;
                            }
                            $y = $today->diff($birthDate)->y;
                            $m = $today->diff($birthDate)->m;
                            $d = $today->diff($birthDate)->d;   
                        }
                        ?>
              <table>
                  <tbody class="text-sm">
                    <tr>
                    <td><?= lang('Global.name') ?></td><td>: <a href="<?= base_url('account/profile/').'/'.$orders[0]['member'][0]['id'] ?>"><b><span><?= $orders[0]['member'][0]['first_name'].' '.$orders[0]['member'][0]['last_name']?></span></b></a></td>
                    </tr>
                    <tr>
                    <td><?= lang('Global.phone') ?></td><td>: <span><?= $orders[0]['member'][0]['phone'] ?></span></td>
                    </tr>
                    <tr>
                    <td>Email</td><td>: <span><?= $orders[0]['member'][0]['email'] ?></span></td>
                    </tr>
                    <tr>
                    <td><?= lang('Global.sex') ?></td><td>: <span><?= $orders[0]['member'][0]['sex'] ?></span></td>
                    </tr>
                    <tr>
                    <td><?= lang('Global.age') ?></td><td>: <span><?= $y." tahun ".$m." bulan ".$d." hari" ?></span></td>
                    </tr>
                    <tr>
                    <td><?= lang('Global.education_now') ?></td><td>: <span><?= $orders[0]['member'][0]['education_level'] ?></span></td>
                    </tr>
                    <tr>
                    <td><?= lang('Global.semester') ?></td><td>: <span><?= $orders[0]['member'][0]['semester'] ?></span></td>
                    </tr>
                    <tr>
                    <td><?= lang('Global.company') ?></td><td>: <span><?= $orders[0]['member'][0]['company'] ?></span></td>
                    </tr>
                    <tr>
                    <td><?= lang('Global.address') ?></td><td>: <span><?= $orders[0]['member'][0]['address'] ?></span></td>
                    </tr>
                  </tbody>
                </table>
            </div>
            
          </div>
          <hr>
          <h3><?= $orders[0]['products'][0]['nama'] ?></h3>
          <span class="text-sm"><?= $orders[0]['products'][0]['desc'] ?></span>
          <br>
          <sm><a href="">Order code : <?= 'PIPETEST'.sprintf('%04d', $orders[0]['id']) ?></a></sm>
          <br>
          Status : 
          <?php 
            if($orders[0]['products'][0]['start-summarization']=='0000-00-00 00:00:00'){
              ?>
              <span class="badge badge-info mr-4">
                <span class="status"><?= lang('Global.ready_to_summarize') ?></span>
              </span>
              <?php
            } else if ($orders[0]['products'][0]['status']=='start-summarization'){
              ?>
              <span class="badge badge-warning mr-4">
                <span class="status"><?= lang('Global.on_summarization') ?></span>
              </span>
              <a target="_blank" href="<?php echo base_url('pdf/mpdf/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id'].'/I');?>" class="btn btn btn-success"><?= lang('Global.preview_result') ?></a>
              <?php
            } else if ($orders[0]['products'][0]['status']=='summarized'){
              ?>
              <span class="badge badge-success mr-4">
                <span class="status"><?= lang('Global.done') ?></span>
              </span> 
              <a target="_blank" href="<?php echo base_url('pdf/mpdf/'.$orders[0]['id'].'/'.$orders[0]['products'][0]['id'].'/D');?>" class="btn btn btn-success"><?= lang('Global.download_result') ?></a>
              <?php
            } else {

            }
          ?>
          <hr>
        </div>
        <?php 
        // dd($bagiantes);
        foreach ($jenistes as $key0 => $jtes) {
          // d($orders);
          // d($key0);
          // d($bagian);
          ?>

        <div class="card-header border-0">
            <div class="">
              <h3><?=($key0+1).'. '.$orders[0]['products'][0]['jenistes'][$key0]['nama'] ?></h3>
              <!-- <span><?= $orders[0]['products'][0]['jenistes'][$key0]['desc'] ?></span> -->
            </div>
        </div>
        <style>
          .ringkas table th, .table td {
    padding: 0.4rem;
    vertical-align: top;
}

        </style>
        <?php 
        // dd($jtes['bagiantes']);
        foreach ($jtes['bagiantes'] as $key2 => $bagian) {
        ?>
                    <!-- p>Soal yang terjawab :</p>
                    <p>Soal yang tidak terjawab :</p> -->
                    
                        <div class="table">
                          <table class="ringkas table-responsive" style="border: 1px solid black;" >
                            
                            <tbody class="list">
                            <td style="width:200px;border: 1px solid black;"><?= $bagian['judul'] ?></td>
                            <td style="width:30px; text-align:center;border: 1px solid black;"><?php if(!empty($bagian['tidak_menjawab'][0]['tidak_menjawab'])) echo $bagian['tidak_menjawab'][0]['tidak_menjawab'] ?></td>
                              <?php  
                                foreach($bagian['soal'] as $key => $soal) {
                                $nomorsoal = $key+1;
                               
                                ?>
                                
                                  <?php if(!empty($soal['jawabanmember'][0]['jawaban_isian'])) {
                                    echo '<td style="background-color:green; color:white;border: 1px solid black;">'.$nomorsoal.'</td>'; 
                                  } else {
                                    echo '<td style="background-color:red; color:white;border: 1px solid black;">'.$nomorsoal.'</td>';
                                    } ?>
                                
                                <!-- <td>
                                </td> -->
                                <!-- <td><input type="number"></input></td> -->
                              <?php
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>
                       
                    <!-- akhir isian -->
                    
              <?php
            }
            }
            ?>
      <!-- <div class="card"> -->
        <!-- Card header -->
        <?php 
        // dd($bagiantes);
        foreach ($jenistes as $key0 => $jtes) {
          // d($orders);
          // d($key0);
          // d($bagian);
          ?>

        <div class="card-header border-0">
            <div class="">
              <h3><?=($key0+1).'. '.$orders[0]['products'][0]['jenistes'][$key0]['nama'] ?></h3>
              <!-- <span><?= $orders[0]['products'][0]['jenistes'][$key0]['desc'] ?></span> -->
            </div>
        </div>
        <?php 
        foreach ($jtes['bagiantes'] as $key2 => $bagian) {
        ?>
        <div class="card-header border-0">
          <?= $bagian['judul'] ?><br>
          Tidak menjawab : 
          <?php if(!empty($bagian['tidak_menjawab'][0]['tidak_menjawab'])) echo $bagian['tidak_menjawab'][0]['tidak_menjawab'] ?>
              <h5 class="mb-0 col-8">
                  </h5>
                <div id="collapse<?= $bagian['id']; ?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body">
                    <!-- p>Soal yang terjawab :</p>
                    <p>Soal yang tidak terjawab :</p> -->
                    
                        <div class="table-responsive">
                          <table id="" class="table" >
                            <thead class="thead-light">
                              <tr>
                                <th class="th-sm text-center">Bagian
                                </th>
                                <th class="th-sm text-center">Nomor Soal
                                </th>
                               <!--  <th class="th-sm">Soal
                                </th> -->
                                <th class="th-sm text-center">Waktu menjawab
                                </th>
                                <th class="th-sm text-center">Sub bagian
                                </th>
                                <th class="th-sm text-center">Urutan Soal <br> per sub bagian 
                                </th>
                                <!-- <th class="th-sm">Jawaban
                                </th> -->
                                <!-- <th class="th-sm">Skor -->
                                <!-- </th> -->
                              </tr>
                            </thead>
                            <tbody class="list">
                              <?php  
                                foreach($bagian['soal'] as $key => $soal) {
                                $nomorsoal = $key+1;
                               
                                ?>
                              <tr>
                                <td class="text-center"><?= $bagian['urutan'] ?></td>
                                
                                <td class="text-center"><?= $nomorsoal ?></td>
                               <!--  <td><?= $soal['soal'] ?>
                                  <?php
                                    if($soal['gambar']){
                                      ?>
                                      <div class="col-sm-12">
                                        <img src="<?php echo base_url($soal['gambar']); ?>" class="rounded img-fluid" alt="..." style="height: 150px;">
                                      </div>
                                      <?php
                                    }
                                  ?>
                                </td> -->
                                <td class="text-center">
                                  <?php if(!empty($soal['jawabanmember'][0]['jawaban_isian'])) {echo $soal['jawabanmember'][0]['jawaban_time']; } else {echo '<span style="color:#9C020E">Kosong<span>';} ?>
                                </td>
                                <td class="text-center"><?= $soal['sub_bagian'] ?></td>
                                <td class="text-center"><?= $soal['urutan'] ?></td>
                                <!-- <td>
                                  <?php if(!empty($soal['jawabanmember'][0]['jawaban_isian'])) {echo $soal['jawabanmember'][0]['score']; } ?>
                                </td> -->
                                <!-- <td><input type="number"></input></td> -->
                              </tr>
                              <?php
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>
                       
                    <!-- akhir isian -->
                    
                  </div>
                </div>
              </div>
              <!-- akhir card -->
              <?php
            }
            }
            ?>
        <!-- </div> -->
      </div>
    </div>
  </div>
</div>
