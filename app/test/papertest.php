<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="PIPE Psikotest">
  <meta name="author" content="PIPE Psikotest">
  <title>PIPE Psikotest</title>
  <!-- Favicon -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
  <link rel="shortcut icon" type="image/png" href="<?php echo base_url('src/assets/img/brand/favicon.png'); ?>">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/nucleo/css/nucleo.css'); ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/@fortawesome/Montserrat-free/css/all.min.css'); ?>" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/css/argon.css?v=1.2.0'); ?>" type="text/css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="<?php echo base_url('src/assets/vendor/bootstrap/bootstrap.min.css'); ?>" type="text/css">
  <!-- Datatable -->
  <link rel="stylesheet" href="<?php echo base_url('src/node_modules/mdbootstrap/css/addons/datatables2.min.css'); ?>" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel='stylesheet'>
  <link rel="stylesheet" href="<?php echo base_url('src/assets/js/notifications.css'); ?>" type="text/css">


  <style>
  body {
      font-family: 'Montserrat';
      overflow-x: hidden;
    }
  @media (max-width: 768px) { /* use the max to specify at each container level */
      .table-descriprion {    
          width:360px;  /* adjust to desired wrapping */
          display:table;
          white-space: pre-wrap; /* css-3 */
          white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
          white-space: -pre-wrap; /* Opera 4-6 */
          white-space: -o-pre-wrap; /* Opera 7 */
          word-wrap: break-word; /* Internet Explorer 5.5+ */
      }
  }
  </style>

  <style type="text/css">
    @charset "UTF-8";
  @import url("//fonts.googleapis.com/css?family=Raleway:400,800");
  @import url("//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css");
  .row:after {
    clear: both;
    content: "";
    display: table;
  }
  .row:before {
    content: "";
    display: table;
  }

  *,
  *:after,
  *:before {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
  }

  body {
    padding-right: 0px;
    background: #fff;
    font-family: "Montserrat";
    line-height: 1.55;
    color: #000000;
    font-weight: 400;
  }
  body a {
    color: #27b198;
    text-decoration: none;
    border-bottom: 2px solid #fff;
  }
  body a:hover {
    border-color: #eeeeee;
  }

  code {
    background: #fff;
    color: #999999;
    padding: 2px 8px;
  }

  header {
    position: relative;
    text-align: center;
    text-transform: uppercase;
    color: #232941;
    border-bottom: 1px solid dareken(#FDC834, 15%);
    letter-spacing: 4px;
    padding: 12px 0;
    background: #fafafa;
    border-bottom: 1px solid #eeeeee;
  }

  .wrap {
    max-width: 48em;
    margin: 0 auto;
    padding: 2.5em 0 4em;
  }

  .row.last {
    border-bottom: 1px solid #eeeeee;
  }

  section {
    position: relative;
    padding: 20px 20px;
    width: 100%;
    /*min-height: 12em;*/
    float: left;
    /*background: #fafafa;*/
    border-top: 1px solid #eeeeee;
    border-left: 1px solid #eeeeee;
  }
  section:nth-child(even) {
    border-right: 1px solid #eeeeee;
  }
  section.item-incorrect {
    background: #f6f6f6;
  }
  section.item-incorrect:before {
    position: absolute;
    z-index: 399;
    bottom: 0;
    right: 0;
    content: "";
    height: 0;
    width: 0;
    border: 35px solid;
    border-color: transparent #ff5c61 #ff5c61 transparent;
  }
  section.item-incorrect:after {
    line-height: 1.4;
    position: absolute;
    z-index: 499;
    font-family: "Montserrat";
    content: "";
    bottom: 0;
    right: 7px;
    font-size: 1.9em;
    color: #ff383e;
  }
  section.item-correct:before {
    position: absolute;
    z-index: 399;
    bottom: 0;
    right: 0;
    content: "";
    height: 0;
    width: 0;
    border: 35px solid;
    border-color: transparent #48d7bd #48d7bd transparent;
  }
  section.item-correct:after {
    line-height: 1.4;
    position: absolute;
    z-index: 499;
    font-family: "Montserrat";
    content: "";
    bottom: 0;
    right: 7px;
    font-size: 1.9em;
    color: #FDC834;
  }

  input[type="radio"] {
    position: absolute;
    visibility: hidden;
  }
  input[type="radio"] + label {
    background: #fff;
    display: inline-block;
    padding: 5px 15px;
    margin: 5px 10px 5px 0;
    border: 1px solid #eeeeee;
    -webkit-transition: all 0.1s linear;
    -moz-transition: all 0.1s linear;
    transition: all 0.1s linear;
  }
  input[type="checkbox"] + label {
    background: #fff;
    display: inline-block;
    padding: 5px 15px;
    margin: 5px 10px 5px 0;
    border: 1px solid #eeeeee;
    -webkit-transition: all 0.1s linear;
    -moz-transition: all 0.1s linear;
    transition: all 0.1s linear;
  }
  input[type="radio"] + label:before {
    content: "\f10c";
    font-family: "fontawesome";
    margin-right: 7px;
    color: #FDC834;
  }
  input[type="radio"] + label:hover {
    cursor: pointer;
  }
  input[type="radio"]:checked + label {
    background: #FDC834;
    color: #232941;
  }
  input[type="radio"]:checked + label:before {
    content: "\f192";
    color: #232941;
  }


  input[type="checkbox"]:checked + label {
    background: #FDC834;
    color: #232941;
  }
  .question {
    /*font-weight: bold;*/
  }

  .submit {
    padding: 20px 5px;
  }
  .submit button {
    display: block;
    outline: none;
    width: 300px;
    margin: 1em auto 1.5em;
    padding: 0.8em 1em;
    background: #f8f8f8;
    color: gainsboro;
    text-transform: uppercase;
    letter-spacing: 3px;
    border: 1px solid #eeeeee;
  }

  #emc-score {
    text-align: center;
    opacity: 0;
    padding: 0;
    -webkit-transition: all 0.55s ease;
    -moz-transition: all 0.55s ease;
    transition: all 0.55s ease;
  }
  #emc-score.new-score {
    opacity: 1;
    background: #FDC834;
    color: #fbfbfb;
    padding: 20px;
  }

  #emc-submit {
    position: relative;
    -webkit-transition: all 0.33s ease;
    -moz-transition: all 0.33s ease;
    transition: all 0.33s ease;
  }
  #emc-submit.ready-show {
    background: #FDC834;
    color: #232941;
    border: none;
    border-bottom: 2px solid #26af96;
    box-shadow: 0 1px 1px rgba(68, 68, 68, 0.2);
    -webkit-transform: rotateX(360deg);
    -moz-transform: rotateX(360deg);
    -ms-transform: rotateX(360deg);
    -o-transform: rotateX(360deg);
    transform: rotateX(360deg);
  }
  #emc-submit.ready-show:hover {
    color: #fbfbfb;
    background: #27b198;
    border-color: #232941;
  }
  #emc-submit.ready-show:active {
    top: 2px;
    border-bottom: none;
  }

  #emc-progress {
    height: 100%;
    border: 1px solid #232941;
    background: #FDC834;
  }

  #emc-progress_inner {
    width: 100%;
    height: 0;
    background: #232941;
    -webkit-transition: height 0.33s cubic-bezier(0.68, -0.55, 0.265, 1.55);
    -moz-transition: height 0.33s cubic-bezier(0.68, -0.55, 0.265, 1.55);
    transition: height 0.33s cubic-bezier(0.68, -0.55, 0.265, 1.55);
  }

  #emc-progress_ind {
    position: absolute;
    display: block;
    width: 100%;
    font-size: 0.7em;
    font-weight: bold;
    /*padding: 7px 5px 5px;*/
    top: 0;
    left: 0;
    text-align: center;
    color: #232941;
  }

  #emc-timer {
    position: absolute;
    display: block;
    width: 100%;
    font-size: 0.7em;
    font-weight: bold;
    /*padding: 7px 5px 5px;*/
    top: 0;
    right: 0;
    text-align: center;
    color: #232941;
  }

  footer {
    position: fixed;
    width: 40px;
    height: 100%;
    /*background: rgba(251, 251, 251, 0.85);*/
    background: #fff;
    bottom: 0;
    left: 0;
    padding: 25px 12px 10px;
  }

  .main-content {
      position: relative;
      padding-left: 20px;
  }

  .attrib {
    padding: 10px 0;
    text-align: center;
  }
  .attrib i {
    margin-right: 7px;
    margin-left: 7px;
  }

</style>

<!-- text animation -->
<style type="text/css">
  /* define the animation */
  @-webkit-keyframes marquee {
    0%   { -webkit-transform: translate(0, 0); }
    100% { -webkit-transform: translate(0, -200%); }
  } 
    @keyframes marquee {
    0%   { -webkit-transform: translate(0, 0); }
    100% { -webkit-transform: translate(0, -100%); }
  } 
  

  
  /* define your limiting container */
  .marquee {
    border: solid 1px;
    border-color: #d6d6d6;
    white-space: nowrap;
    overflow: hidden;
    box-sizing: border-box;
    height: 100px;
  }
  /* this is the tray moving around your container */
  .marquee span {
    display: inline-block;
    padding-top: 80px;
    font-size: 16px;
    
    text-indent: 0;
    animation: marquee 20s linear infinite; /* here you select the animation */
      -webkit-animation: marquee 50s linear infinite; /* here you select the animation */
  }
  /* pause the animation on mouse over */
  .marquee span:hover {
    /*animation-play-state: paused;*/
      /*-webkit-animation-play-state: paused;*/
  }

  /*progress bar*/
  .round-time-bar {
      margin: 1rem;
      overflow: hidden;
    }
    .round-time-bar div {
      height: 5px;
      animation: roundtime calc(var(--duration) * 5s) steps(var(--duration))
        forwards;
      transform-origin: left center;
      background: linear-gradient(to bottom, red, #900);
    }

    .round-time-bar[data-style="smooth"] div {
      animation: roundtime calc(var(--duration) * 5s) linear forwards;
    }

    .round-time-bar[data-style="fixed"] div {
      width: calc(var(--duration) * 5%);
    }

    .round-time-bar[data-color="blue"] div {
      background: linear-gradient(to bottom, #64b5f6, #1565c0);
    }

    @keyframes roundtime {
      to {
        /* More performant than `width` */
        transform: scaleX(0);
      }
    }

</style>
  <script src="<?php echo base_url('src/assets/js/notifications.js'); ?>"></script>

<!-- munculkan petunjuk jika nomor bagian diklik pertama kali -->
<script type="text/javascript">
  function timer2(){
    // 1 jam
  var counts  = 180;
  
     interval = setInterval(function(){
  var count = counts - 1
  var c_h =  Math.floor(count/3600 );
  var c_min =  Math.floor(count/60%60 );
    var c_sec =  Math.floor(count%60);
  
  document.getElementById('count2').innerHTML= `${ c_min} : ${c_sec}`;
  counts--
  
  if (count <= 0  )
  {
    clearInterval(interval);
  
    document.getElementById('count2').innerHTML='Waktu habis';
    // or..
    // $('#exampleModal').modal('show');
    // alert("You're out of time!");
    }
  }, 1000);
  }

  var cekbagian = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  function petunjuk(id_bagian) {
    if (id_bagian==1){
        $("#tabs-icons-text-"+id_bagian+"-tab").click();
        $(".nav-link").removeClass("active");
        $("#"+id_bagian).addClass("active");
    } else {
      if (cekbagian[id_bagian-1]==0){
        // buka bagian lalu munculin modal petunjuk 
        console.log("#exampleModal"+id_bagian);
        $("#tabs-icons-text-"+id_bagian+"-tab").click();
        $(".nav-link").removeClass("active");
        $("#"+id_bagian).addClass("active");
        $('#petunjukfullpage'+id_bagian).modal('show');
        
        // $("#buttonpetunjuk"+id_bagian).click();
        cekbagian[id_bagian-1]=1;
      } else {
        $("#tabs-icons-text-"+id_bagian+"-tab").click();
        $(".nav-link").removeClass("active");
        $("#"+id_bagian).addClass("active");
      }
    }
  }

  function start_memorizing() { 
    // sembunyikan petunjuk bagian 9 setelah 3 menit
        timer2();
    var x = document.getElementById("marquee");
    var button = document.getElementById("start_memorizing");
    var p = document.getElementById("textpetunjuk");
    var s = document.getElementById("textsoal");
    // if (x.style.visibility == "hidden") {
      x.style.visibility = "visible";
      button.style.visibility = "hidden";
      console.log(p.style.visibility);
      setTimeout(function() {
        p.style.visibility = "visible";
        s.style.visibility = "visible";
        console.log(s);
        x.style.visibility = "hidden";
      // }, 4000);
      }, 180000);
    // } 
    // else {
    //   x.style.visibility = "visible";
    //   console.log("visible");
    // }
  }

  
</script>

</head>

<body>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Search form -->
         <!--  <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
            <div class="form-group mb-0">
              <div class="input-group input-group-alternative input-group-merge">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-search"></i></span>
                </div>
                <input class="form-control" placeholder="Search" type="text">
              </div>
            </div>

            <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Tutup">
              <span aria-hidden="true">×</span>
            </button>
          </form> -->
          <!-- Navbar links -->
          
          <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
            <li class="nav-item dropdown">
              <!-- <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> -->
                <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" class="ml-3" src="<?php echo base_url('src/assets/img/profile/none.jpg'); ?>">
                  </span>
                  <div class="media-body  ml-3">
                    <span class="mb-0 text-sm  font-weight-bold" style="color: #fff"><?= $profile->first_name.' '.$profile->last_name ?></span>
                  </div>
                </div>
              <!-- </a> -->
              <!-- <div class="dropdown-menu  dropdown-menu-right ">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0">Welcome!</h6>
                </div>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-single-02"></i>
                  <span>My profile</span>
                </a>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-settings-gear-65"></i>
                  <span>Settings</span>
                </a>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-calendar-grid-58"></i>
                  <span>Activity</span>
                </a>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-support-16"></i>
                  <span>Support</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="<?= base_url('auth/logout'); ?>" class="dropdown-item">
                  <i class="ni ni-user-run"></i>
                  <span>Logout</span>
                </a>
              </div> -->
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Main Content -->
    <div>
        <div class="header bg-primary pb-6">
          <div class="container-fluid">
            <div class="header-body">
              <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0 pl-2"><?= $orders[0]['products'][0]['jenistes'][0]['nama']; ?></h6>

                  <nav aria-label="breadcrumb" class="d-none ml-md-4">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark d-none">
                      <li><?= lang('Global.order_code') ?> : <b><?= 'PIPE Psikotest'.sprintf('%04d', $orders[0]['id'])  ?></b></li>
                    </ol>
                  </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                                <h6 class="h2 text-white d-inline-block mb-0" id="count" style="padding-right: 20px">Timer</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
         <!-- modal petunjuk umum full page awal -->
         <div class="modal show pr-0 mr-0" id="briefingModal" tabindex="-1" role="dialog" aria-labelledby="briefingModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog-centered" role="document" style="width: 100%; height: 100%; margin: 0; padding: 0;">
            <div class="modal-content" style="height: auto; min-height: 100%; border-radius: 0;">
              <div class="modal-header" style="background-color: ">
                <h5 class="modal-title text-center" id="briefingModalLabel" style="color: #fff">PETUNJUK UMUM <?= $orders[0]['products'][0]['jenistes'][0]['nama']; ?></h5>
              </div>
              <div class="modal-body">
                <div style="position: absolute; left: 50%; top: 0%; transform: translate(-50%, -0%);">
                  <h2 class="text-dark pb-3">PETUNJUK UMUM <?= $orders[0]['products'][0]['jenistes'][0]['nama']; ?></h2>
                  <ol style="position: relative; left: -20px">
                    <?php
                    // tes minat
                    if ($orders[0]['products'][0]['jenistes'][0]['id_jenistes']==2){
                      ?>
                      <li class="pb-2">Kerjakanlah soal-soal yang tersedia setelah <b>membaca dan memahami petunjuk dan contoh soal yang diberikan pada setiap bagian soal.</b></li>
                      <li class="pb-2"><b>Tidak ada yang salah maupun benar dalam pernyataan yang Anda pilih. Pilihan yang paling tepat adalah yang menggambarkan tentang diri Anda. Anda diwajibkan menjawab semua pernyataan.</b></li>
                      <li class="pb-2">Peserta <b>dapat mengganti jawaban</b> yang dianggap lebih tepat apabila waktu tes masih tersedia.</li>
                      <li class="pb-2">Peserta disarankan untuk mengerjakan soal dengan <b>cepat dan teliti</b> karena pelaksanaan tes dibatasi waktu.</li>
                      <li class="pb-2">Apabila batas <b>waktu pengerjaan tes berakhir</b> maka soal akan terkunci dan peserta harus mengakhiri tes.</li>
                      <li class="pb-2">Anda akan membaca beberapa <b>pernyataan yang dibagi</b> menjadi tiga bagian :</li>
                      <ul style="position: relative; left: -10px">
                        <li class="pb-2">Bagian pertama merupakan pernyataan mengenai aktivitas atau kegiatan.</li>
                        <li class="pb-2">Bagian kedua merupakan pernyataan mengenai kemampuan.</li>
                        <li>Bagian ketiga pernyataan yang berkaitan dengan profesi atau pekerjaan.</li>
                        <ul>
                          <?php
                      // tes kepribadian
                    } else if ($orders[0]['products'][0]['jenistes'][0]['id_jenistes']==3){
                      ?>
                      <li class="pb-2">Kerjakanlah soal-soal yang tersedia setelah <b>membaca dan memahami petunjuk dan contoh soal yang diberikan pada setiap bagian soal.</b></li>
                      <li class="pb-2"><b>Tidak ada yang salah maupun benar dalam pernyataan yang Anda pilih. Pilihan yang paling tepat adalah yang menggambarkan tentang diri Anda. Anda diwajibkan menjawab semua pernyataan.</li></b>
                      <li class="pb-2">Peserta <b>dapat mengganti jawaban</b> yang dianggap lebih tepat apabila waktu tes masih tersedia.</li>
                      <li class="pb-2">Peserta disarankan untuk mengerjakan soal dengan <b>cepat dan teliti</b> karena pelaksanaan tes dibatasi waktu.</li>
                      <li>Apabila batas <b>waktu pengerjaan tes berakhir</b> maka soal akan terkunci dan peserta harus mengakhiri tes.</li>
                      <?php
                    }
                    ?>
                  </ol>
                  <div class="text-center pt-3">
                    <button onclick="return mulaites()" type="button" class="btn btn-primary" data-dismiss="modal">Saya telah memahami petunjuk</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- modal pencet selesai tapi belum jawab semu -->
        <div class="modal fade" id="exampleModalBelumSelesai" tabindex="-1" role="dialog" aria-labelledby="modalSelesai" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="modalSelesai">Selesai</h5>
              </div>
              <div class="modal-body">
                <span>Anda wajib menjawab semua soal sebelum mengakhiri tes. Periksa kembali jawaban Anda sebelum menyelesaikan tes ini.</span>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Baik</button>
              </div>
            </div>
          </div>
        </div>
        <!-- modal pencet selesai -->
        <div class="modal fade" id="exampleModalSelesaiMengerjakan" tabindex="-1" role="dialog" aria-labelledby="modalSelesai" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="modalSelesai">Selesai</h5>
              </div>
              <div class="modal-body">
                <span>Anda sudah mengerjakan semua soal dan ingin mengakhiri tes ? </span>
              </div>
              <div class="modal-footer">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button> -->
                <a type="button" id="selesai2" href="<?= base_url('test/done_test/'
                  .$orders[0]['id']
                  .'/'
                  .$orders[0]['products'][0]['id']
                  .'/'
                  .$orders[0]['products'][0]['jenistes'][0]['id']
                  ) ?>" class="btn btn-primary">Selesai</a>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              </div>
            </div>
          </div>
        </div>
        <!-- modal waktu habis -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Waktu Habis</h5>
              </div>
              <div class="modal-body">
                <span>Terima kasih telah mengerjakan tes. Tes Anda kami hentikan karena telah melampaui batas waktu. Jawaban Anda telah disimpan, silahkan untuk keluar dari tes dengan mengklik tombol dibawah ini</span>
              </div>
              <div class="modal-footer">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button> -->
                <a type="button" href="<?= base_url('test/done_test/'
                  .$orders[0]['id']
                  .'/'
                  .$orders[0]['products'][0]['id']
                  .'/'
                  .$orders[0]['products'][0]['jenistes'][0]['id']
                  ) ?>" class="btn btn-primary">Keluar</a>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal connection lost -->
        <!-- Modal full page-->
        <div class="modal mr-0 pr-0" id="connection_lost" tabindex="-1" role="dialog" aria-labelledby="connection_lostLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog-centered mr-0 pr-0 w-100" role="document" style="width: 100%; height: 100%; margin: 0; padding: 0;">
                <div class="modal-content" style="height: auto; min-height: 100%; border-radius: 0;">
                        <div class="modal-header">
                        </div>
                    <div class="modal-body">
                    <div style="position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);">
                    <h5 class="modal-title text-dark pb-2" id="connection_lostLabel" style="line-height: 30px">Koneksi internet Anda terputus, silahkan muat ulang halaman untuk melanjutkan mengerjakan tes saat koneksi Anda kembali stabil.
                            <div class="modal-footer text-center">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button> -->
                <!--<a type="button" href="<?= base_url('dashboard') ?>" class="btn btn-primary">Kembali ke dashboard</a>-->
              </div>
                            </div>
                        </div>
                    </div>
                <div>
            </div>
        </div>
    </div>
</div>
        <!-- Page content -->
        <div class="container-fluid mt--6  ml-2">
          <div class="row">
            <div class="col">
              <div class="card">
                <!-- awal percobaan -->
                <div class="nav-wrapper">
                    <ul class="nav nav-pills ml-3 mr-3 nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                      <?php
                       foreach ($bagiantes as $bagian){
                      ?>
                          <li class="nav-item">
                              <a id="<?= $bagian['urutan'] ?>" class="nav-link mb-sm-3 mb-md-0 <?php if($bagian['urutan']=='1') echo 'active' ?>" onclick="return petunjuk(this.id);" class="btn btn-primary">Bagian <?= $bagian['urutan'] ?></a>

                              <a class="nav-link mb-sm-3 mb-md-0 <?php if($bagian['urutan']=='1') echo 'active' ?>" id="tabs-icons-text-<?= $bagian['urutan'] ?>-tab" data-toggle="tab" href="#tabs-icons-text-<?= $bagian['urutan'] ?>" role="tab" aria-controls="tabs-icons-text-<?= $bagian['urutan'] ?>" aria-selected="true" hidden>Bagian <?= $bagian['urutan'] ?>
                                <?php if($bagian['id_jenistes']==2){
                                  if ($bagian['urutan']==1){
                                    echo "<br>Aktivitas";
                                  } else if ($bagian['urutan']==2){
                                    echo "<br>Kemampuan";
                                  } else if ($bagian['urutan']==3){
                                    echo "<br>Profesi";
                                  }
                                }?>
                              </a>
                          </li>
                      <?php
                      }
                      ?>
                        <!-- <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="ni ni-calendar-grid-58 mr-2"></i>Messages</a>
                        </li> -->
                    </ul>
                </div>
                <div class="card shadow">
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                          
                          <?php foreach ($bagiantes as $bagian){
                          ?>
                            <div class="tab-pane fade show <?php if($bagian['urutan']=='1') echo 'active' ?>" id="tabs-icons-text-<?= $bagian['urutan'] ?>" role="tabpanel" aria-labelledby="tabs-icons-text-<?= $bagian['urutan'] ?>-tab">
                              <div>
                                <!-- Button trigger modal -->
                                <button type="button" id="buttonpetunjuk<?= $bagian['urutan'] ?>" class="btn-primary mb-3 ml-0" style="position: relative; left: -9px; font-weight: 600" data-toggle="modal" data-target="#exampleModal<?= $bagian['urutan'] ?>">
                                  Lihat petunjuk
                                </button>
                                <!-- Modal petunjuk full page pertama kali -->
                                <!-- Modal full page-->
                                <div class="modal mr-0 pr-0" id="petunjukfullpage<?=$bagian['urutan'] ?>" tabindex="-1" role="dialog" aria-labelledby="petunjukfullpageLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                                    <div class="modal-dialog-centered mr-0 pr-0" role="document" style="width: 100%; height: 100%; margin: 0; padding: 0;">
                                        <div class="modal-content" style="height: auto; min-height: 100%; border-radius: 0;">
                                                <div class="modal-header">
                                                </div>
                                            <div class="modal-body">
                                            <div style="position: absolute; left: 30%; top: 50%; transform: translate(-30%, -50%);">
                                            <h5 class="modal-title text-dark pb-2" id="petunjukfullpageLabel">PETUNJUK PENGERJAAN DAN CONTOH SOAL BAGIAN <?=$bagian['urutan'] ?></h5>
                                            <div  <?php if ($bagian['urutan'] == '9') echo 'id="textpetunjuk" style="visibility: hidden;"' ?>>
                                                <?=$bagian['petunjuk'] ?>
                                                    <div class="modal-footer">
                                                    <button onclick="return agreement(<?=$bagian['urutan'] ?>)" type="button" class="btn btn-primary" data-dismiss="modal">Saya sudah memahami petunjuk dan contoh soal</button>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                <!-- Modal -->
                                <div data-backdrop="static" class="modal fade" id="exampleModal<?= $bagian['urutan'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document" >
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">PETUNJUK PENGERJAAN DAN CONTOH SOAL BAGIAN <?= $bagian['urutan'] ?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <div id="runningtext">
                                          <?php 
                                            if($bagian['urutan']==9){
                                              ?>
                                              <span>Anda diminta untuk menghafalkan kata-kata yang akan ditampilkan berikut ini</span>
                                              <a id="start_memorizing" onclick="return start_memorizing();" class="btn btn-sm btn-primary" style="color: #fff">Mulai</a>
                                              <h6 class="h2 d-inline-block mb-0" id="count2" style="padding-right: 20px">Timer</h6>

                                              <p class="marquee text-center" id="marquee" style="visibility: hidden;">
                                                      <span class="text-center">
                                                        <b>BUNGA</b> : <br>Soka-Larat-Flamboyan-Yasmin-Dahlia<br/>
                                                        <b>PERKAKAS</b> : <br>Wajan-Jarum-Kikir-Cangkul-Palu<br/>
                                                        <b>BURUNG</b> : <br>Itik-Elang-Walet-Tekukur-Nuri<br/>
                                                        <b>KESENIAN</b> : <br>Quintet-Arca-Opera-Gamelan-Ukiran<br/>
                                                        <b>BINATANG</b> : <br>Musang-Rusa-Beruang-Zebra-Harimau<br/>
                                                        <b>BUNGA</b> : <br>Soka-Larat-Flamboyan-Yasmin-Dahlia<br/>
                                                        <b>PERKAKAS</b> : <br>Wajan-Jarum-Kikir-Cangkul-Palu<br/>
                                                        <b>BURUNG</b> : <br>Itik-Elang-Walet-Tekukur-Nuri<br/>
                                                        <b>KESENIAN</b> : <br>Quintet-Arca-Opera-Gamelan-Ukiran<br/>
                                                        <b>BINATANG</b> : <br>Musang-Rusa-Beruang-Zebra-Harimau<br/>
<!-- 
                                                        soka<br/>
                                                        larat<br/>
                                                        flamboyan<br/>
                                                        yasmin<br/>
                                                        dahlia<br/>
                                                        wajan<br/>
                                                        jarum<br/>
                                                        kikir<br/>
                                                        cangkul<br/>
                                                        palu<br/>
                                                        itik<br/>
                                                        elang<br/>
                                                        walet<br/>
                                                        tekukur<br/>
                                                        nuri<br/>
                                                        quintet<br/>
                                                        arca<br/>
                                                        opera<br/>
                                                        gamelan<br/>
                                                        ukiran<br/>
                                                        musang<br/>
                                                        rusa<br/>
                                                        beruang<br/>
                                                        zebra<br/>
                                                        harimau<br/> -->
                                                      </span>
                                              </p>
                                              <div id='progressbar1'></div>
                                              <?php
                                            }
                                          ?>
                                        </div>
                                        <div  <?php if($bagian['urutan']=='9') echo 'id="textpetunjuk" style="visibility: hidden;"' ?>>
                                        <?= $bagian['petunjuk'] ?>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                      </div>
                                        </div>
                                      </div>
                                      <div>
                                      </div>
                                      
                                    </div>
                                  </div>
                                </div>
                              </div>
                                <div class="row" <?php if($bagian['urutan']=='9') echo 'id="textsoal" style="visibility: hidden;"' ?>>
                                  <!-- awal soal -->
                                  <?php  
                                  foreach($bagian['soal'] as $key => $soal) {
                                  $nomorsoal = $key+1;
                                  $pj = "[";
                                    foreach($soal['pilihanjawaban'] as $pilihanjawaban) { 
                                        $pj = $pj."\"".$pilihanjawaban['jawaban']."\", "; }
                                        $pj = rtrim($pj, ", ");
                                        $pj = $pj."]"; 
                                    ?>
                                    <section data-quiz-item >
                                      <div class="question"><?= $nomorsoal.'. '.$soal['soal']; ?></div>
                                      <?php
                                        if($soal['gambar']){
                                          ?>
                                          <div class="col-sm-12">
                                            <img src="<?php echo base_url($soal['gambar']); ?>" class="rounded img-fluid" alt=".." style="height: 150px;">
                                          </div>
                                          <?php
                                        }
                                      ?>
                                      <!-- pilihan -->
                                      <div class="choices" data-choices='<?= $pj; ?>' data-type='<?= $bagian["tipe_jawaban"]; ?>' data-bagian='<?= $bagian["id"]; ?>'>
                                        <?php 
                                        foreach($soal['pilihanjawaban'] as $pilihanjawaban){
                                          $checked = "";
                                          if (!empty($soal['jawabanmember'])){
                                              if ($bagian['tipe_jawaban']=='checkbox'){
                                                $checkbox = explode("_", $soal['jawabanmember'][0]['urutan_jawaban']);
                                                foreach($checkbox as $cb){
                                                  if ($pilihanjawaban['urutan']==$cb)
                                                  $checked = "checked";
                                                }
                                              } else {
                                                if ($pilihanjawaban['urutan']==$soal['jawabanmember'][0]['urutan_jawaban'])
                                                $checked = "checked";
                                              }
                                          }
                                        
                                        ?>
                                          <input type="<?= $bagian['tipe_jawaban'] ?>" name="<?= $bagian['urutan'].'_'.$soal['urutan'].'_'.$soal['sub_bagian']; ?>" id="<?= $bagian['urutan'].'_'.$soal['urutan'].'_'.$pilihanjawaban['urutan'].'_'.$soal['sub_bagian']; ?>"  
                                          <?php
                                            echo $checked;
                                          ?>
                                          >
                                          <label for="<?= $bagian['urutan'].'_'.$soal['urutan'].'_'.$pilihanjawaban['urutan'].'_'.$soal['sub_bagian']; ?>">
                                          <?= $pilihanjawaban['jawaban'] ?>
                                          <?php
                                            if($pilihanjawaban['gambar']){
                                              ?>
                                                <img src="<?php echo base_url($pilihanjawaban['gambar']); ?>" class="rounded img-fluid" alt=".." style="height: 100px;">
                                              <?php
                                            }
                                          ?>
                                          </label>

                                        <?php
                                        }
                                        if ($bagian['tipe_jawaban']=="text"){
                                        ?>
                                          <input type="<?= $bagian['tipe_jawaban'] ?>" value="<?php if(!empty($soal['jawabanmember'][0]['jawaban_isian'])) {echo $soal['jawabanmember'][0]['jawaban_isian']; } ?>" name="<?= $bagian['urutan'].'_'.$soal['urutan']; ?>" id="<?= $bagian['urutan'].'_'.$soal['urutan'].'_0'; ?>" 
                                          >
                                        <?php
                                        }
                                        ?>
                                      </div>
                                    </section>
                                    <?php
                                  }
                                    
                                  ?>
                                <div class="container">
                                      <div class="row">
                                          <div class="col-md-12 text-right">
                                          <?php
                                          if ($orders[0]['products'][0]['jenistes'][0]['id_jenistes']==2){
                                            if ($bagian['urutan']==3){
                                              ?>
                                                <button type="button" onclick="return selesai()" id="ModalSelesai" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#exampleModalSelesai">
                                                  Akhiri Tes
                                                </button>
                                                <?php
                                              } else {
                                                ?>
                                              <a type="button" onclick="return maumulaibagian(<?=$bagian['urutan']+1 ?>);"  class="btn btn-primary">Lanjut ke bagian <?=$bagian['urutan']+1 ?></a>
                                                <?php
                                            }  
                                          }
                                          else if ($orders[0]['products'][0]['jenistes'][0]['id_jenistes']==3){
                                            ?>
                                                <button type="button" onclick="return selesai()" id="ModalSelesai" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#exampleModalSelesai">
                                                  Akhiri Tes
                                                </button>
                                                <?php
                                          }

                                            ?>
                                          </div>
                                      </div>
                                  </div>
                                <!-- akhir soal -->
                                </div>
                            </div>
                              
                          <?php
                          }
                          ?>
                        </div>
                    </div>
                </div>


                <div id="emc-score"></div>
                  <!-- <div class="wrap">
                    <div class="submit">
                    <button id="emc-submit">Submit Answers</button>
                    </div>
                  </div> -->
                  <footer>
                  <div id="emc-progress"></div>
                  </footer>
                  <div class="attrib">
                    <!-- <p>Take the full quiz <i class="fa fa-long-arrow-right"></i> <a href="http://davidshariff.com/quiz/" target="_blank">http://davidshariff.com/quiz/</a></p> -->
                  </div>
                <!-- akhir percobaan -->
                <!-- Card footer -->
                <div class="card-footer py-4">
                  <div class="row align-items-center justify-content-md-between">
                    <div class="col-md-5">
                      <div class="copyright">
                        &copy; <?php echo date("Y"); ?> <a href="" target="_blank">PIPE Psikotest</a>.
                      </div>
                    </div>
                    <div class="col-md-7 text-right">
                      <p class="mb-0">Jika terdapat kendala, silahkan hubungi Admin</p>
                      <a href="https://api.whatsapp.com/send?phone=62895-0514-7095" target="_blank"><p class="mb-0"><i class="fab fa-whatsapp mr-2"></i>0895-0514-7095</p></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

    </div>
    
      <!-- Footer -->

    </div>
  </div>
  <!-- Vue js -->
  <script src="https://vuejs.org/js/vue.min.js"></script>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="<?php echo base_url('src/assets/vendor/jquery/dist/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/js-cookie/js.cookie.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js'); ?>"></script>
  <!-- Optional JS -->
  <script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.min.js'); ?>"></script>
  <script src="<?php echo base_url('src/assets/vendor/chart.js/dist/Chart.extension.js'); ?>"></script>
  <!-- Argon JS -->
  <script src="<?php echo base_url('src/assets/js/argon.js?v=1.2.0'); ?>"></script>
  <!-- Datatable -->
  <script src="<?php echo base_url('src/node_modules/mdbootstrap/js/addons/datatables2.min.js'); ?>" type="text/javascript"></script>
  

<!-- script timer -->
<script type="text/javascript">
var count_checkpoint = 0;
var progress_jawab = 0;
$('#selesai1').click(function(){
  $(window).unbind('beforeunload');

});
$('#selesai2').click(function(){
  $(window).unbind('beforeunload');

});
  function timer(){
    
  <?php
  if ($timer_checkpoint!=0){
    ?>
    var counts  = <?= $timer_checkpoint ?>;
    <?php
  } 
  else if($orders[0]['products'][0]['jenistes'][0]['id_jenistes']=='1'){
    ?>
    var counts  = 9000;
    <?php
  } else {
    ?>

  var counts  = 3600;
  <?php
  }
   ?>
    
     interval = setInterval(function(){
      var id_memberorderproductjenistes = <?php echo $orders[0]['products'][0]['jenistes'][0]['id'] ?>;
      var count = counts - 1
      count_checkpoint = count;
      var c_h =  Math.floor(count/3600 );
      var c_min =  Math.floor(count/60%60 );
      var c_sec =  Math.floor(count%60);
  
  document.getElementById('count').innerHTML= `${ c_h} : ${ c_min} : ${c_sec}`;
  counts--
  
  // waktu sampe habis
  if (count == 0)
      {
        clearInterval(interval);
        counts = 0;
        $.ajax({
          url:'<?=base_url()?>/test/save_checkpoint',
          method: 'post',
          data: {
            bagian_tes: 0, 
            timer_checkpoint: -1 , 
            id_memberorderproductjenistes: id_memberorderproductjenistes},
          dataType: 'json',
          success: function(response){
            console.log('checkpoint selesai disimpan');
          }, error: function(errorThrown){
                    console.log('There is an error, check internet connection!');
              // if (!navigator.onLine){
                $('#connection_lost').modal('show');
                clearInterval(interval);
              // }
          } 
        });
        document.getElementById('count').innerHTML='Waktu habis';
        $('#exampleModal').modal({backdrop: 'static', keyboard: false})
        // klik lanjut ke bagian selanjutnya
        } else if (count <= -1){
          clearInterval(interval);
        counts = 0;
          $.ajax({
            url:'<?=base_url()?>/test/save_checkpoint',
            method: 'post',
            data: {
              bagian_tes: 0, 
              timer_checkpoint: -1 , 
              id_memberorderproductjenistes: id_memberorderproductjenistes},
            dataType: 'json',
            success: function(response){
              console.log('checkpoint selesai disimpan');
            }, error: function(errorThrown){
                      console.log('There is an error, check internet connection!');
              // if (!navigator.onLine){
                $('#connection_lost').modal('show');
                clearInterval(interval);
              // }
            } 
          });
          document.getElementById('count').innerHTML='Selesai';
        $('#exampleModal').modal({backdrop: 'static', keyboard: false})
        } else {
          // simpan checkpoint real time
          $.ajax({
            url:'<?=base_url()?>/test/save_checkpoint',
            method: 'post',
            data: {
              bagian_tes: 0, 
              timer_checkpoint: count, 
              id_memberorderproductjenistes: id_memberorderproductjenistes},
            dataType: 'json',
            success: function(response){
              console.log('checkpoint realtime disimpan  timer '+count);
            }, error: function(errorThrown){
                      console.log('There is an error, check internet connection!');
              // if (!navigator.onLine){
                $('#connection_lost').modal('show');
                clearInterval(interval);
              // }
            } 
          });
        }
      }, 1000);
  }
</script>
<script type="text/javascript">

  (function($) {

  $.fn.emc = function(options) {
    
    var defaults = {
      key: [],
      scoring: "normal",
      progress: true
    },
    settings = $.extend(defaults,options),

    $quizItems = $('[data-quiz-item]'),
    // pilihan jawaban 
    $choices = $('[data-choices]'),
    itemCount = $quizItems.length,
    bagian_tes = '1', 
    chosen = [],
    $option = null,
    $label = null;

    // console.log($types);
    // console.log();
    
   emcInit();
  //  timer();
    
   if (settings.progress) {
      var $bar = $('#emc-progress'),
          $inner = $('<div id="emc-progress_inner"></div>'),
          $perc = $('<span id="emc-progress_ind">0/'+itemCount+'</span>');
      $bar.append($inner).prepend($perc);
    }
    
    function emcInit() {


      $quizItems.each( function(index,value) {

      var $this = $(this),
          $choiceEl = $this.find('.choices'),
          choices = $choiceEl.data('choices');
          type = $choiceEl.data('type');
          bagian = $choiceEl.data('bagian');
          // type = $choiceEl.data('type');
          // console.log(Object.keys(bagian.key));
          // console.log(bagian);
          // radio atau checkbox
          // console.log('pilihannya'+type);
        // for (var i = 0; i < choices.length; i++) {
        //   $option = $('<input name="'+index+'" id="'+index+'_'+i+'" type="radio">');
        //   // $option = $('<input name="'+index+'" id="'+index+'_'+i+'" type="'+type+'">');
        //   $label = $('<label for="'+index+'_'+i+'">'+choices[i]+'</label>');
        //   $choiceEl.append($option).append($label);
          
        //  // input name 2 clicked
        //   // $option.on( 'change', function() {
        //   // //   // get id and name
        //   //   return getChosen();
        //   // }); 
        // }
      });
    }
    
    $("input:radio").on('change', function(e){

        $jawaban = e.target.id;
        var jawaban_isian = e.target.value;
        var code = $jawaban.split("_");

        var id_memberorderproductjenistes = <?php echo $orders[0]['products'][0]['jenistes'][0]['id'] ?>;
        console.log('id membernya ; ');
        // todo
        
        var bagian_tes=code[0];
        var urutan_soal=code[1];
        var urutan_jawaban=code[2];
        var sub_bagian=code[3];
        console.log(urutan_soal+"ada"+urutan_jawaban);
        console.log("sub bagian");
        $.ajax({
         url:'<?=base_url()?>/test/save_jawaban',
         method: 'post',
         data: {
          urutan_soal: urutan_soal, 
          urutan_jawaban: urutan_jawaban,
          jawaban_isian: jawaban_isian,
          counts: count_checkpoint,
          bagian_tes: bagian_tes, 
          sub_bagian: sub_bagian, 
          id_memberorderproductjenistes: id_memberorderproductjenistes},
         dataType: 'json',
         success: function(response){ 
              const myNotification = window.createNotification({
                  });
                if(response==1){
                  myNotification({ 
                    title: 'Disimpan',
                    message: 'Bagian '+bagian_tes+' nomor '+urutan_soal+' berhasil disimpan',
                    closeOnClick: true,
                    displayTutupButton: false,
                    positionClass: 'nfc-top-left',
                    onclick: false,
                    showDuration: 2000,
                    theme: 'success'
                    });
                } else  {
                  myNotification({ 
                    title: 'Data tidak berhasil disimpan',
                    message: 'Bagian '+bagian_tes+' nomor '+urutan_soal+' tidak berhasil disimpan',
                    closeOnClick: true,
                    displayTutupButton: false,
                    positionClass: 'nfc-top-left',
                    onclick: false,
                    showDuration: 2000,
                    theme: 'error'
                    });
                }
            console.log(response);
         }, error: function(errorThrown){
                  // alert(errorThrown);
                  alert("There is an error, check internet connection!");
        } 
       });
            return getChosen();


      // alert("The text has been changed.");
    }); 

    $("input:text").on('change', function(e){

        $jawaban = e.target.id;
        var jawaban_isian = e.target.value;
        var code = $jawaban.split("_");
        var id_memberorderproductjenistes = <?php echo $orders[0]['products'][0]['jenistes'][0]['id'] ?>;
        // todo
        
        var bagian_tes=code[0];
        var urutan_soal=code[1];
        var urutan_jawaban=code[2];
        // console.log(urutan_soal+"ada"+urutan_jawaban);
        $.ajax({
         url:'<?=base_url()?>/test/save_jawaban',
         method: 'post',
         data: {
          urutan_soal: urutan_soal, 
          urutan_jawaban: urutan_jawaban,
          jawaban_isian: jawaban_isian,
          counts: count_checkpoint,
          bagian_tes: bagian_tes, 
          id_memberorderproductjenistes: id_memberorderproductjenistes},
         dataType: 'json',
         success: function(response){
            const myNotification = window.createNotification({
                });
                if(response==1){
                  myNotification({ 
                    title: 'Disimpan',
                    message: 'Jawaban Anda "'+jawaban_isian+'" bagian '+bagian_tes+' nomor '+urutan_soal+' berhasil disimpan',
                    closeOnClick: true,
                    displayTutupButton: false,
                    positionClass: 'nfc-top-left',
                    onclick: false,
                    showDuration: 2000,
                    theme: 'success'
                    });
                } else  {
                  myNotification({ 
                    title: 'Data tidak berhasil disimpan',
                    message: 'Jawaban Anda "'+jawaban_isian+'" bagian '+bagian_tes+' nomor '+urutan_soal+' tidak berhasil disimpan',
                    closeOnClick: true,
                    displayTutupButton: false,
                    positionClass: 'nfc-top-left',
                    onclick: false,
                    showDuration: 2000,
                    theme: 'error'
                    });
                }
            console.log(response);
         }, error: function(errorThrown){
                  // alert(errorThrown);
                  alert("There is an error, check internet connection!");
        } 
       });
            return getChosen();


      // alert("The text has been changed.");
    }); 

    $("input:checkbox").on('change', function(e){

        $jawaban = e.target.id;
        $name = e.target.name;
        

        var code = $jawaban.split("_");

        var jawaban_isian = e.target.value;
        var id_memberorderproductjenistes = <?php echo $orders[0]['products'][0]['jenistes'][0]['id'] ?>;
        
        var bagian_tes=code[0];
        var urutan_soal=code[1];
        var urutan_jawaban="";
        // console.log("bagian tes"+bagian_tes+" urutan_soal"+urutan_soal+" urutan_jawaban"+urutan_jawaban);
        $("input:checkbox[name="+code[0]+"_"+code[1]+"_]:checked").each(function () {
        // $("input:checkbox:checked").each(function () {
            // alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
            checked_checkbox = $(this).attr("id").split("_");
            // console.log(code.);
            urutan_jawaban = urutan_jawaban+checked_checkbox[2]+"_";
        });
       $.ajax({
         url:'<?=base_url()?>/test/save_jawaban',
         method: 'post',
         data: {
          urutan_soal: urutan_soal, 
          urutan_jawaban: urutan_jawaban,
          jawaban_isian: jawaban_isian,
          counts: count_checkpoint,
          bagian_tes: bagian_tes, 
          // sub_bagian: sub_bagian, 
          id_memberorderproductjenistes: id_memberorderproductjenistes},
         dataType: 'json',
         success: function(response){
            const myNotification = window.createNotification({
                });
                if(response==1){
                  myNotification({ 
                    title: 'Disimpan',
                    message: 'Bagian '+bagian_tes+' nomor '+urutan_soal+' berhasil disimpan',
                    closeOnClick: true,
                    displayTutupButton: false,
                    positionClass: 'nfc-top-left',
                    onclick: false,
                    showDuration: 2000,
                    theme: 'success'
                    });
                } else  {
                  myNotification({ 
                    title: 'Data tidak berhasil disimpan',
                    message: 'Bagian '+bagian_tes+' nomor '+urutan_soal+' tidak berhasil disimpan',
                    closeOnClick: true,
                    displayTutupButton: false,
                    positionClass: 'nfc-top-left',
                    onclick: false,
                    showDuration: 2000,
                    theme: 'error'
                    });
                }
            console.log(response);
         }, error: function(errorThrown){
                  // alert(errorThrown);
                  alert("There is an error, check internet connection!");
        } 
       });
            return getChosen();
    }); 

    // $('input[name="2"]').on('change', function() { 
    //         alert('name 2 clicked');
    //       });
    
    function getChosen() {
      chosen = [];
      $choices.each( function() {
        var $inputs = $(this).find('input[type="radio"]');
        $inputs.each( function(index,value) {
          if($(this).is(':checked')) {
            console.log(value.name);
            chosen.push(index + 1);
            // simpan ke database
            // alert('terima kasih sudah menjawab'+chosen + 'soal '+index);
          } 
        });

      });
      getProgress();
    }
    
    function getProgress() {
      console.log(chosen);
      var prog = (chosen.length / itemCount) * 100 + "%",
          $submit = $('#emc-submit');
      progress_jawab = prog;
      if (settings.progress) {
        minuspetunjuk = chosen.length;   
        if(<?php echo $bagiantes[0]['id_jenistes'] ?> == 1){
          minuspetunjuk = chosen.length-10;   
        }
        $perc.text(minuspetunjuk+'/'+itemCount);  
        $inner.css({height: prog});
      }
      // if (chosen.length === itemCount) {
        // $submit.addClass('ready-show');
        // $submit.click( function(){

          // return scoreNormal();
        // });
      // }
    }
    
    function scoreNormal() {
      var wrong = [],
          score = null,
          $scoreEl = $('#emc-score');
          // console.log(settings.key);
          // console.log(Object.keys(settings.key));
          // console.log('terjawab'+chosen[0]);
      for (var i = 0; i < itemCount; i++) {
        if (chosen[i]) {
        } 
        if (chosen[i] != settings.key[i]) {
          wrong.push(i);
        }
      }
      // console.log(settings);
      $quizItems.each( function(index) {
        var $this = $(this);
        if ($.inArray(index, wrong) !== -1 ) {
          $this.removeClass('item-correct').addClass('item-incorrect');
        } else {
          $this.removeClass('item-incorrect').addClass('item-correct');
        }
      });
      
      score = ((itemCount - wrong.length) / itemCount).toFixed(2) * 100 + "%";
      $scoreEl.text("You scored a "+score).addClass('new-score');
      // $('html,body').animate({scrollTop: 0}, 50);
    }
 
  }
}(jQuery));

// jawaban
$(document).emc({
  key: ["5","1","2","2","2","2","1","1"]
});

</script>

<script type="text/javascript">
  $(document).ready(function () {
    $('#briefingModal').modal('toggle');
  });
  function mulaites() { 
    $('#briefingModal').modal('hide');
    // console.log('help');
    
    // $("#tabs-icons-text-1-tab").click();
    // $(".nav-link").removeClass("active");
    // $("#1").addClass("active");    
    $("#buttonpetunjuk1").click();
  }

  function mulaites() { 
    $('#briefingModal').modal('hide');
    // $('#petunjukfullpage1').modal('hide');
    $('#petunjukfullpage1').modal('show');
  }

  function agreement($nobagian) { 
    $('#petunjukfullpage'+$nobagian).modal('hide');
    if ($nobagian==1){
    timer();
    }
  }

  function maumulaibagian($nobagian) { 
    console.log("buka petunjuk full page bagian "+$nobagian);
    window.scrollTo(0, 0);
    $("#tabs-icons-text-"+$nobagian+"-tab").click();
    $(".nav-link").removeClass("active");
    $("#"+$nobagian).click();
    // $('#petunjukfullpage'+$nobagian).modal('show');
  }

  function selesai(){
    if (progress_jawab=='100%'){
        $('#exampleModalSelesaiMengerjakan').modal('show');  
    } else {
        $('#exampleModalBelumSelesai').modal('show');  
    }
  }
</script>


</body>

</html> 