<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class TempSeeder extends Seeder
{
	public function run()
	{
       

        //PRODUCT
        $data = [
            [
                'nama'          =>  'Layanan Tes Lengkap',
                'harga'          =>  '1100000',
                'short_desc'         =>  'Paket Lengkap terdiri dari 3 jenis tes sekaligus diantaranya tes kecerdasan, tes minat bakat dan tes kepribadian.',
                'desc'         =>  'Tes kecerdasan ini akan membantu Anda untuk mengetahui tingkat kecerdasan (IQ) yang Anda miliki. Tes ini terdiri dari 9 sub tes yang harus Anda kerjakan dengan secepat-cepatnya dan seteliti mungkin sehingga Anda dapat mengerjakan secara cepat dan tepat. Waktu yang dibutuhkan untuk menyelesaikan soal ini kurang lebih 2 jam 30 menit.',
            ],
            [
                'nama'          =>  'Layanan Tes Kecerdasan',
                'harga'          =>  '500000',
                'short_desc'         =>  'Ketahui tingkat kecerdasan (IQ)',
                'desc'         =>  'Tes kecerdasan ini akan membantu Anda untuk mengetahui tingkat kecerdasan (IQ) yang Anda miliki. Tes ini terdiri dari 9 sub tes yang harus Anda kerjakan dengan secepat-cepatnya dan seteliti mungkin sehingga Anda dapat mengerjakan secara cepat dan tepat. Waktu yang dibutuhkan untuk menyelesaikan soal ini kurang lebih 2 jam 30 menit.',
            ],
            [
                'nama'          =>  'Layanan Tes Minat Bakat',
                'harga'          =>  '400000',
                'short_desc'         =>  'Memilih jurusan maupun karir yang tepat',
                'desc'         =>  'Tes ini akan membantu untuk mengungkap minat karier yang akan bermanfaat bagi Anda dalam proses pemilihan jurusan maupun karier (pekerjaan) di masa mendatang. Anda akan dihadapkan pada beberapa pernyataan yang harus Anda pilih. Pernyataan tersebut tidak ada yang benar maupun salah. Pilihan yang paling tepat adalah pilihan sesuai dengan kondisi yang ada pada diri Anda saat ini. Anda diberikan waktu 1 jam untuk dapat menyelesaikan keseluruhan tes minat ini.',
            ],
            [
                'nama'          =>  'Layanan Tes Kepribadian',
                'harga'          =>  '300000',
                'short_desc'         =>  'Ketahui kepribadian Anda',
                'desc'         =>  'Tes kepribadian akan membantu untuk mengungkap kepribadian Anda. Anda akan diharuskan untuk memilih salah satu dari dua pernyataan yang disajikan. Waktu yang dibutuhkan untuk menyelesaikan tes kepribadian ini maksimal 1 jam.',
            ],
        ];
        $this->db->table('product')->insertBatch($data);

        //JENIS TES
        $data = [
            [
                'nama'          =>  'TES KECERDASAN',
                'desc'         =>  'Tes kecerdasan ini akan membantu Anda untuk mengetahui tingkat kecerdasan (IQ) yang Anda miliki. Tes ini terdiri dari 9 sub tes yang harus Anda kerjakan dengan secepat-cepatnya dan seteliti mungkin sehingga Anda dapat mengerjakan secara cepat dan tepat. Waktu yang dibutuhkan untuk menyelesaikan soal ini kurang lebih 2 jam 30 menit.',
            ],
            [
                'nama'          =>  'TES MINAT BAKAT',
                'desc'         =>  'Tes ini akan membantu untuk mengungkap minat karier yang akan bermanfaat bagi Anda dalam proses pemilihan jurusan maupun karier (pekerjaan) di masa mendatang. Anda akan dihadapkan pada beberapa pernyataan yang harus Anda pilih. Pernyataan tersebut tidak ada yang benar maupun salah. Pilihan yang paling tepat adalah pilihan sesuai dengan kondisi yang ada pada diri Anda saat ini. Anda diberikan waktu 1 jam untuk dapat menyelesaikan keseluruhan tes minat ini.',
            ],
            [
                'nama'          =>  'TES KEPRIBADIAN',
                'desc'         =>  'Tes kepribadian akan membantu untuk mengungkap kepribadian Anda. Anda akan diharuskan untuk memilih salah satu dari dua pernyataan yang disajikan. Waktu yang dibutuhkan untuk menyelesaikan tes kepribadian ini maksimal 1 jam.',
            ],
        ];
        $this->db->table('jenistes')->insertBatch($data);

       

        
	}
}
