<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class IonAuthSeeder extends Seeder
{
	public function run()
	{
		$groups = [
            [
                'id'          => 1,
                'name'        => 'admin',
                'description' => 'Administrator',
            ],
            [
                'id'          => 2,
                'name'        => 'members',
                'description' => 'General User',
            ],
            [
                'id'          => 3,
                'name'        => 'psychologists',
                'description' => 'Psychologists',
            ],
        ];
        $this->db->table('groups')->insertBatch($groups);

        $users = [
            [
                'ip_address'              => '127.0.0.1',
                'username'                => 'administrator',
                'password'                => '$2y$08$200Z6ZZbp3RAEXoaWcMA6uJOFicwNZaqk4oDhqTUiFXFe63MG.Daa',
                'email'                   => 'admin@admin.com',
                'activation_code'         => '',
                'forgotten_password_code' => null,
                'created_on'              => '1268889823',
                'last_login'              => '1268889823',
                'active'                  => '1',
                'first_name'              => 'Admin',
                'last_name'               => 'istrator',
                'company'                 => 'ADMIN',
                'phone'                   => '',
                'birthplace'              => '',
                'birthdate'               => '',
                'address'                 => '',
                'description'             => '',

            ],
            [
                'ip_address'              => '127.0.0.1',
                'username'                => 'member',
                'password'                => '$2y$08$200Z6ZZbp3RAEXoaWcMA6uJOFicwNZaqk4oDhqTUiFXFe63MG.Daa',
                'email'                   => 'member@member.com',
                'activation_code'         => '',
                'forgotten_password_code' => null,
                'created_on'              => '1268889823',
                'last_login'              => '1268889823',
                'active'                  => '1',
                'first_name'              => 'Member',
                'last_name'               => 'EduBridge',
                'company'                 => 'UGM',
                'phone'                   => '00505147095',
                'birthplace'              => '',
                'birthdate'               => '',
                'address'                 => 'Jl Biru Tua nomor 4, Kebumen',
                'description'             => '',
            ],
            [
                'ip_address'              => '127.0.0.1',
                'username'                => 'psikolog',
                'password'                => '$2y$08$200Z6ZZbp3RAEXoaWcMA6uJOFicwNZaqk4oDhqTUiFXFe63MG.Daa',
                'email'                   => 'psikolog@psikolog.com',
                'activation_code'         => '',
                'forgotten_password_code' => null,
                'created_on'              => '1268889823',
                'last_login'              => '1268889823',
                'active'                  => '1',
                'first_name'              => 'Edubridge',
                'last_name'               => '.Psi',
                'company'                 => 'PSI',
                'phone'                   => '00554786786',
                'birthplace'              => '',
                'birthdate'               => '',
                'address'                 => '',
                'description'             => '',
            ],
        ];
        $this->db->table('users')->insertBatch($users);

        $usersGroups = [
            [
                'user_id'  => '1',
                'group_id' => '1',
            ],
            [
                'user_id'  => '2',
                'group_id' => '2',
            ],
            [
                'user_id'  => '3',
                'group_id' => '3',
            ],
        ];
        $this->db->table('users_groups')->insertBatch($usersGroups);
	}
}
