<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MemberOrderProductJenistes extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'INT',
	           'constraint'     => 11,
	           'unsigned'       => TRUE,
	           'auto_increment' => TRUE
	        ],
	        'id_memberorderproduct'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'id_jenistes'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'start-test'       => [
	            'type'           => 'DATETIME',
	        ],
	        'done-test'       => [
	            'type'           => 'DATETIME',
	        ],
	        'start-correction'       => [
	            'type'           => 'DATETIME',
	        ],
	        'corrected'       => [
	            'type'           => 'DATETIME',
	        ],
	        'summary'       => [
	            'type'           => 'LONGTEXT',
	            'null'           => true
	        ],
	        'chart'       => [
	            'type'           => 'LONGTEXT',
	            'null'           => true
	        ],
	        'id_psikolog'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'status'       => [
	            'type'           => 'ENUM',
	            'constraint'     => array('start-test', 'done-test', 'start-correction', 'corrected'),
	            'null'           => true
	        ],
	        'deleted'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '0',
	        ],
	        'updated_at'     => [
	             'type'           => 'TIMESTAMP',
	             
	        ],
	  ]);
	  $this->forge->addKey('id', TRUE);
	  $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('memberorderproductjenistes');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
