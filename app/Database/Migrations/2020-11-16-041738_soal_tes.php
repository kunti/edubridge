<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SoalTes extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'INT',
	           'constraint'     => 11,
	           'unsigned'       => TRUE,
	           'auto_increment' => TRUE
	        ],
	        'id_jenistes'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'urutan_bagian_tes'       => [
	            'type'           => 'SMALLINT',
	            'constraint'     => '3',
	        ],
	        'sub_bagian'       => [
	            'type'           => 'SMALLINT',
	            'constraint'     => '3',
	            'null'           => true
	        ],
	        'nama_sub_bagian'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '2',
	            'null'           => true
	        ],
	        'urutan'       => [
	            'type'           => 'SMALLINT',
	            'constraint'     => '3',
	        ],
	        'soal'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '255',
	             'null'           => true
	        ],
	        'gambar'     => [
	             'type'           => 'TEXT',
	             'null'           => true
	        ],
	        'deleted'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '0',
	        ],
	        'updated_at'     => [
	             'type'           => 'TIMESTAMP',

	        ],
	  ]);
	  $this->forge->addKey('id', TRUE);
	  $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('soaltes');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
