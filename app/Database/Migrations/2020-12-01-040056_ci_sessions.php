<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CiSessions extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'VARCHAR',
	           'constraint'     => '128',
	           'null'           => false
	        ],
	        'ip_address'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '45',
	           	'null'           => false
	        ],
	        'timestamp'     => [
	            'type'           => 'INT',
	            'constraint'     => '10',
	           	'null'           => false
	        ],
	        'data'     => [
	            'type'           => 'blob',
	           	'null'           => false
	        ],
	  ]);
	  $this->forge->addKey('id', TRUE);
	  // $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('ci_sessions');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
