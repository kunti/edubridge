<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class JenisTes extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'INT',
	           'constraint'     => 11,
	           'unsigned'       => TRUE,
	           'auto_increment' => TRUE
	        ],
	        'nama'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '255',
	        ],
	        'desc'     => [
	             'type'           => 'TEXT',
	             'null'           => true
	        ],
	        'gambar'     => [
	             'type'           => 'TEXT',
	             'null'           => true
	        ],
	        'used'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '1',
	        ],
	        'updated_at'     => [
	             'type'           => 'TIMESTAMP',

	        ],
	  ]);
	  $this->forge->addKey('id', TRUE);
	  $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('jenistes');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
