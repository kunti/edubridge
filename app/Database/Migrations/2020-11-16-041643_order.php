<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;


class Order extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'INT',
	           'constraint'     => 11,
	           'unsigned'       => TRUE,
	           'auto_increment' => TRUE
	        ],
	        'id_member'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'order_code'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '10',
	        ],
	        'cart'       => [
	            'type'           => 'DATETIME',
	        ],
	        'checkout'       => [
	            'type'           => 'DATETIME',
	        ],
	        'paid'       => [
	            'type'           => 'DATETIME',
	        ],
	        'paid-confirm'       => [
	            'type'           => 'DATETIME',
	        ],
	        'bukti_pembayaran'     => [
	             'type'           => 'TEXT',
	             'null'           => true
	        ],
	        'approved'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '0',
	        ],
	        'status'       => [
	            'type'           => 'ENUM',
	            'constraint'     => array('cart', 'checkout', 'paid', 'paid-confirm'),
	            'null'           => true
	        ],
	        'deleted'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '0',
	        ],
	        'updated_at'     => [
	             'type'           => 'TIMESTAMP',

	        ],
	  ]);
	  $this->forge->addKey('id', TRUE);
	  $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('order');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
