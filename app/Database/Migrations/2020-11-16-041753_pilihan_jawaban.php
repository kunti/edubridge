<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PilihanJawaban extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'INT',
	           'constraint'     => 11,
	           'unsigned'       => TRUE,
	           'auto_increment' => TRUE
	        ],
	        'id_jenistes'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'urutan_bagian_tes'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'sub_bagian'       => [
	            'type'           => 'SMALLINT',
	            'constraint'     => '3',
	            'null'           => true
	        ],
	        'urutan_soal'       => [
	            'type'           => 'SMALLINT',
	            'constraint'     => '3',
	        ],
	        'urutan'       => [
	            'type'           => 'SMALLINT',
	            'constraint'     => '3',
	        ],
	        'jawaban'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '255',
	             'null'           => true
	        ],
	        'gambar'     => [
	             'type'           => 'TEXT',
	             'null'           => true
	        ],
	        'correct_answer'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '0',
	        ],
	        'deleted'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '0',
	        ],
	        'updated_at'     => [
	             'type'           => 'TIMESTAMP',
	             'null'           => true

	        ],
	  ]);
	  $this->forge->addKey('id', TRUE);
	  $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('pilihanjawaban');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
