<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class FeedbackMember extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'INT',
	           'constraint'     => 11,
	           'unsigned'       => TRUE,
	           'auto_increment' => TRUE
	        ],
	        'id_member'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'feedback'     => [
	             'type'           => 'TEXT',
	             'null'           => true
	        ],
	        'status'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '0',
	        ],
	        'updated_at'     => [
	             'type'           => 'TIMESTAMP',
	             
	        ],
	  ]);
	  $this->forge->addKey('id', TRUE);
	  $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('feedbackmember');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
