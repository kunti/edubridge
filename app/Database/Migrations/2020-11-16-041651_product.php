<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Product extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'INT',
	           'constraint'     => 11,
	           'unsigned'       => TRUE,
	           'auto_increment' => TRUE
	        ],
	        'nama'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '255',
	        ],
	        'harga'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'short_desc'     => [
	             'type'           => 'TEXT',
	             'null'           => true
	        ],
	        'desc'     => [
	             'type'           => 'TEXT',
	             'null'           => true
	        ],
	        'gambar'     => [
	             'type'           => 'TEXT',
	             'null'           => true
	        ],
	        'viewed'       => [
	            'type'           => 'INT',
	            'constraint'     => '5',
	            'default' 		 => '0',
	        ],
	        'shared'       => [
	            'type'           => 'INT',
	            'constraint'     => '5',
	            'default' 		 => '0',
	        ],
	        'used'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '1',
	        ],
	        'updated_at'     => [
	             'type'           => 'TIMESTAMP',

	        ],
	  ]);
	  $this->forge->addKey('id', TRUE);
	  $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('product');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
