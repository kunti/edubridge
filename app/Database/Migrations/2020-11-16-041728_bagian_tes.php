<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class BagianTes extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'INT',
	           'constraint'     => 11,
	           'unsigned'       => TRUE,
	           'auto_increment' => TRUE
	        ],
	        'id_jenistes'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'urutan'       => [
	            'type'           => 'SMALLINT',
	            'constraint'     => '3',
	        ],
	        'tipe_jawaban'       => [
	            'type'           => 'ENUM',
	            'constraint'     => array('radio', 'checkbox', 'text'),
	            'default' 		 => 'radio',
	        ],
	        'judul'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '255',
	        ],
	        'petunjuk'     => [
	             'type'           => 'TEXT',
	             'null'           => true
	        ],
	        'deleted'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '0',
	        ],
	        'updated_at'     => [
	             'type'           => 'TIMESTAMP',

	        ],
	  ]);
	  $this->forge->addKey('id', TRUE);
	  $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('bagiantes');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
