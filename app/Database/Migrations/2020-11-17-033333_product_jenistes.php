<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ProductJenistes extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'INT',
	           'constraint'     => 11,
	           'unsigned'       => TRUE,
	           'auto_increment' => TRUE
	        ],
	        'id_product'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'id_jenistes'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'deleted'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '0',
	        ],
	        
	  ]);
	  $this->forge->addKey('id', TRUE);
	  $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('productjenistes');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
