<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class JawabanMember extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'INT',
	           'constraint'     => 11,
	           'unsigned'       => TRUE,
	           'auto_increment' => TRUE
	        ],
	        'id_memberorderproductjenistes'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'urutan_bagian_tes'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'sub_bagian'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	            'null'           => true
	        ],
	        'urutan_soal'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'urutan_jawaban'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '100',
	            'null'           => true
	        ],
	        'jawaban'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '255',
	            'null'           => true
	        ],
	        'jawaban_isian'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '255',
	            'null'           => true
	        ],
	        'jawaban_time'     => [
	             'type'           => 'DATETIME',
	             'null'           => true
	        ],
	        'score'       => [
	            'type'           => 'SMALLINT',
	            'constraint'     => '2',
	            'null'           => true
	        ],
	        'score_time'     => [
	             'type'           => 'DATETIME',
	             'null'           => true
	        ],
	        'scored_by'     => [
	             'type'           => 'INT',
	             'constraint'     => '11',
	            'null'           => true
	        ],
	        'deleted'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '0',
	        ],
	        'updated_at'     => [
	             'type'           => 'TIMESTAMP',
	             
	        ],
	  ]);
	  $this->forge->addKey('id', TRUE);
	  $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('jawabanmember');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
