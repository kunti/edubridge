<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MemberOrderProduct extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'INT',
	           'constraint'     => 11,
	           'unsigned'       => TRUE,
	           'auto_increment' => TRUE
	        ],
	        'id_order'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'id_product'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'nama'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '255',
	        ],
	        'harga'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        // 'start-test'       => [
	        //     'type'           => 'DATETIME',
	        // ],
	        // 'done-test'       => [
	        //     'type'           => 'DATETIME',
	        // ],
	        // 'start-correction'       => [
	        //     'type'           => 'DATETIME',
	        // ],
	        // 'corrected'       => [
	        //     'type'           => 'DATETIME',
	        // ],
	        'start-summarization'       => [
	            'type'           => 'DATETIME',
	        ],
	        'summarized'       => [
	            'type'           => 'DATETIME',
	        ],
	        'summary'       => [
	            'type'           => 'LONGTEXT',
	            'null'           => true
	        ],
	        'chart'       => [
	            'type'           => 'TEXT',
	            'null'           => true
	        ],
	        'jurusan1'       => [
	            'type'           => 'TEXT',
	            'null'           => true
	        ],
	        'jurusan2'       => [
	            'type'           => 'TEXT',
	            'null'           => true
	        ],
	        'jurusan3'       => [
	            'type'           => 'TEXT',
	            'null'           => true
	        ],
	        'jurusan4'       => [
	            'type'           => 'TEXT',
	            'null'           => true
	        ],
	        'jurusan5'       => [
	            'type'           => 'TEXT',
	            'null'           => true
	        ],
	        'descjurusan1'       => [
	            'type'           => 'TEXT',
	            'null'           => true
	        ],
	        'descjurusan2'       => [
	            'type'           => 'TEXT',
	            'null'           => true
	        ],
	        'descjurusan3'       => [
	            'type'           => 'TEXT',
	            'null'           => true
	        ],
	        'descjurusan4'       => [
	            'type'           => 'TEXT',
	            'null'           => true
	        ],
	        'descjurusan5'       => [
	            'type'           => 'TEXT',
	            'null'           => true
	        ],
	        'id_psikolog'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'status'       => [
	            'type'           => 'ENUM',
	            'constraint'     => array('start-summarization', 'summarized'),
	            'null'           => true
	        ],
	        'downloaded'       => [
	            'type'           => 'INT',
	            'constraint'     => '3',
	            'default' 		 => '0',
	        ],
	        'deleted'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '0',
	        ],
	        'updated_at'     => [
	             'type'           => 'TIMESTAMP',
	             
	        ],
	  ]);
	  $this->forge->addKey('id', TRUE);
	  $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('memberorderproduct');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
