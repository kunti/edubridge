<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PenilaianTes extends Migration
{
	public function up()
	{
		$this->forge->addField([
	      'id'           => [
	           'type'           => 'INT',
	           'constraint'     => 11,
	           'unsigned'       => TRUE,
	           'auto_increment' => TRUE
	        ],
	        'id_member_order_product'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'id_psikolog'       => [
	            'type'           => 'INT',
	            'constraint'     => '11',
	        ],
	        'judul'       => [
	            'type'           => 'VARCHAR',
	            'constraint'     => '255',
	        ],
	        'summary'     => [
	             'type'           => 'TEXT',
	             'null'           => true
	        ],
	        'deleted'       => [
	            'type'           => 'TINYINT',
	            'constraint'     => '1',
	            'default' 		 => '0',
	        ],
	        'updated_at'     => [
	             'type'           => 'TIMESTAMP',
	             	
	        ],
	  ]);
	  $this->forge->addKey('id', TRUE);
	  $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
	  $this->forge->createTable('penilaiantes');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
