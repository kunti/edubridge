<?php namespace Config;

class IonAuth extends \IonAuth\Config\IonAuth
{
    // set your specific config
    public $siteTitle                = 'PIPE Psikotes';       // Site Title, example.com
    public $adminEmail               = 'admin@pipe-psikotes.co.id'; // Admin Email, admin@example.com
    public $emailTemplates           = 'App\\Views\\auth\\email\\';
    // ...
}